# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in /Users/songsanghyeon/Library/Android/sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}
#-printmapping map.txt
#-printseeds seed.txt
#-printusage usage.txt
#-printconfiguration config.txt
#noinspection ShrinkerUnresolvedReference


#-libraryjars libs/android-query.0.26.8.jar
#-libraryjars libs/CallMcpayOn_Lib.jar
#-libraryjars libs/KCP_exception.jar
#-libraryjars libs/KCP_van.jar
#-libraryjars libs/pms-gwacheon-library.jar
#-libraryjars libs/WoosimLib251.jar
#-libraryjars libs/keona_platerecoglib.aar


-allowaccessmodification
-keepattributes *Annotation*
-renamesourcefileattribute SourceFile
-keepattributes SourceFile,LineNumberTable
-repackageclasses ''

-dontwarn kr.kcp.java.**
-keep class kr.kcp.java.** { *; }

-dontwarn com.androidquery.**
-keep class com.androidquery.** { *; }

-dontwarn oauth.**
-dontwarn com.android.auth.TwitterHandle.**

-keep class oauth.** { *; }
-keep class com.android.auth.TwitterHandle.** { *; }


-keep class com.google.gson.** { *; }

-keep class com.google.inject.** { *; }
-keep class com.google.gson.stream.** { *; }

-dontwarn com.google.appengine.api.urlfetch.*
-keep class com.google.gson.examples.android.model.** { *; }

# Gson specific classes
-dontwarn sun.misc.**
#-keep class com.google.gson.stream.** { *; }

# Prevent proguard from stripping interface information from TypeAdapterFactory,
# JsonSerializer, JsonDeserializer instances (so they can be used in @JsonAdapter)
-keep class * implements com.google.gson.TypeAdapterFactory
-keep class * implements com.google.gson.JsonSerializer
-keep class * implements com.google.gson.JsonDeserializer





# Begin: Proguard rules for okhttp3
-dontwarn okhttp3.**
-dontwarn okio.**
-dontnote okhttp3.**

# Platform calls Class.forName on types which do not exist on Android to determine platform.
-dontnote retrofit2.Platform
# Retain generic type information for use by reflection by converters and adapters.
-keepattributes Signature
# Retain declared checked exceptions for use by a Proxy instance.
-keepattributes Exceptions

# Retrofit does reflection on generic parameters. InnerClasses is required to use Signature and
# EnclosingMethod is required to use InnerClasses.
-keepattributes Signature, InnerClasses, EnclosingMethod

# Retain service method parameters when optimizing.
-keepclassmembers,allowshrinking,allowobfuscation interface * {
    @retrofit2.http.* <methods>;
}

# Ignore annotation used for build tooling.
-dontwarn org.codehaus.mojo.animal_sniffer.IgnoreJRERequirement

# Ignore JSR 305 annotations for embedding nullability information.
-dontwarn javax.annotation.**

# Guarded by a NoClassDefFoundError try/catch and only used when on the classpath.
-dontwarn kotlin.Unit

# Top-level functions that can only be used by Kotlin.
-dontwarn retrofit2.-KotlinExtensions

-dontwarn org.spongycastle.**

# Glide
-keep public class * implements com.bumptech.glide.module.GlideModule
-keep public enum com.bumptech.glide.load.ImageHeaderParser$** {
  **[] $VALUES;
  public *;
}
-keep class com.crashlytics.** { *; }
-dontwarn com.crashlytics.**
-dontwarn com.google.code.**
-keepattributes *Annotation*
-dontwarn com.google.code.**
-dontwarn  org.apache.**
-dontwarn  jp.wasabeef.recyclerview.**
-dontwarn  com.nostra13.universalimageloader.**
-dontwarn  org.acra.**

#wasabeef recyclerview
-keep class jp.wasabeef.recyclerview.** { *; }
-keepattributes Signature
#HTTP Legacy
-keep class org.apache.** { *; }
-keep interface org.apache.http.** { *; }
-keepattributes Signature
#Universal Image Loader
-keep class com.nostra13.universalimageloader.** { *; }
-keepattributes Signature
#Acra
-keep class org.acra.**  { *; }
-keepattributes Signature
#Support libraries
-keep class com.android.** { *; }
-keepattributes Signature

# Explicitly preserve all serialization members. The Serializable interface
# is only a marker interface, so it wouldn't save them.
-keepclassmembers class * implements java.io.Serializable {
    static final long serialVersionUID;
    private static final java.io.ObjectStreamField[] serialPersistentFields;
    private void writeObject(java.io.ObjectOutputStream);
    private void readObject(java.io.ObjectInputStream);
    java.lang.Object writeReplace();
    java.lang.Object readResolve();
}

# Preserve all native method names and the names of their classes.
-keepclasseswithmembernames class * {
    native <methods>;
}

-keepclasseswithmembernames class * {
    public <init>(android.content.Context, android.util.AttributeSet);
}

-keepclasseswithmembernames class * {
    public <init>(android.content.Context, android.util.AttributeSet, int);
}

# Preserve static fields of inner classes of R classes that might be accessed
# through introspection.
-keepclassmembers class **.R$* {
  public static <fields>;
}

# Preserve the special static methods that are required in all enumeration classes.
-keepclassmembers enum * {
    public static **[] values();
    public static ** valueOf(java.lang.String);
}

-keep public class * {
    public protected *;
}

-keep class * implements android.os.Parcelable {
  public static final android.os.Parcelable$Creator *;
}

-dontnote android.net.http.*
-dontnote org.apache.http.**
-keepattributes EnclosingMethod


#-dontwarn com.adobe.mobile.*
#-keep class com.adobe.mobile.**{ *;}

#-keep class com.google.android.gms.wearable.** {*;}
#-keep interface com.google.android.gms.wearable.** {*;}

#-dontwarn com.adobe.mobile.MessageNotificationHandler
-keep class com.google.android.gms.wearable.**{*;}
#-dontwarn com.adobe.mobile.MessageNoficition
#-dontwarn com.adobe.mobile.**

-dontwarn com.adobe.mobile.*

#-keep com.google.android.gms.** { *; }
#-keep class com.google.android.gms.wearable.** {*;}


# RxJava 프로가드 예외처리
-keep class rx.schedulers.Schedulers {
    public static <methods>;
}
-keep class rx.schedulers.ImmediateScheduler {
    public <methods>;
}
-keep class rx.schedulers.TestScheduler {
    public <methods>;
}
-keep class rx.schedulers.Schedulers {
    public static ** test();
}
-keepclassmembers class rx.internal.util.unsafe.*ArrayQueue*Field* {
    long producerIndex;
    long consumerIndex;
}
-keepclassmembers class rx.internal.util.unsafe.BaseLinkedQueueProducerNodeRef {
    long producerNode;
    long consumerNode;
}

-dontwarn rx.internal.**
-dontnote rx.internal.**
# RxJava 프로가드 예외처리 끝