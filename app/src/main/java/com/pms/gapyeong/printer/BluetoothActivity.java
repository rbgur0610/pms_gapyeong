package com.pms.gapyeong.printer;

import java.io.IOException;
import java.io.InputStream;

import org.apache.http.util.ByteArrayBuffer;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.pms.gapyeong.R;
import com.pms.gapyeong.common.Constants;
import com.woosim.printer.WoosimBarcode;
import com.woosim.printer.WoosimImage;
import com.woosim.printer.WoosimService;

public class BluetoothActivity extends Activity {
    // Debugging
    private static final String TAG = "MainActivity";
    private static final boolean D = true;

    // Message types sent from the BluetoothPrintService Handler
    public static final int MESSAGE_DEVICE_NAME = 1;
    public static final int MESSAGE_TOAST = 2;
    public static final int MESSAGE_READ = 3;

    // Key names received from the BluetoothPrintService Handler
    public static final String DEVICE_NAME = "device_name";
    public static final String TOAST = "toast";

    // Intent request codes
    private static final int REQUEST_CONNECT_DEVICE_SECURE = 1;
    private static final int REQUEST_CONNECT_DEVICE_INSECURE = 2;
    private static final int REQUEST_ENABLE_BT = 3;
    
    private static final byte EOT = 0x04;
    private static final byte LF = 0x0a;
    private static final byte ESC = 0x1b;
    private static final byte GS = 0x1d;
    private static final byte[] CMD_INIT_PRT = {ESC, 0x40};		// Initialize printer (ESC @)
    
    // Layout Views
    private boolean mEmphasis = false;
    private boolean mUnderline = false;
    private byte mCharsize = 0x00;
    private byte mJustification = 0x00;
    private TextView mTrack1View;
    private TextView mTrack2View;
    private TextView mTrack3View;
    
    // Name of the connected device
    private String mConnectedDeviceName = null;
    // Local Bluetooth adapter
    private BluetoothAdapter mBluetoothAdapter = null;
    // Member object for the print services
    private BluetoothPrintService mPrintService = null;
    private WoosimService mWoosim = null;
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(D) Log.e(TAG, "+++ ON CREATE +++");
        
        setContentView(R.layout.bluetooth_sample_main);
        
        // Get local Bluetooth adapter
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        // If the adapter is null, then Bluetooth is not supported
        if (mBluetoothAdapter == null) {
            Toast.makeText(this, R.string.toast_bt_na, Toast.LENGTH_LONG).show();
            finish();
            return;
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if(D) Log.e(TAG, "++ ON START ++");

        // If BT is not on, request that it be enabled.
        // setupPrint() will then be called during onActivityResult
        if (!mBluetoothAdapter.isEnabled()) {
            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
        // Otherwise, setup the chat session
        } else {
            if (mPrintService == null) setupPrint();
        }
    }

    @Override
    public synchronized void onResume() {
        super.onResume();
        if(D) Log.e(TAG, "+ ON RESUME +");

        // Performing this check in onResume() covers the case in which BT was
        // not enabled during onStart(), so we were paused to enable it...
        // onResume() will be called when ACTION_REQUEST_ENABLE activity returns.
        if (mPrintService != null) {
            // Only if the state is STATE_NONE, do we know that we haven't started already
            if (mPrintService.getState() == BluetoothPrintService.STATE_NONE) {
              // Start the Bluetooth print services
            	mPrintService.start();
            }
        }
    }

    private void setupPrint() {
        if(D) Log.d(TAG, "setupPrint()");
        
        Spinner spinner = (Spinner) findViewById(R.id.spn_char_size);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
                this, R.array.char_size_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(
                new OnItemSelectedListener() {
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    	if (position == 1) mCharsize = (byte)0x11;
                    	else if (position == 2) mCharsize = (byte)0x22;
                    	else if (position == 3) mCharsize = (byte)0x33;
                    	else if (position == 4) mCharsize = (byte)0x44;
                    	else if (position == 5) mCharsize = (byte)0x55;
                    	else if (position == 6) mCharsize = (byte)0x66;
                    	else if (position == 7) mCharsize = (byte)0x77;
                    	else mCharsize = (byte)0x00;
                    }
                    
                    public void onNothingSelected(AdapterView<?> parent) { }
                });

        mTrack1View = (TextView)findViewById(R.id.view_1track);
        mTrack2View = (TextView)findViewById(R.id.view_2track);
        mTrack3View = (TextView)findViewById(R.id.view_3track);

        // Initialize the BluetoothPrintService to perform bluetooth connections
        mPrintService = new BluetoothPrintService(this, mHandler);
        mWoosim = new WoosimService(mHandler);
    }

    // The Handler that gets information back from the BluetoothPrintService
    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
            case MESSAGE_DEVICE_NAME:
                // save the connected device's name
                mConnectedDeviceName = msg.getData().getString(DEVICE_NAME);
                Toast.makeText(getApplicationContext(), "Connected to " + mConnectedDeviceName, Toast.LENGTH_SHORT).show();
                break;
            case MESSAGE_TOAST:
                Toast.makeText(getApplicationContext(), msg.getData().getString(TOAST), Toast.LENGTH_SHORT).show();
                break;
            case MESSAGE_READ:
                mWoosim.processRcvData((byte[])msg.obj, msg.arg1);
                break;
            case WoosimService.MESSAGE_PRINTER:
            	switch (msg.arg1) {
            	case WoosimService.MSR:
            		Log.d(TAG, "MSR");
            		if (msg.arg2 == 0) {
            			Toast.makeText(getApplicationContext(), "MSR reading failure", Toast.LENGTH_SHORT).show();
            		} else {
                    	byte[][] track = (byte[][])msg.obj;
                    	if (track[0] != null) {
                    		String str = new String(track[0]); 
                    		mTrack1View.setText(str);
                    	}
                    	if (track[1] != null) {
                    		String str = new String(track[1]);
                    		mTrack2View.setText(str);
                    	}
                    	if (track[2] != null) {
                    		String str = new String(track[2]);
                    		mTrack3View.setText(str);
                    	}
            		}
                	break;
            	}
            	break;
            }
        }
    };

    @Override
    public synchronized void onPause() {
        super.onPause();
        if(D) Log.e(TAG, "- ON PAUSE -");
    }

    @Override
    public void onStop() {
        super.onStop();
        if(D) Log.e(TAG, "-- ON STOP --");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // Stop the Bluetooth print services
        if (mPrintService != null) mPrintService.stop();
        if(D) Log.e(TAG, "--- ON DESTROY ---");
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.bluetooth_menu, menu);
        return true;
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent serverIntent = null;
        switch (item.getItemId()) {
        case R.id.secure_connect_scan:
            // Launch the DeviceListActivity to see devices and do scan
            serverIntent = new Intent(this, BluetoothDeviceListActivity.class);
            startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE_SECURE);
            return true;
        case R.id.insecure_connect_scan:
            // Launch the DeviceListActivity to see devices and do scan
            serverIntent = new Intent(this, BluetoothDeviceListActivity.class);
            startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE_INSECURE);
            return true;
        }
        return false;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(D) Log.d(TAG, "onActivityResult " + resultCode);
        switch (requestCode) {
        case REQUEST_CONNECT_DEVICE_SECURE:
            // When DeviceListActivity returns with a device to connect
            if (resultCode == Activity.RESULT_OK) {
                connectDevice(data, true);
            }
            break;
        case REQUEST_CONNECT_DEVICE_INSECURE:
            // When DeviceListActivity returns with a device to connect
            if (resultCode == Activity.RESULT_OK) {
                connectDevice(data, false);
            }
            break;
        case REQUEST_ENABLE_BT:
            // When the request to enable Bluetooth returns
            if (resultCode == Activity.RESULT_OK) {
                // Bluetooth is now enabled, so set up a print
            	setupPrint();
            } else {
                // User did not enable Bluetooth or an error occurred
                Log.d(TAG, "BT not enabled");
                Toast.makeText(this, R.string.bt_not_enabled_leaving, Toast.LENGTH_SHORT).show();
                finish();
            }
        }
    }

    private void connectDevice(Intent data, boolean secure) {
    	try{
			// Get the device MAC address
			String address = data.getExtras().getString(BluetoothDeviceListActivity.EXTRA_DEVICE_ADDRESS);
			// Get the BLuetoothDevice object
			BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
			// Attempt to connect to the device
			mPrintService.connect(device, secure);
		} catch (IllegalArgumentException e) {
			System.out.println("IllegalArgumentException 예외 발생");
		} catch (NullPointerException e) {
			System.out.println("NullPointerException 예외 발생");
		}

    }
    
    /**
     * Print data.
     * @param data  A byte array to print.
     */
    private void sendData(byte[] data) {
        // Check that we're actually connected before trying printing
        if (mPrintService.getState() != BluetoothPrintService.STATE_CONNECTED) {
            Toast.makeText(this, R.string.not_connected, Toast.LENGTH_SHORT).show();
            return;
        }
        
        // Check that there's actually something to send
        if (data.length > 0) 
        	mPrintService.write(data);
    }

    /**
     * On click function for sample print button. 
     */
    public void onCheckboxClicked(View v) {
    	boolean checked = ((CheckBox) v).isChecked();
    	
    	switch(v.getId()) {
    	case R.id.cbx_emphasis:
    		if (checked) mEmphasis = true;
    		else mEmphasis = false;
    		break;
    	case R.id.cbx_underline:
    		if (checked) mUnderline = true;
    		else mUnderline = false;
    		break;
    	}
    }
    
    public void onRadioButtonClicked(View v) {
    	boolean checked = ((RadioButton) v).isChecked();
    	
    	switch(v.getId()) {
    	case R.id.rbtn_left: 
    		if (checked) mJustification = (byte)0x00;
    		break;
    	case R.id.rbtn_center:
    		if (checked) mJustification = (byte)0x01;
    		break;
    	case R.id.rbtn_right:
    		if (checked) mJustification = (byte)0x02;
    		break;
    	}
    }
    
    public void printText(View v) {
    	EditText editText = (EditText) findViewById(R.id.edit_text);
    	String string = editText.getText().toString();
    	byte[] text = string.getBytes();
    	if (text.length == 0) return;
    	
    	ByteArrayBuffer buffer = new ByteArrayBuffer(1024);
    	
    	byte[] cmd = {ESC, 0x45, (byte)(mEmphasis ? 1 : 0),		// ESC E
    				  ESC, 0x2D, (byte)(mUnderline ? 1 : 0),	// ESC -
    				  GS,  0x21, mCharsize,						// GS !	
    				  ESC, 0x61, mJustification};				// ESC a
    	
    	buffer.append(cmd, 0, cmd.length);
    	buffer.append(text, 0, text.length);
    	buffer.append(LF);
    	
    	sendData(CMD_INIT_PRT);
    	sendData(buffer.toByteArray());
    }
    
    public void pirnt2inchReceipt(View v) {
    	InputStream inStream = getResources().openRawResource(R.raw.receipt2);
    	sendData(CMD_INIT_PRT);
    	try {
    		byte[] data = new byte[inStream.available()];
    		while (inStream.read(data) != -1)
    		{
    			sendData(data);
    		}
    	} catch (IOException e) {
			if(Constants.DEBUG_PRINT_LOG){
				e.printStackTrace();
			}else{
				System.out.println("예외 발생");
			}
    	} finally {
    		if (inStream != null)
				try {
					inStream.close();
				} catch (IOException e) {
					if(Constants.DEBUG_PRINT_LOG){
						e.printStackTrace();
					}else{
						System.out.println("예외 발생");
					}
				}
    	}
    }
    
    public void pirntBMPImage(View v) {
    	BitmapFactory.Options options = new BitmapFactory.Options();
		options.inScaled = false;
    	Bitmap bmp = BitmapFactory.decodeResource(getResources(), R.drawable.logo1, options);
    	if (bmp == null) {
    		Log.e(TAG, "resource decoding is failed");
    		return;
    	}
    	
    	byte[] data = WoosimImage.printRGBbitmap(0, 0, 384, 200, bmp);
    	bmp.recycle();
    	
    	byte[] cmd_pagemode = {ESC, 0x4c};	// Select page mode (ESC L)
    	byte[] cmd_stdmode = {ESC, 0x53};	// Select standard mode (ESC S)
    	
    	sendData(cmd_pagemode);
    	sendData(data);
    	sendData(cmd_stdmode);
    }

    /**
     * On click function for barcode print button. 
     */
    public void print1DBarcode(View v) {
    	final byte[] barcode =  {0x30,0x31,0x32,0x33,0x34,0x35,0x36,0x37,0x38,0x39,0x30};
    	final byte[] barcode8 = {0x30,0x31,0x32,0x33,0x34,0x35,0x36,0x37};
    	ByteArrayBuffer buffer = new ByteArrayBuffer(1024);
    	
    	final String title1 = "UPC-A Barcode\r\n";
    	byte[] UPCA = WoosimBarcode.createBarcode(WoosimBarcode.UPC_A, 2, 60, true, barcode);
    	buffer.append(title1.getBytes(), 0, title1.getBytes().length);
    	buffer.append(UPCA, 0, UPCA.length);
    	buffer.append(LF);
    	
    	final String title2 = "UPC-E Barcode\r\n";
    	byte[] UPCE = WoosimBarcode.createBarcode(WoosimBarcode.UPC_E, 2, 60, true, barcode);
    	buffer.append(title2.getBytes(), 0, title2.getBytes().length);
    	buffer.append(UPCE, 0, UPCE.length);
    	buffer.append(LF);
    	
    	final String title3 = "EAN13 Barcode\r\n";
    	byte[] EAN13 = WoosimBarcode.createBarcode(WoosimBarcode.EAN13, 2, 60, true, barcode);
    	buffer.append(title3.getBytes(), 0, title3.getBytes().length);
    	buffer.append(EAN13, 0, EAN13.length);
    	buffer.append(LF);
    	
    	final String title4 = "EAN8 Barcode\r\n";
    	byte[] EAN8 = WoosimBarcode.createBarcode(WoosimBarcode.EAN8, 2, 60, true, barcode8);
    	buffer.append(title4.getBytes(), 0, title4.getBytes().length);
    	buffer.append(EAN8, 0, EAN8.length);
    	buffer.append(LF);
    	
    	final String title5 = "CODE39 Barcode\r\n";
    	byte[] CODE39 = WoosimBarcode.createBarcode(WoosimBarcode.CODE39, 2, 60, true, barcode);
    	buffer.append(title5.getBytes(), 0, title5.getBytes().length);
    	buffer.append(CODE39, 0, CODE39.length);
    	buffer.append(LF);
    	
    	final String title6 = "ITF Barcode\r\n";
    	byte[] ITF = WoosimBarcode.createBarcode(WoosimBarcode.ITF, 2, 60, true, barcode);
    	buffer.append(title6.getBytes(), 0, title6.getBytes().length);
    	buffer.append(ITF, 0, ITF.length);
    	buffer.append(LF);
    	
    	final String title7 = "CODEBAR Barcode\r\n";
    	byte[] CODEBAR = WoosimBarcode.createBarcode(WoosimBarcode.CODEBAR, 2, 60, true, barcode);
    	buffer.append(title7.getBytes(), 0, title7.getBytes().length);
    	buffer.append(CODEBAR, 0, CODEBAR.length);
    	buffer.append(LF);
    	
    	sendData(CMD_INIT_PRT);
    	sendData(buffer.toByteArray());
    }
    
    public void print2DBarcode(View v) {
    	final byte[] barcode = {0x30,0x31,0x32,0x33,0x34,0x35,0x36,0x37,0x38,0x39,0x30};
    	ByteArrayBuffer buffer = new ByteArrayBuffer(1024);
    	
    	final String title1 = "PDF417 2D Barcode\r\n";
    	byte[] PDF417 = WoosimBarcode.create2DBarcodePDF417(2, 3, 4, 2, false, barcode);
    	buffer.append(title1.getBytes(), 0, title1.getBytes().length);
    	buffer.append(PDF417, 0, PDF417.length);
    	buffer.append(LF);
    	
    	final String title2 = "DATAMATRIX 2D Barcode\r\n";
    	byte[] dataMatrix = WoosimBarcode.create2DBarcodeDataMatrix(0, 0, 4, barcode);
    	buffer.append(title2.getBytes(), 0, title2.getBytes().length);
    	buffer.append(dataMatrix, 0, dataMatrix.length);
    	buffer.append(LF);
    	
    	final String title3 = "QR-CODE 2D Barcode\r\n";
    	byte[] QRCode = WoosimBarcode.create2DBarcodeQRCode(0, (byte)0x4d, 5, barcode);
    	buffer.append(title3.getBytes(), 0, title3.getBytes().length);
    	buffer.append(QRCode, 0, QRCode.length);
    	buffer.append(LF);
    	
    	final String title4 = "Micro PDF417 2D Barcode\r\n";
    	byte[] microPDF417 = WoosimBarcode.create2DBarcodeMicroPDF417(2, 2, 0, 2, barcode);
    	buffer.append(title4.getBytes(), 0, title4.getBytes().length);
    	buffer.append(microPDF417, 0, microPDF417.length);
    	buffer.append(LF);
    	
    	final String title5 = "Truncated PDF417 2D Barcode\r\n";
    	byte[] truncPDF417 = WoosimBarcode.create2DBarcodeTruncPDF417(2, 3, 4, 2, false, barcode);
    	buffer.append(title5.getBytes(), 0, title5.getBytes().length);
    	buffer.append(truncPDF417, 0, truncPDF417.length);
    	buffer.append(LF);
    	
    	// Maxicode can be printed only with RX version
    	final String title6 = "Maxicode 2D Barcode\r\n";
    	final byte[] mxcode = {0x41,0x42,0x43,0x44,0x45,0x31,0x32,0x33,0x34,0x35,0x61,0x62,0x63,0x64,0x65};
    	byte[] maxCode = WoosimBarcode.create2DBarcodeMaxicode(4, mxcode);
    	buffer.append(title6.getBytes(), 0, title6.getBytes().length);
    	buffer.append(maxCode, 0, maxCode.length);
    	buffer.append(LF);
    	
    	sendData(CMD_INIT_PRT);
    	sendData(buffer.toByteArray());
    }

    public void printGS1Databar(View v) {
    	final byte[] data = {0x30,0x30,0x30,0x31,0x32,0x33,0x34,0x35,0x36,0x37,0x38,0x39,0x30};
    	ByteArrayBuffer buffer = new ByteArrayBuffer(1024);
    	
    	final String title0 = "GS1 Databar type0\r\n";
    	byte[] gs0 = WoosimBarcode.createGS1Databar(0, 2, data);
    	buffer.append(title0.getBytes(), 0, title0.getBytes().length);
    	buffer.append(gs0, 0, gs0.length);
    	buffer.append(LF);
    	
    	final String title1 = "GS1 Databar type1\r\n";
    	byte[] gs1 = WoosimBarcode.createGS1Databar(1, 2, data);
    	buffer.append(title1.getBytes(), 0, title1.getBytes().length);
    	buffer.append(gs1, 0, gs1.length);
    	buffer.append(LF);
    	
    	final String title2 = "GS1 Databar type2\r\n";
    	byte[] gs2 = WoosimBarcode.createGS1Databar(2, 2, data);
    	buffer.append(title2.getBytes(), 0, title2.getBytes().length);
    	buffer.append(gs2, 0, gs2.length);
    	buffer.append(LF);
    	
    	final String title3 = "GS1 Databar type3\r\n";
    	byte[] gs3 = WoosimBarcode.createGS1Databar(3, 2, data);
    	buffer.append(title3.getBytes(), 0, title3.getBytes().length);
    	buffer.append(gs3, 0, gs3.length);
    	buffer.append(LF);
    	
    	final String title4 = "GS1 Databar type4\r\n";
    	byte[] gs4 = WoosimBarcode.createGS1Databar(4, 2, data);
    	buffer.append(title4.getBytes(), 0, title4.getBytes().length);
    	buffer.append(gs4, 0, gs4.length);
    	buffer.append(LF);
    	
    	final String title5 = "GS1 Databar type5\r\n";
    	final byte[] data5 = {0x5b,0x30,0x31,0x5d,0x39,0x30,0x30,0x31,0x32,0x33,0x34,0x35,0x36,0x37,0x38,0x39,0x30,0x38,
    						  0x5b,0x33,0x31,0x30,0x33,0x5d,0x30,0x31,0x32,0x32,0x33,0x33};
    	byte[] gs5 = WoosimBarcode.createGS1Databar(5, 2, data5);
    	buffer.append(title5.getBytes(), 0, title5.getBytes().length);
    	buffer.append(gs5, 0, gs5.length);
    	buffer.append(LF);
    	
    	final String title6 = "GS1 Databar type6\r\n";
    	final byte[] data6 = {0x5b,0x30,0x31,0x5d,0x39,0x30,0x30,0x31,0x32,0x33,0x34,0x35,0x36,0x37,0x38,0x39,0x30,0x38,
    						  0x5b,0x33,0x31,0x30,0x33,0x5d,0x30,0x31,0x32,0x32,0x33,0x33,
    						  0x5b,0x31,0x35,0x5d,0x39,0x39,0x31,0x32,0x33,0x31};
    	byte[] gs6 = WoosimBarcode.createGS1Databar(6, 4, data6);
    	buffer.append(title6.getBytes(), 0, title6.getBytes().length);
    	buffer.append(gs6, 0, gs6.length);
    	buffer.append(LF);
    	
    	sendData(CMD_INIT_PRT);
    	sendData(buffer.toByteArray());
    }
    
    public void setMSRDoubleTrackMode(View v) {
    	byte[] cmd_msr_dbltrack = {ESC, 0x4d, 0x45};	// Set MSR double track reading mode (ESC M E)
    	sendData(cmd_msr_dbltrack);
    }

    public void setMSRTripleTrackMode(View v) {
    	byte[] cmd_msr_tpltrack = {ESC, 0x4d, 0x46};	// Set MSR triple track reading mode (ESC M F)
    	sendData(cmd_msr_tpltrack);
    }

    public void cancelMSRMode(View v) {
    	byte[] cmd_msr_cancel = {EOT};	// Cancel MSR reading mode
    	sendData(cmd_msr_cancel);
    }
    
    public void clearMSRInfo(View v) {
    	mTrack1View.setText("");
    	mTrack2View.setText("");
    	mTrack3View.setText("");
    }

}
