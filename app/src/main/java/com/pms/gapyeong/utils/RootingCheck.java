package com.pms.gapyeong.utils;

import android.app.Activity;
import android.os.Environment;

import java.io.File;
import java.io.IOException;

/**
 * Created by KimJehyun on 2017. 5. 29..
 */

public class RootingCheck {
    private static Activity mActivity;
    //   루팅 체크
    public static final String ROOT_PATH = Environment.getExternalStorageDirectory() + "";
    public static final String ROOTING_PATH_1 = "/system/xbin/su";
    public static final String ROOTING_PATH_2 = "/system/bin/su";
    public static final String ROOTING_PATH_3 = "/system/bin/.user/.su";
    public static final String ROOTING_PATH_4 = "/system/app/Superuser.apk/";
    public static final String ROOTING_PATH_5 = "/dev/com.noshufou.android.su";
    public static final String ROOTING_PATH_6 = "/data/data/com.tegrak.lagfix";
    public static final String ROOTING_PATH_7 = "/data/data/eu.chainfire.supersu";
    public static final String ROOTING_PATH_8 = "/data/data/com.noshufou.android.su";
    public static final String ROOTING_PATH_9 = "/data/data/com.jrummy.root.browserfree";
    public static final String ROOTING_PATH_10 = "/data/app/com.tegrak.lagfix.apk";
    public static final String ROOTING_PATH_11 = "/data/app/eu.chainfire.supersu.apk";
    public static final String ROOTING_PATH_12 = "/data/app/com.noshufou.android.su.apk";
    public static final String ROOTING_PATH_13 = "/data/app/com.jrummy.root.browserfree.apk";
    public static final String ROOTING_PATH_14 = "/su/bin/su";
    public static final String ROOTING_PATH_15 = "/su/xbin/su";
    public static final String ROOTING_PATH_16 = "/su/bin/s.user/.su";
    public static final String ROOTING_PATH_17 = "/data/data/com.jrummy.busybox.installer";
    public static final String ROOTING_PATH_18 = "/data/app/com.jrummy.busybox.installer";
    public static final String ROOTING_PATH_19 = "/data/data/me.blog.markan.UnRooting";
    public static final String ROOTING_PATH_20 = "/data/app/me.blog.markan.UnRooting";
    public static final String ROOTING_PATH_21 = "/data/data/com.formyhm.hideroot";
    public static final String ROOTING_PATH_22 = "/data/app/com.formyhm.hideroot";

    public static String[] RootFilesPath = new String[]{
            ROOT_PATH + ROOTING_PATH_1,
            ROOT_PATH + ROOTING_PATH_2,
            ROOT_PATH + ROOTING_PATH_3,
            ROOT_PATH + ROOTING_PATH_4,
            ROOT_PATH + ROOTING_PATH_5,
            ROOT_PATH + ROOTING_PATH_6,
            ROOT_PATH + ROOTING_PATH_7,
            ROOT_PATH + ROOTING_PATH_8,
            ROOT_PATH + ROOTING_PATH_9,
            ROOT_PATH + ROOTING_PATH_10,
            ROOT_PATH + ROOTING_PATH_11,
            ROOT_PATH + ROOTING_PATH_12,
            ROOT_PATH + ROOTING_PATH_13,
            ROOT_PATH + ROOTING_PATH_14,
            ROOT_PATH + ROOTING_PATH_15,
            ROOT_PATH + ROOTING_PATH_16,
            ROOT_PATH + ROOTING_PATH_17,
            ROOT_PATH + ROOTING_PATH_18,
            ROOT_PATH + ROOTING_PATH_19,
            ROOT_PATH + ROOTING_PATH_20,
            ROOT_PATH + ROOTING_PATH_21,
            ROOT_PATH + ROOTING_PATH_22
    };


    public static Boolean chkRooting(Activity activity){
        mActivity=activity;

        // 170511 jhkim. 루팅체크
        boolean isRootingFlag;
        try {
            Runtime.getRuntime().exec("su");
            return true;  // 루팅인 경우

        } catch (IOException e) {
            System.out.println("IOException 예외 발생");

            isRootingFlag = false;
        }
        if (!isRootingFlag) {
            isRootingFlag = checkRootingFiles(createFiles(RootFilesPath));
        }

        return isRootingFlag;
    }

    // 루팅파일 의심 Path를 가진 파일들을 생성 한다.
    private static File[] createFiles(String[] sfiles) {
        File[] rootingFiles = new File[sfiles.length];
        for (int i = 0; i < sfiles.length; i++) {
            rootingFiles[i] = new File(sfiles[i]);
        }
        return rootingFiles;
    }

    // 루팅파일 여부를 확인 한다.
    private static boolean checkRootingFiles(File... file) {
        boolean result = false;
        for (File f : file) {
            if (f != null && f.exists() && f.isFile()) {
                result = true;
                break;
            } else {
                result = false;
            }
        }
        return result;
    }
}
