package com.pms.gapyeong.vo;

import java.io.Serializable;

/**
 * 주차현황 
 *
 */
public class ParkIO implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 9169283425170620740L;
	private  String kn_comment;

	private String pioNum;		 // 주차번호
	private String cdPark;		 // 주차장 코드
	private String pioDay;		 // 주차일
	private String carNo;		 // 차량 번호
	private String cdArea;		 // 주차 면수
	private String parkType;	 // 정기권, 일일권, 시간권
	private String disCd;  		 // 요금 할인
	private String tel;			 // 연락처
	private String chargeType;	 // 요금종류
	
	private String parkInAmt;	 // 주차원금(입차)
	private String parkOutAmt;   // 주차원금(출차)
	private String parkAmt;		 // 주차원금(총합)
	
	private String advInAmt;	 // 선불금(입차)
	private String advOutAmt;	 // 선불금(출차)
	private String advAmt;		 // 선불금(총합)
	
	private String disInAmt;	 // 할인금액(입차)
	private String disOutAmt;	 // 할인금액(출차)
	private String disAmt;		 // 할인금액(총합)
	
	private String cashInAmt;	 // 현금금액(입차)
	private String cashOutAmt;	 // 현금금액(출차)
	private String cashAmt;		 // 현금금액(총합)
	
	private String cardInAmt;	 // 카드금액(입차)
	private String cardOutAmt;   // 카드금액(출차)
	private String cardAmt;		 // 카드금액(총합)
	
	private String truncInAmt;	 // 절삭금액(입차)
	private String truncOutAmt;	 // 절삭금액(출차)
	private String truncAmt;	 // 절삭금액(총합)
	
	private String couponAmt;	 // 쿠폰금액
	private String returnAmt;	 // 환불금액
	private String yetAmt;		 // 미납금액 총액
	private String realAmt;		 // 총 납부금액
	
	private String parkTime;	 // 주차 분
	private String parkInDay;	 // 입차시간 
	private String parkOutDay;	 // 출차시간
	private String cardNo;		 // 카드번호
	private String cardCompany;  // 카드사명
	private String approvalNo;	 // 승인번호
	private String approvalDate; // 승인일자
	private String cash_approvalNo;
	private String cash_approvalDate;
	
	public ParkIO() {
		
	}
	
	public ParkIO(String pioNum 
				, String cdPark
				, String pioDay
				, String carNo
				, String cdArea
				, String parkType
				, String disCd
				, String tel
				, String parkInAmt
				, String parkOutAmt
				, String parkAmt
				, String advInAmt
				, String advOutAmt
				, String advAmt
				, String disInAmt
				, String disOutAmt
				, String disAmt
				, String cashInAmt
				, String cashOutAmt
				, String cashAmt
				, String cardInAmt
				, String cardOutAmt
				, String cardAmt
				, String truncInAmt
				, String truncOutAmt
				, String truncAmt
				, String couponAmt
				, String returnAmt
				, String yetAmt
				, String realAmt
				, String parkTime
				, String parkInDay
				, String parkOutDay
				, String cardNo
				, String cardCompany
				, String approvalNo
				, String approvalDate
				, String chargeType
			, String kn_comment
			) {
		this.pioNum       = pioNum;
		this.cdPark    	  = cdPark;
		this.pioDay    	  = pioDay;
		this.carNo     	  = carNo;
		this.cdArea	      = cdArea;
		this.parkType     = parkType;
		this.disCd        = disCd;
		this.tel          = tel;
		this.parkInAmt    = parkInAmt;
		this.parkOutAmt   = parkOutAmt;
		this.parkAmt   	  = parkAmt;
		this.advInAmt     = advInAmt;
		this.advOutAmt    = advOutAmt;
		this.advAmt       = advAmt;
		this.disInAmt  	  = disInAmt;
		this.disOutAmt 	  = disOutAmt;
		this.disAmt    	  = disAmt;
		this.cashInAmt    = cashInAmt;
		this.cashOutAmt   = cashOutAmt;
		this.cashAmt      = cashAmt;
		this.cardInAmt    = cardInAmt;
		this.cardOutAmt   = cardOutAmt;
		this.cardAmt      = cardAmt;
		this.truncInAmt   = truncInAmt;
		this.truncOutAmt  = truncOutAmt;
		this.truncAmt     = truncAmt;
		this.couponAmt 	  = couponAmt;
		this.returnAmt 	  = returnAmt;
		this.yetAmt       = yetAmt;
		this.realAmt      = realAmt;
		this.parkTime 	  = parkTime;
		this.parkInDay 	  = parkInDay;
		this.parkOutDay   = parkOutDay;
		this.cardNo   	  = cardNo;
		this.cardCompany  = cardCompany;
		this.approvalNo   = approvalNo;
		this.approvalDate = approvalDate;
		this.chargeType   = chargeType;
		this.kn_comment = kn_comment;
	}
	
	public void setCashApprovalNo(String _cashno) {
		this.cash_approvalNo = _cashno;
	}
	
	public String getCashApprovalNo() {
		return this.cash_approvalNo;
	}
	
	public void setCashApprovalDate(String _cashdate) {
		this.cash_approvalDate = _cashdate;
	}
	
	public String getCashApprovalDate() {
		return this.cash_approvalDate;
	}		
	
	public String getPioNum() {
		return pioNum;
	}

	public void setPioNum(String pioNum) {
		this.pioNum = pioNum;
	}
	
	public String getCdPark() {
		return cdPark;
	}

	public void setCdPark(String cdPark) {
		this.cdPark = cdPark;
	}
	
	public String getPioDay() {
		return pioDay;
	}
	
	public void setPioDay(String pioDay) {
		this.pioDay = pioDay;
	}	
	
	public String getCarNo() {
		return carNo;
	}

	public void setCarNo(String carNo) {
		this.carNo = carNo;
	}
	
	public String getCdArea() {
		return cdArea;
	}

	public void setCdArea(String cdArea) {
		this.cdArea = cdArea;
	}	
	
	public String getParkType() {
		return parkType;
	}
	
	public void setParkType(String parkType) {
		this.parkType = parkType;
	}
	
	public String getDisCd() {
		return disCd;
	}

	public void setDisCd(String disCd) {
		this.disCd = disCd;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}
	
	public String getParkInAmt() {
		return parkInAmt;
	}

	public void setParkInAmt(String parkInAmt) {
		this.parkInAmt = parkInAmt;
	}

	public String getParkOutAmt() {
		return parkOutAmt;
	}

	public void setParkOutAmt(String parkOutAmt) {
		this.parkOutAmt = parkOutAmt;
	}	
	
	public String getParkAmt() {
		return parkAmt;
	}

	public void setParkAmt(String parkAmt) {
		this.parkAmt = parkAmt;
	}

	
	public String getAdvInAmt() {
		return advInAmt;
	}

	public void setAdvInAmt(String advInAmt) {
		this.advInAmt = advInAmt;
	}

	public String getAdvOutAmt() {
		return advOutAmt;
	}

	public void setAdvOutAmt(String advOutAmt) {
		this.advOutAmt = advOutAmt;
	}
	
	public String getAdvAmt() {
		return advAmt;
	}

	public void setAdvAmt(String advAmt) {
		this.advAmt = advAmt;
	}
	
	
	public String getDisInAmt() {
		return disInAmt;
	}

	public void setDisInAmt(String disInAmt) {
		this.disInAmt = disInAmt;
	}

	public String getDisOutAmt() {
		return disOutAmt;
	}

	public void setDisOutAmt(String disOutAmt) {
		this.disOutAmt = disOutAmt;
	}
	
	public String getDisAmt() {
		return disAmt;
	}

	public void setDisAmt(String disAmt) {
		this.disAmt = disAmt;
	}
	
	
	public String getCashInAmt() {
		return cashInAmt;
	}

	public void setCashInAmt(String cashInAmt) {
		this.cashInAmt = cashInAmt;
	}

	public String getCashOutAmt() {
		return cashOutAmt;
	}

	public void setCashOutAmt(String cashOutAmt) {
		this.cashOutAmt = cashOutAmt;
	}
	
	public String getCashAmt() {
		return cashAmt;
	}
	
	public void setCashAmt(String cashAmt) {
		this.cashAmt = cashAmt;
	}
	
	
	public String getCardInAmt() {
		return cardInAmt;
	}

	public void setCardInAmt(String cardInAmt) {
		this.cardInAmt = cardInAmt;
	}

	public String getCardOutAmt() {
		return cardOutAmt;
	}

	public void setCardOutAmt(String cardOutAmt) {
		this.cardOutAmt = cardOutAmt;
	}

	public String getCardAmt() {
		return cardAmt;
	}
	
	public void setCardAmt(String cardAmt) {
		this.cardAmt = cardAmt;
	}
	
	public String getTruncInAmt() {
		return truncInAmt;
	}

	public void setTruncInAmt(String truncInAmt) {
		this.truncInAmt = truncInAmt;
	}

	public String getTruncOutAmt() {
		return truncOutAmt;
	}

	public void setTruncOutAmt(String truncOutAmt) {
		this.truncOutAmt = truncOutAmt;
	}

	public String getTruncAmt() {
		return truncAmt;
	}

	public void setTruncAmt(String truncAmt) {
		this.truncAmt = truncAmt;
	}
	
	
	public String getCouponAmt() {
		return couponAmt;
	}

	public void setCouponAmt(String couponAmt) {
		this.couponAmt = couponAmt;
	}	
	
	public String getReturnAmt() {
		return returnAmt;
	}
	
	public void setReturnAmt(String returnAmt) {
		this.returnAmt = returnAmt;
	}
	
	public String getYetAmt() {
		return yetAmt;
	}
	
	public void setYetAmt(String yetAmt) {
		this.yetAmt = yetAmt;
	}
	
	public String getRealAmt() {
		return realAmt;
	}

	public void setRealAmt(String realAmt) {
		this.realAmt = realAmt;
	}

	public String getParkTime() {
		return parkTime;
	}

	public void setParkTime(String parkTime) {
		this.parkTime = parkTime;
	}
	
	public String getParkInDay() {
		return parkInDay;
	}
	
	public void setParkInDay(String parkInDay) {
		this.parkInDay = parkInDay;
	}

	public String getParkOutDay() {
		return parkOutDay;
	}

	public void setParkOutDay(String parkOutDay) {
		this.parkOutDay = parkOutDay;
	}
	
	public String getCardNo() {
		return cardNo;
	}

	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}

	public String getCardCompany() {
		return cardCompany;
	}

	public void setCardCompany(String cardCompany) {
		this.cardCompany = cardCompany;
	}

	public String getApprovalNo() {
		return approvalNo;
	}

	public void setApprovalNo(String approvalNo) {
		this.approvalNo = approvalNo;
	}	
	
	public String getApprovalDate() {
		return approvalDate;
	}

	public void setApprovalDate(String approvalDate) {
		this.approvalDate = approvalDate;
	}	
	
	public String getChargeType() {
		return chargeType;
	}

	public void setChargeType(String chargeType) {
		this.chargeType = chargeType;
	}	

	@Override
	public String toString() {
		return "pioNum : " + this.pioNum + "|" 
				+ "parkType : " + this.parkType + "|"
				+ "returnAmt : " + this.returnAmt + "|"
				+ "realAmt : " + this.realAmt + "|"
				+ "pioDay : " + this.pioDay + "|"
				+ "tel : " + this.tel + "|"
				+ "parkInDay : " + this.parkInDay + "|"
				+ "couponAmt : " + this.couponAmt + "|"
				+ "parkAmt : " + this.parkAmt + "|"
				+ "disAmt : " + this.disAmt + "|"
				+ "parkTime : " + this.parkTime;
	}

	public String getKn_comment() {
		return kn_comment;
	}

	public void setKn_comment(String kn_comment) {
		this.kn_comment = kn_comment;
	}
}
