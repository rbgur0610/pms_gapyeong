package com.pms.gapyeong.vo;

import java.util.ArrayList;

import org.apache.http.util.ByteArrayBuffer;

import com.pms.gapyeong.common.Util;


/**
 * 입출정산현황
 *
 */
public class ReceiptType14Vo extends ReceiptVo {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4407099741664069582L;
	
	private String title;
	String date;			// 월간마감
	String parkName;		// 주차요원
	String empName;
	String manager;			// 파주시청
	String managerTel;		// 031-940-2768
	String empPhone;
	ArrayList<ParkIO> items;
	
	
	public ReceiptType14Vo(String title, String today, String parkName,
			String empName, String empPhone, String bIZ_NAME,
			String bIZ_TEL, ArrayList<ParkIO> parkIO) {
		// TODO Auto-generated constructor stub
		
		
		this.title = title;
		this.date = today;
		this.parkName = parkName;
		this.empName = empName;
		this.empPhone = empPhone;
		this.manager = bIZ_NAME;
		this.managerTel = bIZ_TEL;
		this.items = parkIO;
		
	}
	
	static final int ALIGN_CENTER = 0;
	static final int ALIGN_LEFT = 1;
	static final int ALIGN_RIGHT = 2;
	
	public static String getEmpty(int max, int align, String value) {
		StringBuffer sb = new StringBuffer();
		
		if (value == null) {
			value = "0";
		}
		int len = value.length();
		
		
		switch (align) {
		case ALIGN_CENTER:
			int left = (max - len) / 2;
			for (int i = 0; i < left; i++) {
				sb.append(" ");
			}
			sb.append(value);
			for (int i = (left + len); i < max; i++) {
				sb.append(" ");
			}
			break;
			
		case ALIGN_LEFT:
			sb.append(value);
			for (int i = len; i <= max; i++) {
				sb.append(" ");
			}
			break;
			
		case ALIGN_RIGHT:
			for (int i = 0; i < (max - len); i++) {
				sb.append(" ");
			}
			sb.append(value);
			break;
		}
		
		return sb.substring(0, max);
	}

	
	@Override
	public ByteArrayBuffer print() {
		int totalAmt = 0;
		int totalRealAmt = 0;

		byte[] lSize = getPrintOption(PRINT_TEXT_SIZE_LARGE);
		byte[] nSize = getPrintOption(PRINT_TEXT_SIZE_DEFAULT);
		
		ByteArrayBuffer bf = new ByteArrayBuffer(1024);
		bf.append(lSize,0,lSize.length);
		replaceByteDate(bf,"         [" + title + "]\n\n");//제일큰글자
		bf.append(nSize,0,nSize.length);
		replaceByteDate(bf,"출력일자 : "+ date+"\n");
		replaceByteDate(bf,"================================\n");
		replaceByteDate(bf,"주차장명 : "+parkName+"\n");
		replaceByteDate(bf,"주차요원 : "+empName+"\n");
		replaceByteDate(bf,"출차날짜 : "+date.substring(0, date.indexOf(" "))+"\n");
		replaceByteDate(bf,"--------------------------------\n");
		replaceByteDate(bf,"차량번호 |  출차시간  | 납부금액\n");
		replaceByteDate(bf,"--------------------------------\n");
			for (ParkIO i : items) {
				replaceByteDate(bf,getEmpty(10, ALIGN_LEFT, i.getCarNo())  
						+ getEmpty(11, ALIGN_CENTER, i.getParkOutDay().substring(8, 10) + ":"
						+ i.getParkOutDay().substring(10, 12) + "(" + i.getParkTime().trim()+ ")")
						+ getEmpty(7, ALIGN_RIGHT, Util.replaceMoney(i.getRealAmt()))
						+ "\n");
				
				totalAmt += Integer.parseInt(i.getParkAmt());
				totalRealAmt += Integer.parseInt(i.getRealAmt());
			}
		replaceByteDate(bf,"--------------------------------\n");
		replaceByteDate(bf,"총거래건수 : "+Util.replaceMoney(items.size()) + "건\n");
		//xx 2015.02.01 제외
		//replaceByteDate(bf,"총주차금액 : "+Util.replaceMoney(totalAmt) + "원\n");
		replaceByteDate(bf,"총납부금액 : "+Util.replaceMoney(totalRealAmt)  + "원\n");
		replaceByteDate(bf,"================================\n");
		replaceByteDate(bf,manager + "\n");

		replaceByteDate(bf,"********************************\n");
		replaceByteDate(bf,getOutPrint() + "\n");
		replaceByteDate(bf,"********************************\n");
		//xx 2015.02.01 제외
		//replaceByteDate(bf,"문의전화 : "+empPhone+"\n");
		replaceByteDate(bf,"\n\n");
		return bf;
	}
	
	

}



/**
 * 	bf.append(getEmpty(10, ALIGN_LEFT, i.getCarNumber()) + "|" 
						+ getEmpty(10, ALIGN_LEFT, i.getParkOutTime().substring(8, 10) + ":" + i.getParkOutTime().substring(10, 12) + "(" + i.getParkTime().trim()+ ")") + "|"
						+ getEmpty(8, ALIGN_RIGHT, Util.replaceMoney(i.getParkAmt())) + "|" 
						+ getEmpty(8, ALIGN_RIGHT, Util.replaceMoney(i.getRealAmt())) + "|" 
						+ getEmpty(4, ALIGN_CENTER, IntroActivity.getCommonCodeValue("PY", i.getParkType())) + "\n");
						*/
