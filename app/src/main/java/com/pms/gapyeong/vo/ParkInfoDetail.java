package com.pms.gapyeong.vo;

public class ParkInfoDetail {
	
	String codePark; 		// CD_PARK : 주차장코드
	String knPark;			// KN_PARK : 주차장명
	String codeArea;		// CD_AREA : 주차면수
	String inService;		// IN_SERVICE : 입차서비스(분)
	String outService;		// OUT_SERVICE : 출차서비스(분)
	String returnService;	// RETURN_SERVICE : 회차서비스(분)
	String baseSection;		// BASE_SECTION :기본구간(분)
	String baseAMT;			// BASE_AMT : 기본금액
	String repeatSection;	// REPEAT_SECTION :반복구간(분)
	String repeatAMT;		// REPEAT_AMT :반복금액 
	String maxAMT;			// MAX_AMT :일일최대금액
	String ticketAmt;
	String ticketPrint; //화면 마지막 설
	String holidayTime;
	String weekdayTime;
	String inPrint;
	String outPrint;
	public ParkInfoDetail(String codePark,
			String knPark, String codeArea,
			String inService, String outService,
			String returnService, String baseSection,
			String baseAMT, String repeatSection,
			String repeatAMT, String maxAMT,String ticketPrint,String holiday,String weekday,String tkAmt,String inprint,String outprint) {
		
		this.codePark = codePark;
		this.knPark = knPark;
		this.codeArea = codeArea;
		this.inService = inService;
		this.outService = outService;
		this.returnService = returnService;
		this.baseSection = baseSection;
		this.baseAMT = baseAMT;
		this.repeatSection = repeatSection;
		this.repeatAMT = repeatAMT;
		this.maxAMT = maxAMT;
		this.ticketPrint = ticketPrint;
		this.holidayTime = holiday;
		this.weekdayTime = weekday;
		this.ticketAmt = tkAmt;
		this.inPrint = inprint;
		this.outPrint = outprint;
		
	}
	public String getInPrint(){
		return inPrint;
	}
	
	public String getOutPrint() {
		return outPrint;
	}
	
	
	public String getTicketAmt(){
		return ticketAmt;
	}
	
	public String getHolidayTime() {
		return holidayTime;
	}

	public String getWeekdayTime() {
		return weekdayTime;
	}

	public String getTicketPrint() {
		return ticketPrint;
	}
	
	
	public String getCodePark() {
		return codePark;
	}
	public String getKnPark() {
		return knPark;
	}
	public String getCodeArea() {
		return codeArea;
	}
	public String getInService() {
		return inService;
	}
	public String getOutService() {
		return outService;
	}
	public String getReturnService() {
		return returnService;
	}
	public String getBaseSection() {
		return baseSection;
	}
	public String getBaseAMT() {
		return baseAMT;
	}
	public String getRepeatSection() {
		return repeatSection;
	}
	public String getRepeatAMT() {
		return repeatAMT;
	}
	public String getMaxAMT() {
		return maxAMT;
	}
}
