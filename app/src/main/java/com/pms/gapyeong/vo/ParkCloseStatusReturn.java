package com.pms.gapyeong.vo;

import java.io.Serializable;

import com.pms.gapyeong.common.Util;

public class ParkCloseStatusReturn implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3399003983367836075L;
	
	/**
	 * 미수금납부 차량 출력항목
	 */
	private String carNo;
	private String disCd;
	private String knDiscd;
	private String billType;
	private String knBillType;
	private String returnAmt;
	private String returnDate;
	
	public ParkCloseStatusReturn(String carNo, String disCd, String knDiscd
							   , String billType, String knBillType, String returnAmt, String returnDate) {
		this.carNo 	  	= Util.isNVL(carNo);
		this.disCd 	  	= Util.isNVL(disCd);
		this.knDiscd  	= Util.isNVL(knDiscd);
		this.billType 	= Util.isNVL(billType);
		this.knBillType = Util.isNVL(knBillType);
		this.returnAmt  = Util.addComma(Util.isNVL(returnAmt,"0"));
		this.returnDate = Util.isNVL(returnDate);
	}
	
	public String getCarNo() {
		return carNo;
	}

	public void setCarNo(String carNo) {
		this.carNo = carNo;
	}

	public String getDisCd() {
		return disCd;
	}

	public void setDisCd(String disCd) {
		this.disCd = disCd;
	}

	public String getKnDiscd() {
		return knDiscd;
	}

	public void setKnDiscd(String knDiscd) {
		this.knDiscd = knDiscd;
	}

	public String getBillType() {
		return billType;
	}

	public void setBillType(String billType) {
		this.billType = billType;
	}

	public String getKnBillType() {
		return knBillType;
	}

	public void setKnBillType(String knBillType) {
		this.knBillType = knBillType;
	}

	public String getReturnAmt() {
		return returnAmt;
	}

	public void setReturnAmt(String returnAmt) {
		this.returnAmt = returnAmt;
	}

	public String getReturnDate() {
		return returnDate;
	}

	public void setReturnDate(String returnDate) {
		this.returnDate = returnDate;
	}	
	
}
