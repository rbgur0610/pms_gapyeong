package com.pms.gapyeong.vo;

import com.pms.gapyeong.common.Constants;
import com.pms.gapyeong.common.Util;

import org.apache.http.util.ByteArrayBuffer;

/**
 * 입차 영수증
 */
public class ReceiptType13Vo extends ReceiptVo {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8449913322993640160L;
	
	private String title;			// 타이틀
	private String date;			// 출력일자
	private String codeArea;		// 주차면
	private String parkType;		// 주차종류
	private String amtType;			// 요금종류
	private String disTypeName;		// 요금할인
	private String totalUnpay;		// 총미수금액
	private String prepayAmt;		// 선납금액
	private String inTime;			// 입차시간
	private String outTime;			// 출차예정시간
	private String totalMin;		// 총 분
	private String managerNum;
	private String manager;
	private String managerTel;
	private String approvalNo = "";
	private String cardCompany="";
	private String cardNo="";
	private String PrintText="";
	private String CashReceipt = "";
	private boolean printBill = false;
	private String empTicket_print = "";
	private String workTime = "";
	String couponAmt="";
	
	
	public ReceiptType13Vo(String title,	// 타이틀
					String date,			// 출력일자
					String codeArea,		// 주차면
					String parkType,		// 주차종류
					String amtType,			// 요금종류
					String disTypeName,		// 요금할인
					String totalUnpay,		// 총미수금액
					String prepayAmt,		// 선납금액
					String inTime,			// 입차시간
					String outTime,			// 출차예정시간
					String totalMin,		// 총 분
					String managerNum,
					String manager,
					String managerTel, String ticket_print, String _workTime) {
		this.title = title;
		this.date = date;			
		this.codeArea = codeArea;
		this.parkType = parkType;
		this.amtType = amtType;
		this.disTypeName = disTypeName;
		this.totalUnpay = totalUnpay;
		this.prepayAmt = prepayAmt;
		this.inTime = inTime;
		this.outTime = outTime;
		this.totalMin = totalMin;
		this.managerNum = managerNum;
		this.manager    = manager;
		this.managerTel = managerTel;
		this.empTicket_print = ticket_print;
		this.workTime = _workTime;
	}

	
	
	@Override
	public ByteArrayBuffer print() {
		byte[] xxSize = getPrintOption(PRINT_TEXT_SIZE_MAX);
		byte[] lSize = getPrintOption(PRINT_TEXT_SIZE_LARGE);
		byte[] nSize = getPrintOption(PRINT_TEXT_SIZE_DEFAULT);
		
		
		ByteArrayBuffer bf = new ByteArrayBuffer(1024);
		bf.append(lSize,0,lSize.length);
		replaceByteDate(bf,"         [" + title + "]\n");//제일큰글자
		bf.append(nSize,0,nSize.length);
		//replaceByteDate(bf,"출력일자 : "+ date+"\n");
		replaceByteDate(bf,"--------------------------------\n");
		replaceByteDate(bf,empTicket_print +"\n");
		replaceByteDate(bf,"--------------------------------\n");
		bf.append(lSize,0,lSize.length);
		replaceByteDate(bf,"차량번호 : " + this.getCarNum() +"\n");
		bf.append(xxSize,0,xxSize.length);
		replaceByteDate(bf,"주차번호: " + codeArea+"\n");
		//replaceByteDate(bf,"주차장명 : " + this.getParkName() +"\n");		
		bf.append(nSize,0,nSize.length);
		//replaceByteDate(bf,"할인종류 : " + disTypeName+"\n");
		replaceByteDate(bf,"입차시간 : "+ Util.dateString(inTime)+"\n"); 
		bf.append(nSize,0,nSize.length);
		replaceByteDate(bf,"********************************\n");
		bf.append(lSize,0,lSize.length);
		replaceByteDate(bf,"담당자 :" + this.getEmpName()+" ☎"+this.getEmpTel()+"\n"); 
		bf.append(nSize,0,nSize.length);
		replaceByteDate(bf,"********************************\n");
		replaceByteDate(bf,"부재시 위 번호로 연락바랍니다."+"\n"); 
		replaceByteDate(bf,"--------------------------------\n");
		if(!Util.isEmpty(getEmpBusiness_Tel())){
			replaceByteDate(bf,getEmpBusiness_Tel()+"\n");
			replaceByteDate(bf,"--------------------------------\n");
		}

		if(totalUnpay.length()>0){
			bf.append(lSize,0,lSize.length);
			replaceByteDate(bf,"미수금액 : " + Util.replaceMoney(totalUnpay)+"원\n");
			replaceByteDate(bf,"--------------------------------\n");
		}

		bf.append(lSize,0,lSize.length);
		replaceByteDate(bf,"주차장명 : " + this.getParkName() +"\n");		
		bf.append(nSize,0,nSize.length);
		replaceByteDate(bf,"할인종류 : " + disTypeName+"\n");
		replaceByteDate(bf,"--------------------------------\n");
		replaceByteDate(bf,"운영시간 : " + workTime+"\n");
		
		if(!outTime.equals(""))
			if("출차영수증".equals(title))
			  replaceByteDate(bf,"출차시간 : " + Util.dateString(outTime) + "\n");
			else
			  replaceByteDate(bf,"출차예정시간 : " + Util.dateString(outTime) + "\n");
		
		if(!totalMin.equals("")){
			replaceByteDate(bf,"주차시간 : " + totalMin+"\n");
		}
		//bf.append(lSize,0,lSize.length);
		//replaceByteDate(bf,"미수금액 : " + Util.replaceMoney(totalUnpay)+"원\n");
		if ("입차취소영수증".equals(title)) {
			replaceByteDate(bf,"취소금액 : " + Util.replaceMoney(prepayAmt)+"원\n");
		} else {
			
			if("시간권(선불)".equals(parkType) || "04".equals(parkType) || "일일권".equals(parkType) || "02".equals(parkType)) {
				replaceByteDate(bf,"선납금액 : " + Util.replaceMoney(prepayAmt)+"원\n");
			}
		}
		
		bf.append(nSize,0,nSize.length);

		try {
			if(!printBill){
				replaceByteDate(bf,"--------------------------------\n");
				if(!approvalNo.equals(""))
				{
				   replaceByteDate(bf,"결제방법 : 카드결제\n");
				   replaceByteDate(bf,"--------------------------------\n");
				}else{
				   replaceByteDate(bf,"결제방법 : 현금결제\n");
				}
			}
			if(!cardNo.equals("")){
				replaceByteDate(bf,"카드번호 :" + Util.cardnumString(cardNo) + "\n");
			}
			if(!cardCompany.equals(""))
				replaceByteDate(bf,"카드사 명 : "+ cardCompany + "\n");
			if(!approvalNo.equals("")){
				replaceByteDate(bf,"승인번호 : " + approvalNo + "\n");
			}
		} catch (NullPointerException e) {
			if(Constants.DEBUG_PRINT_LOG){
				e.printStackTrace();
			}else{
				System.out.println("예외 발생");
			}
		}

		if(!"".equals(CashReceipt)&&CashReceipt!=null&&!"null".equals(CashReceipt)){
			replaceByteDate(bf,"--------------------------------\n");
			replaceByteDate(bf,"현금영수증 발행\n");
			replaceByteDate(bf,"승인번호:"+CashReceipt + "\n");
		}
		
		
		if (PrintText != null && !PrintText.equals("")) {
			replaceByteDate(bf,"--------------------------------\n");
			replaceByteDate(bf,PrintText + "\n");
			replaceByteDate(bf,"--------------------------------\n");
		}
		bf.append(lSize,0,lSize.length);
		replaceByteDate(bf,this.getKN_BANK() + " " + this.getAccount_No() + " " + this.getKN_ACCOUNT() + " " + this.getBank_MSG() + "\n");
		bf.append(nSize,0,nSize.length);
		replaceByteDate(bf,"--------------------------------\n");
		replaceByteDate(bf,"사업자번호 : "+managerNum + "\n");
		bf.append(nSize,0,lSize.length);
		
		//replaceByteDate(bf,"주차요원 : "+this.getEmpName()+"\n");
		//if(!Util.isEmpty(this.getEmpPhone())){
		//	bf.append(lSize,0,lSize.length);
		//	replaceByteDate(bf,"연 락 처 : "+this.getEmpPhone()+"\n");
		//}
		bf.append(nSize,0,nSize.length);
		
		if(!Util.isEmpty(this.getEmpTel())){
			replaceByteDate(bf,"문의  전화 : "+this.getEmpPhone()+"\n");
		}

		replaceByteDate(bf,"--------------------------------\n");

		replaceByteDate(bf,"********************************\n");
		replaceByteDate(bf,getOutPrint() + "\n");
		replaceByteDate(bf,"********************************\n");
		//bf.append(lSize,0,lSize.length);
		//replaceByteDate(bf,managerTel + "\n");
		//replaceByteDate(bf,"\n\n");
		return bf;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getParkType() {
		return parkType;
	}

	public void setParkType(String parkType) {
		this.parkType = parkType;
	}

	public String getAmtType() {
		return amtType;
	}

	public void setAmtType(String amtType) {
		this.amtType = amtType;
	}

	public String getDisTypeName() {
		return disTypeName;
	}

	public void setDisTypeName(String disTypeName) {
		this.disTypeName = disTypeName;
	}

	public String getTotalUnpay() {
		return totalUnpay;
	}

	public void setTotalUnpay(String totalUnpay) {
		this.totalUnpay = totalUnpay;
	}

	public String getPrepayAmt() {
		return prepayAmt;
	}

	public void setPrepayAmt(String prepayAmt) {
		this.prepayAmt = prepayAmt;
	}

	public String getInTime() {
		return inTime;
	}

	public void setInTime(String inTime) {
		this.inTime = inTime;
	}

	public String getOutTime() {
		return outTime;
	}

	public void setOutTime(String outTime) {
		this.outTime = outTime;
	}

	public String getTotalMin() {
		return totalMin;
	}

	public void setTotalMin(String totalMin) {
		this.totalMin = totalMin;
	}
	
	public String getManagerNum() {
		return managerNum;
	}

	public void setManagerNum(String managerNum) {
		this.managerNum = managerNum;
	}

	public String getManager() {
		return manager;
	}

	public void setManager(String manager) {
		this.manager = manager;
	}

	public String getManagerTel() {
		return managerTel;
	}

	public void setManagerTel(String managerTel) {
		this.managerTel = managerTel;
	}

	public String getApprovalNo() {
		return approvalNo;
	}

	public void setApprovalNo(String approvalNo) {
		this.approvalNo = approvalNo;
	}

	public String getCardCompany() {
		return cardCompany;
	}

	public void setCardCompany(String cardCompany) {
		this.cardCompany = cardCompany;
	}

	public String getCardNo() {
		return cardNo;
	}

	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}

	public String getPrintText() {
		return PrintText;
	}

	public void setPrintText(String printText) {
		PrintText = printText;
	}

	public String getCashReceipt() {
		return CashReceipt;
	}

	public void setCashReceipt(String cashReceipt) {
		CashReceipt = cashReceipt;
	}

	public boolean isPrintBill() {
		return printBill;
	}

	public void setPrintBill(boolean printBill) {
		this.printBill = printBill;
	}
	
	public void setCouponAmt(String val) {
		this.couponAmt = val;
	}
	
}
