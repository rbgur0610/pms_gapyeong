package com.pms.gapyeong.vo;

import java.io.Serializable;

import com.pms.gapyeong.pay.PayVo;

public class ExitPaymentData extends PayVo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8666045899081463345L;
	
	private String couponAmt = "0";
	private String returnAmt = "0";
	
	public String getCouponAmt() {
		return couponAmt;
	}
	
	public void setCouponAmt(String couponAmt) {
		this.couponAmt = couponAmt;
	}
	
	public String getReturnAmt() {
		return returnAmt;
	}
	
	public void setReturnAmt(String returnAmt) {
		this.returnAmt = returnAmt;
	}
	
}
