package com.pms.gapyeong.vo;

import java.io.Serializable;

import com.pms.gapyeong.common.Util;

public class ParkCloseStatusTicket implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1178919151089603448L;
	
	/**
	 * 정기권 차량 출력항목
	 */
	private String carNo;
	private String payAmt;
	private String ticketDate;

	public ParkCloseStatusTicket(String carNo, String payAmt, String ticketDate) {
		this.carNo 	  	= Util.isNVL(carNo);
		this.payAmt  	= Util.addComma(Util.isNVL(payAmt,"0"));
		this.ticketDate = Util.isNVL(ticketDate);
	}
	
	public String getCarNo() {
		return carNo;
	}

	public void setCarNo(String carNo) {
		this.carNo = carNo;
	}
	
	public String getPayAmt() {
		return payAmt;
	}

	public void setPayAmt(String payAmt) {
		this.payAmt = payAmt;
	}

	public String getTicketDate() {
		return ticketDate;
	}

	public void setTicketDate(String ticketDate) {
		this.ticketDate = ticketDate;
	}	
	
}
