package com.pms.gapyeong.vo;

import java.io.Serializable;

import com.pms.gapyeong.common.Util;

public class ParkCloseStatusYet implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6381100251158138394L;
	
	/**
	 * 미납발생 차량 출력항목
	 */
	private String carNo;
	private String disCd;
	private String knDiscd;
	private String parkTime;
	private String yetAmt;
	
	public ParkCloseStatusYet(String carNo, String disCd, String knDiscd, String parkTime, String yetAmt) {
		this.carNo 	  = Util.isNVL(carNo);
		this.disCd 	  = Util.isNVL(disCd);
		this.knDiscd  = Util.isNVL(knDiscd);
		this.parkTime = Util.isNVL(parkTime);
		this.yetAmt   = Util.addComma(Util.isNVL(yetAmt,"0"));
	}
	
	public String getCarNo() {
		return carNo;
	}

	public void setCarNo(String carNo) {
		this.carNo = carNo;
	}

	public String getDisCd() {
		return disCd;
	}

	public void setDisCd(String disCd) {
		this.disCd = disCd;
	}

	public String getKnDiscd() {
		return knDiscd;
	}

	public void setKnDiscd(String knDiscd) {
		this.knDiscd = knDiscd;
	}

	public String getParkTime() {
		return parkTime;
	}

	public void setParkTime(String parkTime) {
		this.parkTime = parkTime;
	}

	public String getYetAmt() {
		return yetAmt;
	}

	public void setYetAmt(String yetAmt) {
		this.yetAmt = yetAmt;
	}	
	
}
