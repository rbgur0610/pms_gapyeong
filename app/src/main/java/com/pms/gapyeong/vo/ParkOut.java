package com.pms.gapyeong.vo;

import java.io.Serializable;

public class ParkOut implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7490377202824216612L;

//	PIO_NUM : 주차번호
	String pioNumber;
	
//	CD_PARK: 주차장번호
	String parkCode;
	
	//	EMP_CD : 근무자코드
	String empCode;
	
//	WORK_DAY: 날짜
	String workDay;
	
//	OUT_TIME : 출차시간(8)
	String outTime;
	
//	CAR_NO : 차량번호
	String carNumber;
	
//	PARK_TYPE : 주차유형
	String parkType;
	
//	CAR_OWNER : 소유주(변경가능)
	String carOwner;

	//	TEL : 연락처(변경가능)
	String tel;
	
//	STATE_CD : O1 (입차)(2013.12.09)
	String stateCode;
	
//	YET_CD : P3-1,P3-2,P3-4 (미수상세코드)
	String yetCode;
	
//	ADV_AMT : 선불금
	String advAmt;
	
//	BILL_TYPE : 결재방법
	String billType;
	
//	DIC_CODE : 할인코드
	String dicCode;
	
//	REAL_AMT : 납부금액
	String realAmt;
	
//	CARD_NO: 카드번호
	String cardNo;
	
//	CARD_COMPANY: 카드사명
	String cardCompany;
	
//	TRANSACTION_NO : 거래번호
	String transactionNo;
	
//	APPROVAL_NO : 승인번호
	String approvalNo;
	
//	APPROVAL_DATE : 승인일시 
	String approvalDate;
	
//	CATNUMBER :12자리
	String catnumber;
	
//	RETURN_AMT : 환불금액
	String returnAmt;
	
//	DIC_AMT : 할인금액
	String dicAmt;
	
//	CASH_AMT :현금
	String cashAmt;
	
//	CARD_AMT : 카드금액
	String cardAmt;
	
//	YET_AMT :미납금액
	String yetAmt;
	
//	TRUNC_AMT:절삭금액(2013.12.06)
	String truncAmt;
	
//	YET_RETURN:미환불금액(2014.01.10)
	String yetReturn;
	
//	YET_RETURNCARD:미환불카드금액(2014.02.07)
	String yetReturnCard;
	
	String comment ; 

//	COUPON_NO : 쿠폰번호
	String couponNo;
	
//	COUPON_TYPE : 쿠폰타입
	String couponType;
	
//	COUPON_QTY : 쿠폰갯수
	String couponQty;
	
//	COUOIN_AMT :쿠폰금액
	String couponAmt ; 
	
//	COUPON_DATA :쿠폰데이터	
	String couponData;
	
	// 사진 삭제 여부 IS_PIC_DEL
	boolean isDeletePicture;

	String chargeTypeStr;

	public ParkOut(String pioNumber, 
			String parkCode, 
			String empCode,
			String workDay,
			String outTime,
			String carNumber,
			String parkType,
			String tel,
			String stateCode,
			String advAmt,
			String billType,
			String dicCode,
			String realAmt,
			String cardNo,
			String approvalNo,
			String catnumber,
			String returnAmt,
			String dicAmt,
			String cashAmt,
			String cardAmt,
			String yetAmt,
			String truncAmt,
			String yetReturn, 
			String yetReturnCard,
			String couponNo,
			String couponData, 
			boolean isDelete) {
		this.pioNumber = pioNumber;
		this.parkCode = parkCode; 
		this.empCode = empCode;
		this.workDay = workDay;
		this.outTime = outTime;
		this.carNumber = carNumber;
		this.parkType = parkType;
		this.tel = tel;
		this.stateCode = stateCode; 
		this.advAmt = advAmt;
		this.billType = billType;
		this.dicCode = dicCode;
		this.realAmt = realAmt;
		this.cardNo = cardNo;
		this.approvalNo = approvalNo;
		this.couponNo = couponNo;
		this.catnumber = catnumber;
		this.couponData = couponData;
		this.returnAmt = returnAmt;
		this.dicAmt = dicAmt;
		this.cashAmt = cashAmt;
		this.cardAmt = cardAmt;
		this.yetAmt = yetAmt;
		this.truncAmt = truncAmt;
		this.yetReturn = yetReturn;
		this.yetReturnCard = yetReturnCard;
		this.isDeletePicture = isDelete;
	}
    
	public String getPioNumber() {
		return pioNumber;
	}

	public void setPioNumber(String pioNumber) {
		this.pioNumber = pioNumber;
	}

	public String getParkCode() {
		return parkCode;
	}
	
	public void setParkCode(String parkCode) {
		this.parkCode = parkCode;
	}
	
	public String getEmpCode() {
		return empCode;
	}
	
	public void setEmpCode(String empCode) {
		this.empCode = empCode;
	}
	
	public String getWorkDay() {
		return workDay;
	}
	
	public void setWorkDay(String workDay) {
		this.workDay = workDay;
	}	
	
	public String getOutTime() {
		return outTime;
	}

	public void setOutTime(String outTime) {
		this.outTime = outTime;
	}
	
	public String getCarNumber() {
		return carNumber;
	}

	public void setCarNumber(String carNumber) {
		this.carNumber = carNumber;
	}
	
	public String getParkType() {
		return parkType;
	}
	
	public void setParkType(String parkType) {
		this.parkType = parkType;
	}
	
	public String getTel() {
		return tel;
	}
	
	public void setTel(String tel) {
		this.tel = tel;
	}
	
	public String getCarOwner() {
		return carOwner;
	}

	public void setCarOwner(String carOwner) {
		this.carOwner = carOwner;
	}
	
	public String getStateCode() {
		return stateCode;
	}

	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}
	
	public String getBillType() {
		return billType;
	}
	
	public void setBillType(String billType) {
		this.billType = billType;
	}
	
	public String getDicCode() {
		return dicCode;
	}
	
	public void setDicCode(String dicCode) {
		this.dicCode = dicCode;
	}
	
	public String getYetCode() {
		return yetCode;
	}
	
	public void setYetCode(String yetCode) {
		this.yetCode = yetCode;
	}

	public String getRealAmt() {
		return realAmt;
	}

	public void setRealAmt(String realAmt) {
		this.realAmt = realAmt;
	}
	
	
	public String getCashAmt() {
		return cashAmt;
	}	

	public void setCashAmt(String cashAmt) {
		this.cashAmt = cashAmt;
	}

	public String getCardAmt() {
		return cardAmt;
	}
	
	public void setCardAmt(String cardAmt) {
		this.cardAmt = cardAmt;
	}

	public String getAdvAmt() {
		return advAmt;
	}
	
	public void setAdvAmt(String advAmt) {
		this.advAmt = advAmt;
	}
	
	public String getYetAmt() {
		return yetAmt;
	}
	
	public void setYetAmt(String yetAmt) {
		this.yetAmt = yetAmt;
	}
	
	public String getDicAmt() {
		return dicAmt;
	}
	
	public void setDicAmt(String dicAmt) {
		this.dicAmt = dicAmt;
	}	

	public String getReturnAmt() {
		return returnAmt;
	}
	
	public void setReturnAmt(String returnAmt) {
		this.returnAmt = returnAmt;
	}
	
	public String getTruncAmt() {
		return truncAmt;
	}
	
	public void setTruncAmt(String truncAmt) {
		this.truncAmt = truncAmt;
	}	
	
	public String getYetReturn() {
		return yetReturn;
	}

	public void setYetReturn(String yetReturn) {
		this.yetReturn = yetReturn;
	}
	
	public String getYetReturnCard() {
		return yetReturnCard;
	}
	
	public void setYetReturnCard(String yetReturnCard) {
		this.yetReturnCard = yetReturnCard;
	}
	
	public String getCatnumber() {
		return catnumber;
	}

	public void setCatnumber(String catnumber) {
		this.catnumber = catnumber;
	}
	
	public String getCardNo() {
		return cardNo;
	}

	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}

	public String getCardCompany() {
		return cardCompany;
	}

	public void setCardCompany(String cardCompany) {
		this.cardCompany = cardCompany;
	}

	public String getTransactionNo() {
		return transactionNo;
	}

	public void setTransactionNo(String transactionNo) {
		this.transactionNo = transactionNo;
	}	
	
	public String getApprovalNo() {
		return approvalNo;
	}

	public void setApprovalNo(String approvalNo) {
		this.approvalNo = approvalNo;
	}
	
	public String getApprovalDate() {
		return approvalDate;
	}

	public void setApprovalDate(String approvalDate) {
		this.approvalDate = approvalDate;
	}	
	
	public String getCouponNo() {
		return couponNo;
	}

	public void setCouponNo(String couponNo) {
		this.couponNo = couponNo;
	}	
	
	public String getCouponType() {
		return couponType;
	}

	public void setCouponType(String couponType) {
		this.couponType = couponType;
	}
	
	public String getCouponQty() {
		return couponQty;
	}
	
	public void setCouponQty(String couponQty) {
		this.couponQty = couponQty;
	}

	public String getCouponData() {
		return couponData;
	}

	public void setCouponData(String couponData) {
		this.couponData = couponData;
	}
	
	public String getCouponAmt() {
		if(couponAmt==null)
			return "";
		return couponAmt;
	}
	
	public void setCouponAmt(String val) {
		this.couponAmt = val.replace("", "").replace("원","");
	}

	public String getComment() {
		if(comment==null)
			return "";
		return comment;
	}
	
	public void setComment(String cmm) {
		this.comment = cmm;
	}
	
	public String getChargeTypeStr() {
		return chargeTypeStr;
	}

	public void setChargeTypeStr(String chargeTypeStr) {
		this.chargeTypeStr = chargeTypeStr;
	}
	
	public boolean isDeletePicture() {
		return isDeletePicture;
	}
	
	public void setDeletePicture(boolean isDeletePicture) {
		this.isDeletePicture = isDeletePicture;
	}
	
}
