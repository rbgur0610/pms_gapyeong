package com.pms.gapyeong.vo;

import java.io.Serializable;

import com.pms.gapyeong.common.Util;

public class ParkCloseStatusDiscountList implements Serializable {
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 3113895007960159216L;
	/**
	 * 무료 할인 차량 출력항목
	 */
	private String DIS_CD;
	private String KN_DIS_CD;
	private String PARK_INCOME;
	private String PARK_AMT;
	private String CNT;

	public ParkCloseStatusDiscountList(String _dis, String _kn_dis, String _income, String _amt, String _cnt) {
		this.DIS_CD = _dis;
		this.KN_DIS_CD 	  	= Util.isNVL(_kn_dis);
		this.PARK_INCOME  	= _income;
		this.PARK_AMT = _amt;
		this.CNT = _cnt;
		
	}
	
	public String getDiscd() {
		return DIS_CD;
	}

	
	public String getKN_DIS_CD() {
		return KN_DIS_CD;
	}

	public String getPARK_INCOME() {
		return PARK_INCOME;
	}


	public String getPARK_AMT() {
		return PARK_AMT;
	}

	public String getCNT() {
		return CNT;
	}
	
	
	
}
