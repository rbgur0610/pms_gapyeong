package com.pms.gapyeong.vo;

import org.apache.http.util.ByteArrayBuffer;

import com.pms.gapyeong.common.Constants;
import com.pms.gapyeong.common.Util;

import android.util.Log;

/**
 * 미수금확인증 영수증
 */
public class ReceiptType5Vo extends ReceiptVo {

    /**
     *
     */
    private static final long serialVersionUID = -3771580210828903027L;

    private String title;
    private String today;
    private String chargeType;
    private String disType;
    private String totalUnpay;
    private String manager;

    private String managerBankMsg;
    private String managerBank;
    private String managerAcctno;
    private String managerAcctname;
    private String managerCopyRight;

    private String pName;
    private String pIn;
    private String pOut;
    private String pNum;
    private String pGroup = "1000";

    public ReceiptType5Vo(String title, String today,
                          String chargeType, String disType,
                          String totalUnpay, String manager, String managerBankMsg, String managerBank, String managerAcctno, String managerAcctname, String managerCopyRight,
                          String pNum,
                          String pName, String pIn, String pOut, String gc) {
        // TODO Auto-generated constructor stub

        this.title = title;
        this.today = today;
        this.chargeType = chargeType;
        this.disType = disType;
        this.totalUnpay = totalUnpay;

        this.manager = manager;
        this.managerBankMsg = managerBankMsg;
        this.managerBank = managerBank;
        this.managerAcctno = managerAcctno;
        this.managerAcctname = managerAcctname;
        this.managerCopyRight = managerCopyRight;
        this.pNum = pNum;
        this.pName = pName;
        this.pIn = setChangeDate(pIn);
        this.pOut = setChangeDate(pOut);
        this.pGroup = gc;

    }


    public void setGroupCode(String gc) {
        this.pGroup = gc;
    }

    private String setChangeDate(String date) {

        String cDate = date;
        Log.e("cDate", cDate);
        try {
            cDate =
                    date.substring(0, 8) + " "
                            + date.substring(date.length() - 6, date.length() - 4) + ":"
                            + date.substring(date.length() - 4, date.length() - 2) + ":"
                            + date.substring(date.length() - 2, date.length());
        } catch (NullPointerException e) {
            System.out.println("NullPointerException 예외 발생");
        } catch (IndexOutOfBoundsException e) {
            System.out.println("IndexOutOfBoundsException 예외 발생");
        }
        Log.e("cDate", cDate);
        return cDate;
    }


    @Override
    public ByteArrayBuffer print() {

        byte[] lSize = getPrintOption(PRINT_TEXT_SIZE_LARGE);
        byte[] nSize = getPrintOption(PRINT_TEXT_SIZE_DEFAULT);


        ByteArrayBuffer bf = new ByteArrayBuffer(1024);
        bf.append(lSize, 0, lSize.length);
        replaceByteDate(bf, "         [" + title + "]\n");//제일큰글자
        bf.append(nSize, 0, nSize.length);
        //replaceByteDate(bf,"출력일자 : "+ today + "\n");
        replaceByteDate(bf, "================================\n");
        bf.append(lSize, 0, lSize.length);
        replaceByteDate(bf, "차량번호 : " + this.getCarNum() + "\n");//큰글자
        replaceByteDate(bf, "주차장명 : " + pName + "\n");
        bf.append(nSize, 0, nSize.length);
        replaceByteDate(bf, "입차시간 : " + Util.dateString(pIn) + "\n");
        replaceByteDate(bf, "출차시간 : " + Util.dateString(pOut) + "\n");
        replaceByteDate(bf, "--------------------------------\n");
        //replaceByteDate(bf,"요금종류 : "+chargeType+"\n");
        replaceByteDate(bf, "할인종류 : " + disType + "\n");
        bf.append(lSize, 0, lSize.length);
        replaceByteDate(bf, "미수금액 : " + Util.replaceMoney(totalUnpay) + "원\n");
        bf.append(lSize, 0, nSize.length);

        bf.append(nSize, 0, nSize.length);
        replaceByteDate(bf, "--------------------------------\n");
        bf.append(lSize, 0, lSize.length);
        if (this.getKN_ACCOUNT() == null || "".equals(this.getKN_ACCOUNT())) {
            replaceByteDate(bf, this.getKN_BANK() + " " + this.getAccount_No() + "\n");
            replaceByteDate(bf, this.getBank_MSG() + "\n");
        } else {
            replaceByteDate(bf, this.getKN_BANK() + " " + this.getAccount_No() + "\n");
            replaceByteDate(bf, this.getKN_ACCOUNT() + "\n");
            replaceByteDate(bf, this.getBank_MSG() + "\n");
        }


        bf.append(nSize, 0, nSize.length);
        replaceByteDate(bf, "--------------------------------\n");
        replaceByteDate(bf, "사업자번호 : " + pNum + "\n");
        bf.append(nSize, 0, lSize.length);
        replaceByteDate(bf, "주차요원 :" + this.getEmpName() + "\n");
        bf.append(nSize, 0, nSize.length);
        if (!Util.isEmpty(this.getEmpPhone()) && !"usim is not".equals(this.getEmpPhone().toLowerCase())) {
            bf.append(lSize, 0, lSize.length);
            replaceByteDate(bf, "연 락 처 : " + this.getEmpTel() + "\n");
        }
        bf.append(nSize, 0, nSize.length);
        if (!Util.isEmpty(this.getEmpTel())) {
            replaceByteDate(bf, "문의전화 : " + this.getEmpPhone() + "\n");
        }
        replaceByteDate(bf, "--------------------------------\n");
        if (!Util.isEmpty(getEmpBusiness_Tel())) {
            replaceByteDate(bf, getEmpBusiness_Tel() + "\n");
            replaceByteDate(bf, "--------------------------------\n");
        }


        replaceByteDate(bf, "********************************\n");
        replaceByteDate(bf, getOutPrint() + "\n");
        replaceByteDate(bf, "********************************\n");
        //if(!Util.isEmpty(managerCopyRight)){
        //	  bf.append(lSize,0,lSize.length);
        //	  replaceByteDate(bf,managerCopyRight+"\n");
        // }
        //	replaceByteDate(bf,"\n\n");
        return bf;
    }

}
