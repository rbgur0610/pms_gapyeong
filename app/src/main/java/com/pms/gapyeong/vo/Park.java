package com.pms.gapyeong.vo;

import java.io.Serializable;

import com.pms.gapyeong.common.Util;

public class Park implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2715361865964071418L;
	
	private String codePark;		// 주차장 코드
	private String knPark;			// 주차장 명
	private String empCode;			// 근무자 코드
	private String codeArea;		// 총 주차면수
	private String codeState;		// 주차장 급지
	private String startTime;		// 근무 시작시간
	private String endTime;			// 종료 시간
	private String serviceStartTime;	// 무료 주차 시작 시간
	private String serviceEndTime;		// 무료 주차 종료시간
	private String inQty;			// 현재 입차 차량 수
	private String availableQty;	// 사용 가능한 주차면 수 
	private String sumInQty;		// 입차 누계
	private String sumOutQty;		// 출차 누계
	private String parkName;		// 주차장 이름
	private String ticketAmt;
	private String catNumber;
	private String bizName;
	
	private String bizBankMsg;
	private String bizAccountNo;
	private String bizBank;
	private String bizAccountName;
	private String bizCopyRight;

	private String bizTel;
	private String bizNo;
	private String inPrint;
	private String outPrint;
	private String ticketPrint;
	private String dayAmt;
	private String maxAmt;
	
	String inService;		// 입차 서비스(분)
	String outService;		// 출차 서비스(분)
	String returnService;
	String baseSection;
	String baseAmt;
	String repeatSection;
	String repeatAmt;
	String truncMethod;
	String truncUnit;
	String group_cd;
	String advBoundTime;
	
	public Park(String codePark,
					String knPark,
					String empCode,			
					String codeArea,		
					String codeState,		
					String startTime,		
					String endTime,			
					String serviceStartTime,		
					String serviceEndTime,			
					String inQty,			
					String availableQty,	 
					String sumInQty,		
					String sumOutQty,
					String parkName,
					String ticketAmt,
					String catNumber,
					String bizName,
					String bizBankMsg,
					String bizAccountNo,
					String bizBank,
					String bizAccountName,
					String bizCopyRight,
					String bizTel,
					String bizNo,
					String inPrint,
					String outPrint,
					String ticketPrint,
					String dayAmt,
					String maxAmt,
					String inService,
					String outService,		
					String returnService,
					String baseSection,
					String baseAmt,
					String repeatSection,
					String repeatAmt,
					String truncMethod,
					String truncUnit,
					String advBoundTime,
					String group_cd
					) {		
		this.codePark = codePark;
		this.knPark = knPark;
		this.empCode = empCode;
		this.codeArea = codeArea;
		this.codeState = codeState;
		this.startTime = startTime;
		this.endTime   = endTime;
		this.serviceStartTime = serviceStartTime;
		this.serviceEndTime   = serviceEndTime;
		this.inQty = inQty;
		this.availableQty = availableQty;
		this.sumInQty = sumInQty;
		this.sumOutQty = sumOutQty;
		this.parkName = parkName;
		this.setTicketAmt(ticketAmt);
		this.setCatNumber(catNumber);
		this.bizName =bizName;
		
		this.bizBankMsg 	= bizBankMsg;
		this.bizAccountNo   = bizAccountNo;
		this.bizBank 	    = bizBank;
		this.bizAccountName = bizAccountName;
		this.bizCopyRight   = bizCopyRight;
		
		this.bizTel=bizTel;
		this.bizNo=bizNo;
		
		this.inPrint =inPrint;
		this.outPrint=outPrint;
		this.ticketPrint=ticketPrint;
		this.dayAmt = dayAmt;
		this.maxAmt = maxAmt;
		
		this.inService  = inService;
		this.outService = outService;		
		this.returnService = returnService;
		this.baseSection = baseSection;
		this.baseAmt = baseAmt;
		this.repeatSection = repeatSection;
		this.repeatAmt = repeatAmt;
		
		this.truncMethod = truncMethod;
		this.truncUnit = truncUnit;
		
		this.advBoundTime = advBoundTime;
		this.group_cd = group_cd;
		
	}
	

	public String getGroup_cd() {
		return this.group_cd;
	}
	
	public String getParkName() {
		return parkName;
	}
	
	public void setParkName(String parkName) {
		this.parkName = parkName;
	}
	 
	public String getCodePark() {
		return codePark;
	}

	public void setCodePark(String codePark) {
		this.codePark = codePark;
	}
	
	public String getKnPark() {
		return knPark;
	}

	public void setKnPark(String knPark) {
		this.knPark = knPark;
	}
	
	public String getEmpCode() {
		return empCode;
	}

	public void setEmpCode(String empCode) {
		this.empCode = empCode;
	}
	
	public String getCodeArea() {
		return codeArea;
	}	

	public void setCodeArea(String codeArea) {
		this.codeArea = codeArea;
	}

	public String getCodeState() {
		return codeState;
	}

	public void setCodeState(String codeState) {
		this.codeState = codeState;
	}
	
	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	
	public String getEndTime() {
		return endTime;
	}	

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getServiceStartTime() {
		return serviceStartTime;
	}

	public void setServiceStartTime(String serviceStartTime) {
		this.serviceStartTime = serviceStartTime;
	}

	public String getServiceEndTime() {
		return serviceEndTime;
	}

	public void setServiceEndTime(String serviceEndTime) {
		this.serviceEndTime = serviceEndTime;
	}	
	
	public String getInService() {
		return inService;
	}

	public void setInService(String inService) {
		this.inService = inService;
	}
	
	public String getOutService() {
		return outService;
	}
	
	public void setOutService(String outService) {
		this.outService = outService;
	}		
	
	public String getReturnService() {
		return returnService;
	}

	public void setReturnService(String returnService) {
		this.returnService = returnService;
	}
	
	public String getInQty() {
		return inQty;
	}

	public void setInQty(String inQty) {
		this.inQty = inQty;
	}
	
	public String getAvailableQty() {
		return availableQty;
	}

	public void setAvailableQty(String availableQty) {
		this.availableQty = availableQty;
	}
	
	public String getSumInQty() {
		return sumInQty;
	}

	public void setSumInQty(String sumInQty) {
		this.sumInQty = sumInQty;
	}
	
	public String getSumOutQty() {
		return sumOutQty;
	}
	
	public void setSumOutQty(String sumOutQty) {
		this.sumOutQty = sumOutQty;
	}

	public String getTruncMethod() {
		return truncMethod;
	}

	public void setTruncMethod(String truncMethod) {
		this.truncMethod = truncMethod;
	}

	public String getTruncUnit() {
		return truncUnit;
	}

	public void setTruncUnit(String truncUnit) {
		this.truncUnit = truncUnit;
	}

	public String getBaseSection() {
		return baseSection;
	}

	public void setBaseSection(String baseSection) {
		this.baseSection = baseSection;
	}

	public String getRepeatSection() {
		return repeatSection;
	}

	public void setRepeatSection(String repeatSection) {
		this.repeatSection = repeatSection;
	}
	
	public String getAdvBoundTime() {
		return advBoundTime;
	}

	public void setAdvBoundTime(String advBoundTime) {
		this.advBoundTime = advBoundTime;
	}	
	
	public String getBaseAmt() {
		return baseAmt;
	}

	public void setBaseAmt(String baseAmt) {
		this.baseAmt = baseAmt;
	}
	
	public String getRepeatAmt() {
		return repeatAmt;
	}

	public void setRepeatAmt(String repeatAmt) {
		this.repeatAmt = repeatAmt;
	}

	public String getDayAmt() {
		return Util.isNVL(dayAmt,"0");
	}

	public void setDayAmt(String dayAmt) {
		this.dayAmt = dayAmt;
	}
	
	public String getMaxAmt() {
		return Util.isNVL(maxAmt,"0");
	}

	public void setMaxAmt(String maxAmt) {
		this.maxAmt = maxAmt;
	}

	public void setTicketAmt(String ticketAmt) {
		this.ticketAmt = ticketAmt;
	}

	public String getTicketAmt() {
		return ticketAmt;
	}

	public String getCatNumber() {
		return catNumber;
	}

	public void setCatNumber(String catNumber) {
		this.catNumber = catNumber;
	}

	public String getBizName() {
		return bizName;
	}

	public void setBizName(String bizName) {
		this.bizName = bizName;
	}
	
	public String getBizBankMsg() {
		return bizBankMsg;
	}

	public void setBizBankMsg(String bizBankMsg) {
		this.bizBankMsg = bizBankMsg;
	}

	public String getBizAccountNo() {
		return bizAccountNo;
	}

	public void setBizAccountNo(String bizAccountNo) {
		this.bizAccountNo = bizAccountNo;
	}

	public String getBizBank() {
		return bizBank;
	}

	public void setBizBank(String bizBank) {
		this.bizBank = bizBank;
	}

	public String getBizAccountName() {
		return bizAccountName;
	}

	public void setBizAccountName(String bizAccountName) {
		this.bizAccountName = bizAccountName;
	}

	public String getBizCopyRight() {
		return bizCopyRight;
	}

	public void setBizCopyRight(String bizCopyRight) {
		this.bizCopyRight = bizCopyRight;
	}	

	public String getBizTel() {
		return bizTel;
	}

	public void setBizTel(String bizTel) {
		this.bizTel = bizTel;
	}

	public String getBizNo() {
		return bizNo;
	}

	public void setBizNo(String bizNo) {
		this.bizNo = bizNo;
	}

	public String getInPrint() {
		return inPrint;
	}

	public void setInPrint(String inPrint) {
		this.inPrint = inPrint;
	}

	public String getOutPrint() {
		return outPrint;
	}

	public void setOutPrint(String outPrint) {
		this.outPrint = outPrint;
	}

	public String getTicketPrint() {
		return ticketPrint;
	}

	public void setTicketPrint(String ticketPrint) {
		this.ticketPrint = ticketPrint;
	}

}
