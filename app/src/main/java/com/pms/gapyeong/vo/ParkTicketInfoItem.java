package com.pms.gapyeong.vo;

import java.io.Serializable;

public class ParkTicketInfoItem  implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6260267372853306461L;
	
	private String cdPark;
	private String cdType;
	private String carNo;
	private String ticketNo;	 // 정기권번호
	private String startDay;
	private String endDay;
	private String carKind;
	private String carOwner;	 // 소유주
	private String owrnerTel;	 // 연락처
	private String ticketDay;	 // 구입일
	private String ticketAmt;	 // 정기권금액
	private String payAmt;		 // 납부금액
	private String yetAmt;		 // 미납금액
	private String disAmt;		 // 할인금액
	private String cardNo;		 // 카드번호
	private String cardCompany;	 // 카드사
	private String approvalNo;	 // 승인번호
	private String approvalDate; // 승인일자
	private String disCd;		 // 할인코드
	private String stateCd;		 // 상태코드
	private String address;		 // 주소 
	private String comment;		 // 코멘트
	private String cash_approvalNo;
	private String cash_approvalDate;
	
	
	public ParkTicketInfoItem(String cdPark,     String cdType,	  	   String carNo,  	   String ticketNo,
							  String startDay,   String endDay, 	   String carKind,	   String carOwner,
							  String owrnerTel,  String ticketDay,     String ticketAmt,
							  String payAmt,	 String yetAmt,		   String disAmt,		   
							  String cardNo,	 String cardCompany,   String approvalNo,  String approvalDate,		   
							  String disCd,		 String stateCd,       String address,     String comment) {
		
			this.setCdPark(cdPark);
			this.setCdType(cdType);
			this.setCarNo(carNo);
			this.setTicketNo(ticketNo);
			this.setStartDay(startDay);
			this.setEndDay(endDay);
			this.setEndDay(endDay);
			this.setCarKind(carKind);
			this.setCarOwner(carOwner);
			this.setOwrnerTel(owrnerTel);
			this.setTicketDay(ticketDay);
			this.setTicketAmt(ticketAmt);
			this.setPayAmt(payAmt);
			this.setYetAmt(yetAmt);
			this.setDisAmt(disAmt);
			this.setCardNo(cardNo);
			this.setCardCompany(cardCompany);
			this.setApprovalNo(approvalNo);
			this.setApprovalDate(approvalDate);
			this.setDisCd(disCd);
			this.setStateCd(stateCd);
			this.setAddress(address);
			this.setComment(comment);
			
	}
	
	public String getCdPark() {
		return cdPark;
	}

	public void setCdPark(String cdPark) {
		this.cdPark = cdPark;
	}

	public String getCdType() {
		return cdType;
	}

	public void setCdType(String cdType) {
		this.cdType = cdType;
	}

	public String getCarNo() {
		return carNo;
	}

	public void setCarNo(String carNo) {
		this.carNo = carNo;
	}

	public String getStartDay() {
		return startDay;
	}

	public void setStartDay(String startDay) {
		this.startDay = startDay;
	}

	public String getEndDay() {
		return endDay;
	}

	public void setEndDay(String endDay) {
		this.endDay = endDay;
	}
	
	public String getCarKind() {
		return carKind;
	}

	public void setCarKind(String carKind) {
		this.carKind = carKind;
	}

	public String getTicketNo() {
		return ticketNo;
	}

	public void setTicketNo(String ticketNo) {
		this.ticketNo = ticketNo;
	}

	public String getCarOwner() {
		return carOwner;
	}

	public void setCarOwner(String carOwner) {
		this.carOwner = carOwner;
	}

	public String getOwrnerTel() {
		return owrnerTel;
	}

	public void setOwrnerTel(String owrnerTel) {
		this.owrnerTel = owrnerTel;
	}

	public String getTicketDay() {
		return ticketDay;
	}

	public void setTicketDay(String ticketDay) {
		this.ticketDay = ticketDay;
	}

	public String getTicketAmt() {
		return ticketAmt;
	}

	public void setTicketAmt(String ticketAmt) {
		this.ticketAmt = ticketAmt;
	}

	public String getPayAmt() {
		return payAmt;
	}

	public void setPayAmt(String payAmt) {
		this.payAmt = payAmt;
	}

	public String getYetAmt() {
		return yetAmt;
	}

	public void setYetAmt(String yetAmt) {
		this.yetAmt = yetAmt;
	}

	public String getDisAmt() {
		return disAmt;
	}

	public void setDisAmt(String disAmt) {
		this.disAmt = disAmt;
	}

	public String getCardNo() {
		return cardNo;
	}

	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}

	public String getCardCompany() {
		return cardCompany;
	}

	public void setCardCompany(String cardCompany) {
		this.cardCompany = cardCompany;
	}

	public String getApprovalNo() {
		return approvalNo;
	}

	public void setApprovalNo(String approvalNo) {
		this.approvalNo = approvalNo;
	}

	public String getApprovalDate() {
		return approvalDate;
	}

	public void setApprovalDate(String approvalDate) {
		this.approvalDate = approvalDate;
	}	
	
	public void setCashApprovalNo(String approvalNo) {
		this.cash_approvalNo = approvalNo;
	}

	public void setCashApprovalDate(String approvalDate) {
		this.cash_approvalDate = approvalDate;
	}	

	public String getCashApprovalNo() {
		return cash_approvalNo;
	}	
	public String getCashApprovalDate() {
		return cash_approvalDate;
	}	
	
	public String getDisCd() {
		return disCd;
	}

	public void setDisCd(String disCd) {
		this.disCd = disCd;
	}


	public String getStateCd() {
		return stateCd;
	}

	public void setStateCd(String stateCd) {
		this.stateCd = stateCd;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	
	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}	
	
	
}
