package com.pms.gapyeong.vo;

import java.util.ArrayList;

import org.apache.http.util.ByteArrayBuffer;

import com.pms.gapyeong.common.Util;

/**
 * 미수현황
 *
 */
public class ReceiptType9Vo extends ReceiptVo {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6415634510650996487L;
	
	private String title;
	String date;			// 월간마감
	String manager;			
	String managerTel;		
	ArrayList<UnPayManagerItem> item;
	
	public ReceiptType9Vo(String title, String today, String bIZ_NAME, String bIZ_TEL, ArrayList<UnPayManagerItem> unPayList) {
		// TODO Auto-generated constructor stub
		this.title = title;
		this.date = today;
		this.manager = bIZ_NAME;
		this.managerTel = bIZ_TEL;
		this.item = unPayList;
	}


	@Override
	public ByteArrayBuffer print() {
		byte[] lSize = getPrintOption(PRINT_TEXT_SIZE_LARGE);
		byte[] nSize = getPrintOption(PRINT_TEXT_SIZE_DEFAULT);
		
		ByteArrayBuffer bf = new ByteArrayBuffer(1024);
		bf.append(lSize,0,lSize.length);
		replaceByteDate(bf,"        [" + title + "]\n\n");//제일큰글자
		bf.append(nSize,0,nSize.length);
		replaceByteDate(bf,"출력일자 : "+ date+"\n");
		replaceByteDate(bf,"================================\n");
		if("미수환수현황".equals(title)){
			replaceByteDate(bf,"차량번호 |   환수날짜   |  금액 \n");
		}else{
			replaceByteDate(bf,"차량번호 |   미수날짜   |  금액 \n");
		}
		replaceByteDate(bf,"--------------------------------\n");
		int totalAmt = 0;
		for (int i = 0; i < item.size(); i++) {
			replaceByteDate(bf,getEmpty(10, ALIGN_LEFT,item.get(i).getCarNo())
					+getEmpty(10, ALIGN_LEFT, Util.dateString(item.get(i).getPioDay()))
					+getEmpty(8, ALIGN_RIGHT,Util.replaceMoney(item.get(i).getSumAmt()))+"\n");
			totalAmt+=Integer.parseInt(item.get(i).getSumAmt());
		}
		replaceByteDate(bf,"--------------------------------\n");
							// TODO 실제값 조정해야 함 아래 두개
		if("미수환수현황".equals(title)) {
			replaceByteDate(bf, "총환수건수: " + Util.replaceMoney(item.size()) + "개\n");
			replaceByteDate(bf, "총환수금액: " + Util.replaceMoney(totalAmt) + "원\n");
		} else {
			replaceByteDate(bf, "총미수건수: " + Util.replaceMoney(item.size()) + "개\n");
			replaceByteDate(bf, "총미수금액: " + Util.replaceMoney(totalAmt) + "원\n");
		}
//		replaceByteDate(bf,"총입금액: "+totalAmt + "개\n");
		replaceByteDate(bf,"--------------------------------\n");
		replaceByteDate(bf,"사업자명 : "+manager + "\n");
		bf.append(lSize,0,lSize.length);
		replaceByteDate(bf,"주차요원 :"+this.getEmpName()+"\n");
		bf.append(nSize,0,nSize.length);
		if(!Util.isEmpty(this.getEmpPhone())){
			bf.append(lSize,0,lSize.length);
			replaceByteDate(bf,"연 락 처 : "+this.getEmpPhone()+"\n");
		}
		bf.append(nSize,0,nSize.length);
		if(!Util.isEmpty(this.getEmpTel())){
			bf.append(lSize,0,lSize.length);
			replaceByteDate(bf,"문의전화 : "+this.getEmpTel()+"\n");
		}	
		replaceByteDate(bf,"--------------------------------\n");


		replaceByteDate(bf,"********************************\n");
		replaceByteDate(bf,getOutPrint() + "\n");
		replaceByteDate(bf,"********************************\n");
		replaceByteDate(bf,managerTel + "\n");
		replaceByteDate(bf,"\n\n");
		return bf;
	}
	
	
	

	
	
}
