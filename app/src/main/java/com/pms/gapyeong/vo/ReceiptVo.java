package com.pms.gapyeong.vo;

import android.util.Log;

import com.pms.gapyeong.common.Constants;

import org.apache.http.util.ByteArrayBuffer;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;



public abstract class ReceiptVo implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private static final byte ESC = 0x1b;
	private static final byte GS = 0x1d;
	
	protected static final byte PRINT_TEXT_SIZE_MAX = 0x11;
	protected static final byte PRINT_TEXT_SIZE_LARGE = 0x10;
	protected static final byte PRINT_TEXT_SIZE_DEFAULT = 0x00;
    private boolean mEmphasis = false;
    private boolean mUnderline = false;
    private byte mCharsize = 0x00;
	private byte mJustification = 0x00;
	
	// 주차번호
	private String pioNum = "";
	// 차량번호
	private String carNum = "";
	// 주차장
	private String parkName = "";
	// 주차요원
	private String empName = "";
	// 주차요원 핸드폰
	private String empPhone = "";
	// 주차요원 연락처
	private String empTel = "";
	
	private String empBank_MSG = "";
	private String empAccount_No = "";
	private String empKN_ACCOUNT = "";
	private String empKN_BANK = "";



	private String empBusiness_Tel = "";

	private String vcnt_account = "";
	private String ipgm_date = "";
	private String out_print = "";
	/**
	 * 
	 * @param isEmphasis
	 * @param isUnderline
	 * @param charSize
	 * @return
	 */
	protected byte[] getPrintOption(boolean isEmphasis, boolean isUnderline, byte charSize) {
//		ByteArrayBuffer buffer = new ByteArrayBuffer(1024);
		
		byte[] cmd = { ESC, 0x45, (byte) (isEmphasis ? 1 : 0), // ESC E
				ESC, 0x2D, (byte) (isUnderline ? 1 : 0), // ESC -
				GS, 0x21, charSize, // GS !
				ESC, 0x61, mJustification }; // ESC a
//		buffer.append(cmd, 0, cmd.length);
		
		return cmd;
	}
	
	protected byte[] getPrintOption(boolean isEmphasis, byte charSize) {
		return getPrintOption(isEmphasis, false, charSize);
	}
	
	protected byte[] getPrintOption(byte charSize) {
		return getPrintOption(true, false, charSize);
	}
	
	public void replaceByteDate(ByteArrayBuffer bf, String str){
		byte[] text;
		try {
			Log.e("printMsg",str);
			text = str.getBytes("EUC-KR");
			
			if (text.length == 0)
				return;
		
			bf.append(text,0,text.length);
			
		} catch (UnsupportedEncodingException e) {
			if(Constants.DEBUG_PRINT_LOG){
				e.printStackTrace();
			}else{
				System.out.println("예외 발생");
			}
		}
	}
	
	
	
	public static final int ALIGN_CENTER = 0;
	public static final int ALIGN_LEFT = 1;
	public static final int ALIGN_RIGHT = 2;
	
	public static String getEmpty(int max, int align, String value) {
		StringBuffer sb = new StringBuffer();
		
		if (value == null) {
			value = "0";
		}
		int len = value.length();
		
		
		switch (align) {
		case ALIGN_CENTER:
			int left = (max - len) / 2;
			for (int i = 0; i < left; i++) {
				sb.append(" ");
			}
			sb.append(value);
			for (int i = (left + len); i < max; i++) {
				sb.append(" ");
			}
			break;
			
		case ALIGN_LEFT:
			sb.append(value);
			for (int i = len; i <= max; i++) {
				sb.append(" ");
			}
			break;
			
		case ALIGN_RIGHT:
			for (int i = 0; i < (max - len); i++) {
				sb.append(" ");
			}
			sb.append(value);
			break;
		}
		
		return sb.toString();
	}

	
	public abstract ByteArrayBuffer print();
	
	public String getPioNum() {
		return pioNum;
	}

	public void setPioNum(String pioNum) {
		this.pioNum = pioNum;
	}	

	public String getCarNum() {
		return carNum;
	}

	public void setCarNum(String carNum) {
		this.carNum = carNum;
	}
	
	public String getParkName() {
		return parkName;
	}

	public void setParkName(String parkName) {
		this.parkName = parkName;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}	

	public String getEmpPhone() {
		return empPhone;
	}

	public void setEmpPhone(String empPhone) {
		this.empPhone = empPhone;
	}

	public String getEmpTel() {
		return empTel;
	}

	public void setEmpTel(String empTel) {
		this.empTel = empTel;
	}
	
	public void setBank_MSG(String _msg) {
		this.empBank_MSG = _msg;
	}	
	
	public String getBank_MSG()
	{
		return this.empBank_MSG;
	}	
	
	public void setAccount_No(String _no) {
		this.empAccount_No = _no;
	}
	
	public String getAccount_No()
	{
		return this.empAccount_No;
	}		
	
	public void setKN_ACCOUNT(String _account) {
		this.empKN_ACCOUNT = _account;
	}		
	
	public String getKN_ACCOUNT()
	{
		return this.empKN_ACCOUNT;
	}	
	
	public void setKN_BANK(String _bank) {
		this.empKN_BANK = _bank;
	}	
	
	public String getKN_BANK()
	{
		return this.empKN_BANK;
	}

	public void setVcnt_account(String account) { this.vcnt_account = account;}

	public String getVcnt_account() { return this.vcnt_account;}

	public void setIpgm_date(String _date) { this.ipgm_date = _date;}

	public String getIpgm_date() { return this.ipgm_date;}


	public String getEmpBusiness_Tel() {
		return empBusiness_Tel;
	}

	public void setEmpBusiness_Tel(String empBusiness_Tel) {
		this.empBusiness_Tel = empBusiness_Tel;
	}


	public void setOutPrint(String _out_print) {
		this.out_print = _out_print;
	}

	public String getOutPrint() { return this.out_print;}

}
