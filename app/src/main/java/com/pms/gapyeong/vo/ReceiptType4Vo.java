package com.pms.gapyeong.vo;

import com.pms.gapyeong.common.Constants;
import com.pms.gapyeong.common.Util;

import org.apache.http.util.ByteArrayBuffer;

/**
 * 출차 카드 승인 영수증
 *
 */
public class ReceiptType4Vo extends ReceiptVo {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2340458924154189284L;

	// 주차번호
	private String pioNum = "";
	
	String couponAmt="";
	String date;
	String parkType;
	String parkTime; 
	String codeArea; 
	String dcType;
	String inTime; 
	String outTime; 
	String payAmt;
	String prePayAmt; 
	String parkAmt;
	String unPayAmt;
	String dcAmt; 
	String pNum; 
	String manager; 
	String managerTel;
	String title = "주차영수증";
	String cardNo 	   = "";
	String cardCompany = "";
	String PrintText   = "";
	private String cardConfirmNumber= "";
	String CashReceipt = "";
	public ReceiptType4Vo(String date, String parkType, String parkTime, String codeArea, String dcType
			, String inTime, String outTime, String payAmt, String prePayAmt, String parkAmt, String unPayAmt
			, String dcAmt, String pNum, String manager, String managerTel) {
		
		this.date = date;		    // 출력일자
		this.parkType = parkType;	// 주차종류 :
		this.parkTime = parkTime;	// 주차시간 :
		this.codeArea = codeArea;	// 주차면 :
		this.dcType = dcType;		// 할인종류 :
		this.inTime = inTime;		// 입차시간 :
		this.outTime = outTime;		// 출차시간 :
		this.payAmt = payAmt;		// 납부금액 :
		this.prePayAmt = prePayAmt;	// 선납금액 :
		this.parkAmt = parkAmt;		// 주차요금 :
		this.unPayAmt = unPayAmt;	// 미납금액 :
		this.dcAmt = dcAmt;			// 할인금액 :
		this.pNum = pNum;			// 사업자번호
		this.manager = manager;		//
		this.managerTel = managerTel;	//
	}
	
	public String getTitle() {
		return title;
	}

	@Override
	public ByteArrayBuffer print() {
		

		byte[] lSize = getPrintOption(PRINT_TEXT_SIZE_LARGE);
		byte[] nSize = getPrintOption(PRINT_TEXT_SIZE_DEFAULT);
		
		ByteArrayBuffer bf = new ByteArrayBuffer(1024);
		
		if(Integer.valueOf(payAmt) < 0) {
			title = "환불영수증";
		}
		
		bf.append(lSize,0,lSize.length);
		replaceByteDate(bf,"         [" + title + "]\n");//제일큰글자
		bf.append(nSize,0,nSize.length);
		//replaceByteDate(bf,"출력일자 :"+ date + "\n");
		replaceByteDate(bf,"================================\n");
		if(!cardNo.equals("")){
			try {
				replaceByteDate(bf,"카드번호 :" + Util.cardnumString(cardNo) + "\n");
			} catch (NullPointerException e) {
				if(Constants.DEBUG_PRINT_LOG){
					e.printStackTrace();
				}else{
					System.out.println("예외 발생");
				}
			}
			
			if(!cardCompany.equals("")){
				replaceByteDate(bf,"카드사 명 :"+ cardCompany + "\n");
			}
			if(!cardConfirmNumber.equals("")){
				replaceByteDate(bf,"승인번호 :" + cardConfirmNumber + "\n");
			}
			replaceByteDate(bf,"--------------------------------\n");
		}
		bf.append(lSize,0,lSize.length);
		replaceByteDate(bf,"차량번호 :"+this.getCarNum()+"\n");//큰글자 
		
		if(!("").equals(codeArea)) {
			replaceByteDate(bf,"주차번호 :"+codeArea+"\n");
		}
		replaceByteDate(bf,"주차장명 :"+this.getParkName()+"\n");		
		bf.append(nSize,0,nSize.length);
		if(title.equals("승인취소영수증")||title.equals("결제취소")||title.equals("결제취소영수증"))
		{
			if(!("").equals(inTime)) {
				replaceByteDate(bf,"입차시간 :"+Util.dateString(inTime)+"\n");
			}
			
			if(!("").equals(outTime)) {
				replaceByteDate(bf,"출차시간 :"+Util.dateString(outTime)+"\n");
			}
		}else{
			if(!Util.isEmpty(inTime)){
				if(inTime.length() >= 14){
					if(!("").equals(inTime)) {
						replaceByteDate(bf,"입차시간 :"+Util.dateString(inTime)+"\n");
					}
				} else {
					if(!("").equals(inTime)) {
						replaceByteDate(bf,"입차시간 :"+Util.dateString(Util.getYmdhms("yyyyMMdd")+inTime)+"\n");
					}
				}
			}
			
			if(!Util.isEmpty(outTime)){
				if(inTime.length() >= 14){
					if(!("").equals(outTime)) {
						replaceByteDate(bf,"출차시간 :"+Util.dateString(outTime)+"\n");
					}
				} else {
					if(!("").equals(outTime)) {
						replaceByteDate(bf,"출차시간 :"+Util.dateString(Util.getYmdhms("yyyyMMdd")+outTime)+"\n");
					}
				}				
			}
		}
	
		//replaceByteDate(bf,"주차시간 :"+parkTime+"\n");
		//replaceByteDate(bf,"주차종류 :"+parkType+"\n");
		if(!("").equals(dcType)) {
			replaceByteDate(bf,"할인종류 :"+dcType+"\n");
		}
		//replaceByteDate(bf,"주차요금 :"+Util.replaceMoney(parkAmt)+"원\n");
		//replaceByteDate(bf,"할인금액 :"+Util.replaceMoney(dcAmt)+"원\n");
		//replaceByteDate(bf,"미수금액 :"+Util.replaceMoney(unPayAmt)+"원\n");
		//bf.append(lSize,0,lSize.length);
		
		if(Integer.valueOf(payAmt) < 0) {
			bf.append(nSize,0,nSize.length);
			replaceByteDate(bf,"선납금액 :"+Util.replaceMoney(prePayAmt)+"원\n");//큰글자
		}
		if(title.equals("승인취소영수증")||title.equals("결제취소")||title.equals("결제취소영수증"))
		{
			replaceByteDate(bf,"취소금액 :"+Util.replaceMoney(payAmt)+"원\n");//큰글자 
		}
		else{
			if (couponAmt != null && !"".equals(couponAmt) && !"0".equals(couponAmt)&& !"0원".equals(couponAmt) ) {
				replaceByteDate(bf, "쿠폰금액 :" + Util.replaceMoney(couponAmt) + "원\n");//큰글자
				bf.append(lSize,0,nSize.length);

				int coupon = Integer.parseInt(couponAmt.replace("원","").replace(",",""));
				int pay = Integer.parseInt(payAmt.replace("원","").replace(",",""));
				replaceByteDate(bf,"납부금액 :"+Util.replaceMoney(pay-coupon)+"원\n");//큰글자
			}else {
				bf.append(lSize,0,nSize.length);
				replaceByteDate(bf,"납부금액 :"+Util.replaceMoney(payAmt)+"원\n");//큰글자
			}

		}
		
		bf.append(nSize,0,nSize.length);
		if(!"".equals(CashReceipt)){
			replaceByteDate(bf,"--------------------------------\n");
			replaceByteDate(bf,"현금영수증 발행\n");
			replaceByteDate(bf,"승인번호:"+CashReceipt + "\n");
		}
		
		replaceByteDate(bf,"--------------------------------\n");
		replaceByteDate(bf,"사업자번호 : "+pNum + "\n");
		bf.append(nSize,0,lSize.length);
		replaceByteDate(bf,"주차요원 :"+this.getEmpName()+"\n");
		bf.append(nSize,0,nSize.length);
		if(!Util.isEmpty(this.getEmpPhone())){
			bf.append(lSize,0,lSize.length);
			replaceByteDate(bf,"연 락 처 : "+this.getEmpTel()+"\n");
		}
		bf.append(nSize,0,nSize.length);
		if(!Util.isEmpty(this.getEmpTel())){
			replaceByteDate(bf,"문의전화 : "+this.getEmpPhone()+"\n");
		}		
		replaceByteDate(bf,"--------------------------------\n");
		if(!Util.isEmpty(getEmpBusiness_Tel())){
			replaceByteDate(bf,getEmpBusiness_Tel()+"\n");
			replaceByteDate(bf,"--------------------------------\n");
		}


		replaceByteDate(bf,"********************************\n");
		replaceByteDate(bf,getOutPrint() + "\n");
		replaceByteDate(bf,"********************************\n");


		bf.append(lSize,0,lSize.length);
		//replaceByteDate(bf,managerTel+"\n");
		//replaceByteDate(bf,"\n\n");
		return bf;
	}
	
	
	public String getPioNum() {
		return pioNum;
	}

	public void setPioNum(String pioNum) {
		this.pioNum = pioNum;
	}	
	
	public void setConfirmCashReceipt(String val){
		this.CashReceipt = val;
	}
	
	public void setConfirmCardNum(String val){
		this.cardConfirmNumber = val;
	}
	public void setPrint(String val){
		this.PrintText = val;
	}

	public void setCouponAmt(String val) {
		this.couponAmt = val;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}
	
	public void setCardCompany(String cardCompany) {
		this.cardCompany = cardCompany;
	}

	
	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getParkType() {
		return parkType;
	}

	public void setParkType(String parkType) {
		this.parkType = parkType;
	}

	public String getParkTime() {
		return parkTime;
	}

	public void setParkTime(String parkTime) {
		this.parkTime = parkTime;
	}

	public String getDcType() {
		return dcType;
	}

	public void setDcType(String dcType) {
		this.dcType = dcType;
	}

	public String getInTime() {
		return inTime;
	}

	public void setInTime(String inTime) {
		this.inTime = inTime;
	}

	public String getOutTime() {
		return outTime;
	}

	public void setOutTime(String outTime) {
		this.outTime = outTime;
	}

	public String getPayAmt() {
		return payAmt;
	}

	public void setPayAmt(String payAmt) {
		this.payAmt = payAmt;
	}

	public String getPrePayAmt() {
		return prePayAmt;
	}

	public void setPrePayAmt(String prePayAmt) {
		this.prePayAmt = prePayAmt;
	}

	public String getParkAmt() {
		return parkAmt;
	}

	public void setParkAmt(String parkAmt) {
		this.parkAmt = parkAmt;
	}

	public String getUnPayAmt() {
		return unPayAmt;
	}

	public void setUnPayAmt(String unPayAmt) {
		this.unPayAmt = unPayAmt;
	}

	public String getDcAmt() {
		return dcAmt;
	}

	public void setDcAmt(String dcAmt) {
		this.dcAmt = dcAmt;
	}

	public String getpNum() {
		return pNum;
	}

	public void setpNum(String pNum) {
		this.pNum = pNum;
	}

	public String getManager() {
		return manager;
	}

	public void setManager(String manager) {
		this.manager = manager;
	}

	public String getManagerTel() {
		return managerTel;
	}

	public void setManagerTel(String managerTel) {
		this.managerTel = managerTel;
	}
	
	
	
}
