package com.pms.gapyeong.vo;

import java.io.Serializable;

public class UnPayManagerItem implements Serializable{
	private static final long serialVersionUID = 1L;
	private String pioNum;
	private String cdPark;
	private String knPark;
	private String parkIn;
	private String parkOut;
	private String amt;
	
	private String pioDay;
	private String disCd;
	private String parkType;
	private String cdGubun;
	private String addAmt;
	private String sumAmt;
	
	private String cdState;
	private String carState;
	private String knResult;
	private String carNo;
	private String parkIO;
	private String submitAmt;
	private String submitDate;
	private String offAmt;
	private String changeDis;
	private String BILL_TYPE;
	private String cash_app_no;
	private String cash_app_date;	
	private String Group_code;
	private boolean isChecked;
/**
COUNT :미납건수
PIO_NUM : 주차번호
CD_PARK : 주차장코드
KN_PARK : 주차장명
PARK_IN : 입차시간
PARK_OUT : 출차시간
AMT : 미납금액
PIO_DAY : 주차일(20140105)
DIS_CD : 할인코드(20140105)
PARK_TYPE: 요금종류(일일권)
CD_GUBUN: 미납종류(미출,도주)
PARK_INOUT
ADD_AMT : 가산금(20140105)
SUM_AMT : 미납금누계총액(20140105)
SUBMIT_AMT  : 미납환수금 
SUBMIT_DATE : 미납환수일자(2014.12.22) 추가 
CD_STATE : 상태코드(2013.12.31)
CAR_STATE : Y (압류대상), N (정상) (2014.01.08)
KN_RESULT : 압류대상 또는 정상(2014.01.08)
CHANGE_DIS : 웹 할인코드 
 */
	
	public UnPayManagerItem(String pioNum, String cdPark,String knPark,
			String parkIn, String parkOut,String amt
			,String pioDay, String disCd,String parkType,
			String cdGubun, String addAmt,String sumAmt,String cdState,
			String carState, String knResult,String carNo,String parkInOut,
			String submitAmt, String submitDate,
			String offAmt,String changeDis, String Gcode) {
		this.setPioNum(pioNum);
		this.setCdPark(cdPark);
		this.setKnPark(knPark);
		this.setParkIn(parkIn);
		this.setParkOut(parkOut);
		this.setAmt(amt);
		this.setPioDay(pioDay);
		this.setDisCd(disCd);
		this.setParkType(parkType);
		this.setCdGubun(cdGubun);
		this.setAddAmt(addAmt);
		this.setSumAmt(sumAmt);
		this.setCdState(cdState);
		this.setCarState(carState);
		this.setKnResult(knResult);
		this.setCarNo(carNo);
		this.setParkIO(parkInOut);
		this.setSubmitAmt(submitAmt);
		this.setSubmitDate(submitDate);
		this.setOffAmt(offAmt);
		this.setChangeDis(changeDis);
		this.setGroup_code(Gcode);
	}

	
	public void setGroup_code(String _code) {
		this.Group_code = _code;
	}
	public String getGroup_code() {
		return Group_code;
	}	
	
	public void setBill_type(String _type) {
		this.BILL_TYPE = _type;
	}
	public String getBill_type() {
		return BILL_TYPE;
	}		
	
	public void setCash_app_no(String _no) {
		this.cash_app_no = _no;
	}
	public String getCash_app_no() {
		return cash_app_no;
	}	
	
	public void setCash_app_date(String _date) {
		this.cash_app_date = _date;
	}
	public String getCash_app_date() {
		return cash_app_date;
	}	
	
	public void setPioNum(String pioNum) {
		this.pioNum = pioNum;
	}
	public String getPioNum() {
		return pioNum;
	}

	public void setCdPark(String cdPark) {
		this.cdPark = cdPark;
	}

	public String getCdPark() {
		return cdPark;
	}

	public void setKnPark(String knPark) {
		this.knPark = knPark;
	}

	public String getKnPark() {
		return knPark;
	}

	public void setParkIn(String parkIn) {
		this.parkIn = parkIn;
	}

	public String getParkIn() {
		return parkIn;
	}

	public void setParkOut(String parkOut) {
		this.parkOut = parkOut;
	}

	public String getParkOut() {
		return parkOut;
	}

	public void setAmt(String amt) {
		this.amt = amt;
	}

	public String getAmt() {
		if(amt.equals(""))
			return "0";
		return amt;
	}

	public void setPioDay(String pioDay) {
		this.pioDay = pioDay;
	}

	public String getPioDay() {
		return pioDay;
	}

	public void setDisCd(String disCd) {
		this.disCd = disCd;
	}

	public String getDisCd() {
		return disCd;
	}

	public void setParkType(String parkType) {
		this.parkType = parkType;
	}

	public String getParkType() {
		return parkType;
	}

	public void setCdGubun(String cdGubun) {
		this.cdGubun = cdGubun;
	}

	public String getCdGubun() {
		return cdGubun;
	}

	public void setAddAmt(String addAmt) {
		
		this.addAmt = addAmt;
	}

	public String getAddAmt() {
		if(addAmt.equals(""))
			return "0";
		return addAmt;
	}

	public void setSumAmt(String sumAmt) {
		this.sumAmt = sumAmt;
	}

	public String getSumAmt() {
		if(sumAmt.equals(""))
			return "0";
		return sumAmt;
	}
	
	public void setCdState(String cdState) {
		this.cdState = cdState;
	}

	public String getCdState() {
		return cdState;
	}

	public void setCarState(String carState) {
		this.carState = carState;
	}

	public String getCarState() {
		return carState;
	}

	public void setKnResult(String knResult) {
		this.knResult = knResult;
	}

	public String getKnResult() {
		return knResult;
	}

	public String getCarNo() {
		return carNo;
	}

	public void setCarNo(String carNo) {
		this.carNo = carNo;
	}

	public String getParkIO() {
		return parkIO;
	}

	public void setParkIO(String parkIO) {
		this.parkIO = parkIO;
	}

	public String getSubmitAmt() {
		if(submitAmt==null||submitAmt.equals(""))
			return "0";
			
			return submitAmt;
	}

	public void setSubmitAmt(String submitAmt) {
		this.submitAmt = submitAmt;
	}

	public String getSubmitDate() {
		return submitDate;
	}

	public void setSubmitDate(String submitDate) {
		this.submitDate = submitDate;
	}		
	
	public String getOffAmt() {
		return offAmt;
	}

	public void setOffAmt(String offAmt) {
		this.offAmt = offAmt;
	}

	public String getChangeDis() {
		return changeDis;
	}

	public void setChangeDis(String changeDis) {
		this.changeDis = changeDis;
	}
	
	public boolean getChecked() {
		return this.isChecked;
	
	}
	
	public void setChecked(boolean ischeck) {
		this.isChecked = ischeck;
	
	}

}
