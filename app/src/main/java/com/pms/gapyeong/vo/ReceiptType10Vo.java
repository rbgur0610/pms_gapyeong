package com.pms.gapyeong.vo;

import org.apache.http.util.ByteArrayBuffer;

import com.pms.gapyeong.common.Constants;
import com.pms.gapyeong.common.Util;


/**
 * 미수입금영수증
 *
 */
public class ReceiptType10Vo extends ReceiptVo {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4807875653482118752L;
	
	String date;
	String dealType;
	String inputAmt;
	String etcReason;
	String inDate;
	String parkType;
	String chargeType;
	String dcType;
	String prePayAmt;
	String totalUnpayAmt;
	String unPayAmt;
	String unPayType;
	String totalInputAmt="0";
	String returnDate="";
	String pNum;
	String manager;
	String managerTel;
	String cardNo ="";
	String cardCompany = "";
	private String cardConfirmNumber;
	String title = "미수환수영수증";
	String CashReceipt ="";
	String uPname = "";
	String pIn    = "";
	String pOut   = "";
	String couponAmt="";

	public ReceiptType10Vo(String date		// 출력일자
			, String dealType				// 거래종류
			, String inputAmt				// 입금액
			, String etcReason				// 기타이유
			, String inDate					// 미수날짜
			, String parkType				// 주차종류
			, String chargeType				// 요금종류
			, String dcType					// 할인종류
			, String prePayAmt				// 미납잔액
			, String totalUnpayAmt			// 총 미납금
			, String unPayAmt				// 미납금액
			, String unPayType				// 미납타입
			, String totalInputAmt			// 총 입금액
			, String returnDate				// 미수환수일자
			, String pNum					// 사업자번호
			, String manager
			, String managerTel
			, String uPname
			, String pIn
			, String pOut) {
		
		this.date = date;
		this.dealType = dealType;
		this.inputAmt = inputAmt;
		this.etcReason = etcReason;
		this.inDate = inDate;
		this.parkType = parkType;
		this.chargeType = chargeType;
		this.dcType = dcType;
		this.prePayAmt = prePayAmt;
		this.totalUnpayAmt = totalUnpayAmt;
		this.returnDate  = returnDate;
		this.unPayAmt = unPayAmt;
		this.unPayType = unPayType;
		this.totalInputAmt = totalInputAmt;
		this.pNum = pNum;
		this.manager = manager;
		this.managerTel = managerTel;
		this.uPname = uPname;
		this.pIn  = pIn;
		this.pOut = pOut;
	}

	@Override
	public ByteArrayBuffer print() {
		byte[] lSize = getPrintOption(PRINT_TEXT_SIZE_LARGE);
		byte[] nSize = getPrintOption(PRINT_TEXT_SIZE_DEFAULT);
		
		ByteArrayBuffer bf = new ByteArrayBuffer(1024);
		bf.append(lSize,0,lSize.length);
		replaceByteDate(bf, title + "\n\n");//제일큰글자
		bf.append(nSize,0,nSize.length);
		replaceByteDate(bf,"출력일자 : "+ date+"\n");
		replaceByteDate(bf,"================================\n");
		bf.append(lSize,0,lSize.length);
		replaceByteDate(bf,"차량번호 : " + this.getCarNum() + "\n");//큰글자
		replaceByteDate(bf,"주차장명 : "+uPname+"\n");
		bf.append(nSize,0,nSize.length);
		replaceByteDate(bf,"미수날짜 : "+Util.dateString(inDate)+"\n");
		replaceByteDate(bf,"입차시간 : "+Util.dateString(pIn)+"\n");
		replaceByteDate(bf,"출차시간 : "+Util.dateString(pOut)+"\n");
		replaceByteDate(bf,"--------------------------------\n");
//		replaceByteDate(bf,"거래종류 : "+dealType+"\n");
		replaceByteDate(bf,"요금종류 : "+chargeType+"\n");
		replaceByteDate(bf,"할인종류 : "+dcType+"\n");
		replaceByteDate(bf,"주차요금 : "+Util.replaceMoney(unPayAmt)+"원\n");//큰글자 
		replaceByteDate(bf,"미수타입 : "+unPayType+"\n");
		
		if(!etcReason.equals(""))
		replaceByteDate(bf,"기타이유 : "+etcReason+"\n");
		
		if(!inputAmt.equals(""))
		replaceByteDate(bf,"입금액 : "+inputAmt+"\n");//큰글자
		
		replaceByteDate(bf,"--------------------------------\n");
		bf.append(lSize,0,lSize.length);
		replaceByteDate(bf,"미수원금 : "+Util.replaceMoney(totalUnpayAmt)+"원\n");
		bf.append(nSize,0,nSize.length);
		replaceByteDate(bf,"환수금액 : "+Util.replaceMoney(totalInputAmt)+"원\n");
		
		if("".equals(totalInputAmt)){
			totalInputAmt = "0";
		}
		
		int totalunPay = Integer.parseInt(totalUnpayAmt)-Integer.parseInt(totalInputAmt);
		
		replaceByteDate(bf,"총미수잔액 : "+Util.replaceMoney(totalunPay)+"원\n");
		replaceByteDate(bf,"환수날짜 : "+ returnDate +"\n");
		
		if(!"".equals(CashReceipt)){
			replaceByteDate(bf,"--------------------------------\n");
			replaceByteDate(bf,"현금영수증 발행\n");
			replaceByteDate(bf,"승인번호: "+CashReceipt + "\n");
		}
		

		if(!cardNo.equals(""))
		{		
			replaceByteDate(bf,"--------------------------------\n");
			try {
				replaceByteDate(bf,"카드번호 :" + Util.cardnumString(cardNo) + "\n");
			} catch (NullPointerException e) {
				if(Constants.DEBUG_PRINT_LOG){
					e.printStackTrace();
				}else{
					System.out.println("예외 발생");
				}
			}
			
			if(!cardCompany.equals(""))
				replaceByteDate(bf,"카드사 명 : "+ cardCompany + "\n");
			if(!cardConfirmNumber.equals(""))
				replaceByteDate(bf,"승인번호 : " + cardConfirmNumber + "\n");
		}
		replaceByteDate(bf,"--------------------------------\n");
		//replaceByteDate(bf,"사업자명 : "+manager + "\n");
		replaceByteDate(bf,"사업자번호 : "+pNum+"\n");
		bf.append(lSize,0,lSize.length);
		replaceByteDate(bf,"주차요원 : "+this.getEmpName()+"\n");
		if(!Util.isEmpty(this.getEmpPhone())){
			bf.append(lSize,0,lSize.length);
			replaceByteDate(bf,"연 락 처 : "+this.getEmpTel()+"\n");
		}
		bf.append(nSize,0,nSize.length);
		if(!Util.isEmpty(this.getEmpTel())){
			replaceByteDate(bf,"문의전화 : "+this.getEmpPhone()+"\n");
		}
		replaceByteDate(bf,"--------------------------------\n");


		replaceByteDate(bf,"********************************\n");
		replaceByteDate(bf,getOutPrint() + "\n");
		replaceByteDate(bf,"********************************\n");
		//bf.append(lSize,0,lSize.length);
		//replaceByteDate(bf,managerTel + "\n");
		//replaceByteDate(bf,"\n\n");
		return bf;
	}
	
	public void setConfirmCashReceipt(String val){
		this.CashReceipt = val;
	}
	
	public void setTitle(String val){
		this.title = val;
	}
	
	public void setConfirmCardNum(String val){
		this.cardConfirmNumber = val;
	}

	public void setInputTotalAmt(String val) {
		this.totalInputAmt = val;
	}
	
	public void setUnpayTotal(String val) {
		this.prePayAmt = val;
	}
	
	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}
	
	public void setCardCompany(String cardCompany) {
		this.cardCompany = cardCompany;
	}

	public String getDate() {
		return date;
	}

	public String getDealType() {
		return dealType;
	}

	public String getInputAmt() {
		return inputAmt;
	}

	public String getEtcReason() {
		return etcReason;
	}

	public String getDcType() {
		return dcType;
	}

	public String getInDate() {
		return inDate;
	}


	public String getPrePayAmt() {
		return prePayAmt;
	}

	public String getTotalUnpayAmt() {
		return totalUnpayAmt;
	}

	public String getUnPayAmt() {
		return unPayAmt;
	}

	public String getUnPayType() {
		return unPayType;
	}

	public String getTotalInputAmt() {
		if("".equals(totalInputAmt))
			totalInputAmt = "0";
		
		return totalInputAmt;
	}

	public String getpNum() {
		return pNum;
	}

	public String getManager() {
		return manager;
	}

	public String getManagerTel() {
		return managerTel;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public void setDealType(String dealType) {
		this.dealType = dealType;
	}

	public void setInputAmt(String inputAmt) {
		this.inputAmt = inputAmt;
	}

	public void setEtcReason(String etcReason) {
		this.etcReason = etcReason;
	}

	public void setDcType(String dcType) {
		this.dcType = dcType;
	}

	public void setInDate(String inDate) {
		this.inDate = inDate;
	}

	public void setPrePayAmt(String prePayAmt) {
		this.prePayAmt = prePayAmt;
	}

	public void setTotalUnpayAmt(String totalUnpayAmt) {
		this.totalUnpayAmt = totalUnpayAmt;
	}

	public void setUnPayAmt(String unPayAmt) {
		this.unPayAmt = unPayAmt;
	}

	public void setUnPayType(String unPayType) {
		this.unPayType = unPayType;
	}

	public void setTotalInputAmt(String totalInputAmt) {
		this.totalInputAmt = totalInputAmt;
	}


	public void setpNum(String pNum) {
		this.pNum = pNum;
	}

	public void setManager(String manager) {
		this.manager = manager;
	}

	public void setManagerTel(String managerTel) {
		this.managerTel = managerTel;
	}
	
	public void setCouponAmt(String val) {
		this.couponAmt = val;
	}
	
}
