package com.pms.gapyeong.vo;

import org.apache.http.util.ByteArrayBuffer;

import com.pms.gapyeong.common.Constants;
import com.pms.gapyeong.common.Util;


/**
 * 정기권 영수증
 *
 */
public class ReceiptType11Vo extends ReceiptVo {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6501584551620834306L;
	
	String date;
	String ticketType;
	String useTime;
	String dcType;
	String sTime;
	String eTime;
	String payAmt;
	String ticketAmt;
	String dcAmt;
	String pNum;
	String manager;
	String managerTel;
	String cardNo 	   	     = "";
	String cardCompany 	     = "";
	String cardConfirmNumber = "";
	String PrintText = "";
	String yetAmt="";
	String CashReceipt= "";
	String type= "";
	
	//정기권영수증 
	public ReceiptType11Vo(String date			// 출력일자
			               , String ticketType	// 요금제
			               , String useTime		// 사용기간
			               , String dcType		// 할인종류
			               , String sTime		// 시작날짜
			               , String eTime		// 종료날짜
			               , String payAmt		// 납부금액
			               , String ticketAmt	// 정기요금
			               , String dcAmt		// 할인금액
			               , String pNum		// 사업자번호
			               , String manager
			               , String managerTel
			               , String type) {
		
		this.date = date;
		this.ticketType = ticketType;
		this.useTime = useTime;
		this.dcType = dcType;
		this.sTime = sTime;
		this.eTime = eTime;
		this.payAmt = payAmt;
		this.ticketAmt = ticketAmt;
		this.dcAmt = dcAmt;
		this.pNum = pNum;
		this.manager = manager;
		this.managerTel = managerTel;
		this.type = type;
	}

	@Override
	public ByteArrayBuffer print() {
		byte[] lSize = getPrintOption(PRINT_TEXT_SIZE_LARGE);
		byte[] nSize = getPrintOption(PRINT_TEXT_SIZE_DEFAULT);
		
		ByteArrayBuffer bf = new ByteArrayBuffer(1024);
		bf.append(lSize,0,lSize.length);
		replaceByteDate(bf,"      [정기권영수증]\n");//제일큰글자
		bf.append(nSize,0,nSize.length);
		//replaceByteDate(bf,"출력일자 : "+ date+"\n");
		replaceByteDate(bf,"================================\n");
		bf.append(lSize,0,lSize.length);
		replaceByteDate(bf,"차량번호 : "+this.getCarNum()+"\n");//큰글자 
		replaceByteDate(bf,"주차장명 : "+this.getParkName()+"\n");
		bf.append(nSize,0,nSize.length);
		replaceByteDate(bf,"시작날짜 : "+sTime+"\n");
		replaceByteDate(bf,"종료날짜 : "+eTime+"\n");
//		replaceByteDate(bf,"사용기간 : "+useTime+"일\n");
		//replaceByteDate(bf,"요금제 : "+ticketType+"\n");
		replaceByteDate(bf,"할인종류 : "+dcType+"\n");
		replaceByteDate(bf,"정기요금 : "+Util.replaceMoney(ticketAmt)+"원"+"\n");
		replaceByteDate(bf,"할인금액 : "+Util.replaceMoney(dcAmt)+"원"+"\n");
		
		bf.append(lSize,0,lSize.length);
		if(payAmt.equals("0"))
		{
		  // 재출력 시 납부금액이 없더라도 납부금액으로 출력	
		  if(type.equals("reprint")){
			  replaceByteDate(bf,"납부금액 : "+Util.replaceMoney(payAmt)+"원"+"\n");//큰글자 
		  } else {
			  replaceByteDate(bf,"미수금액 : "+Util.replaceMoney(yetAmt)+"원"+"\n");//큰글자 
		  }
		} else {
			replaceByteDate(bf,"납부금액 : "+Util.replaceMoney(payAmt)+"원"+"\n");//큰글자 
		}
		
		bf.append(nSize,0,nSize.length);
		if(!"".equals(CashReceipt)){
			replaceByteDate(bf,"--------------------------------\n");
			replaceByteDate(bf,"현금영수증 발행\n");
			replaceByteDate(bf,"승인번호: "+CashReceipt + "\n");
		}
		
		replaceByteDate(bf,"--------------------------------\n");
		if(!cardNo.equals(""))
		{
			try {
				replaceByteDate(bf,"카드번호 :" + Util.cardnumString(cardNo) + "\n");
			} catch (NullPointerException e) {
				if(Constants.DEBUG_PRINT_LOG){
					e.printStackTrace();
				}else{
					System.out.println("NullPointerException 예외 발생");
				}
			}
			if(!cardCompany.equals(""))
			replaceByteDate(bf,"카드사 명 : " + cardCompany + "\n");
			if(!cardConfirmNumber.equals(""))
			replaceByteDate(bf,"승인번호 : " + cardConfirmNumber + "\n");
			replaceByteDate(bf,"--------------------------------\n");
		}
		
		replaceByteDate(bf,PrintText+"\n");
		replaceByteDate(bf,"--------------------------------\n");
		replaceByteDate(bf,"사업자번호 : "+pNum + "\n");
		bf.append(nSize,0,lSize.length);		
		replaceByteDate(bf,"주차요원 : "+this.getEmpName()+"\n");
		if(!Util.isEmpty(this.getEmpPhone())){
			bf.append(lSize,0,lSize.length);
			replaceByteDate(bf,"연 락 처 : "+this.getEmpTel()+"\n");
		}
		bf.append(nSize,0,nSize.length);
		if(!Util.isEmpty(this.getEmpTel())){
			replaceByteDate(bf,"문의전화 : "+this.getEmpPhone()+"\n");
		}		
		replaceByteDate(bf,"--------------------------------\n");


		replaceByteDate(bf,"********************************\n");
		replaceByteDate(bf,getOutPrint() + "\n");
		replaceByteDate(bf,"********************************\n");

		//bf.append(lSize,0,lSize.length);
		//replaceByteDate(bf,managerTel + "\n");
		//replaceByteDate(bf,"\n\n");
		return bf;
	}
	
	public void setConfirmCashReceipt(String val){
		this.CashReceipt = val;
	}
	
	public void setConfirmCardNum(String val){
		this.cardConfirmNumber = val;
	}
	
	public void setPayment(String val){
		this.payAmt = val;
	}
	public void setYetAmt(String val){
		this.yetAmt = val;
	}
	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}
	public void setPrint(String val){
		this.PrintText = val;
	}
	public void setCardCompany(String cardCompany) {
		this.cardCompany = cardCompany;
	}

	public String getDate() {
		return date;
	}

	public String getTicketType() {
		return ticketType;
	}

	public String getUseTime() {
		return useTime;
	}

	public String getDcType() {
		return dcType;
	}

	public String getsTime() {
		return sTime;
	}

	public String geteTime() {
		return eTime;
	}

	public String getPayAmt() {
		return payAmt;
	}

	public String getTicketAmt() {
		return ticketAmt;
	}

	public String getDcAmt() {
		return dcAmt;
	}

	public String getpNum() {
		return pNum;
	}

	public String getManager() {
		return manager;
	}

	public String getManagerTel() {
		return managerTel;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public void setTicketType(String ticketType) {
		this.ticketType = ticketType;
	}

	public void setUseTime(String useTime) {
		this.useTime = useTime;
	}

	public void setDcType(String dcType) {
		this.dcType = dcType;
	}

	public void setsTime(String sTime) {
		this.sTime = sTime;
	}

	public void seteTime(String eTime) {
		this.eTime = eTime;
	}

	public void setPayAmt(String payAmt) {
		this.payAmt = payAmt;
	}

	public void setTicketAmt(String ticketAmt) {
		this.ticketAmt = ticketAmt;
	}

	public void setDcAmt(String dcAmt) {
		this.dcAmt = dcAmt;
	}

	public void setpNum(String pNum) {
		this.pNum = pNum;
	}

	public void setManager(String manager) {
		this.manager = manager;
	}

	public void setManagerTel(String managerTel) {
		this.managerTel = managerTel;
	}

}
