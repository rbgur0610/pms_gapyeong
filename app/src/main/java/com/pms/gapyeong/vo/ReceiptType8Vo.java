package com.pms.gapyeong.vo;

import java.io.Serializable;
import java.util.ArrayList;

import org.apache.http.util.ByteArrayBuffer;

import com.pms.gapyeong.common.Util;

/**
 * 정기권현황
 *
 */
public class ReceiptType8Vo extends ReceiptVo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -287504974861342259L;
	
	private String title;
	String date;			
	String manager;			
	String managerTel;		
	String empPhone;
	ArrayList<ParkTicketInfoItem> item;
	
	public ReceiptType8Vo(String title, String today, String bIZ_NAME, String bIZ_TEL, ArrayList<ParkTicketInfoItem> unPayList) {
		// TODO Auto-generated constructor stub
		
		this.title = title;
		this.date = today;
		this.manager = bIZ_NAME;
		this.managerTel = bIZ_TEL;
		this.item = unPayList;
	}

	
	@Override
	public ByteArrayBuffer print() {
		byte[] lSize = getPrintOption(PRINT_TEXT_SIZE_LARGE);
		byte[] nSize = getPrintOption(PRINT_TEXT_SIZE_DEFAULT);
		
		
		ByteArrayBuffer bf = new ByteArrayBuffer(1024);
		bf.append(lSize,0,lSize.length);
		replaceByteDate(bf,"         [" + title + "]\n\n");//제일큰글자
		bf.append(nSize,0,nSize.length);
		replaceByteDate(bf,"출력일자 : "+ date+"\n");
		replaceByteDate(bf,"================================\n");
//		replaceByteDate(bf,"차량번호 |시작날짜|구분|결제금액\n");
		replaceByteDate(bf,"차량번호 |시작날짜|결제금액\n");
		replaceByteDate(bf,"--------------------------------\n");
		int totalAmt = 0;
		for (int i = 0; i < item.size(); i++) {
//			String btype = IntroActivity.getCommonCodeValue("B1", item.get(i).getBillType());//한글로 넣으려고 했으나 칸을 너무 많이 차지함
			replaceByteDate(bf,getEmpty(9, ALIGN_LEFT,item.get(i).getCarNo())
					+getEmpty(8, ALIGN_LEFT,item.get(i).getStartDay())
//					+getEmpty(3, ALIGN_CENTER, item.get(i).getBillType())
					+getEmpty(8, ALIGN_RIGHT,Util.replaceMoney(item.get(i).getPayAmt()))+"\n");
			totalAmt+=Integer.parseInt(item.get(i).getPayAmt());
		}
		replaceByteDate(bf,"--------------------------------\n");
		replaceByteDate(bf,"총거래건수: "+Util.replaceMoney(item.size()) + "개\n");
		replaceByteDate(bf,"총입금액: "+Util.replaceMoney(totalAmt) + "원\n");
//		replaceByteDate(bf,"총미납금액: "+Util.replaceMoney(totalAmt) + "원\n");
//		replaceByteDate(bf,"총취소금액: "+Util.replaceMoney(totalAmt) + "원\n");
		replaceByteDate(bf,"--------------------------------\n");
		replaceByteDate(bf,"사업자명 : "+manager + "\n");
		replaceByteDate(bf,"주차장명 : "+this.getParkName()+"\n");
		bf.append(lSize,0,lSize.length);
		replaceByteDate(bf,"주차요원 :"+this.getEmpName()+"\n");
		bf.append(nSize,0,nSize.length);
		if(!Util.isEmpty(this.getEmpPhone())){
			bf.append(lSize,0,lSize.length);
			replaceByteDate(bf,"연 락 처 : "+this.getEmpPhone()+"\n");
		}
		bf.append(nSize,0,nSize.length);
		if(!Util.isEmpty(this.getEmpTel())){
			replaceByteDate(bf,"문의전화 : "+this.getEmpTel()+"\n");
		}	
		replaceByteDate(bf,"--------------------------------\n");


		replaceByteDate(bf,"********************************\n");
		replaceByteDate(bf,getOutPrint() + "\n");
		replaceByteDate(bf,"********************************\n");
		bf.append(lSize,0,lSize.length);
		replaceByteDate(bf,managerTel + "\n");
		replaceByteDate(bf,"\n\n");
		return bf;
	}
	
	
	
}
