package com.pms.gapyeong.vo;

import java.io.Serializable;
import java.util.ArrayList;

import com.pms.gapyeong.common.Util;

public class ParkCloseStatusItem implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4911863143741325118L;

	//	TIME_CNT : 시간권카운트
	String timeCnt = "0";
//	TIME_AMT :시간권금액
	String timeAmt = "0";
	
//	TICKET_CASH_CNT : 정기권 현금 카운트
	String ticketCashCnt = "0";
//	TICKET_CASH_AMT : 정기권 현금 금액
	String ticketCashAmt = "0";

//	TICKET_CARD_CNT : 정기권 카드 카운트
	String ticketCardCnt = "0";
//	TICKET_CARD_AMT : 정기권 카드 금액
	String ticketCardAmt = "0";
	
//	TICKET_CNT : 정기권 카운트
	String ticketCnt = "0";
//	TICKET_AMT : 정기권 금액
	String ticketAmt = "0";
	
	//	PAY_CNT : 미납금환수 카운트
	String payCnt = "0";
//	PAY_AMT : 미납금환수 금액
	String payAmt = "0";
//	SUM_CASH_CNT : 총현금입금 카운트
	String sumCashCnt = "0";
//	SUM_CASH_AMT : 총현금 입금액
	String sumCashAmt = "0";
//	SUM_CARD_CNT : 총카드입금 카운트
	String sumCardCnt = "0";  
//	SUM_CARD_AMT : 총카드입금
	String sumCardAmt = "0";
//	YET_CNT : 총미납카운트
	String yetCnt = "0";
//	YET_AMT : 총미납금액
	String yetAmt = "0";
//	RETURN_CNT : 환불카운트
	String returnCnt = "0";
//	RETURN_AMT : 환불금액
	String returnAmt = "0";
	
	String totalCouponCnt = "0";
	String totalCouponAmt = "0";
	
	String MH_CASH_CNT  ="0";
	String MH_CASH_AMT  ="0";
	String MH_CARD_CNT  ="0";
	String MH_CARD_AMT  ="0";
	
	String MH_CASH_CNT_TODAY  ="0";
	String MH_CASH_AMT_TODAY  ="0";
	String MH_CARD_CNT_TODAY  ="0";
	String MH_CARD_AMT_TODAY  ="0";	
	
	// TOTAL_CASH_CNT 총 현금카운트
	String totalCashCnt;
	// TOTAL_CASH_AMT 총 현금액
	String totalCashAmt;
	// TOTAL_CARD_CNT 총 카드 카운트
	String totalCardCnt;
	// TOTAL_CARD_AMT 총 카드 수입
	String totalCardAmt;
	// TOTAL_CNT  총현금+카드카운트
	String totalCnt;
	// TOTAL_AMT  총수입금액(현금 + 카드)
	String totalAmt;
	String freeCountSum;
	String coupon_sell_cnt = "0";
	String coupon_sell_amt = "0";
	
	// 미납발생 차량 출력항목
	ArrayList<ParkCloseStatusYet> yetList;
	// 미수금납부 차량 출력항목
	ArrayList<ParkCloseStatusReturn> returnList;
	// 정기권 차량 출력항목
	ArrayList<ParkCloseStatusTicket> ticketList;
	ArrayList<ParkCloseStatusDiscountList> DiscountList;
	ArrayList<ParkCloseStatusDiscountList> CouponList;
	
	public ParkCloseStatusItem(String timeCnt, 	  	  String timeAmt
							 , String ticketCashCnt,  String ticketCashAmt
							 , String ticketCardCnt,  String ticketCardAmt
							 , String ticketCnt,  	  String ticketAmt
							 , String payCnt, 	  	  String payAmt
							 , String sumCashCnt, 	  String sumCashAmt
							 , String sumCardCnt, 	  String sumCardAmt
							 , String yetCnt, 	  	  String yetAmt
							 , String returnCnt,  	  String returnAmt
							 , String totalCouponCnt, String totalCouponAmt
							 , String totalCashCnt,   String totalCashAmt
							 , String totalCardCnt,   String totalCardAmt
							 , String totalCnt, 	  String totalAmt
							 , ArrayList<ParkCloseStatusYet> yetList
							 , ArrayList<ParkCloseStatusReturn> returnList
							 , ArrayList<ParkCloseStatusTicket> ticketList 
							 , String _mCashCnt, String _mCashAmt, String _mCardCnt,String _mCardAmt
							 , String _mCashCntToday, String _mCashAmtToday, String _mCardCntToday,String _mCardAmtToday,
							 String freeCountSum
							 , ArrayList<ParkCloseStatusDiscountList> DiscountList, ArrayList<ParkCloseStatusDiscountList> CouponList) {
		
		this.timeCnt   = Util.isNVL(timeCnt,"0");
		this.timeAmt   = Util.isNVL(timeAmt,"0");
		this.ticketCashCnt = Util.isNVL(ticketCashCnt,"0");
		this.ticketCashAmt = Util.isNVL(ticketCashAmt,"0");
		this.ticketCardCnt = Util.isNVL(ticketCardCnt,"0");
		this.ticketCardAmt = Util.isNVL(ticketCardAmt,"0");
		this.ticketCnt = Util.isNVL(ticketCnt,"0");
		this.ticketAmt = Util.isNVL(ticketAmt,"0");
		this.payCnt = Util.isNVL(payCnt,"0");
		this.payAmt = Util.isNVL(payAmt,"0");
		this.sumCashCnt = Util.isNVL(sumCashCnt,"0");
		this.sumCashAmt = Util.isNVL(sumCashAmt,"0");
		this.sumCardCnt = Util.isNVL(sumCardCnt,"0");
		this.sumCardAmt = Util.isNVL(sumCardAmt,"0");
		this.yetCnt 	= Util.isNVL(yetCnt,"0");
		this.yetAmt 	= Util.isNVL(yetAmt,"0");
		this.returnCnt 	= Util.isNVL(returnCnt,"0");
		this.returnAmt 	= Util.isNVL(returnAmt,"0");
		this.totalCouponCnt	= Util.isNVL(totalCouponCnt,"0");
		this.totalCouponAmt	= Util.isNVL(totalCouponAmt,"0");
		
		this.totalCashCnt	= Util.isNVL(totalCashCnt,"0");
		this.totalCashAmt	= Util.isNVL(totalCashAmt,"0");
		this.totalCardCnt	= Util.isNVL(totalCardCnt,"0");
		this.totalCardAmt	= Util.isNVL(totalCardAmt,"0");
		this.totalCnt		= Util.isNVL(totalCnt,"0");
		this.totalAmt		= Util.isNVL(totalAmt,"0");
		
		this.yetList    = yetList;
		this.returnList = returnList;
		this.ticketList = ticketList;
		this.MH_CASH_CNT = _mCashCnt;
		this.MH_CASH_AMT = _mCashAmt;
		this.MH_CARD_CNT = _mCardCnt;
		this.MH_CARD_AMT = _mCardAmt;
		this.MH_CASH_CNT_TODAY = _mCashCntToday;
		this.MH_CASH_AMT_TODAY = _mCashAmtToday;
		this.MH_CARD_CNT_TODAY = _mCardCntToday;
		this.MH_CARD_AMT_TODAY = _mCardAmtToday;
		this.freeCountSum =freeCountSum;
		this.DiscountList = DiscountList;
		this.CouponList = CouponList;
	}
	
	public ParkCloseStatusItem() {}
	
	
	public String getTimeCnt() {
		return timeCnt;
	}
	
	public String get_MHCount()
	{
		return this.MH_CARD_CNT;
	}

	public String get_MHAmt()
	{
		return this.MH_CARD_AMT;
	}
	
	public String get_MH_Cashcount()
	{
		return this.MH_CASH_CNT;
	}
	
	public String get_MH_CashAmt()
	{
		return this.MH_CASH_AMT;
	}
	
	public void setCouponSellCnt(String couponCnt) {
		this.coupon_sell_cnt = couponCnt;
	}

	public String getCouponSellCnt() {
		return coupon_sell_cnt;
	}
	
	public void setCouponSellAmt(String couponAmt) {
		this.coupon_sell_amt = couponAmt;
	}

	public String getCouponSellAmt() {
		return coupon_sell_amt;
	}		

	public void setTimeCnt(String timeCnt) {
		this.timeCnt = timeCnt;
	}

	public String getTimeAmt() {
		return timeAmt;
	}

	public void setTimeAmt(String timeAmt) {
		this.timeAmt = timeAmt;
	}

	public String getTicketCashCnt() {
		return ticketCashCnt;
	}

	public void setTicketCashCnt(String ticketCashCnt) {
		this.ticketCashCnt = ticketCashCnt;
	}

	public String getTicketCashAmt() {
		return ticketCashAmt;
	}

	public void setTicketCashAmt(String ticketCashAmt) {
		this.ticketCashAmt = ticketCashAmt;
	}

	public String getTicketCardCnt() {
		return ticketCardCnt;
	}

	public void setTicketCardCnt(String ticketCardCnt) {
		this.ticketCardCnt = ticketCardCnt;
	}

	public String getTicketCardAmt() {
		return ticketCardAmt;
	}

	public void setTicketCardAmt(String ticketCardAmt) {
		this.ticketCardAmt = ticketCardAmt;
	}	
	
	public String getTicketCnt() {
		return ticketCnt;
	}

	public void setTicketCnt(String ticketCnt) {
		this.ticketCnt = ticketCnt;
	}

	public String getTicketAmt() {
		return ticketAmt;
	}

	public void setTicketAmt(String ticketAmt) {
		this.ticketAmt = ticketAmt;
	}

	public String getPayCnt() {
		return payCnt;
	}

	public void setPayCnt(String payCnt) {
		this.payCnt = payCnt;
	}

	public String getPayAmt() {
		return payAmt;
	}

	public void setPayAmt(String payAmt) {
		this.payAmt = payAmt;
	}

	public String getSumCashCnt() {
		return sumCashCnt;
	}

	public void setSumCashCnt(String sumCashCnt) {
		this.sumCashCnt = sumCashCnt;
	}

	public String getSumCashAmt() {
		return sumCashAmt;
	}

	public void setSumCashAmt(String sumCashAmt) {
		this.sumCashAmt = sumCashAmt;
	}

	public String getSumCardCnt() {
		return sumCardCnt;
	}

	public void setSumCardCnt(String sumCardCnt) {
		this.sumCardCnt = sumCardCnt;
	}

	public String getSumCardAmt() {
		return sumCardAmt;
	}

	public void setSumCardAmt(String sumCardAmt) {
		this.sumCardAmt = sumCardAmt;
	}

	public String getYetCnt() {
		return yetCnt;
	}

	public void setYetCnt(String yetCnt) {
		this.yetCnt = yetCnt;
	}

	public String getYetAmt() {
		return yetAmt;
	}

	public void setYetAmt(String yetAmt) {
		this.yetAmt = yetAmt;
	}

	public String getReturnCnt() {
		return returnCnt;
	}

	public void setReturnCnt(String returnCnt) {
		this.returnCnt = returnCnt;
	}

	public String getReturnAmt() {
		return returnAmt;
	}

	public void setReturnAmt(String returnAmt) {
		this.returnAmt = returnAmt;
	}

	public String getTotalCouponCnt() {
		return totalCouponCnt;
	}

	public void setTotalCouponCnt(String totalCouponCnt) {
		this.totalCouponCnt = totalCouponCnt;
	}

	public String getTotalCouponAmt() {
		return totalCouponAmt;
	}

	public void setTotalCouponAmt(String totalCouponAmt) {
		this.totalCouponAmt = totalCouponAmt;
	}

	public String getTotalCashCnt() {
		return totalCashCnt;
	}

	public void setTotalCashCnt(String totalCashCnt) {
		this.totalCashCnt = totalCashCnt;
	}

	public String getTotalCashAmt() {
		return totalCashAmt;
	}

	public void setTotalCashAmt(String totalCashAmt) {
		this.totalCashAmt = totalCashAmt;
	}

	public String getTotalCardCnt() {
		return totalCardCnt;
	}

	public void setTotalCardCnt(String totalCardCnt) {
		this.totalCardCnt = totalCardCnt;
	}

	public String getTotalCardAmt() {
		return totalCardAmt;
	}

	public void setTotalCardAmt(String totalCardAmt) {
		this.totalCardAmt = totalCardAmt;
	}

	public String getTotalCnt() {
		return totalCnt;
	}

	public void setTotalCnt(String totalCnt) {
		this.totalCnt = totalCnt;
	}

	public String getTotalAmt() {
		return totalAmt;
	}

	public void setTotalAmt(String totalAmt) {
		this.totalAmt = totalAmt;
	}	
	
	public void setFreeCountSum(String freeCountSum) {
		this.freeCountSum = freeCountSum;
	}
	public void getFreeCountSum(String freeCountSum) {
		this.freeCountSum = freeCountSum;
	}


}
