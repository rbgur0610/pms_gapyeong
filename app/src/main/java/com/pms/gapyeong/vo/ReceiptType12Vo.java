package com.pms.gapyeong.vo;

import org.apache.http.util.ByteArrayBuffer;
import com.pms.gapyeong.common.Util;

/**
 * 일간/주간 업무 보고
 *
 */
public class ReceiptType12Vo extends ReceiptVo {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2450973183253560958L;
	
	private String title;
	String printDate;		// 출력일자
	String date;			// 월간마감
	String emptyName;		// 주차요원
	String parkName;		// 주차장명
	
	String manager;			// 파주시청
	String managerTel;		// 031-940-2768
	
	ParkCloseStatusItem item;
	
	
	public ReceiptType12Vo(String title
						   , String printDate		// 출력일자
						   , String date			// 월간마감
						   , String parkName		// 주차장명
						   , String emptyName		// 주차요원
						   , String manager			
						   , String managerTel		
						   , ParkCloseStatusItem item) {
		this.title = title;
		this.printDate = printDate;
		this.date = date;
		this.parkName = parkName;
		this.manager = manager;
		this.managerTel = managerTel;
		this.item = item;
		this.emptyName = emptyName;
	}
	
	
	static final int ALIGN_CENTER = 0;
	static final int ALIGN_LEFT = 1;
	static final int ALIGN_RIGHT = 2;
	
	public static String getEmpty(int max, int align, String value) {
		StringBuffer sb = new StringBuffer();
		
		if (value == null) {
			value = "0";
		}
		int len = value.length();
		
		
		switch (align) {
		case ALIGN_CENTER:
			int left = (max - len) / 2;
			for (int i = 0; i < left; i++) {
				sb.append(" ");
			}
			sb.append(value);
			for (int i = (left + len); i < max; i++) {
				sb.append(" ");
			}
			break;
			
		case ALIGN_LEFT:
			sb.append(value);
			for (int i = len; i <= max; i++) {
				sb.append(" ");
			}
			break;
			
		case ALIGN_RIGHT:
			for (int i = 0; i < (max - len); i++) {
				sb.append(" ");
			}
			sb.append(value);
			break;
		}
		
		return sb.substring(0, max);
	}
	
	@Override
	public ByteArrayBuffer print() {
		
		byte[] lSize = getPrintOption(PRINT_TEXT_SIZE_LARGE);
		byte[] nSize = getPrintOption(PRINT_TEXT_SIZE_DEFAULT);
		
		ByteArrayBuffer bf = new ByteArrayBuffer(1024);
		bf.append(lSize,0,lSize.length);
		replaceByteDate(bf,"     [" + title + "]\n\n");//제일큰글자
		bf.append(nSize,0,nSize.length);
		replaceByteDate(bf,"출력일자 : "+ printDate+"\n");
		replaceByteDate(bf,"근무날짜 : "+ date+"\n");
		replaceByteDate(bf,"주차장명 : "+ parkName+"\n");
		replaceByteDate(bf,"주차요원 : "+ emptyName+"\n");
		// 2016-10-25 앱-마감관리 에서 일일보고서 출력시 출력되는 부분중 중간에 내용추가 요청
		// 현재는 현금만 합산이 되게 부탁드립니다. 내년1월이후 부터는 카드도입하지만 해당 내용은 현금을 관리자에게 납부하기 위한 용이어서요
		
		String nCashCnt = "0";
		String nCashAmt = "0";
		nCashCnt = String.valueOf(Integer.valueOf(item.sumCashCnt.replaceAll(",", ""))-Integer.valueOf(item.MH_CASH_CNT.replaceAll(",", "")));
		nCashAmt = String.valueOf(Integer.valueOf(item.sumCashAmt.replaceAll(",", "")) -Integer.valueOf(item.MH_CASH_AMT.replaceAll(",", "")));
						
		
		String sumCashCnt = item.sumCashCnt.replaceAll(",", "");
		String sumCashAmt = item.sumCashAmt.replaceAll(",", "");
		
		String ticketCashCnt = item.ticketCashCnt.replaceAll(",", "");
		String ticketCashAmt = item.ticketCashAmt.replaceAll(",", "");
		
		//Log.d("111", item.yetCnt.replaceAll(",", "") + "~"+ item.MH_CARD_CNT + "~" + item.MH_CASH_CNT);
		//Log.d("111", item.yetAmt.replaceAll(",", "") + "~"+ item.MH_CARD_AMT + "~" + item.MH_CASH_AMT);
		String yetCnt = "0";
		String yetAmt = "0";
		yetCnt = String.valueOf(Integer.valueOf(item.yetCnt.replaceAll(",", ""))-Integer.valueOf(item.MH_CASH_CNT_TODAY.replaceAll(",", ""))-Integer.valueOf(item.MH_CARD_CNT_TODAY.replaceAll(",", "")));
		yetAmt = String.valueOf(Integer.valueOf(item.yetAmt.replaceAll(",", "")) -Integer.valueOf(item.MH_CASH_AMT_TODAY.replaceAll(",", ""))-Integer.valueOf(item.MH_CARD_AMT_TODAY.replaceAll(",", "")));
		
		int freeCountSum = Integer.parseInt(item.freeCountSum.replaceAll(",", ""));
		//총계 = 일일주차(현금) + 월정기(현금)
		int totalCashCnt = Integer.parseInt(sumCashCnt)+Integer.parseInt(ticketCashCnt);
		int totalCashAmt = Integer.parseInt(sumCashAmt)+Integer.parseInt(ticketCashAmt);
		int chargedCount = Integer.parseInt(item.sumCashCnt.replaceAll(",", ""))+Integer.parseInt(item.sumCardCnt.replaceAll(",", ""));// - freeCountSum;
		int TOP_day_cash_cnt = Integer.parseInt(item.sumCashCnt.replaceAll(",", "")) + Integer.parseInt(item.MH_CASH_CNT.replaceAll(",", "")) + Integer.parseInt(item.freeCountSum.replaceAll(",", ""));
		int Top_day_card_cnt = Integer.parseInt(item.sumCardCnt.replaceAll(",", ""))+  Integer.parseInt(item.MH_CARD_CNT.replaceAll(",", ""));
		int Top_day_cash_amt = Integer.parseInt(item.sumCashAmt.replaceAll(",", "")) + Integer.parseInt(item.MH_CASH_AMT.replaceAll(",", ""));
		int Top_day_card_amt = Integer.parseInt(item.sumCardAmt.replaceAll(",", ""))+  Integer.parseInt(item.MH_CARD_AMT.replaceAll(",", ""));
		int Top_total_cash_cnt = TOP_day_cash_cnt +Integer.parseInt(item.ticketCashCnt.replaceAll(",", ""));
		int Top_total_card_cnt = Top_day_card_cnt +Integer.parseInt(item.ticketCardCnt.replaceAll(",", ""));
		int Top_total_cash_amt = Top_day_cash_amt +Integer.parseInt(item.ticketCashAmt.replaceAll(",", ""));
		int Top_total_card_amt = Top_day_card_amt +Integer.parseInt(item.ticketCardAmt.replaceAll(",", ""));
						
		bf.append(lSize,0,lSize.length);


		replaceByteDate(bf,"================================\n");
		replaceByteDate(bf,"총    계   - " + getEmpty(5, ALIGN_CENTER, String.format("%,d", Top_total_cash_cnt)+"건") + "-" + getEmpty(9, ALIGN_RIGHT, String.format("%,d", Top_total_cash_amt)+"원") + "\n");
		replaceByteDate(bf,"일일주차   - " + getEmpty(5, ALIGN_CENTER, Util.addComma(TOP_day_cash_cnt)+"건") + "-" + getEmpty(9, ALIGN_RIGHT, String.format("%,d", Top_day_cash_amt)+"원") + "\n");
		replaceByteDate(bf,"월 정 기   - " + getEmpty(5, ALIGN_CENTER, Util.addComma(Integer.parseInt(item.ticketCashCnt.replaceAll(",", "")))+"건") + "-" + getEmpty(9, ALIGN_RIGHT, Util.addComma(Integer.parseInt(item.ticketCashAmt.replaceAll(",", "")))+"원") + "\n");
		replaceByteDate(bf,"미    수   - " + getEmpty(5, ALIGN_CENTER, yetCnt+"건") + "-" + getEmpty(9, ALIGN_RIGHT, Util.addComma(yetAmt)+"원") + "\n");
		replaceByteDate(bf,"유료   - " + getEmpty(6, ALIGN_CENTER, chargedCount+"건") + "무료  - " + getEmpty(5, ALIGN_RIGHT, item.freeCountSum+"건") + "\n");
		replaceByteDate(bf,"================================\n");
		replaceByteDate(bf,"총    계   - " + getEmpty(5, ALIGN_CENTER, String.format("%,d", Top_total_card_cnt)+"건") + "-" + getEmpty(9, ALIGN_RIGHT, String.format("%,d", Top_total_card_amt)+"원") + "\n");
		replaceByteDate(bf,"일일주차   - " + getEmpty(5, ALIGN_CENTER, Util.addComma(Top_day_card_cnt)+"건") + "-" + getEmpty(9, ALIGN_RIGHT, String.format("%,d", Top_day_card_amt)+"원") + "\n");
		replaceByteDate(bf,"월 정 기   - " + getEmpty(5, ALIGN_CENTER, Util.addComma(Integer.parseInt(item.ticketCardCnt.replaceAll(",", "")))+"건") + "-" + getEmpty(9, ALIGN_RIGHT, Util.addComma(Integer.parseInt(item.ticketCardAmt.replaceAll(",", "")))+"원") + "\n");
		replaceByteDate(bf,"================================\n");			
		bf.append(nSize,0,nSize.length);
		replaceByteDate(bf,"   입금현황     건수   입금금액\n");
		replaceByteDate(bf,"--------------------------------\n");
		replaceByteDate(bf,"시간주차금액   -" + getEmpty(5, ALIGN_CENTER, item.timeCnt) + "-" + getEmpty(9, ALIGN_RIGHT, item.timeAmt) + "\n");
		bf.append(nSize,0,nSize.length);
		replaceByteDate(bf,"미수환수(현금) -" + getEmpty(5, ALIGN_CENTER, item.MH_CASH_CNT)  + "-" + getEmpty(9, ALIGN_RIGHT, item.MH_CASH_AMT)  + "\n");
		bf.append(nSize,0,nSize.length);
		replaceByteDate(bf,"미수환수(카드) -" + getEmpty(5, ALIGN_CENTER, item.MH_CARD_CNT)  + "-" + getEmpty(9, ALIGN_RIGHT, item.MH_CARD_AMT)  + "\n");
		bf.append(nSize,0,nSize.length);
		replaceByteDate(bf,"정기권(현금)   -" + getEmpty(5, ALIGN_CENTER, item.ticketCashCnt) + "-" + getEmpty(9, ALIGN_RIGHT, item.ticketCashAmt) + "\n");
		bf.append(nSize,0,nSize.length);
		replaceByteDate(bf,"정기권(카드)   -" + getEmpty(5, ALIGN_CENTER, item.ticketCardCnt) + "-" + getEmpty(9, ALIGN_RIGHT, item.ticketCardAmt) + "\n");
		bf.append(nSize,0,nSize.length);
		replaceByteDate(bf,"시간권(현금)   -" + getEmpty(5, ALIGN_CENTER, item.sumCashCnt)    + "-" + getEmpty(9, ALIGN_RIGHT, item.sumCashAmt) + "\n");
		bf.append(nSize,0,nSize.length);
		replaceByteDate(bf,"시간권(카드)   -" + getEmpty(5, ALIGN_CENTER, item.sumCardCnt) 	  + "-" + getEmpty(9, ALIGN_RIGHT, item.sumCardAmt) + "\n");
		replaceByteDate(bf,"쿠폰금액       -"   + getEmpty(5, ALIGN_CENTER, item.totalCouponCnt)+ "-" + getEmpty(9, ALIGN_RIGHT, item.totalCouponAmt) + "\n");
		replaceByteDate(bf,"미수금액       -"   + getEmpty(5, ALIGN_CENTER, yetCnt) 		  + "-" + getEmpty(9, ALIGN_RIGHT, Util.addComma(yetAmt)) + "\n");
		replaceByteDate(bf,"환불금액       -"   + getEmpty(5, ALIGN_CENTER, item.returnCnt) 	  + "-" + getEmpty(9, ALIGN_RIGHT, item.returnAmt) + "\n");
		replaceByteDate(bf,"--------------------------------\n");
		replaceByteDate(bf,"총 계\n");
		replaceByteDate(bf,"총수입금액     -" + getEmpty(5, ALIGN_CENTER, item.totalCnt) 	   + "-" + getEmpty(9, ALIGN_RIGHT, item.totalAmt) + "\n");
		replaceByteDate(bf,"총카드금액     -" + getEmpty(5, ALIGN_CENTER, item.totalCardCnt) + "-" + getEmpty(9, ALIGN_RIGHT, String.format("%,d", Integer.parseInt(item.totalCardAmt))) + "\n");
		replaceByteDate(bf,"총현금금액     -" + getEmpty(5, ALIGN_CENTER, item.totalCashCnt) + "-" + getEmpty(9, ALIGN_RIGHT, item.totalCashAmt) + "\n");
		replaceByteDate(bf,"--------------------------------\n");
		
		// 미납발생 차량 출력항목
		if(item.yetList != null && item.yetList.size() > 0){
			replaceByteDate(bf,"<미납발생 차량목록>\n");
			for(ParkCloseStatusYet info : item.yetList){
				replaceByteDate(bf,info.getCarNo()+getEmpty(6, ALIGN_LEFT, "")+getEmpty(9, ALIGN_RIGHT, info.getYetAmt())+"원"+"\n");
				replaceByteDate(bf,info.getParkTime()+getEmpty(6, ALIGN_LEFT, "")+getEmpty(12, ALIGN_CENTER, info.getKnDiscd())+"\n");
			}
			replaceByteDate(bf,"--------------------------------\n");
		}
		
		// 미수금납부 차량 출력항목
		if(item.returnList != null && item.returnList.size() > 0){
			replaceByteDate(bf,"<미수금납부 차량목록>\n");
			for(ParkCloseStatusReturn info : item.returnList){
				replaceByteDate(bf,info.getCarNo()+getEmpty(6, ALIGN_LEFT, "")+getEmpty(9, ALIGN_RIGHT, info.getReturnAmt())+"원"+"\n");
				replaceByteDate(bf,info.getReturnDate()+"\n");
				replaceByteDate(bf,info.getKnBillType()+"\n");
			}
			replaceByteDate(bf,"--------------------------------\n");
		}
		
		// 정기권 차량 출력항목
		if(item.ticketList != null && item.ticketList.size() > 0){
			replaceByteDate(bf,"<월정기 차량목록>\n");
			for(ParkCloseStatusTicket info : item.ticketList){
				replaceByteDate(bf,info.getCarNo()+getEmpty(6, ALIGN_LEFT, "")+getEmpty(9, ALIGN_RIGHT, info.getPayAmt())+"원"+"\n");
				replaceByteDate(bf,info.getTicketDate()+"\n");
			}
			replaceByteDate(bf,"--------------------------------\n");
		}
		
		if(item.CouponList != null && item.CouponList.size() > 0){
			int hit  = 0;
			int amt = 0;
			
			replaceByteDate(bf,"<쿠폰 종류별>\n");
			bf.append(nSize,0,nSize.length);
			replaceByteDate(bf,"  쿠폰명칭      장수   쿠폰요금\n");
			replaceByteDate(bf,"--------------------------------\n");
			for(ParkCloseStatusDiscountList info : item.CouponList){
				replaceByteDate(bf, getEmpty(10, ALIGN_CENTER, info.getKN_DIS_CD())+"-"+ getEmpty(5, ALIGN_CENTER, Util.addComma(info.getCNT()))+"-" + getEmpty(9, ALIGN_CENTER, Util.addComma(info.getPARK_INCOME())) + "\n");
			}
			replaceByteDate(bf,"--------------------------------\n");
		}
		
		return bf;
	}
	

}
