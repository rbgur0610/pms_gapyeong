package com.pms.gapyeong.vo;

import java.io.Serializable;

public class StatusBoard implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2917009578920842477L;
	
	ParkIO parkIO;
	
	public ParkIO getParkIO() {
		return parkIO;
	}
	
	public boolean isEmpty() {
		return (parkIO == null);
	}
	
	public void setParkIO(ParkIO io) {
		this.parkIO = io;
	}
}
