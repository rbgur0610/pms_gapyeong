package com.pms.gapyeong.vo;

public class PictureInfo {
	
	private String id;
	private String pioNum;
	private String path;
	private String cdPark;
	private String carNo;
	private String pictureType;
	private String date;
	
	private boolean isChecked;
	
	public PictureInfo() {}
	
	public PictureInfo(String id, String pioNum, String path, String cdPark, String carNo, String type, String date) {
		this.id  	= id;
		this.pioNum = pioNum;
		this.path 	= path;
		this.cdPark = cdPark;
		this.carNo 	= carNo;
		this.pictureType = type;
		this.date 	= date;
	}
	
	@Override
	public String toString() {
		return 	  "  id : " + id
				+ ", pioNum : " + pioNum
				+ ", path : " + path
				+ ", cdPark : " + cdPark
				+ ", carNo : " + carNo
				+ ", pictureType : " + pictureType
				+ ", date : " + date
				+ ", isChecked : " + isChecked;
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public String getPioNum() {
		return pioNum;
	}

	public void setPioNum(String pioNum) {
		this.pioNum = pioNum;
	}
	
	public String getPath() {
		return path;
	}
	
	public void setPath(String path) {
		this.path = path;
	}
	
	public String getCdPark() {
		return cdPark;
	}

	public void setCdPark(String cdPark) {
		this.cdPark = cdPark;
	}

	public String getCarNo() {
		return carNo;
	}
	
	public void setCarNo(String carNo) {
		this.carNo = carNo;
	}
	
	public String getPictureType() {
		return pictureType;
	}
	
	public void setPictureType(String pictureType) {
		this.pictureType = pictureType;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}
	
	public boolean isChecked() {
		return isChecked;
	}

	public void setChecked(boolean isChecked) {
		this.isChecked = isChecked;
	}	

}
