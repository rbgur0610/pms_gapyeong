package com.pms.gapyeong.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.pms.gapyeong.R;
import com.pms.gapyeong.common.Constants;
import com.pms.gapyeong.common.ImageLoader;
import com.pms.gapyeong.common.Util;

public class CarPictureViewDetailActivity extends Activity {

	
	private ImageView iv_image;
	private String imageURL = "";
	
	private String TAG = this.getClass().getSimpleName();
	
	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.car_picture_view_detail_activity);
		
		Intent intent = getIntent();
		if (intent != null) {
			imageURL = Util.isNVL(intent.getStringExtra("imageURL"));
			Log.d(TAG, " viewDetail ::: imageURL >>> " + imageURL);
		}
	
		iv_image = (ImageView) findViewById(R.id.iv_image);
		iv_image.setOnClickListener(new View.OnClickListener() {
        	@Override
        	public void onClick(View v) {
        		finish();
        	}
        });

		/***
		File imgFile = new File(imagePath);
		
		if(imgFile.exists()){			
			ImageLoader.setImageView(imagePath,iv_image);
		} else{
			MsgUtil.ToastMessage(this, "파일이 존재하지 않습니다.");
		}
		***/
		
		if(!Util.isEmpty(imageURL)){
			ImageLoader.setImageUrlView(this, Constants.SERVER_HOST + imageURL, iv_image);
		}

	}
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}
	
}
