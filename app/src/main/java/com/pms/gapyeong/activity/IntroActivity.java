package com.pms.gapyeong.activity;

import org.json.JSONException;
import org.json.JSONObject;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.pms.gapyeong.BuildConfig;
import com.pms.gapyeong.R;
import com.pms.gapyeong.common.Constants;
import com.pms.gapyeong.common.MsgUtil;
import com.pms.gapyeong.common.Util;
import com.pms.gapyeong.database.CodeHelper;
import com.pms.gapyeong.utils.RootingCheck;

import java.util.List;

public class IntroActivity extends BaseActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        changeStatusBarColor("#2BBDFF");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.intro_layout);

        if (Constants.SERVER_HOST.equals("http://gccs.dev.parkingstyle.co.kr:6211") && Constants.isDevMode) {
            MsgUtil.ToastMessage(this, "개발서버로 접속되어있습니다.", Toast.LENGTH_LONG);
        }

        CodeHelper db = CodeHelper.getInstance(this);
        if (db.isExist()) {
            db.dropTable();
        }


        if (!BuildConfig.DEBUG && RootingCheck.chkRooting(this)) {
            // 얼럿창 띄운 후 앱 종료
            showAlert(this, "본 기기는 OS 변조(탈옥)된 기기로 판명되어 안정성을 보장할 수 없기에 종료됩니다.", "확인", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            }, null, null);
            return;
        }


        TedPermission.with(this)
                .setPermissionListener(permissionlistener)
                .setDeniedMessage("권한 설정을 해주세요. 권한설정을 거절한 경우 어플리케이션 설정->권한에서 직접 설정하실수 있습니다.")
                .setPermissions(Manifest.permission.READ_SMS, Manifest.permission.CAMERA, Manifest.permission.READ_PHONE_STATE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.ACCESS_COARSE_LOCATION)
                .check();


    }


    public static void showAlert(Context mCon, String msg, String positiveBtn, DialogInterface.OnClickListener positiveBtnListener, String negativeBtn,
                                 DialogInterface.OnClickListener negativeBtnListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mCon);
        builder
                .setTitle(R.string.app_name)
                .setMessage(msg)
                .setCancelable(false)
                .setPositiveButton(positiveBtn, positiveBtnListener);
        if (negativeBtn != null) {
            builder.setNegativeButton(negativeBtn, negativeBtnListener);
        }
        android.app.AlertDialog dialog = builder.create();    // 알림창 객체 생성
        dialog.show();    // 알림창 띄우기

    }

    PermissionListener permissionlistener = new PermissionListener() {
        @Override
        public void onPermissionGranted() {
            apiCodeClassCall();
        }

        @Override
        public void onPermissionDenied(List<String> deniedPermissions) {
            Toast.makeText(IntroActivity.this, "권한을 승인해주세요.\n", Toast.LENGTH_SHORT).show();
        }
    };


    private void goLoginActivity() {
        startActivity(new Intent(this, LoginActivity.class));
        finish();
    }

    /**
     * 공통 코드 조회
     */
    private void apiCodeClassCall() {
        showProgressDialog(false);
        String url = Constants.API_SERVER + "?" + Constants.METHOD + "=" + Constants.INTERFACEID_COMMONCODE;
        String param = "&CD_CLASS=" + Util.getEncodeStr("%")
                + "&" + Constants.API_FORMAT + "=" + Constants.JSON_FORMAT;
        Log.d("111", " apiCodeClassCall url >>> " + url + param);
        AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>();
        cb.url(url + param).type(JSONObject.class).weakHandler(this, "codeClassCallback").redirect(true).retry(3).fileCache(false).expire(-1);
        AQuery aq = new AQuery(this);
        aq.ajax(cb);
    }

    public void codeClassCallback(String url, JSONObject json, AjaxStatus status) {

        closeProgressDialog();
        Log.d(TAG, " codeClassCallback  url ====== " + url + "   json  : " + json.toString());

        CodeHelper db = CodeHelper.getInstance(this);


        // successful ajax call
        if (json != null) {
            String KN_RESULT = json.optString("KN_RESULT");
            if ("1".equals(json.optString("CD_RESULT"))) {
                codeJsonArr = json.optJSONArray("RESULT");

                int Len = codeJsonArr.length();
                try {

                    for (int i = 0; i < Len; i++) {
                        JSONObject res = codeJsonArr.getJSONObject(i);
                        String CD_CLASS = res.optString("CD_CLASS");
                        String KN_CLASS = res.optString("KN_CLASS");
                        String CD_CODE = res.optString("CD_CODE");
                        String KN_CODE = res.optString("KN_CODE");
                        String CD_VALUE = res.optString("CD_VALUE");
                        String CD_VALUE2 = res.optString("CD_VALUE2");
                        String CD_VALUE3 = res.optString("CD_VALUE3");
                        String DIS_RATE = res.optString("DIS_RATE");
                        String DIS_AMT = res.optString("DIS_AMT");
                        String DIS_TIME = res.optString("DIS_TIME");
                        db.insert(CD_CLASS, KN_CLASS, CD_CODE, KN_CODE, CD_VALUE, CD_VALUE2, CD_VALUE3, DIS_RATE, DIS_AMT, DIS_TIME);
                    } // end for

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    if (Constants.DEBUG_PRINT_LOG) {
                        e.printStackTrace();
                    } else {
                        System.out.println("JSONException 예외 발생");
                    }
                }
            } else {
                MsgUtil.ToastMessage(this, KN_RESULT, Toast.LENGTH_LONG);
            }
        } else {
            MsgUtil.ToastMessage(this, Constants.DATA_FAIL, Toast.LENGTH_LONG);
        }

        goLoginActivity();

    }

}
