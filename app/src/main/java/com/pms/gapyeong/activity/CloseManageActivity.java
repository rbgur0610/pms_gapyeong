package com.pms.gapyeong.activity;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import android.bluetooth.BluetoothDevice;
import android.util.Log;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

import com.pms.gapyeong.adapter.CouponAdapter;
import com.pms.gapyeong.pay.PayVo;
import com.pms.gapyeong.vo.ParkCloseStatusDiscountList;
import com.pms.gapyeong.R;
import com.pms.gapyeong.adapter.ImageLoaderAdapter;
import com.pms.gapyeong.common.CalendarInfo;
import com.pms.gapyeong.common.Constants;
import com.pms.gapyeong.common.DatePickerMonth;
import com.pms.gapyeong.common.DeviceInfo;
import com.pms.gapyeong.common.FileUtil;
import com.pms.gapyeong.common.ImageLoader;
import com.pms.gapyeong.common.MsgUtil;
import com.pms.gapyeong.common.Preferences;
import com.pms.gapyeong.common.Util;
import com.pms.gapyeong.database.CodeHelper;
import com.pms.gapyeong.database.PictureHelper;
import com.pms.gapyeong.pay.PayData;
import com.pms.gapyeong.vo.ParkCloseStatusItem;
import com.pms.gapyeong.vo.ParkCloseStatusReturn;
import com.pms.gapyeong.vo.ParkCloseStatusTicket;
import com.pms.gapyeong.vo.ParkCloseStatusYet;
import com.pms.gapyeong.vo.PictureInfo;
import com.pms.gapyeong.vo.ReceiptType12Vo;
import com.pms.gapyeong.vo.ReceiptType13Vo;

public class CloseManageActivity extends BaseActivity implements OnItemClickListener, android.widget.CompoundButton.OnCheckedChangeListener {

    static final int RESULT_CODE_PICTURE = 1;

    private Button btn_top_00;
    private Button btn_top_01;
    private Button btn_top_02;
    private Button btn_top_03;
    private FrameLayout frame_body;
    private LinearLayout close_manager_view;
    private LinearLayout picture_manager_view;
    private LinearLayout setting_view;
    private LinearLayout coupon_view;
    // Close
    TextView inputTime;
    TextView inputTimePrice;
    TextView inputCommutationTicket;
    TextView inputCommutationTicketPrice;
    TextView inputUnpaidBalance;
    TextView inputUnpaidBalancePrice;
    TextView inputTotalCash;
    TextView inputTotalCashPrice;
    TextView inputTotalCard;
    TextView inputTotalCardPrice;
    TextView inputPrepay;
    TextView inputPrepayPrice;
    TextView createUnpaidBalance;
    TextView createUnpaidBalancePrice;
    TextView createRefund;
    TextView createRefundPrice;
    TextView closeStart;
    ParkCloseStatusItem closeStatusItem;

    // Picture
    private TextView tvDate;
    private TextView et_car_number;
    private TextView tvKindPicuture;
    private ImageLoaderAdapter pictureAdapter;
    private ListView pictureListView;
    private Button btnDeletePic;
    private ImageLoader loader = new ImageLoader();

    // Setting View
    int boardSize;
    boolean isInputPicture;
    boolean isExitPicture;
    boolean isRegisterCommutationTicket;
    boolean isDeleteAppImg;
    boolean isDeleteIoImg;
    boolean isDeleteServerImg;

    private View btn_top_left;

    private TextView tv_top_title;

    private Spinner spSearchOption;

    private String search_date;

    ReceiptType12Vo type12Vo;

    private Button btn_close;
    private Button btn_close_auto;
    private Button btn_daily_report;

    private InputMethodManager ipm;
    private boolean closeButtonStatus = false;
    private String couponData;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.close_layout);

        boardSize = Preferences.getValue(this, Constants.PREFERENCE_SETTING_STATUS_BOARD_SIZE, Constants.SETTING_STATUS_BOARD_SIZE_15);
        isInputPicture = Preferences.getValue(this, Constants.PREFERENCE_SETTING_INPUT_PICTURE, false);
        isExitPicture = Preferences.getValue(this, Constants.PREFERENCE_SETTING_EXIT_PICTURE, false);
        isRegisterCommutationTicket = Preferences.putValue(this, Constants.PREFERENCE_SETTING_REGISTER_COMMUTATION_TICKET, false);
        isDeleteAppImg = Preferences.getValue(this, Constants.PREFERENCE_SETTING_DELETE_APP_IMG, false);
        isDeleteIoImg = Preferences.getValue(this, Constants.PREFERENCE_SETTING_DELETE_IO_IMG, false);
        isDeleteServerImg = Preferences.getValue(this, Constants.PREFERENCE_SETTING_DELETE_SERVER_IMG, false);
    }

    @Override
    protected void initLayoutSetting() {
        super.initLayoutSetting();

        tv_top_title = (TextView) findViewById(R.id.tv_top_title);
        tv_top_title.setText("마감관리");

        btn_top_left = (Button) findViewById(R.id.btn_top_left);
        btn_top_left.setVisibility(View.VISIBLE);
        btn_top_left.setOnClickListener(this);

        frame_body = (FrameLayout) findViewById(R.id.frame_body);

        btn_top_00 = (Button) findViewById(R.id.btn_top_00);
        btn_top_01 = (Button) findViewById(R.id.btn_top_01);
        btn_top_02 = (Button) findViewById(R.id.btn_top_02);
        btn_top_03 = (Button) findViewById(R.id.btn_top_03);


        btn_top_00.setOnClickListener(this);
        btn_top_01.setOnClickListener(this);
        btn_top_02.setOnClickListener(this);
        btn_top_03.setOnClickListener(this);


        close_manager_view = (LinearLayout) View.inflate(this, R.layout.close_manager_view, null);
        picture_manager_view = (LinearLayout) View.inflate(this, R.layout.picture_manager_view, null);
        setting_view = (LinearLayout) View.inflate(this, R.layout.setting_view, null);
        coupon_view = (LinearLayout) View.inflate(this, R.layout.dialog_coupon, null);


        frame_body.addView(close_manager_view);
        frame_body.addView(picture_manager_view);
        frame_body.addView(setting_view);
        frame_body.addView(coupon_view);


        initTopTapStatus(btn_top_00);

        close_manager_view.setVisibility(View.VISIBLE);

        initClosedLayoutView();
        initPictureLayoutView();
        initSettingLayoutView();
        initCouponLayoutView();
    }

    private void initClosedLayoutView() {
        inputTime = (TextView) findViewById(R.id.input_time);
        inputTimePrice = (TextView) findViewById(R.id.input_time_price);
        inputCommutationTicket = (TextView) findViewById(R.id.input_commutation_ticket);
        inputCommutationTicketPrice = (TextView) findViewById(R.id.input_commutation_ticket_price);
        inputUnpaidBalance = (TextView) findViewById(R.id.input_unpaid_balance);
        inputUnpaidBalancePrice = (TextView) findViewById(R.id.input_unpaid_balance_price);
        inputTotalCash = (TextView) findViewById(R.id.input_total_cash);
        inputTotalCashPrice = (TextView) findViewById(R.id.input_total_cash_price);
        inputTotalCard = (TextView) findViewById(R.id.input_total_card);
        inputTotalCardPrice = (TextView) findViewById(R.id.input_total_card_price);
        inputPrepay = (TextView) findViewById(R.id.input_prepay);
        inputPrepayPrice = (TextView) findViewById(R.id.input_prepay_price);
        createUnpaidBalance = (TextView) findViewById(R.id.create_unpaid_balance);
        createUnpaidBalancePrice = (TextView) findViewById(R.id.create_unpaid_balance_price);
        createRefund = (TextView) findViewById(R.id.create_refund);
        createRefundPrice = (TextView) findViewById(R.id.create_refund_price);
        spSearchOption = (Spinner) findViewById(R.id.spSearchOption);

        btn_close = (Button) findViewById(R.id.btn_close);
        btn_close_auto = (Button) findViewById(R.id.btn_close_auto);
        btn_daily_report = (Button) findViewById(R.id.daily_report);
        inputTime.setText("0건");
        inputTimePrice.setText("0원");
        inputCommutationTicket.setText("0건");
        inputCommutationTicketPrice.setText("0원");
        inputUnpaidBalance.setText("0건");
        inputUnpaidBalancePrice.setText("0원");
        inputTotalCash.setText("0건");
        inputTotalCashPrice.setText("0원");
        inputTotalCard.setText("0건");
        inputTotalCardPrice.setText("0원");
        inputPrepay.setText("0건");
        inputPrepayPrice.setText("0원");
        createUnpaidBalance.setText("0건");
        createUnpaidBalancePrice.setText("0원");
        createRefund.setText("0건");
        createRefundPrice.setText("0원");

        closeStart = (TextView) findViewById(R.id.close_start);

        search_date = today.substring(0, 4) + "-" + today.substring(4, 6) + "-" + today.substring(6);
        closeStart.setText(today.substring(0, 4) + "-" + today.substring(4, 6) + "-" + today.substring(6));

        ArrayAdapter<String> searchOptionAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, new String[]{"일일조회", "월별조회"});
        searchOptionAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spSearchOption.setAdapter(searchOptionAdapter);
        spSearchOption.setOnItemSelectedListener(dialogSelectedListener);
    }

    private boolean isSearchOption;
    OnItemSelectedListener dialogSelectedListener = new OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {
            isSearchOption = position == 0 ? true : false;
            if (isSearchOption) {
                closeStart.setText(search_date);
                apiParkCloseStatusDayCall(search_date.replaceAll("-", ""));

                ((Button) findViewById(R.id.daily_report)).setEnabled(true);
                ((Button) findViewById(R.id.monthly_report)).setEnabled(false);

            } else {
                closeStart.setText(search_date.substring(0, 7));
                apiParkCloseStatusMonCall(search_date.replaceAll("-", "").substring(0, 6));

                ((Button) findViewById(R.id.daily_report)).setEnabled(false);
                ((Button) findViewById(R.id.monthly_report)).setEnabled(true);
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> arg0) {

        }
    };

    private void initPictureLayoutView() {
        tvDate = (TextView) findViewById(R.id.date);
        et_car_number = (TextView) findViewById(R.id.et_car_number);
        tvKindPicuture = (TextView) findViewById(R.id.kind_picture);
        pictureListView = (ListView) findViewById(R.id.list_picture);

        btnDeletePic = (Button) findViewById(R.id.delete_pic);
        btnDeletePic.setEnabled(false);

        pictureListView.setOnItemClickListener(this);
        tvDate.setText(String.format("%d-%02d-%02d"
                , calendar.get(Calendar.YEAR)
                , (calendar.get(Calendar.MONTH) + 1)
                , calendar.get(Calendar.DAY_OF_MONTH)));

        setCommonItem("DR", Constants.COMMON_TYPE_PICTURE_TYPE, tvKindPicuture, 2);

        ipm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

        et_car_number.setOnEditorActionListener(new OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                // TODO Auto-generated method stub
                switch (actionId) {
                    case EditorInfo.IME_ACTION_SEARCH:
                        loadPicture();
                        break;
                    default:
                        return false;
                }
                return false;
            }
        });

    }

    private void initSettingLayoutView() {
        ((RadioGroup) findViewById(R.id.status_board_size)).setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                Log.i(TAG, "checkedId : " + checkedId);
                switch (checkedId) {
                    case R.id.cell_9:
                        boardSize = Constants.SETTING_STATUS_BOARD_SIZE_9;
                        break;
                    case R.id.cell_12:
                        boardSize = Constants.SETTING_STATUS_BOARD_SIZE_12;
                        break;
                    case R.id.cell_15:
                        boardSize = Constants.SETTING_STATUS_BOARD_SIZE_15;
                        break;
                    case R.id.cell_18:
                        boardSize = Constants.SETTING_STATUS_BOARD_SIZE_18;
                        break;
                    case R.id.cell_21:
                        boardSize = Constants.SETTING_STATUS_BOARD_SIZE_21;
                        break;
                    case R.id.cell_24:
                        boardSize = Constants.SETTING_STATUS_BOARD_SIZE_24;
                        break;
                }
            }
        });

        CheckBox delete_app_img = (CheckBox) findViewById(R.id.delete_app_img);
        CheckBox delete_io_img = (CheckBox) findViewById(R.id.delete_io_img);
        CheckBox delete_server_img = (CheckBox) findViewById(R.id.delete_server_img);

        delete_app_img.setOnCheckedChangeListener(checkBoxListener);
        delete_io_img.setOnCheckedChangeListener(checkBoxListener);
        delete_server_img.setOnCheckedChangeListener(checkBoxListener);

        switch (boardSize) {
            case Constants.SETTING_STATUS_BOARD_SIZE_9:
                ((RadioButton) findViewById(R.id.cell_9)).setChecked(true);
                break;
            case Constants.SETTING_STATUS_BOARD_SIZE_12:
                ((RadioButton) findViewById(R.id.cell_12)).setChecked(true);
                break;
            case Constants.SETTING_STATUS_BOARD_SIZE_15:
                ((RadioButton) findViewById(R.id.cell_15)).setChecked(true);
                break;
            case Constants.SETTING_STATUS_BOARD_SIZE_18:
                ((RadioButton) findViewById(R.id.cell_18)).setChecked(true);
                break;
            case Constants.SETTING_STATUS_BOARD_SIZE_21:
                ((RadioButton) findViewById(R.id.cell_21)).setChecked(true);
                break;
            case Constants.SETTING_STATUS_BOARD_SIZE_24:
                ((RadioButton) findViewById(R.id.cell_24)).setChecked(true);
                break;
        }

        delete_app_img.setChecked(isDeleteAppImg);
        //delete_io_img.setChecked(isDeleteIoImg);
        //delete_server_img.setChecked(isDeleteServerImg);
        delete_io_img.setEnabled(false);
        delete_server_img.setEnabled(false);
    }

    CompoundButton.OnCheckedChangeListener checkBoxListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            Log.d(TAG, " isChecked >>> " + isChecked);
            switch (buttonView.getId()) {
                case R.id.delete_app_img:
                    isDeleteAppImg = isChecked;
                    Log.d(TAG, " isDeleteAppImg >>> " + isDeleteAppImg);
                    break;
                case R.id.delete_io_img:
                    isDeleteIoImg = isChecked;
                    break;
                case R.id.delete_server_img:
                    isDeleteServerImg = isChecked;
                    break;
            }
        }
    };

    private void initTopTapStatus(View v) {
        btn_top_00.setSelected(false);
        btn_top_01.setSelected(false);
        btn_top_02.setSelected(false);
        btn_top_03.setSelected(false);

        v.setSelected(true);

        close_manager_view.setVisibility(View.GONE);
        picture_manager_view.setVisibility(View.GONE);
        setting_view.setVisibility(View.GONE);
        coupon_view.setVisibility(View.GONE);
    }

    private String pioNum;
    private String parkAmt;

    private void initCouponLayoutView() {

        if (codeJsonArr == null)
            return;

        Button btn_ok = (Button) findViewById(R.id.btn_ok);
        Button btn_cancel = (Button) findViewById(R.id.btn_cancel);
        //btn_coupon_qty   = (Button) findViewById(R.id.btn_coupon_qty);
        final TextView tvTotalAmt = (TextView) findViewById(R.id.tvTotalAmt);
        ListView Lv_dialog = (ListView) findViewById(R.id.Lv_dialog);

        ArrayList<JSONObject> arrayList = new ArrayList<JSONObject>();
        int Len = codeJsonArr.length();

        for (int i = 0; i < Len; i++) {
            try {
                JSONObject res = codeJsonArr.getJSONObject(i);
                if ("T2".equals(res.optString("CD_CLASS"))) {
                    //   Log.d(TAG, " CD_CODE >>>> " + res.optString("CD_CODE") +  " KN_CODE >>>> " + res.optString("KN_CODE"));
                    arrayList.add(res);
                }
            } catch (JSONException e) {
                if(Constants.DEBUG_PRINT_LOG){
                    e.printStackTrace();
                }else{
                    System.out.println("예외 발생");
                };
            }
        }  // end for


        final CouponAdapter couponAdapter = new CouponAdapter(this, tvTotalAmt, arrayList, "TEXT");
        Lv_dialog.setAdapter(couponAdapter);

        btn_ok.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                /**XXX 이전버전 2015.10.31 작업
                 if(orgPayment < Util.sToi(tvTotalAmt.getText().toString().replace("원","")))
                 {
                 MsgUtil.ToastMessage(BaseActivity.this, "쿠폰금액이 결제금액보다 큽니다.");
                 return;
                 }
                 **/
                pioNum = makeCouponNumber(parkCode, empCode, today);
                int charge = Integer.parseInt(tvTotalAmt.getText().toString().replace(",", "").replace("원", ""));

                if (charge == 0) {
                    MsgUtil.AlertDialog(CloseManageActivity.this, "알림", "선택한 쿠폰이 없습니다.\n쿠폰을 선택 후 결제를 진행해 주세요.");
                    return;
                }

                parkAmt = "" + charge;
                ReceiptType13Vo type13Vo = new ReceiptType13Vo("쿠폰판매"        // 타이틀
                        , Util.getYmdhms("yyyy-MM-dd HH:mm:ss")    // 출력일자
                        , "0"            // 주차면
                        , "04"                                        // 주차종류
                        , "시간권(선불)"            // 요금종류
                        , "일반"        // 요금할인
                        , "0"    // 총미수금액
                        , "" + charge                                // 선납금액
                        , Util.getYmdhms("HH:mm:ss")        // 시작시간
                        , Util.getYmdhms("HH:mm:ss")        // 출차예정시간
                        , "0"    // 총 분
                        , BIZ_NO            // 사업자번호
                        , BIZ_NAME            // 사업자명
                        , BIZ_TEL, "", "");            // 사업자연락처

                type13Vo.setPioNum(pioNum);
                type13Vo.setCarNum("쿠폰판매");
                type13Vo.setParkName(parkName);
                type13Vo.setEmpName(empName);
                type13Vo.setEmpPhone(empPhone);
                type13Vo.setEmpTel(empTel);

                prepayDialog("쿠폰판매"  // 주차종류
                        , "" + charge                       // 선납금액
                        , type13Vo);


            }
        });

        btn_cancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                setToast("취소되었습니다.");
                finish();
            }
        });

    }


    @Override
    protected void HandlerListener(Message msg) {
        switch (msg.what) {
            case 0:
                PictureHelper helper = PictureHelper.getInstance(CloseManageActivity.this);
                int size = pictureAdapter.getCount();
                for (int i = 0; i < size; i++) {
                    PictureInfo item = pictureAdapter.getItem(i);
                    if (item.isChecked()) {
                        helper.delete(item.getPath());
                    }
                }
                loadPicture();
                closeProgressDialog();
                break;
        }
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        initTopTapStatus(v);
        switch (v.getId()) {
            case R.id.btn_top_left:
                finish();
                break;
            case R.id.btn_top_00:
                close_manager_view.setVisibility(View.VISIBLE);
                tv_top_title.setText("마감관리");
                break;
            case R.id.btn_top_01:
                picture_manager_view.setVisibility(View.VISIBLE);
                tv_top_title.setText("사진관리");
                loadPicture();
                break;
            case R.id.btn_top_02:
                setting_view.setVisibility(View.VISIBLE);
                tv_top_title.setText("환경설정");
                break;
            case R.id.btn_top_03:
                coupon_view.setVisibility(View.VISIBLE);
                tv_top_title.setText("쿠폰관리");
                break;
        }
    }

    /**
     * 마감관리에서의 onClick 처리
     *
     * @param v
     */
    public void onClickCloseManager(View v) {

        PayData payData = new PayData(this, OUT_PRINT, IN_PRINT, TICKET_PRINT);

        switch (v.getId()) {

            case R.id.close_start:
                if (isSearchOption) {
                    new CalendarDlg(this).show();
                } else {
                    int yy = Integer.parseInt(search_date.replaceAll("-", "").substring(0, 4));
                    int mm = Integer.parseInt(search_date.replaceAll("-", "").substring(4, 6));
                    int dd = Integer.parseInt(search_date.replaceAll("-", "").substring(6));

                    DatePickerMonth sdateDialog = new DatePickerMonth(this, yy, mm - 1, dd);
                    DatePickerMonth.onDateChangeCallBack sCallback = new DatePickerMonth.onDateChangeCallBack() {
                        @Override
                        public void onDateChangeCallBack(int year, int month, int day) {
                            // TODO Auto-generated method stub
                            search_date = String.format("%d-%02d-%02d", year, (month + 1), day);
                            closeStart.setText(search_date.substring(0, 7));
                            apiParkCloseStatusDayCall(closeStart.getText().toString().replaceAll("-", ""));
                        }
                    };
                    sdateDialog.setOndateChangeCallBack(sCallback);
                }

                break;

            case R.id.btn_close:
            case R.id.btn_close_auto:
                //WORK_DAY : 작업일
                //CD_PARK : 주차장코드
                /**XXX 이전버전 2015.11.01 작업
                 int workEndMinute = (Integer.parseInt(workEndTime.substring(0, 2)) * 60) + Integer.parseInt(workEndTime.substring(2));

                 String currentTime =Util.getYmdhms("HHmm");
                 int currentMinute = (Integer.parseInt(currentTime.substring(0, 2)) * 60) + Integer.parseInt(currentTime.substring(2));

                 Log.i(TAG, "workEndTime : " + workEndTime + ", workEndMinute : " + workEndMinute);
                 Log.i(TAG, "currentTime : " + currentTime + ", currentMinute : " + currentMinute);

                 boolean isClose = ((workEndMinute - currentMinute) < 10 ? true : false);
                 **/

                String currentTime = Util.getYmdhms("HHmm");
                Log.d(TAG, " 222 workEndTime : " + workEndTime + ", currentTime : " + currentTime);
                Log.d(TAG, " 333 workEndTime : " + Util.sToi(workEndTime) + ", currentTime : " + Util.sToi(currentTime));

                boolean isClose = false;

                if (Util.sToi(currentTime) >= Util.sToi(workEndTime)) {
                    isClose = true;
                } else {
                    isClose = false;
                }

                int cnt = 0;
			
			/*
			if( workEndTime.substring(0,2) == currentTime.substring(0, 2))
			{
				cnt = Util.sToi(workEndTime.substring(2,4)) - Util.sToi(currentTime.substring(2,4));
				
				if(cnt <=30)
				{
					isClose = true;
				}
			}
			*/

                if (!isClose) {
                    if (today.equals(search_date.replaceAll("-", ""))) {
                        twoButtonDialog(9999, "알림", "근무시간 이후 마감이 가능합니다.", false).show();
                        return;
                    }
                }
                if (v.getId() == R.id.btn_close) {  //마감 버튼 클릭 시 퇴근사진 저장 후 마감
                    closeButtonStatus = false;
//				Intent intent = new Intent(this, WorkImageActivity.class);
//				intent.putExtra("pictureType", Constants.PICTURE_TYPE_T2 );
//				startActivityForResult(intent, Constants.COMMON_TYPE_CLOSE_PICTURE);
                    apiParkIoListCall(Util.getEncodeStr("%"), "I1", search_date.replaceAll("-", ""), search_date.replaceAll("-", ""), 1);
                } else {
                    closeButtonStatus = true;
                    twoButtonDialog(1888, "알림", "자동마감을 하시겠습니까?").show();
                }
                break;

            case R.id.daily_report:
                if (closeStatusItem == null)
                    return;

                type12Vo = new ReceiptType12Vo("일일업무보고"
                        , Util.getYmdhms("yyyy-MM-dd HH:mm:ss")            // 출력일자
                        , closeStart.getText().toString()
                        , parkName        // 주차장명
                        , empName        // 주차요원
                        , BIZ_NAME
                        , BIZ_TEL
                        , closeStatusItem);

                payData.printDefault(PayData.PAY_TYPE_PARK_CLOSE_STATUS, type12Vo, mPrintService);

                payData.ImagePrint(empLogo_image);
                try {
                    payData.printText("\n\n");
                } catch (UnsupportedEncodingException e) {
                    System.out.println("UnsupportedEncodingException 예외 발생");
                }

                break;

            case R.id.monthly_report:
                if (closeStatusItem == null)
                    return;

                type12Vo = new ReceiptType12Vo("월간업무보고"
                        , Util.getYmdhms("yyyy-MM-dd HH:mm:ss")            // 출력일자
                        , closeStart.getText().toString()
                        , parkName        // 주차장명
                        , empName        // 주차요원
                        , BIZ_NAME
                        , BIZ_TEL
                        , closeStatusItem);

                payData.printDefault(PayData.PAY_TYPE_PARK_CLOSE_STATUS, type12Vo, mPrintService);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        String transactionNo = "";
        String approvalNo = "";
        String approvalDate = "";
        String cardNo = "";
        String cardCompany = "";
        String cardAmt = "";


        switch (requestCode) {
            case Constants.COMMON_TYPE_CLOSE_PICTURE:
                //사진저장 완료 후 근무마감 실행
                if (!closeButtonStatus) {
                    apiParkIoListCall(Util.getEncodeStr("%"), "I1", search_date.replaceAll("-", ""), search_date.replaceAll("-", ""), 1);
                } else {
                    apiParkIoListCall(Util.getEncodeStr("%"), "I1", search_date.replaceAll("-", ""), search_date.replaceAll("-", ""), 2);
                }
                break;
            case PayData.PAY_DIALOG_TYPE_PREPAY_CARD:
                Log.d(TAG, " requestCode >>> " + requestCode);
                try {
                    try {
                        mPrintService.stop();
                        if (mPrintService.getState() == 0) {
                            BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(mBluetoothAddress);
                            mPrintService.connect(device, true);
                        }

                        Thread.sleep(1000);
                    } catch (IllegalArgumentException e) {
                        System.out.println("IllegalArgumentException 예외 발생");
                    } catch (NullPointerException e) {
                        System.out.println("NullPointerException 예외 발생");
                    } catch (InterruptedException e) {
                        System.out.println("예외 발생");
                    }

                    transactionNo = Preferences.getValue(this, Constants.PREFERENCE_MCPAY_TRANSACTION_NO, "");
                    approvalNo = Preferences.getValue(this, Constants.PREFERENCE_MCPAY_APPROVAL_NO, "");
                    approvalDate = Preferences.getValue(this, Constants.PREFERENCE_MCPAY_APPROVAL_DATE, "");
                    cardNo = Preferences.getValue(this, Constants.PREFERENCE_MCPAY_CARD_NO, "");
                    cardCompany = Preferences.getValue(this, Constants.PREFERENCE_MCPAY_CARD_COMPANY, "");
                    cardAmt = Preferences.getValue(this, Constants.PREFERENCE_MCPAY_TOTAL_AMT, "");

                    if (transactionNo == "" || cardNo == "" || approvalNo == "") {
                        Log.d("111", "Card error");
                        return;
                    }

                    if (cardNo.length() >= 12) {
                        cardNo = cardNo.substring(0, 4) + cardNo.substring(12);
                    }

                } catch (NullPointerException e) {
                    if(Constants.DEBUG_PRINT_LOG){
                        e.printStackTrace();
                    }else{
                        System.out.println("예외 발생");
                    }
                }

                Preferences.putValue(this, Constants.PREFERENCE_MCPAY_TRANSACTION_NO, "");
                Preferences.putValue(this, Constants.PREFERENCE_MCPAY_APPROVAL_NO, "");
                Preferences.putValue(this, Constants.PREFERENCE_MCPAY_APPROVAL_DATE, "");
                Preferences.putValue(this, Constants.PREFERENCE_MCPAY_CARD_NO, "");
                Preferences.putValue(this, Constants.PREFERENCE_MCPAY_CARD_COMPANY, "");
                Preferences.putValue(this, Constants.PREFERENCE_MCPAY_TOTAL_AMT, "");

                PayVo payVo = new PayVo();
                payVo.setTransactionNo(transactionNo);
                payVo.setApprovalNo(approvalNo);
                payVo.setApprovalDate(approvalDate);
                payVo.setCardNo(cardNo);
                payVo.setCardCompany(cardCompany);
                payVo.setCardAmt(cardAmt);
                payVo.setCashAmt("0");
                payVo.setCatNumber(catNumber);
//                request(PayData.PAY_DIALOG_TYPE_PREPAY_CARD, payVo);
                printResult(payVo);
                break;
        }
    }

	@Override
	protected void request(int type, Object data) {
        if (type == PayData.PAY_DIALOG_TYPE_COUPON) {
            couponData = (String) data;
        }else{
            PayVo payVo = (PayVo) data;
            if (type == PayData.PAY_DIALOG_TYPE_PREPAY_CASH ) {
                printResult(payVo);
            }
            apiCouponInCall(payVo);
        }



	}

	private void printResult(PayVo payVo) {
		// 입차 등록

		if (payVo == null) {
			payVo = new PayVo();
			payVo.setCashAmt("0");
			payVo.setCardAmt("0");
		}

		int cardAmt = Util.sToi(payVo.getCardAmt());
		int cashAmt = Util.sToi(payVo.getCashAmt());


		ReceiptType13Vo type13Vo = new ReceiptType13Vo("쿠폰판매"    // 타이틀
				, Util.getYmdhms("yyyy-MM-dd HH:mm:ss")    // 출력일자
				, "0"        // 주차면
				, "시간권(선불)"            // 주차종류
				, "일반"        // 요금종류
				, "일반"    // 요금할인
				, ""        // 총미수금액
				, parkAmt    // 선납금액
				, Util.getYmdhms("HH:mm:ss")    // 입차시간
				, Util.getYmdhms("HH:mm:ss")        // 출차예정시간
				, "0"    // 총 분
				, BIZ_NO            // 사업자번호
				, BIZ_NAME            // 사업자명
				, BIZ_TEL,"","");            // 사업자연락처


		type13Vo.setPioNum(pioNum);
		type13Vo.setCarNum("쿠폰판매");
		type13Vo.setParkName(parkName);
		type13Vo.setEmpName(empName);
		type13Vo.setEmpPhone(empPhone);
		type13Vo.setEmpTel(empTel);

		type13Vo.setCashReceipt(payVo.getCashApprovalNo());
		type13Vo.setCardNo(payVo.getCardNo());
		type13Vo.setCardCompany(payVo.getCardCompany());
		type13Vo.setApprovalNo(payVo.getApprovalNo());


		PayData payData = new PayData(this, OUT_PRINT, IN_PRINT, TICKET_PRINT);
		payData.printDefault(PayData.PAY_DIALOG_TYPE_ENTRANCE_TIME, type13Vo, mPrintService);
	}

	// 쿠폰판매 등록(수정)
	private void apiCouponInCall(PayVo payVo) {

		if (payVo == null) {
			payVo = new PayVo();
			payVo.setCashAmt("0");
			payVo.setCardAmt("0");
		}


		int cardAmt = Util.sToi(payVo.getCardAmt());
		int cashAmt = Util.sToi(payVo.getCashAmt());

		String prePayment = (cardAmt > 0 ? "" + cardAmt : "" + cashAmt);

		Log.d(TAG, " prePayment>>> " + prePayment);

		String parkTypeCode = cch.findCodeByKnCode("PY", "시간권(선불)");
		String discountTypeCode = cch.findCodeByKnCode("DC", "일반");

		Map<String, Object> params = new HashMap<String, Object>();

		// method 셋팅 - 입차등록
		params.put(Constants.METHOD, Constants.INTERFACEID_COUPON_SELL_COMMIT);
		// format
		params.put(Constants.API_FORMAT, Constants.JSON_FORMAT);
		// 26자리 주차번호 BaseActivity.makepioNum(cdPark, empCode, parkDay)
		params.put("PIO_NUM", (pioNum == null ? BaseActivity.makePIONumber(parkCode, empCode, today) : pioNum));
		// 주차장코드(3자리)
		params.put("CD_PARK", parkCode);
		// 주차면
		params.put("CD_AREA", "0");
		// 주차일 (8자리)
		params.put("PARK_DAY", today);
		// 입차일
		params.put("IN_DATE", Util.getYmdhms("yyyy-MM-dd"));

		params.put("CHARGE_TYPE", "");
		// 주차 입차시간
		params.put("IN_TIME", Util.getYmdhms("HH:mm:ss"));
		// 주차 출차시간
		params.put("OUT_TIME", Util.getYmdhms("HH:mm:ss"));

		// 차량 번호
		params.put("CAR_NO", "쿠폰판매");
		// 주차형태 02
		params.put("PARK_TYPE", parkTypeCode);
		// 할인 코드
		params.put("DIS_CD", discountTypeCode);

		// 현금 영수증 관련
		params.put("CASH_APPROVAL_NO", payVo.getCashApprovalNo());
		params.put("CASH_APPROVAL_DATE", payVo.getCashApprovalDate());
		params.put("CATNUMBER", payVo.getCatNumber());

		// 카드 결제 관련
		params.put("TRANSACTION_NO", payVo.getTransactionNo());
		params.put("APPROVAL_NO", payVo.getApprovalNo());
		params.put("APPROVAL_DATE", payVo.getApprovalDate());
		params.put("CARD_NO", payVo.getCardNo());
		params.put("CARD_COMPANY", payVo.getCardCompany());

		if (parkTypeCode.equals("01") || parkTypeCode.equals("03")) {  // 시간권(후불)은  출차등록 시 금액 계산 정기권은 금액계산없이 입차
			// realamt 원금
			params.put("REAL_AMT", 0);
			// 현금 금액 = 선불결제화면에서 직접입력한 값입력
			params.put("CASH_AMT", 0);
			// 카드 금액 = 선불결제화면에서 카드결재금액
			params.put("CARD_AMT", 0);
			// 선납
			params.put("ADV_AMT", 0);
			// 할인 금액 = 할인코드에 맞는 총합
			params.put("DIS_AMT", 0);
			// 절삭 금액 = 100단위 삭제
			params.put("TRUNC_AMT", 0);
			// 선불권 처리 방법
			params.put("ADV_METHOD", "");
		} else {
			// 주차 원금
			params.put("REAL_AMT", parkAmt);
			// 현금 금액
			params.put("CASH_AMT", "" + cashAmt);
			// 카드 금액
			params.put("CARD_AMT", "" + cardAmt);
			// 선납 금액
			params.put("ADV_AMT", "" + prePayment);
			// 할인 금액 = 할인코드에 맞는 총합
			params.put("DIS_AMT", "0");
			// 절삭 금액 = 100단위 삭제
			params.put("TRUNC_AMT", "0");

			// 선불권  처리방법
			if ("04".equals(parkTypeCode)) {
				// 선불권 차량 입차
				params.put("ADV_METHOD", "ADV0");
			}
		}

		// 환불 금액 - 입차시 환불 금액은 0
		params.put("RETURN_AMT", "0");
		// 미납 금액 - 입차시 미납 금액은 0
		params.put("YET_AMT", "0");


		// 연락처
		params.put("TEL", "");
		// I1(입차)
		params.put("STATE_CD", "O1");
		// 등록사원(4자리)
		params.put("EMP_CD", empCode);
		// commnet
		params.put("KN_COMMENT", "");

		showProgressDialog(false);

		String url = Constants.API_SERVER;
		Log.d(TAG, " apiParkInResult url >>> " + url + " " + params);

		AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>();
		cb.url(url).params(params).type(JSONObject.class).weakHandler(this, "couponSellCallback").redirect(true).retry(3).fileCache(false).expire(-1);
		AQuery aq = new AQuery(this);
		aq.ajax(cb);
	}

	public void couponSellCallback(String url, JSONObject json, AjaxStatus status) {
		Log.d(TAG, " parkInResultCallback  json ====== " + json);
		closeProgressDialog();
		// successful ajax call
		if (json != null) {
			String KN_RESULT = json.optString("KN_RESULT");
			if ("1".equals(json.optString("CD_RESULT"))) {
				MsgUtil.ToastMessage(CloseManageActivity.this, "쿠폰이 판매되었습니다.", Toast.LENGTH_LONG);
				//Intent intent = new Intent(this, StatusBoardActivity.class);
				//startActivity(intent);
				finish();
			} else {
				MsgUtil.ToastMessage(CloseManageActivity.this, KN_RESULT, Toast.LENGTH_LONG);
			}
		} else {
			MsgUtil.ToastMessage(CloseManageActivity.this, Constants.DATA_FAIL, Toast.LENGTH_LONG);
		}
	}


	@Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long arg3) {
        Log.e(TAG, "onItemClick : " + position);
        ImageLoaderAdapter adapter = (ImageLoaderAdapter) parent.getAdapter();
        PictureInfo item = adapter.getItem(position);
        viewPictureDialog(item);
    }

    @Override
    public void onBasicDlgItemSelected(int tag, boolean choice) {
        super.onBasicDlgItemSelected(tag, choice);

        if (100 == tag) {
            if (choice) {
                apiParkCloseUpdate();
            } else {
                setToast("취소되었습니다.");
            }
        } else if (309 == tag) {
            /**XXX 이전버전 2015.11.01 작업
             Intent intent = new Intent(CloseManageActivity.this, ExitManageActivity.class);
             **/
            Intent intent = new Intent(CloseManageActivity.this, StatusBoardActivity.class);
            intent.putExtra("search_date", search_date);
            startActivity(intent);
            finish();
        } else if (1888 == tag) {
//			Intent intent = new Intent(this, WorkImageActivity.class);
//			intent.putExtra("pictureType", Constants.PICTURE_TYPE_T2 );
//			startActivityForResult(intent, Constants.COMMON_TYPE_CLOSE_PICTURE);
            apiParkIoListCall(Util.getEncodeStr("%"), "I1", search_date.replaceAll("-", ""), search_date.replaceAll("-", ""), 2);
        }
    }

    public Dialog viewPictureDialog(PictureInfo item) {
        final Dialog viewPictureDialog;

            viewPictureDialog = new Dialog(this,
                    android.R.style.Theme_Holo_Light_Dialog);

        viewPictureDialog.setContentView(R.layout.dialog_picture_view);
        viewPictureDialog.setTitle("사진보기");

        ((TextView) viewPictureDialog.findViewById(R.id.car_number)).setText(item.getCarNo());

        CodeHelper cch = CodeHelper.getInstance(this);
        ((TextView) viewPictureDialog.findViewById(R.id.kind_pic)).setText(cch.findCodeByCdCode("DR", item.getPictureType()));

        final ImageView ivPicture = (ImageView) viewPictureDialog.findViewById(R.id.img);
        loader.loadBitmap(item.getPath(), ivPicture);

        ((Button) viewPictureDialog.findViewById(R.id.btn_ok)).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                viewPictureDialog.dismiss();
            }
        });

        viewPictureDialog.setOnCancelListener(new OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
            }
        });

        viewPictureDialog.show();
        return viewPictureDialog;
    }

    /**
     * 사진 관리에서의 onClick 처리
     *
     * @param v
     */
    public void onClickPicture(View v) {
        switch (v.getId()) {
            case R.id.date:
                new CalendarDlg(this).show();
                break;

            case R.id.kind_picture:
                setCommonDialog("검색조건선택", "DR", Constants.COMMON_TYPE_PICTURE_TYPE, tvKindPicuture);
                break;

            case R.id.btn_car_search:
                loadPicture();
                break;

            case R.id.delete_pic:
                showProgressDialog();
                mHandler.sendEmptyMessage(0);
                break;
        }
    }

    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        int position = (Integer) buttonView.getTag();
        PictureInfo item = pictureAdapter.getItem(position);
        item.setChecked(isChecked);

        if (checkList()) {
            btnDeletePic.setEnabled(true);
        } else {
            btnDeletePic.setEnabled(false);
        }
        pictureAdapter.notifyDataSetChanged();
    }

    boolean checkList() {
        int size = pictureAdapter.getCount();

        for (int i = 0; i < size; i++) {
            PictureInfo item = pictureAdapter.getItem(i);
            if (item.isChecked()) {
                return true;
            }
        }

        return false;
    }

    public void getSelectListDialogData(int tag, String data) {
        if (tag == Constants.COMMON_TYPE_PICTURE_TYPE) {
            tvKindPicuture.setText(data);
            loadPicture();
        }
    }


    /**
     * 환경설정에서의 onClick 처리
     *
     * @param v
     */
    public void onClickStting(View v) {
        Preferences.putValue(this, Constants.PREFERENCE_SETTING_STATUS_BOARD_SIZE, boardSize);
        Preferences.putValue(this, Constants.PREFERENCE_SETTING_INPUT_PICTURE, isInputPicture);
        Preferences.putValue(this, Constants.PREFERENCE_SETTING_EXIT_PICTURE, isExitPicture);
        Preferences.putValue(this, Constants.PREFERENCE_SETTING_REGISTER_COMMUTATION_TICKET, isRegisterCommutationTicket);
        Preferences.putValue(this, Constants.PREFERENCE_SETTING_DELETE_APP_IMG, isDeleteAppImg);
        Preferences.putValue(this, Constants.PREFERENCE_SETTING_DELETE_IO_IMG, isDeleteIoImg);
        Preferences.putValue(this, Constants.PREFERENCE_SETTING_DELETE_SERVER_IMG, isDeleteServerImg);
        finish();
    }


    Calendar calendar = Calendar.getInstance();

    public class CalendarDlg extends Dialog {

        public class myCalendar extends CalendarInfo {
            public myCalendar(Context context, LinearLayout layout) {
                super(context, layout);
            }

            @Override
            public void myClickEvent(int yyyy, int MM, int dd) {
                search_date = String.format("%d-%02d-%02d", yyyy, (MM + 1), dd);
                if (close_manager_view.getVisibility() == View.VISIBLE) {        // 마감관리
                    closeStart.setText(search_date);
                    apiParkCloseStatusDayCall(search_date.replaceAll("-", ""));

                } else if (picture_manager_view.getVisibility() == View.VISIBLE) {    // 사진관리
                    tvDate.setText(search_date);
                    loadPicture();
                }

                CalendarDlg.this.cancel();
                super.myClickEvent(yyyy, MM, dd);
            }
        }

        TextView tvs[];
        Button btns[];

        public CalendarDlg(Context context) {
            super(context, android.R.style.Theme_Holo_Light_Dialog_NoActionBar);
            setContentView(R.layout.calendar_layout);

            LinearLayout lv = (LinearLayout) findViewById(R.id.calendar_ilayout);

            tvs = new TextView[3];
            tvs[0] = (TextView) findViewById(R.id.tv1);
            tvs[1] = (TextView) findViewById(R.id.tv2);
            tvs[2] = null;

            btns = new Button[4];
            btns[0] = null;
            btns[1] = null;
            btns[2] = (Button) findViewById(R.id.Button03);
            btns[3] = (Button) findViewById(R.id.Button04);

            myCalendar cal = new myCalendar(context, lv);

            cal.setControl(btns);
            cal.setViewTarget(tvs);

            cal.initCalendar(search_date);
        }
    }

    void loadPicture() {

        // 키보드 숨기기
        if (ipm != null && et_car_number != null) {
            ipm.hideSoftInputFromWindow(et_car_number.getWindowToken(), 0);
        }

        CodeHelper cch = CodeHelper.getInstance(this);
        PictureHelper pictureHelper = PictureHelper.getInstance(this);

        String carNumber = et_car_number.getText() != null ? et_car_number.getText().toString() : "";
        String kindPicture = tvKindPicuture.getText() != null ? tvKindPicuture.getText().toString() : "";

        ArrayList<PictureInfo> list = (ArrayList<PictureInfo>) pictureHelper.getSearchList(tvDate.getText().toString().replaceAll("-", "")
                , carNumber
                , cch.findCodeByKnCode("DR", kindPicture));

        if (list == null || list.size() <= 0) {
            if (pictureAdapter != null) {
                pictureAdapter.clear();
                pictureAdapter.notifyDataSetChanged();
            }
            return;
        }

        pictureAdapter = new ImageLoaderAdapter(this, R.layout.picture_list_row, list);
        pictureAdapter.setCheckListener(this);
        pictureListView.setAdapter(pictureAdapter);

        for (int i = 0; i < list.size(); i++) {
            PictureInfo item = list.get(i);
            Log.i(TAG, "" + i + " = " + item);
        }
    }


    /**
     * 주차장 마감 체크
     */
    private void apiParkClose() {
        showProgressDialog(false);
        String url = Constants.API_SERVER + "?" + Constants.METHOD + "=" + Constants.INTERFACEID_PARK_CLOSE;
        String param = "&CD_PARK=" + parkCode
                + "&WORK_DAY=" + search_date.replaceAll("-", "")
                + "&" + Constants.API_FORMAT + "=" + Constants.JSON_FORMAT;
        Log.d(TAG, " apiParkClose url >>> " + url + param);

        AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>();
        cb.url(url + param).type(JSONObject.class).weakHandler(this, "parkCloseCallback").redirect(true).retry(3).fileCache(false).expire(-1);
        AQuery aq = new AQuery(this);
        aq.ajax(cb);
    }

    public void parkCloseCallback(String url, JSONObject json, AjaxStatus status) {

        closeProgressDialog();
        Log.d(TAG, " parkCloseCallback  json ====== " + json);

        // successful ajax call
        if (json != null) {
            String KN_RESULT = json.optString("KN_RESULT");
            if ("".equals(json.optString("CD_STATE"))) {
                // 마감정보가 없다는 팝업 표시 후 종료
                MsgUtil.ToastMessage(this, KN_RESULT);
                btn_close.setEnabled(false);
                //  2015.05.06 GS파크 마감 제한 시간 없도록 처리...
                btn_close_auto.setEnabled(false);
                btn_daily_report.setEnabled(true);
            } else if ("C".equals(json.optString("CD_STATE"))) {
                // 마감이 완료되었다는 팝업 표시 후 종료
                MsgUtil.ToastMessage(this, "주차장 영업 마감되었습니다.");
                btn_close.setEnabled(false);
                //  2015.05.06 GS파크 마감 제한 시간 없도록 처리...
                btn_close_auto.setEnabled(false);
                btn_daily_report.setEnabled(true);
            } else {
                btn_close.setEnabled(true);
                //  2015.05.06 GS파크 마감 제한 시간 없도록 처리...
                btn_close_auto.setEnabled(true);
                btn_daily_report.setEnabled(false);
            }
        } else {
            MsgUtil.ToastMessage(this, Constants.DATA_FAIL, Toast.LENGTH_LONG);
        }

    }

    /**
     * 주차 현황 체크
     */
    private void apiParkIoListCall(String carNo, String codeState, String startDay, String endDay, int type) {
        showProgressDialog();

        String url = Constants.API_SERVER + "?" + Constants.METHOD + "=" + Constants.INTERFACEID_PARK_IO;
        String param = "&CD_PARK=" + parkCode
                + "&CAR_NO=" + carNo
                + "&CD_STATE=" + codeState
                + "&START_DAY=" + startDay
                + "&END_DAY=" + endDay
                + "&" + Constants.API_FORMAT + "=" + Constants.JSON_FORMAT;
        Log.d(TAG, " apiParkIoListCall url >>> " + url + param);

        AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>();
        if (type == 1) {
            cb.url(url + param).type(JSONObject.class).weakHandler(this, "parkIoListCallback").redirect(true).retry(3).fileCache(false).expire(-1);
        } else {
            cb.url(url + param).type(JSONObject.class).weakHandler(this, "parkIoListAutoCallback").redirect(true).retry(3).fileCache(false).expire(-1);
        }
        AQuery aq = new AQuery(this);
        aq.ajax(cb);
    }

    public void parkIoListCallback(String url, JSONObject json, AjaxStatus status) {

        closeProgressDialog();

        Log.d(TAG, " parkIoListCallback  json ====== " + json);

        // successful ajax call
        if (json != null) {
            String KN_RESULT = json.optString("KN_RESULT");
            if (json.optInt("COUNT", 0) > 0) {
                twoButtonDialog(309, "알림", "출차되지 않은 차가 있습니다.", false).show();
            } else {
                twoButtonDialog(100, "알림", "마감을 하시겠습니까?").show();
            }
        } else {
            MsgUtil.ToastMessage(this, Constants.DATA_FAIL, Toast.LENGTH_LONG);
        }

    }

    public void parkIoListAutoCallback(String url, JSONObject json, AjaxStatus status) {

        closeProgressDialog();

        Log.d(TAG, " parkIoListAutoCallback  json ====== " + json);

        String advList = "";
        String laterList = "";
        String dayList = "";
        String ticketList = "";

        // successful ajax call
        if (json != null) {
            String KN_RESULT = json.optString("KN_RESULT");
            if ("1".equals(json.optString("CD_RESULT"))) {
                JSONArray jsonArr = json.optJSONArray("RESULT");
                int Len = jsonArr.length();
                for (int i = 0; i < Len; i++) {
                    try {
                        JSONObject res = jsonArr.getJSONObject(i);
                        String PIO_NUM = res.optString("PIO_NUM");
                        String PARK_IN = res.optString("PARK_IN");
                        String OUT_DATE = Util.getYmdhms("yyyyMMdd");
                        String OUT_TIME = Util.getYmdhms("HHmmss");

                        if (Integer.valueOf(OUT_TIME.substring(0, 5).replace(":", "")) > Util.sToi(workEndTime)) {
                            OUT_TIME = workEndTime.substring(0, 2) + ":" + workEndTime.substring(2, 4) + ":00";
                        }

                        String CAR_NO = res.optString("CAR_NO");
                        String TEL = res.optString("TEL");
                        String DIS_CD = res.optString("DIS_CD");
                        String PARK_TYPE = res.optString("PARK_TYPE");
                        String KN_CHARGE_TYPE = res.optString("KN_CHARGE_TYPE");
                        String ADV_IN_AMT = res.optString("ADV_IN_AMT");
                        String DIS_IN_AMT = res.optString("DIS_IN_AMT");
                        String TRUNC_IN_AMT = res.optString("TRUNC_IN_AMT");

                        // 미출차 일일권 리스트
                        if ("02".equals(PARK_TYPE)) {
                            dayList += "PIO_NUM=" + PIO_NUM + "^"
                                    + "OUT_DATE=" + OUT_DATE + "^"
                                    + "OUT_TIME=" + OUT_TIME + "^"
                                    + "STATE_CD=O1" + ",";
                        }
                        // 미출차 정기권 리스트
                        if ("03".equals(PARK_TYPE)) {
                            ticketList += "PIO_NUM=" + PIO_NUM + "^"
                                    + "OUT_DATE=" + OUT_DATE + "^"
                                    + "OUT_TIME=" + OUT_TIME + "^"
                                    + "STATE_CD=O1" + ",";
                        }
                        // 미출차 시간권 후불 리스트
                        if ("01".equals(PARK_TYPE)) {
                            laterList += setParkAmt(PIO_NUM, PARK_IN, OUT_DATE, OUT_TIME, CAR_NO, TEL, DIS_CD, KN_CHARGE_TYPE, PARK_TYPE, ADV_IN_AMT, DIS_IN_AMT, TRUNC_IN_AMT);
                        }

                        // 미출차 시간권 선불 리스트
                        if ("04".equals(PARK_TYPE)) {
                            advList += setParkAmt(PIO_NUM, PARK_IN, OUT_DATE, OUT_TIME, CAR_NO, TEL, DIS_CD, KN_CHARGE_TYPE, PARK_TYPE, ADV_IN_AMT, DIS_IN_AMT, TRUNC_IN_AMT);
                        }

                    } catch (JSONException e) {
                        if(Constants.DEBUG_PRINT_LOG){
                            e.printStackTrace();
                        }else{
                            System.out.println("예외 발생");
                        }
                    }
                }
            }
        } else {
            MsgUtil.ToastMessage(this, Constants.DATA_FAIL, Toast.LENGTH_LONG);
        }

        Log.d(TAG, "dayList >>> " + dayList);
        Log.d(TAG, "ticketList >>> " + ticketList);
        Log.d(TAG, "laterList >>> " + laterList);
        Log.d(TAG, "advList >>> " + advList);

        apiParkAutoCloseUpdate(dayList, ticketList, laterList, advList);

    }


    private String setParkAmt(String pioNum, String parkIn, String outDate, String outTime
            , String carNo, String tel, String disCd, String knChargeType, String parkTypeCode
            , String advInAmt, String disInAmt, String truncInAmt) {


        //###############################################################
        //  STEP 1 : 주차 시간 계산
        //###############################################################
        Log.d(TAG, "date1 : " + parkIn + ", date2 : " + outDate + " " + outTime);
        String parkMin = minuteCalc(parkIn, outDate + outTime);
        Log.d(TAG, " parkMin >>> " + parkMin);


        //###############################################################
        //  STEP 2 : 할인 시간 계산
        //###############################################################
        // 요금할인 정보
        // disCd

        // 할인시간 정보
        int disTime = Util.sToi(cch.searchCodeDisTime("DC", disCd), 0);
        if (disTime > 0) {
//			Log.d(TAG, "parkMin 2 : " + parkMin);
            parkMin = "" + (Util.sToi(parkMin, 0) - disTime);
//			Log.d(TAG, "parkMin 3 : " + parkMin);
            if ((Util.sToi(parkMin, 0) < 0)) {
//				Log.d(TAG, "parkMin 4 : " + parkMin);
                parkMin = "0";
            }
        }


        //###############################################################
        //  STEP 3 : 할인 금액 계산
        //###############################################################

        // 주차종류
        // parkTypeCode

        // 요금종류코드
        String chargeCode = "";

        if ("2".equals(parkClass)) {
            //chargeCode = cch.findCodeByKnCode("AM2", knChargeType);
            chargeCode = "M2-1";
        } else if ("3".equals(parkClass)) {
            //chargeCode = cch.findCodeByKnCode("AM3", knChargeType);
            chargeCode = "M3-1";
        } else if ("4".equals(parkClass)) {
            //chargeCode = cch.findCodeByKnCode("AM4", knChargeType);
            chargeCode = "M4-1";
        } else if ("5".equals(parkClass)) {
            //chargeCode = cch.findCodeByKnCode("AM5", knChargeType);
            chargeCode = "M5-1";
        } else if ("6".equals(parkClass)) {
            //chargeCode = cch.findCodeByKnCode("AM6", knChargeType);
            chargeCode = "M6-1";
        }

        // 주차금액 계산
        String total = parkCharge(parkMin, chargeCode, parkTypeCode);
        String charge = "0";
        String discountAmt = "0";
        int truncAmt = 0;

        if ("02".equals(parkTypeCode)) {  // 일일권
            total = "0";  // 일일권은 출차시 0원
            charge = "0";
            discountAmt = "0";  // 일일권은 할인을 하지 않는다.
            truncAmt = 0;
        } else if ("03".equals(parkTypeCode)) { // 정기권
            total = "0";  // 정기권은 출차시 0원
            charge = "0";
            discountAmt = "0";  // 정기권은 할인을 하지 않는다.
            truncAmt = 0;
        } else {

            if ("04".equals(parkTypeCode)) { // 시간권 선불 : 총 주차요금에서 이전에 할인 받은 금액 차감
                Log.d(TAG, " total as is >>> " + total);
                total = "" + (Util.sToi(total) - Util.sToi(advInAmt) - Util.sToi(disInAmt) - Util.sToi(truncInAmt));
                Log.d(TAG, " total to be >>> " + total);
            }

            // 주차금액 셋팅
            charge = total;

            if (Util.sToi(total) > 0) {

                int tempDisAmt1 = 0;
                int tempDisAmt2 = 0;

                String disAmt = Util.isNVL(cch.searchCodeDisAmt("DC", disCd), "0");
                if (!"0".equals(disAmt)) {
                    // 할인금액 설정
                    tempDisAmt1 = Util.sToi(disAmt);
                    charge = "" + (Util.sToi(charge) - tempDisAmt1);
                    Log.d(TAG, "setParkAmt ::: charge >>> " + charge + "  tempDisAmt1 : " + tempDisAmt1);
                }

                // 할인률 조회
                String disRate = cch.searchCodeDisRate("DC", disCd);
                if (!"0".equals(disRate)) {
                    // 할인률에 따른 할인금액 설정
                    tempDisAmt2 = Util.sToi(Util.isNVL("" + discountChargeRate(charge, disRate), "0"));
                    charge = "" + (Util.sToi(charge) - tempDisAmt2);
                    Log.d(TAG, "setParkAmt ::: charge >>> " + charge + "  tempDisAmt2 : " + tempDisAmt2);
                }

                // 총 할인금액
                discountAmt = "" + (tempDisAmt1 + tempDisAmt2);
                Log.d(TAG, " setParkAmt >>> charge : " + charge + " discountAmt : " + discountAmt);

                if (Util.sToi(charge, 0) < 0) {
                    charge = "0";
                }
            } else { // 환불 발생 건 - 마감 시에는 발생할 내역이 없겠지만...

                // 할인률 조회
                String disRate = cch.searchCodeDisRate("DC", disCd);

                // 할인률 100% 적용이면
                if ("100".equals(disRate)) {
                    charge = "0";
                    discountAmt = total;
                } else {
                    if (!"0".equals(disRate)) {
                        // 할인률에 따른 할인금액 설정
                        discountAmt = Util.isNVL("" + discountChargeRate(total, disRate), "0");
                    }

                    Log.d(TAG, " setParkAmt >>> return total  : " + total + " discountAmt : " + discountAmt);
                    charge = Util.toStr((Util.sToi(total) - Util.sToi(discountAmt)));
                    Log.d(TAG, " setParkAmt >>> return charge : " + charge + " discountAmt : " + discountAmt);
                }
            }  // end if total
        } // end if parkTypeCode

        Log.d(TAG, " total  <<<>>> " + total);
        Log.d(TAG, " charge <<<>>> " + charge);
        Log.d(TAG, " discountAmt  <<<>>> " + discountAmt);


        //###############################################################
        //  STEP 4 : 납부 금액 계산
        //###############################################################

        int payment = 0;

        // 일일권 또는 정기권일 경우 주차요금 및 납부금액은 계산하지 않는다.
        if ("02".equals(parkTypeCode) || "03".equals(parkTypeCode)) {
        } else { // 시간권 후불 또는 선불
            // 납부금액 = 주차요금
            payment = Util.sToi(charge);
        }


        //###############################################################
        //  STEP 5 : 절삭 금액 계산
        //###############################################################

        if (!"0".equals(truncUnit)) {
            truncAmt = payment % Integer.parseInt(truncUnit);
        } else {
            truncAmt = 0;
        }

        //** 과천시 할인금액이 없으면 절삭 적용
        // 주차할인코드
        // String disCode = cch.findCodeByKnCode("DC", discountName);
        if ("01".equals(disCd)) {
            payment = payment - truncAmt;
        } else { // 할인금액이 있으면 올림 적용
            if (truncAmt > 0) {
                // 납부금액은 절삭 올림 적용
                payment = payment + (Util.sToi(truncUnit) - truncAmt);
                // 할인금액은 절삭 차감
                discountAmt = "" + (Util.sToi(discountAmt) - (Util.sToi(truncUnit) - truncAmt));
                truncAmt = 0;
            }
        }


        //###############################################################
        //  STEP 6 : 정상출차 및 미수출차 구분
        //###############################################################

        String appendQuery = "";
        if (payment > 0) { // 추가요금 미수처리
            appendQuery = "STATE_CD=O2^&YET_CD=P3-2^";
            if ("04".equals(parkTypeCode)) { // 시간권 선불인 경우 선불권 처리방법 셋팅
                appendQuery = appendQuery + "ADV_METHOD=ADV2^";
            }
        } else {  // 정상출차
            appendQuery = "STATE_CD=O1^YET_CD=^";
            if ("04".equals(parkTypeCode)) { // 시간권 선불인 경우 선불권 처리방법 셋팅

                // 할인률 조회
                String disRate = cch.searchCodeDisRate("DC", disCd);

                // 선불권 할인률 100% 적용
                if ("100".equals(disRate)) {
                    appendQuery = appendQuery + "ADV_METHOD=ADV4^";
                } else {
                    appendQuery = appendQuery + "ADV_METHOD=ADV3^";
                }
            }
        }

        String strList = "";
        strList = "PIO_NUM=" + pioNum + "^"
                + "OUT_DATE=" + outDate + "^"
                + "OUT_TIME=" + outTime + "^"
                + "CAR_NO=" + carNo + "^"
                + "TEL=" + tel + "^"
                + "DIS_CD=" + disCd + "^"
                + "REAL_AMT=" + total + "^"
                + "DIS_AMT=" + discountAmt + "^"
                + "YET_AMT=" + payment + "^"
                + "TRUNC_AMT=" + Math.abs(truncAmt) + "^"
                + appendQuery + ",";
        return strList;

    }

    /**
     * 주차장 자동마감
     */
    private void apiParkAutoCloseUpdate(String dayList, String ticketList, String laterList, String advList) {
        Map<String, Object> params = new HashMap<String, Object>();

        // method 셋팅...
        params.put(Constants.METHOD, Constants.INTERFACEID_PARK_CLOSE_AUTO_UPDATE);
        // format
        params.put(Constants.API_FORMAT, Constants.JSON_FORMAT);
        // 주차장 코드
        params.put("CD_PARK", parkCode);
        // 근무일 
        params.put("WORK_DAY", search_date.replaceAll("-", ""));
        // 사번코드
        params.put("EMP_CD", empCode);
        // 상태코드 
        params.put("CD_STATE", "C");
        // 일일권 리스트
        params.put("DAY_LIST", dayList);
        // 정기권 리스트
        params.put("TICKET_LIST", ticketList);
        // 시간권(후불) 리스트
        params.put("LATER_LIST", laterList);
        // 시간권(선불) 리스트
        params.put("ADV_LIST", advList);

        showProgressDialog(false);

        String url = Constants.API_SERVER;
        Log.d(TAG, " apiParkAutoCloseUpdate url >>> " + url + " params ::: " + params);

        AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>();
        cb.url(url).params(params).type(JSONObject.class).weakHandler(this, "parkCloseUpdateCallback").redirect(true).retry(3).fileCache(false).expire(-1);
        AQuery aq = new AQuery(this);
        aq.ajax(cb);
    }

    /**
     * 주차장 마감
     */
    private void apiParkCloseUpdate() {

        if (search_date.replaceAll("-", "").equals("")) {
            MsgUtil.ToastMessage(this, "마감일자를 선택해주세요.");
            return;
        }

        showProgressDialog(false);
        String url = Constants.API_SERVER + "?" + Constants.METHOD + "=" + Constants.INTERFACEID_PARK_CLOSE_UPDATE;
        String param = "&CD_PARK=" + parkCode
                + "&WORK_DAY=" + search_date.replaceAll("-", "")
                + "&EMP_CD=" + empCode
                + "&CD_STATE=C"
                + "&" + Constants.API_FORMAT + "=" + Constants.JSON_FORMAT;
        Log.d(TAG, " apiParkCloseUpdate url >>> " + url + param);

        AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>();
        cb.url(url + param).type(JSONObject.class).weakHandler(this, "parkCloseUpdateCallback").redirect(true).retry(3).fileCache(false).expire(-1);
        AQuery aq = new AQuery(this);
        aq.ajax(cb);
    }

    public void parkCloseUpdateCallback(String url, JSONObject json, AjaxStatus status) {

        closeProgressDialog();
        Log.d(TAG, " parkCloseUpdateCallback  json ====== " + json);

        // successful ajax call
        if (json != null) {
            String KN_RESULT = json.optString("KN_RESULT");
            if ("1".equals(json.optString("CD_RESULT"))) {
                Toast.makeText(this, KN_RESULT, Toast.LENGTH_LONG).show();

                btn_close.setEnabled(false);
                btn_close_auto.setEnabled(false);
                btn_daily_report.setEnabled(true);
                ViewReload();
                if (Preferences.getValue(this, Constants.PREFERENCE_SETTING_DELETE_APP_IMG, false)) {
                    PictureHelper pictureHelper = PictureHelper.getInstance(this);
                    pictureHelper.deleteAll();
                    FileUtil.directoryDelete(new java.io.File(Constants.IMG_SAVE_PATH));
                }
            } else {
                MsgUtil.ToastMessage(this, KN_RESULT, Toast.LENGTH_LONG);
            }
        } else {
            MsgUtil.ToastMessage(this, Constants.DATA_FAIL, Toast.LENGTH_LONG);
        }

    }

    void ViewReload() {
        inputTime.setText("0건");
        inputTimePrice.setText("0원");
        inputCommutationTicket.setText("0건");
        inputCommutationTicketPrice.setText("0원");
        inputUnpaidBalance.setText("0건");
        inputUnpaidBalancePrice.setText("0원");
        inputTotalCash.setText("0건");
        inputTotalCashPrice.setText("0원");
        inputTotalCard.setText("0건");
        inputTotalCardPrice.setText("0원");
        inputPrepay.setText("0건");
        inputPrepayPrice.setText("0원");
        createUnpaidBalance.setText("0건");
        createUnpaidBalancePrice.setText("0원");
        createRefund.setText("0건");
        createRefundPrice.setText("0원");
        closeStatusItem = null;
        apiParkCloseStatusDayCall(search_date.replaceAll("-", ""));
    }


    void setCloseData(JSONObject res, JSONArray yetArr, JSONArray returnArr, JSONArray ticketArr, JSONArray DiscountArr, JSONArray CouponArr) {

        // 미납발생 차량 출력항목
        ArrayList<ParkCloseStatusYet> yetList = new ArrayList<ParkCloseStatusYet>();
        if (yetArr != null) {
            int len = yetArr.length();
            for (int i = 0; i < len; i++) {
                try {
                    JSONObject jsonObj = yetArr.getJSONObject(i);
                    ParkCloseStatusYet item = new ParkCloseStatusYet(jsonObj.optString("CAR_NO")
                            , jsonObj.optString("DIS_CD")
                            , jsonObj.optString("KN_DIS_CD")
                            , jsonObj.optString("PARK_TIME")
                            , jsonObj.optString("YET_AMT"));
                    yetList.add(item);
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    if(Constants.DEBUG_PRINT_LOG){
                        e.printStackTrace();
                    }else{
                        System.out.println("예외 발생");
                    }
                }
            } // end for
        } // end yet

        // 미수금납부 차량 출력항목
        ArrayList<ParkCloseStatusReturn> returnList = new ArrayList<ParkCloseStatusReturn>();
        if (returnArr != null) {
            int len = returnArr.length();
            for (int i = 0; i < len; i++) {
                try {
                    JSONObject jsonObj = returnArr.getJSONObject(i);
                    ParkCloseStatusReturn item = new ParkCloseStatusReturn(jsonObj.optString("CAR_NO")
                            , jsonObj.optString("DIS_CD")
                            , jsonObj.optString("KN_DIS_CD")
                            , jsonObj.optString("BILL_TYPE")
                            , jsonObj.optString("KN_BILL_TYPE")
                            , jsonObj.optString("RETURN_AMT")
                            , jsonObj.optString("RETURN_DATE"));
                    returnList.add(item);
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    if(Constants.DEBUG_PRINT_LOG){
                        e.printStackTrace();
                    }else{
                        System.out.println("예외 발생");
                    }
                }
            } // end for
        } // end return

        // 정기권 차량 출력항목
        ArrayList<ParkCloseStatusTicket> ticketList = new ArrayList<ParkCloseStatusTicket>();
        if (ticketArr != null) {
            int len = ticketArr.length();
            for (int i = 0; i < len; i++) {
                try {
                    JSONObject jsonObj = ticketArr.getJSONObject(i);
                    ParkCloseStatusTicket item = new ParkCloseStatusTicket(jsonObj.optString("CAR_NO")
                            , jsonObj.optString("PAY_AMT")
                            , jsonObj.optString("TICKET_DATE"));
                    ticketList.add(item);
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    if(Constants.DEBUG_PRINT_LOG){
                        e.printStackTrace();
                    }else{
                        System.out.println("예외 발생");
                    }
                }
            } // end for
        } // end return

        // 할인 차량 출력항목

        ArrayList<ParkCloseStatusDiscountList> DiscountList = new ArrayList<ParkCloseStatusDiscountList>();
        if (DiscountArr != null) {
            int len = DiscountArr.length();
            for (int i = 0; i < len; i++) {
                try {
                    JSONObject jsonObj = DiscountArr.getJSONObject(i);
                    //Log.d("111", "22------------------" + jsonObj.optString("DIS_CD") + "~" + cch.findCodeByCdCode("DC", jsonObj.optString("DIS_CD")));
                    ParkCloseStatusDiscountList item = new ParkCloseStatusDiscountList(jsonObj.optString("DIS_CD")
                            , jsonObj.optString("KN_DIS_CD")
                            , jsonObj.optString("PARK_INCOME")
                            , jsonObj.optString("PARKAMT")
                            , jsonObj.optString("CNT")
                    );
                    DiscountList.add(item);
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    if(Constants.DEBUG_PRINT_LOG){
                        e.printStackTrace();
                    }else{
                        System.out.println("예외 발생");
                    }
                }
            } // end for
        }

        // 쿠폰 차량 출력항목
        ArrayList<ParkCloseStatusDiscountList> CouponList = new ArrayList<ParkCloseStatusDiscountList>();
        if (CouponArr != null) {
            int len = CouponArr.length();
            for (int i = 0; i < len; i++) {
                try {
                    JSONObject jsonObj = CouponArr.getJSONObject(i);
                    ParkCloseStatusDiscountList item = new ParkCloseStatusDiscountList(jsonObj.optString("COUPON_TYPE")
                            , jsonObj.optString("COUPON_TITLE")
                            , jsonObj.optString("COUPON_AMT")
                            , jsonObj.optString("PARKAMT")
                            , jsonObj.optString("CNT")
                    );
                    CouponList.add(item);
                } catch (JSONException e) {
                    if(Constants.DEBUG_PRINT_LOG){
                        e.printStackTrace();
                    }else{
                        System.out.println("예외 발생");
                    }
                }
            }
        }


        if (res != null) {
            closeStatusItem = new ParkCloseStatusItem(res.optString("TIME_CNT", "0")
                    , Util.addComma(res.optString("TIME_AMT", "0"))
                    , res.optString("TICKET_CASH_CNT", "0")
                    , Util.addComma(res.optString("TICKET_CASH_AMT", "0"))
                    , res.optString("TICKET_CARD_CNT", "0")
                    , Util.addComma(res.optString("TICKET_CARD_AMT", "0"))
                    , res.optString("TICKET_CNT", "0")
                    , Util.addComma(res.optString("TICKET_AMT", "0"))
                    , res.optString("PAY_CNT", "0")
                    , Util.addComma(res.optString("PAY_AMT", "0"))
                    , res.optString("SUM_CASH_CNT", "0")
                    , Util.addComma(res.optString("SUM_CASH_AMT", "0"))
                    , res.optString("SUM_CARD_CNT", "0")
                    , Util.addComma(res.optString("SUM_CARD_AMT", "0"))
                    , res.optString("YET_CNT", "0")
                    , Util.addComma(res.optString("YET_AMT", "0"))
                    , res.optString("RETURN_CNT", "0")
                    , Util.addComma(res.optString("RETURN_AMT", "0"))
                    , res.optString("SUM_COUPON_CNT", "0")
                    , Util.addComma(res.optString("SUM_COUPON_AMT", "0"))
                    , res.optString("TOTAL_CASH_CNT", "0")
                    , Util.addComma(res.optString("TOTAL_CASH_AMT", "0"))
                    , res.optString("TOTAL_CARD_CNT", "0")
                    , res.optString("TOTAL_CARD_AMT", "0")
                    , res.optString("TOTAL_CNT", "0")
                    , Util.addComma(res.optString("TOTAL_AMT", "0"))
                    , yetList
                    , returnList
                    , ticketList
                    , res.optString("MH_CASH_CNT", "0")
                    , Util.addComma(res.optString("MH_CASH_AMT", "0"))
                    , res.optString("MH_CARD_CNT", "0")
                    , Util.addComma(res.optString("MH_CARD_AMT", "0"))
                    , res.optString("MH_CASH_CNT_TODAY", "0")
                    , Util.addComma(res.optString("MH_CASH_AMT_TODAY", "0"))
                    , res.optString("MH_CARD_CNT_TODAY", "0")
                    , Util.addComma(res.optString("MH_CARD_AMT_TODAY", "0"))
                    , res.optString("FREE_COUNT_SUM", "0")
                    , DiscountList, CouponList
            );
            closeStatusItem.setCouponSellAmt(Util.addComma(res.optString("COUPON_SELL_AMT", "0")));
            closeStatusItem.setCouponSellCnt(res.optString("COUPON_SELL_CNT", "0"));

        } else {
            closeStatusItem = new ParkCloseStatusItem("0"
                    , "0"
                    , "0"
                    , "0"
                    , "0"
                    , "0"
                    , "0"
                    , "0"
                    , "0"
                    , "0"
                    , "0"
                    , "0"
                    , "0"
                    , "0"
                    , "0"
                    , "0"
                    , "0"
                    , "0"
                    , "0"
                    , "0"
                    , "0"
                    , "0"
                    , "0"
                    , "0"
                    , "0"
                    , "0"
                    , yetList
                    , returnList
                    , ticketList
                    , "0"
                    , "0"
                    , "0"
                    , "0"
                    , "0"
                    , "0"
                    , "0"
                    , "0"
                    , "0"
                    , DiscountList, CouponList
            );
        }

        // 시간권입금
        inputTime.setText(closeStatusItem.getTimeCnt() + "건");
        inputTimePrice.setText(closeStatusItem.getTimeAmt() + "원");
        // 정기권입금
        inputCommutationTicket.setText(closeStatusItem.getTicketCnt() + "건");
        inputCommutationTicketPrice.setText(closeStatusItem.getTicketAmt() + "원");
        // 미수환수액
        inputUnpaidBalance.setText(closeStatusItem.getPayCnt() + "건");
        inputUnpaidBalancePrice.setText(closeStatusItem.getPayAmt() + "원");
        // 총현금입금
        String totalCashCount = String.valueOf(Util.sToi(closeStatusItem.getSumCashCnt().replaceAll(",", "")) + Util.sToi(closeStatusItem.get_MH_Cashcount().replaceAll(",", "")) + Util.sToi(closeStatusItem.getTicketCashCnt().replaceAll(",", "")));
        String totalCashAmt = String.valueOf(Util.sToi(closeStatusItem.getSumCashAmt().replaceAll(",", "")) + Util.sToi(closeStatusItem.get_MH_CashAmt().replaceAll(",", "")) + Util.sToi(closeStatusItem.getTicketCashAmt().replaceAll(",", "")));
        inputTotalCash.setText(totalCashCount + "건");
        inputTotalCashPrice.setText(Util.addComma(totalCashAmt) + "원");
        // 총카드입금
        //String totalCardCount = String.valueOf(Util.sToi(closeStatusItem.getSumCardCnt()) + Util.sToi(closeStatusItem.getTicketCardCnt()) + Util.sToi(res.optString("MH_CARD_CNT","0")));
        //String totalCardAmt = String.valueOf(Util.sToi(closeStatusItem.getSumCardAmt().replaceAll(",","")) + Util.sToi(closeStatusItem.getTicketCardAmt().replaceAll(",","")) + Util.sToi(res.optString("MH_CARD_AMT","0")));
        String totalCardCount = String.valueOf(Util.sToi(closeStatusItem.getSumCardCnt()) + Util.sToi(closeStatusItem.get_MHCount()) + Util.sToi(closeStatusItem.getTicketCardCnt()));
        String totalCardAmt = String.valueOf(Util.sToi(closeStatusItem.getSumCardAmt().replaceAll(",", "")) + +Util.sToi(closeStatusItem.get_MHAmt().replaceAll(",", "")) + Util.sToi(closeStatusItem.getTicketCardAmt().replaceAll(",", "")));

        inputTotalCard.setText(totalCardCount + "건");
        inputTotalCardPrice.setText(Util.addComma(totalCardAmt) + "원");
        //inputTotalCard.setText(closeStatusItem.getSumCardCnt() + "건");
        //inputTotalCardPrice.setText(closeStatusItem.getSumCardAmt() + "원");

        // 총쿠폰입금
        inputPrepay.setText(closeStatusItem.getTotalCouponCnt() + "건");
        inputPrepayPrice.setText(closeStatusItem.getTotalCouponAmt() + "원");
        // 총미수발생
        createUnpaidBalance.setText(closeStatusItem.getYetCnt() + "건");
        createUnpaidBalancePrice.setText(closeStatusItem.getYetAmt() + "원");
        // 총환불발생
        createRefund.setText(closeStatusItem.getReturnCnt() + "건");
        createRefundPrice.setText(closeStatusItem.getReturnAmt() + "원");
    }

    private void apiParkCloseStatusDayCall(String searchDate) {
        showProgressDialog();
        String url = Constants.API_SERVER + "?" + Constants.METHOD + "=" + Constants.INTERFACEID_PARK_CLOSE_STATUS_DAY_EMPCD
                + "&CD_PARK=" + parkCode
                + "&EMP_CD=" + empCode
                + "&CLOSE_DAY=" + searchDate
                + "&" + Constants.API_FORMAT + "=" + Constants.JSON_FORMAT;
        Log.d(TAG, " apiParkCloseStatusDayCall url >>> " + url);

        AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>();
        cb.url(url).type(JSONObject.class).weakHandler(this, "parkCloseStatusDayCallback").redirect(true).retry(3).fileCache(false).expire(-1);
        AQuery aq = new AQuery(this);
        aq.ajax(cb);
    }

    public void parkCloseStatusDayCallback(String url, JSONObject json, AjaxStatus status) {
        Log.d(TAG, " parkCloseStatusDayCallback  json ====== " + json);

        closeProgressDialog();
        JSONObject res = null;
        JSONArray yetArr = null;
        JSONArray returnArr = null;
        JSONArray ticketArr = null;
        JSONArray DiscountArr = null;
        JSONArray CouponArr = null;

        // successful ajax call
        if (json != null) {
            String KN_RESULT = json.optString("KN_RESULT");
            if ("1".equals(json.optString("CD_RESULT"))) {
                JSONArray jsonArr = json.optJSONArray("RESULT");
                int len = jsonArr.length();
                for (int i = 0; i < len; i++) {
                    try {
                        res = jsonArr.getJSONObject(i);
                    } catch (JSONException e) {
                        if(Constants.DEBUG_PRINT_LOG){
                            e.printStackTrace();
                        }else{
                            System.out.println("예외 발생");
                        }
                    }
                } // end for

                yetArr = json.optJSONArray("YET_LIST");
                returnArr = json.optJSONArray("RETURN_LIST");
                ticketArr = json.optJSONArray("TICKET_LIST");
                DiscountArr = json.optJSONArray("DISCOUNT_LIST");
                CouponArr = json.optJSONArray("COUPON_LIST");


                // Toast.makeText(this, KN_RESULT, Toast.LENGTH_LONG).show();
            } else {
                // Toast.makeText(this, KN_RESULT, Toast.LENGTH_LONG).show();
            }
        } else {
            Toast.makeText(this, Constants.DATA_FAIL, Toast.LENGTH_LONG).show();
        }

        setCloseData(res, yetArr, returnArr, ticketArr, DiscountArr, CouponArr);
        apiParkClose();
    }

    private void apiParkCloseStatusMonCall(String searchDate) {
        showProgressDialog();
        String url = Constants.API_SERVER + "?" + Constants.METHOD + "=" + Constants.INTERFACEID_PARK_CLOSE_STATUS_MON_EMPCD
                + "&CD_PARK=" + parkCode
                + "&CLOSE_DAY=" + searchDate
                + "&" + Constants.API_FORMAT + "=" + Constants.JSON_FORMAT;
        Log.d(TAG, " apiParkCloseStatusMonCall url >>> " + url);

        AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>();
        cb.url(url).type(JSONObject.class).weakHandler(this, "parkCloseStatusMonCallback").redirect(true).retry(3).fileCache(false).expire(-1);
        AQuery aq = new AQuery(this);
        aq.ajax(cb);
    }

    public void parkCloseStatusMonCallback(String url, JSONObject json, AjaxStatus status) {
        Log.d(TAG, " parkCloseStatusMonCallback  json ====== " + json);

        closeProgressDialog();

        JSONObject res = null;
        JSONArray yetArr = null;
        JSONArray returnArr = null;
        JSONArray ticketArr = null;
        JSONArray DiscountArr = null;
        JSONArray CouponArr = null;

        // successful ajax call
        if (json != null) {
            String KN_RESULT = json.optString("KN_RESULT");
            if ("1".equals(json.optString("CD_RESULT"))) {
                JSONArray jsonArr = json.optJSONArray("RESULT");
                int len = jsonArr.length();
                for (int i = 0; i < len; i++) {
                    try {
                        res = jsonArr.getJSONObject(i);
                    } catch (JSONException e) {
                        if(Constants.DEBUG_PRINT_LOG){
                            e.printStackTrace();
                        }else{
                            System.out.println("예외 발생");
                        }
                    }
                } // end for

                yetArr = json.optJSONArray("YET_LIST");
                returnArr = json.optJSONArray("RETURN_LIST");
                ticketArr = json.optJSONArray("TICKET_LIST");

                // Toast.makeText(this, KN_RESULT, Toast.LENGTH_LONG).show();
            } else {
                // Toast.makeText(this, KN_RESULT, Toast.LENGTH_LONG).show();
            }
        } else {
            Toast.makeText(this, Constants.DATA_FAIL, Toast.LENGTH_LONG).show();
        }

        setCloseData(res, yetArr, returnArr, ticketArr, DiscountArr, CouponArr);

    }


}