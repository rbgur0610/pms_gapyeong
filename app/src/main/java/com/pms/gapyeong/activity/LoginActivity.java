package com.pms.gapyeong.activity;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Paint;
import android.support.design.widget.TextInputEditText;
import android.util.DisplayMetrics;
import android.util.Log;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Message;
import android.os.StrictMode;
import android.telephony.TelephonyManager;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnKeyListener;
import android.widget.Button;
import android.widget.Toast;

import com.pms.gapyeong.R;
import com.pms.gapyeong.common.ApplicationInfo;
import com.pms.gapyeong.common.Constants;
import com.pms.gapyeong.common.DeviceInfo;
import com.pms.gapyeong.common.Device;
import com.pms.gapyeong.common.Preferences;
import com.pms.gapyeong.common.Util;
import com.pms.gapyeong.database.Preference;
import com.pms.gapyeong.update.config.DownloadKey;
import com.pms.gapyeong.update.view.UpdateDialog;
import com.woosim.printer.WoosimImage;

public class LoginActivity extends BaseActivity {

	private TextInputEditText tietId;
	private TextInputEditText tietPassword;

	private String appURL;
	private Boolean DEVELOPER_MODE = true;
	private Device device = null;
	private boolean isUsim = true;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		changeStatusBarColor("#2BBDFF");

		if (DEVELOPER_MODE) {
			StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
			StrictMode.setThreadPolicy(policy);
		}
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login_layout);
		
		DisplayMetrics _dm = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(_dm);
		DEVICE_DPI = String.valueOf(_dm.widthPixels);	
		
		TelephonyManager tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE); 
		if (tm.getSimState() == TelephonyManager.SIM_STATE_ABSENT){
			isUsim = false;
		}

		try
		{
			device = getDeviceInfo();
		}
		catch(NullPointerException e)
		{
			if(Constants.DEBUG_PRINT_LOG){
				e.printStackTrace();
			}else{
				System.out.println("예외 발생");
			}
			isUsim = false;
		}
	}
	
	@Override
	protected void initLayoutSetting() {
		super.initLayoutSetting();

		tietId = (TextInputEditText) findViewById(R.id.textInputEditTextEmail);
		tietPassword = (TextInputEditText) findViewById(R.id.textInputEditTextPassword);
		Button btn_device_reg = findViewById(R.id.btn_device_reg);
		btn_device_reg.setPaintFlags(btn_device_reg.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

		btn_device_reg.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				String IMEI = getIMEI(LoginActivity.this);
				String USER_DEVICE_ID = Util.getDeviceID(LoginActivity.this);
				String sendMsg = "기기등록 요청 [가평]\n"+"디바이스 정보\n"+"모델명 : "+device.getModel()+"["+device.getOsVersion()+"]\n"+"DEVICE_ID : "+USER_DEVICE_ID+"\nIMEI : "+IMEI;
				Log.e("sendMsg",sendMsg);
				// SMS 발송
				Uri uri = Uri.parse("smsto:01063552023");
				Intent it = new Intent(Intent.ACTION_SENDTO, uri);
				it.putExtra("sms_body", sendMsg);
				startActivity(it);
			}
		});



		tietPassword.setOnKeyListener(new OnKeyListener() {
			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				// TODO Auto-generated method stub
				if(KeyEvent.ACTION_UP==event.getAction()&&keyCode==66){
					apiLoginCall();
				}
				return false;
			}
		});

		Button btnLoginConfirm = (Button)findViewById(R.id.btn_login_confirm);
		Button btnLoginCancel = (Button)findViewById(R.id.btn_login_cancel);

		String UserId = new Preference(this).getString(Constants.PREFERENCE_LOGIN_ID, "");

		if(!UserId.equals("")) {
			tietId.setText(UserId);
//			etLoginPass.setText("9000");
			tietPassword.requestFocus();
//			apiLoginCall();
		}
		
		btnLoginConfirm.setOnClickListener(this);
		btnLoginCancel.setOnClickListener(this);
		
	}
	
	private void apiLoginCall(){
		
		if (tietId.getText().length() <= 0 || tietPassword.getText().length() <= 0) {
			Toast.makeText(this, "아이디 또는 비밀번호를 입력해 주세요.", Toast.LENGTH_LONG).show();
			return ;
		}
		
		showProgressDialog();
		String eTel = null;
		String eModel = null;
		String eOsversion = null;
		if(device != null)
		{
			if(isUsim) {
				eTel			 = "0" + device.getMobile().substring(3, device.getMobile().length());
				eTel			 = eTel.substring(0,3) + "-" +eTel.substring(3,7) + "-" +eTel.substring(7,eTel.length());
			} else {
				eTel = "Usim is not";
			}
			eModel			 = device.getModel();
			eOsversion		 = device.getOsVersion();
		}



		String url = Constants.API_SERVER + "?" + Constants.METHOD +"="+ Constants.INTERFACEID_LOGIN
				   + "&EMP_CD=" + tietId.getText().toString()
				   + "&PWD=" + tietPassword.getText().toString()
				   + "&APP_VERSION=" + ApplicationInfo.getAppVersion(this)
				   + "&DEVICE_ID="+Util.getDeviceID(this)
				   + "&DEVICE_TEL="+eTel
				   + "&PLATFORM_TYPE="+eModel
				   + "&PLATFORM_OS="+eOsversion
				   + "&"+Constants.API_FORMAT +"="+ Constants.JSON_FORMAT;
		Log.d("111", " apiLoginCall url >>> " + url);
		AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>();        
		cb.url(url).type(JSONObject.class).weakHandler(this, "loginCallback").redirect(true).retry(3).fileCache(false).expire(-1);
		AQuery aq = new AQuery(this);
		aq.ajax(cb);	
		
	}

	@SuppressLint("MissingPermission")
	public String getIMEI(Activity activity) {
		TelephonyManager telephonyManager = (TelephonyManager) activity
				.getSystemService(Context.TELEPHONY_SERVICE);
		return telephonyManager.getDeviceId();
	}


	public void loginCallback(String url, JSONObject json, AjaxStatus status){
		Log.d(TAG, " loginCallback  json ====== " +json);
		
		closeProgressDialog();
		Device device = getDeviceInfo();
		int Len = 0;
		
		// successful ajax call          
        if(json != null){   
        	if("1".equals(json.optString("CD_RESULT"))){
				empName   		 = json.optString("KN_EMP");
				empCode  		 = json.optString("EMP_CD");
				empPhone	 	 = json.optString("PHONE");
				empTel		 	 = json.optString("TEL");
				if(isUsim) {
					empTel			 = "0" + device.getMobile().substring(3, device.getMobile().length());
					empTel			 = empTel.substring(0,3) + "-" +empTel.substring(3,7) + "-" +empTel.substring(7,empTel.length());
				}
				empSystemVersion = json.optString("APP_VERSION");
				parkCode  		 = json.optString("CD_PARK");
				
				Log.d(TAG, " empPhone >>> " + empPhone);
				Log.d(TAG, " empTel >>> " + empTel);
				
    			JSONObject res2 = json.optJSONObject("PARK_BUSINESS_INFO");
    			empBank_MSG = res2.optString("BANK_MSG");
    			empAccount_No = res2.optString("ACCOUNT_NO");
    			empKN_ACCOUNT = res2.optString("KN_ACCOUNT");
    			empKN_BANK = res2.optString("KN_BANK");
				empBusiness_TEL = res2.optString("TEL");

				Log.d(TAG, " empBusiness_TEL >>> " + empBusiness_TEL);
				BitmapFactory.Options options = new BitmapFactory.Options();
				options.inScaled = false;
				Bitmap bmp = BitmapFactory.decodeResource(getResources(), R.drawable.logo, options);
		        
				if (bmp == null) {
		          Log.e(TAG, "resource decoding is failed");
		          return;
		        
				}
		        
				empLogo_image = WoosimImage.printBitmap(0, 0, 320, 108, bmp);
				bmp.recycle();	
    			
				Len = 0;				
				
				// 스케쥴리스트 초기화
				if(getScheduleList() != null){
					getScheduleList().clear();
				}
				
				JSONArray jsonArr = json.optJSONArray("SCHEDULE_RESULT");
        		Len = jsonArr.length();
        		for (int i = 0; i < Len; i++) {
        			JSONObject res = jsonArr.optJSONObject(i);
        			Map<String,String> map = new HashMap<String,String>();
        			map.put("CD_PARK", 	   res.optString("CD_PARK"));
        			map.put("KN_PARK", 	   res.optString("KN_PARK"));
        			map.put("EMP_CD", 	   res.optString("EMP_CD"));
        			map.put("SCH_LEVEL",   res.optString("SCH_LEVEL"));
        			map.put("ETC_EMP_CD0", res.optString("ETC_EMP_CD0"));
        			map.put("ETC_EMP_CD1", res.optString("ETC_EMP_CD1"));
        			map.put("ETC_EMP_CD2", res.optString("ETC_EMP_CD2"));
        			map.put("ETC_EMP_CD3", res.optString("ETC_EMP_CD3"));
        			map.put("ETC_EMP_CD4", res.optString("ETC_EMP_CD4"));
        			map.put("ETC_EMP_CD5", res.optString("ETC_EMP_CD5"));
        			map.put("ETC_EMP_CD6", res.optString("ETC_EMP_CD6"));
        			map.put("ETC_EMP_CD7", res.optString("ETC_EMP_CD7"));
        			getScheduleList().add(map);        			
        		}
				
        		int myAppVersion     = Util.sToi(ApplicationInfo.getAppVersion(this).replace(".",""));
        		Log.d(TAG, " myAppVersion >>> " + myAppVersion);
				int serverAppVersion = Util.sToi(empSystemVersion.replace(".",""));
				Log.d(TAG, " serverAppVersion >>> " + serverAppVersion);
				
				if(serverAppVersion>myAppVersion){
					appURL = json.optString("APP_URL");
                    DownloadKey.apkUrl = appURL;
                    DownloadKey.version = empSystemVersion;
                    DownloadKey.changeLog ="최신 업데이트 파일이 있습니다. 다운로드 해주세요.";

                    startActivityForResult(new Intent(this,UpdateDialog.class),100);


					return;
	
				} else {
					new Preference(this).putString(Constants.PREFERENCE_LOGIN_ID,  tietId.getText().toString());
					new Preference(this).putString(Constants.PREFERENCE_LOGIN_PWD,  tietPassword.getText().toString());
					goToMainActivity();
				}
          } else {
        	// TODO 에러 팝업 처리
        	String KN_RESULT = json.optString("KN_RESULT");
        	Toast.makeText(this, KN_RESULT, Toast.LENGTH_LONG).show();
        	//Toast.makeText(this, Constants.DATA_EMPTY, Toast.LENGTH_LONG).show();
         }
      } else {
    	  Toast.makeText(this, Constants.DATA_FAIL, Toast.LENGTH_LONG).show();
      }
		
	}

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if(requestCode==100){
            finish();
        }
    }

    @Override
	public void onClick(View v) {
		
		super.onClick(v);
	
		switch(v.getId()){
		  case R.id.btn_login_confirm:
			 apiLoginCall();
			break;
			
		  case R.id.btn_login_cancel:
			twoButtonDialog(Constants.MAINACTIVITY_DESTROY_ACTIVITY,getStr(R.string.title_exit),getStr(R.string.msg_exit_app)).show();
			break;
		}
		
	}

	@Override
	protected void HandlerListener(Message msg) {
		// TODO Auto-generated method stub
		super.HandlerListener(msg);
	}

	@SuppressLint("MissingPermission")
    public Device getDeviceInfo() {
        Device device = null;

        // 1. mobile
        String mobile = null;
        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        Log.d("Result", "11" + telephonyManager.getLine1Number());
        if ( telephonyManager.getLine1Number() != null ) {
            Log.d("Result", "22" +telephonyManager.getLine1Number());
            mobile = telephonyManager.getLine1Number();
        }

        // 2. osVersion
        String osVersion = Build.VERSION.RELEASE;

        // 3. model
        String model = Build.MODEL;

        // 4. display
        String display = getDisplay(this);

        // 5. manufacturer
        String manufacturer = Build.MANUFACTURER;

        // 6. macAddress
        String macAddress = getMacAddress(this);

        device = new Device(mobile, osVersion, model, display, manufacturer, macAddress);

        return device;
    }
    
    /**
     *  get display resolution
     */
    private static String getDisplay(Context context) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        windowManager.getDefaultDisplay().getMetrics(displayMetrics);
        int deviceWidth = displayMetrics.widthPixels;
        int deviceHeight = displayMetrics.heightPixels;

        return deviceWidth + "x" + deviceHeight;
    }

    /**
     * get WiFi MAC address
     */
    private static String getMacAddress(Context context) {
        WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        WifiInfo info = wifiManager.getConnectionInfo();

        return info.getMacAddress();
    }

}
