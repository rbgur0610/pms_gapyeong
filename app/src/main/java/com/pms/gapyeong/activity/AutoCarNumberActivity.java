package com.pms.gapyeong.activity;

import java.io.File;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.pms.gapyeong.R;
import com.pms.gapyeong.common.Constants;
import com.pms.gapyeong.common.MsgUtil;
import com.pms.gapyeong.common.Util;
import com.pms.gapyeong.vo.ParkIO;

import keona.keona_platerecoglib.keona.keona_platerecoglib.camera.KeonaRecogLayout;
import keona.keona_platerecoglib.keona.keona_platerecoglib.camera.KeonaRecogMODE;
import keona.recogmode.handle.KeonaRecogHandleMessage;

import android.content.Intent;
import android.content.res.Configuration;
import android.hardware.Camera.Size;
import android.os.*;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.Toast;
import android.widget.FrameLayout.LayoutParams;
import android.widget.TextView;
import android.widget.ZoomControls;

/**
 * 자동차 번호 자동인식 화면 Activity
 * <p>
 * sss
 */
public class AutoCarNumberActivity extends BaseActivity {

    private final String TAG = this.getClass().getSimpleName();

    private String mImgPath;
    private String mImgName;
    private String mPioNumber;
    private String mCarNumber;

    private boolean bCarNum = false;
    private boolean bCarImg = false;
    private String CD_AREA = "";

    private KeonaRecogLayout mFrame;

    private TextView txt_result;

    private Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case KeonaRecogHandleMessage.RecogMessage_PlateNumber:

                    mCarNumber = msg.obj.toString();
                    txt_result.setText(mCarNumber);
                    if (mCarNumber.trim().length() > 4) {
                        Log.d(TAG, " mCarNumber >>> " + mCarNumber);
                        if (!bCarNum) {
                            Constants.AUTO_CAR_NUMBER = mCarNumber;
                            Log.d(TAG, " AUTO_CAR_NUMBER >>> " + Constants.AUTO_CAR_NUMBER);
                            bCarNum = true;
                        }
                        // 차량번호 추출 및 사진 저장 완료 시
                        if (bCarNum && bCarImg) {
                            Log.d(TAG, " RecogMessage_PlateNumber ");
                            apiParkIoListCall(Constants.AUTO_CAR_NUMBER, "I1", today, today);
                        }
                    }
                    break;

                case KeonaRecogHandleMessage.CameraControl_Zoom_In:
                    mFrame.CameraControl_ZoomIn();
                    break;

                case KeonaRecogHandleMessage.CameraControl_Zoom_Out:
                    mFrame.CameraControl_ZoomOut();
                    break;

                //카메라에, 사진 찍기를 요청
                case KeonaRecogHandleMessage.CameraControl_TakePicture:
                    mFrame.CameraControl_TakePicture();
                    break;

                //저장된 사진파일의 이름이 리턴됩니다
                case KeonaRecogHandleMessage.RecogMessage_TakePicture:
                    Toast.makeText(getApplicationContext(), "파일명 : " + msg.obj.toString() + " 저장되었습니다.", Toast.LENGTH_SHORT).show();
                    bCarImg = true;
                    // 차량번호 추출 및 사진 저장 완료 시
                    if (bCarNum && bCarImg) {
                        Log.d(TAG, " RecogMessage_TakePicture  ");
                        apiParkIoListCall(Constants.AUTO_CAR_NUMBER, "I1", today, today);
                    }
                    break;
                //카메라에서 지원되는 사진 사이즈를 전달
                case KeonaRecogHandleMessage.CameraControl_PictureSize_Get:
                    //*****************Example Code*********************
                    List<Size> list_CameraPictureSize = (List<Size>) msg.obj;
                    for (Size m : list_CameraPictureSize) {
                        Log.i("Keon-A", "Return Message PictureSize : " + m.width + "/" + m.height);
                    }
                    mHandler.sendEmptyMessage(KeonaRecogHandleMessage.CameraControl_PictureSize_Set);

                    break;
                //카메라에 사진 해상도를 적용
                case KeonaRecogHandleMessage.CameraControl_PictureSize_Set:
                    //*****************Example Code*********************
                    //mFrame.CameraControl_CameraPictureSize(1600,1200);
                    mFrame.CameraControl_CameraPictureSize(640, 480);
                    break;

                //라이브러리에서의 알림 메시지
                //case KeonaRecogHandleMessage.Notice:
                //	String str_Message = (String) msg.obj;
                //	Toast.makeText(getApplicationContext(), str_Message, Toast.LENGTH_SHORT).show();
                //	break;
            }


        }

        ;
    };

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        // TODO Auto-generated method stub
        super.onConfigurationChanged(newConfig);

        mFrame.Keona_ConfigurationChanged(newConfig);

        Log.d(TAG, "AutoCarNumberActivity : onConfigurationChanged");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
//        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.autocar_number_activity);

        mImgPath = Constants.IMG_SAVE_PATH + "/" + Util.getYmdhms("yyyyMMdd");
        Log.d(TAG, " mImgPath >>> " + mImgPath);

        File file = new File(mImgPath);
        if (!file.exists())  // 경로에 폴더가 있는지 확인
            file.mkdirs();

        txt_result = (TextView) findViewById(R.id.txt_result);

        mPioNumber = makePIONumber(parkCode, empCode, today);
        mImgName = mPioNumber + "_1.png";
        mFrame = new KeonaRecogLayout(this);
        FrameLayout.LayoutParams mFrameParam = (android.widget.FrameLayout.LayoutParams) new LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT);
        mFrame.setLayoutParams(mFrameParam);

        //인식 모드는 라이센스 키의 속성을 정합니다. 휴대전화로 인증을 하였다면 RECOG_CELLULAR를,
        //WIFI기기로 인증하시길 원한다면 RECOG_WIFI를 옵션에 넣어주세요.
        //아무것도 선택하지 않았다면 기본적으로 RECOG_CELLULAR가 됩니다.
        //mFrame.setKeonaReocog_Mode(KeonaRecogMODE.RECOG_WIFI);


//		mFrame.setKeonaOptions_PicturePath(mImgPath+"/auto_^yyyy^MM^dd^HH^mm^ss.jpg");	//사진 저장 경로 설정
        mFrame.setKeonaOptions_PicturePath(mImgPath + "/" + mImgName);    //사진 저장 경로 설정
        mFrame.setKeonaOptions_PictureMode(KeonaRecogMODE.OPTIONS_PictureSave_Immediately);    //인식후 자동 즉시 촬영
        mFrame.PrepareToRecog(mHandler);

        FrameLayout mFrame_Main = (FrameLayout) findViewById(R.id.frame_recog);
        mFrame_Main.addView(mFrame);

        ZoomControls mZoomCamera = (ZoomControls) findViewById(R.id.zoomControls_camerazoom);

        mZoomCamera.setOnZoomInClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                mHandler.sendEmptyMessage(KeonaRecogHandleMessage.CameraControl_Zoom_In);
            }
        });

        mZoomCamera.setOnZoomOutClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                mHandler.sendEmptyMessage(KeonaRecogHandleMessage.CameraControl_Zoom_Out);
            }
        });

        //사진찍기를 위한 버튼
		/*
		Button mTakePicture = (Button)findViewById(R.id.btn_takepictrue);
		mTakePicture.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub튼
				//사진찍기를 요청합니다.
				mHandler.sendEmptyMessage(KeonaRecogHandleMessage.CameraControl_TakePicture);
			}
		});
		*/

        Intent intent = getIntent();
        if (intent != null) {
            CD_AREA = intent.getStringExtra("CD_AREA");
        }


        Log.d(TAG, "AutoCarNumberActivity : onCreate");

        //mHandler.sendEmptyMessage(KeonaRecogHandleMessage.CameraControl_PictureSize_Set);

        Button btn_top_left = (Button) findViewById(R.id.btn_top_left);
        btn_top_left.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        Button btn_top_inputcar = (Button) findViewById(R.id.btn_top_inputcar);
        btn_top_inputcar.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                startInputCarNumber(CD_AREA);
            }
        });

    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();

        bCarNum = false;
        bCarImg = false;
        Log.d(TAG, "AutoCarNumberActivity : onResume");
    }

    @Override
    protected void onStart() {
        // TODO Auto-generated method stub
        super.onStart();
        //mHandler.sendEmptyMessage(KeonaRecogHandleMessage.CameraControl_PictureSize_Set);
        Log.d(TAG, "AutoCarNumberActivity : onStart");

    }

    @Override
    protected void onStop() {
        // TODO Auto-generated method stub
        super.onStop();

        mFrame.Keona_ResumeActivity();
        Log.d(TAG, "AutoCarNumberActivity : onStop");
    }

    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
        Log.d(TAG, "AutoCarNumberActivity : onPause");
    }

    @Override
    protected void onRestart() {
        // TODO Auto-generated method stub
        super.onRestart();
    }

    /**
     * 서버에 입차되었는지 요청한다.
     */
    private void apiParkIoListCall(String carNo, String codeState, String startDay, String endDay) {
        //showProgressDialog(false);

        String url = Constants.API_SERVER + "?" + Constants.METHOD + "=" + Constants.INTERFACEID_PARK_IO;
        String param = "&CD_PARK=" + parkCode
                + "&CAR_NO=" + carNo
                + "&CD_STATE=" + codeState
                + "&START_DAY=" + startDay
                + "&END_DAY=" + endDay
                + "&" + Constants.API_FORMAT + "=" + Constants.JSON_FORMAT;
        Log.d(TAG, " apiParkIoListCall url >>> " + url + param);

        AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>();
        cb.url(url + param).type(JSONObject.class).weakHandler(this, "parkIoListCallback").redirect(true).retry(3).fileCache(false).expire(-1);
        AQuery aq = new AQuery(this);
        aq.ajax(cb);
    }

    public void parkIoListCallback(String url, JSONObject json, AjaxStatus status) {

        closeProgressDialog();
        Log.d(TAG, " parkIoListCallback  json ====== " + json);

        // successful ajax call
        if (json != null) {
            String KN_RESULT = json.optString("KN_RESULT");
            if ("1".equals(json.optString("CD_RESULT"))) {
                JSONArray jsonArr = json.optJSONArray("RESULT");
                int Len = jsonArr.length();
                for (int i = 0; i < Len; i++) {
                    try {
                        JSONObject res = jsonArr.getJSONObject(i);
                        ParkIO info = new ParkIO(res.optString("PIO_NUM")
                                , res.optString("CD_PARK")
                                , res.optString("PIO_DAY")
                                , res.optString("CAR_NO")
                                , res.optString("CD_AREA")
                                , res.optString("PARK_TYPE")
                                , res.optString("DIS_CD")
                                , res.optString("TEL")
                                , res.optString("PARK_IN_AMT")
                                , res.optString("PARK_OUT_AMT")
                                , res.optString("PARK_AMT")
                                , res.optString("ADV_IN_AMT")
                                , res.optString("ADV_OUT_AMT")
                                , res.optString("ADV_AMT")
                                , res.optString("DIS_IN_AMT")
                                , res.optString("DIS_OUT_AMT")
                                , res.optString("DIS_AMT")
                                , res.optString("CASH_IN_AMT")
                                , res.optString("CASH_OUT_AMT")
                                , res.optString("CASH_AMT")
                                , res.optString("CARD_IN_AMT")
                                , res.optString("CARD_OUT_AMT")
                                , res.optString("CARD_AMT")
                                , res.optString("TRUNC_IN_AMT")
                                , res.optString("TRUNC_OUT_AMT")
                                , res.optString("TRUNC_AMT")
                                , res.optString("COUPON_AMT")
                                , res.optString("RETURN_AMT")
                                , res.optString("YET_AMT")
                                , res.optString("REAL_AMT")
                                , res.optString("PARK_TIME")
                                , res.optString("PARK_IN")
                                , res.optString("PARK_OUT")
                                , res.optString("CARD_NO")
                                , res.optString("CARD_COMPANY")
                                , res.optString("APPROVAL_NO")
                                , res.optString("APPROVAL_DATE")
                                , res.optString("CHARGE_TYPE")
                                , res.optString("KN_COMMENT"));

                        // 입차 리스트에 있으면 출차 등록으로 이동
                        if (info.getCarNo().contains(Constants.AUTO_CAR_NUMBER)) {
                            Intent intent = new Intent(AutoCarNumberActivity.this, ExitCommitActivity.class);
                            intent.putExtra("parkIO", info);
                            startActivity(intent);
                            finish();
                            return;
                        }
                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        if (Constants.DEBUG_PRINT_LOG) {
                            e.printStackTrace();
                        } else {
                            System.out.println("예외 발생");
                        }
                    }
                }  // end for

            } else {
                // MsgUtil.ToastMessage(this, KN_RESULT, Toast.LENGTH_LONG);
                // 입차 리스트에 없으므로 입차 등록으로 이동
                Intent intent = new Intent(AutoCarNumberActivity.this, EntranceTimeActivity.class);
                intent.putExtra(Constants.CAR_INPUT_MODE, Constants.CAR_AUTO);
                intent.putExtra("CD_AREA", CD_AREA);
                // Constants.AUTO_CAR_NUMBER = mCarNumberKeep;
                Constants.AUTO_PIO_NUMBER = mPioNumber;
                Constants.AUTO_CAR_IMG_PATH = mImgPath;
                Constants.AUTO_CAR_IMG_NAME = mImgName;
                startActivity(intent);
                finish();
                return;
            }
        } else {
            MsgUtil.ToastMessage(AutoCarNumberActivity.this, Constants.DATA_FAIL, Toast.LENGTH_LONG);
        }

    }

    private void startInputCarNumber(String carArea) {

        try {

            Intent i = new Intent(this, InputCarNumerActivity.class);
            i.putExtra(Constants.CAR_INPUT_MODE, "INCAR");
            i.putExtra("CD_AREA", carArea);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(i);
            overridePendingTransition(0, 0);
        } catch (NullPointerException e) {
            System.out.println("NullPointerException 예외 발생");
        }
    }
}