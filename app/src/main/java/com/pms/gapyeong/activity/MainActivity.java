package com.pms.gapyeong.activity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import android.net.Uri;
import android.util.Log;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.pms.gapyeong.R;
import com.pms.gapyeong.common.Constants;
import com.pms.gapyeong.common.DeviceInfo;
import com.pms.gapyeong.common.MsgUtil;
import com.pms.gapyeong.common.Preferences;
import com.pms.gapyeong.common.Util;
import com.pms.gapyeong.database.StatusBoardHelper;
import com.pms.gapyeong.printer.BluetoothDeviceListActivity;
import com.pms.gapyeong.printer.BluetoothPrintService;
import com.pms.gapyeong.vo.Park;
import com.pms.gapyeong.vo.ParkInfoDetail;
import com.woosim.printer.WoosimService;

public class MainActivity extends BaseActivity {
    private Dialog carParkInfoDialog;
    private TextView tvCarpark;
    private Button btn_sel_park;
    private Button btn_car_io;
    private Button btnExit;
    private Button btnManage;
    private Button btnTicket;
    private Button btnUnpaid;
    private TextView tv_top_title;
    private Button btn_top_left;
    private Button btn_top_right;

    private Button btn_auto_car;

    private Park parkInfo;
    private Button tvTimeDc;
    private Button tvParkType;
    private Button tvTicketPrice;
    private Button tvTicketDc;
    private TextView worker;
    RelativeLayout dimm;
    LinearLayout main;

    private static boolean isScreenOnOff = false;
    private int iChoicePark = -1;


    // Key names received from the BluetoothPrintService Handler
    public final String DEVICE_NAME = "device_name";
    public final String TOAST = "toast";


    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.main_layout);

        // Get local Bluetooth adapter
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        // If the adapter is null, then Bluetooth is not supported
        if (mBluetoothAdapter == null) {
            Toast.makeText(this, R.string.toast_bt_na, Toast.LENGTH_LONG).show();
            finish();
            return;
        }

        // Initialize the BluetoothPrintService to perform bluetooth connections
        mPrintService = new BluetoothPrintService(this, mHandler);
        mWoosim = new WoosimService(mHandler);
    }

    @Override
    protected void HandlerListener(Message msg) {
        // TODO Auto-generated method stub
        super.HandlerListener(msg);
        switch (msg.what) {
            case MESSAGE_DEVICE_NAME:
                // save the connected device's name
                mConnectedDeviceName = msg.getData().getString(DEVICE_NAME);
                Log.i("HandlerListener", "MESSAGE_DEVICE_NAME ::: " + mConnectedDeviceName);
                MsgUtil.ToastMessage(this, "Connected to " + mConnectedDeviceName);
                break;
            case MESSAGE_TOAST:
                String message = msg.getData().getString(TOAST);
                Log.i("HandlerListener", "MESSAGE_TOAST ::: " + message);
                MsgUtil.ToastMessage(this, message);
                break;
            case MESSAGE_READ:
                Log.i("HandlerListener", "MESSAGE_READ MAIN ACTIVITY");
                mWoosim.processRcvData((byte[]) msg.obj, msg.arg1);
                break;
        }

    }

    @Override
    protected void onStart() {
        // TODO Auto-generated method stub
        super.onStart();

        setupPrint();
    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();

        // 주차장 정보 요청 > 입출차시 새로고침되도록.
        apiParkInfoCall(today);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onMenuItemSelected(int featureId, MenuItem item) {

        switch (item.getItemId()) {
            case R.id.findBluetooth:
                if (mPrintService == null || mBluetoothAdapter == null) {
                    setupPrint();
                } else {
                    if (mPrintService.getState() == BluetoothPrintService.STATE_CONNECTED && mBluetoothAdapter.isEnabled()) {
                        MsgUtil.ToastMessage(this, mConnectedDeviceName + " 프린터가 연결되어있습니다.");
                    } else {
                        setupPrint();
                    }
                }
                break;
            default:
                break;
        }
        return super.onMenuItemSelected(featureId, item);
    }


    @Override
    protected void initLayoutSetting() {

        super.initLayoutSetting();

        dimm = (RelativeLayout) findViewById(R.id.dimm);
        main = (LinearLayout) findViewById(R.id.main);
        tvCarpark = (TextView) findViewById(R.id.tv_carpark);
        btn_sel_park = (Button) findViewById(R.id.btn_sel_park);
        btn_car_io = (Button) findViewById(R.id.btn_car_io);
        btnExit = (Button) findViewById(R.id.btn_exit);
        btnManage = (Button) findViewById(R.id.btn_manage);
        btnTicket = (Button) findViewById(R.id.btn_ticket);
        btnUnpaid = (Button) findViewById(R.id.btn_unpaid);
        tv_top_title = (TextView) findViewById(R.id.tv_top_title);
        btn_top_left = (Button) findViewById(R.id.btn_top_left);
        btn_top_left.setBackgroundResource(R.drawable.btn_top_press);
        btn_top_right = (Button) findViewById(R.id.btn_top_right);
        worker = (TextView) findViewById(R.id.worker);
        btn_auto_car = (Button) findViewById(R.id.btn_auto_car);

        btn_top_right.setVisibility(View.VISIBLE);
        btn_top_left.setVisibility(View.VISIBLE);
        btn_top_right.setOnClickListener(this);
        worker.setOnClickListener(this);
        btn_top_left.setOnClickListener(this);
        tvCarpark.setOnClickListener(this);
        btn_sel_park.setOnClickListener(this);
        btnExit.setOnClickListener(this);
        btnManage.setOnClickListener(this);
        btnTicket.setOnClickListener(this);
        btnUnpaid.setOnClickListener(this);
        dimm.setOnClickListener(this);

        btn_car_io.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, StatusBoardActivity.class));
            }
        });

        //2016-10-25 앱-번호인식 버튼 활성화요청(메인화면,입출차,면수선택후 우측상단의 번호인식 활성화) 메인화면 번호인식 비활성화
        btn_auto_car.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //클릭 시 아무 반응없게
                Intent mIntent = new Intent(MainActivity.this, AutoCarNumberActivity.class);
                startActivity(mIntent);
            }
        });

        tv_top_title.setText("메인화면");
        // btn_auto_car.setEnabled(false);  //번호인식 버튼 비활성화
        setScreenOnOff(isScreenOnOff);

        Button btn_remote = (Button) findViewById(R.id.btn_remote);
        Button btn_payment = (Button) findViewById(R.id.btn_payment);

        btn_remote.setVisibility(View.VISIBLE);
        btn_payment.setVisibility(View.VISIBLE);


        btn_remote.setOnClickListener(this);
        btn_payment.setOnClickListener(this);
    }

    private void setScreenOnOff(boolean val) {
        if (isScreenOnOff) {
            dimm.bringToFront();
            btn_top_left.setText("고정\n해제");
        } else {
            main.bringToFront();
            btn_top_left.setText("화면\n고정");
        }
    }

    private void setupPrint() {

        /**````
         **/
        if (mPrintService == null) {
            mPrintService = new BluetoothPrintService(this, mHandler);
        } else if (mBluetoothAdapter == null) {
            mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        }

        if (!mBluetoothAdapter.isEnabled()) {
            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
        } else {
            Log.i(TAG, "mPrintService.getState()==" + mPrintService.getState());
            if (mPrintService.getState() == BluetoothPrintService.STATE_LISTEN || mPrintService.getState() == BluetoothPrintService.STATE_NONE) {
                Intent serverIntent = new Intent(this, BluetoothDeviceListActivity.class);
                startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE_SECURE);
            }
        }

    }

    @Override
    public void onClick(View v) {

        super.onClick(v);

        switch (v.getId()) {
            case R.id.btn_remote:
                callPlayStore("com.teamviewer.quicksupport.market");
                break;
            case R.id.btn_payment:
                callPlayStore("com.mcpay.icpayon");
                break;
            case R.id.worker:
                userInfoDialog();
                break;
            case R.id.tv_carpark:
                apiParkDetailCall();
                break;
            case R.id.btn_sel_park:

                List<String> list = new ArrayList<String>();
                for (Map<String, String> map : super.getScheduleList()) {
                    list.add(map.get("KN_PARK"));
                }

                final CharSequence[] items = list.toArray(new CharSequence[list.size()]);

                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle("주차장 선택");
                builder.setSingleChoiceItems(items, -1, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item) {
                        iChoicePark = item;
                    }
                });
                builder.setPositiveButton(R.string.btn_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Log.i(TAG, " iChoicePark ::: " + iChoicePark);
                        if (iChoicePark == -1) {
                            MsgUtil.ToastMessage(MainActivity.this, "주차장을 선택해주세요.");
                        } else {
                            requestParkSelect(iChoicePark);
                            iChoicePark = -1;
                        }
                    }
                }).setNegativeButton(R.string.btn_cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                AlertDialog alert = builder.create();
                alert.setCancelable(false);
                alert.show();
                break;

            case R.id.btn_exit:
                startActivity(new Intent(this, ExitManageActivity.class));
                break;
            case R.id.btn_ticket:
                startActivity(new Intent(this, TicketManageActivity.class));
                break;
            case R.id.btn_unpaid:
                startActivity(new Intent(this, UnpaidManagerActivity.class));
                break;
            case R.id.btn_manage:
                startActivity(new Intent(this, CloseManageActivity.class));
                break;
            case R.id.btn_top_left:
                isScreenOnOff = !isScreenOnOff;

                if (isScreenOnOff) {
                    setToast("화면이 고정 되었습니다.");
                } else {
                    setToast("화면고정이 해제 되었습니다.");
                }

                setScreenOnOff(isScreenOnOff);
                break;

            case R.id.btn_top_right:
                onBackPressed();
                break;

            case R.id.tvTicketDc:
                setCommonDialog("정기권할인", "DC", Constants.COMMON_TYPE_TIME_DC, tvTicketDc);
                break;
            case R.id.tvParkType:
                setCommonDialog("주차종류", "PY", Constants.COMMON_TYPE_PARK_TYPE, tvParkType);
                break;
            case R.id.tvTimeDc:
                setCommonDialog("시간권할인", "DC", Constants.COMMON_TYPE_TIME_DC, tvTimeDc);
                break;
        }

    }

    private void callPlayStore(String appPackageName) {
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        } catch (android.content.ActivityNotFoundException anfe) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
        }
    }

    private String subStringTime(String t) {
        String sHh = t.substring(0, 2);
        String sMm = t.substring(2, 4);

        String eHh = t.substring(4, 6);
        String eMm = t.substring(6);
        return sHh + ":" + sMm + " ~ " + eHh + ":" + eMm;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CONNECT_DEVICE_SECURE:
                Log.e("requestCode", "REQUEST_CONNECT_DEVICE_SECURE");
                // When DeviceListActivity returns with a device to connect
                if (resultCode == Activity.RESULT_OK) {
                    connectDevice(data, true);
                }
                break;
            case REQUEST_CONNECT_DEVICE_INSECURE:
                Log.e("requestCode", "REQUEST_CONNECT_DEVICE_INSECURE");
                // When DeviceListActivity returns with a device to connect
                if (resultCode == Activity.RESULT_OK) {
                    connectDevice(data, false);
                }
                break;
            case REQUEST_ENABLE_BT:
                Log.e(TAG, "REQUEST_ENABLE_BT ::: " + resultCode);
                // When the request to enable Bluetooth returns
                if (resultCode == Activity.RESULT_OK) {
                    // Bluetooth is now enabled, so set up a print
                    setupPrint();
                } else {
                    // User did not enable Bluetooth or an error occurred
                    Log.d(TAG, "bluetooth not enabled");
                    Toast.makeText(this, R.string.bt_not_enabled_leaving, Toast.LENGTH_SHORT).show();
                }
        }
    }

    private void connectDevice(Intent data, boolean secure) {
        try{
            // Get the device MAC address
            mBluetoothAddress = data.getExtras().getString(BluetoothDeviceListActivity.EXTRA_DEVICE_ADDRESS);
            // Get the BLuetoothDevice object
            BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(mBluetoothAddress);
            // Attempt to connect to the device
            mPrintService.connect(device, secure);
        } catch (IllegalArgumentException e) {
            System.out.println("IllegalArgumentException 예외 발생");
        } catch (NullPointerException e) {
            System.out.println("NullPointerException 예외 발생");
        }

    }

    private void apiParkDetailCall() {
        showProgressDialog();

        String url = Constants.API_SERVER + "?" + Constants.METHOD + "=" + Constants.INTERFACEID_PARKDETAIL
                + "&CD_PARK=" + parkCode
                + "&" + Constants.API_FORMAT + "=" + Constants.JSON_FORMAT;
        Log.d(TAG, " apiParkDetailCall url >>> " + url);

        AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>();
        cb.url(url).type(JSONObject.class).weakHandler(this, "parkDetailCallback").redirect(true).retry(3).fileCache(false).expire(-1);
        AQuery aq = new AQuery(this);
        aq.ajax(cb);
    }

    public void parkDetailCallback(String url, JSONObject json, AjaxStatus status) {
        closeProgressDialog();
        int Len = 0;
        Log.d(TAG, " parkDetailCallback  json ====== " + json);
        if (json != null) {
            //successful ajax call, show status code and json content
            if ("1".equals(json.optString("CD_RESULT"))) {
                JSONArray jsonArr = json.optJSONArray("RESULT");
                Len = jsonArr.length();
                ParkInfoDetail pid = null;
                for (int i = 0; i < Len; i++) {
                    JSONObject res = jsonArr.optJSONObject(i);
                    pid = new ParkInfoDetail(
                            res.optString("CD_PARK")
                            , res.optString("KN_PARK")
                            , res.optString("CD_AREA")
                            , res.optString("IN_SERVICE")
                            , res.optString("OUT_SERVICE")
                            , res.optString("RETURN_SERVICE")
                            , res.optString("BASE_SECTION")
                            , res.optString("BASE_AMT")
                            , res.optString("REPEAT_SECTION")
                            , res.optString("REPEAT_AMT")
                            , res.optString("MAX_AMT")
                            , res.optString("TICKET_PRINT")
                            , res.optString("HOLIDAY_WORKTIME")
                            , res.optString("GENERAL_WORKTIME")
                            , res.optString("TICKET_AMT")
                            , res.optString("IN_PRINT")
                            , res.optString("OUT_PRINT"));
                }

                if (pid != null)
                    carParkInfoDialog(pid).show();
            } else {
                //ajax error, show error code
                String KN_RESULT = json.optString("KN_RESULT");
                MsgUtil.ToastMessage(MainActivity.this, KN_RESULT);
            }
        } else {
            MsgUtil.ToastMessage(MainActivity.this, Constants.DATA_FAIL);
        }
    }

    private Dialog carParkInfoDialog(ParkInfoDetail info) {

        carParkInfoDialog = new Dialog(this,
                android.R.style.Theme_Translucent_NoTitleBar);
        carParkInfoDialog.setContentView(R.layout.car_park_info_layout);
        carParkInfoDialog.setCancelable(true);

        ((TextView) carParkInfoDialog.findViewById(R.id.tv_top_title)).setText("주차장정보");

        ((Button) carParkInfoDialog.findViewById(R.id.btn_top_right)).setVisibility(View.VISIBLE);
        ((Button) carParkInfoDialog.findViewById(R.id.btn_top_right)).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                carParkInfoDialog.dismiss();
            }
        });

        ((TextView) carParkInfoDialog.findViewById(R.id.detail_parkname)).setText(info.getKnPark());
        ((TextView) carParkInfoDialog.findViewById(R.id.detail_park_board_number)).setText(info.getCodeArea() + "대");


        try {
            ((TextView) carParkInfoDialog.findViewById(R.id.weekday_park_time)).setText(subStringTime(info.getWeekdayTime()));
            ((TextView) carParkInfoDialog.findViewById(R.id.hollyday_park_time)).setText(subStringTime(info.getHolidayTime()));
        } catch (NullPointerException e) {
            // TODO: 시간값 체크시 없으면 에러나는거 때문에 걸어놨습니다.
            if (Constants.DEBUG_PRINT_LOG) {
                e.printStackTrace();
            } else {
                System.out.println("예외 발생");
            }
        }


        final TextView tvTimePrice = (TextView) carParkInfoDialog.findViewById(R.id.tvTimePrice);


        if ("2".equals(parkClass)) {
            setCommonItem("AM2", Constants.COMMON_TYPE_CHARGE_TYPE, tvTimePrice);
        } else if ("3".equals(parkClass)) {
            setCommonItem("AM3", Constants.COMMON_TYPE_CHARGE_TYPE, tvTimePrice);
        } else if ("4".equals(parkClass)) {
            setCommonItem("AM4", Constants.COMMON_TYPE_CHARGE_TYPE, tvTimePrice);
        } else if ("5".equals(parkClass)) {
            setCommonItem("AM5", Constants.COMMON_TYPE_CHARGE_TYPE, tvTimePrice);
        } else if ("6".equals(parkClass)) {
            setCommonItem("AM6", Constants.COMMON_TYPE_CHARGE_TYPE, tvTimePrice);
        }

        //setCommonItem("AM2", Constants.COMMON_TYPE_CHARGE_TYPE, tvTimePrice);

        ((TextView) carParkInfoDialog.findViewById(R.id.tvInformation))
                .setText(info.getInPrint() + "\n-------------------------------------\n"
                        + info.getOutPrint() + "\n-------------------------------------\n"
                        + info.getTicketPrint());

        tvTimeDc = (Button) carParkInfoDialog.findViewById(R.id.tvTimeDc);
        tvParkType = (Button) carParkInfoDialog.findViewById(R.id.tvParkType);
        tvTicketPrice = (Button) carParkInfoDialog.findViewById(R.id.tvTicketPrice);
        tvTicketDc = (Button) carParkInfoDialog.findViewById(R.id.tvTicketDc);

        setCommonItem("DC", Constants.COMMON_TYPE_TIME_DC, tvTicketDc);
        setCommonItem("PY", Constants.COMMON_TYPE_PARK_TYPE, tvParkType);
        setCommonItem("DC", Constants.COMMON_TYPE_CHARGE_DISCOUNT, tvTimeDc);
        tvTicketPrice.setText(Util.addComma(info.getTicketAmt()) + "원");
        tvTimeDc.setOnClickListener(this);
        tvParkType.setOnClickListener(this);
        tvTicketPrice.setOnClickListener(this);
        tvTicketDc.setOnClickListener(this);

        return carParkInfoDialog;
    }

    private void requestParkSelect(int index) {
        Log.d(TAG, " getScheduleList  SIZE ::::: " + super.getScheduleList().size());
        Log.d(TAG, " index ::::: " + index);
        if (super.getScheduleList().size() >= index) {
            Map<String, String> map = super.getScheduleList().get(index);
            Log.d(TAG, " schedule ::::: " + map);
            Log.d(TAG, " CD_PARK ::::: " + map.get("CD_PARK"));
            parkCode = map.get("CD_PARK");
            // 주차장 정보를 변경하면 스케쥴 체크 로직 실행...
            apiScheduleCheckCall();
        } else {
            MsgUtil.ToastMessage(MainActivity.this, "주차장을 다시 선택해주세요.");
        }
    }

    private void apiScheduleCheckCall() {

        showProgressDialog();

        String url = Constants.API_SERVER + "?" + Constants.METHOD + "=" + Constants.INTERFACEID_SCHEDULE_CHECK
                + "&EMP_CD=" + empCode
                + "&CD_PARK=" + parkCode
                + "&DEVICE_ID=" + Util.getDeviceID(this)
                + "&" + Constants.API_FORMAT + "=" + Constants.JSON_FORMAT;
        Log.d("333", " apiScheduleCheckCall url >>> " + url);
        AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>();
        cb.url(url).type(JSONObject.class).weakHandler(this, "scheduleCheckCallback").redirect(true).retry(3).fileCache(false).expire(-1);
        AQuery aq = new AQuery(this);
        aq.ajax(cb);

    }

    public void scheduleCheckCallback(String url, JSONObject json, AjaxStatus status) {
        Log.d(TAG, " scheduleCheckCallback  json ====== " + json);

        closeProgressDialog();

        int Len = 0;

        // successful ajax call
        if (json != null) {
            if ("1".equals(json.optString("CD_RESULT"))) {

                // 스케쥴리스트 초기화
                if (getScheduleList() != null) {
                    getScheduleList().clear();
                }

                JSONArray jsonArr = json.optJSONArray("SCHEDULE_RESULT");
                Len = jsonArr.length();
                for (int i = 0; i < Len; i++) {
                    JSONObject res = jsonArr.optJSONObject(i);
                    Map<String, String> map = new HashMap<String, String>();
                    map.put("CD_PARK", res.optString("CD_PARK"));
                    map.put("KN_PARK", res.optString("KN_PARK"));
                    map.put("EMP_CD", res.optString("EMP_CD"));
                    map.put("SCH_LEVEL", res.optString("SCH_LEVEL"));
                    map.put("ETC_EMP_CD0", res.optString("ETC_EMP_CD0"));
                    map.put("ETC_EMP_CD1", res.optString("ETC_EMP_CD1"));
                    map.put("ETC_EMP_CD2", res.optString("ETC_EMP_CD2"));
                    map.put("ETC_EMP_CD3", res.optString("ETC_EMP_CD3"));
                    map.put("ETC_EMP_CD4", res.optString("ETC_EMP_CD4"));
                    map.put("ETC_EMP_CD5", res.optString("ETC_EMP_CD5"));
                    map.put("ETC_EMP_CD6", res.optString("ETC_EMP_CD6"));
                    map.put("ETC_EMP_CD7", res.optString("ETC_EMP_CD7"));
                    getScheduleList().add(map);
                }

                apiParkInfoCall(today);

            } else {
                // TODO 에러 팝업 처리
                String KN_RESULT = json.optString("KN_RESULT");
                Toast.makeText(this, KN_RESULT, Toast.LENGTH_LONG).show();
                //Toast.makeText(this, Constants.DATA_EMPTY, Toast.LENGTH_LONG).show();
            }

        } else {
            Toast.makeText(this, Constants.DATA_FAIL, Toast.LENGTH_LONG).show();
        }

    }


    private void apiParkInfoCall(String date) {

        if (parkCode == null) {
            setToast("앱이 비정상적 데이터로 인하여 종료됩니다. 다시시작해 주세요.");
            setDestroyList();
            return;
        }

        showProgressDialog();

        // AQUtility.cleanCacheAsync(this);
        String url = Constants.API_SERVER + "?" + Constants.METHOD + "=" + Constants.INTERFACEID_PARKINFO
                + "&CD_PARK=" + parkCode
                + "&WORK_DAY=" + date
                + "&" + Constants.API_FORMAT + "=" + Constants.JSON_FORMAT;

        Log.d(TAG, " apiParkInfoCall url >>> " + url);

        AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>();
//		cb.url(url).type(JSONObject.class).weakHandler(this, "parkInfoCallback").redirect(true).fileCache(false).expire(-1);
        cb.url(url).type(JSONObject.class).weakHandler(this, "parkInfoCallback").redirect(true).retry(3).fileCache(false).expire(-1);
        AQuery aq = new AQuery(this);
        aq.ajax(cb);

    }

    public void parkInfoCallback(String url, JSONObject json, AjaxStatus status) {
        Log.d("MainActivity", " parkInfoCallback  json ====== " + json);
        closeProgressDialog();

        int Len = 0;

        // successful ajax call
        if (json != null) {
            if ("1".equals(json.optString("CD_RESULT"))) {
                JSONArray jsonArr = json.optJSONArray("RESULT");
                Len = jsonArr.length();
                boolean isPicture = false;
                for (int i = 0; i < Len; i++) {
                    JSONObject res = jsonArr.optJSONObject(i);
                    parkInfo = new Park(res.optString("CD_PARK")
                            , res.optString("KN_PARK")
                            , res.optString("EMP_CD")
                            , res.optString("CD_AREA")
                            , res.optString("CD_STATE")
                            , res.optString("START_TIME")
                            , res.optString("END_TIME")
                            , res.optString("SERVICE_START_TIME", "0")
                            , res.optString("SERVICE_END_TIME", "0")
                            , res.optString("IN_QTY")
                            , res.optString("AVAILABLE_QTY")
                            , res.optString("SUM_IN_QTY")
                            , res.optString("SUM_OUT_QTY")
                            , res.optString("KN_PARK")
                            , res.optString("TICKET_AMT")
                            , res.optString("CATNUMBER")
                            , res.optString("KN_BUSINESS")
                            , res.optString("BANK_MSG")
                            , res.optString("ACCOUNT_NO")
                            , res.optString("KN_BANK")
                            , res.optString("KN_ACCOUNT")
                            , res.optString("COPY_RIGHT")
                            , res.optString("TEL")
                            , res.optString("BIZ_NO")
                            , res.optString("IN_PRINT")
                            , res.optString("OUT_PRINT")
                            , res.optString("TICKET_PRINT")
                            , res.optString("DAY_AMT")
                            , res.optString("MAX_AMT")
                            , res.optString("IN_SERVICE")
                            , res.optString("OUT_SERVICE")
                            , res.optString("RETURN_SERVICE")
                            , res.optString("BASE_SECTION")
                            , res.optString("BASE_AMT")
                            , res.optString("REPEAT_SECTION")
                            , res.optString("REPEAT_AMT")
                            , res.optString("TRUNC_METHOD")
                            , res.optString("TRUNC_UNIT")
                            , res.optString("ADV_BOUND_TIME")
                            , res.optString("GROUP_CD"));

                    isPicture = res.optInt("PICTURE_CNT", 0) > 0 ? true : false;

                } // end for

                tvCarpark.setText(parkInfo.getParkName());
                worker.setText(empName);

                ((TextView) findViewById(R.id.total_car)).setText(parkInfo.getCodeArea() + "대");


                ((TextView) findViewById(R.id.work_time)).setText(parkInfo.getStartTime().substring(0, 2)
                        + ":"
                        + parkInfo.getStartTime().substring(2)
                        + "~"
                        + parkInfo.getEndTime().substring(0, 2)
                        + ":"
                        + parkInfo.getEndTime().substring(2));

                String inQty = parkInfo.getInQty() == null ? "0대" : parkInfo.getInQty() + "대";
                String emptyQty = parkInfo.getAvailableQty() == null ? "0대" : parkInfo.getAvailableQty() + "대";
                String sumQty = parkInfo.getSumInQty() == null ? "0대" : parkInfo.getSumInQty() + "대";
                String sumOutQty = parkInfo.getSumOutQty() == null ? "0대" : parkInfo.getSumOutQty() + "대";

                ((TextView) findViewById(R.id.input_qty)).setText(inQty);
                ((TextView) findViewById(R.id.empty_qty)).setText(emptyQty);
                ((TextView) findViewById(R.id.sum_io_qty)).setText(sumQty);
                ((TextView) findViewById(R.id.sum_out_qty)).setText(sumOutQty);

                // 근무시간
                workStartTime = parkInfo.getStartTime();
                // 근무종료시간
                workEndTime = parkInfo.getEndTime();

                // 무료 주차 시작 시간
                serviceStartTime = Util.isNVL(parkInfo.getServiceStartTime(), "0");
                // 무료 주차 종료시간
                serviceEndTime = Util.isNVL(parkInfo.getServiceEndTime(), "0");
                // 주차장
                parkName = parkInfo.getKnPark();

                // 주차장  cd_state
                parkCdstate = parkInfo.getCodeState();

                // CAT NUMBER[카드사용시]
                catNumber = parkInfo.getCatNumber();

                // 주차장 급지
                parkClass = parkInfo.getCodeState();

                empGroupCD = parkInfo.getGroup_cd();

                empTicket_print = parkInfo.getTicketPrint();
                //Log.d(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>", empGroupCD);

                // 요금종류코드
                String chargeClassCode = "";

                if ("2".equals(parkClass)) {
                    chargeClassCode = "AM2";
                } else if ("3".equals(parkClass)) {
                    chargeClassCode = "AM3";
                } else if ("4".equals(parkClass)) {
                    chargeClassCode = "AM4";
                } else if ("5".equals(parkClass)) {
                    chargeClassCode = "AM5";
                } else if ("6".equals(parkClass)) {
                    chargeClassCode = "AM6";
                }

                //chargeClassCode = "AM2";

                /*** 2015.11.13 - 코드정보에서 불러오는 것으로 변경...
                 // 주차요금 최대금액
                 parkMaxAmt = Util.sToi(parkInfo.getMaxAmt(),0);
                 // 일일권 금액
                 dayAmt     = Util.sToi(parkInfo.getDayAmt(),0);
                 // 정기권 금액
                 ticketAmt  = Util.isNVL(parkInfo.getTicketAmt(),"0");
                 // 기본구간 ex)30분
                 baseSection = parkInfo.getBaseSection();
                 // 기본금액 ex)700원
                 baseAmt = parkInfo.getBaseAmt();
                 // 반복구간 ex)10분
                 repeatSection = parkInfo.getRepeatSection();
                 // 반복금액 ex)300원
                 repeatAmt = parkInfo.getRepeatAmt();
                 ***/
                int codeLen = codeJsonArr.length();
                for (int i = 0; i < codeLen; i++) {
                    try {
                        JSONObject res = codeJsonArr.getJSONObject(i);
                        if (res.optString("CD_CLASS").equals(chargeClassCode)) {
                            // Log.d(TAG, " DIS_AMT >>>> " + res.optString("DIS_AMT") +  " CD_VALUE >>>> " + res.optString("CD_VALUE"));
                            // 기본구간
                            baseSection = res.optString("BASE_TIME", "0");
                            // 기본금액
                            baseAmt = res.optString("BASE_AMT", "0");
                            // 반복구간
                            repeatSection = res.optString("DIS_TIME", "0");
                            // 반복금액
                            repeatAmt = res.optString("DIS_AMT", "0");
                            // 주차최대금액
                            parkMaxAmt = res.optInt("CD_VALUE", 0);
                            // 일일권 금액
                            dayAmt = res.optInt("CD_VALUE2", 0);
                            // 정기권 금액
                            ticketAmt = res.optString("CD_VALUE3", "0");
                            break;
                        }


                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        if (Constants.DEBUG_PRINT_LOG) {
                            e.printStackTrace();
                        } else {
                            System.out.println("예외 발생");
                        }
                    }
                } // end for

                Log.d(TAG, " baseSection >>> " + baseSection);
                Log.d(TAG, " baseAmt >>> " + baseAmt);
                Log.d(TAG, " repeatSection >>> " + repeatSection);
                Log.d(TAG, " repeatAmt >>> " + repeatAmt);
                Log.d(TAG, " parkMaxAmt >>> " + parkMaxAmt);
                Log.d(TAG, " dayAmt >>> " + dayAmt);
                Log.d(TAG, " ticketAmt >>> " + ticketAmt);

                // 입차서비스 구간(분)
                inService = parkInfo.getInService();
                // 출차서비스 구간(분)
                outService = parkInfo.getOutService();
                // 회차서비스 구간(분)
                returnService = parkInfo.getReturnService();
                // 절삭방법 - 2014.12.17 아직까지 상세구현 미비...
                truncMethod = parkInfo.getTruncMethod();
                // 절삭단위 ex) 100원
                truncUnit = parkInfo.getTruncUnit();
                disAdj = Util.sToi(parkInfo.getTruncUnit(),100);

                // 선불가능시간
                advBoundTime = parkInfo.getAdvBoundTime();

                IN_PRINT = parkInfo.getInPrint();
                OUT_PRINT = parkInfo.getOutPrint();

                Preferences.putValue(this, Constants.PREFERENCE_OUT_PRINT, OUT_PRINT);

                TICKET_PRINT = parkInfo.getTicketPrint();

                BIZ_NAME = parkInfo.getBizName();
                BIZ_BANK_MSG = parkInfo.getBizBankMsg();
                BIZ_BANK = parkInfo.getBizBank();
                BIZ_ACCOUNT_NO = parkInfo.getBizAccountNo();
                BIZ_ACCOUNT_NAME = parkInfo.getBizAccountName();
                BIZ_COPY_RIGHT = parkInfo.getBizCopyRight();
                BIZ_TEL = parkInfo.getBizTel();
                BIZ_NO = parkInfo.getBizNo();

                // 출근 사진 필수 빠짐... 일부 공단

                //if(Constants.isWorkPicture && !workStartTime.substring(0,2).equals("00")){
                if (Constants.isWorkPicture) {
                    Log.d("111", " isPicture >>> " + Constants.isWorkPicture + "~" + isPicture + "~" + workStartTime.substring(0, 2));
                    // 출근사진이 없다면...
                    if (!isPicture) {
                        checkWorkCommit();
                    }
                }

                Constants.STATUS_BOARD_TOTAL_AREA = Util.sToi(parkInfo.getCodeArea());

                statusBoardHelper = StatusBoardHelper.getInstance(Integer.parseInt(parkInfo.getCodeArea()));
                statusBoardHelper.setMaxCount(Integer.parseInt(parkInfo.getCodeArea()));
                statusBoardHelper.clear();
            } else {
                // TODO 에러 팝업 처리
                String KN_RESULT = json.optString("KN_RESULT");
                Toast.makeText(this, KN_RESULT, Toast.LENGTH_LONG).show();
            }
        } else {
            //	  Toast.makeText(this, Constants.DATA_FAIL, Toast.LENGTH_LONG).show();
        }

    }

    private void checkWorkCommit() {
        int nowTime = Util.sToi(Util.getYmdhms("HHmm"));
        // 출근 시간 전에만 사진 촬영이 가능하도록 체크... (오전 여섯시 이전부터...)
        Log.d("111", nowTime + "~" + Constants.isWorkPictureStartTime + "~" + workStartTime);
        //무조건 사진 찍도록..
        //if(nowTime > Util.sToi(Constants.isWorkPictureStartTime) && nowTime <= Util.sToi(workStartTime)){
        Intent intent = new Intent(this, WorkImageActivity.class);
        intent.putExtra("pictureType", Constants.PICTURE_TYPE_T1);
        startActivity(intent);
        //}
        /**XXX 이전버전 2015.10.31 작업
         // 출근 사진 촬영시간인지 체크...
         if(nowTime >= Util.sToi(workStartTime)){
         if(nowTime > Util.sToi(Constants.isWorkPictureStartTime) && nowTime <= Util.sToi(Constants.isWorkPictureEndTime)){
         Intent intent = new Intent(this, WorkImageActivity.class);
         intent.putExtra("pictureType", Constants.PICTURE_TYPE_T1 );
         startActivity(intent);
         }
         }
         **/
    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        Log.e("THIS ACTIVITY===", TAG);
        twoButtonDialog(Constants.MAINACTIVITY_DESTROY_ACTIVITY,
                getStr(R.string.title_exit), getStr(R.string.msg_exit_app))
                .show();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // Stop the Bluetooth print services
        if (mPrintService != null) {
            mPrintService.stop();
        }
    }

    private Dialog userInfoDialog() {
        final Dialog mBasicDialog;

        mBasicDialog = new Dialog(this,
                android.R.style.Theme_Holo_Light_Dialog);

        mBasicDialog.setContentView(R.layout.dialog_user_info_layout);
        mBasicDialog.setTitle("단말기정보");

        Button btn_ok = (Button) mBasicDialog.findViewById(R.id.btn_ok);
        TextView tvManager = (TextView) mBasicDialog.findViewById(R.id.tvManager);
        TextView tvPhoneNumber = (TextView) mBasicDialog.findViewById(R.id.tvPhoneNumber);
        TextView tvDeviceNumber = (TextView) mBasicDialog.findViewById(R.id.tvDeviceNumber);
        TextView tvSystemVersion = (TextView) mBasicDialog.findViewById(R.id.tvSystemVersion);

        tvManager.setText(empName);
        tvPhoneNumber.setText(empPhone.trim());
        tvDeviceNumber.setText(DeviceInfo.getPhoneNumber(this));
        tvSystemVersion.setText(empSystemVersion);

        btn_ok.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                mBasicDialog.dismiss();
            }
        });

        mBasicDialog.setOnCancelListener(new OnCancelListener() {

            @Override
            public void onCancel(DialogInterface dialog) {
                // TODO Auto-generated method stub

                mBasicDialog.dismiss();
            }
        });

        mBasicDialog.show();
        return mBasicDialog;
    }

}
