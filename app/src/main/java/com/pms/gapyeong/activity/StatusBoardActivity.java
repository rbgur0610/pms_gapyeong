package com.pms.gapyeong.activity;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.util.ByteArrayBuffer;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnKeyListener;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.TextView.OnEditorActionListener;

import com.pms.gapyeong.R;
import com.pms.gapyeong.adapter.StatusBoardAdapter;
import com.pms.gapyeong.common.Constants;
import com.pms.gapyeong.common.MsgUtil;
import com.pms.gapyeong.common.Preferences;
import com.pms.gapyeong.common.Util;
import com.pms.gapyeong.pay.PayData;
import com.pms.gapyeong.vo.ParkIO;
import com.pms.gapyeong.vo.ReceiptType13Vo;
import com.pms.gapyeong.vo.ReceiptType4Vo;

public class StatusBoardActivity extends BaseActivity {

    FrameLayout statusBoardLayout;
    private static final int DIALOG_PARK_BOARD = 500;
    private ViewPager mPager;
    //private TextView btn_top_00;
    //private TextView btn_top_01;
    //private TextView btn_top_02;
    //private TextView btn_top_03;
    private Button btn_top_left;
    private Button btn_top_refresh;
    private Button btn_page_prev;
    private Button btn_page_next;
    private Button btn_auto_car;
    private Button btn_hold;
    private EditText et_car_number;
    private Button btn_car_search;
    private StatusBoardAdapter adapter;
    private InputMethodManager ipm;
    private int selectedPage = 0;
    private String PRINT_TYPE = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.status_board_layout);
    }

    @Override
    protected void initLayoutSetting() {
        super.initLayoutSetting();

        TextView tv_top_title = (TextView) findViewById(R.id.tv_top_title);
        tv_top_title.setText("주차현황");

        btn_top_left = (Button) findViewById(R.id.btn_top_left);
        btn_top_left.setVisibility(View.VISIBLE);
        btn_top_left.setOnClickListener(this);

        btn_top_refresh = (Button) findViewById(R.id.btn_top_refresh);
        btn_top_refresh.setVisibility(View.VISIBLE);
        btn_top_refresh.setOnClickListener(this);

        mPager = (ViewPager) findViewById(R.id.pager);
        //btn_top_00 = (TextView) findViewById(R.id.btn_top_00);
        //btn_top_01 = (TextView) findViewById(R.id.btn_top_01);
        //btn_top_02 = (TextView) findViewById(R.id.btn_top_02);
        //btn_top_03 = (TextView) findViewById(R.id.btn_top_03);

        btn_page_prev = (Button) findViewById(R.id.btn_page_prev);
        btn_page_next = (Button) findViewById(R.id.btn_page_next);

        btn_hold = (Button) findViewById(R.id.btn_hold);
        btn_auto_car = (Button) findViewById(R.id.btn_auto_car);

        //2016-10-25 앱-번호인식 버튼 활성화요청(메인화면,입출차,면수선택후 우측상단의 번호인식 활성화) btn_auto_car.setEnabled(false);
        btn_hold.setEnabled(false);

        btn_page_prev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
				
				/*
				int nowPage = mPager.getCurrentItem() - 1;
				if(nowPage < 0){
					nowPage = 0;
				}
				
				mPager.setCurrentItem( nowPage );
				*/

                apiParkIoListCall_Print("I1");

            }
        });

        btn_page_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
        		/*
        		int count   = mPager.getAdapter().getCount();
        		int nowPage = mPager.getCurrentItem() + 1;
				if(nowPage > count){
					nowPage = count;
				}       
				
        		mPager.setCurrentItem( nowPage );
        		*/
                apiParkIoListCall_Print("O1");

            }
        });

        btn_hold.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Constants.isHOLD) { // 홀드 모드 -> 비홀드 모드
                    Constants.HOLD_TIME = "";
                    Constants.isHOLD = false;
                    //btn_top_00.setTextColor(getResources().getColor(R.color.Black));
                    //btn_top_01.setTextColor(getResources().getColor(R.color.Black));
                    //btn_top_02.setTextColor(getResources().getColor(R.color.Black));
                    //btn_top_03.setTextColor(getResources().getColor(R.color.Black));
                    //btn_top_00.setBackgroundResource(R.drawable.btn_gradation_empty);
                    //btn_top_01.setBackgroundResource(R.drawable.btn_gradation_normal);
                    //btn_top_02.setBackgroundResource(R.drawable.btn_gradation_prepayment);
                    //btn_top_03.setBackgroundResource(R.drawable.btn_gradation_ticket);
                } else { // 비홀드 모드 -> 홀드 모드
                    Constants.HOLD_TIME = Util.getYmdhms("HH:mm:ss");
                    Constants.isHOLD = true;
                    //btn_top_00.setTextColor(getResources().getColor(R.color.White));
                    //btn_top_01.setTextColor(getResources().getColor(R.color.White));
                    //btn_top_02.setTextColor(getResources().getColor(R.color.White));
                    //btn_top_03.setTextColor(getResources().getColor(R.color.White));
                    //btn_top_00.setBackgroundResource(R.drawable.btn_gradation_hold_empty);
                    //btn_top_01.setBackgroundResource(R.drawable.btn_gradation_hold_normal);
                    //btn_top_02.setBackgroundResource(R.drawable.btn_gradation_hold_prepayment);
                    //btn_top_03.setBackgroundResource(R.drawable.btn_gradation_hold_ticket);
                }
                if (adapter != null) {
                    adapter.notifyDataSetChanged();
                }
            }
        });

        btn_auto_car.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mIntent = new Intent(StatusBoardActivity.this, AutoCarNumberActivity.class);
                startActivity(mIntent);
            }
        });

        et_car_number = (EditText) findViewById(R.id.et_car_number);
        btn_car_search = (Button) findViewById(R.id.btn_car_search);

        ipm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

        et_car_number.setOnEditorActionListener(new OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                // TODO Auto-generated method stub
                switch (actionId) {
                    case EditorInfo.IME_ACTION_SEARCH:
                        apiParkIoListCall_find();
                        break;
                    default:
                        return false;
                }
                return false;
            }
        });

        et_car_number.setOnKeyListener(new OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // TODO Auto-generated method stub
                if (KeyEvent.ACTION_UP == event.getAction() && keyCode == 66) {
                    apiParkIoListCall_find();
                }
                return false;
            }
        });

        btn_car_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                apiParkIoListCall_find();
            }
        });

    }


    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        apiParkIoListCall();
    }

    public void parkIoListCallback_Print(String url, JSONObject json, AjaxStatus status) {
        closeProgressDialog();

        JSONArray jsonArr = null;
        String park_in = "";
        int this_time = 0;
        int in_date = 0;
        int this_count = 0;
        String PARK_TYPE = "";
        String title = "";
        String totalTime = "";
        String outTime = "";

        // successful ajax call
        if (json != null) {
            if ("1".equals(json.optString("CD_RESULT"))) {
                jsonArr = json.optJSONArray("RESULT");

                int Len = jsonArr.length();
                try {
                    for (int i = 0; i < Len; i++) {

                        JSONObject res = jsonArr.getJSONObject(i);

                        if ("I1".equals(PRINT_TYPE)) {
                            park_in = res.optString("PARK_IN");
                        } else {
                            park_in = res.optString("PARK_OUT");
                        }
                        this_time = Integer.valueOf(park_in.substring(8, 14));
                        if (in_date <= this_time) {
                            in_date = this_time;
                            this_count = i;
                        }
                    }

                    JSONObject res2 = jsonArr.getJSONObject(this_count);
                    String parkType = cch.findCodeByCdCode("PY", res2.optString("PARK_TYPE"));
                    String disCd = cch.findCodeByCdCode("DC", res2.optString("DIS_CD"));

                    if ("01".equals(res2.optString("PARK_TYPE"))) {    // 시간권(후불), // 정기권
                        title = "주차증";
                        totalTime = "";
                        outTime = "";
                    } else if ("03".equals(res2.optString("PARK_TYPE"))) {    // 일일권, 시간권(선불)
                        title = "주차증";
                        totalTime = "";
                        outTime = "";
                    } else if ("02".equals(res2.optString("PARK_TYPE"))) {    // 일일권, 시간권(선불)
                        title = "선불영수증";
                        totalTime = minTohour(Integer.valueOf(res2.optString("PARK_TIME")));
                        outTime = res2.optString("PARK_OUT");
                    } else if ("04".equals(res2.optString("PARK_TYPE"))) {    // 일일권, 시간권(선불)
                        title = "선불영수증";
                        totalTime = minTohour(Integer.valueOf(res2.optString("PARK_TIME")));
                        outTime = res2.optString("PARK_OUT");
                    }


                    if ("I1".equals(PRINT_TYPE)) {
                        String workTime = workStartTime.substring(0, 2)
                                + ":"
                                + workStartTime.substring(2)
                                + "~"
                                + workEndTime.substring(0, 2)
                                + ":"
                                + workEndTime.substring(2);//입차영수증

                        ReceiptType13Vo type13Vo = new ReceiptType13Vo(title        // 타이틀
                                , Util.getYmdhms("yyyy-MM-dd HH:mm:ss")    // 출력일자
                                , res2.optString("CD_AREA")            // 주차면
                                , parkType                // 주차종류
                                , res2.optString("CHARGE_TYPE")            // 요금종류
                                , disCd        // 요금할인
                                , res2.optString("YET_AMT")    // 총미수금액
                                , res2.optString("ADV_IN_AMT")                                // 선납금액
                                , res2.optString("PARK_IN")        // 시작시간
                                , outTime        // 출차예정시간
                                , totalTime    // 총 분
                                , BIZ_NO            // 사업자번호
                                , BIZ_NAME            // 사업자명
                                , BIZ_TEL, empTicket_print, workTime);            // 사업자연락처

                        type13Vo.setPioNum(res2.optString("PIO_NUM"));
                        type13Vo.setCarNum(res2.optString("CAR_NO"));
                        type13Vo.setParkName(parkName);
                        type13Vo.setEmpName(empName);
                        type13Vo.setEmpPhone(empPhone);
                        type13Vo.setEmpTel(empTel);
                        type13Vo.setAccount_No(empAccount_No);
                        type13Vo.setBank_MSG(empBank_MSG);
                        type13Vo.setKN_ACCOUNT(empKN_ACCOUNT);
                        type13Vo.setKN_BANK(empKN_BANK);
                        type13Vo.setEmpBusiness_Tel(empBusiness_TEL);

                        if (("시간권(후불)").equals(parkType)) {
                            type13Vo.setPrintBill(true);
                        }

                        type13Vo.setCashReceipt(res2.optString("CASH_APPROVAL_NO"));
                        type13Vo.setCardNo(res2.optString("CARD_NO"));
                        type13Vo.setCardCompany(res2.optString("CARD_COMPANY"));
                        type13Vo.setApprovalNo(res2.optString("APPROVAL_NO"));
                        type13Vo.setOutPrint(Preferences.getValue(this, Constants.PREFERENCE_OUT_PRINT, ""));


                        PayData payData = new PayData(this, OUT_PRINT, IN_PRINT, TICKET_PRINT);
                        payData.printDefault(PayData.PAY_DIALOG_TYPE_ENTRANCE_TIME, type13Vo, mPrintService);

                        payData.ImagePrint(empLogo_image);
                        try {
                            payData.printText("\n\n");
                        } catch (UnsupportedEncodingException e) {
                            if(Constants.DEBUG_PRINT_LOG){
                                e.printStackTrace();
                            }else{
                                System.out.println("예외 발생");
                            }
                        }


                    } else {

                        //출차영수증
                        int cutAmt = 0;
                        int spare = 0;


//					  if(Integer.valueOf(res2.optString("PARK_OUT_AMT")) < 0) {  //환불발생 시
//						  cutAmt = Integer.valueOf(res2.optString("PARK_OUT_AMT")) + Integer.valueOf(res2.optString("TRUNC_OUT_AMT"));
//					  } else {
//						  if(Integer.valueOf(res2.optString("CASH_OUT_AMT")) > 0) {  //출차 현금결제
//							  cutAmt = Integer.valueOf(res2.optString("CASH_OUT_AMT"));
//						  } else {
//							  if(Integer.valueOf(res2.optString("CARD_OUT_AMT")) > 0) {   //출차 카드결제
//								  cutAmt = Integer.valueOf(res2.optString("CARD_OUT_AMT"));
//							  } else {     //출차 정기권
//								  cutAmt = Integer.valueOf(res2.optString("PARK_OUT_AMT"));;
//							  }
//						  }
//					  }

                        //홍규혁 출영 처리
                        if (Integer.valueOf(res2.optString("PARK_OUT_AMT")) < 0) {  //환불발생 시
                            cutAmt = -(Integer.valueOf(res2.optString("RETURN_AMT")));
                        } else {
                            if (Integer.valueOf(res2.optString("CASH_AMT")) > 0) {  //출차 현금결제
                                cutAmt = Integer.valueOf(res2.optString("CASH_OUT_AMT"));
                            } else {
                                if (Integer.valueOf(res2.optString("CARD_AMT")) > 0) {   //출차 카드결제
                                    cutAmt = Integer.valueOf(res2.optString("CARD_OUT_AMT"));
                                } else {//출차 정기권 혹은 전체 할인
                                    cutAmt = Integer.valueOf(res2.optString("REAL_AMT"));
                                    ;
                                }
                            }
                        }


                        ReceiptType4Vo type4Vo = new ReceiptType4Vo(Util.getYmdhms("yyyy-MM-dd HH:mm:ss")    // 출력일자
                                , parkType            // 주차종류 :
                                , minTohour(Integer.valueOf(res2.optString("PARK_TIME")))            // 주차시간 :
                                , res2.optString("CD_AREA") + "면"        // 주차면 :
                                , disCd        // 할인종류 :
                                , res2.optString("PARK_IN")        // 입차시간 :
                                , res2.optString("PARK_OUT")        // 출차시간 :
                                , "" + String.valueOf(cutAmt)        // 납부금액 :
                                , res2.optString("ADV_AMT")        // 선납금액 :
                                , res2.optString("PARK_AMT")        // 주차요금 :
                                , res2.optString("YET_AMT")        // 미납금액 :
                                , res2.optString("DIS_AMT")        // 할인금액 :
                                , BIZ_NO                    // 사업자번호
                                , BIZ_NAME
                                , BIZ_TEL);

                        type4Vo.setPioNum(res2.optString("PIO_NUM"));
                        type4Vo.setCarNum(res2.optString("CAR_NO"));
                        type4Vo.setParkName(parkName);
                        type4Vo.setEmpName(empName);
                        type4Vo.setEmpPhone(empPhone);
                        type4Vo.setEmpTel(empTel);
                        type4Vo.setEmpBusiness_Tel(empBusiness_TEL);
                        type4Vo.setTitle("출차영수증");
                        type4Vo.setCouponAmt(res2.optString("COUPON_AMT"));

                        type4Vo.setConfirmCashReceipt(res2.optString("CASH_APPROVAL_NO"));
                        type4Vo.setCardNo(res2.optString("CARD_NO"));
                        type4Vo.setCardCompany(res2.optString("CARD_COMPANY"));
                        type4Vo.setConfirmCardNum(res2.optString("APPROVAL_NO"));
                        type4Vo.setOutPrint(Preferences.getValue(this, Constants.PREFERENCE_OUT_PRINT, ""));


                        PayData payData = new PayData(this, OUT_PRINT, IN_PRINT, TICKET_PRINT);
                        payData.printDefault(PayData.PAY_DIALOG_TYPE_EXIT_ONLY, type4Vo, mPrintService);
                        payData.ImagePrint(empLogo_image);
                        try {
                            payData.printText("\n\n");
                            apiPrintUpdate(res2.optString("PIO_NUM"));
                        } catch (UnsupportedEncodingException e) {
                            if(Constants.DEBUG_PRINT_LOG){
                                e.printStackTrace();
                            }else{
                                System.out.println("예외 발생");
                            }
                        }

                    }


                } catch (JSONException e) {
                    if(Constants.DEBUG_PRINT_LOG){
                        e.printStackTrace();
                    }else{
                        System.out.println("예외 발생");
                    }
                    Toast.makeText(this, Constants.DATA_FAIL, Toast.LENGTH_LONG).show();
                }

            } else {
                Toast.makeText(this, Constants.DATA_FAIL, Toast.LENGTH_LONG).show();
            }
        } else {
            Toast.makeText(this, Constants.DATA_FAIL, Toast.LENGTH_LONG).show();
        }
    }

    public void replaceByteDate(ByteArrayBuffer bf, String str) {
        byte[] text;
        try {
            Log.e("printMsg", str);
            text = str.getBytes("EUC-KR");

            if (text.length == 0)
                return;

            bf.append(text, 0, text.length);

        } catch (UnsupportedEncodingException e) {
            if(Constants.DEBUG_PRINT_LOG){
                e.printStackTrace();
            }else{
                System.out.println("예외 발생");
            }
        }
    }

    private String minTohour(int _min) {
        int first = Integer.valueOf(_min / 60);
        int second = _min % 60;
        String f = "";
        String e = "";
        if (first < 10) {
            f = "0" + "" + String.valueOf(first);
        } else {
            f = String.valueOf(first);
        }

        if (second < 10) {
            e = "0" + "" + String.valueOf(second);
        } else {
            e = String.valueOf(second);
        }

        return f + ":" + e;
    }

    private int upAmount(int _amt) {
        int spare = _amt % 100;

        return _amt + spare;
    }

    //parkIoResult
    private void apiParkIoListCall_Print(String _tic) {
        showProgressDialog();
        PRINT_TYPE = _tic;
        String url = Constants.API_SERVER + "?" + Constants.METHOD + "=" + Constants.INTERFACEID_PARK_IO;
        String param = "&CD_PARK=" + parkCode
                + "&CD_STATE=" + _tic
                + "&CAR_NO=" + Util.getEncodeStr("%")
                + "&START_DAY=" + today
                + "&END_DAY=" + today
                + "&" + Constants.API_FORMAT + "=" + Constants.JSON_FORMAT;
        Log.d(TAG, " apiParkIoListCall url >>> " + url + param);

        AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>();
        cb.url(url + param).type(JSONObject.class).weakHandler(this, "parkIoListCallback_Print").redirect(true).retry(3).fileCache(false).expire(-1);
        AQuery aq = new AQuery(this);
        aq.ajax(cb);
    }


    private void apiParkIoListCall() {
        showProgressDialog();
        String url = Constants.API_SERVER + "?" + Constants.METHOD + "=" + Constants.INTERFACEID_PARK_IO;
        String param = "&CD_PARK=" + parkCode
                + "&CD_STATE=I1"
                + "&CAR_NO=" + Util.getEncodeStr("%")
                + "&START_DAY=" + today
                + "&END_DAY=" + today
                + "&" + Constants.API_FORMAT + "=" + Constants.JSON_FORMAT;
        Log.d(TAG, " apiParkIoListCall url >>> " + url + param);

        AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>();
        cb.url(url + param).type(JSONObject.class).weakHandler(this, "parkIoListCallback").redirect(true).retry(3).fileCache(false).expire(-1);
        AQuery aq = new AQuery(this);
        aq.ajax(cb);
    }

    private void apiParkIoListCall_find() {

        // 키보드 숨기기
        if (ipm != null && et_car_number != null) {
            ipm.hideSoftInputFromWindow(et_car_number.getWindowToken(), 0);
        }

        if (et_car_number.getText().length() <= 0) {
            Toast.makeText(this, "차량번호를 입력해 주세요.", Toast.LENGTH_LONG).show();
            return;
        }

        //showProgressDialog();
        String url = Constants.API_SERVER + "?" + Constants.METHOD + "=" + Constants.INTERFACEID_PARK_IO;
        String param = "&CD_PARK=" + parkCode
                + "&CD_STATE=I1"
                + "&CAR_NO=" + et_car_number.getText()
                + "&START_DAY=" + today
                + "&END_DAY=" + today
                + "&" + Constants.API_FORMAT + "=" + Constants.JSON_FORMAT;
        Log.d(TAG, " apiParkIoListCall_find url >>> " + url + param);


        AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>();
        cb.url(url + param).type(JSONObject.class).weakHandler(this, "parkIoListCallback_find").redirect(true).retry(3).fileCache(false).expire(-1);
        AQuery aq = new AQuery(this);
        aq.ajax(cb);
    }

    public void parkIoListCallback_find(String url, JSONObject json, AjaxStatus status) {
        Log.d("parkIoListCallback_find", json.toString());
        //closeProgressDialog();
        JSONArray jsonArr = null;

        // successful ajax call
        if (json != null) {

            et_car_number.setText("");

            if ("1".equals(json.optString("CD_RESULT"))) {
                jsonArr = json.optJSONArray("RESULT");

                int Len = jsonArr.length();

                ArrayList<ParkIO> list = new ArrayList<ParkIO>();
                //ArrayList<String> list2 = new ArrayList<String>();

                // 초기화
                try {
                    for (int i = 0; i < Len; i++) {
                        JSONObject res = jsonArr.getJSONObject(i);
                        ParkIO info = new ParkIO(res.optString("PIO_NUM")
                                , res.optString("CD_PARK")
                                , res.optString("PIO_DAY")
                                , res.optString("CAR_NO")
                                , res.optString("CD_AREA")
                                , res.optString("PARK_TYPE")
                                , res.optString("DIS_CD")
                                , res.optString("TEL")
                                , res.optString("PARK_IN_AMT")
                                , res.optString("PARK_OUT_AMT")
                                , res.optString("PARK_AMT")
                                , res.optString("ADV_IN_AMT")
                                , res.optString("ADV_OUT_AMT")
                                , res.optString("ADV_AMT")
                                , res.optString("DIS_IN_AMT")
                                , res.optString("DIS_OUT_AMT")
                                , res.optString("DIS_AMT")
                                , res.optString("CASH_IN_AMT")
                                , res.optString("CASH_OUT_AMT")
                                , res.optString("CASH_AMT")
                                , res.optString("CARD_IN_AMT")
                                , res.optString("CARD_OUT_AMT")
                                , res.optString("CARD_AMT")
                                , res.optString("TRUNC_IN_AMT")
                                , res.optString("TRUNC_OUT_AMT")
                                , res.optString("TRUNC_AMT")
                                , res.optString("COUPON_AMT")
                                , res.optString("RETURN_AMT")
                                , res.optString("YET_AMT")
                                , res.optString("REAL_AMT")
                                , res.optString("PARK_TIME")
                                , res.optString("PARK_IN")
                                , res.optString("PARK_OUT")
                                , res.optString("CARD_NO")
                                , res.optString("CARD_COMPANY")
                                , res.optString("APPROVAL_NO")
                                , res.optString("APPROVAL_DATE")
                                , res.optString("CHARGE_TYPE")
                                , res.optString("KN_COMMENT")
                        );
                        list.add(info);

                        // Log.d(TAG, " CarNumber >>> " + info.getCarNumber() + " info >>> " + info.toString());
                    }
                } catch (JSONException e) {
                    if(Constants.DEBUG_PRINT_LOG){
                        e.printStackTrace();
                    }else{
                        System.out.println("예외 발생");
                    }
                    Len = 0;
                }

                if (Len == 1) {
                    Intent i = new Intent(StatusBoardActivity.this, ExitCommitActivity.class);
                    i.putExtra("parkIO", list.get(0));
                    startActivity(i);
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                } else if (Len > 1) {
                    basicListDialog(Constants.COMMON_TYPE_DDAM_CARNO_FIND, "검색차량", list, "FIND");
                }

            }
        } else {
            Toast.makeText(this, Constants.DATA_FAIL, Toast.LENGTH_LONG).show();
        }

    }


    public void parkIoListCallback(String url, JSONObject json, AjaxStatus status) {

        Log.d(TAG, " parkIoListCallback  json ====== " + json);

        int BOARD_SIZE = Preferences.getValue(this, Constants.PREFERENCE_SETTING_STATUS_BOARD_SIZE, Constants.SETTING_STATUS_BOARD_SIZE_15);

        try {
            statusBoardHelper.clear();
        } catch (NullPointerException e) {
            if(Constants.DEBUG_PRINT_LOG){
                e.printStackTrace();
            }else{
                System.out.println("예외 발생");
            }
        }

        closeProgressDialog();

        JSONArray jsonArr = null;

        // successful ajax call
        if (json != null) {
            if ("1".equals(json.optString("CD_RESULT"))) {
                jsonArr = json.optJSONArray("RESULT");
            } else {
                // TODO 에러 팝업 처리
                // String KN_RESULT = json.optString("KN_RESULT");
                // Toast.makeText(this, KN_RESULT, Toast.LENGTH_LONG).show();
                // Toast.makeText(this, Constants.DATA_EMPTY, Toast.LENGTH_LONG).show();
            }

            int totalPageCount = 0;

            try {
                totalPageCount = (Constants.STATUS_BOARD_TOTAL_AREA - 1) / BOARD_SIZE + 1;
                Log.d(TAG, " STATUS_BOARD_TOTAL_AREA >>> " + Constants.STATUS_BOARD_TOTAL_AREA + "  totalPageCount : ");
            } catch (ArithmeticException e) {
                if(Constants.DEBUG_PRINT_LOG){
                    e.printStackTrace();
                }else{
                    System.out.println("예외 발생");
                }
            }

            selectedPage = mPager.getCurrentItem();

            statusBoardHelper.setParkIoJson(jsonArr);

            adapter = new StatusBoardAdapter(this, statusBoardHelper, totalPageCount);
            adapter.setStatusDate(workStartTime, workEndTime, serviceStartTime, serviceEndTime);
            mPager.setAdapter(adapter);
            mPager.setOffscreenPageLimit(totalPageCount);

            if (selectedPage > 0) {
                mPager.setCurrentItem(selectedPage);
            }


        } else {
            Toast.makeText(this, Constants.DATA_FAIL, Toast.LENGTH_LONG).show();
        }

    }

    protected void apiPrintUpdate(String pionum) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put(Constants.METHOD, Constants.INTERFACEID_PRINT_UPDATE);
        params.put(Constants.API_FORMAT, Constants.JSON_FORMAT);
        params.put("PIO_NUM", pionum);

        showProgressDialog(false);
        String url = Constants.API_SERVER;

        AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>();
        cb.url(url).params(params).type(JSONObject.class).weakHandler(this, "printUpdateCallback").redirect(true).retry(3).fileCache(false).expire(-1);
        AQuery aq = new AQuery(this);
        aq.ajax(cb);
    }

    public void printUpdateCallback(String url, JSONObject json, AjaxStatus status) {

        closeProgressDialog();

        Log.d(TAG, " printUpdateCallback  json ====== " + json);

        // successful ajax call
        if (json != null) {
            String KN_RESULT = json.optString("KN_RESULT");
            if ("1".equals(json.optString("CD_RESULT"))) {
                MsgUtil.ToastMessage(this, KN_RESULT);
            } else {
                MsgUtil.ToastMessage(this, KN_RESULT);
            }
        } else {
            MsgUtil.ToastMessage(this, Constants.DATA_FAIL, Toast.LENGTH_LONG);
        }

    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.btn_top_left:
                finish();
                break;
            case R.id.btn_top_refresh:
                apiParkIoListCall();
                break;
        }
    }

}