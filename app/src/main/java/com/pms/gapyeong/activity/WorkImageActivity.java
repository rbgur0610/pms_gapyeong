package com.pms.gapyeong.activity;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;

import android.util.Log;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.pms.gapyeong.R;
import com.pms.gapyeong.common.Camera;
import com.pms.gapyeong.common.Constants;
import com.pms.gapyeong.common.ImageLoader;
import com.pms.gapyeong.common.MsgUtil;
import com.pms.gapyeong.common.Util;
import com.pms.gapyeong.database.PictureHelper;

public class WorkImageActivity extends BaseActivity {
	
	private Camera camera;
	private TextView tv_top_title;
	private ImageView iv_image;
	private Button btn_sendImage;
	private Button btn_sendData;
	
	private PictureHelper pictureHelper = PictureHelper.getInstance(this);
	private String pictureNo   = "";
	private String pictureType = "";
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);

		setContentView(R.layout.work_lmage_layout);
		
	}

	@Override
	protected void initLayoutSetting() {
		// TODO Auto-generated method stub
		super.initLayoutSetting();
		
		tv_top_title= (TextView)findViewById(R.id.tv_top_title);
		
		btn_sendImage= (Button)findViewById(R.id.btn_sendImage);
		btn_sendData= (Button)findViewById(R.id.btn_sendData);
		
		iv_image = (ImageView)findViewById(R.id.iv_image);
		
		camera = new Camera(this);
		
		Intent intent = getIntent();
		if (intent != null) {
			pictureType = Util.isNVL(intent.getStringExtra("pictureType"));
		}		
		
		if(pictureType.equals(Constants.PICTURE_TYPE_T1)){
			tv_top_title.setText("출근확인");
		} else if(pictureType.equals(Constants.PICTURE_TYPE_T2)){
			tv_top_title.setText("퇴근확인");
		}
		
		if(pictureType.equals(Constants.PICTURE_TYPE_T1)){
			pictureNo = "S-"+parkCode+"-"+empCode+"-"+Util.getYmdhms("yyyyMMdd")+"-"+Util.getYmdhms("HHmmss");
		} else if(pictureType.equals(Constants.PICTURE_TYPE_T2)){
			pictureNo = "E-"+parkCode+"-"+empCode+"-"+Util.getYmdhms("yyyyMMdd")+"-"+Util.getYmdhms("HHmmss");
		}
		
		btn_sendImage.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				camera.takePhoto(pictureNo);				
			}
		});
	
		btn_sendData.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				String imagePath = camera.imagePath;
				
				if(Util.isEmpty(imagePath)){
					MsgUtil.ToastMessage(WorkImageActivity.this, "사진을 등록해주세요.");
					return;
				}
				
				// 로컬 사진 저장
				pictureHelper.delete(pictureNo, imagePath);
				pictureHelper.insert(pictureNo, imagePath, parkCode, "", Constants.PICTURE_TYPE_T1, Util.getYmdhms("yyyyMMdd"));
				
				// 서버 사진 저장 
				apiWorkPictureUploadCall(imagePath);				
			}
		});
	
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		
		super.onActivityResult(requestCode, resultCode, data);
		
		Log.d(TAG, " requestCode >>> " + requestCode);
		switch (requestCode) {
		  case Camera.TAKE_PICTURE :
			  
				if(resultCode == RESULT_CANCELED){
					return;			
				}			  
				  
				String imagePath = camera.imagePath;
				if (imagePath == null) {
					MsgUtil.ToastMessage(this, "사진촬영 에러. 사진을 다시 촬영해 주세요", Toast.LENGTH_LONG);
					return ;
				}
				
				// 이미지 리사이즈...
				ImageLoader.resizeImage(imagePath);
				// 이미지 뷰...
				ImageLoader.setImageView(imagePath, iv_image);				
				
			break;
			
			case Camera.SELECT_CROP_IMAGE:
				if(resultCode == RESULT_CANCELED){
					return;			
				}					
				camera.selectCropResult(data);
			break;			
		} // end switch
	}

	private void apiWorkPictureUploadCall(String imagePath){
		showProgressDialog();
        Map<String, Object> params = new HashMap<String, Object>();
        params.put(Constants.API_FORMAT, Constants.JSON_FORMAT);
        params.put("CD_PARK",  	  parkCode);
        params.put("EMP_CD",      empCode);
        params.put("CD_TYPE",     pictureType);
        params.put("WORK_DAY", 	  today);
        
        Log.d(TAG, " imagePath >>> " + imagePath);
        Log.d(TAG, " empCode >>> " + empCode);
        //Alternatively, put a File or InputStream instead of byte[]
        File file = new File(imagePath);    
        params.put("IMAGE_FILE", file);        
        
        String url = Constants.API_SERVER + "?" + Constants.METHOD + "=" + Constants.INTERFACEID_WORK_PICTURE_UPLOAD;
        Log.d(TAG, " apiWorkPictureUploadCall url >>> " + url + " params ::: " + params);
		AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>();        
		cb.url(url).params(params).type(JSONObject.class).weakHandler(this, "workPictureUploadCallback").redirect(true).retry(3).fileCache(false).expire(-1);
		AQuery aq = new AQuery(this);
		aq.ajax(cb);
	}		
	
	public void workPictureUploadCallback(String url, JSONObject json, AjaxStatus status){
		Log.d(TAG, " workPictureUploadCallback  json ====== " +json);
		closeProgressDialog();
		// successful ajax call          
        if(json != null){   
        	String KN_RESULT = json.optString("KN_RESULT");
           if("1".equals(json.optString("CD_RESULT"))){
        	   MsgUtil.ToastMessage(this, KN_RESULT, Toast.LENGTH_LONG);
        	   if(pictureType.equals(Constants.PICTURE_TYPE_T2)){
	 	    	   Intent intent = new Intent();
	 	    	   setResult(RESULT_OK, intent);
        	   }
           } else {
        	   MsgUtil.ToastMessage(this, KN_RESULT, Toast.LENGTH_LONG);
           }
           finish();
        } else {
    	    MsgUtil.ToastMessage(this, Constants.DATA_FAIL, Toast.LENGTH_LONG);
        }
	}			
	
	@Override
	public void onBackPressed() {
		// 액티비티 종료 막기 
		// super.onBackPressed();
	}
	
}