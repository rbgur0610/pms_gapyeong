package com.pms.gapyeong.activity;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import android.app.Dialog;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.pms.gapyeong.R;
import com.pms.gapyeong.adapter.UnPayManagerAdapter;
import com.pms.gapyeong.common.CalendarInfo;
import com.pms.gapyeong.common.Constants;
import com.pms.gapyeong.common.MsgUtil;
import com.pms.gapyeong.common.Preferences;
import com.pms.gapyeong.common.Util;
import com.pms.gapyeong.database.CodeHelper;
import com.pms.gapyeong.pay.PayData;
import com.pms.gapyeong.pay.PayVo;
import com.pms.gapyeong.vo.ReceiptType10Vo;
import com.pms.gapyeong.vo.ReceiptType9Vo;
import com.pms.gapyeong.vo.UnPayManagerItem;

public class UnpaidManagerActivity extends BaseActivity implements AdapterView.OnItemClickListener, CompoundButton.OnCheckedChangeListener {
    private Button btn_cal_st;
    private Button btn_cal_ed;
    private int mYear;
    private int mMonth;
    private int mDay;

    private EditText et_car_number;
    private Button btn_car_search;
    private Button btnSearchType;
    private ListView lvManager;
    private TextView tvHistoryNum;
    private Button btnListPrint;
    private Button btnListPrint2;
    private Button btnTotalReturn;
    private calendarDlg caledarDialog;
    private Button btn_top_left;
    private ArrayList<UnPayManagerItem> unPayList;
    private ArrayList<UnPayManagerItem> selectedUnpay = new ArrayList<UnPayManagerItem>();
    private UnPayManagerAdapter mAdapter = null;
    private TextView tvTable00;
    private TextView tvTable01;
    private TextView tvTable02;
    private TextView tvTable03;
    private TextView tvTable04;
    private LinearLayout layoutButton01;
    private LinearLayout layoutButton02;

    private int PrintCount = 0;
    boolean isFinish;
    private String carNumber = "";

    private InputMethodManager ipm;

    private JSONArray jsonArr = null;
    ;

    private int currentPage = 1;
    private int totalLen = 0;
    private boolean lastitemVisibleFlag = false;        //화면에 리스트의 마지막 아이템이 보여지는지 체크

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.unpay_manager_layout);
        String date = Util.getYmdhms("yyyy-MM-dd");
        mYear = Integer.parseInt(date.split("-")[0]);
        mMonth = Integer.parseInt(date.split("-")[1]);
        mDay = Integer.parseInt(date.split("-")[2]);

        unPayList = new ArrayList<UnPayManagerItem>();

    }


    @Override
    protected void initLayoutSetting() {
        // TODO Auto-generated method stub
        super.initLayoutSetting();

        TextView tv_top_title = (TextView) findViewById(R.id.tv_top_title);
        tv_top_title.setText("미수관리");
        btn_top_left = (Button) findViewById(R.id.btn_top_left);
        btn_top_left.setVisibility(View.VISIBLE);
        btn_top_left.setOnClickListener(this);

        tvTable00 = (TextView) findViewById(R.id.tvTable00);
        tvTable01 = (TextView) findViewById(R.id.tvTable01);
        tvTable02 = (TextView) findViewById(R.id.tvTable02);
        tvTable03 = (TextView) findViewById(R.id.tvTable03);
        tvTable04 = (TextView) findViewById(R.id.tvTable04);


        et_car_number = (EditText) findViewById(R.id.et_car_number);
        btn_car_search = (Button) findViewById(R.id.btn_car_search);
        btnSearchType = (Button) findViewById(R.id.btnSearchType);
        lvManager = (ListView) findViewById(R.id.lvManager);
        tvHistoryNum = (TextView) findViewById(R.id.tvHistoryNum);
        btnListPrint = (Button) findViewById(R.id.btnListPrint);
        btnListPrint2 = (Button) findViewById(R.id.btnListPrint2);
        btnTotalReturn = (Button) findViewById(R.id.btnTotalReturn);
        layoutButton01 = (LinearLayout) findViewById(R.id.layoutButton);
        layoutButton02 = (LinearLayout) findViewById(R.id.layoutButton2);
        layoutButton01.setVisibility(View.GONE);
        layoutButton02.setVisibility(View.VISIBLE);
        tvTable04.setVisibility(View.GONE);

        btn_cal_st = (Button) findViewById(R.id.btn_cal_st);
        btn_cal_ed = (Button) findViewById(R.id.btn_cal_ed);

        setCommonItem("K1", Constants.COMMON_TYPE_UNPAID_TYPE, btnSearchType);

        btn_cal_st.setText(mYear + Util.addZero(mMonth) + Util.addZero(mDay));
        btn_cal_ed.setText(mYear + Util.addZero(mMonth) + Util.addZero(mDay));
        btn_cal_st.setOnClickListener(this);
        btn_cal_ed.setOnClickListener(this);
        btnListPrint.setOnClickListener(this);
        btnListPrint2.setOnClickListener(this);
        btnTotalReturn.setOnClickListener(this);
        btnSearchType.setOnClickListener(this);


        ipm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

        et_car_number.setOnEditorActionListener(new OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                // TODO Auto-generated method stub
                switch (actionId) {
                    case EditorInfo.IME_ACTION_SEARCH:
                        requestUnPayList();
                        break;
                    default:
                        return false;
                }
                return false;
            }
        });

        btn_car_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestUnPayList();
            }
        });


        Intent i = getIntent();
        if (i != null) {
            isFinish = i.getBooleanExtra("finish", false);
            carNumber = i.getStringExtra("carNo");
            et_car_number.setText(carNumber);
        }


        requestUnPayList();
    }


    /**
     * COUNT :미납건수
     * PIO_NUM : 주차번호
     * CD_PARK : 주차장코드
     * KN_PARK : 주차장명
     * PARK_IN : 입차시간
     * PARK_OUT : 출차시간
     * AMT : 미납금액
     * PIO_DAY : 주차일(20140105)
     * DIS_CD : 할인코드(20140105)
     * PARK_TYPE: 요금종류(일일권)
     * CD_GUBUN: 미납종류(미출,도주)
     * ADD_AMT : 가산금(20140105)
     * SUM_AMT : 미납금누계총액(20140105)
     * CD_STATE : 상태코드(2013.12.31)
     * CAR_STATE : Y (압류대상), N (정상) (2014.01.08)
     * KN_RESULT : 압류대상 또는 정상(2014.01.08)
     */

    private void setLayout() {

        // 초기화
        currentPage = 1;
        if (unPayList != null) {  //picture_list_row
            unPayList.clear();
            selectedUnpay.clear();
            PrintCount = 0;
        }

        String searchCode = cch.findCodeByKnCode("K1", btnSearchType.getText().toString());
        mAdapter = new UnPayManagerAdapter(this, unPayList, searchCode);

        if (et_car_number.getText().toString().equals("") || searchCode.equals("K3")) {
            layoutButton01.setVisibility(View.VISIBLE);
            layoutButton02.setVisibility(View.GONE);
            tvTable04.setVisibility(View.GONE);
            mAdapter.VisibleCheckbox(true);
        } else {
            layoutButton01.setVisibility(View.GONE);
            layoutButton02.setVisibility(View.VISIBLE);
            tvTable04.setVisibility(View.VISIBLE);

            mAdapter.VisibleCheckbox(false);
        }
        mAdapter.setCheckListener(this);


        lvManager.setAdapter(mAdapter);

        lvManager.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
                // TODO Auto-generated method stub
                goToUnpaidInfoActivity(position);
            }
        });

        lvManager.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                //현재 화면에 보이는 첫번째 리스트 아이템의 번호(firstVisibleItem) + 현재 화면에 보이는 리스트 아이템의 갯수(visibleItemCount)가 리스트 전체의 갯수(totalItemCount) -1 보다 크거나 같을때
                lastitemVisibleFlag = (totalItemCount > 0) && (firstVisibleItem + visibleItemCount >= totalItemCount);
            }

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
//	        	 Log.d(TAG, " scrollState >>>> " + scrollState);
//	        	 Log.d(TAG, " OnScrollListener.SCROLL_STATE_IDLE >>>> " + OnScrollListener.SCROLL_STATE_IDLE);
                //OnScrollListener.SCROLL_STATE_IDLE은 스크롤이 이동하다가 멈추었을때 발생되는 스크롤 상태입니다.
                //즉 스크롤이 바닦에 닿아 멈춘 상태에 처리를 하겠다는 뜻
                if (scrollState == OnScrollListener.SCROLL_STATE_IDLE && lastitemVisibleFlag) {
                    //TODO 화면이 바닦에 닿을때 처리
                    if (totalLen > mAdapter.getCount()) {
                        currentPage++;
                        setData();
                    }
                }
            }
        });

    }


    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        int position = (Integer) buttonView.getTag();
        UnPayManagerItem item = mAdapter.getItem(position);
        item.setChecked(isChecked);
        mAdapter.notifyDataSetChanged();
    }


    private void setData() {

        if (jsonArr == null) {
            setToast("검색 결과가 없습니다.");
            return;
        }

        int page = currentPage;
        int start_index = (page - 1) * Constants.LIST_SIZE;
        int last_index = (page * Constants.LIST_SIZE);

        if (last_index > totalLen) {
            last_index = totalLen;
        }

//	   Log.d(TAG, " start_index >>> " + start_index );
//	   Log.d(TAG, " last_index >>> " + last_index );
        for (int i = start_index; i < last_index; i++) {
            try {
                JSONObject res = jsonArr.getJSONObject(i);
                UnPayManagerItem info = new UnPayManagerItem(
                        res.optString("PIO_NUM")
                        , res.optString("CD_PARK")
                        , res.optString("KN_PARK")
                        , res.optString("PARK_IN")
                        , res.optString("PARK_OUT")
                        , res.optString("AMT")
                        , res.optString("PIO_DAY")
                        , res.optString("DIS_CD")
                        , res.optString("PARK_TYPE")
                        , res.optString("CD_GUBUN")
                        , res.optString("ADD_AMT")
                        , res.optString("SUM_AMT")
                        , res.optString("CD_STATE")
                        , res.optString("CAR_STATE")
                        , res.optString("KN_RESULT")
                        , res.optString("CAR_NO")
                        , res.optString("PARK_INOUT")
                        , res.optString("SUBMIT_AMT")
                        , res.optString("SUBMIT_DATE")
                        , res.optString("OFF_AMT")
                        , res.optString("CHANGE_DIS")
                        , res.optString("GROUP_CD"));
                info.setCash_app_date(res.optString("APPROVAL_DATE"));
                info.setCash_app_no(res.optString("APPROVAL_NO"));
                info.setBill_type(res.optString("BILL_TYPE"));
                unPayList.add(info);

            } catch (JSONException e) {
                if(Constants.DEBUG_PRINT_LOG){
                    e.printStackTrace();
                }else{
                    System.out.println("예외 발생");
                }
            }
        }  // end for

        mAdapter.notifyDataSetChanged();
        closeProgressDialog();
    }


    protected void goToUnpaidInfoActivity(int position) {
        Intent i = new Intent(UnpaidManagerActivity.this, UnpaidInfoActivity.class);
        i.putExtra("UNPAY_LIST", unPayList.get(position));
        i.putExtra("UnpayType", cch.findCodeByKnCode("K1", btnSearchType.getText().toString()));
        i.putExtra("finish", isFinish);
        startActivityForResult(i, 7777);
    }

    private void payTotalReturn() {
        int count = 0;
        StringBuilder sb = new StringBuilder();
        int totalMoney = 0;
        int lastIndex = 0;
        ArrayList<ReceiptType10Vo> arr = new ArrayList<ReceiptType10Vo>();

        selectedUnpay.clear();
        for (int i = 0; i < unPayList.size(); i++) {
            //if(String.valueOf(unPayList.get(i).getChecked()).equals("true"))
            if (unPayList.get(i).getChecked()) {
                selectedUnpay.add(unPayList.get(i));
                totalMoney += Integer.valueOf(unPayList.get(i).getSumAmt());
                count++;
                //arr.add(type10Vo);
                lastIndex = i;

            }
        }


        if (count > 0) {

            ReceiptType10Vo type10Vo = new ReceiptType10Vo(Util.getYmdhms("yyyy-MM-dd HH:mm:ss")        // 출력일자
                    , "통합결제"                // 거래종류
                    , ""                // 입금액
                    , ""                // 기타이유
                    , unPayList.get(lastIndex).getPioDay()                     // 미수날짜
                    , unPayList.get(lastIndex).getParkType()         // 주차종류
                    , "30분-500원"
                    , cch.findCodeByCdCode("DC", unPayList.get(lastIndex).getDisCd())
                    , "0"                                     // 미납잔액
                    , unPayList.get(lastIndex).getSumAmt().toString().replace("원", "").replace(",", "")                // 총 미납금
                    , unPayList.get(lastIndex).getAmt().replace("원", "").replace(",", "")                // 미납금액
                    , "미수"            // 미납타입
                    , ""            // 총 입금액
                    , Util.getYmdhms("yyyy-MM-dd HH:mm") // 미수환수일
                    , BIZ_NO                    // 사업자번호
                    , BIZ_NAME
                    , BIZ_TEL
                    , unPayList.get(lastIndex).getKnPark()
                    , unPayList.get(lastIndex).getParkIn()  //입차시간
                    , unPayList.get(lastIndex).getParkOut() //출차시간
            );

            type10Vo.setTitle("       [미수환수영수증]");
            type10Vo.setPioNum(unPayList.get(lastIndex).getPioNum());
            type10Vo.setCarNum(unPayList.get(lastIndex).getCarNo());
            type10Vo.setParkName(parkName);
            type10Vo.setEmpName(empName);
            type10Vo.setEmpPhone(empPhone);
            type10Vo.setEmpTel(empTel);
            type10Vo.setEmpBusiness_Tel(empBusiness_TEL);
            type10Vo.setOutPrint(Preferences.getValue(this, Constants.PREFERENCE_OUT_PRINT, ""));
            //ReceiptType10Vo type10Vo = null;
            unpayDialog("통합결제", String.valueOf(totalMoney), type10Vo);
        }

    }


    private static boolean clickCalBtn = true;

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        super.onClick(v);
        switch (v.getId()) {

            case R.id.btn_top_left:
                finish();
                break;

            case R.id.btnListPrint:
            case R.id.btnListPrint2:
                String searchCode = cch.findCodeByKnCode("K1", btnSearchType.getText().toString());
                ReceiptType9Vo type9Vo = null;
                if (searchCode.equals("K3")) {
                    type9Vo = new ReceiptType9Vo("미수환수현황", Util.getYmdhms("yyyy-MM-dd HH:mm:ss"), BIZ_NAME, BIZ_TEL, unPayList);
                } else {
                    type9Vo = new ReceiptType9Vo("미수자료현황", Util.getYmdhms("yyyy-MM-dd HH:mm:ss"), BIZ_NAME, BIZ_TEL, unPayList);
                }

                type9Vo.setParkName(parkName);
                type9Vo.setEmpName(empName);
                type9Vo.setEmpPhone(empPhone);
                type9Vo.setEmpTel(empTel);
                type9Vo.setEmpBusiness_Tel(empBusiness_TEL);
                type9Vo.setOutPrint(Preferences.getValue(this, Constants.PREFERENCE_OUT_PRINT, ""));
                PayData payData = new PayData(this, OUT_PRINT, IN_PRINT, TICKET_PRINT);
                payData.printDefault(PayData.PAY_TYPE_UNPAY_MANAGER, type9Vo, mPrintService);
                break;
            case R.id.btnTotalReturn:
                payTotalReturn();
                break;
            case R.id.btnSearchType:
                setCommonDialog("검색종류", "K1", Constants.COMMON_TYPE_UNPAID_TYPE, btnSearchType);
                break;

            case R.id.btn_cal_st:
                clickCalBtn = true;
                caledarDialog = new calendarDlg(this);
                caledarDialog.show();
                break;

            case R.id.btn_cal_ed:
                clickCalBtn = false;
                caledarDialog = new calendarDlg(this);
                caledarDialog.show();
                break;
        }
    }

    @Override
    public void getSelectListDialogData(int tag, String data) {
        // TODO Auto-generated method stub
        super.getSelectListDialogData(tag, data);

        String searchCode = cch.findCodeByKnCode("K1", btnSearchType.getText().toString());
        if (searchCode.equals("K3")) {
            tvTable02.setText("미수금액\n환수금액");
            tvTable03.setText("종류(상태)");
        } else {
            tvTable02.setText("미수금액");
            tvTable03.setText("종류(이유)");
        }
        requestUnPayList();
    }

    private void requestUnPayList() {

        // 키보드 숨기기
        if (ipm != null && et_car_number != null) {
            ipm.hideSoftInputFromWindow(et_car_number.getWindowToken(), 0);
        }

        // 초기화
        if (mAdapter != null && unPayList != null) {
            unPayList.clear();
            selectedUnpay.clear();
            PrintCount = 0;
            mAdapter.notifyDataSetChanged();
        }
        tvHistoryNum.setText("0건");

        String startDate = btn_cal_st.getText().toString();
        String endDate = btn_cal_ed.getText().toString();

        CodeHelper cch = CodeHelper.getInstance(this);
        apiParkUnpayListCall(cch.findCodeByKnCode("K1", btnSearchType.getText().toString()), et_car_number.getText().toString(), parkCode, startDate, endDate);

    }

    private void apiParkUnpayListCall(String unPayType, String carNo, String cdPark, String startDay, String EndDay) {
        showProgressDialog();

        String startDate = startDay;
        String endDate = EndDay;

        if (Util.isEmpty(carNo)) {
            carNo = Util.getEncodeStr("%");
        } else {
            startDate = "20010101";
        }

        String url = Constants.API_SERVER + "?" + Constants.METHOD + "=" + Constants.INTERFACEID_PARKUNPAY_INFO;
        String param = "&CAR_NO=" + carNo
                + "&CD_GUBUN=" + unPayType
                + "&CD_PARK=" + parkCode
                + "&START_DATE=" + startDate
                + "&END_DATE=" + endDate
                + "&" + Constants.API_FORMAT + "=" + Constants.JSON_FORMAT;
        Log.d(TAG, " apiParkUnpayListCall url >>> " + url + param);

        AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>();
        cb.url(url + param).type(JSONObject.class).weakHandler(this, "parkUnpayListCallback").redirect(true).retry(3).fileCache(false).expire(-1);
        AQuery aq = new AQuery(this);
        aq.ajax(cb);
    }

    public void parkUnpayListCallback(String url, JSONObject json, AjaxStatus status) {

        closeProgressDialog();

        Log.d(TAG, " parkUnpayListCallback url  :  " + url + " json ====== " + json);

        // successful ajax call
        if (json != null) {
            String KN_RESULT = json.optString("KN_RESULT");
            if ("1".equals(json.optString("CD_RESULT"))) {
                jsonArr = json.optJSONArray("RESULT");
                totalLen = jsonArr.length();
                tvHistoryNum.setText(totalLen + "건");
                showProgressDialog();
                setLayout();
                setData();
            } else {
                setLayout();
                tvHistoryNum.setText("0건");
                MsgUtil.ToastMessage(this, KN_RESULT, Toast.LENGTH_LONG);
            }
        } else {
            MsgUtil.ToastMessage(this, Constants.DATA_FAIL, Toast.LENGTH_LONG);
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case 7777:
                if (resultCode == RESULT_OK) {
                    setResult(RESULT_OK, data);
                    finish();
                } else {
                    if (isFinish) {
                        finish();
                    } else {
                        requestUnPayList();
                    }
                }
                break;
            case PayData.PAY_DIALOG_TYPE_UNPAY_CARD:  //카드결제

                String transactionNo = "";
                String approvalNo = "";
                String approvalDate = "";
                String cardNo = "";
                String cardAmt = "";
                String cardCompany = "";
//				String pioNum 	     = "";
//				String carNum 	     = "";			  
                try {
                    try {
                        mPrintService.stop();
                        if (mPrintService.getState() == 0) {

                            BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(mBluetoothAddress);
                            mPrintService.connect(device, true);
                        }
                    }  catch (IllegalArgumentException e) {
                        System.out.println("IllegalArgumentException 예외 발생");
                    } catch (NullPointerException e) {
                        System.out.println("NullPointerException 예외 발생");
                    }
                    //Log.d(TAG, "approval : " + Preferences.getValue(this, Constants.PREFERENCE_MCPAY_APPROVAL_NO, ""));
                    transactionNo = Preferences.getValue(this, Constants.PREFERENCE_MCPAY_TRANSACTION_NO, "");
                    approvalNo = Preferences.getValue(this, Constants.PREFERENCE_MCPAY_APPROVAL_NO, "");
                    approvalDate = Preferences.getValue(this, Constants.PREFERENCE_MCPAY_APPROVAL_DATE, "");
                    cardNo = Preferences.getValue(this, Constants.PREFERENCE_MCPAY_CARD_NO, "");
                    cardCompany = Preferences.getValue(this, Constants.PREFERENCE_MCPAY_CARD_COMPANY, "");
                    cardAmt = Preferences.getValue(this, Constants.PREFERENCE_MCPAY_TOTAL_AMT, "");

                    if (transactionNo == "") {
                        Log.d("111", "Card error");
                        return;
                    }

                    if (cardNo.length() >= 12) {
                        cardNo = cardNo.substring(0, 4) + cardNo.substring(12);
                    }

                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        if(Constants.DEBUG_PRINT_LOG){
                            e.printStackTrace();
                        }else{
                            System.out.println("예외 발생");
                        }
                    }

                    Bundle eB = new Bundle();
                    eB.putString("transactionNo", transactionNo);
                    eB.putString("approvalNo", approvalNo);
                    eB.putString("approvalDate", approvalDate);
                    eB.putString("cardNo", cardNo);
                    eB.putString("cardCompany", cardCompany);
                    eB.putString("cardAmt", cardAmt);
                    eB.putString("pioNum", gReceiptVo.getPioNum());
                    eB.putString("carNum", gReceiptVo.getCarNum());

                } catch (NullPointerException e) {
                    if(Constants.DEBUG_PRINT_LOG){
                        e.printStackTrace();
                    }else{
                        System.out.println("예외 발생");
                    }
                }

                InitPreferences();

                // 서버 결제 로그 저장 step2
                String url = Constants.API_SERVER + "?" + Constants.METHOD + "=" + Constants.INTERFACEID_LOG_PARAMETER;
                String param = "&transactionNo=" + transactionNo
                        + "&approvalNo=" + approvalNo
                        + "&approvalDate=" + approvalDate
                        + "&cardNo=" + cardNo
                        + "&cardCompany=" + cardCompany
                        + "&cardAmt=" + cardAmt
                        + "&class=" + this.getClass().getSimpleName()
                        + "&type=pay_step2"
                        + "&" + Constants.API_FORMAT + "=" + Constants.JSON_FORMAT;

                Log.d(TAG, " apiParkLogCall step 2 url >>> " + url + param);

                AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>();
                cb.url(url + param).type(JSONObject.class).weakHandler(this, "").redirect(true).retry(3).fileCache(false).expire(-1);
                AQuery aq = new AQuery(this);
                aq.ajax(cb);

                //unpayDialog.dismiss();
                PayVo payVo = new PayVo();
                payVo.setTransactionNo(transactionNo);
                payVo.setApprovalNo(approvalNo);
                payVo.setApprovalDate(approvalDate);
                payVo.setCardNo(cardNo);
                payVo.setCardCompany(cardCompany);
                payVo.setCardAmt(cardAmt);
                payVo.setCashAmt("0");
                payVo.setCatNumber(catNumber);

                request(PayData.PAY_DIALOG_TYPE_UNPAY_CARD, payVo);
                break;

        }
    }

    @Override
    protected void request(int type, Object data) {
        // TODO Auto-generated method stub
        super.request(type, data);

        if (type == PayData.PAY_DIALOG_TYPE_UNPAY_CASH) {
            PayVo payVo = (PayVo) data;
            apiUnpayPay(payVo, "CASH");

        } else {
            //PayVo payVo = (PayVo)data;
            //apiUnpayPay(payVo);
            PayVo payVo = (PayVo) data;
            apiUnpayPay(payVo, "CARD");
        }

    }

    /**
     * 미납결재
     *
     * @param payVo
     */


    private void apiUnpayPay(PayVo payVo, String payType) {

        if (payVo == null) {
            payVo = new PayVo();
            payVo.setCashAmt("0");
            payVo.setCardAmt("0");
        }

        for (int i = 0; i < selectedUnpay.size(); i++) {

            Map<String, Object> params = new HashMap<String, Object>();

            // method 셋팅...
            params.put(Constants.METHOD, Constants.INTERFACEID_PARK_UNPAY_PAY);
            // format
            params.put(Constants.API_FORMAT, Constants.JSON_FORMAT);
            // 주차번호
            params.put("PIO_NUM", selectedUnpay.get(i).getPioNum());
            // CD_PARK: 주차장번호
            params.put("CD_PARK", parkCode);
            // EMP_CD : 근무자코드
            params.put("EMP_CD", empCode);

            // CAR_NO : 차량번호
            params.put("CAR_NO", selectedUnpay.get(i).getCarNo());

            // 현금 영수증 관련
            params.put("CASH_APPROVAL_NO", payVo.getCashApprovalNo());
            params.put("CASH_APPROVAL_DATE", payVo.getCashApprovalDate());

            // 카드 결제 관련
            params.put("TRANSACTION_NO", payVo.getTransactionNo());
            params.put("APPROVAL_NO", payVo.getApprovalNo());
            params.put("APPROVAL_DATE", payVo.getApprovalDate());
            params.put("CARD_NO", payVo.getCardNo());
            params.put("CARD_COMPANY", payVo.getCardCompany());
            params.put("CATNUMBER", payVo.getCatNumber());


            ReceiptType10Vo type10Vo = new ReceiptType10Vo(Util.getYmdhms("yyyy-MM-dd HH:mm:ss")        // 출력일자
                    , "통합결제"                // 거래종류
                    , ""                // 입금액
                    , ""                // 기타이유
                    , selectedUnpay.get(i).getPioDay()                     // 미수날짜
                    , selectedUnpay.get(i).getParkType()         // 주차종류
                    , "30분-500원"
                    , cch.findCodeByCdCode("DC", selectedUnpay.get(i).getDisCd())
                    , "0"                                     // 미납잔액
                    , selectedUnpay.get(i).getSumAmt().toString().replace("원", "").replace(",", "")                // 총 미납금
                    , selectedUnpay.get(i).getAmt().replace("원", "").replace(",", "")                // 미납금액
                    , "미수"            // 미납타입
                    , selectedUnpay.get(i).getSumAmt().toString().replace("원", "").replace(",", "")            // 총 입금액
                    , Util.getYmdhms("yyyy-MM-dd HH:mm") // 미수환수일
                    , BIZ_NO                    // 사업자번호
                    , BIZ_NAME
                    , BIZ_TEL
                    , selectedUnpay.get(i).getKnPark()
                    , selectedUnpay.get(i).getParkIn()  //입차시간
                    , selectedUnpay.get(i).getParkOut() //출차시간
            );

            type10Vo.setTitle("       [미수환수영수증]");
            type10Vo.setPioNum(selectedUnpay.get(i).getPioNum());
            type10Vo.setCarNum(selectedUnpay.get(i).getCarNo());
            type10Vo.setParkName(parkName);
            type10Vo.setEmpName(empName);
            type10Vo.setEmpPhone(empPhone);
            type10Vo.setEmpTel(empTel);
            type10Vo.setEmpBusiness_Tel(empBusiness_TEL);
            type10Vo.setOutPrint(Preferences.getValue(this, Constants.PREFERENCE_OUT_PRINT, ""));
            PayData payData = new PayData(this, OUT_PRINT, IN_PRINT, TICKET_PRINT);
            if (payType.equals("CASH")) {
                params.put("CASH_AMT", selectedUnpay.get(i).getSumAmt());
                params.put("CARD_AMT", "0");

                payData.printDefault(PayData.PAY_DIALOG_TYPE_UNPAY_SUCCESS, type10Vo, mPrintService);
                payData.ImagePrint(empLogo_image);
                try {
                    payData.printText("\n\n");
                } catch (UnsupportedEncodingException e) {
                    if(Constants.DEBUG_PRINT_LOG){
                        e.printStackTrace();
                    }else{
                        System.out.println("예외 발생");
                    }
                }
            } else {
                params.put("CASH_AMT", "0");
                params.put("CARD_AMT", selectedUnpay.get(i).getSumAmt());

                Bundle eB = new Bundle();
                eB.putString("transactionNo", payVo.getTransactionNo());
                eB.putString("approvalNo", payVo.getApprovalNo());
                eB.putString("approvalDate", payVo.getApprovalDate());
                eB.putString("cardNo", payVo.getCardNo());
                eB.putString("cardCompany", payVo.getCardCompany());
                eB.putString("cardAmt", payVo.getCatNumber());
                eB.putString("pioNum", selectedUnpay.get(i).getPioNum());
                eB.putString("carNum", selectedUnpay.get(i).getCarNo());


                payData.printPayment(PayData.PAY_DIALOG_TYPE_UNPAY_CARD, type10Vo, mPrintService, eB, null);
                payData.ImagePrint(empLogo_image);
                try {
                    payData.printText("\n\n");
                } catch (UnsupportedEncodingException e) {
                    if(Constants.DEBUG_PRINT_LOG){
                        e.printStackTrace();
                    }else{
                        System.out.println("예외 발생");
                    }
                }

            }

            // TRUNC_AMT:절삭금액
            params.put("TRUNC_AMT", "0");

            showProgressDialog(false);

            String url = Constants.API_SERVER;
            Log.d(TAG, " apiUnpayPay url >>> " + url + " " + params);

            AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>();
            cb.url(url).params(params).type(JSONObject.class).weakHandler(this, "unpayPayCallback").redirect(true).retry(3).fileCache(false).expire(-1);
            AQuery aq = new AQuery(this);
            aq.ajax(cb);
        }

    }

    public void unpayPayCallback(String url, JSONObject json, AjaxStatus status) {
        closeProgressDialog();
        Log.d(TAG, " unpayPayCallback  json ====== " + json);
        if (json != null) {
            String KN_RESULT = json.optString("KN_RESULT");
            //successful ajax call, show status code and json content
            if ("1".equals(json.optString("CD_RESULT"))) {

                PrintCount++;

                if (PrintCount >= selectedUnpay.size()) {
                    MsgUtil.ToastMessage(UnpaidManagerActivity.this, KN_RESULT, Toast.LENGTH_LONG);
                    Intent intent = new Intent();
                    intent.putExtra("RESULT_PAY_OK", "1");
                    setResult(RESULT_OK, intent);
                    PrintCount = 0;
                    finish();
                }
            } else {
                //ajax error, show error code
                MsgUtil.ToastMessage(UnpaidManagerActivity.this, KN_RESULT, Toast.LENGTH_LONG);
            }
        } else {
            MsgUtil.ToastMessage(UnpaidManagerActivity.this, Constants.DATA_FAIL);
        }
    }


    Calendar calendar = Calendar.getInstance();
    private String mDate;

    public class calendarDlg extends Dialog {

        public class myCalendar extends CalendarInfo {
            public myCalendar(Context context, LinearLayout layout) {
                super(context, layout);
            }

            @Override
            public void myClickEvent(int yyyy, int MM, int dd) {
                mYear = yyyy;
                mMonth = MM + 1;
                mDay = dd;
                mDate = mYear + Util.addZero(mMonth) + Util.addZero(mDay);

                if (clickCalBtn) {
                    btn_cal_st.setText(mDate);
                } else {
                    btn_cal_ed.setText(mDate);
                }

                requestUnPayList();
                calendarDlg.this.cancel();
                super.myClickEvent(yyyy, MM, dd);

            }
        }

        TextView tvs[];
        Button btns[];

        public calendarDlg(Context context) {

            super(context, android.R.style.Theme_Holo_Light_Dialog_NoActionBar);
            setContentView(R.layout.calendar_layout);

            LinearLayout lv = (LinearLayout) findViewById(R.id.calendar_ilayout);

            tvs = new TextView[3];
            tvs[0] = (TextView) findViewById(R.id.tv1);
            tvs[1] = (TextView) findViewById(R.id.tv2);
            tvs[2] = null;

            btns = new Button[4];
            btns[0] = null;
            btns[1] = null;
            btns[2] = (Button) findViewById(R.id.Button03);
            btns[3] = (Button) findViewById(R.id.Button04);

            myCalendar cal = new myCalendar(context, lv);

            cal.setControl(btns);
            cal.setViewTarget(tvs);
            if (clickCalBtn) {
                cal.initCalendar(btn_cal_st.getText().toString());
            } else {
                cal.initCalendar(btn_cal_ed.getText().toString());
            }
        }
    }


    @Override
    public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
        // TODO Auto-generated method stub

    }
}
