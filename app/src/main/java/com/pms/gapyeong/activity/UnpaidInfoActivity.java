package com.pms.gapyeong.activity;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Random;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.util.Log;
import android.app.TimePickerDialog;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.pms.gapyeong.common.CalendarInfo;
import com.pms.gapyeong.common.DeviceInfo;
import com.pms.gapyeong.common.Preferences;
import com.pms.gapyeong.R;
import com.pms.gapyeong.common.Constants;
import com.pms.gapyeong.common.MsgUtil;
import com.pms.gapyeong.common.Util;
import com.pms.gapyeong.database.CodeHelper;
import com.pms.gapyeong.pay.PayData;
import com.pms.gapyeong.pay.PayVo;
import com.pms.gapyeong.vo.ExitPaymentData;
import com.pms.gapyeong.vo.ParkIO;
import com.pms.gapyeong.vo.ReceiptType10Vo;
import com.pms.gapyeong.vo.ReceiptType4Vo;
import com.pms.gapyeong.vo.ReceiptType5Vo;
import com.pms.gapyeong.vo.UnPayManagerItem;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

public class UnpaidInfoActivity extends ExitAbstractActivity {

    private Button btn_car_no_1;
    private Button btn_car_no_2;
    private Button btn_car_no_3;
    private Button btn_car_no_4;
    private Button btn_car_no_manual;

    private TextView tvCarpark;
    private TextView tvUnpaidDate;
    private TextView tvUnpaidType;
    private TextView tvChargeDiscount;
    private Button btnParkType;
    private Button btnChargeType;
    private Button btnChargeDiscount;
    private TextView tvParkTime_st;
    private TextView tvParkTime_ed;
    private TextView tvParkTime_time;
    private TextView tvUnPayment;
    private TextView tvAddPayment;
    private TextView tvThisPayment;
    private Button btn_Receipt;
    private Button btn_receiptPublish;
    private Button btn_receiptPublish2;
    private Button btn_payback;
    private Button btn_top_left;
    private Button btnPicture;
    private UnPayManagerItem unPayList;
    public final static int PREPAY_TYPE = 600;
    private ReceiptType10Vo type10Vo;

    private String UnpayType;
    private View returnLayout;
    private TextView tvReturnAmt;
    private TextView tvMinusPayment;

    private String carNo = "";
    private ReceiptType4Vo type4Vo;
    private String group_code = "";
    private String discountAmt = "0";
    private int truncAmt = 0;
    public CodeHelper cch;
    private String defaultEndTime;
    private String defaultStartTime;
    private ParkIO parkIO;


    private String m_workStartTime;                // 근무 시작 시간
    private String m_workEndTime;                    // 근무 종료시간
    private String m_serviceStartTime;                // 무료 주차 시작 시간
    private String m_serviceEndTime;                // 무료 주차 종료시간
    private String m_inService;
    private String m_outService;
    private String m_baseSection;
    private String m_baseAmt;
    private String m_repeatSection;
    private String m_repeatAmt;
    private String m_parkClass;
    private int m_parkMaxAmt = 0;
    private String m_today;

    private String gOrdr_idxx = "";
    private String gVcnt_account = "";
    private String gVcnt_account_copy = "";
    private String gIpgm_date = "";
    private String gBank_name = "";
    private Button btn_vr_account;
    private Button btn_discount;
    private Button btn_bank;
    private Button bts_impdate;
    private TextView tvPayType;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.unpay_info_layout);
    }

    @Override
    protected void initLayoutSetting() {
        // TODO Auto-generated method stub
        super.initLayoutSetting();

//        String date = Util.getYmdhms("yyyy-MM-dd");
//        mYear = Integer.parseInt(date.split("-")[0]);
//        mMonth = Integer.parseInt(date.split("-")[1]);
//
//        if (mMonth == 12) {
//            mYear = mYear + 1;
//            mMonth = 1;
//        } else {
//            mMonth = mMonth + 1;
//        }
//        //mDay   = Integer.parseInt(date.split("-")[2]);
//        mDay = 5;


        Calendar cDate = Util.nextMonth(1);
        mYear = cDate.get(Calendar.YEAR);
        mMonth = cDate.get(Calendar.MONTH) + 1;
//        mDay = cDate.getActualMaximum(Calendar.DAY_OF_MONTH);
        mDay = 5;


        Intent i = getIntent();
        unPayList = (UnPayManagerItem) i.getSerializableExtra("UNPAY_LIST");
        UnpayType = i.getStringExtra("UnpayType");

        TextView tv_top_title = (TextView) findViewById(R.id.tv_top_title);

        btn_top_left = (Button) findViewById(R.id.btn_top_left);
        btn_top_left.setVisibility(View.VISIBLE);
        btn_top_left.setOnClickListener(this);
        returnLayout = (View) findViewById(R.id.returnLayout);
        tvReturnAmt = (TextView) findViewById(R.id.tvReturnAmt);
        tvCarpark = (TextView) findViewById(R.id.tvCarpark);
        tvMinusPayment = (TextView) findViewById(R.id.tvMinusPayment);

        btn_car_no_1 = (Button) findViewById(R.id.btn_car_no_1);
        btn_car_no_2 = (Button) findViewById(R.id.btn_car_no_2);
        btn_car_no_3 = (Button) findViewById(R.id.btn_car_no_3);
        btn_car_no_4 = (Button) findViewById(R.id.btn_car_no_4);
        btn_car_no_manual = (Button) findViewById(R.id.btn_car_no_manual);

        tvUnpaidDate = (TextView) findViewById(R.id.tvUnpaidDate);
        tvUnpaidType = (TextView) findViewById(R.id.tvUnpaidType);
        btnParkType = (Button) findViewById(R.id.btnParkType);
        btnChargeType = (Button) findViewById(R.id.btnChargeType);
        btnChargeDiscount = (Button) findViewById(R.id.btnChargeDiscount);
        tvParkTime_st = (TextView) findViewById(R.id.tvParkTime_st);
        tvParkTime_ed = (TextView) findViewById(R.id.tvParkTime_ed);
        tvParkTime_time = (TextView) findViewById(R.id.tvParkTime_time);
        tvUnPayment = (TextView) findViewById(R.id.tvUnPayment);
        tvAddPayment = (TextView) findViewById(R.id.tvAddPayment);
        tvChargeDiscount = (TextView) findViewById(R.id.tvChargeDiscount);
        tvThisPayment = (TextView) findViewById(R.id.tvThisPayment);
        btn_Receipt = (Button) findViewById(R.id.btn_Receipt);
        btn_payback = (Button) findViewById(R.id.btn_payback);
        btn_receiptPublish = (Button) findViewById(R.id.btn_receiptPublish);
        btn_receiptPublish2 = (Button) findViewById(R.id.btn_receiptPublish2);
        btn_receiptPublish.setVisibility(View.GONE);
        btn_receiptPublish2.setVisibility(View.GONE);
        btnPicture = (Button) findViewById(R.id.btnPicture);


        btn_vr_account = (Button) findViewById(R.id.btn_vr_account);
        btn_discount = (Button) findViewById(R.id.btn_discount);

        btn_vr_account.setOnClickListener(this);
        btn_discount.setOnClickListener(this);

        btn_payback.setOnClickListener(this);
        btn_Receipt.setOnClickListener(this);
        btn_receiptPublish.setOnClickListener(this);
        btn_receiptPublish2.setOnClickListener(this);
        btnPicture.setOnClickListener(this);
        tvPayType = (TextView) findViewById(R.id.tvPayType);


        if ("u6".equals(unPayList.getCdState().toLowerCase())) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("압류요청 차량입니다. 미수환수 시 관리자에게 보고 바랍니다. ");
            builder.setPositiveButton(R.string.btn_ok, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            AlertDialog alert = builder.create();
            alert.setCancelable(false);
            alert.show();
        }


        if (UnpayType.equals("K3")) {
            tv_top_title.setText("미수환수정보");
            btn_Receipt.setText("영수증");
            btn_payback.setVisibility(View.GONE);
            btn_vr_account.setEnabled(false);
            btn_discount.setEnabled(false);

            if (("C1").equals(unPayList.getBill_type())) {
                if (("").equals(unPayList.getCash_app_no())) {
                    btn_receiptPublish2.setVisibility(View.VISIBLE);
                }
            }
        } else {
            tv_top_title.setText("미수정보");
            btn_Receipt.setText("청구서");
            btn_payback.setVisibility(View.VISIBLE);
        }

        m_today = unPayList.getPioDay();
        apiParkInfoCall(unPayList.getPioDay(), unPayList.getCdPark());

        setLayout();
    }

    private void setLayout() {

        cch = CodeHelper.getInstance(this);
        group_code = unPayList.getGroup_code();
        btnParkType.setText(cch.findCodeByCdCode("PY", unPayList.getParkType()));

        if ("2".equals(parkClass)) {
            setCommonItem("AM2", Constants.COMMON_TYPE_CHARGE_TYPE, btnChargeType);
        } else if ("3".equals(parkClass)) {
            setCommonItem("AM3", Constants.COMMON_TYPE_CHARGE_TYPE, btnChargeType);
        } else if ("4".equals(parkClass)) {
            setCommonItem("AM4", Constants.COMMON_TYPE_CHARGE_TYPE, btnChargeType);
        } else if ("5".equals(parkClass)) {
            setCommonItem("AM5", Constants.COMMON_TYPE_CHARGE_TYPE, btnChargeType);
        } else if ("6".equals(parkClass)) {
            setCommonItem("AM6", Constants.COMMON_TYPE_CHARGE_TYPE, btnChargeType);
        }

        //setCommonItem("AM2", Constants.COMMON_TYPE_CHARGE_TYPE, btnChargeType);

        if ("".equals(cch.findCodeByCdCode("DC", unPayList.getChangeDis()))) {
            btnChargeDiscount.setText(cch.findCodeByCdCode("DC", unPayList.getDisCd()));
        } else {
            btnChargeDiscount.setText(cch.findCodeByCdCode("DC", unPayList.getDisCd()) + "/" + cch.findCodeByCdCode("DC", unPayList.getChangeDis()));
        }

        tvCarpark.setText(unPayList.getKnPark());

        carNo = unPayList.getCarNo();
        setCarnoView(carNo, btn_car_no_1, btn_car_no_2, btn_car_no_3, btn_car_no_4, btn_car_no_manual);

        tvUnpaidDate.setText(unPayList.getPioDay().substring(0, 4) + "-" + unPayList.getPioDay().substring(4, 6) + "-" + unPayList.getPioDay().substring(6));
        tvUnpaidType.setText(unPayList.getCdGubun());

        tvChargeDiscount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String parkType = cch.findCodeByKnCode("PY", btnParkType.getText().toString());
                // 일일권 또는 선납금일 경우
                if (parkType.equals("02") || parkType.equals("04")) {
                    MsgUtil.ToastMessage(UnpaidInfoActivity.this, btnParkType.getText().toString() + " 은 요금할인을 변경할 수 없습니다.");
                    return;
                } else {
                    setCommonDialog("요금할인", "DC", Constants.COMMON_TYPE_CHARGE_DISCOUNT, tvChargeDiscount);
                }
            }
        });

        tvParkTime_ed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                return;
                //DialogTimePicker(tvParkTime_ed,PREPAY_TYPE);
            }
        });


        try {
            //tvParkTime_st.setText(unPayList.getParkIO());
            //Log.d("111", unPayList.getParkIO());
            //Log.d("111", unPayList.getParkIn());
            //Log.d("111", unPayList.getParkOut());
            //Log.d("111", unPayList.getParkIn().substring(8,10) + ":" + unPayList.getParkIn().substring(10,12));
            //Log.d("111", unPayList.getParkOut().substring(8,10) + ":" + unPayList.getParkOut().substring(10,12));

            tvParkTime_st.setText(unPayList.getParkIn().substring(8, 10) + ":" + unPayList.getParkIn().substring(10, 12));
            tvParkTime_ed.setText(unPayList.getParkOut().substring(8, 10) + ":" + unPayList.getParkOut().substring(10, 12));

        } catch (NullPointerException e) {
            System.out.println("NullPointerException 예외 발생");
        } catch (IndexOutOfBoundsException e) {
            System.out.println("IndexOutOfBoundsException 예외 발생");
        }

        tvUnPayment.setText(Util.addComma(unPayList.getAmt()) + "원");
        tvAddPayment.setText(Util.addComma(unPayList.getAddAmt()) + "원");
        tvMinusPayment.setText(Util.addComma(unPayList.getOffAmt()) + "원");
        tvThisPayment.setText(Util.addComma(unPayList.getSumAmt()) + "원");


        if (UnpayType.equals("K3")) {
            returnLayout.setVisibility(View.VISIBLE);
            tvThisPayment.setText(Util.addComma(0) + "원");
            tvReturnAmt.setText(Util.addComma(unPayList.getSubmitAmt()) + "원");
        }

        apiParkIoListCall(unPayList.getCarNo(), "CARNO", unPayList.getParkIn().substring(0, 8), unPayList.getParkOut().substring(0, 8));

    }

    @Override
    public void getSelectListDialogData(int tag, String data) {
        super.getSelectListDialogData(tag, data);
        switch (tag) {
            case Constants.COMMON_TYPE_CHARGE_DISCOUNT:
                setToast("할인을 적용시 반드시 아래의 할인적용을 누른 후 미수환수를 진행해야합니다. ");
                setParkAmt(2);
                break;

            case Constants.COMMON_TYPE_UNPAID_TYPE:
                ProcessVcnt(btn_bank.getText().toString(), bts_impdate.getText().toString());
                break;
        }
    }


    public void ProcessVcnt(String btnbank, String impdate) {
        showProgressDialog();
        String date = Util.getYmdhms("yyyy-MM-dd");
        int m_Year = Integer.parseInt(date.split("-")[0]);
        int m_Month = Integer.parseInt(date.split("-")[1]);
        int m_Day = Integer.parseInt(date.split("-")[2]);

        SimpleDateFormat formatter = new SimpleDateFormat("HHmmss", Locale.KOREA);
        Date currentTime = new Date();
        String dTime = formatter.format(currentTime);
        String time = dTime.substring(0, 2) + "" + dTime.substring(2, 4) + "" + dTime.substring(4, 6);
        Random rand = new Random();
        rand.setSeed(new Date().getTime());
        int min = 10000000, max = 99999999;
        int randomNum = rand.nextInt(max - min + 1) + min;

        gOrdr_idxx = m_Year + Util.addZero(m_Month) + Util.addZero(m_Day) + "" + time + "" + randomNum;
        gBank_name = btnbank;

        String url = Constants.SERVER_HOST + "/road/service.vcnt";
        String car_number = btn_car_no_1.getText().toString() + "" + btn_car_no_2.getText().toString() + "" + btn_car_no_3.getText().toString() + "" + btn_car_no_4.getText().toString();
        String param = "?id=APP"
                + "&act=PAY_ACTION"
                + "&pio_num=" + unPayList.getPioNum()
                + "&req_tx=pay"
                + "&currency=WON"
                + "&pay_method=VCNT"
                + "&good_name=U1"
                + "&good_mny=" + unPayList.getAmt().replaceAll(",", "")
                + "&ordr_idxx=" + gOrdr_idxx
                + "&ipgm_name=" + car_number
                + "&ipgm_bank=" + "BK" + cch.findCodeByKnCode("BA", btnbank)
                + "&ipgm_date=" + impdate + "235959";
        Log.d("333", " =============apiParkIoListCall url >>> " + url + param);

        AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>();
        cb.url(url + param).type(JSONObject.class).weakHandler(this, "callBackVcnt").redirect(true).retry(3).fileCache(false).expire(-1);
        AQuery aq = new AQuery(this);
        aq.ajax(cb);
    }

    public void callBackVcnt(String url, JSONObject json, AjaxStatus status) {
        JSONArray jsonArr = null;
        if (json != null) {
            Log.d("333", json.optString("account"));
            gVcnt_account_copy = json.optString("account");
        } else {
            Toast.makeText(this, Constants.DATA_FAIL, Toast.LENGTH_LONG).show();
        }

        closeProgressDialog();
    }


    void setParkTime(int _order) {

        // 할인금액 초기화
        discountAmt = "0";

        // 절삭금액 초기화
        truncAmt = 0;
        //----String parkAmt = parkIO.getParkInAmt();
        String date1 = unPayList.getParkIn().substring(0, 8) + tvParkTime_st.getText().toString().replace(":", "") + "00";
        String date2 = unPayList.getParkOut().substring(0, 8) + tvParkTime_ed.getText().toString().replace(":", "") + "00";
        String retday = "";

        if (date2.length() <= 12) {
            date2 = date2 + "00";
        }

        //###############################################################
        //  STEP 1 : 주차 시간 계산
        //###############################################################
        //String parkMin = minuteCalc(date1, date2);
        String parkMin = minuteCalc_Return(date1, date2, m_workStartTime, m_workEndTime, m_serviceStartTime, m_serviceEndTime, m_today);

        //###############################################################
        //  STEP 2 : 할인 시간 계산
        //###############################################################
        // 요금할인 정보
        String discountName = "";
        if (_order == 1) {
            discountName = cch.findCodeByCdCode("DC", unPayList.getDisCd());
        } else {
            discountName = tvChargeDiscount.getText().toString();
        }

        String discountCode = cch.findCodeByKnCode("DC", discountName);

        // 할인시간 설정
        if (!discountName.equals("")) {
            int disTime = Util.sToi(cch.findCodeByDisTime("DC", discountName), 0);
            if (disTime > 0) {
                Log.d(TAG, "parkMin 2 : " + parkMin);
                parkMin = "" + (Util.sToi(parkMin, 0) - disTime);
                Log.d(TAG, "parkMin 3 : " + parkMin);
                if ((Util.sToi(parkMin, 0) < 0)) {
                    Log.d(TAG, "parkMin 4 : " + parkMin);
                    parkMin = "0";
                }
            }
        }

        // 주차시간 표시
        tvParkTime_time.setText(minuteView(parkMin));


        // 결제여부(미결제, 현금, 카드)
        if ("01".equals(parkIO.getParkType()) || "04".equals(parkIO.getParkType()) || "02".equals(parkIO.getParkType())) {
            if ("".equals(parkIO.getCardNo())) {
                tvPayType.setText(" 현금");
            } else {
                tvPayType.setText(" 카드");
            }
        } else {
            tvPayType.setText(" 미결제");
        }
    }

    void setParkAmt(int _order) {

        // 할인금액 초기화
        discountAmt = "0";

        // 절삭금액 초기화
        truncAmt = 0;
        //----String parkAmt = parkIO.getParkInAmt();
        String date1 = unPayList.getParkIn().substring(0, 8) + tvParkTime_st.getText().toString().replace(":", "") + "00";
        String date2 = unPayList.getParkOut().substring(0, 8) + tvParkTime_ed.getText().toString().replace(":", "") + "00";
        String retday = "";

        if (date2.length() <= 12) {
            date2 = date2 + "00";
        }

        //###############################################################
        //  STEP 1 : 주차 시간 계산
        //###############################################################
        //String parkMin = minuteCalc(date1, date2);
        String parkMin = minuteCalc_Return(date1, date2, m_workStartTime, m_workEndTime, m_serviceStartTime, m_serviceEndTime, m_today);


        //###############################################################
        //  STEP 2 : 할인 시간 계산
        //###############################################################
        // 요금할인 정보
        String discountName = "";
        if (_order == 1) {
            discountName = cch.findCodeByCdCode("DC", unPayList.getDisCd());
        } else {
            discountName = tvChargeDiscount.getText().toString();
        }

        String discountCode = cch.findCodeByKnCode("DC", discountName);

        // 할인시간 설정
        if (!discountName.equals("")) {
            int disTime = Util.sToi(cch.findCodeByDisTime("DC", discountName), 0);
            if (disTime > 0) {
                Log.d(TAG, "parkMin 2 : " + parkMin);
                parkMin = "" + (Util.sToi(parkMin, 0) - disTime);
                Log.d(TAG, "parkMin 3 : " + parkMin);
                if ((Util.sToi(parkMin, 0) < 0)) {
                    Log.d(TAG, "parkMin 4 : " + parkMin);
                    parkMin = "0";
                }
            }
        }

        Log.d("111", "==========" + parkMin + "~" + discountName);

        // 주차시간 표시
        tvParkTime_time.setText(minuteView(parkMin));


        //###############################################################
        //  STEP 3 : 할인 금액 계산
        //###############################################################

        // 주차종류
        String parkTypeCode = cch.findCodeByKnCode("PY", btnParkType.getText().toString());

        // 요금종류코드
        String chargeCode = "";
        if ("2".equals(m_parkClass)) {
            chargeCode = cch.findCodeByKnCode("AM2", btnChargeType.getText().toString());
        } else if ("3".equals(m_parkClass)) {
            chargeCode = cch.findCodeByKnCode("AM3", btnChargeType.getText().toString());
        } else if ("4".equals(m_parkClass)) {
            chargeCode = cch.findCodeByKnCode("AM4", btnChargeType.getText().toString());
        } else if ("5".equals(m_parkClass)) {
            chargeCode = cch.findCodeByKnCode("AM5", btnChargeType.getText().toString());
        } else if ("6".equals(m_parkClass)) {
            chargeCode = cch.findCodeByKnCode("AM6", btnChargeType.getText().toString());
        }

        // 주차금액 계산
        //String total  = parkCharge(parkMin, chargeCode, parkTypeCode);
        String total = parkCharge(parkMin, chargeCode, parkTypeCode, m_inService, m_outService, m_baseSection, m_baseAmt, m_repeatSection, m_repeatAmt, m_parkMaxAmt);
        Log.d("111", "total==========" + total);
        String charge = "0";

        if (Util.sToi(unPayList.getAmt()) < Util.sToi(total)) {
            total = unPayList.getAmt();
        }

        // 과금 된 실제 주차금액 표시
        tvThisPayment.setText(total + "원");

        if ("04".equals(parkTypeCode)) {  // 시간권 선불 : 총 주차요금 = 입차 시 선납금액 - 입차 시 할인금액  - 입차 시 절삭금액 차감
            //Log.d("111", "------"+total + "~"+parkIO.getAdvInAmt() + "~"+parkIO.getDisInAmt() + "~"+ parkIO.getTruncInAmt());
            total = "" + (Util.sToi(total) - Util.sToi(parkIO.getAdvInAmt()) - Util.sToi(parkIO.getDisInAmt()) - Util.sToi(parkIO.getTruncInAmt()));
            if (Util.sToi(parkIO.getAdvInAmt()) == 0) {
                total = "0";
            }
            Log.d(TAG, " total to be >>> " + total);
        }


        // 주차금액 셋팅
        charge = total;
        if (Util.sToi(total) >= 0) {

            int tempDisAmt1 = 0;
            int tempDisAmt2 = 0;

            // 할인금액 조회
            String disAmt = Util.isNVL(cch.findCodeByDisAmt("DC", discountName), "0");

            if (!"0".equals(disAmt)) {
                // 할인금액 설정
                tempDisAmt1 = Util.sToi(disAmt);
                charge = "" + (Util.sToi(charge) - tempDisAmt1);
            }


            // 할인률 조회
            String disRate = cch.findCodeByDisRate("DC", discountName);

            if (!"0".equals(disRate)) {
                // 할인률에 따른 할인금액 설정
                tempDisAmt2 = Util.sToi(Util.isNVL("" + discountChargeRate(charge, disRate), "0"));
                charge = "" + (Util.sToi(charge) - tempDisAmt2);
                Log.d(TAG, "setParkAmt ::: charge >>> " + charge + "  tempDisAmt2 : " + tempDisAmt2);
            }

            // 총 할인금액
            discountAmt = "" + (tempDisAmt1 + tempDisAmt2);
            Log.d(TAG, " setParkAmt >>> charge : " + charge + " discountAmt : " + discountAmt);


            if (Util.sToi(charge, 0) < 0) {
                charge = "0";
            }


        } // end if parkTypeCode


        // 주차원금 셋팅
        tvThisPayment.setTag(charge);
        tvMinusPayment.setText(discountAmt + "원");
        Log.d("111", "charge==========" + charge);


        //###############################################################
        //  STEP 4 : 납부 금액 계산
        //###############################################################

        int payment = 0;


        payment = Util.sToi(charge);

        //###############################################################
        //  STEP 5 : 절삭 금액 계산
        //###############################################################

        if (!"0".equals(truncUnit)) {
            truncAmt = payment % Util.sToi(truncUnit);
        } else {
            truncAmt = 0;
        }

        //payment = payment - truncAmt;

        // 절삭금액은 +금액으로 표시(미환불 금액)
        //truncAmt = Math.abs(truncAmt);

        //** 할인금액이 없으면 절삭 적용
        // 주차할인코드
        String disCode = cch.findCodeByKnCode("DC", discountName);

        if ("01".equals(disCode)) {
            payment = payment - truncAmt;
        } else { // 할인금액이 있으면 올림 적용
            if (truncAmt > 0) {
                // 납부금액은 절삭 올림 적용
                payment = payment + (Util.sToi(truncUnit) - truncAmt);
                // 할인금액은 절삭 차감
                discountAmt = "" + (Util.sToi(discountAmt) - (Util.sToi(truncUnit) - truncAmt));
                truncAmt = 0;
            } else if (truncAmt < 0) {
                payment = payment - (Util.sToi(truncUnit) + truncAmt);
                // 할인금액은 절삭 차감
                discountAmt = "" + (Util.sToi(discountAmt) + (Util.sToi(truncUnit) + truncAmt));
                truncAmt = 0;

            }
        }

        Log.e(TAG, "" + truncAmt + "~" + truncUnit);

        // 절삭금액은 +금액으로 표시(미환불 금액)
        truncAmt = Math.abs(truncAmt);
        tvThisPayment.setText(payment + "원");  //주차선납금계산
        Log.d("111", "payment==========" + payment);
        //tvTruncPayment.setText(truncAmt + "원");

    }

    @Override
    protected void request(int type, Object data) {
        // TODO Auto-generated method stub
        super.request(type, data);

        if (type == PayData.PAY_DIALOG_TYPE_COUPON) {
            couponData = (String) data;
        } else if (type == PayData.PAY_DIALOG_TYPE_CASH_RECEIPT) {
            PayVo payVo = (PayVo) data;
            type4Vo.setConfirmCashReceipt(payVo.getCashApprovalNo());
            unPayList.setCash_app_no(payVo.getCashApprovalNo());
            showProgressDialog();

            Map<String, Object> params = new HashMap<String, Object>();
            params.put(Constants.METHOD, Constants.INTERFACEID_CASH_RECEIPT_UPDATE);
            params.put(Constants.API_FORMAT, Constants.JSON_FORMAT);
            params.put("PIO_NUM", unPayList.getPioNum());
            params.put("CASH_APP_NO", payVo.getCashApprovalNo());
            params.put("CASH_APP_DATE", payVo.getCashApprovalDate());
            params.put("BILL_TYPE", "C1");


            String url = Constants.API_SERVER;
            Log.d(TAG, " apiCashReceiptCall url >>> " + url + " params ::: " + params.toString());

            AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>();
            cb.url(url).params(params).type(JSONObject.class).weakHandler(this, "cashReceiptCallback").redirect(true).retry(3).fileCache(false).expire(-1);
            AQuery aq = new AQuery(this);
            aq.ajax(cb);
        } else {
            PayVo payVo = (PayVo) data;
            apiUnpayPay(payVo);
        }

    }

    public void cashReceiptCallback(String url, JSONObject json, AjaxStatus status) {
        closeProgressDialog();
        Log.d(TAG, " parkOutCancelCallback  json ====== " + json);
        if (json != null) {
            String KN_RESULT = json.optString("KN_RESULT");
            //successful ajax call, show status code and json content
            if ("1".equals(json.optString("CD_RESULT"))) {
                btn_receiptPublish.setVisibility(View.GONE);
                btn_receiptPublish2.setVisibility(View.GONE);

                //PayData payData = new PayData(this, OUT_PRINT, IN_PRINT, TICKET_PRINT);
                //payData.printDefault(PayData.PAY_DIALOG_TYPE_UNPAY_SUCCESS, type10Vo, mPrintService);
                publishReceipt();
                MsgUtil.ToastMessage(UnpaidInfoActivity.this, KN_RESULT, Toast.LENGTH_LONG);
                //finish();
            } else {
                //ajax error, show error code
                MsgUtil.ToastMessage(UnpaidInfoActivity.this, KN_RESULT, Toast.LENGTH_LONG);
            }
        } else {
            MsgUtil.ToastMessage(UnpaidInfoActivity.this, Constants.DATA_FAIL);
        }


    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub

        super.onClick(v);

        PayData payData = new PayData(this, OUT_PRINT, IN_PRINT, TICKET_PRINT);

        switch (v.getId()) {
            case R.id.btn_vr_account:
                exitUnpayDialog(mYear, mMonth, mDay);
                break;

            case R.id.btn_discount:

//                try {
//                    // 할인금액 조회
//                    String disRate = Util.isNVL(cch.findCodeByDisRate("DC", tvChargeDiscount.getText().toString()), "0");
//                    if (!"0".equals(disRate) && !is4picSave(parkIO.getPioNum())) {
//                        goToPictureView(parkIO.getPioNum(), parkIO.getCarNo(), Constants.PICTURE_TYPE_DIS, parkIO.getCdPark(), empCode);
//                        Toast.makeText(UnpaidInfoActivity.this, "할인을 선택한 경우 사진등록이 필요합니다.", Toast.LENGTH_LONG).show();
//                        return;
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
                reqDiscount();


                break;
            case R.id.btn_top_left:
                finish();
                break;
            case R.id.btn_payback:
                //todo 할인 등록 시 api정상적오르 했는지 체크 후 등록
                String strDiscountName = "";
                if (tvChargeDiscount.getText().toString().equals("")) {
                    strDiscountName = btnChargeDiscount.getText().toString();
                } else {
                    strDiscountName = tvChargeDiscount.getText().toString();
                }


                type10Vo = new ReceiptType10Vo(Util.getYmdhms("yyyy-MM-dd HH:mm:ss")        // 출력일자
                        , ""                // 거래종류
                        , ""                // 입금액
                        , ""                // 기타이유
                        , unPayList.getPioDay()                     // 미수날짜
                        , btnParkType.getText().toString()         // 주차종류
                        , btnChargeType.getText().toString()     // 요금종류
                        //, btnChargeDiscount.getText().toString() // 할인종류
                        , strDiscountName
                        , "0"                                     // 미납잔액
                        , tvThisPayment.getText().toString().replace("원", "").replace(",", "")                // 총 미납금
                        , unPayList.getAmt().replace("원", "").replace(",", "")                // 미납금액
                        , tvUnpaidType.getText().toString()                // 미납타입
                        , ""            // 총 입금액
                        , Util.getYmdhms("yyyy-MM-dd HH:mm") // 미수환수일
                        , BIZ_NO                    // 사업자번호
                        , BIZ_NAME
                        , BIZ_TEL
                        , unPayList.getKnPark()
                        , unPayList.getParkIn()  //입차시간
                        , unPayList.getParkOut() //출차시간
                );

                type10Vo.setTitle("       [미수환수영수증]");
                type10Vo.setPioNum(unPayList.getPioNum());
                type10Vo.setCarNum(carNo);
                type10Vo.setParkName(parkName);
                type10Vo.setEmpName(empName);
                type10Vo.setEmpPhone(empPhone);
                type10Vo.setEmpTel(empTel);
                type10Vo.setEmpBusiness_Tel(empBusiness_TEL);
                type10Vo.setOutPrint(Preferences.getValue(this, Constants.PREFERENCE_OUT_PRINT, ""));
                //unpayDialog(btnParkType.getText().toString(), String.valueOf(unPayList.getSumAmt()), type10Vo);
                unpayDialog(btnParkType.getText().toString(), String.valueOf(tvThisPayment.getText().toString().replace("원", "").replace(",", "")), type10Vo);
                break;
            case R.id.btn_Receipt:
                //TODO 미수차량 주차장이 따로 받는게 있는지 확인
                if (UnpayType.equals("K3")) {
                    publishReceipt();
                } else {
//                    if (Util.sToi(unPayList.getPioDay()) >= 20180902) {
                    Log.d("333", "===============");
                        getBackVcnt();
//                    } else {
//                    unpayReceipt();
//                    }
                }
                break;
            case R.id.btn_receiptPublish:
                type4Vo = new ReceiptType4Vo(Util.getYmdhms("yyyy-MM-dd HH:mm:ss")    // 출력일자
                        , ""    // 주차종류 :
                        , ""        // 주차시간 :
                        , ""        // 주차면 :
                        , ""        // 할인종류 :
                        , ""        // 입차시간 :
                        , ""        // 출차시간 :
                        , ""            // 납부금액 :
                        , ""        // 선납금액 :
                        , ""        // 주차요금 :
                        , ""        // 미납금액 :
                        , ""        // 할인금액 :
                        , BIZ_NO                    // 사업자번호
                        , BIZ_NAME
                        , BIZ_TEL);

                type4Vo.setPioNum(unPayList.getPioNum());
                type4Vo.setCarNum(carNo);
                type4Vo.setParkName(parkName);
                type4Vo.setEmpName(empName);
                type4Vo.setEmpPhone(empPhone);
                type4Vo.setEmpTel(empTel);
                type4Vo.setCouponAmt("");
                type4Vo.setEmpBusiness_Tel(empBusiness_TEL);
                type4Vo.setConfirmCardNum("");
                type4Vo.setCardNo(unPayList.getCarNo());
                type4Vo.setConfirmCashReceipt(unPayList.getCash_app_no());
                type4Vo.setOutPrint(Preferences.getValue(this, Constants.PREFERENCE_OUT_PRINT, ""));
                cashReceiptShowDialog(Util.sToi(tvThisPayment.getText().toString().replace(",", "").replace("원", "")), type4Vo, btnParkType.getText().toString(), "AFTER");
                break;
            case R.id.btn_receiptPublish2:
                type4Vo = new ReceiptType4Vo(Util.getYmdhms("yyyy-MM-dd HH:mm:ss")    // 출력일자
                        , ""    // 주차종류 :
                        , ""        // 주차시간 :
                        , ""        // 주차면 :
                        , ""        // 할인종류 :
                        , ""        // 입차시간 :
                        , ""        // 출차시간 :
                        , ""            // 납부금액 :
                        , ""        // 선납금액 :
                        , ""        // 주차요금 :
                        , ""        // 미납금액 :
                        , ""        // 할인금액 :
                        , BIZ_NO                    // 사업자번호
                        , BIZ_NAME
                        , BIZ_TEL);

                type4Vo.setPioNum(unPayList.getPioNum());
                type4Vo.setCarNum(carNo);
                type4Vo.setParkName(parkName);
                type4Vo.setEmpName(empName);
                type4Vo.setEmpPhone(empPhone);
                type4Vo.setEmpTel(empTel);
                type4Vo.setCouponAmt("");
                type4Vo.setEmpBusiness_Tel(empBusiness_TEL);
                type4Vo.setConfirmCardNum("");
                type4Vo.setCardNo(unPayList.getCarNo());
                type4Vo.setConfirmCashReceipt(unPayList.getCash_app_no());
                type4Vo.setOutPrint(Preferences.getValue(this, Constants.PREFERENCE_OUT_PRINT, ""));
                cashReceiptShowDialog(Util.sToi(tvReturnAmt.getText().toString().replace(",", "").replace("원", "")), type4Vo, btnParkType.getText().toString(), "AFTER");
                break;
            case R.id.btnPicture:
                goToPictureViewUnpay(parkIO.getPioNum(), parkIO.getCarNo(), Constants.PICTURE_TYPE_O1, parkIO.getCdPark(), empCode);
                break;
        }

    }


    /**
     * String url = "http://puc.parkingstyle.co.kr:7005/road/service.api";
     * String param = "?act=CARINFO_UPDATE"
     * + "&pin_num="
     * + "&change_dis="
     * + "&change_dis_amt="
     * <p>
     * 패러미터
     * act 'CARINFO_UPDATE'
     * pin_num 주차번호
     * change_dis 할인코드
     * change_dis_amt 할인금액
     * <p>
     * 반환값(JSON)
     * result 실패 0 성공 1
     * count 업데이트된 데이터 수
     * change_dis 입력한 값 그대로 반환
     * change_dis_amt 입력한 값 그대로 반환
     */
    public void reqDiscount() {
        if (tvChargeDiscount.getText().toString().equals("")) {
            Toast.makeText(this, "선택된 할인이 없습니다.", Toast.LENGTH_SHORT).show();
            return;
        }

        String url = Constants.SERVER_HOST + "/road/service.api";

        int dis_amt = 0;

        try {
            dis_amt = retAmt(tvUnPayment.getText().toString().replace("원", "").replace(",", "")) - retAmt(tvThisPayment.getText().toString().replace("원", "").replace(",", ""));
        } catch (NullPointerException e) {
            if (Constants.DEBUG_PRINT_LOG) {
                e.printStackTrace();
            } else {
                System.out.println("예외 발생");
            }
        }


        String param = "?act=CARINFO_UPDATE" + "&pin_num=" + parkIO.getPioNum()
                + "&change_dis=" + cch.findCodeByKnCode("DC", tvChargeDiscount.getText().toString()) + "&change_dis_amt=" + dis_amt;
        Log.d("333", " INTERFACEID_UNPAY_VCNT url >>> " + url + param);

        AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>();
        cb.url(url + param).type(JSONObject.class).weakHandler(this, "callbackDiscount").redirect(true).retry(3).fileCache(false).expire(-1);
        AQuery aq = new AQuery(this);
        aq.ajax(cb);
    }

    public void callbackDiscount(String url, JSONObject json, AjaxStatus status) {

        // successful ajax call
        if (json != null) {
            if ("1".equals(json.optString("result"))) {
                setToast("할인적용에 성공 하였습니다.");
            } else {
                setToast("할인적용에 실패 하였습니다.");
            }
        } else {
            setToast("할인적용에 실패 하였습니다.");
        }
    }

    private int retAmt(String amt) {
        if (amt == null) return 0;

        if ("".equals(amt)) return 0;

        return Integer.parseInt(amt);
    }

    public void getBackVcnt() {
        JSONArray jsonArr = null;
        // successful ajax call
        gVcnt_account = "";
        gIpgm_date = "";

        String url = Constants.API_SERVER + "?" + Constants.METHOD + "=" + Constants.INTERFACEID_UNPAY_VCNT;
        String param = "&pio_num=" + parkIO.getPioNum()
                + "&" + Constants.API_FORMAT + "=" + Constants.JSON_FORMAT;
        Log.d("333", " INTERFACEID_UNPAY_VCNT url >>> " + url + param);

        AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>();
        cb.url(url + param).type(JSONObject.class).weakHandler(this, "callbackGetVcntInfo").redirect(true).retry(3).fileCache(false).expire(-1);
        AQuery aq = new AQuery(this);
        aq.ajax(cb);
    }

    public void callbackGetVcntInfo(String url, JSONObject json, AjaxStatus status) {

        JSONArray jsonArr = null;
        // successful ajax call
        if (json != null) {
            if ("1".equals(json.optString("CD_RESULT"))) {
                jsonArr = json.optJSONArray("RESULT");
                int Len = jsonArr.length();
                try {
                    for (int i = 0; i < Len; i++) {
                        JSONObject res = jsonArr.getJSONObject(i);
                        Log.d("333", res.optString("bankcode") + "~" + res.optString("bankcode").substring(2, 4));
                        gBank_name = cch.findCodeByCdCode("BA", res.optString("bankcode").substring(2, 4));
                        gVcnt_account = res.optString("account");
                        gIpgm_date = res.optString("ipgm_date");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        unpayReceipt();
    }


    private void unpayReceipt() {
        PayData payData = new PayData(this, OUT_PRINT, IN_PRINT, TICKET_PRINT);
        ReceiptType5Vo type5Vo = new ReceiptType5Vo("미수청구서"
                , Util.getYmdhms("yyyy-MM-dd HH:mm:ss")            // 출력일자
                , btnChargeType.getText().toString()      // 요금종류
                , btnChargeDiscount.getText().toString()  // 할인종류
                , unPayList.getSumAmt() //총미납금
                , BIZ_NAME
                , BIZ_BANK_MSG
                , BIZ_BANK
                , BIZ_ACCOUNT_NO
                , BIZ_ACCOUNT_NAME
                , BIZ_COPY_RIGHT
                , BIZ_NO
                , unPayList.getKnPark()//주차장명
                , unPayList.getParkIn()//입차시간
                , unPayList.getParkOut()//출차시간
                , group_code
        );

        type5Vo.setPioNum(unPayList.getPioNum());
        type5Vo.setCarNum(carNo);
        type5Vo.setParkName(parkName);
        type5Vo.setEmpName(empName);
        type5Vo.setEmpPhone(empPhone);
        type5Vo.setEmpTel(empTel);
        type5Vo.setEmpBusiness_Tel(empBusiness_TEL);
        if (!gVcnt_account.equals("")) {
            type5Vo.setKN_BANK(gBank_name);
            type5Vo.setAccount_No(subStringBankAcc(gVcnt_account));
            type5Vo.setBank_MSG("납부기한 : " + gIpgm_date.substring(0, 4) + "-" + gIpgm_date.substring(4, 6) + "-" + gIpgm_date.substring(6, 8));
            type5Vo.setKN_ACCOUNT("가평군청");
        } else {
            type5Vo.setAccount_No(empAccount_No);
            type5Vo.setBank_MSG(empBank_MSG);
            type5Vo.setKN_ACCOUNT(empKN_ACCOUNT);
            type5Vo.setKN_BANK(empKN_BANK);
        }

        //type5Vo.setGroupCode(empGroupCD);
        type5Vo.setOutPrint(Preferences.getValue(this, Constants.PREFERENCE_OUT_PRINT, ""));
        payData.printDefault(PayData.PAY_DIALOG_TYPE_UNPAY_BILL, type5Vo, mPrintService);
        payData.ImagePrint(empLogo_image);
        try {
            payData.printText("\n\n");
        } catch (UnsupportedEncodingException e) {
            System.out.println("UnsupportedEncodingException 예외 발생");
        }
    }

    private void publishReceipt() {
        PayData payData = new PayData(this, OUT_PRINT, IN_PRINT, TICKET_PRINT);
        type10Vo = new ReceiptType10Vo(Util.getYmdhms("yyyy-MM-dd HH:mm:ss")        // 출력일자
                , ""                // 거래종류
                , ""                // 입금액
                , ""                // 기타이유
                , unPayList.getPioDay()                     // 미수날짜
                , btnParkType.getText().toString()         // 주차종류
                , btnChargeType.getText().toString()     // 요금종류
                , btnChargeDiscount.getText().toString() // 할인종류
                , "0"                                     // 미납잔액
                , tvThisPayment.getText().toString().replace("원", "").replace(",", "")    // 총 미납금
                , unPayList.getAmt().replace("원", "").replace(",", "")                    // 미납금액
                , tvUnpaidType.getText().toString()    // 미납타입
                , unPayList.getSubmitAmt()            // 총 입금액
                , unPayList.getSubmitDate()        // 미수환수일자
                , BIZ_NO                            // 사업자번호
                , BIZ_NAME
                , BIZ_TEL
                , unPayList.getKnPark()
                , unPayList.getParkIn()//입차시간
                , unPayList.getParkOut() //출차시간
        );

        type10Vo.setTitle("       [미수환수영수증]");
        type10Vo.setPioNum(unPayList.getPioNum());
        type10Vo.setConfirmCashReceipt(unPayList.getCash_app_no());
        type10Vo.setCarNum(carNo);
        type10Vo.setParkName(parkName);
        type10Vo.setEmpName(empName);
        type10Vo.setEmpPhone(empPhone);
        type10Vo.setEmpTel(empTel);
        type10Vo.setEmpBusiness_Tel(empBusiness_TEL);
        type10Vo.setOutPrint(Preferences.getValue(this, Constants.PREFERENCE_OUT_PRINT, ""));

        payData.printDefault(PayData.PAY_DIALOG_TYPE_UNPAY_SUCCESS, type10Vo, mPrintService);

        payData.ImagePrint(empLogo_image);
        try {
            payData.printText("\n\n");
        } catch (UnsupportedEncodingException e) {
            System.out.println("UnsupportedEncodingException 예외 발생");
        }

    }

    /**
     * 미납결재
     *
     * @param payVo
     */
    private void apiUnpayPay(PayVo payVo) {

        if (payVo == null) {
            payVo = new PayVo();
            payVo.setCashAmt("0");
            payVo.setCardAmt("0");
        }

        Map<String, Object> params = new HashMap<String, Object>();

        // method 셋팅...
        params.put(Constants.METHOD, Constants.INTERFACEID_PARK_UNPAY_PAY_NEW);
        // format
        params.put(Constants.API_FORMAT, Constants.JSON_FORMAT);
        // 주차번호 
        params.put("PIO_NUM", unPayList.getPioNum());
        // CD_PARK: 주차장번호
        params.put("CD_PARK", parkCode);
        // EMP_CD : 근무자코드
        params.put("EMP_CD", empCode);

        // CAR_NO : 차량번호
        params.put("CAR_NO", carNo);

        // 현금 영수증 관련
        params.put("CASH_APPROVAL_NO", payVo.getCashApprovalNo());
        params.put("CASH_APPROVAL_DATE", payVo.getCashApprovalDate());

        // 카드 결제 관련
        params.put("TRANSACTION_NO", payVo.getTransactionNo());
        params.put("APPROVAL_NO", payVo.getApprovalNo());
        params.put("APPROVAL_DATE", payVo.getApprovalDate());
        params.put("CARD_NO", payVo.getCardNo());
        params.put("CARD_COMPANY", payVo.getCardCompany());
        params.put("CATNUMBER", payVo.getCatNumber());

        // CASH_AMT :현금
        params.put("CASH_AMT", payVo.getCashAmt());
        // CARD_AMT : 카드금액
        params.put("CARD_AMT", payVo.getCardAmt());
        // TRUNC_AMT:절삭금액
        String parkOut = unPayList.getParkOut().substring(0, 4) + "-" + unPayList.getParkOut().substring(4, 6) + "-" + unPayList.getParkOut().substring(6, 8) + " " + tvParkTime_ed.getText().toString() + ":00";
        //params.put("TRUNC_AMT", tvTruncPayment.getText().toString().replace("원","").replace(",", ""));
        params.put("TRUNC_AMT", truncAmt);
        params.put("DIS_CD", cch.findCodeByKnCode("DC", tvChargeDiscount.getText().toString()));
        //params.put("DIS_AMT", tvMinusPayment.getText().toString().replace("원","").replace(",", ""));
        if (Util.sToi(unPayList.getOffAmt()) > 0) {
            int sum = Util.sToi(tvMinusPayment.getText().toString().replace("원", "").replace(",", "")) - Util.sToi(unPayList.getOffAmt());
            params.put("DIS_AMT", String.valueOf(sum));
        } else {
            params.put("DIS_AMT", tvMinusPayment.getText().toString().replace("원", "").replace(",", ""));
        }

        params.put("PARK_OUT", parkOut);


        String real_amt = "0";
        //쿠폰금액이 주차원금을 넘었을 경우 쿠폰금액을 주차원금으로... Yoon-1.09
        if (Integer.valueOf(payVo.getCashAmt()) > 0) {
            real_amt = payVo.getCashAmt();
        } else {
            real_amt = payVo.getCardAmt();
        }

        if (payVo.getCouponAmt() != "") {
            if (Integer.valueOf(real_amt) > 0) {

                if (Integer.valueOf(real_amt) < Integer.valueOf(payVo.getCouponAmt())) {
                    params.put("COUPON_AMT", real_amt);
                } else {
                    params.put("COUPON_AMT", payVo.getCouponAmt());
                }
            } else {
                params.put("COUPON_AMT", payVo.getCouponAmt());
            }
        }

        if (couponData != null)
            params.put("COUPON_DATA", couponData);

        showProgressDialog(false);

        String url = Constants.API_SERVER;
        Log.d(TAG, " apiUnpayPay url >>> " + url + " " + params);

        AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>();
        cb.url(url).params(params).type(JSONObject.class).weakHandler(this, "unpayPayCallback").redirect(true).retry(3).fileCache(false).expire(-1);
        AQuery aq = new AQuery(this);
        aq.ajax(cb);

    }

    public void unpayPayCallback(String url, JSONObject json, AjaxStatus status) {
        closeProgressDialog();
        Log.d(TAG, " unpayPayCallback  json ====== " + json);
        if (json != null) {
            String KN_RESULT = json.optString("KN_RESULT");
            //successful ajax call, show status code and json content
            if ("1".equals(json.optString("CD_RESULT"))) {
                MsgUtil.ToastMessage(UnpaidInfoActivity.this, KN_RESULT, Toast.LENGTH_LONG);
                Intent intent = new Intent();
                intent.putExtra("RESULT_PAY_OK", "1");
                setResult(RESULT_OK, intent);
                finish();
            } else {
                //ajax error, show error code
                MsgUtil.ToastMessage(UnpaidInfoActivity.this, KN_RESULT, Toast.LENGTH_LONG);
            }
        } else {
            MsgUtil.ToastMessage(UnpaidInfoActivity.this, Constants.DATA_FAIL);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode,
                                    Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {

            case PayData.PAY_DIALOG_TYPE_UNPAY_CARD:  //카드결제

                String transactionNo = "";
                String approvalNo = "";
                String approvalDate = "";
                String cardNo = "";
                String cardAmt = "";
                String cardCompany = "";
//				String pioNum 	     = "";
//				String carNum 	     = "";			  
                try {
                    //Log.d("111", "mPrintService.getState() : " + mPrintService.getState());
                    try {
                        mPrintService.stop();
                        if (mPrintService.getState() == 0) {

                            BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(mBluetoothAddress);
                            mPrintService.connect(device, true);
                        }
                    }  catch (IllegalArgumentException e) {
                        System.out.println("IllegalArgumentException 예외 발생");
                    } catch (NullPointerException e) {
                        System.out.println("NullPointerException 예외 발생");
                    }

                    //Log.d(TAG, "approval : " + Preferences.getValue(this, Constants.PREFERENCE_MCPAY_APPROVAL_NO, ""));

                    if (data != null) {
                        Bundle eB = data.getExtras();
                        if (eB != null) {
                            /**
                             Log.d(TAG, " eB >>>> " + eB );
                             for (String key : eB.keySet()) {
                             Log.d(TAG, " eB :::: " + key + " => " + eB.get(key) );
                             }
                             **/
                        }

                        transactionNo = Util.replaceNullIntent(eB, "transactionNo");
                        approvalNo = Util.replaceNullIntent(eB, "approvalNo");
                        approvalDate = Util.replaceNullIntent(eB, "approvalDate");
                        cardNo = Util.replaceNullIntent(eB, "cardNo");
                        cardCompany = Util.replaceNullIntent(eB, "cardCompany");
                        cardAmt = Util.isNVL(Util.replaceNullIntent(eB, "cardAmt"), "0");
//					   pioNum 	 	   	 = Util.replaceNullIntent(eB,"pioNum"); 
//					   carNum 	 	   	 = Util.replaceNullIntent(eB,"carNum");
                    }

                    transactionNo = Preferences.getValue(this, Constants.PREFERENCE_MCPAY_TRANSACTION_NO, "");
                    approvalNo = Preferences.getValue(this, Constants.PREFERENCE_MCPAY_APPROVAL_NO, "");
                    approvalDate = Preferences.getValue(this, Constants.PREFERENCE_MCPAY_APPROVAL_DATE, "");
                    cardNo = Preferences.getValue(this, Constants.PREFERENCE_MCPAY_CARD_NO, "");
                    cardCompany = Preferences.getValue(this, Constants.PREFERENCE_MCPAY_CARD_COMPANY, "");
                    cardAmt = Preferences.getValue(this, Constants.PREFERENCE_MCPAY_TOTAL_AMT, "");

                    if (transactionNo == "") {
                        Log.d(TAG, "Card error");
                        return;
                    }
                    //prepayDialog.dismiss();
                    //cardAmt = tvThisPayment.getText().toString().replaceAll("원", "");

                    if (cardNo.length() >= 12) {
                        cardNo = cardNo.substring(0, 4) + cardNo.substring(12);
                    }

                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        System.out.println("InterruptedException 예외 발생");
                    }

                    Bundle eB = new Bundle();
                    eB.putString("transactionNo", transactionNo);
                    eB.putString("approvalNo", approvalNo);
                    eB.putString("approvalDate", approvalDate);
                    eB.putString("cardNo", cardNo);
                    eB.putString("cardCompany", cardCompany);
                    eB.putString("cardAmt", cardAmt);
                    eB.putString("pioNum", gReceiptVo.getPioNum());
                    eB.putString("carNum", gReceiptVo.getCarNum());

                    //((ReceiptType10Vo)gReceiptVo).setCouponAmt(payVo.getCouponAmt());
                    //((ReceiptType10Vo)gReceiptVo).setTotalInputAmt(String.valueOf(cardAmt));


                    PayData payData = new PayData(this, OUT_PRINT, IN_PRINT, TICKET_PRINT);
                    payData.printPayment(PayData.PAY_DIALOG_TYPE_UNPAY_CARD, gReceiptVo, mPrintService, eB, null);
                    payData.ImagePrint(empLogo_image);
                    try {
                        payData.printText("\n\n");
                    } catch (UnsupportedEncodingException e) {
                        if (Constants.DEBUG_PRINT_LOG) {
                            e.printStackTrace();
                        } else {
                            System.out.println("예외 발생");
                        }
                    }


                } catch (NullPointerException e) {
                    if (Constants.DEBUG_PRINT_LOG) {
                        e.printStackTrace();
                    } else {
                        System.out.println("예외 발생");
                    }
                }

                InitPreferences();

                // 서버 결제 로그 저장 step2
                String url = Constants.API_SERVER + "?" + Constants.METHOD + "=" + Constants.INTERFACEID_LOG_PARAMETER;
                String param = "&transactionNo=" + transactionNo
                        + "&approvalNo=" + approvalNo
                        + "&approvalDate=" + approvalDate
                        + "&cardNo=" + cardNo
                        + "&cardCompany=" + cardCompany
                        + "&cardAmt=" + cardAmt
                        + "&class=" + this.getClass().getSimpleName()
                        + "&type=pay_step2"
                        + "&" + Constants.API_FORMAT + "=" + Constants.JSON_FORMAT;


                Log.d(TAG, " apiParkLogCall step 2 url >>> " + url + param);

                AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>();
                cb.url(url + param).type(JSONObject.class).weakHandler(this, "").redirect(true).retry(3).fileCache(false).expire(-1);
                AQuery aq = new AQuery(this);
                aq.ajax(cb);

                //unpayDialog.dismiss();
                PayVo payVo = new PayVo();
                payVo.setTransactionNo(transactionNo);
                payVo.setApprovalNo(approvalNo);
                payVo.setApprovalDate(approvalDate);
                payVo.setCardNo(cardNo);
                payVo.setCardCompany(cardCompany);
                payVo.setCardAmt(cardAmt);
                payVo.setCashAmt("0");
                payVo.setCatNumber(catNumber);

                request(PayData.PAY_DIALOG_TYPE_UNPAY_CARD, payVo);
                break;
            case PayData.PAY_DIALOG_TYPE_CASH_RECEIPT:  // 현금영수증 출 차 취 소
                try {
                    mPrintService.stop();
                    if (mPrintService.getState() == 0) {

                        BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(mBluetoothAddress);
                        mPrintService.connect(device, true);
                    }
                }  catch (IllegalArgumentException e) {
                    System.out.println("IllegalArgumentException 예외 발생");
                } catch (NullPointerException e) {
                    System.out.println("NullPointerException 예외 발생");
                }

                String approvalNo2 = Preferences.getValue(this, Constants.PREFERENCE_MCPAY_APPROVAL_NO, "");
                String approvalDate2 = Preferences.getValue(this, Constants.PREFERENCE_MCPAY_APPROVAL_DATE, "");
                String amt_tot = Preferences.getValue(this, Constants.PREFERENCE_MCPAY_TOTAL_AMT, "");

                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    System.out.println("InterruptedException 예외 발생");
                }


                if (("").equals(approvalNo2)) {
                    return;
                }

                PayVo payVo2 = new PayVo();  //현금영수증참조
                payVo2.setCashApprovalNo(approvalNo2);
                payVo2.setCashApprovalDate(approvalDate2);
                tvCashBill.setTag(payVo2);
                tvCashBill.setText("발행");
                InitPreferences();

                //cashReceiptRun(Util.sToi(amt_tot), g_vo);
                ExitPaymentData exitVo = new ExitPaymentData();
                exitVo.setCashApprovalNo(approvalNo2);
                exitVo.setCashApprovalDate(approvalDate2);
                request(PayData.PAY_DIALOG_TYPE_CASH_RECEIPT, exitVo);

                if (cashReceiptDialog != null) {
                    cashReceiptDialog.dismiss();
                    cashReceiptDialog = null;
                }
                g_vo = null;
                tvCashBill = null;
                break;

        }
    }


    @Override
    protected void requestApi(String type, Object data) {
        if (type == Constants.INTERFACEID_PARK_IO) {
            ArrayList<ParkIO> list = (ArrayList<ParkIO>) data;
            parkIO = new ParkIO();
            if (list != null) {
                for (ParkIO pIO : list) {
                    if (pIO.getPioNum().equals(unPayList.getPioNum())) {
                        parkIO = pIO;
                        setParkTime(1);
                    }
                }
            }
        }
    }


    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
    }


    public void DialogTimePicker(final TextView tv, final int Tag) {

        TimePickerDialog.OnTimeSetListener mTimeSetListener = new TimePickerDialog.OnTimeSetListener() {
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

                String t = (hourOfDay < 10 ? "0" + hourOfDay : hourOfDay) + ":" + (minute < 10 ? "0" + minute : minute);

                int startWorkTime = Integer.parseInt(workStartTime.replace(":", ""));
                int endWorkTime = Integer.parseInt(workEndTime.replace(":", ""));
                int dTime = Integer.parseInt(t.replace(":", ""));


                Log.d(TAG, dTime + "~" + startWorkTime + "~" + endWorkTime);
                if (dTime < startWorkTime || dTime > endWorkTime) {

                    //defaultEndTime = workEndTime.substring(0, 2) + ":" + workEndTime.substring(2);
                    //tv.setText(defaultEndTime);
                    tvParkTime_ed.setText(unPayList.getParkOut().substring(8, 10) + ":" + unPayList.getParkOut().substring(10, 12));

                    return;
                }
				
				/*
				if (tv == tvParkTime_st) {
					defaultStartTime = t;
					
					// 선택한 시작시간이 종료시간보다 클 경우 자동으로 종료 시간을 근무 종료 시간으로 설정
					if (dTime > Integer.parseInt(tvParkTime_ed.getText().toString().replace(":", "").trim())) {
						tvParkTime_ed.setText(workEndTime.substring(0, 2) + ":" + workEndTime.substring(2));
					}
					
				} else {
					defaultEndTime = t;
					// 선택한 종료시간이 시작시간보다 작을 경우 자동으로 시작 시간을 근무 시작 시간으로 설정
					if (dTime < Integer.parseInt(tvParkTime_st.getText().toString().replace(":", "").trim())) {
						tvParkTime_st.setText(workStartTime.substring(0, 2) + ":" + workEndTime.substring(2));
					}
				}
				*/
                if (dTime < Integer.parseInt(tvParkTime_st.getText().toString().replace(":", "").trim())) {
                    tvParkTime_ed.setText(unPayList.getParkOut().substring(8, 10) + ":" + unPayList.getParkOut().substring(10, 12));
                    return;
                }

                if (dTime > Integer.parseInt(tvParkTime_ed.getText().toString().replace(":", "").trim())) {
                    tvParkTime_ed.setText(unPayList.getParkOut().substring(8, 10) + ":" + unPayList.getParkOut().substring(10, 12));
                    return;
                }

                tv.setText(t);

                switch (Tag) {
                    case PREPAY_TYPE:    // 선납금 팝업
                        //minusTimeCheck(tvParkTime_st.getText().toString().replace(":",""), tvParkTime_ed.getText().toString().replace(":",""),tvParkTime_time);
                        setParkAmt(2);
                        break;
                }
            }
        };

        String timeStr = "";
        String timeHH = "";
        String timeMi = "";
        if (tv == tvParkTime_st) {
            timeStr = tvParkTime_st.getText().toString().replaceAll(":", "");
        } else {
            timeStr = tvParkTime_ed.getText().toString().replaceAll(":", "");
        }

        timeStr = tvParkTime_ed.getText().toString().replaceAll(":", "");

        timeHH = timeStr.substring(0, 2);
        timeMi = timeStr.substring(2);

        TimePickerDialog timePicker = new TimePickerDialog(this, R.style.CustomDatePickerDialog,
                mTimeSetListener,
                Integer.parseInt(timeHH),
                Integer.parseInt(timeMi), false);
        timePicker.show();
    }

    private void apiParkInfoCall(String date, String cdpark) {

        if (cdpark == null) {
            setToast("앱이 비정상적 데이터로 인하여 종료됩니다. 다시시작해 주세요.");
            setDestroyList();
            return;
        }

        showProgressDialog();

        // AQUtility.cleanCacheAsync(this);
        String url = Constants.API_SERVER + "?" + Constants.METHOD + "=" + Constants.INTERFACEID_PARKINFO
                + "&CD_PARK=" + cdpark
                + "&WORK_DAY=" + date
                + "&" + Constants.API_FORMAT + "=" + Constants.JSON_FORMAT;

        Log.d(TAG, " apiParkInfoCall url >>> " + url);

        AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>();
        cb.url(url).type(JSONObject.class).weakHandler(this, "parkInfoCallback").redirect(true).retry(3).fileCache(false).expire(-1);
        AQuery aq = new AQuery(this);
        aq.ajax(cb);

    }

    public void parkInfoCallback(String url, JSONObject json, AjaxStatus status) {
        Log.d(TAG, " parkInfoCallback  json ====== " + json);
        closeProgressDialog();

        int Len = 0;

        // successful ajax call
        if (json != null) {
            if ("1".equals(json.optString("CD_RESULT"))) {
                JSONArray jsonArr = json.optJSONArray("RESULT");
                Len = jsonArr.length();
                boolean isPicture = false;
                for (int i = 0; i < Len; i++) {
                    JSONObject res = jsonArr.optJSONObject(i);
                    // 근무시간
                    m_workStartTime = res.optString("START_TIME");
                    // 근무종료시간
                    m_workEndTime = res.optString("END_TIME");
                    // 무료 주차 시작 시간
                    m_serviceStartTime = res.optString("SERVICE_START_TIME", "0");
                    // 무료 주차 종료시간
                    m_serviceEndTime = res.optString("SERVICE_END_TIME", "0");
                    m_parkClass = res.optString("CD_STATE");

                    // 입차서비스 구간(분)
                    m_inService = res.optString("IN_SERVICE");
                    // 출차서비스 구간(분)
                    m_outService = res.optString("OUT_SERVICE");
                    // 회차서비스 구간(분)

                } // end for

                // 요금종류코드
                String chargeClassCode = "";

                if ("2".equals(m_parkClass)) {
                    chargeClassCode = "AM2";
                } else if ("3".equals(m_parkClass)) {
                    chargeClassCode = "AM3";
                } else if ("4".equals(m_parkClass)) {
                    chargeClassCode = "AM4";
                } else if ("5".equals(m_parkClass)) {
                    chargeClassCode = "AM5";
                } else if ("6".equals(m_parkClass)) {
                    chargeClassCode = "AM6";
                }

                apiCodeClassCall(chargeClassCode);
            } else {
                // TODO 에러 팝업 처리
                String KN_RESULT = json.optString("KN_RESULT");
                Toast.makeText(this, KN_RESULT, Toast.LENGTH_LONG).show();
            }
        } else {
            //	  Toast.makeText(this, Constants.DATA_FAIL, Toast.LENGTH_LONG).show();
        }

    }

    /**
     * 공통 코드 조회
     */
    private void apiCodeClassCall(String CD_CLASS) {
        showProgressDialog(false);
        String url = Constants.API_SERVER + "?" + Constants.METHOD + "=" + Constants.INTERFACEID_COMMONCODE;
        String param = "&CD_CLASS=" + CD_CLASS
                + "&" + Constants.API_FORMAT + "=" + Constants.JSON_FORMAT;
        Log.d(TAG, " apiCodeClassCall url >>> " + url + param);
        AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>();
        cb.url(url + param).type(JSONObject.class).weakHandler(this, "codeClassCallback").redirect(true).retry(3).fileCache(false).expire(-1);
        AQuery aq = new AQuery(this);
        aq.ajax(cb);
    }

    public void codeClassCallback(String url, JSONObject json, AjaxStatus status) {

        closeProgressDialog();
        Log.d("111", "============= codeClassCallback  url ====== " + url);

        // successful ajax call
        if (json != null) {
            String KN_RESULT = json.optString("KN_RESULT");
            if ("1".equals(json.optString("CD_RESULT"))) {
                JSONArray m_codeJsonArr = json.optJSONArray("RESULT");

                int Len = m_codeJsonArr.length();
                try {

                    for (int i = 0; i < Len; i++) {
                        JSONObject res = m_codeJsonArr.getJSONObject(i);
                        m_baseSection = res.optString("BASE_TIME", "0");
                        // 기본금액
                        m_baseAmt = res.optString("BASE_AMT", "0");
                        // 반복구간
                        m_repeatSection = res.optString("DIS_TIME", "0");
                        // 반복금액
                        m_repeatAmt = res.optString("DIS_AMT", "0");
                        // 주차최대금액
                        m_parkMaxAmt = res.optInt("CD_VALUE", 0);
                        // 일일권 금액
                    } // end for

                    //setParkAmt(1);

                } catch (JSONException e) {
                    System.out.println("JSONException 예외 발생");
                }
            } else {
                MsgUtil.ToastMessage(this, KN_RESULT, Toast.LENGTH_LONG);
            }
        } else {
            MsgUtil.ToastMessage(this, Constants.DATA_FAIL, Toast.LENGTH_LONG);
        }
    }

    public Dialog exitUnpayDialog(final int yyear, final int mon, final int dday) {
        final Dialog exitUnpayDialog;

        cch = CodeHelper.getInstance(this);

        final TextView tvUnpayReason;

        exitUnpayDialog = new Dialog(this,
                android.R.style.Theme_Holo_Light_Dialog);

        exitUnpayDialog.setContentView(R.layout.virtual_unpay_dialog);
        exitUnpayDialog.setTitle("미수가상계좌");

        bts_impdate = (Button) exitUnpayDialog.findViewById(R.id.btn_impdate);
        bts_impdate.setText(yyear + Util.addZero(mon) + Util.addZero(dday));

        ((Button) exitUnpayDialog.findViewById(R.id.btn_impdate)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new calendarDlg(exitUnpayDialog.getContext(), bts_impdate).show();
            }
        });

        btn_bank = (Button) exitUnpayDialog.findViewById(R.id.btn_bank);
        btn_bank.setText("농협");
//        setCommonItem("BA", Constants.COMMON_TYPE_UNPAID_TYPE, btn_bank, "11");
        ProcessVcnt(btn_bank.getText().toString(), bts_impdate.getText().toString());
//        btn_bank.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                setCommonDialog("은행종류", "BA", Constants.COMMON_TYPE_UNPAID_TYPE, btn_bank);
//            }
//        });

        ((Button) exitUnpayDialog.findViewById(R.id.btn_ok)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!gVcnt_account_copy.equals("")) {
                    //todo 가상계좌 업데이트 api연동 필요.
                    String maxData = String.valueOf(yyear).substring(2) + "." + Util.addZero(mon) + "." + Util.addZero(dday);
                    String vrInfo = carNo + "\n" + btn_bank.getText().toString() + "\n" + subStringBankAcc(gVcnt_account_copy) + "\n가평군청\n" + tvUnPayment.getText().toString() + "\n" + maxData + "까지";
                    gVcnt_account = gVcnt_account_copy;
                    gIpgm_date = String.valueOf(yyear) + Util.addZero(mon) + Util.addZero(dday);

                    sendSMSDialog(vrInfo);
                    unpayReceipt();
                }
                exitUnpayDialog.dismiss();
            }
        });

        ((Button) exitUnpayDialog.findViewById(R.id.btn_cancel)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                exitUnpayDialog.dismiss();
            }
        });

        exitUnpayDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
            }
        });

        exitUnpayDialog.show();
        return exitUnpayDialog;
    }

    private String mDate;
    private int mYear;
    private int mMonth;
    private int mDay;

    public class calendarDlg extends Dialog {

        public class myCalendar extends CalendarInfo {
            private Button btn_target;

            public myCalendar(Context context, LinearLayout layout, Button _btn) {
                super(context, layout);
                btn_target = _btn;
            }

            @Override
            public void myClickEvent(int yyyy, int MM, int dd) {
                mYear = yyyy;
                mMonth = MM + 1;
                mDay = dd;
                mDate = mYear + Util.addZero(mMonth) + Util.addZero(mDay);

                btn_target.setText(mDate);
                calendarDlg.this.cancel();
                super.myClickEvent(yyyy, MM, dd);
            }
        }

        TextView tvs[];
        Button btns[];

        public calendarDlg(Context context, Button btnCal) {

            super(context, android.R.style.Theme_Holo_Light_Dialog_NoActionBar);
            setContentView(R.layout.calendar_layout);

            LinearLayout lv = (LinearLayout) findViewById(R.id.calendar_ilayout);

            tvs = new TextView[3];
            tvs[0] = (TextView) findViewById(R.id.tv1);
            tvs[1] = (TextView) findViewById(R.id.tv2);
            tvs[2] = null;

            btns = new Button[4];
            btns[0] = null;
            btns[1] = null;
            btns[2] = (Button) findViewById(R.id.Button03);
            btns[3] = (Button) findViewById(R.id.Button04);

            myCalendar cal = new myCalendar(context, lv, btnCal);

            cal.setControl(btns);
            cal.setViewTarget(tvs);
            cal.initCalendar(btnCal.getText().toString());
        }
    }


    public Dialog sendSMSDialog(final String vrInfo) {
        final Dialog mDialog;

        mDialog = new Dialog(this,
                android.R.style.Theme_Holo_Light_Dialog);

        mDialog.setContentView(R.layout.send_sms_dialog);
        mDialog.setTitle("가상계좌 문자서비스");

        final EditText ed_sms_num = (EditText) mDialog.findViewById(R.id.ed_sms_num);

        Button btn_confirm = (Button) mDialog.findViewById(R.id.btn_confirm);
        Button btn_cancel = (Button) mDialog.findViewById(R.id.btn_cancel);

        btn_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                // SMS 발송
                Uri uri = Uri.parse("smsto:" + ed_sms_num.getText().toString());
                Intent it = new Intent(Intent.ACTION_SENDTO, uri);
                it.putExtra("sms_body", vrInfo);
                startActivity(it);


                mDialog.dismiss();
            }
        });

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });

        mDialog.show();
        return mDialog;
    }

    private String subStringBankAcc(String acc) {
        StringBuffer account = new StringBuffer();
        if (acc.length() > 0) {
            int idx = 0;
            for (int i = 0; i < acc.length(); i++) {
                if (i > 3 && i % 4 == 0) {
                    account.append(acc, i - 4, i);
                    account.append("-");
                    idx = i;
                } else if (i == acc.length() - 1) {
                    account.append(acc, idx, acc.length());
                }
            }
        }
        return account.toString();
    }

}
