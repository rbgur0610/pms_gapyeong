package com.pms.gapyeong.activity;

import java.util.ArrayList;

import android.util.Log;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.TextView.OnEditorActionListener;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.pms.gapyeong.R;
import com.pms.gapyeong.adapter.ExitManagerAdapter;
import com.pms.gapyeong.common.CalendarInfo;
import com.pms.gapyeong.common.Constants;
import com.pms.gapyeong.common.Util;
import com.pms.gapyeong.database.CodeHelper;
import com.pms.gapyeong.pay.PayData;
import com.pms.gapyeong.vo.ParkIO;
import com.pms.gapyeong.vo.ReceiptType14Vo;
import com.pms.gapyeong.vo.UnPayManagerItem;

public class ExitManageActivity extends ExitAbstractActivity {
	private Button btn_cal_st;
	private Button btn_cal_ed;
	private Button btnSearchType;
	private Button btnListPrint;
	private TextView tvHistoryNum;
	
	ExitManagerAdapter mAdapter;
	private ListView lvExitManager;
	
	private calendarDlg caledarDialog;
	private Button btn_top_left;
	
	private EditText et_car_number;
	private Button btn_car_search;
	
	private TextView tvTable00;
	private TextView tvTable01;
	private TextView tvTable02;
	private TextView tvTable03;
	private ArrayList<ParkIO> exitList;
	
	private InputMethodManager ipm;
	
	private ArrayList<ParkIO> parkIoList;
	
	private int currentPage   = 1;
	private int totalLen 	  = 0; 
	private boolean lastitemVisibleFlag = false;        //화면에 리스트의 마지막 아이템이 보여지는지 체크

	@Override
	public void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);

		setContentView(R.layout.exit_manage_layout);
		
		exitList = new ArrayList<ParkIO>();
		
		cch = CodeHelper.getInstance(this);
	}

	@Override
	protected void initLayoutSetting() {
		super.initLayoutSetting();

		TextView tv_top_title = (TextView) findViewById(R.id.tv_top_title);
		tv_top_title.setText("출차관리");
		
		//table setting
		tvTable00= (TextView) findViewById(R.id.tvTable00);
		tvTable01= (TextView) findViewById(R.id.tvTable01);
		tvTable02= (TextView) findViewById(R.id.tvTable02);
		tvTable03= (TextView) findViewById(R.id.tvTable03);
		
		btn_top_left = (Button) findViewById(R.id.btn_top_left);
		btn_top_left.setVisibility(View.VISIBLE);
		btn_top_left.setOnClickListener(this);
		
		btnListPrint= (Button) findViewById(R.id.btnListPrint);
		tvHistoryNum = (TextView)findViewById(R.id.tvHistoryNum);
		lvExitManager = (ListView)findViewById(R.id.lvExitManager);
		
		et_car_number  = (EditText)findViewById(R.id.et_car_number);
		btn_car_search = (Button)findViewById(R.id.btn_car_search);
		
		btnSearchType = (Button) findViewById(R.id.btnSearchType);
		btn_cal_st = (Button) findViewById(R.id.btn_cal_st);
		btn_cal_ed = (Button) findViewById(R.id.btn_cal_ed);
		btn_cal_st.setText(Util.getYmdhms("yyyy-MM-dd"));
		btn_cal_ed.setText(Util.getYmdhms("yyyy-MM-dd"));
		
		btn_cal_st.setOnClickListener(this);
		btn_cal_ed.setOnClickListener(this);
		btnSearchType.setOnClickListener(this);
		btnListPrint.setOnClickListener(this);
		
		setCommonItem("IO", Constants.COMMON_TYPE_PARK_STATUS, btnSearchType, 1);
		
		if("출차완료".equals(btnSearchType.getText().toString()) || "입차취소".equals(btnSearchType.getText().toString()))
		{
			btnListPrint.setVisibility(View.VISIBLE);
		}else{
			btnListPrint.setVisibility(View.GONE);
		}
		
		// 관리 메뉴에서 넘어 온 검색날짜 
		Intent intent = getIntent();
		if (intent != null) {
			String search_date = intent.getStringExtra("search_date");
			if(!Util.isEmpty(search_date)){
				btn_cal_st.setText(search_date);
			}
		}
		
		ipm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
		
		et_car_number.setOnEditorActionListener(new OnEditorActionListener() { 
			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				// TODO Auto-generated method stub
				switch (actionId) {
					case EditorInfo.IME_ACTION_SEARCH:
					  requestParkIO();
					break;
					default:
					return false;
				}				
				return false;
			}
		});	
		
		btn_car_search.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				requestParkIO();
			}
		});		
		
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		
		requestParkIO();
	}
	
	
	ReceiptType14Vo type14Vo;
	private static boolean clickCalBtn = true;
	@Override
	public void onClick(View v) {
		super.onClick(v);
		switch (v.getId()) {
		case R.id.btn_top_left:
			finish();
			break;
		case R.id.btnSearchType:
			setCommonDialog("검색종류","IO", Constants.COMMON_TYPE_PARK_STATUS, btnSearchType);
			break;
			
		case R.id.btn_cal_st:
			if(caledarDialog == null) {
				caledarDialog = new calendarDlg(this);
			}
			caledarDialog.show();
			clickCalBtn = true;
			break;
			
			//입/출차 현황 영수증 출력
		case R.id.btnListPrint:
			String title = "";
			if ("입차취소".equals(btnSearchType.getText().toString())) {
				title = "입차취소현황";
			} else if ("입차중".equals(btnSearchType.getText().toString())) {
				title = "입차현황";
			} else {
				title = "출차현황";
			}
			
			type14Vo = new ReceiptType14Vo(title, Util.getYmdhms("yyyy-MM-dd HH:mm:ss"), parkName, empName, empPhone, BIZ_NAME, BIZ_TEL, exitList);
			PayData payData = new PayData(this, OUT_PRINT, IN_PRINT, TICKET_PRINT);
			payData.printDefault(PayData.PAY_TYPE_EXIT_MANAGER, type14Vo, mPrintService);					
			break;
			
		case R.id.btn_cal_ed:
			if(caledarDialog == null) {
				caledarDialog = new calendarDlg(this);
			}			
			caledarDialog.show();
			clickCalBtn = false;
			break;
		}
	}
	
	public void getSelectListDialogData(int tag, String data) {
	
		if (Constants.COMMON_TYPE_PARK_STATUS == tag) {
			// 주차 현황 요청
			
			String date = btn_cal_st.getText().toString();
			Log.d(TAG, "search date : " + date);
			
			btnSearchType.setText(data);
			requestParkIO();
			if("출차완료".equals(btnSearchType.getText().toString()) || "입차취소".equals(btnSearchType.getText().toString()))
			{
				btnListPrint.setVisibility(View.VISIBLE);
			} else {
				btnListPrint.setVisibility(View.GONE);
			}
		}
	}
	
	private void setLayout(){
		
		// 초기화 
		currentPage = 1;
		if(exitList != null){
			exitList.clear();
		}
		
		/**
		 * 미출차시 > 입차정보
		 * 출차시>출차정보 (하단 출차취소/영수증 디세이블)
		 * 입차시 > 입차정보 (출차 및 미납출차는 하단 5개 버튼 모두 디세이블)
		 * 입차취소시 > 입차정보(타이틀 입차취소정보로 변경) 하단버튼 디세이블 모두
		 * 출차취소> 출차정보(타이틀 출차취소정보로변경) 하단버튼 디세이블
		 */
		String title = btnSearchType.getText().toString();
		
		if ("출차완료".equals(title)) {
			tvTable00.setText("출차날짜");
			tvTable01.setText("차량번호");
			tvTable02.setText("주차금액");
			tvTable03.setText("종류(상태)");
			
			btnListPrint.setEnabled(true);
		} else if ("출차취소".equals(title)) {
			tvTable00.setText("출차날짜");
			tvTable01.setText("차량번호");
			tvTable02.setText("주차금액");
			tvTable03.setText("종류(이유)");
			
			btnListPrint.setEnabled(false);
		} else if ("입차취소".equals(title)) {
			tvTable00.setText("입차날짜");
			tvTable01.setText("차량번호");
			tvTable02.setText("입차시간");
			tvTable03.setText("종류(이유)");
			
			btnListPrint.setEnabled(false);
		} else {
			tvTable00.setText("등록날짜");
			tvTable01.setText("차량번호");
			tvTable02.setText("입차시간");
			tvTable03.setText("주차종류");
			
			btnListPrint.setEnabled(true);
		}		
		
		
		mAdapter = new ExitManagerAdapter(this, R.layout.exit_manager_row, exitList, title);
		lvExitManager.setAdapter(mAdapter);
		lvExitManager.setOnItemClickListener(new  OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
				// TODO Auto-generated method stub
				
				String parkIoCode = cch.findCodeByKnCode("IO", btnSearchType.getText().toString());
				Intent intent = null;
				
				ParkIO item = (ParkIO) exitList.get(position);
				
				//"O0".equals(parkIoCode) || 출차취소 
				if ("O2".equals(parkIoCode)){ // 미수 
					Log.d(TAG, " parkIoCode >>> " + parkIoCode);
					apiParkUnpayListCall("K2", item.getCarNo(), parkCode, today, today);
					return;
				} else if ("O1".equals(parkIoCode)) { // 출차완료 
					intent = new Intent(ExitManageActivity.this, ExitInfoActivity.class);
					intent.putExtra("title", "출차정보");
				} else {
					//intent = new Intent(ExitManageActivity.this, EntranceInfoActivity.class);
					intent = new Intent(ExitManageActivity.this, ExitCommitActivity.class);
					intent.putExtra("title", "입차정보");
					intent.putExtra("search_date", btn_cal_st.getText().toString().replace("-",""));
				}
				
				intent.putExtra("parkIO", item);
				intent.putExtra("parkIoName", btnSearchType.getText().toString());
				intent.putExtra("parkIoCode", parkIoCode);
				startActivity(intent);				
			}
		});
		
		lvExitManager.setOnScrollListener(new AbsListView.OnScrollListener() {
		    @Override
		    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
		    	//현재 화면에 보이는 첫번째 리스트 아이템의 번호(firstVisibleItem) + 현재 화면에 보이는 리스트 아이템의 갯수(visibleItemCount)가 리스트 전체의 갯수(totalItemCount) -1 보다 크거나 같을때
		    	lastitemVisibleFlag = (totalItemCount > 0) && (firstVisibleItem + visibleItemCount >= totalItemCount); 
		    }   
		    
		    @Override
		    public void onScrollStateChanged(AbsListView view, int scrollState) {
//	        	 Log.d(TAG, " scrollState >>>> " + scrollState);
//	        	 Log.d(TAG, " OnScrollListener.SCROLL_STATE_IDLE >>>> " + OnScrollListener.SCROLL_STATE_IDLE);
	        	 //OnScrollListener.SCROLL_STATE_IDLE은 스크롤이 이동하다가 멈추었을때 발생되는 스크롤 상태입니다. 
	        	 //즉 스크롤이 바닦에 닿아 멈춘 상태에 처리를 하겠다는 뜻
		         if(scrollState == OnScrollListener.SCROLL_STATE_IDLE && lastitemVisibleFlag) {
		             //TODO 화면이 바닦에 닿을때 처리
		        	 if(totalLen > mAdapter.getCount()){ 
		        		 currentPage++;
		        		 setData();
		        	 }
			     } 		    	
		    }
		});		
		
	}
	
	private void setData(){
		
	   if(parkIoList == null)
	   {
			return;
	   }
	   
	   int page = currentPage;
	   int start_index = (page - 1) * Constants.LIST_SIZE;
	   int last_index  = (page * Constants.LIST_SIZE);
	   
	   if(last_index > totalLen){
		   last_index = totalLen;   
	   }
	   
//	   Log.d(TAG, " start_index >>> " + start_index );
//	   Log.d(TAG, " last_index >>> " + last_index );
	   for(int i=start_index; i<last_index; i++){
		   ParkIO info = parkIoList.get(i);
		   exitList.add(info);			
	   }  // end for 		
	   
	   mAdapter.notifyDataSetChanged();
	   
	}
		
	
	private void requestParkIO() {
		
		// 키보드 숨기기
		if(ipm != null && et_car_number != null){
			ipm.hideSoftInputFromWindow(et_car_number.getWindowToken(), 0);
		}
		
		// 초기화
		if (mAdapter != null) {
			mAdapter.clear();
			mAdapter.notifyDataSetChanged();
		}
		tvHistoryNum.setText("0건");
		
		cch = CodeHelper.getInstance(this);
		String carNo = et_car_number.getText().toString();
		if ("".equals(carNo)) {
			carNo = Util.getEncodeStr("%");
		}
		
		String searchDate = btn_cal_st.getText().toString().replaceAll("-", "");
		String searchEndDate = btn_cal_ed.getText().toString().replaceAll("-", "");
		String searchType = cch.findCodeByKnCode("IO", btnSearchType.getText().toString());
		apiParkIoListCall(carNo, searchType, searchDate, searchEndDate);
	}
		
	@Override
	protected void requestApi(String type, Object data) {
		if (type == Constants.INTERFACEID_PARK_IO) {
			ArrayList<ParkIO> list = (ArrayList<ParkIO>) data;
			
			if(list != null){
				totalLen = list.size();
			} else {
				totalLen = 0;
			}
			
			Log.d(TAG, "totalLen : " + totalLen);
			tvHistoryNum.setText(totalLen+"건");
			parkIoList = list;

			setLayout();
			setData();
		} else if (type == Constants.INTERFACEID_PARKUNPAY_INFO) {
			/**
			 * 주차장 미수정보 조회
			 */
			ArrayList<UnPayManagerItem> unPayList = (ArrayList<UnPayManagerItem>) data;
			
			if(unPayList != null && unPayList.size() > 0){
				Intent i = new Intent(this, UnpaidInfoActivity.class);
				i.putExtra("UNPAY_LIST", unPayList.get(0));
				i.putExtra("UnpayType",  "K2");
				startActivity(i);			
			}
		}
	}	
	
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		switch (requestCode) {
			default :
			break;
		}
	}
	
	public class calendarDlg extends Dialog
	{

		public class myCalendar extends CalendarInfo
		{
			public myCalendar(Context context, LinearLayout layout) 
			{
				super(context, layout);
			}

			@Override
			public void myClickEvent(int yyyy, int MM, int dd) 
			{

				String dateStr = yyyy + "-" + Util.addZero(MM+1) + "-" + Util.addZero(dd);
			
				if(clickCalBtn){
					btn_cal_st.setText(dateStr);
				}else{
					btn_cal_ed.setText(dateStr);
				}
				
				calendarDlg.this.cancel() ;
				super.myClickEvent(yyyy, MM, dd);
				
				// 서버에 재 요청
//				StringBuffer sb = new StringBuffer();
//				sb.append(mYear).append(mMonth).append(mDay);
				requestParkIO();
			}
		}

		TextView tvs[] ;
		Button btns[] ;

		public calendarDlg( Context context ) 
		{

			super(context,android.R.style.Theme_Holo_Light_Dialog_NoActionBar);
			setContentView(R.layout.calendar_layout);

			LinearLayout lv = (LinearLayout)findViewById( R.id.calendar_ilayout ) ;

			tvs = new TextView[3] ;
			tvs[0] = (TextView)findViewById( R.id.tv1 ) ;
			tvs[1] = (TextView)findViewById( R.id.tv2 ) ;
			tvs[2] = null ;

			btns = new Button[4] ;
			btns[0] = null ;
			btns[1] = null ;
			btns[2] = (Button)findViewById( R.id.Button03 ) ;
			btns[3] = (Button)findViewById( R.id.Button04 ) ;

			myCalendar cal = new myCalendar( context, lv ) ;

			cal.setControl( btns ) ;
			cal.setViewTarget( tvs ) ;

			cal.initCalendar(btn_cal_st.getText().toString().replace("-","")) ;
		}
	}
	
}
