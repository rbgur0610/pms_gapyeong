package com.pms.gapyeong.activity;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import android.util.Log;
import android.app.AlertDialog;
import android.bluetooth.BluetoothDevice;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.pms.gapyeong.common.Preferences;
import com.pms.gapyeong.vo.ExitPaymentData;
import com.pms.gapyeong.R;
import com.pms.gapyeong.common.Constants;
import com.pms.gapyeong.common.ImageLoader;
import com.pms.gapyeong.common.MsgUtil;
import com.pms.gapyeong.common.Util;
import com.pms.gapyeong.database.CodeHelper;
import com.pms.gapyeong.pay.PayData;
import com.pms.gapyeong.pay.PayVo;
import com.pms.gapyeong.vo.ParkTicketInfoItem;
import com.pms.gapyeong.vo.PictureInfo;
import com.pms.gapyeong.vo.ReceiptType11Vo;
import com.pms.gapyeong.vo.ReceiptType4Vo;

public class TicketDetailActivity extends BaseActivity {

    private ImageView iv_image;

    Calendar calendar = Calendar.getInstance();

    private Button btn_cal_st;
    private Button btn_cal_ed;

    private Button btn_car_no_1;
    private Button btn_car_no_2;
    private Button btn_car_no_3;
    private Button btn_car_no_4;
    private Button btn_car_no_manual;

    private Button btnChargeType;
    private Button btnChargeDiscount;
    private TextView tvTicketAmt;
    private TextView tvUnPayment;
    private TextView tvPayment;
    private View btn_top_left;
    private TextView tvDateDuring;
    private EditText edCarOwner;
    private EditText edPhoneNumber;
    private EditText edMemo;

    private Button btn_ticket_cancel;
    private Button btn_ticket_print;
    private Button btn_ticket_mod;
    private Button btn_renew_add;
    private Button btn_receiptPublish;

    private ReceiptType11Vo type11Vo;
    private ParkTicketInfoItem ticketData;
    private int disAmt = 0;
    private int finalAmt = 0;

    private String ticketNo = "";
    private String carNo = "";
    private String CD_TYPE = "";
    private String img_ticket_path = "";
    private String img_ticket_file = "";
    private String gTicketDay = "";
    private ReceiptType4Vo type4Vo;

    private ArrayList<PictureInfo> pictureList;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ticket_detail_layout);

        cch = CodeHelper.getInstance(this);
    }

    @Override
    protected void initLayoutSetting() {
        // TODO Auto-generated method stub
        super.initLayoutSetting();

        TextView tv_top_title = (TextView) findViewById(R.id.tv_top_title);
        tv_top_title.setText("정기권상세보기");

        btn_top_left = (Button) findViewById(R.id.btn_top_left);
        btn_top_left.setVisibility(View.VISIBLE);

        tvDateDuring = (TextView) findViewById(R.id.tvDateDuring);
        tvUnPayment = (TextView) findViewById(R.id.tvUnPayment);
        tvPayment = (TextView) findViewById(R.id.tvPayment);
        tvTicketAmt = (TextView) findViewById(R.id.tvTicketAmt);

        btn_car_no_1 = (Button) findViewById(R.id.btn_car_no_1);
        btn_car_no_2 = (Button) findViewById(R.id.btn_car_no_2);
        btn_car_no_3 = (Button) findViewById(R.id.btn_car_no_3);
        btn_car_no_4 = (Button) findViewById(R.id.btn_car_no_4);
        btn_car_no_manual = (Button) findViewById(R.id.btn_car_no_manual);
        btn_receiptPublish = (Button) findViewById(R.id.btn_receiptPublish);
        btn_receiptPublish.setVisibility(View.GONE);

        edCarOwner = (EditText) findViewById(R.id.edCarOwner);
        edPhoneNumber = (EditText) findViewById(R.id.edPhoneNumber);
        edMemo = (EditText) findViewById(R.id.edMemo);

        btnChargeType = (Button) findViewById(R.id.btnChargeType);
        btnChargeDiscount = (Button) findViewById(R.id.btnChargeDiscount);
        iv_image = (ImageView) findViewById(R.id.iv_image);

        btn_cal_st = (Button) findViewById(R.id.btn_cal_st);
        btn_cal_ed = (Button) findViewById(R.id.btn_cal_ed);

        btn_ticket_cancel = (Button) findViewById(R.id.btn_ticket_cancel);
        btn_ticket_print = (Button) findViewById(R.id.btn_ticket_print);
        btn_ticket_mod = (Button) findViewById(R.id.btn_ticket_mod);
        btn_renew_add = (Button) findViewById(R.id.btn_renew_add);

        Intent i = getIntent();
        if (i != null) {
            ticketData = (ParkTicketInfoItem) i.getSerializableExtra("TicketData");

            CD_TYPE = ticketData.getCdType();
            Log.d(TAG, " CD_TYPE >>> " + CD_TYPE);

            gTicketDay = ticketData.getTicketDay();
            edCarOwner.setText(ticketData.getCarOwner());
            edPhoneNumber.setText(ticketData.getOwrnerTel());
            btn_cal_st.setText(Util.dateString(ticketData.getStartDay()));
            btn_cal_ed.setText(Util.dateString(ticketData.getEndDay()));

            long dateDiff = 0;
            try {
                dateDiff = Util.diffOfDate(Util.dateStringExt(ticketData.getStartDay() + "120000"), Util.dateStringExt(ticketData.getEndDay() + "120000"));
            } catch (NullPointerException e) {
                if (Constants.DEBUG_PRINT_LOG) {
                    e.printStackTrace();
                } else {
                    System.out.println("예외 발생");
                }
            }

            //시작과 끝날짜 차이 하루단위로 작성.
            tvDateDuring.setText(dateDiff + "일");
            tvTicketAmt.setText(Util.addComma(ticketData.getTicketAmt()) + "원");
            tvUnPayment.setText(Util.addComma(ticketData.getYetAmt()) + "원");
            tvPayment.setText(Util.addComma(ticketData.getPayAmt()) + "원");

            disAmt = Util.sToi(ticketData.getDisAmt(), 0);
            finalAmt = Util.sToi(ticketData.getPayAmt(), 0);

            Log.d(TAG, " finalAmt >>> " + finalAmt);

            edMemo.setText(ticketData.getComment());

            ticketNo = ticketData.getTicketNo();
            carNo = ticketData.getCarNo();

            setCarnoView(carNo, btn_car_no_1, btn_car_no_2, btn_car_no_3, btn_car_no_4, btn_car_no_manual);

            Constants.KEY_CARNO_1 = btn_car_no_1.getText().toString();
            Constants.KEY_CARNO_2 = btn_car_no_2.getText().toString();
            Constants.KEY_CARNO_3 = btn_car_no_3.getText().toString();
            Constants.KEY_CARNO_4 = btn_car_no_4.getText().toString();
            Constants.KEY_CARNO_MANUAL = btn_car_no_manual.getText().toString();
        }

        btnChargeType.setText(cch.findCodeByCdCode("RP", CD_TYPE));
        btnChargeDiscount.setText(cch.findCodeByCdCode("TIDC", ticketData.getDisCd()));

        Log.d(TAG, " ticketNo >>> " + ticketNo);
        Log.d(TAG, " carNo >>> " + carNo);

        // 정기권취소 가능여부

        Calendar c = Calendar.getInstance();
        String tDay = "";
        String nYear = String.valueOf(c.get(Calendar.YEAR));
        String nMonth = "";
        String nDay = "";

        if ((c.get(Calendar.MONTH) + 1) < 10) {
            nMonth = "0" + String.valueOf(c.get(Calendar.MONTH) + 1);
        } else {
            nMonth = String.valueOf(c.get(Calendar.MONTH) + 1);
        }

        if (c.get(Calendar.DAY_OF_MONTH) < 10) {
            nDay = "0" + String.valueOf(c.get(Calendar.DAY_OF_MONTH));
        } else {
            nDay = String.valueOf(c.get(Calendar.DAY_OF_MONTH));
        }

        tDay = nYear + "" + nMonth + "" + nDay;

        if (tDay.equals(gTicketDay)) {
            btn_ticket_cancel.setEnabled(true);
            btn_ticket_mod.setEnabled(false);
        } else {
            btn_ticket_cancel.setEnabled(false);
            btn_ticket_mod.setEnabled(true);
        }

        if (("").equals(ticketData.getApprovalNo())) {
            if (("").equals(ticketData.getCashApprovalNo())) {
                btn_receiptPublish.setVisibility(View.VISIBLE);
            }
        }
		
		/*
		if(Constants.isTicketCancel){
			if("05".equals(CD_TYPE)){
				btn_ticket_cancel.setEnabled(true);
			} else {
				btn_ticket_cancel.setEnabled(false);
			}
		}
		*/
        type11Vo = new ReceiptType11Vo(Util.getYmdhms("yyyy-MM-dd HH:mm:ss")            // 출력일자
                , "정기권"            // 요금제
                , tvDateDuring.getText().toString().replaceAll("일", "")        // 사용기간
                , btnChargeDiscount.getText().toString()                        // 할인종류
                , btn_cal_st.getText().toString()        // 시작날짜
                , btn_cal_ed.getText().toString()        // 종료날짜
                , Util.addComma(finalAmt)    // 납부금액
                , ticketAmt                    // 정기요금
                , String.valueOf(disAmt)        // 할인금액
                , BIZ_NO                        // 사업자번호
                , BIZ_NAME
                , BIZ_TEL
                , "reprint");

        type11Vo.setPioNum(ticketNo);
        type11Vo.setCarNum(carNo);
        type11Vo.setParkName(parkName);
        type11Vo.setEmpName(empName);
        type11Vo.setEmpPhone(empPhone);
        type11Vo.setEmpTel(empTel);
        type11Vo.setEmpBusiness_Tel(empBusiness_TEL);
        type11Vo.setPrint(TICKET_PRINT);
        type11Vo.setCardNo(ticketData.getCardNo());
        type11Vo.setCardCompany(ticketData.getCardCompany());
        type11Vo.setConfirmCardNum(ticketData.getApprovalNo());
        type11Vo.setConfirmCashReceipt(ticketData.getCashApprovalNo());
        type11Vo.setOutPrint(Preferences.getValue(this, Constants.PREFERENCE_OUT_PRINT, ""));
        apiParkTicketPictureCall();

        btn_top_left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        iv_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToTicketPictureView(ticketNo, carNo, Constants.PICTURE_TYPE_R1, parkCode, empCode);
            }
        });

        btn_ticket_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder builder = new AlertDialog.Builder(TicketDetailActivity.this);
                builder.setTitle("정기권").setMessage("정기권을 취소하시겠습니까?");
                builder.setPositiveButton(R.string.btn_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        canceledTicketProcess();
                        dialog.dismiss();
                    }
                }).setNegativeButton(R.string.btn_cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                AlertDialog alert = builder.create();
                alert.setCancelable(false);
                alert.show();

            }
        });

        btn_ticket_print.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PayData payData = new PayData(TicketDetailActivity.this, OUT_PRINT, IN_PRINT, TICKET_PRINT);
                payData.printDefault(PayData.PAY_DIALOG_TYPE_TICKET_SUCCESS, type11Vo, mPrintService);
                payData.ImagePrint(empLogo_image);
                try {
                    payData.printText("\n\n");
                } catch (UnsupportedEncodingException e) {
                    System.out.println("UnsupportedEncodingException 예외 발생");
                }
            }
        });

        btn_ticket_mod.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
				/*
				AlertDialog.Builder builder = new AlertDialog.Builder(TicketDetailActivity.this);
				builder.setTitle("정기권").setMessage("정기권을 수정하시겠습니까?");
				builder.setPositiveButton(R.string.btn_ok, new DialogInterface.OnClickListener()
				{
					@Override
					public void onClick( DialogInterface dialog, int which )
					{
						apiTicketUpdateCall();
						dialog.dismiss();
					}
				}).setNegativeButton(R.string.btn_cancel, new DialogInterface.OnClickListener()
	 			{
	 				@Override
	 				public void onClick(DialogInterface dialog, int which) {
	 					dialog.dismiss();
	 				}
	 			});				
				AlertDialog alert = builder.create();
				alert.setCancelable(false);
			    alert.show();
			    */
                AlertDialog.Builder builder = new AlertDialog.Builder(TicketDetailActivity.this);
                builder.setTitle("정기권").setMessage("정기권을 환불 하시겠습니까?");
                builder.setPositiveButton(R.string.btn_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        apiTicketCancelCall();
                        dialog.dismiss();
                    }
                }).setNegativeButton(R.string.btn_cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                AlertDialog alert = builder.create();
                alert.setCancelable(false);
                alert.show();
            }

        });

        btn_renew_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TicketDetailActivity.this, TicketSignActivity.class);
                intent.putExtra("renew_ticketNo", ticketNo);
                intent.putExtra("renew_date", ticketData.getEndDay());
                intent.putExtra("renew_car_no", ticketData.getCarNo());
                intent.putExtra("renew_discount", btnChargeDiscount.getText().toString());
                intent.putExtra("renew_owner", ticketData.getCarOwner());
                intent.putExtra("renew_tel", ticketData.getOwrnerTel());
                intent.putExtra("renew_car_kind", ticketData.getCarKind());
                intent.putExtra("renew_car_kind", ticketData.getCarKind());
                intent.putExtra("renew_state_cd", ticketData.getStateCd());
                intent.putExtra("renew_address", ticketData.getAddress());
                intent.putExtra("renew_comment", edMemo.getText().toString());
                intent.putExtra("renew_ticket_path", img_ticket_path);
                intent.putExtra("renew_ticket_file", img_ticket_file);
                startActivity(intent);
                img_ticket_path = null;
                img_ticket_file = null;
                finish();
            }
        });

        btn_car_no_1.setOnClickListener(this);
        btn_car_no_2.setOnClickListener(this);
        btn_car_no_3.setOnClickListener(this);
        btn_car_no_4.setOnClickListener(this);
        btn_car_no_manual.setOnClickListener(this);
        btn_receiptPublish.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.btn_car_no_1:
                goToActivityInputCarNumber(Constants.RESULT_INPUT_CAR_NUMBER, Constants.CAR_UPDATE, 1);
                break;
            case R.id.btn_car_no_2:
                goToActivityInputCarNumber(Constants.RESULT_INPUT_CAR_NUMBER, Constants.CAR_UPDATE, 2);
                break;
            case R.id.btn_car_no_3:
                goToActivityInputCarNumber(Constants.RESULT_INPUT_CAR_NUMBER, Constants.CAR_UPDATE, 3);
                break;
            case R.id.btn_car_no_4:
                goToActivityInputCarNumber(Constants.RESULT_INPUT_CAR_NUMBER, Constants.CAR_UPDATE, 4);
                break;
            case R.id.btn_car_no_manual:
                goToActivityInputCarNumber(Constants.RESULT_INPUT_CAR_NUMBER, Constants.CAR_UPDATE, 5);
                break;
            case R.id.btn_receiptPublish:
                type4Vo = new ReceiptType4Vo(Util.getYmdhms("yyyy-MM-dd HH:mm:ss")    // 출력일자
                        , ""    // 주차종류 :
                        , ""        // 주차시간 :
                        , ""        // 주차면 :
                        , ""        // 할인종류 :
                        , ""        // 입차시간 :
                        , ""        // 출차시간 :
                        , ""            // 납부금액 :
                        , ""        // 선납금액 :
                        , ""        // 주차요금 :
                        , ""        // 미납금액 :
                        , ""        // 할인금액 :
                        , BIZ_NO                    // 사업자번호
                        , BIZ_NAME
                        , BIZ_TEL);

                type4Vo.setPioNum(ticketData.getTicketNo());
                type4Vo.setCarNum(carNo);
                type4Vo.setParkName(parkName);
                type4Vo.setEmpName(empName);
                type4Vo.setEmpPhone(empPhone);
                type4Vo.setEmpBusiness_Tel(empBusiness_TEL);
                type4Vo.setEmpTel(empTel);
                type4Vo.setCouponAmt("");
                type4Vo.setConfirmCardNum("");
                type4Vo.setCardNo(ticketData.getCarNo());
                type4Vo.setConfirmCashReceipt(ticketData.getCashApprovalNo());
                type4Vo.setOutPrint(Preferences.getValue(this, Constants.PREFERENCE_OUT_PRINT, ""));
                cashReceiptShowDialog(Util.sToi(tvPayment.getText().toString().replace(",", "").replace("원", "")), type4Vo, "TICKET", "AFTER");
                break;
        }
    }

    @Override
    protected void request(int type, Object data) {

        if (type == PayData.PAY_DIALOG_TYPE_CASH_RECEIPT) {
            PayVo payVo = (PayVo) data;
            type4Vo.setConfirmCashReceipt(payVo.getCashApprovalNo());
            type11Vo.setConfirmCashReceipt(payVo.getCashApprovalNo());
            showProgressDialog();

            Map<String, Object> params = new HashMap<String, Object>();
            params.put(Constants.METHOD, Constants.INTERFACEID_CASH_RECEIPT_UPDATE);
            params.put(Constants.API_FORMAT, Constants.JSON_FORMAT);
            params.put("PIO_NUM", ticketData.getTicketNo());
            params.put("CASH_APP_NO", payVo.getCashApprovalNo());
            params.put("CASH_APP_DATE", payVo.getCashApprovalDate());
            params.put("BILL_TYPE", "C3");


            String url = Constants.API_SERVER;
            Log.d(TAG, " apiCashReceiptCall url >>> " + url + " params ::: " + params.toString());

            AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>();
            cb.url(url).params(params).type(JSONObject.class).weakHandler(this, "cashReceiptCallback").redirect(true).retry(3).fileCache(false).expire(-1);
            AQuery aq = new AQuery(this);
            aq.ajax(cb);
        } else if (type == PayData.PAY_DIALOG_CANCEL_APPROVAL_EXIT) {
            canceledTicket();
        }
    }

    public void cashReceiptCallback(String url, JSONObject json, AjaxStatus status) {
        closeProgressDialog();
        Log.d(TAG, " parkOutCancelCallback  json ====== " + json);
        if (json != null) {
            String KN_RESULT = json.optString("KN_RESULT");
            //successful ajax call, show status code and json content
            if ("1".equals(json.optString("CD_RESULT"))) {
                btn_receiptPublish.setVisibility(View.GONE);
                PayData payData = new PayData(TicketDetailActivity.this, OUT_PRINT, IN_PRINT, TICKET_PRINT);
                payData.printDefault(PayData.PAY_DIALOG_TYPE_TICKET_SUCCESS, type11Vo, mPrintService);
                payData.ImagePrint(empLogo_image);
                try {
                    payData.printText("\n\n");
                } catch (UnsupportedEncodingException e) {
                    System.out.println("UnsupportedEncodingException 예외 발생");
                }
                MsgUtil.ToastMessage(TicketDetailActivity.this, KN_RESULT, Toast.LENGTH_LONG);
                //finish();
            } else {
                //ajax error, show error code
                MsgUtil.ToastMessage(TicketDetailActivity.this, KN_RESULT, Toast.LENGTH_LONG);
            }
        } else {
            MsgUtil.ToastMessage(TicketDetailActivity.this, Constants.DATA_FAIL);
        }


    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        // TODO Auto-generated method stub
        super.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case Constants.RESULT_INPUT_CAR_NUMBER:
                if (resultCode != RESULT_OK) {
                    return;
                }

                if (data != null) { // 번호 재입력 시
                    String carInputMode = Util.isNVL(data.getStringExtra(Constants.CAR_INPUT_MODE));
                    if (Constants.CAR_NORMAL.equals(carInputMode)) {
                        carNo = Constants.KEY_CARNO_1 + Constants.KEY_CARNO_2 + Constants.KEY_CARNO_3 + Constants.KEY_CARNO_4;

                        setCarnoView(Constants.KEY_CARNO_1, Constants.KEY_CARNO_2, Constants.KEY_CARNO_3, Constants.KEY_CARNO_4, "",
                                btn_car_no_1, btn_car_no_2, btn_car_no_3, btn_car_no_4, btn_car_no_manual, Constants.CAR_NORMAL);

                    } else if (Constants.CAR_MANUAL.equals(carInputMode)) {
                        carNo = Constants.KEY_CARNO_MANUAL;

                        setCarnoView("", "", "", "", Constants.KEY_CARNO_MANUAL,
                                btn_car_no_1, btn_car_no_2, btn_car_no_3, btn_car_no_4, btn_car_no_manual, Constants.CAR_MANUAL);

                    }

                }
                break;
            case Constants.RESULT_CAR_PICTURE:
                Log.d(TAG, " RESULT_CAR_PICTURE ::: " + Constants.RESULT_CAR_PICTURE);
                apiParkTicketPictureCall();
                break;
            case PayData.PAY_DIALOG_CANCEL_APPROVAL_EXIT:
                String transactionNo = Preferences.getValue(this, Constants.PREFERENCE_MCPAY_TRANSACTION_NO, "");
                String approvalNo = Preferences.getValue(this, Constants.PREFERENCE_MCPAY_APPROVAL_NO, "");
                String approvalDate = Preferences.getValue(this, Constants.PREFERENCE_MCPAY_APPROVAL_DATE, "");
                String cardNo = Preferences.getValue(this, Constants.PREFERENCE_MCPAY_CARD_NO, "");
                String cardCompany = Preferences.getValue(this, Constants.PREFERENCE_MCPAY_CARD_COMPANY, "");

                if (transactionNo == "" || cardNo == "" || approvalNo == "") {
                    Log.d("111", "Card error");
                    return;
                }

                canceledTicket();
                break;
            case PayData.PAY_DIALOG_TYPE_CASH_RECEIPT:
                try {
                    mPrintService.stop();
                    if (mPrintService.getState() == 0) {

                        BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(mBluetoothAddress);
                        mPrintService.connect(device, true);
                    }
                } catch (IllegalArgumentException e) {
                    System.out.println("IllegalArgumentException 예외 발생");
                } catch (NullPointerException e) {
                    System.out.println("NullPointerException 예외 발생");
                }

                approvalNo = Preferences.getValue(this, Constants.PREFERENCE_MCPAY_APPROVAL_NO, "");
                approvalDate = Preferences.getValue(this, Constants.PREFERENCE_MCPAY_APPROVAL_DATE, "");
                String amt_tot = Preferences.getValue(this, Constants.PREFERENCE_MCPAY_TOTAL_AMT, "");


                Log.d("111", "amt_totamt_totamt_tot : " + amt_tot);

                PayVo payVo = new PayVo();  //현금영수증참조
                payVo.setCashApprovalNo(approvalNo);
                payVo.setCashApprovalDate(approvalDate);
                tvCashBill.setTag(payVo);
                tvCashBill.setText("발행");
                InitPreferences();
                //cashReceiptRun(Util.sToi(amt_tot), g_vo);

                ExitPaymentData exitVo = new ExitPaymentData();
                exitVo.setCashApprovalNo(approvalNo);
                exitVo.setCashApprovalDate(approvalDate);
                request(PayData.PAY_DIALOG_TYPE_CASH_RECEIPT, exitVo);
                if (cashReceiptDialog != null) {
                    cashReceiptDialog.dismiss();
                    cashReceiptDialog = null;
                }
                g_vo = null;
                tvCashBill = null;
                break;


        } // end switch
    }

    //정기권 취소 판단..//
    private void canceledTicketProcess() {

        //카드결제라면...
        if (!("").equals(ticketData.getApprovalNo().toString()) || !("").equals(ticketData.getCardNo().toString())) {

            type4Vo = new ReceiptType4Vo(Util.getYmdhms("yyyy-MM-dd HH:mm:ss")    // 출력일자
                    , ""    // 주차종류 :
                    , ""        // 주차시간 :
                    , ""        // 주차면 :
                    , ""        // 할인종류 :
                    , ""        // 입차시간 :
                    , ""        // 출차시간 :
                    , ""            // 납부금액 :
                    , ""        // 선납금액 :
                    , ""        // 주차요금 :
                    , ""        // 미납금액 :
                    , ""        // 할인금액 :
                    , BIZ_NO                    // 사업자번호
                    , BIZ_NAME
                    , BIZ_TEL);

            type4Vo.setPioNum(ticketData.getTicketNo());
            type4Vo.setCarNum(carNo);
            type4Vo.setParkName(parkName);
            type4Vo.setEmpName(empName);
            type4Vo.setEmpPhone(empPhone);
            type4Vo.setEmpBusiness_Tel(empBusiness_TEL);
            type4Vo.setEmpTel(empTel);
            type4Vo.setCouponAmt("");
            type4Vo.setCardNo(ticketData.getCardNo());
            type4Vo.setConfirmCardNum(ticketData.getApprovalNo());
            type4Vo.setConfirmCashReceipt(ticketData.getCashApprovalNo());
            type4Vo.setOutPrint(Preferences.getValue(this, Constants.PREFERENCE_OUT_PRINT, ""));
            // TODO 결제 취소..
            goToPayCancelActivity(PayData.PAY_DIALOG_CANCEL_APPROVAL_EXIT, ticketData.getPayAmt(), ticketData.getApprovalNo(), ticketData.getApprovalDate(), type4Vo);
        } else {
            canceledTicket();
        }
    }

    //취소
    private void canceledTicket() {
        //MsgUtil.ToastMessage(this, "준비중입니다.", Toast.LENGTH_LONG);

        showProgressDialog();
        String url = Constants.API_SERVER + "?" + Constants.METHOD + "=" + Constants.INTERFACEID_PARK_TICKET_CANCEL
                + "&TICKET_NO=" + ticketNo
                + "&CD_TYPE=" + CD_TYPE
                + "&CD_PARK=" + parkCode
                + "&EMP_CODE=" + empCode
                + "&CAR_NO=" + carNo
                + "&CAR_OWNER=" + edCarOwner.getText().toString()
                + "&OWRNER_TEL=" + edPhoneNumber.getText().toString()
                + "&KN_COMMENT=" + edMemo.getText().toString()
                + "&" + Constants.API_FORMAT + "=" + Constants.JSON_FORMAT;
        Log.d(TAG, " apiTicketCancelCall url >>> " + url);

        AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>();
        cb.url(url).type(JSONObject.class).weakHandler(this, "ticketCancelCallback").redirect(true).retry(3).fileCache(false).expire(-1);
        AQuery aq = new AQuery(this);
        aq.ajax(cb);


    }

    //환불
    private void apiTicketCancelCall() {
        showProgressDialog();
        String url = Constants.API_SERVER + "?" + Constants.METHOD + "=parkTicketReturnResult"
                + "&TICKET_NO=" + ticketNo
                + "&CD_TYPE=" + CD_TYPE
                + "&CD_PARK=" + parkCode
                + "&EMP_CODE=" + empCode
                + "&CAR_NO=" + carNo
                + "&CAR_OWNER=" + edCarOwner.getText().toString()
                + "&OWRNER_TEL=" + edPhoneNumber.getText().toString()
                + "&KN_COMMENT=" + edMemo.getText().toString()
                + "&" + Constants.API_FORMAT + "=" + Constants.JSON_FORMAT;
        Log.d(TAG, " apiTicketCancelCall url >>> " + url);

        AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>();
        cb.url(url).type(JSONObject.class).weakHandler(this, "ticketCancelCallback").redirect(true).retry(3).fileCache(false).expire(-1);
        AQuery aq = new AQuery(this);
        aq.ajax(cb);
    }

    public void ticketCancelCallback(String url, JSONObject json, AjaxStatus status) {
        Log.d(TAG, " ticketCancelCallback  json ====== " + json);

        closeProgressDialog();

        // successful ajax call
        if (json != null) {
            String KN_RESULT = json.optString("KN_RESULT");
            if ("1".equals(json.optString("CD_RESULT"))) {
                Toast.makeText(this, KN_RESULT, Toast.LENGTH_LONG).show();
                finish();
            } else {
                // TODO 에러 팝업 처리
                Toast.makeText(this, KN_RESULT, Toast.LENGTH_LONG).show();
            }
        } else {
            Toast.makeText(this, Constants.DATA_FAIL, Toast.LENGTH_LONG).show();
        }
    }

    private void apiTicketUpdateCall() {
        if (Util.isEmpty(carNo)) {
            MsgUtil.ToastMessage(this, "차량번호가 없습니다.");
            return;
        }

        showProgressDialog();
        String url = Constants.API_SERVER + "?" + Constants.METHOD + "=" + Constants.INTERFACEID_PARK_TICKET_UPDATE
                + "&TICKET_NO=" + ticketNo
                + "&CD_PARK=" + parkCode
                + "&EMP_CODE=" + empCode
                + "&CAR_NO=" + carNo
                + "&CAR_OWNER=" + edCarOwner.getText().toString()
                + "&OWRNER_TEL=" + edPhoneNumber.getText().toString()
                + "&KN_COMMENT=" + edMemo.getText().toString()
                + "&" + Constants.API_FORMAT + "=" + Constants.JSON_FORMAT;
        Log.d(TAG, " apiTicketUpdateCall url >>> " + url);

        AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>();
        cb.url(url).type(JSONObject.class).weakHandler(this, "ticketUpdateCallback").redirect(true).retry(3).fileCache(false).expire(-1);
        AQuery aq = new AQuery(this);
        aq.ajax(cb);
    }

    public void ticketUpdateCallback(String url, JSONObject json, AjaxStatus status) {
        Log.d(TAG, " ticketUpdateCallback  json ====== " + json);

        closeProgressDialog();

        // successful ajax call
        if (json != null) {
            String KN_RESULT = json.optString("KN_RESULT");
            if ("1".equals(json.optString("CD_RESULT"))) {
                Toast.makeText(this, KN_RESULT, Toast.LENGTH_LONG).show();
            } else {
                // TODO 에러 팝업 처리
                Toast.makeText(this, KN_RESULT, Toast.LENGTH_LONG).show();
            }
        } else {
            Toast.makeText(this, Constants.DATA_FAIL, Toast.LENGTH_LONG).show();
        }
    }

    private void apiParkTicketPictureCall() {
        String url = Constants.API_SERVER + "?" + Constants.METHOD + "=" + Constants.INTERFACEID_PARK_TICKET_PICTURE;
        String param = "&TICKET_NO=" + ticketNo
                + "&CD_PARK=" + parkCode
                + "&" + Constants.API_FORMAT + "=" + Constants.JSON_FORMAT;
        Log.d(TAG, " apiParkTicketPictureCall url >>> " + url + param);

        AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>();
        cb.url(url + param).type(JSONObject.class).weakHandler(this, "parkTicketPictureCallback").redirect(true).retry(3).fileCache(false).expire(-1);
        AQuery aq = new AQuery(this);
        aq.ajax(cb);

    }

    public void parkTicketPictureCallback(String url, JSONObject json, AjaxStatus status) {

        Log.d(TAG, " parkTicketPictureCallback  json ====== " + json);

        JSONArray jsonArr = null;

        // successful ajax call
        if (json != null) {
            if ("1".equals(json.optString("CD_RESULT"))) {
                if (pictureList != null) {
                    pictureList.clear();
                } else {
                    pictureList = new ArrayList<PictureInfo>();
                }

                jsonArr = json.optJSONArray("PICTURE_LIST");
                String pictureURL = "";
                int Len = jsonArr.length();
                for (int i = 0; i < Len; i++) {
                    try {
                        JSONObject res = jsonArr.getJSONObject(i);
                        String filePath = res.optString("FILE_PATH");
                        String fileName = res.optString("FILE_NAME");
                        if (i == 0) {
                            pictureURL = Constants.SERVER_HOST + filePath + fileName;
                            Log.d(TAG, " pictureURL >>> " + pictureURL);
                        }
                        PictureInfo info = new PictureInfo();
                        img_ticket_path = filePath;
                        img_ticket_file = fileName;
                        info.setPath(filePath + fileName);
                        pictureList.add(info);
                        break;
                    } catch (JSONException e) {
                        System.out.println("JSONException 예외 발생");
                    }
                }
                if (!Util.isEmpty(pictureURL)) {
                    ImageLoader.setImageUrlView(this, pictureURL, iv_image);
                }

            } else {
                iv_image.setImageDrawable(null);
            }
        } else {
            iv_image.setImageDrawable(null);
            MsgUtil.ToastMessage(this, Constants.DATA_FAIL, Toast.LENGTH_LONG);
        }

    }

}
