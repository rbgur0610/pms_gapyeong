package com.pms.gapyeong.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.pms.gapyeong.R;
import com.pms.gapyeong.common.Constants;
import com.pms.gapyeong.common.CustomKeyboard;
import com.pms.gapyeong.common.Util;
import com.pms.gapyeong.common.CustomKeyboard.KeyboardDoneListener;

public class InputCarNumerActivity extends BaseActivity {

    static final int TYPE_NUMBER = CustomKeyboard.KEYBOARD_TYPE_FRONT_NUMBER;
    static final int TYPE_HANGUL = CustomKeyboard.KEYBOARD_TYPE_HANGUL;
    static final int TYPE_SECTOR = CustomKeyboard.KEYBOARD_TYPE_SECTOR;
    static final int TYPE_NONE = 0x1114;
    static final int TYPE_DONE = 0x1115;
    static final int TYPE_ONLYNUMBER = 0x1116;

    private Button btn_top_left;
    private Button btn_top_camera;
    private EditText et_keyboard1;
    private EditText et_keyboard2;
    private EditText et_keyboard3;
    private EditText et_keyboard4;
    private FrameLayout frame_keyboard;
    private CustomKeyboard cKeyboard;

    // 차량 입력 모드
    private String carInputMode;
    // 차량번호 선택 값
    private String index;
    private String CD_AREA = "";

    private KeyboardDoneListener listener = new KeyboardDoneListener() {
        @Override
        public void onDone(String str1, String str2, String str3, String str4, String stretc) {

            Constants.KEY_CARNO_1 = str1;
            Constants.KEY_CARNO_2 = str2;
            Constants.KEY_CARNO_3 = str3;
            Constants.KEY_CARNO_4 = str4;
            Constants.KEY_CARNO_MANUAL = stretc;

            Intent intent = new Intent();
            if (Constants.CAR_UPDATE.equals(carInputMode) || Constants.CAR_TICKET.equals(carInputMode)) {
                // 일반차량번호입력은 반드시 수동차량번호는 널값으로 셋팅...
                if (Util.isEmpty(Constants.KEY_CARNO_MANUAL)) {
                    intent.putExtra(Constants.CAR_INPUT_MODE, Constants.CAR_NORMAL);
                } else {
                    intent.putExtra(Constants.CAR_INPUT_MODE, Constants.CAR_MANUAL);
                }
                setResult(RESULT_OK, intent);
            } else {
                //	현황판에서 저장되어짐
                //	Constants.BOARD_NUMBER
                intent.setClass(InputCarNumerActivity.this, EntranceTimeActivity.class);
                // 일반차량번호입력은 반드시 수동차량번호 널값으로 셋팅...
                if (Util.isEmpty(Constants.KEY_CARNO_MANUAL)) {
                    intent.putExtra(Constants.CAR_INPUT_MODE, Constants.CAR_NORMAL);
                } else {
                    intent.putExtra(Constants.CAR_INPUT_MODE, Constants.CAR_MANUAL);
                }
                startActivity(intent);
            }
            finish();
        }

        @Override
        public void onDelete() {
            // 키보드 초기화
            cKeyboard.setKeyboardType(CustomKeyboard.KEYBOARD_TYPE_FRONT_NUMBER);
        }

        @Override
        public void onEtc() {
            cKeyboard.setKeyboardType(TYPE_HANGUL);
        }
    };
    private Button btn_three_num;

    private void OpenCamera() {
        Intent mIntent = new Intent(InputCarNumerActivity.this, AutoCarNumberActivity.class);
        mIntent.putExtra("CD_AREA", "" + CD_AREA);
        startActivity(mIntent);

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.input_carnumber_layout);

        // 호출한 Activity 저장, 자동차 번호를 넘겨주기 위해
        Intent intent = getIntent();
        if (intent != null) {
            carInputMode = Util.isNVL(intent.getStringExtra(Constants.CAR_INPUT_MODE));
            index = Util.isNVL(intent.getStringExtra("index"), "2");
            CD_AREA = intent.getStringExtra("CD_AREA");
        }
    }


    @Override
    protected void initLayoutSetting() {
        // TODO Auto-generated method stub
        super.initLayoutSetting();
        TextView tv_top_title = (TextView) findViewById(R.id.tv_top_title);
        tv_top_title.setText("차량번호입력");

        btn_top_left = (Button) findViewById(R.id.btn_top_left);
        btn_top_left.setVisibility(View.VISIBLE);
        btn_top_left.setOnClickListener(this);

        btn_three_num = (Button) findViewById(R.id.btn_three_num);
        btn_three_num.setVisibility(View.VISIBLE);
        btn_three_num.setOnClickListener(this);


        if ("INCAR".equals(carInputMode)) {
            btn_top_camera = (Button) findViewById(R.id.btn_top_autorecog);
            btn_top_camera.setVisibility(View.VISIBLE);
            btn_top_camera.setOnClickListener(this);
            // 2016-10-22 앱-번호인식 버튼 활성화요청(메인화면,입출차,면수선택후 우측상단의 번호인식 활성화) 누락수정 btn_top_camera.setEnabled(false);
        }


        et_keyboard1 = (EditText) findViewById(R.id.et_keyboard1);
        et_keyboard2 = (EditText) findViewById(R.id.et_keyboard2);
        et_keyboard3 = (EditText) findViewById(R.id.et_keyboard3);
        et_keyboard4 = (EditText) findViewById(R.id.et_keyboard4);
        frame_keyboard = (FrameLayout) findViewById(R.id.frame_keyboard);

        if (Constants.CAR_UPDATE.equals(carInputMode) || Constants.CAR_TICKET.equals(carInputMode)) {
            et_keyboard1.setText(Constants.KEY_CARNO_1);
            et_keyboard2.setText(Constants.KEY_CARNO_2);
            et_keyboard3.setText(Constants.KEY_CARNO_3);
            et_keyboard4.setText(Constants.KEY_CARNO_4);
        }

        cKeyboard = new CustomKeyboard(this, frame_keyboard, et_keyboard1, et_keyboard2, et_keyboard3, et_keyboard4, listener, index);
    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        super.onClick(v);

        switch (v.getId()) {
            case R.id.btn_top_left:
                finish();
                break;
            case R.id.btn_top_autorecog:
                OpenCamera();
                break;

            case R.id.btn_three_num:
                v.setSelected(!v.isSelected());

                cKeyboard.setInputThree(v.isSelected());
                if(!v.isSelected()){
                    setToast("3자리 입력이 해제 되었습니다. ");
                }else{
                    setToast("3자리 입력이 활성화 되었습니다. ");
                }
                break;
        }
    }

}
