package com.pms.gapyeong.activity;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.TextView.OnEditorActionListener;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.pms.gapyeong.R;
import com.pms.gapyeong.adapter.TicketManagerAdapter;
import com.pms.gapyeong.common.CalendarInfo;
import com.pms.gapyeong.common.Constants;
import com.pms.gapyeong.common.MsgUtil;
import com.pms.gapyeong.common.Preferences;
import com.pms.gapyeong.common.Util;
import com.pms.gapyeong.database.CodeHelper;
import com.pms.gapyeong.pay.PayData;
import com.pms.gapyeong.vo.ParkTicketInfoItem;
import com.pms.gapyeong.vo.ReceiptType8Vo;

public class TicketManageActivity extends BaseActivity {
	private static boolean clickCalBtn = true;
	private Button btn_ticket;
	private Button btn_cal_st;
	private Button btn_cal_ed;
	private int mYear;
	private int mMonth;
	private int mDay;
	private calendarDlg caledarDialog;
	
	private EditText et_car_number;
	private Button btn_car_search;
	
	private TextView tvHistoryNum;
	private Button btnPrintList;
	private ListView lvTicketManager;
	private Button btnSearchType;
	private Button btn_top_left;
	private ArrayList<ParkTicketInfoItem> ticketList;
	TicketManagerAdapter ticketAdapter = null;
	private TextView tvTable00;
	private TextView tvTable01;
	private TextView tvTable02;
	private TextView tvTable03;
	private TextView tvTable04;
	
	private String mDate = "";
	private InputMethodManager ipm;
	
	private JSONArray jsonArr = null;;
	
	private int currentPage   = 1;
	private int totalLen 	  = 0; 
	private boolean lastitemVisibleFlag = false;        //화면에 리스트의 마지막 아이템이 보여지는지 체크	
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);

		setContentView(R.layout.ticket_manage_layout);
		
		String date = Util.getYmdhms("yyyy-MM-dd");
		mYear = Integer.parseInt(date.split("-")[0]);
		mMonth = Integer.parseInt(date.split("-")[1]);
		mDay = Integer.parseInt(date.split("-")[2]);
		
		cch = CodeHelper.getInstance(this);
		
		ticketList = new ArrayList<ParkTicketInfoItem>();
	}

	@Override
	protected void initLayoutSetting() {
		
		super.initLayoutSetting();
		TextView tv_top_title = (TextView) findViewById(R.id.tv_top_title);
		tv_top_title.setText("정기권관리");
		btn_top_left = (Button) findViewById(R.id.btn_top_left);
		btn_top_left.setVisibility(View.VISIBLE);
		btn_top_left.setOnClickListener(this);
		
		btnSearchType= (Button)findViewById(R.id.btnSearchType);
		btnPrintList= (Button)findViewById(R.id.btnPrintList);
		btn_cal_st= (Button)findViewById(R.id.btn_cal_st);
		btn_cal_ed= (Button)findViewById(R.id.btn_cal_ed);
		btn_ticket = (Button)findViewById(R.id.btn_ticket);
		tvHistoryNum= (TextView)findViewById(R.id.tvHistoryNum);
		
		et_car_number  = (EditText)findViewById(R.id.et_car_number);
		btn_car_search = (Button)findViewById(R.id.btn_car_search);
	
		//리스트 상단 테이블 이름변경 
		tvTable00= (TextView)findViewById(R.id.tvTable00);
		tvTable01= (TextView)findViewById(R.id.tvTable01);
		tvTable02= (TextView)findViewById(R.id.tvTable02);
		tvTable03= (TextView)findViewById(R.id.tvTable03);
		tvTable04= (TextView)findViewById(R.id.tvTable04);
		
		lvTicketManager= (ListView)findViewById(R.id.lvTicketManager);
		mDate = mYear + "/" + Util.addZero(mMonth) + "/" + Util.addZero(mDay);
		
		btn_cal_st.setText(mDate);
		btn_cal_ed.setText(mDate);
	
		//정기권종류RP/사진종류DR
		setCommonItem("RP", Constants.COMMON_TYPE_TICKET_TYPE, btnSearchType, 0);
		
		btnPrintList.setOnClickListener(this);
		
		btnSearchType.setOnClickListener(this);
		btn_ticket.setOnClickListener(this);
		btn_cal_st.setOnClickListener(this);
		btn_cal_ed.setOnClickListener(this);

		ipm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
		
		et_car_number.setOnEditorActionListener(new OnEditorActionListener() { 
			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				// TODO Auto-generated method stub
				switch (actionId) {
					case EditorInfo.IME_ACTION_SEARCH:
						requestTicketList();
					break;
					default:
					return false;
				}				
				return false;
			}
		});	
		
		btn_car_search.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				requestTicketList();
			}
		});		
		
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		requestTicketList(); 
	}
	
	private void goDetailActivity(ParkTicketInfoItem vo){
		startActivity(new Intent(this,TicketDetailActivity.class).putExtra("TicketData",vo));
	}
	
	@Override
	public void getSelectListDialogData(int tag, String data) {
		// TODO Auto-generated method stub
		super.getSelectListDialogData(tag, data);
		//TODO 검색종류에 따른 타이틀 시나리오  마감 시작날짜/종료날짜 선택하여 조회하도록 추가 
//		tvTable00.setText(text);
//		tvTable01.setText(text);
//		tvTable02.setText(text);
//		tvTable03.setText(text);
//		tvTable04.setText(text);
		
		requestTicketList();
		
	}
	
	@Override
	public void onClick(View v) {
	  // TODO Auto-generated method stub
	  super.onClick(v);
	  switch (v.getId()) {
		
		case R.id.btn_top_left:
			  finish();
			break;
		
		case R.id.btnSearchType:
			  setCommonDialog("검색종류","RP", Constants.COMMON_TYPE_TICKET_TYPE, btnSearchType);
			break;
			
		case R.id.btnPrintList:
			  ReceiptType8Vo type8Vo = new ReceiptType8Vo("정기권현황", today, BIZ_NAME, BIZ_TEL, ticketList);
			  
			  type8Vo.setParkName(parkName);
			  type8Vo.setEmpName(empName);
			  type8Vo.setEmpPhone(empPhone);
			  type8Vo.setEmpTel(empTel);
			  type8Vo.setEmpBusiness_Tel(empBusiness_TEL);
			  type8Vo.setOutPrint(Preferences.getValue(this,Constants.PREFERENCE_OUT_PRINT,""));
			  PayData payData = new PayData(this, OUT_PRINT, IN_PRINT, TICKET_PRINT);
			  payData.printDefault(PayData.PAY_TYPE_TICKET_MANAGER, type8Vo, mPrintService);			   
			break;
		case R.id.btn_ticket:
			  Intent intent = new Intent(this,TicketSignActivity.class);
			  startActivity(intent);			
			break;
			
		case R.id.btn_cal_st:
			  clickCalBtn = true;
			  caledarDialog = new calendarDlg(this);
			  caledarDialog.show();
			break;

		case R.id.btn_cal_ed:
			  clickCalBtn = false;
			  caledarDialog = new calendarDlg(this);
			  caledarDialog.show();
			break;
	   }

	}
	
	private void setLayout(){
		
		// 초기화 
		currentPage = 1;
		if(ticketList != null){
			ticketList.clear();
		}
		
		ticketAdapter = new TicketManagerAdapter(this, ticketList);
		
		lvTicketManager.setAdapter(ticketAdapter);
		lvTicketManager.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int pos, long arg3) {
				// TODO Auto-generated method stub
				if(!ticketList.get(pos).getCdPark().equals(""))
				  goDetailActivity(ticketList.get(pos));
				else
				  setToast("잘못된 항목입니다.");
			}
		});
		lvTicketManager.setOnScrollListener(new AbsListView.OnScrollListener() {
		    @Override
		    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
		    	//현재 화면에 보이는 첫번째 리스트 아이템의 번호(firstVisibleItem) + 현재 화면에 보이는 리스트 아이템의 갯수(visibleItemCount)가 리스트 전체의 갯수(totalItemCount) -1 보다 크거나 같을때
		    	lastitemVisibleFlag = (totalItemCount > 0) && (firstVisibleItem + visibleItemCount >= totalItemCount); 
		    }   
		    
		    @Override
		    public void onScrollStateChanged(AbsListView view, int scrollState) {
	        	 //OnScrollListener.SCROLL_STATE_IDLE은 스크롤이 이동하다가 멈추었을때 발생되는 스크롤 상태입니다. 
	        	 //즉 스크롤이 바닦에 닿아 멈춘 상태에 처리를 하겠다는 뜻
		         if(scrollState == OnScrollListener.SCROLL_STATE_IDLE && lastitemVisibleFlag) {
		             //TODO 화면이 바닦에 닿을때 처리
		        	 if(totalLen > ticketAdapter.getCount()){ 
		        		 currentPage++;
		        		 setData();
		        	 }
			     } 		    	
		    }
		});			
		
	}
	
	private void setData(){
		
	   if(jsonArr == null)
	   {
			setToast("검색 결과가 없습니다.");
			return;
	   }
		
	   int page = currentPage;
	   int start_index = (page - 1) * Constants.LIST_SIZE;
	   int last_index  = (page * Constants.LIST_SIZE);
	   
	   if(last_index > totalLen){
		   last_index = totalLen;   
	   }
	   
//		   Log.d(TAG, " start_index >>> " + start_index );
//		   Log.d(TAG, " last_index >>> " + last_index );
	   for(int i=start_index; i<last_index; i++){
		 try {
			 JSONObject res = jsonArr.getJSONObject(i);
			 
			 ParkTicketInfoItem info = new ParkTicketInfoItem(
													  res.optString("CD_PARK")
													, res.optString("CD_TYPE")
													, res.optString("CAR_NO")
													, res.optString("TICKET_NO")
													, res.optString("START_DAY")
													, res.optString("END_DAY")
													, res.optString("CAR_KIND")
													, res.optString("CAR_OWNER")
													, res.optString("OWRNER_TEL")
													, res.optString("TICKET_DAY")
													, res.optString("TICKET_AMT")
													, res.optString("PAY_AMT")
													, res.optString("YET_AMT")
													, res.optString("DIS_AMT")
													, res.optString("CARD_NO")
													, res.optString("CARD_COMPANY")
													, res.optString("APPROVAL_NO")
													, res.optString("APPROVAL_DATE")
													, res.optString("DIS_CD")
													, res.optString("STATE_CD")
													, res.optString("ADDRESS")
													, res.optString("KN_COMMENT"));
			  info.setCashApprovalNo(res.optString("CASH_APPROVAL_NO"));
			  info.setCashApprovalDate(res.optString("CASH_APPROVAL_DATE"));
			  ticketList.add(info);			
			
		   } catch (JSONException e) {
			  // TODO Auto-generated catch block
			 if(Constants.DEBUG_PRINT_LOG){
				 e.printStackTrace();
			 }else{
				 System.out.println("예외 발생");
			 }
		   }
	   }  // end for 		
	   
	   ticketAdapter.notifyDataSetChanged();
	   closeProgressDialog();
	}	
	
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		switch (requestCode) {
		   default :
			 break;
		}
	}
	
	private void requestTicketList() {
		// 키보드 숨기기
		if(ipm != null && et_car_number != null){
			ipm.hideSoftInputFromWindow(et_car_number.getWindowToken(), 0);
		}
		
		// 초기화
		if (ticketAdapter != null && ticketList != null) {
			ticketList.clear();
			ticketAdapter.notifyDataSetChanged();
		}
		tvHistoryNum.setText("0건");		
		
		String startDate = btn_cal_st.getText().toString().replaceAll("/", "");
		String endDate = btn_cal_ed.getText().toString().replaceAll("/", "");
		
		String cd_type = cch.findCodeByKnCode("RP", btnSearchType.getText().toString());
		
		apiParkTicketListCall(et_car_number.getText().toString(), parkCode, startDate, endDate, cd_type);
	}
	
	private void apiParkTicketListCall(String carNo, String cdPark, String startDay, String EndDay, String cd_type){
		showProgressDialog();
		
		if(Util.isEmpty(carNo)){
			carNo = Util.getEncodeStr("%");
		}
		
		String url = Constants.API_SERVER + "?" + Constants.METHOD +"="+ Constants.INTERFACEID_PARK_TICKET_INFO;
		String param = "&TICKET_NO="+Util.getEncodeStr("%")
					 + "&CAR_NO="+carNo
					 + "&CD_PARK="+cdPark
					 + "&START_DAY="+startDay
					 + "&END_DAY="+EndDay
					 + "&CD_TYPE="+cd_type
					 + "&"+Constants.API_FORMAT+"="+Constants.JSON_FORMAT;
		Log.d(TAG, " apiParkTicketListCall url >>> " + url + param);
		
		AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>();        
		cb.url(url+param).type(JSONObject.class).weakHandler(this, "parkTicketListCallback").redirect(true).retry(3).fileCache(false).expire(-1);
		AQuery aq = new AQuery(this);
		aq.ajax(cb); 		
	}		
	
	public void parkTicketListCallback(String url, JSONObject json, AjaxStatus status){
		
		closeProgressDialog();
		
		Log.d(TAG, " parkTicketListCallback  json ====== " +json);
		
		// successful ajax call          
	    if(json != null){   
	       String KN_RESULT = json.optString("KN_RESULT");
	       if("1".equals(json.optString("CD_RESULT"))){
			   jsonArr  = json.optJSONArray("RESULT");
			   totalLen = jsonArr.length();
			   tvHistoryNum.setText(totalLen+"건");
			   showProgressDialog();
			   setLayout();
			   setData();
	       } else {
	    	   tvHistoryNum.setText("0건");
	    	   MsgUtil.ToastMessage(this, KN_RESULT, Toast.LENGTH_LONG);
	       }
	    } else {
	    	MsgUtil.ToastMessage(this, Constants.DATA_FAIL, Toast.LENGTH_LONG);
	    }
		
	}				
	
	//exe 
	/*	
		 final Calendar c = Calendar.getInstance();
			c.add(Calendar.DAY_OF_MONTH, 1);
			mYear = c.get(Calendar.YEAR);
			mMonth = c.get(Calendar.MONTH);
			mDay = c.get(Calendar.DAY_OF_MONTH)-1;
 		
 		CalendarDlg dlg = new CalendarDlg( this ) ; 
	    dlg.show( ) ;
        */
	
	public class calendarDlg extends Dialog
	{

		public class myCalendar extends CalendarInfo
		{
			public myCalendar(Context context, LinearLayout layout) 
			{
				super(context, layout);
			}

			@Override
			public void myClickEvent(int yyyy, int MM, int dd) 
			{
				mYear = yyyy;
				mMonth = MM+1;
				mDay = dd;
				mDate = mYear + "/" + Util.addZero(mMonth) + "/"
				+ Util.addZero(mDay);
				
				if(clickCalBtn){
					btn_cal_st.setText(mDate);
				}else{
					btn_cal_ed.setText(mDate);
				}
				
				requestTicketList();
				
				calendarDlg.this.cancel() ;
				super.myClickEvent(yyyy, MM, dd);
			}
		}

		TextView tvs[] ;
		Button btns[] ;

		public calendarDlg( Context context ) 
		{

			super(context,android.R.style.Theme_Holo_Light_Dialog_NoActionBar);
			setContentView(R.layout.calendar_layout);

			LinearLayout lv = (LinearLayout)findViewById( R.id.calendar_ilayout ) ;

			tvs = new TextView[3] ;
			tvs[0] = (TextView)findViewById( R.id.tv1 ) ;
			tvs[1] = (TextView)findViewById( R.id.tv2 ) ;
			tvs[2] = null ;

			btns = new Button[4] ;
			btns[0] = null ;
			btns[1] = null ;
			btns[2] = (Button)findViewById( R.id.Button03 ) ;
			btns[3] = (Button)findViewById( R.id.Button04 ) ;

			myCalendar cal = new myCalendar( context, lv ) ;

			cal.setControl( btns ) ;
			cal.setViewTarget( tvs ) ;
			
			if(clickCalBtn){
				cal.initCalendar( btn_cal_st.getText().toString() ) ;
			} else {
				cal.initCalendar( btn_cal_ed.getText().toString() ) ;
			}			
		}

	}
}
