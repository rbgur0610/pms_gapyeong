package com.pms.gapyeong.activity;

import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import android.util.Log;
import android.app.Dialog;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.pms.gapyeong.common.Preferences;
import com.pms.gapyeong.R;
import com.pms.gapyeong.common.CalendarInfo;
import com.pms.gapyeong.common.Constants;
import com.pms.gapyeong.common.ImageLoader;
import com.pms.gapyeong.common.MsgUtil;
import com.pms.gapyeong.common.Util;
import com.pms.gapyeong.database.CodeHelper;
import com.pms.gapyeong.pay.PayData;
import com.pms.gapyeong.pay.PayVo;
import com.pms.gapyeong.vo.PictureInfo;
import com.pms.gapyeong.vo.ReceiptType11Vo;

public class TicketSignActivity extends BaseActivity {

	private ImageView iv_image;
	
	Calendar calendar = Calendar.getInstance();
	
	private Button btn_cal_st;
	private Button btn_cal_ed;
	
	private Button btn_car_no_1;
	private Button btn_car_no_2;
	private Button btn_car_no_3;
	private Button btn_car_no_4;
	private Button btn_car_no_manual;		
	
	private EditText edPhoneNumber;
	private Button btnChargeType;
	private Button btnChargeDiscount;
	private TextView tvTicketAmt;
	private TextView tvUnPayment;
	private TextView tvPayment;
	private View btn_top_left;
	private Button btnTicketSubmit;
	private TextView tvDateDuring;
	private EditText edCarOwner;
	private EditText edMemo;
	
	private ReceiptType11Vo type11Vo;
	
	private calendarDlg caledarDialog;
	private int disAmt   = 0;
	private int finalAmt = 0;
	
	private String ticketNo = "";
	private String carNo    = "";
	
	private String renew_car_kind = "";
	private String renew_state_cd = "";
	private String renew_address  = "";
	
	private ArrayList<PictureInfo> pictureList;	
	private ArrayList<String> ticketAmtList = new ArrayList<String>();
	private final int COMMON_TYPE_TICKET_AMT  = 9090;
	private String renew_ticket_path = "";
	private String renew_ticket_file = "";
	private int orgTicketAmt = 0;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.ticket_sign_layout);
		
		ticketNo  = makeTicketNumber(parkCode, empCode, today);		
		
		cch = CodeHelper.getInstance(this);
		
		// 초기화
		Constants.KEY_CARNO_1 = "";
		Constants.KEY_CARNO_2 = "";
		Constants.KEY_CARNO_3 = "";
		Constants.KEY_CARNO_4 = "";
		Constants.KEY_CARNO_MANUAL = "";
		
		parkCdstate = "5";
		
	}
	
	@Override
	protected void initLayoutSetting() {
		// TODO Auto-generated method stub
		super.initLayoutSetting();
		
		TextView tv_top_title = (TextView) findViewById(R.id.tv_top_title);
		tv_top_title.setText("정기권등록");
		
		btn_top_left = (Button) findViewById(R.id.btn_top_left);
		btn_top_left.setVisibility(View.VISIBLE);
		btn_top_left.setOnClickListener(this);
		
		tvDateDuring  = (TextView)findViewById(R.id.tvDateDuring);
		tvUnPayment   = (TextView)findViewById(R.id.tvUnPayment);
		tvPayment	  = (TextView)findViewById(R.id.tvPayment);
		tvTicketAmt   = (TextView)findViewById(R.id.tvTicketAmt);
		
		btn_car_no_1 = (Button) findViewById(R.id.btn_car_no_1);
		btn_car_no_2 = (Button) findViewById(R.id.btn_car_no_2);
		btn_car_no_3 = (Button) findViewById(R.id.btn_car_no_3);
		btn_car_no_4 = (Button) findViewById(R.id.btn_car_no_4);
		btn_car_no_manual = (Button) findViewById(R.id.btn_car_no_manual);			
		
		edPhoneNumber 	  = (EditText)findViewById(R.id.edPhoneNumber);
		edCarOwner 		  = (EditText)findViewById(R.id.edCarOwner);
		edMemo	   		  = (EditText)findViewById(R.id.edMemo);
		btnChargeType 	  = (Button)findViewById(R.id.btnChargeType);
		btnChargeDiscount = (Button)findViewById(R.id.btnChargeDiscount);
		btnTicketSubmit   = (Button)findViewById(R.id.btnTicketSubmit);
		iv_image   = (ImageView) findViewById(R.id.iv_image);
		btn_cal_st = (Button) findViewById(R.id.btn_cal_st);
		btn_cal_ed = (Button) findViewById(R.id.btn_cal_ed);
		
		final Calendar c = Calendar.getInstance();
		
		String renew_date     = "";
		String renew_discount = "";
		
		Intent intent = getIntent();
		if(intent != null){
			carNo 		   = Util.isNVL(intent.getStringExtra("renew_car_no"));		
			renew_date     = Util.isNVL(intent.getStringExtra("renew_date"));		
			renew_discount = Util.isNVL(intent.getStringExtra("renew_discount"));	
			
			edCarOwner.setText(Util.isNVL(intent.getStringExtra("renew_owner")));
			edPhoneNumber.setText(Util.isNVL(intent.getStringExtra("renew_tel")));
			
			renew_car_kind = Util.isNVL(intent.getStringExtra("renew_car_kind"));	
			renew_state_cd = Util.isNVL(intent.getStringExtra("renew_state_cd"));	
			renew_address  = Util.isNVL(intent.getStringExtra("renew_address"));
			renew_ticket_path = Util.isNVL(intent.getStringExtra("renew_ticket_path"));
			renew_ticket_file =  Util.isNVL(intent.getStringExtra("renew_ticket_file"));
			edMemo.setText(Util.isNVL(intent.getStringExtra("renew_comment")));	
			
			if(renew_ticket_file != "")
			{
		    	PictureInfo info = new PictureInfo();
		    	info.setPath(renew_ticket_path + renew_ticket_file);
		    	pictureList = new ArrayList<PictureInfo>();
		    	pictureList.add(info);
		    	String pictureURL = Constants.SERVER_HOST + renew_ticket_path + renew_ticket_file;

		    	if(!Util.isEmpty(pictureURL)){
				  ImageLoader.setImageUrlView(TicketSignActivity.this, pictureURL, iv_image);
		    	}		 
		    	
		    	apiParkTicketPictureUploadCall(renew_ticket_path, renew_ticket_file);
		    	
			}
			
		}
		
		if(!Util.isEmpty(carNo)){
			setCarnoView(carNo,btn_car_no_1, btn_car_no_2, btn_car_no_3, btn_car_no_4, btn_car_no_manual);
		}
		
		if(!Util.isEmpty(renew_discount)){
			btnChargeDiscount.setText(renew_discount);
		} else {
			setCommonItem("TIDC", Constants.COMMON_TYPE_CHARGE_DISCOUNT, btnChargeDiscount);
		}
		
		int sYear=0, sMonth=0, sDay=0;
		
		if(!Util.isEmpty(renew_date)){ // 정기권 연장처리 
			sYear  = Util.sToi(renew_date.substring(0, 4));
			sMonth = Util.sToi(renew_date.substring(4, 6));
			sDay   = Util.sToi(renew_date.substring(6, 8));
			try {
				DateFormat df = new SimpleDateFormat("yyyyMMdd",new Locale("ko", "KOREA"));
				Date date = df.parse(renew_date);
				c.setTime(date);
				
				c.add(Calendar.DATE, 1);
				
				sYear = c.get(Calendar.YEAR);
				sMonth = c.get(Calendar.MONTH);
				sDay = c.get(Calendar.DAY_OF_MONTH);					
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				if(Constants.DEBUG_PRINT_LOG){
					e.printStackTrace();
				}else{
					System.out.println("예외 발생");
				}
			}
		} else {
			goToActivityInputCarNumber(Constants.RESULT_INPUT_CAR_NUMBER, Constants.CAR_TICKET, 2);
			sYear  = c.get(Calendar.YEAR);
			sMonth = c.get(Calendar.MONTH);
			sDay   = c.get(Calendar.DAY_OF_MONTH);
		}
		
		setDateView(sYear, sMonth, sDay);
		
		btnChargeDiscount.setOnClickListener(this);
		iv_image.setOnClickListener(this);
		btn_cal_st.setOnClickListener(this);
		btn_cal_ed.setOnClickListener(this);
		btnChargeType.setOnClickListener(this);
		btnTicketSubmit.setOnClickListener(this);
		
		btn_car_no_1.setOnClickListener(this);
		btn_car_no_2.setOnClickListener(this);
		btn_car_no_3.setOnClickListener(this);
		btn_car_no_4.setOnClickListener(this);
		btn_car_no_manual.setOnClickListener(this);		
		
		tvTicketAmt.setOnClickListener(this);
		tvUnPayment.setOnClickListener(this);
		
		
		int Len = codeJsonArr.length();
		for (int i = 0; i < Len; i++) {
			try {
				JSONObject res = codeJsonArr.getJSONObject(i);
				parkClass = "2";
				
				if("2".equals(parkClass)){
					if("AM2".equals(res.optString("CD_CLASS"))){
						ticketAmtList.add(Util.addComma(res.optString("CD_VALUE3","0"))+"원");
					}
				} else if("3".equals(parkClass)){
					if("AM3".equals(res.optString("CD_CLASS"))){
						ticketAmtList.add(Util.addComma(res.optString("CD_VALUE3","0"))+"원");
					}
				} else if("4".equals(parkClass)){
					if("AM4".equals(res.optString("CD_CLASS"))){
						ticketAmtList.add(Util.addComma(res.optString("CD_VALUE3","0"))+"원");
					}
				} else if("5".equals(parkClass)){
					if("AM5".equals(res.optString("CD_CLASS"))){
						ticketAmtList.add(Util.addComma(res.optString("CD_VALUE3","0"))+"원");
					}
				} else if("6".equals(parkClass)){
					if("AM6".equals(res.optString("CD_CLASS"))){
						ticketAmtList.add(Util.addComma(res.optString("CD_VALUE3","0"))+"원");
					}
				}
							

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				if(Constants.DEBUG_PRINT_LOG){
					e.printStackTrace();
				}else{
					System.out.println("예외 발생");
				}
			}			
		}  // end for
		
		if(ticketAmtList.size() > 0){
			ticketAmt = ticketAmtList.get(0).replaceAll("원", "").replaceAll(",","");
		}
		
		
		orgTicketAmt = Integer.valueOf(ticketAmt);
		finalAmt = Util.sToi(ticketAmt);
		tvTicketAmt.setText(Util.addComma(ticketAmt)+"원");
		tvUnPayment.setText("0원");
		setCommonItem("B1", Constants.COMMON_TYPE_CHARGE_TYPE, btnChargeType);
		setPayment();
	}
	
	private void apiParkTicketPictureUploadCall(String _path, String _img){
		
        Map<String, Object> params = new HashMap<String, Object>();
        params.put(Constants.API_FORMAT, Constants.JSON_FORMAT);
        params.put("TICKET_NO", ticketNo);
        params.put("CD_PARK",   parkCode);
        params.put("CAR_NO",    carNo);
        params.put("CD_TYPE",   "R1");
        params.put("EMP_CD",    empCode);
        params.put("IMAGE_FILE", _img);        
        params.put("filePath", _path);
        
        String url = Constants.API_SERVER + "?" + Constants.METHOD +"="+ Constants.INTERFACEID_PARK_TICKET_PICTURE_UPLOAD2;
        Log.d(TAG, " apiParkTicketPictureUploadCall url >>> " + url + params);
        
		AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>();        
		cb.url(url).params(params).type(JSONObject.class).weakHandler(this, "parkTicketPictureUploadCallback").redirect(true).retry(3).fileCache(false).expire(-1);
		AQuery aq = new AQuery(this);
		aq.ajax(cb); 
		
	}		
	
	public void parkTicketPictureUploadCallback(String url, JSONObject json, AjaxStatus status){
		Log.d(TAG, " parkTicketPictureUploadCallback  json ====== " +json);
		// successful ajax call          
        if(json != null){   
           String KN_RESULT = json.optString("KN_RESULT");
           if("1".equals(json.optString("CD_RESULT"))){
        	   MsgUtil.ToastMessage(this, KN_RESULT);
           } else {
        	   MsgUtil.ToastMessage(this, KN_RESULT);
           }
        } else {
    	    MsgUtil.ToastMessage(this, Constants.DATA_FAIL, Toast.LENGTH_LONG);
        }
	}		
	
	private void setPayment()
	{
		/*
		if("5".equals(parkCdstate)) {
			setPayment_5();
		} else {
			setPayment_234();
		}
		*/
		setPayment_234();
	}

	private void setPayment_234()
	{
		// 할인금액 초기화
		disAmt = 0;

		int total = Util.sToi(tvTicketAmt.getText().toString().replace("원", "").replace(",", ""),0);

		// 정기권 금액 초기화
		ticketAmt = ""+total;

		// 요금할인 정보
		String chargeDiscount = btnChargeDiscount.getText().toString();

		// 정기권 할인시간은 필요없음...?!
		String dis_rate = cch.findCodeByDisRate("TIDC", chargeDiscount);
		if(!"0".equals(dis_rate)){
			// 할인률 설정
			disAmt   = discountChargeRate(""+total, dis_rate);
		}

		String dis_amt = cch.findCodeByDisAmt("TIDC", chargeDiscount);
		if(!"0".equals(dis_amt)){
			// 할인금액 설정
			disAmt   += Util.sToi(dis_amt,0);
		}

		total = total - disAmt;
		Log.d(TAG, " total >>> " + total);

		String start_date = btn_cal_st.getText().toString();
		String end_date   = btn_cal_ed.getText().toString();

		String[] arrStart = start_date.split("-");
		String[] arrEnd   = end_date.split("-");




		// 2월 28일 29일 때 처리
		if("02".equals(arrStart[1]) && "01".equals(arrStart[2]) && ("28".equals(arrEnd[2]) || "29".equals(arrEnd[2]))){
			finalAmt = total;
			// Log.d(TAG, " finalAmt 111 >>> " + finalAmt );
		} else if("01".equals(arrStart[2]) && ("30".equals(arrEnd[2]) || "31".equals(arrEnd[2]))){ // 시작일이 1일이고 마지막일자로 선택일 때 처리
			finalAmt = total;
			// Log.d(TAG, " finalAmt 222 >>> " + finalAmt );
		} else {
			try {
				Calendar cal = Calendar.getInstance();
				cal.set(Util.sToi(arrStart[0]), Util.sToi(arrStart[1])-1, Util.sToi(arrStart[2]));
//				cal.add(Calendar.MONTH, -1);
//				cal.add(Calendar.DATE,  0);

				int maxDay = cal.getActualMaximum(Calendar.DAY_OF_MONTH);

				//  Log.d(TAG, " maxDay >>> " + maxDay );

				String dateDiff = ""+Util.diffOfDate(Util.dateStringExt(arrStart[0]+arrStart[1]+arrStart[2]+"120000")
						, Util.dateStringExt(arrStart[0]+arrStart[1]+maxDay+"120000"));
				dateDiff  = String.valueOf(Integer.parseInt(dateDiff)+1);
				int payAmt = total / maxDay;
				int totalAmt = (payAmt*Util.sToi(dateDiff))>total?total:(payAmt*Util.sToi(dateDiff));

				int truncAmt = 0;
				if (!"0".equals(truncUnit)) {
					truncAmt = totalAmt % Util.sToi(truncUnit);
				} else {
					truncAmt = 0;
				}

				finalAmt = totalAmt - truncAmt;

			} catch (NullPointerException e) {
				if(Constants.DEBUG_PRINT_LOG){
					e.printStackTrace();
				}else{
					System.out.println("예외 발생");
				}
			}
		}

		setChargeType();
	}


	private void setPayment_234_old()
	{
		// 할인금액 초기화
		disAmt = 0;
		
		int total = Util.sToi(tvTicketAmt.getText().toString().replace("원", "").replace(",", ""),0);
		
		// 정기권 금액 초기화 
		ticketAmt = ""+total;
		
		// 요금할인 정보 
		String chargeDiscount = btnChargeDiscount.getText().toString();
		
		// 정기권 할인시간은 필요없음...?! 
		String dis_rate = cch.findCodeByDisRate("TIDC", chargeDiscount);		
		if(!"0".equals(dis_rate)){
			// 할인률 설정
			disAmt   = discountChargeRate(""+total, dis_rate);
		}		
		
		String dis_amt = cch.findCodeByDisAmt("TIDC", chargeDiscount);
		if(!"0".equals(dis_amt)){
			// 할인금액 설정
			disAmt   += Util.sToi(dis_amt,0);
		}				
		
		
		if(disAmt < 0)
		{
			disAmt = -(disAmt);
		}
		
		
		total = total - disAmt;
		Log.d(TAG, " total >>> " + total);
		
		/*
		String start_date = btn_cal_st.getText().toString();
		String end_date   = btn_cal_ed.getText().toString();
		
		String[] arrStart = start_date.split("-");
		String[] arrEnd   = end_date.split("-");
		
		// 2월 28일 29일 때 처리
		if("02".equals(arrStart[1]) && "01".equals(arrStart[2]) && ("28".equals(arrEnd[2]) || "29".equals(arrEnd[2]))){
			finalAmt = total;
			// Log.d(TAG, " finalAmt 111 >>> " + finalAmt );
		} else if("01".equals(arrStart[2]) && ("30".equals(arrEnd[2]) || "31".equals(arrEnd[2]))){ // 시작일이 1일이고 마지막일자로 선택일 때 처리 
			finalAmt = total;
			// Log.d(TAG, " finalAmt 222 >>> " + finalAmt );
		} else {
			try {
			   Calendar cal = Calendar.getInstance();
			   cal.set(Util.sToi(arrStart[0]), Util.sToi(arrStart[1]), Util.sToi(arrStart[2]));
			   cal.add(Calendar.MONTH, -1);
			   cal.add(Calendar.DATE,  0);
			   
			   int maxDay = cal.getActualMaximum(Calendar.DATE);			   
			    
			   
			   String dateDiff = ""+Util.diffOfDate(Util.dateStringExt(arrStart[0]+arrStart[1]+arrStart[2]+"120000")
					   							  , Util.dateStringExt(arrStart[0]+arrStart[1]+maxDay+"120000"));
			   
			   int payAmt = total / maxDay;
			   
			   int totalAmt = (payAmt*Util.sToi(dateDiff));
			   
			   int truncAmt = 0;
			   if (!"0".equals(truncUnit)) {
					truncAmt = totalAmt % Util.sToi(truncUnit);
			   } else {
					truncAmt = 0;
			   }
			   
			   finalAmt = totalAmt - truncAmt;
			   
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}				
		}
		*/
		
	   int truncAmt = 0;
	   if (!"0".equals(truncUnit)) {
			truncAmt = total % Util.sToi(truncUnit);
	   } else {
			truncAmt = 0;
	   }
	   
	   finalAmt = total - truncAmt;		
	   setChargeType();		
	}
	
	private void setPayment_5() {
		
		// 할인금액 초기화
		disAmt = 0;
		String start_date = btn_cal_st.getText().toString();
		String end_date   = btn_cal_ed.getText().toString();
		
		String[] arrStart = start_date.split("-");
		String[] arrEnd   = end_date.split("-");
		
		int st = Integer.valueOf(arrStart[1]);
		int ed = Integer.valueOf(arrEnd[1]);
		int su = ed - st;
		int sum = 0;
		int truncAmt = 0;
		
		switch(su) {
		case 0:
			sum = orgTicketAmt;
			break;
		case 1:
			sum = orgTicketAmt*2;
			break;
		case 2:
			sum = orgTicketAmt*3;
			break;

		}
		
		int total = Util.sToi(tvTicketAmt.getText().toString().replace("원", "").replace(",", ""),0);
		total = sum;
		tvTicketAmt.setText(String.valueOf(total));
				
		// 정기권 금액 초기화 
		ticketAmt = ""+total;

		// 2월 28일 29일 때 처리
		if("02".equals(arrStart[1]) && "01".equals(arrStart[2]) && ("28".equals(arrEnd[2]) || "29".equals(arrEnd[2]))){
			finalAmt = total;
		} else if("01".equals(arrStart[2]) && ("30".equals(arrEnd[2]) || "31".equals(arrEnd[2]))){ // 시작일이 1일이고 마지막일자로 선택일 때 처리 
			finalAmt = total;
		} else {
			try {
			   Calendar cal = Calendar.getInstance();
			   cal.set(Util.sToi(arrStart[0]), Util.sToi(arrStart[1]), Util.sToi(arrStart[2]));
			   cal.add(Calendar.MONTH, -1);
			   cal.add(Calendar.DATE,  0);
			   
			   int maxDay = cal.getActualMaximum(Calendar.DATE);			   
			    
			   //  Log.d(TAG, " maxDay >>> " + maxDay );
			   
			   String dateDiff = ""+Util.diffOfDate(Util.dateStringExt(arrStart[0]+arrStart[1]+arrStart[2]+"120000")
					   							  , Util.dateStringExt(arrStart[0]+arrStart[1]+maxDay+"120000"));
			   
			   int payAmt = orgTicketAmt / maxDay;
			   
			   int totalAmt = (payAmt*Util.sToi(dateDiff));

			   if (!"0".equals(truncUnit)) {
					truncAmt = totalAmt % Util.sToi(truncUnit);
			   } else {
					truncAmt = 0;
			   }
			   
			   finalAmt = totalAmt - truncAmt;
			   Log.d("finalAmt >>>>>>>>", String.valueOf(totalAmt) + "~" + String.valueOf(truncAmt));
			   if(su==1) {
				   finalAmt = finalAmt + orgTicketAmt;
			   } else if(su==2) {
				   finalAmt = finalAmt + (orgTicketAmt*2);
			   }			   
			   
			} catch (NullPointerException e) {
				if(Constants.DEBUG_PRINT_LOG){
					e.printStackTrace();
				}else{
					System.out.println("예외 발생");
				}
			}				
		}
		
		// 요금할인 정보 
		String chargeDiscount = btnChargeDiscount.getText().toString();
		
		// 정기권 할인시간은 필요없음...?! 
		String dis_rate = cch.findCodeByDisRate("TIDC", chargeDiscount);		
		if(!"0".equals(dis_rate)){
			// 할인률 설정
			disAmt   = discountChargeRate(""+finalAmt, dis_rate);
		}		
		
		String dis_amt = cch.findCodeByDisAmt("TIDC", chargeDiscount);
		if(!"0".equals(dis_amt)){
			// 할인금액 설정
			disAmt   += Util.sToi(dis_amt,0);
		}				
		
		if(disAmt < 0)
		{
			disAmt = -(disAmt);
		}
				
		
		finalAmt = finalAmt - disAmt;
		
	    if (!"0".equals(truncUnit)) {
			truncAmt = finalAmt % Util.sToi(truncUnit);
	    } else {
			truncAmt = 0;
	    }		
		
	    finalAmt = finalAmt - truncAmt;
	    
		Log.d(TAG, " finalAmt >>> " + finalAmt);		
		
		setChargeType();
		
	}
	
	private void setChargeType() {
		Log.d(TAG, " finalAmt >>> " + finalAmt);
		if(btnChargeType.getText().toString().equals("무통장입금"))
		{
			//무통장입금 선택시 
			IS_CHARGETYPE = false;
			tvPayment.setText("0원");
			tvUnPayment.setText(Util.addComma(finalAmt)+"원");
		} else {
			IS_CHARGETYPE = true;
			tvPayment.setText(Util.addComma(finalAmt)+"원");
			tvUnPayment.setText("0원");
		}		
	}
	
	private void setDateView(int year, int month, int day) {

		int sYear=0, sMonth=0, sDay=0;
		int eMonth = 0;
		int tempMonth = 0;

		Log.d(TAG, " year : " + year);
		Log.d(TAG, " month : " + month);
		Log.d(TAG, " day : " + day);
		
		sYear  = year;
		sMonth = month + 1;

		if("5".equals(parkCdstate)) {
			tempMonth = month + 1;
			switch(tempMonth)
			{
				case 1:
				case 2:
				case 3:
					eMonth = 3;
					break;
				case 4:
				case 5:
				case 6:
					eMonth = 6;
					break;
				case 7:
				case 8:
				case 9:
					eMonth = 9;
					break;
				case 10:
				case 11:
				case 12:
					eMonth = 12;
					break;
			}
		} else {
			//sMonth = month + 1;
			eMonth = month + 1;
		}

		eMonth = month + 1;


		sDay   = day;
		
		Log.d(TAG, " sYear : " + sYear);
		Log.d(TAG, " sMonth : " + sMonth);
		Log.d(TAG, " sDay : " + sDay);
		
		Calendar cal = Calendar.getInstance();
		cal.set(year, month, day);
		cal.add(Calendar.MONTH, 0);
		cal.add(Calendar.DATE,  0);
		   
		//int eDay = cal.getActualMaximum(Calendar.DATE);
		int eDay = 0;
		if("5".equals(parkCdstate)) {
			switch(eMonth) {
				case 3:
					eDay = 31;
					break;
				case 6:
					eDay = 30;
					break;
				case 9:
					eDay = 30;
					break;
				case 12:
					eDay = 31;
					break;
			}

		} else {
			eDay = cal.getActualMaximum(Calendar.DATE);
		}
		eDay = cal.getActualMaximum(Calendar.DATE);

		Log.d(TAG, " eDay : " + eDay);

		/*
		String sDate = Util.addZero(sYear) + "-" + Util.addZero(sMonth) + "-" + Util.addZero(sDay);
		btn_cal_st.setText(sDate);
		Log.d(TAG, " eDay : " + eDay);
		String endDate = Util.addMinusDate(Util.addMinusDate(sDate+" 00:00:01", 1, 2),-24,1).substring(0, 10);
		Log.d(TAG, " eDay : " + eDay);
		btn_cal_ed.setText(endDate);
		*/
		String sDate = Util.addZero(sYear) + "-" + Util.addZero(sMonth) + "-" + Util.addZero(sDay);
		btn_cal_st.setText(sDate);

		String endDate = Util.addZero(sYear) + "-" + Util.addZero(eMonth) + "-" + Util.addZero(eDay);
		btn_cal_ed.setText(endDate);
		setPayment();
	}
	
	@Override
	protected void request(int type, Object data) {
		if(type == PayData.PAY_DIALOG_TYPE_TICKET_CASH){
			PayVo payVo =(PayVo)data;
			apiParkTicketCall(payVo);
			PayData payData = new PayData(this, OUT_PRINT, IN_PRINT, TICKET_PRINT);
			
			//if(Constants.isReceiptPrint){
				payData.printDefault(PayData.PAY_DIALOG_TYPE_TICKET_SUCCESS, type11Vo, mPrintService);
				payData.ImagePrint(empLogo_image);
				try
				{
					  payData.printText("\n\n");
				}
				catch(UnsupportedEncodingException e)
				{
					System.out.println("UnsupportedEncodingException 예외 발생");
				}
			//}
			
		} else if(type == PayData.PAY_DIALOG_TYPE_TICKET_CARD){
			PayVo payVo =(PayVo)data;
			apiParkTicketCall(payVo);			
		}
	}
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		super.onClick(v);
		switch (v.getId()) {
		
		  case R.id.btn_car_no_1:
			goToActivityInputCarNumber(Constants.RESULT_INPUT_CAR_NUMBER, Constants.CAR_UPDATE, 1);
			break;
			
		  case R.id.btn_car_no_2:
			goToActivityInputCarNumber(Constants.RESULT_INPUT_CAR_NUMBER, Constants.CAR_UPDATE, 2);
			break;
			
		  case R.id.btn_car_no_3:
			goToActivityInputCarNumber(Constants.RESULT_INPUT_CAR_NUMBER, Constants.CAR_UPDATE, 3);
			break;
			
		  case R.id.btn_car_no_4:
			goToActivityInputCarNumber(Constants.RESULT_INPUT_CAR_NUMBER, Constants.CAR_UPDATE, 4);
			break;
			
		  case R.id.btn_car_no_manual:
			goToActivityInputCarNumber(Constants.RESULT_INPUT_CAR_NUMBER, Constants.CAR_UPDATE, 5);
			break;			
			
		  case R.id.btnTicketSubmit:
			
			if(carNo.equals("")){
				setToast("차량번호가 없습니다.");
				return;
			}
//			else if(edPhoneNumber.getText().toString().equals("")){
//				setToast("전화번호가 없습니다.");
//				return;
//			}
			
			// 정기권 사진 필수 체크
			if(Constants.isTicketPicture){
			   // 4급지는 안 찍어도 됨	
			   if(!"110".equals(parkCode) && !"4".equals(parkClass)){
				   if(pictureList == null || pictureList.size() < 0){
					   MsgUtil.AlertDialog(this, "알림", "최소한 정기권사진 1장이 필요 합니다.\n정기권사진촬영 후 이용해 주시기 바랍니다.");
					   return ;
				   }				
			   }
			}
			
			if(IS_CHARGETYPE) // 정상정기권
			{
			   type11Vo = new ReceiptType11Vo(Util.getYmdhms("yyyy-MM-dd HH:mm:ss")			// 출력일자
							               , "정기권"			// 요금제
							               , tvDateDuring.getText().toString().replaceAll("일", "")		// 사용기간
							               , btnChargeDiscount.getText().toString()						// 할인종류
							               , btn_cal_st.getText().toString()		// 시작날짜
							               , btn_cal_ed.getText().toString()		// 종료날짜
							               , Util.addComma(finalAmt)	// 납부금액
							               , ticketAmt					// 정기요금
							               , String.valueOf(disAmt)		// 할인금액
										   , BIZ_NO						// 사업자번호
										   , BIZ_NAME
										   , BIZ_TEL
										   , "");
			   
			    type11Vo.setPioNum(ticketNo);
			    type11Vo.setCarNum(carNo);
			    type11Vo.setParkName(parkName);
			    type11Vo.setEmpName(empName);
			    type11Vo.setEmpPhone(empPhone);
			    type11Vo.setEmpTel(empTel);
				type11Vo.setEmpBusiness_Tel(empBusiness_TEL);
				type11Vo.setOutPrint(Preferences.getValue(this,Constants.PREFERENCE_OUT_PRINT,""));
				ticketDialog("정기권", ticketAmt, tvUnPayment.getText().toString().replaceAll("원","").replaceAll(",",""), finalAmt, type11Vo);
			} else {  // 미수정기권(무통장입금)
			   type11Vo = new ReceiptType11Vo(Util.getYmdhms("yyyy-MM-dd HH:mm:ss")			// 출력일자
							               , "정기권"			// 요금제
							               , tvDateDuring.getText().toString().replaceAll("일", "")		// 사용기간
							               , btnChargeDiscount.getText().toString()						// 할인종류
							               , btn_cal_st.getText().toString()		// 시작날짜
							               , btn_cal_ed.getText().toString()		// 종료날짜
							               , Util.addComma(finalAmt)	// 납부금액
							               , ticketAmt					// 정기요금
							               , String.valueOf(disAmt)		// 할인금액
										   , BIZ_NO						// 사업자번호
										   , BIZ_NAME
										   , BIZ_TEL
										   , "");
			   
			     type11Vo.setPioNum(ticketNo);
			     type11Vo.setCarNum(carNo);
			     type11Vo.setParkName(parkName);
			     type11Vo.setEmpName(empName);
			     type11Vo.setEmpPhone(empPhone);
			     type11Vo.setEmpTel(empTel);
					type11Vo.setEmpBusiness_Tel(empBusiness_TEL);
				type11Vo.setOutPrint(Preferences.getValue(this,Constants.PREFERENCE_OUT_PRINT,""));
				 apiParkTicketCall(null);
			}
			break;
			
		  case R.id.btn_top_left:
			finish();
			break;
			
		  case R.id.btnChargeDiscount:
			setCommonDialog("요금할인","TIDC", Constants.COMMON_TYPE_CHARGE_DISCOUNT, btnChargeDiscount);
			break;
			
		  case R.id.btnChargeType:
			setCommonDialog("요금종류","B1", Constants.COMMON_TYPE_CHARGE_TYPE, btnChargeType);
			break;
			
		  case R.id.tvTicketAmt:
			  basicListDialog(COMMON_TYPE_TICKET_AMT, "정기권금액", ticketAmtList, tvTicketAmt, null);
			  break;
			
		  case R.id.btn_cal_st:
			caledarDialog = new calendarDlg(this);
			caledarDialog.show();
			break;
			
		  case R.id.btn_cal_ed:
			setToast("시작날짜만 변경 가능합니다. ");
			break;
			
		  case R.id.iv_image:
			goToTicketPictureView(ticketNo, carNo, Constants.PICTURE_TYPE_R1, parkCode,  empCode);			
			break;
			
		  case R.id.tvUnPayment:
			if(!tvUnPayment.getText().toString().replaceAll("원", "").equals("0"))
			goToUnpaidManagerActivity(carNo);
			break;
		}
		
	}
	
	
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		// TODO Auto-generated method stub
		super.onConfigurationChanged(newConfig);
	}

	private boolean IS_CHARGETYPE = true;
	
	@Override
	public void getSelectListDialogData(int tag,String data) {
		switch (tag) {
		case Constants.COMMON_TYPE_CHARGE_DISCOUNT:
			setPayment();
			break;
			
		case COMMON_TYPE_TICKET_AMT :
			setPayment();
			break;
			
		case Constants.COMMON_TYPE_CHARGE_TYPE:
			setChargeType();
			break;
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		
		super.onActivityResult(requestCode, resultCode, data);
		
		Log.d(TAG, " requestCode >>> " + requestCode);
		switch (requestCode) {
			case Constants.RESULT_INPUT_CAR_NUMBER:
				if(resultCode != RESULT_OK){
					return;
				}
				
				if (data != null) { // 번호 입력 시
					String carInputMode = Util.isNVL(data.getStringExtra(Constants.CAR_INPUT_MODE));
					if(Constants.CAR_NORMAL.equals(carInputMode)){
						carNo = Constants.KEY_CARNO_1 + Constants.KEY_CARNO_2 + Constants.KEY_CARNO_3 + Constants.KEY_CARNO_4; 
						
						setCarnoView(Constants.KEY_CARNO_1, Constants.KEY_CARNO_2, Constants.KEY_CARNO_3, Constants.KEY_CARNO_4, "",
								 	 btn_car_no_1, btn_car_no_2, btn_car_no_3, btn_car_no_4, btn_car_no_manual, Constants.CAR_NORMAL);
						
					} else if(Constants.CAR_MANUAL.equals(carInputMode)){
						carNo = Constants.KEY_CARNO_MANUAL;
						
						setCarnoView("", "", "", "", Constants.KEY_CARNO_MANUAL,
								 	 btn_car_no_1, btn_car_no_2, btn_car_no_3, btn_car_no_4, btn_car_no_manual, Constants.CAR_MANUAL);
						
					}  					
					
				}			
				break;
			case Constants.RESULT_CAR_PICTURE : 
				Log.d(TAG," RESULT_CAR_PICTURE ::: " + Constants.RESULT_CAR_PICTURE);
				apiParkTicketPictureCall();
				break;
			case PayData.PAY_DIALOG_TYPE_TICKET_CARD   :
					String transactionNo = "";
					String approvalNo    = "";
					String approvalDate  = "";
					String cardNo 	     = "";
					String cardAmt 	     = "";
					String cardCompany   = "";
					try{
						//Log.d("111", "mPrintService.getState() : " + mPrintService.getState());
						try
						{
							mPrintService.stop();
							if (mPrintService.getState()==0) {
			
								BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(mBluetoothAddress);
								mPrintService.connect(device, true);
							}
						} catch (IllegalArgumentException e) {
							System.out.println("IllegalArgumentException 예외 발생");
						} catch (NullPointerException e) {
							System.out.println("NullPointerException 예외 발생");
						}
						
						//Log.d(TAG, "approval : " + Preferences.getValue(this, Constants.PREFERENCE_MCPAY_APPROVAL_NO, ""));

						if(data != null){
						   Bundle eB = data.getExtras();
						   if(eB != null){
							   /**
							   Log.d(TAG, " eB >>>> " + eB );
							   for (String key : eB.keySet()) {
							       Log.d(TAG, " eB :::: " + key + " => " + eB.get(key) );
							   }
							   **/
						   }
						   
						   transactionNo   	 = Util.replaceNullIntent(eB,"transactionNo");
						   approvalNo   	 = Util.replaceNullIntent(eB,"approvalNo");
						   approvalDate 	 = Util.replaceNullIntent(eB,"approvalDate");
						   cardNo      	     = Util.replaceNullIntent(eB,"cardNo");
						   cardCompany   	 = Util.replaceNullIntent(eB,"cardCompany");
						   cardAmt   	 	 = Util.isNVL(Util.replaceNullIntent(eB,"cardAmt"),"0");
						}

						transactionNo = Preferences.getValue(this,Constants.PREFERENCE_MCPAY_TRANSACTION_NO,  "");
						approvalNo = Preferences.getValue(this,Constants.PREFERENCE_MCPAY_APPROVAL_NO,  "");
						approvalDate = Preferences.getValue(this,Constants.PREFERENCE_MCPAY_APPROVAL_DATE,  "");
						cardNo = Preferences.getValue(this,Constants.PREFERENCE_MCPAY_CARD_NO,  "");
						cardCompany = Preferences.getValue(this,Constants.PREFERENCE_MCPAY_CARD_COMPANY,  "");
						cardAmt = Preferences.getValue(this,Constants.PREFERENCE_MCPAY_TOTAL_AMT,  "");
						
						if(transactionNo == "" || cardNo== "" ||  approvalNo == "") {
							Log.d("111", "Card error");
							return;
						}
						//prepayDialog.dismiss();
						if(Integer.valueOf(cardAmt) == 0)
							cardAmt = tvPayment.getText().toString().replaceAll("원", "");
						
						
						Log.d("111", "~~~~~~~~~~~~~~~~~~~~~~~~ : " + cardAmt);
						
					    if(cardNo.length() >= 12){
						   cardNo = cardNo.substring(0,4) + cardNo.substring(12);
					    }
					    
					    try
					    {
					    	Thread.sleep(1000);
					    }catch(InterruptedException e){
							System.out.println("InterruptedException 예외 발생");
						}
				    
			       	 	Bundle eB = new Bundle();
			    		eB.putString("transactionNo", transactionNo);
			    		eB.putString("approvalNo",    approvalNo);
			    		eB.putString("approvalDate",  approvalDate);
			    		eB.putString("cardNo",        cardNo);
			    		eB.putString("cardCompany",   cardCompany);
			    		eB.putString("cardAmt",	      cardAmt);
			    		eB.putString("pioNum",		  gReceiptVo.getPioNum());
			    		eB.putString("carNum",		  gReceiptVo.getCarNum());
			    		PayData payData = new PayData(this, OUT_PRINT, IN_PRINT, TICKET_PRINT);
			    		payData.printPayment(PayData.PAY_DIALOG_TYPE_TICKET_CARD, gReceiptVo, mPrintService, eB, null);
			    		
					    payData.ImagePrint(empLogo_image);
					    try
					    {
						  payData.printText("\n\n");
					    }
					    catch(UnsupportedEncodingException e)
					    {
							System.out.println("UnsupportedEncodingException 예외 발생");
					    }	
					    
					    
					 } catch (NullPointerException e) {
						if(Constants.DEBUG_PRINT_LOG){
							e.printStackTrace();
						}else{
							System.out.println("예외 발생");
						}
					 }
					
					InitPreferences();
					
					// 서버 결제 로그 저장 step2
					String url = Constants.API_SERVER + "?" + Constants.METHOD +"="+ Constants.INTERFACEID_LOG_PARAMETER;
					String param = "&transactionNo="+transactionNo
								 + "&approvalNo="+approvalNo
								 + "&approvalDate="+approvalDate
								 + "&cardNo="+cardNo
								 + "&cardCompany="+cardCompany
								 + "&cardAmt="+cardAmt
								 + "&class="+this.getClass().getSimpleName()
								 + "&type=pay_step2"
								 + "&"+Constants.API_FORMAT+"="+Constants.JSON_FORMAT;

					TAG = "111";
					Log.d(TAG, " apiParkLogCall step 2 url >>> " + url + param);
			        
					AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>();        
					cb.url(url + param).type(JSONObject.class).weakHandler(this, "").redirect(true).retry(3).fileCache(false).expire(-1);
					AQuery aq = new AQuery(this);
					aq.ajax(cb); 				
						 
					ticketDialog.dismiss();
					PayVo payVo = new PayVo();
					payVo.setTransactionNo(transactionNo);
					payVo.setApprovalNo(approvalNo);
					payVo.setApprovalDate(approvalDate);
					payVo.setCardNo(cardNo);
					payVo.setCardCompany(cardCompany);
					payVo.setCardAmt(cardAmt);
					payVo.setCashAmt("0");
					payVo.setCatNumber(catNumber);
					 
					request(PayData.PAY_DIALOG_TYPE_TICKET_CARD, payVo);				  
				  break;
			  case PayData.PAY_DIALOG_TYPE_CASH_RECEIPT:  // 현금영수증
					try
					{
						mPrintService.stop();
						if (mPrintService.getState()==0) {
		
							BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(mBluetoothAddress);
							mPrintService.connect(device, true);
						}
					}  catch (IllegalArgumentException e) {
						System.out.println("IllegalArgumentException 예외 발생");
					} catch (NullPointerException e) {
						System.out.println("NullPointerException 예외 발생");
					}
				  
					approvalNo = Preferences.getValue(this,Constants.PREFERENCE_MCPAY_APPROVAL_NO,  "");
					approvalDate = Preferences.getValue(this,Constants.PREFERENCE_MCPAY_APPROVAL_DATE,  "");
					//String amt_tot = Preferences.getValue(this,Constants.PREFERENCE_MCPAY_TOTAL_AMT,  "");
					String amt_tot = Preferences.getValue(this,Constants.PREFERENCE_MCPAY_TOTAL_AMT,  "");
					
					PayVo payVo2 = new PayVo();  //현금영수증참조
					payVo2.setCashApprovalNo(approvalNo);
					payVo2.setCashApprovalDate(approvalDate);
					tvCashBill.setTag(payVo2);
					tvCashBill.setText("발행");		
					InitPreferences();
				    try
				    {
				    	Thread.sleep(1000);
				    }catch(InterruptedException e){
						System.out.println("InterruptedException 예외 발생");
					}
					cashReceiptRun(Util.sToi(amt_tot), g_vo);
				  break;					
				
				
		} // end switch
		
	}
	
	void apiParkTicketCall(PayVo payVo) {
		
		if(payVo == null){
			payVo = new PayVo(); 
			payVo.setCashAmt("0");			
			payVo.setCardAmt("0");
		}        
		
		int cardAmt = Util.sToi(payVo.getCardAmt());
		int cashAmt = Util.sToi(payVo.getCashAmt());
		
		String payment = (cardAmt > 0 ? ""+cardAmt : ""+cashAmt);
		
        Map<String, Object> params = new HashMap<String, Object>();
        params.put(Constants.METHOD, 	 Constants.INTERFACEID_PARK_TICKET);
        params.put(Constants.API_FORMAT, Constants.JSON_FORMAT);		
        //정기권번호
        params.put("TICKET_NO",  ticketNo);
        // 차종 
        params.put("CAR_KIND",   renew_car_kind);
        // 소유주 
        params.put("CAR_OWNER",  edCarOwner.getText().toString());
        // 연락처
        params.put("OWNER_TEL",  edPhoneNumber.getText().toString());
        // 주차장번호
        params.put("CD_PARK",  	 parkCode);
        // 구입일
        params.put("BUY_DAY",    Util.getYmdhms("yyyyMMdd"));
        // 자동차번호
        params.put("CAR_NO", 	 carNo);
        // 서비스 시작일
        params.put("START_DAY",  btn_cal_st.getText().toString().replace("-", ""));
        // 서비스 종료일
        params.put("END_DAY", 	 btn_cal_ed.getText().toString().replace("-", ""));
        // 실제금액
        params.put("TICKET_AMT", ticketAmt);
        // 할인코드
        params.put("DIS_CD", 	 cch.findCodeByKnCode("TIDC", btnChargeDiscount.getText().toString()));
    	// 할인금액
        params.put("DIS_AMT",    disAmt);
        // 정기권 결제금액
        params.put("PAY_AMT",    payment);
        
        // 현금 영수증 관련
        params.put("CASH_APPROVAL_NO", 	 payVo.getCashApprovalNo());
        params.put("CASH_APPROVAL_DATE", payVo.getCashApprovalDate());
        params.put("CATNUMBER",   	 	 payVo.getCatNumber());
        
        // 카드 결제 관련
        params.put("TRANSACTION_NO", 	payVo.getTransactionNo());
        params.put("APPROVAL_NO",    	payVo.getApprovalNo());
        params.put("APPROVAL_DATE",  	payVo.getApprovalDate());
        params.put("CARD_NO",     		payVo.getCardNo());
        params.put("CARD_COMPANY", 	    payVo.getCardCompany());
        
        int YET_AMT = Util.sToi(tvUnPayment.getText().toString().replace("원", "").replace(",", ""),0);
        if(YET_AMT > 0){
        	// 정기권 종류
        	params.put("CD_TYPE",   "44");
        } else {
        	// 정기권 종류
        	params.put("CD_TYPE",   "05");
        }
        
        // 미수금액 
        params.put("YET_AMT",    YET_AMT);
        // 현금금액  
        params.put("CASH_AMT",   payVo.getCashAmt());
        // 카드금액 
        params.put("CARD_AMT",   payVo.getCardAmt());
        // 사번코드
        params.put("EMP_CD",     empCode);
        // 월정기 구분
        params.put("STATE_CD",  renew_state_cd);
        // 주소
        params.put("ADDRESS",   renew_address);
        // 코멘트
        params.put("KN_COMMENT", edMemo.getText().toString());
        
        showProgressDialog(false);
        
		String url = Constants.API_SERVER;
		Log.d(TAG, " apiParkTicketCall url >>> " + url + " " + params);
        
		AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>();        
		cb.url(url).params(params).type(JSONObject.class).weakHandler(this, "parkTicketCallback").redirect(true).retry(3).fileCache(false).expire(-1);
		AQuery aq = new AQuery(this);
		aq.ajax(cb);   		
	}
	
	public void parkTicketCallback(String url, JSONObject json, AjaxStatus status){
		
		closeProgressDialog();
		
		Log.d(TAG, " parkTicketCallback  json ====== " +json);
		
		// successful ajax call          
        if(json != null){   
           String KN_RESULT = json.optString("KN_RESULT");
           if("1".equals(json.optString("CD_RESULT"))){
	  		  if(!IS_CHARGETYPE){  // 미수정기권(무통장입금)
				  type11Vo.setPayment("0");
				  type11Vo.setYetAmt(Util.toStr(finalAmt));
				  PayData payData = new PayData(this, OUT_PRINT, IN_PRINT, TICKET_PRINT);
				  payData.printDefault(PayData.PAY_DIALOG_TYPE_TICKET_UNPAY, type11Vo, mPrintService);		  			   
			  }
	  		  MsgUtil.ToastMessage(this, KN_RESULT, Toast.LENGTH_LONG);       
	  		  finish();
           } else {
        	   MsgUtil.ToastMessage(this, KN_RESULT, Toast.LENGTH_LONG);
           }
        } else {
    	    MsgUtil.ToastMessage(this, Constants.DATA_FAIL, Toast.LENGTH_LONG);
        }
		
	}			
	
	private void apiParkTicketPictureCall(){
		String url = Constants.API_SERVER + "?" + Constants.METHOD +"="+ Constants.INTERFACEID_PARK_TICKET_PICTURE;
		String param = "&TICKET_NO="+ticketNo
					 + "&CD_PARK="+parkCode
					 + "&"+Constants.API_FORMAT+"="+Constants.JSON_FORMAT;
		
        Log.d(TAG, " apiParkTicketPictureCall url >>> " + url + param);
		
		
		AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>();        
		cb.url(url + param).type(JSONObject.class).weakHandler(this, "parkTicketPictureCallback").redirect(true).retry(3).fileCache(false).expire(-1);
		AQuery aq = new AQuery(this);
		aq.ajax(cb); 			
	}		
	
	public void parkTicketPictureCallback(String url, JSONObject json, AjaxStatus status){
		
		Log.d(TAG, " parkTicketPictureCallback  json ====== " +json);
		
		JSONArray jsonArr = null;
		
		// successful ajax call          
	    if(json != null){   
	       if("1".equals(json.optString("CD_RESULT"))){
	    	   if(pictureList != null){
	    		   pictureList.clear();
	    	   } else {
	    		   pictureList = new ArrayList<PictureInfo>();
	    	   }
	    	   
			   jsonArr = json.optJSONArray("PICTURE_LIST");
			   String pictureURL = "";
			   int Len = jsonArr.length();
			   for(int i=0; i<Len; i++){
				  try {
					JSONObject res = jsonArr.getJSONObject(i);
					String filePath = res.optString("FILE_PATH");
					String fileName = res.optString("FILE_NAME");
					if(i == 0){
						pictureURL = Constants.SERVER_HOST + filePath + fileName;
						Log.d(TAG, " pictureURL >>> " + pictureURL);
					}
			    	PictureInfo info = new PictureInfo();
			    	info.setPath(filePath + fileName);
			    	pictureList.add(info);
					break;
				 } catch (JSONException e) {
					// TODO Auto-generated catch block
					  if(Constants.DEBUG_PRINT_LOG){
						  e.printStackTrace();
					  }else{
						  System.out.println("예외 발생");
					  }
				 }
			  }
			   
			  if(!Util.isEmpty(pictureURL)){
				  ImageLoader.setImageUrlView(TicketSignActivity.this, pictureURL, iv_image);
			  }
	       } else {
	    	   iv_image.setImageDrawable(null);
	       }
	    } else {
	    	iv_image.setImageDrawable(null);
	    	MsgUtil.ToastMessage(TicketSignActivity.this, Constants.DATA_FAIL, Toast.LENGTH_LONG);
	    }            
		
	}	
	
	public class calendarDlg extends Dialog
	{

		public class myCalendar extends CalendarInfo
		{
			public myCalendar(Context context, LinearLayout layout) 
			{
				super(context, layout);
			}

			@Override
			public void myClickEvent(int year, int month, int day) 
			{
				
				setDateView(year, month, day);
				
				setPayment();
				
				calendarDlg.this.cancel() ;
				super.myClickEvent(year, month, day);
			}
		}

		TextView tvs[] ;
		Button btns[] ;

		public calendarDlg( Context context ) 
		{

			super(context,android.R.style.Theme_Holo_Light_Dialog_NoActionBar);
			setContentView(R.layout.calendar_layout);

			LinearLayout lv = (LinearLayout)findViewById( R.id.calendar_ilayout ) ;

			tvs = new TextView[3] ;
			tvs[0] = (TextView)findViewById( R.id.tv1 ) ;
			tvs[1] = (TextView)findViewById( R.id.tv2 ) ;
			tvs[2] = null ;

			btns = new Button[4] ;
			btns[0] = null ;
			btns[1] = null ;
			btns[2] = (Button)findViewById( R.id.Button03 ) ;
			btns[3] = (Button)findViewById( R.id.Button04 ) ;

			myCalendar cal = new myCalendar( context, lv ) ;

			cal.setControl( btns ) ;
			cal.setViewTarget( tvs ) ;

			cal.initCalendar( btn_cal_st.getText().toString().replace("-","") ) ;
		}
	}
	
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}
}
