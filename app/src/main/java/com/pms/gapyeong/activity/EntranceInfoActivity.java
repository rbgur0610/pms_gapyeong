package com.pms.gapyeong.activity;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.pms.gapyeong.R;
import com.pms.gapyeong.common.Constants;
import com.pms.gapyeong.common.DeviceInfo;
import com.pms.gapyeong.common.ImageLoader;
import com.pms.gapyeong.common.MsgUtil;
import com.pms.gapyeong.common.Preferences;
import com.pms.gapyeong.common.Util;
import com.pms.gapyeong.database.CodeHelper;
import com.pms.gapyeong.pay.PayData;
import com.pms.gapyeong.vo.ExitPaymentData;
import com.pms.gapyeong.vo.ParkIO;
import com.pms.gapyeong.vo.ParkOut;
import com.pms.gapyeong.vo.ReceiptType13Vo;
import com.pms.gapyeong.vo.ReceiptType4Vo;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

public class EntranceInfoActivity extends BaseActivity {

	private Button btn_top_left;
	private TextView tv_top_title;
	
	private Button btnOutCommit;
	private Button btnPrintbill;
	private Button btnEntranceCancel;
	
	private Button btn_car_no_1;
	private Button btn_car_no_2;
	private Button btn_car_no_3;
	private Button btn_car_no_4;
	private Button btn_car_no_manual;		
	
	private EditText etPhoneNumber;
	private Button btnParkPosition;
	
	private TextView tvParkType;
	private TextView tvChargeType;
	private TextView tvChargeDiscount;
	
	private TextView tvParkInTime;
	private TextView tvParkOutTime;
	private TextView tvEntrance;
	private TextView tvParkPayment;
	private TextView tvDiscountAmt;
	private TextView tvUnPayment;
	private TextView tvPrePayment;
	private TextView tvPaymentAmt;
	private TextView tvPaymentAmtMinus;
	
	private ImageView iv_image;
	
	private TextView tvExitCarInfo;
	
	private ParkIO parkIO;
	private String exitTime;
	
	private String parkIoName;
	private String parkIoCode;
	
	private Dialog dialog;
	
	private CodeHelper cch;
	
	private ReceiptType4Vo type4Vo;
	
	private String search_date;
	
	private String discountAmt = "0";  // 할인금액
	private int    truncAmt = 0;
	
	private String couponData;
	
	private String carNo      = "";
	
	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.entrance_info_layout);
	}

	@Override
	protected void initLayoutSetting() {
		// TODO Auto-generated method stub
		super.initLayoutSetting();
		
		btn_top_left = (Button) findViewById(R.id.btn_top_left);
		btn_top_left.setVisibility(View.VISIBLE);
		btn_top_left.setOnClickListener(this);
		
		tv_top_title = (TextView) findViewById(R.id.tv_top_title);
		tv_top_title.setText("입차정보");
		
		btnOutCommit = (Button) findViewById(R.id.btnOutCommit);
		btnPrintbill = (Button) findViewById(R.id.btnPrintbill);
		btnEntranceCancel = (Button) findViewById(R.id.btnEntranceCancel);
		
		btn_car_no_1 = (Button) findViewById(R.id.btn_car_no_1);
		btn_car_no_2 = (Button) findViewById(R.id.btn_car_no_2);
		btn_car_no_3 = (Button) findViewById(R.id.btn_car_no_3);
		btn_car_no_4 = (Button) findViewById(R.id.btn_car_no_4);
		btn_car_no_manual = (Button) findViewById(R.id.btn_car_no_manual);				
		
		etPhoneNumber 	  = (EditText) findViewById(R.id.etPhoneNumber);
		btnParkPosition   = (Button) findViewById(R.id.btnParkPosition);
		
		tvParkType 		  = (TextView) findViewById(R.id.tvParkType);
		tvChargeType 	  = (TextView) findViewById(R.id.tvChargeType);
		tvChargeDiscount  = (TextView) findViewById(R.id.tvChargeDiscount);
		
		tvParkInTime 	  = (TextView) findViewById(R.id.tvParkInTime);
		tvParkOutTime 	  = (TextView) findViewById(R.id.tvParkOutTime);
		tvEntrance 		  = (TextView) findViewById(R.id.tvEntrance);
		tvParkPayment 	  = (TextView) findViewById(R.id.tvParkPayment);
		tvDiscountAmt	  = (TextView) findViewById(R.id.tvDiscountAmt);		
		tvUnPayment  	  = (TextView) findViewById(R.id.tvUnPayment);
		tvPrePayment 	  = (TextView) findViewById(R.id.tvPrePayment);
		tvPaymentAmt 	  = (TextView) findViewById(R.id.tvPaymentAmt);
		tvPaymentAmtMinus = (TextView) findViewById(R.id.tvPaymentAmtMinus);
		
		iv_image 		  = (ImageView) findViewById(R.id.iv_image);
		
		tvExitCarInfo 	  = (TextView) findViewById(R.id.tvExitCarInfo);
		
		btnOutCommit.setOnClickListener(this);
		btnPrintbill.setOnClickListener(this);
		btnEntranceCancel.setOnClickListener(this);
		tvUnPayment.setOnClickListener(this);
		
		// 입차취소 가능여부 체크
		if(Constants.isParkInCancel){
			btnEntranceCancel.setEnabled(true);
		} else {
			btnEntranceCancel.setEnabled(false);
		}		
		
		cch = CodeHelper.getInstance(this);
	
		tvParkPayment.setTag("0");
		
		Intent intent = getIntent();
		
		search_date = intent.getStringExtra("search_date");
		
		if(Util.isEmpty(search_date)){
			MsgUtil.AlertDialog(EntranceInfoActivity.this, "날짜정보가 없습니다.");
			return;
		}
		
		if (intent != null) {
			String title = intent.getStringExtra("title");
			if (title != null || !"".equals(title)) {
				tv_top_title.setText(title);
			}
					
			parkIO = (ParkIO) intent.getSerializableExtra("parkIO");
			
			
			if("2".equals(parkClass)){
				setCommonItem("AM2", Constants.COMMON_TYPE_CHARGE_TYPE, tvChargeType, "M2-1");
			} else if("3".equals(parkClass)){
				setCommonItem("AM3", Constants.COMMON_TYPE_CHARGE_TYPE, tvChargeType, "M3-1");
			} else if("4".equals(parkClass)){
				setCommonItem("AM4", Constants.COMMON_TYPE_CHARGE_TYPE, tvChargeType, "M4-1");
			} else if("5".equals(parkClass)){
				setCommonItem("AM5", Constants.COMMON_TYPE_CHARGE_TYPE, tvChargeType, "M5-1");
			} else if("6".equals(parkClass)){
				setCommonItem("AM6", Constants.COMMON_TYPE_CHARGE_TYPE, tvChargeType, "M6-1");
			}
			
			carNo = Util.isNVL(parkIO.getCarNo());
			setCarnoView(carNo,btn_car_no_1, btn_car_no_2, btn_car_no_3, btn_car_no_4, btn_car_no_manual);
			
			parkIoName = intent.getStringExtra("parkIoName");
			parkIoCode = intent.getStringExtra("parkIoCode");
			
			etPhoneNumber.setText(parkIO.getTel());
			btnParkPosition.setText(parkIO.getCdArea() + "면");
			
			tvParkType.setText(cch.findCodeByCdCode("PY", parkIO.getParkType()));
			tvChargeDiscount.setText(cch.findCodeByCdCode("DC", parkIO.getDisCd()));
			
			tvParkInTime.setText(Util.dateString(parkIO.getParkInDay()));
			
			// 선불금
			tvPrePayment.setText(Util.isNVL(parkIO.getAdvAmt(),"0") +"원");

			Log.i(TAG, "parkIoCode : " + parkIoCode);
			if ("I0".equals(parkIoCode)) { // 입차취소
				Log.d(TAG, " ParkOutDay >>> " + parkIO.getParkOutDay());
				String exitDay  = parkIO.getParkOutDay().replaceAll("-", "").replaceAll(":", "").replaceAll(" ", "");
				String exitDate = exitDay.substring(0,8);
				if(parkIO.getParkOutDay().length() >= 12){
					exitTime = parkIO.getParkOutDay().replaceAll("-", "").replaceAll(":", "").replaceAll(" ", "").substring(8, 12);
				}
				tvParkOutTime.setText(Util.dateString(exitDate + " " + exitTime.substring(0, 2) + ":" + exitTime.substring(2, 4)));
			} else { // 입차 중, 출창완료
				String exitDate = Util.getYmdhms("yyyyMMdd");
				exitTime = Util.getYmdhms("HHmm");
				tvParkOutTime.setText(Util.dateString(exitDate + " " + exitTime.substring(0, 2) + ":" + exitTime.substring(2, 4)));
			}
			
			setParkAmt();			
			
			apiParkIOPictureCall();
			
			tvExitCarInfo.setText(parkIoName);			
			
			apiUnpayAttach(parkIO.getCarNo());
		}
	}
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		super.onClick(v);
		switch (v.getId()) {
		case R.id.btn_top_left:
			finish();
			break;

		case R.id.btnOutCommit:
			
			int charge = Integer.parseInt(tvPaymentAmt.getText().toString().replace(",", "").replace("원", ""));
			
			type4Vo = new ReceiptType4Vo(Util.getYmdhms("yyyy-MM-dd HH:mm:ss")	// 출력일자
								, tvParkType.getText().toString()			// 주차종류 :
								, tvEntrance.getText().toString()			// 주차시간 :
								, tvEntrance.getText().toString()			// 주차시간 :
								, tvChargeDiscount.getText().toString()		// 할인종류 :
								, tvParkInTime.getText().toString()			// 입차시간 :
								, tvParkOutTime.getText().toString()		// 출차시간 :
								, ""+charge									// 납부금액 :
								, tvPrePayment.getText().toString().replace(",", "").replace("원", "")		// 선납금액 :
								, tvParkPayment.getText().toString().replace(",", "").replace("원", "")		// 주차요금 :
								, tvUnPayment.getText().toString().replace(",", "").replace("원", "")		// 미납금액 :
								, tvDiscountAmt.getText().toString().replace(",", "").replace("원", "")		// 할인금액 :
								, BIZ_NO				// 사업자번호
								, BIZ_NAME
								, BIZ_TEL);
			
			type4Vo.setTitle("출차영수증");
			
			type4Vo.setPioNum(parkIO.getPioNum());
			type4Vo.setCarNum(carNo);
			type4Vo.setParkName(parkName);
			type4Vo.setEmpName(empName);
			type4Vo.setEmpPhone(empPhone);
			type4Vo.setEmpTel(empTel);
			type4Vo.setEmpBusiness_Tel(empBusiness_TEL);
			type4Vo.setOutPrint(Preferences.getValue(this,Constants.PREFERENCE_OUT_PRINT,""));
			exitDialog(tvParkPayment.getText().toString()
					  , tvPaymentAmt.getText().toString()
					  , type4Vo
					  ,tvUnPayment.getText().toString().replace(",", "").replace("원", ""));
			break;

		case R.id.btnPrintbill:
			try {
				String workTime = workStartTime.substring(0, 2)
						+ ":"
						+ workStartTime.substring(2)
						+ "~"
						+ workEndTime.substring(0, 2)
						+ ":"
						+ workEndTime.substring(2);
				
				ReceiptType13Vo	type13Vo = new ReceiptType13Vo("입차영수증"	// 타이틀
						, Util.getYmdhms("yyyy-MM-dd HH:mm:ss")	 // 출력일자
						, btnParkPosition.getText().toString().replaceAll("면", "")	// 주차면
						, tvParkType.getText().toString()		 // 주차종류
						, tvChargeType.getText().toString()		 // 요금종류
						, tvChargeDiscount.getText().toString()	 // 요금할인
						, tvUnPayment.getText().toString().replaceAll("원", "")		// 총미수금액
						, tvPrePayment.getText().toString().replaceAll("원", "")		// 선납금액
						, tvParkInTime.getText().toString()			// 입차시간 :
						, tvParkOutTime.getText().toString()		// 출차시간 :
						, tvEntrance.getText().toString()	// 총 분
						, BIZ_NO					// 사업자번호
						, BIZ_NAME					// 사업자명
						, BIZ_TEL, empTicket_print, workTime);					// 사업자연락처
				
				type13Vo.setPrintBill(true);
				type13Vo.setPioNum(parkIO.getPioNum());
				type13Vo.setCarNum(carNo);
				type13Vo.setParkName(parkName);
				type13Vo.setEmpName(empName);
				type13Vo.setEmpPhone(empPhone);
				type13Vo.setEmpTel(empTel);
				type13Vo.setAccount_No(empAccount_No);
				type13Vo.setBank_MSG(empBank_MSG);
				type13Vo.setKN_ACCOUNT(empKN_ACCOUNT);
				type13Vo.setKN_BANK(empKN_BANK);
				type13Vo.setEmpBusiness_Tel(empBusiness_TEL);
				type13Vo.setApprovalNo(parkIO.getApprovalNo());
				type13Vo.setCardNo(parkIO.getCardNo());
				type13Vo.setCardCompany(parkIO.getCardCompany());
				type13Vo.setOutPrint(Preferences.getValue(this,Constants.PREFERENCE_OUT_PRINT,""));
				PayData payData = new PayData(this, OUT_PRINT, IN_PRINT, TICKET_PRINT);
				payData.printDefault(PayData.PAY_DIALOG_TYPE_ENTRANCE_TIME, type13Vo, mPrintService);
				payData.ImagePrint(empLogo_image);
				try
				{
					  payData.printText("\n\n");
				}
				catch(UnsupportedEncodingException e)
				{
					if(Constants.DEBUG_PRINT_LOG){
						e.printStackTrace();
					}else{
						System.out.println("예외 발생");
					}
				}

			} catch (NullPointerException e) {
				if(Constants.DEBUG_PRINT_LOG){
					e.printStackTrace();
				}else{
					System.out.println("예외 발생");
				}
			}
			break;

		case R.id.btnEntranceCancel:
			// TODO 결제 취소 처리, 일일권 또는 선불권일 경우 입차 취소시 결제 취소를 해야 한다.
			if ("02".equals(cch.findCodeByKnCode("PY", tvParkType.getText().toString()))
				|| "04".equals(cch.findCodeByKnCode("PY", tvParkType.getText().toString()))) {
				
				if (!Util.isEmpty(parkIO.getApprovalNo()) && !Util.isEmpty(parkIO.getCardCompany())) {
					// 카드 결제 취소
					dialog = entranceTimeCancelDialog("Card");
				} else {
					// 현금 결제 취소
					dialog = entranceTimeCancelDialog("NonCard");
				}
			} else {
				dialog = entranceTimeCancelDialog("NonCard");
			}
			break;

		case R.id.tvUnPayment:
			// 미수 정보일 경우에는 미수환수 리스트로 가지 않는다.
			if ("O2".equals(parkIoCode)) {
				return ;
			}
			if (!tvUnPayment.getText().toString().replaceAll("원", "").trim().equals("0")) {
				goToUnpaidManagerActivity(carNo);
			}
			break;
		}
	}
	
	@Override
	protected void request(int type, Object data) {
		if (type == PayData.PAY_DIALOG_TYPE_COUPON) {
			couponData = (String) data;
		} else if (type == PayData.PAY_DIALOG_TYPE_EXIT_CASH || type == PayData.PAY_DIALOG_TYPE_EXIT_CARD) {
			ExitPaymentData epd = (ExitPaymentData)data;
			apiParkOutCall(epd);
		}
		
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode,
			Intent intent) {
		super.onActivityResult(requestCode, resultCode, intent);
		
		switch (requestCode) {
		
		  case PayData.PAY_DIALOG_CANCEL_ENTRANCE_TIME:
			 if (resultCode == PayData.PAY_DIALOG_CANCEL_ENTRANCE_TIME) {
				apiParkIoCancelCall(edCancelReason.getText().toString());
			 }
			break;
			
		}
	}
	
	
	void setParkAmt() {
		
		// 할인금액 초기화
		discountAmt = "0";
		
		// 절삭금액 초기화
		truncAmt    = 0;

		String date1 = tvParkInTime.getText().toString()  + "00";
		String date2 = tvParkOutTime.getText().toString() + "00";
		
		
		//###############################################################
		//  STEP 1 : 주차 시간 계산
		//###############################################################
		Log.d(TAG, "date1 : " + date1 + ", date2 : " + date2);
		String parkMin = minuteCalc(date1, date2);
		Log.d(TAG, " parkMin >>> " + parkMin);

		
		//###############################################################
		//  STEP 2 : 할인 시간 계산
		//###############################################################
		// 요금할인 정보 
		String discountName = tvChargeDiscount.getText().toString();
		
		// 할인시간 설정
		int disTime  = Util.sToi(cch.findCodeByDisTime("DC", discountName),0);
		if(disTime > 0){
//			Log.d(TAG, "parkMin 2 : " + parkMin);
			parkMin 	= ""+(Util.sToi(parkMin,0) - disTime);
//			Log.d(TAG, "parkMin 3 : " + parkMin);
			if((Util.sToi(parkMin,0) < 0)){
//				Log.d(TAG, "parkMin 4 : " + parkMin);
				parkMin 	= "0";
			}
		}			
		
		// 주차시간 표시 
		tvEntrance.setText(minuteView(parkMin));			
		
		// 입차취소 일 경우
		if ("I0".equals(parkIoCode)) { 
			tvParkPayment.setText("0원");
			tvDiscountAmt.setText("0원");
			tvPaymentAmt.setText("0원");
			tvPaymentAmtMinus.setText("0원");
			
			btnOutCommit.setEnabled(false);
			btnPrintbill.setEnabled(false);
			btnEntranceCancel.setEnabled(false);			
			return;
		}
		
		
		//###############################################################
		//  STEP 3 : 할인 금액 계산
		//###############################################################
		
		// 주차종류
		String parkTypeCode = cch.findCodeByKnCode("PY", tvParkType.getText().toString());

		// 요금종류코드
		String chargeCode = "";
		
		if("2".equals(parkClass)){
			chargeCode = cch.findCodeByKnCode("AM2", tvChargeType.getText().toString());
		} else if("3".equals(parkClass)){
			chargeCode = cch.findCodeByKnCode("AM3", tvChargeType.getText().toString());
		} else if("4".equals(parkClass)){
			chargeCode = cch.findCodeByKnCode("AM4", tvChargeType.getText().toString());
		} else if("5".equals(parkClass)){
			chargeCode = cch.findCodeByKnCode("AM5", tvChargeType.getText().toString());
		} else if("6".equals(parkClass)){
			chargeCode = cch.findCodeByKnCode("AM6", tvChargeType.getText().toString());
		}
		
		
		// 주차금액 계산
		String total  = parkCharge(parkMin, chargeCode, parkTypeCode);
		String charge = "0";
		
		if ("02".equals(parkTypeCode)) {  // 일일권
			total  		= "0";  // 일일권은 출차시 0원 
			charge	    = "0";
			discountAmt = "0";  // 일일권은 할인을 하지 않는다.
			truncAmt    = 0;
			// 과금 된 실제 주차금액 표시
			tvParkPayment.setText(total + "원");
			// 할인금액 표시
			tvDiscountAmt.setText(discountAmt + "원");			
		} else if ("03".equals(parkTypeCode)) { // 정기권 
			total  		= "0";  // 정기권은 출차시 0원
			charge      = "0"; 
			discountAmt = "0";  // 정기권은 할인을 하지 않는다.
			truncAmt    = 0;
			// 과금 된 실제 주차금액 표시
			tvParkPayment.setText(total + "원");
			// 할인금액 표시
			tvDiscountAmt.setText(discountAmt + "원");			
		} else {
			
		   // 과금 된 실제 주차금액 표시
		   tvParkPayment.setText(total + "원");
			
		   if ("04".equals(parkTypeCode)) { // 시간권 선불 : 총 주차요금에서 이전에 할인 받은 금액 차감
				Log.d(TAG, " total as is >>> " + total);
				total = "" + (Util.sToi(total) - Util.sToi(parkIO.getAdvInAmt()) - Util.sToi(parkIO.getDisInAmt()) - Util.sToi(parkIO.getTruncInAmt())); 
				Log.d(TAG, " total to be >>> " + total);
		   }
		   
		   // 주차금액 셋팅 
		   charge = total;
			
		   if(Util.sToi(total) > 0){
			   
			   int tempDisAmt1 = 0;
			   int tempDisAmt2 = 0;
				
			   // 할인금액 조회
			   String disAmt = Util.isNVL(cch.findCodeByDisAmt("DC", discountName),"0");
			   if(!"0".equals(disAmt)){
					// 할인금액 설정
					tempDisAmt1 = Util.sToi(disAmt);
					charge = "" + (Util.sToi(charge) - tempDisAmt1);
					Log.d(TAG, "setParkAmt ::: charge >>> " + charge + "  tempDisAmt1 : " + tempDisAmt1);
			   }
				
			   // 할인률 조회
			   String disRate = cch.findCodeByDisRate("DC", discountName);
			   if(!"0".equals(disRate)){
					// 할인률에 따른 할인금액 설정
					tempDisAmt2 = Util.sToi(Util.isNVL(""+discountChargeRate(charge, disRate),"0"));
					charge = "" + (Util.sToi(charge) - tempDisAmt2);
				   	Log.d(TAG, "setParkAmt ::: charge >>> " + charge + "  tempDisAmt2 : " + tempDisAmt2);
			   }					
			   
			   // 총 할인금액
			   discountAmt = ""+(tempDisAmt1 + tempDisAmt2);
			   Log.d(TAG, " setParkAmt >>> charge : " + charge + " discountAmt : " + discountAmt);

			   // 할인금액 표시
			   if ("04".equals(parkTypeCode)) {  // 시간권 선불 
				   tvDiscountAmt.setText((Util.sToi(parkIO.getDisInAmt()) + Util.sToi(discountAmt)) + "원");
				   // 할인금액 = 할인금액 - 입차 시 절삭금액
				   discountAmt = "" + (Util.sToi(discountAmt) - Util.sToi(parkIO.getTruncInAmt()));
				   // 주차금액 = 주차금액 + 입차 시 절삭금액
				   charge 	   = "" + (Util.sToi(charge) + Util.sToi(parkIO.getTruncInAmt()));
			   } else {
				   tvDiscountAmt.setText(discountAmt + "원");
			   }
			   
			   if(Util.sToi(charge,0) < 0){
				  charge = "0";
			   }
		   } else { // 환불 발생 건
			   
			  // 할인률 조회
			  String disRate = cch.findCodeByDisRate("DC", discountName);
			   
			  // 할인률 100% 적용이면
			  if("100".equals(disRate)){
				  charge	  = "0";
			 	  discountAmt = total;  
			  } else {
				  if(!"0".equals(disRate)){
					// 할인률에 따른 할인금액 설정
					discountAmt = Util.isNVL(""+discountChargeRate(total, disRate),"0");
				  }
				  
				  Log.d(TAG, " setParkAmt >>> return total  : " + total  + " discountAmt : " + discountAmt);
				  charge = Util.toStr((Util.sToi(total) - Util.sToi(discountAmt)));
				  Log.d(TAG, " setParkAmt >>> return charge : " + charge + " discountAmt : " + discountAmt);
				  
			  }
			  
			  // 할인금액 표시 - 환불은 선불권에서만 발생으로  입차 시 할인금액 합산(차감)
			  tvDiscountAmt.setText((Util.sToi(parkIO.getDisInAmt()) + Util.sToi(discountAmt)) + "원");
			  
		   }  // end if total 			   
	    } // end if parkTypeCode
		
		Log.d(TAG, " total  <<<>>> " + total);
		Log.d(TAG, " charge <<<>>> " + charge);
		Log.d(TAG, " discountAmt  <<<>>> " + discountAmt);				
		
		// 주차원금 셋팅
		tvParkPayment.setTag(total);
		
		
		//###############################################################
		//  STEP 4 : 납부 금액 계산
		//###############################################################		
		
		int payment = 0;
		
		// 일일권 또는 정기권일 경우 주차요금 및 납부금액은 계산하지 않는다.
		if ("02".equals(parkTypeCode) || "03".equals(parkTypeCode)) {
			tvParkPayment.setText("0원");
			tvDiscountAmt.setText("0원");
			tvPaymentAmt.setText("0원");
			tvPaymentAmtMinus.setText("0원");
		} else { // 시간권 후불 또는 선불
			// 납부금액 = 주차요금
			payment = Util.sToi(charge);
		}
		
		
		//###############################################################
		//  STEP 5 : 절삭 금액 계산
		//###############################################################			
		
		if (!"0".equals(truncUnit)) {
			truncAmt = payment % Integer.parseInt(truncUnit);
		} else {
			truncAmt = 0;
		}

		//** 과천시 할인금액이 없으면 절삭 적용
		// 주차할인코드
		String disCode = cch.findCodeByKnCode("DC", discountName);
		if("01".equals(disCode)){
			payment = payment - truncAmt;
		} else { // 할인금액이 있으면 올림 적용
			if(truncAmt > 0){
				// 납부금액은 절삭 올림 적용
				payment = payment + (Util.sToi(truncUnit) - truncAmt);
				// 할인금액은 절삭 차감 
				discountAmt = "" + (Util.sToi(discountAmt) - (Util.sToi(truncUnit) - truncAmt));
				truncAmt = 0;
			}
		}		
		
		// 절삭금액은 +금액으로 표시(미환불 금액)
		truncAmt = Math.abs(truncAmt);
		
		/**zzz
		// 절삭금액 표시
		// 총 금액 = 표시 된 주차요금 - 표시 된 할인금액 - 표시 된 선납금액 
		int tvTotalAmt = Util.sToi(tvParkPayment.getText().toString().replaceAll("원", "")) 
				       - Util.sToi(tvDiscountAmt.getText().toString().replaceAll("원", ""))
				       - Util.sToi(tvPrePayment.getText().toString().replaceAll("원", ""));
		int tvTruncAmt = 0;
		if (!"0".equals(truncUnit)) {
			tvTruncAmt = tvTotalAmt % Util.sToi(truncUnit);
		} else {
			tvTruncAmt = 0;
		}
		tvPaymentAmtMinus.setText(tvTruncAmt + "원");
		tvPaymentAmt.setText(payment + "원");	
		***/
		
		tvPaymentAmtMinus.setText(truncAmt + "원");
		tvPaymentAmt.setText(payment + "원");	
		
	}	
	
	private EditText edCancelReason;
	private ParkOut out;
	public Dialog entranceTimeCancelDialog(final String type) {
  		final Dialog cancelDialog;
  		
  			cancelDialog = new Dialog(this,
  					android.R.style.Theme_Holo_Light_Dialog);

  		cancelDialog.setContentView(R.layout.entrance_time_cancel_dialog);
  		cancelDialog.setTitle("입차취소");
  		
  		final TextView reason = (TextView) cancelDialog.findViewById(R.id.cancel_reason);
  		setCommonItem("C1", Constants.COMMON_TYPE_CANCEL, reason, 0);
  		edCancelReason = (EditText)cancelDialog.findViewById(R.id.etc_reason);
  		reason.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				setCommonDialog("취소사유","C1", Constants.COMMON_TYPE_CANCEL, reason);
			}
		});
  		
  		((Button)cancelDialog.findViewById(R.id.btn_ok)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(type.equals("Card"))
				{
					String workTime = workStartTime.substring(0, 2)
							+ ":"
							+ workStartTime.substring(2)
							+ "~"
							+ workEndTime.substring(0, 2)
							+ ":"
							+ workEndTime.substring(2);
					
					ReceiptType13Vo	type13Vo = new ReceiptType13Vo("입차취소영수증",	// 타이틀
							Util.getYmdhms("yyyy-MM-dd HH:mm:ss"),	// 출력일자
							parkIO.getCdArea(),		// 주차면
							tvParkType.getText().toString(),		// 주차종류
							tvChargeType.getText().toString(),		// 요금종류
							tvChargeDiscount.getText().toString(),	// 요금할인
							tvUnPayment.getText().toString().replaceAll("원", ""),		// 총미수금액
							tvPrePayment.getText().toString().replaceAll("원", ""),		// 선납금액
							tvParkInTime.getText().toString(),		// 입차시간
							tvParkOutTime.getText().toString(),		// 출차예정시간
							tvEntrance.getText().toString(),		// 총 분
							BIZ_NO,						// 사업자번호
							BIZ_NAME,					// 사업자명
							BIZ_TEL,empTicket_print,workTime);					// 사업자연락처
					
					type13Vo.setPioNum(parkIO.getPioNum());
					type13Vo.setCarNum(carNo);
					type13Vo.setApprovalNo(parkIO.getApprovalNo());
					type13Vo.setParkName(parkName);
					type13Vo.setEmpName(empName);
					type13Vo.setEmpPhone(empPhone);
					type13Vo.setEmpTel(empTel);
					type13Vo.setAccount_No(empAccount_No);
					type13Vo.setBank_MSG(empBank_MSG);
					type13Vo.setKN_ACCOUNT(empKN_ACCOUNT);
					type13Vo.setKN_BANK(empKN_BANK);
					type13Vo.setEmpBusiness_Tel(empBusiness_TEL);
					type13Vo.setOutPrint(Preferences.getValue(EntranceInfoActivity.this,Constants.PREFERENCE_OUT_PRINT,""));
					goToPayCancelActivity(PayData.PAY_DIALOG_CANCEL_ENTRANCE_TIME, parkIO.getCardAmt(), parkIO.getApprovalNo(), parkIO.getApprovalDate(), type13Vo);
				}
				else
				{
					// 현금 영수증 취소
					// goToCashReceiptCancelActivity(PayData.PAY_DIALOG_CANCEL_CASH_RECEIPT, parkIO.getCashAmt(), parkIO.getApprovalNo(), parkIO.getApprovalDate());
					apiParkIoCancelCall(edCancelReason.getText().toString());
				}
			}
		});
  		
  		((Button)cancelDialog.findViewById(R.id.btn_cancel)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				cancelDialog.dismiss();
			}
		});


  		cancelDialog.show();
  		return cancelDialog;
  	}
	
	private void apiParkOutCall(ExitPaymentData epd) {
		
		boolean isPicDel = Preferences.getValue(this, Constants.PREFERENCE_SETTING_DELETE_SERVER_IMG, false);
		
		out = new ParkOut(parkIO.getPioNum(), 		//	PIO_NUM : 주차번호
					parkIO.getCdPark(), 	//	CD_PARK: 주차장번호
					empCode, 				//	EMP_CD : 근무자코드
					search_date, 			//	WORK_DAY: 날짜
					exitTime , 				//	OUT_TIME : 출차시간(8)
					parkIO.getCarNo(), 		//	CAR_NO : 차량번호
					cch.findCodeByKnCode("PY", tvParkType.getText().toString()),		//	PARK_TYPE : 주차유형
					etPhoneNumber.getText().toString(), 				//	TEL : 연락처(변경가능)
					"O1", 				//	STATE_CD : O1 (입차 ?)
					tvPrePayment.getText().toString().replaceAll("원", "").trim(), 				//	ADV_AMT : 선불금
					"B1", 				//	BILL_TYPE : 결재방법 B1 고정
					parkIO.getDisCd(),	//	DIC_CODE : 할인코드
					tvParkPayment.getText().toString().replaceAll("원", ""), 				//	REAL_AMT : 납부금액
					"", 				//	CREDIT_NO: 카드번호
					"", 				//	APPROVAL_NO : 승인번호
					catNumber, 			//	CATNUMBER :12자리
					"0", 				//	RETURN_AMT : 환불금액
					"0", 				//	DIC_AMT : 할인금액
					"0", 				//	CASH_AMT :현금
					"0", 				//	CARD_AMT : 카드금액
					"", 				//	YET_AMT :미납금액
					""+truncAmt,		//	TRUNC_AMT:절삭금액(2013.12.06)
					"",					// YET_RETURN:미환불금액
					"",
					"", 				//	COUPON_NO : 쿠폰번호
					"",					// YET_RETURNCARD:미환불카드금액
					isPicDel);			// 서버 사진 삭제 여부
		
		if (couponData != null &&  couponData.length() > 1) {
			out.setCouponData(couponData);
		}
		
		String parkType   = cch.findCodeByKnCode("PY", tvParkType.getText().toString());
		
		out.setTruncAmt(""+truncAmt);
		out.setDicAmt(discountAmt);
		out.setRealAmt(tvParkPayment.getTag().toString());
		
		String cashApprovalNo   = "";
		String cashApprovalDate = "";
		if (epd != null) {
			out.setTransactionNo(epd.getTransactionNo());
			out.setApprovalNo(epd.getApprovalNo());
			out.setApprovalDate(epd.getApprovalDate());
			out.setCardNo(epd.getCardNo());
			out.setCardCompany(epd.getCardCompany());
			out.setCardAmt(epd.getCardAmt());
			out.setCatnumber(epd.getCatNumber());
			out.setCashAmt(epd.getCashAmt());
			out.setReturnAmt(epd.getReturnAmt());
			out.setCouponAmt(epd.getCouponAmt());
			
			cashApprovalNo   = epd.getCashApprovalNo();
			cashApprovalDate = epd.getCashApprovalDate();			
		}
		
		/** xx 2015.02.03
		int offAmt =  Util.sToi(out.getRealAmt(),0) - Util.sToi(out.getCashAmt(),0)   - Util.sToi(out.getCardAmt(),0) 
				    - Util.sToi(out.getDicAmt(),0)  - Util.sToi(out.getReturnAmt(),0) - Util.sToi(out.getYetAmt(),0)  
				    - Util.sToi(out.getYetReturn(),0) - Util.sToi(out.getYetReturnCard(),0) - truncAmt;
		**/
		
        Map<String, Object> params = new HashMap<String, Object>();
        params.put(Constants.METHOD, 	 Constants.INTERFACEID_PARK_OUT);
        params.put(Constants.API_FORMAT, Constants.JSON_FORMAT);		
        params.put("PIO_NUM", 	out.getPioNumber());
        params.put("CD_PARK", 	out.getParkCode());
        params.put("EMP_CD", 	out.getEmpCode());
        params.put("WORK_DAY",  out.getWorkDay());
        
        // 출차일
        params.put("OUT_DATE",  Util.getYmdhms("yyyy-MM-dd"));
        // 출차시간
        params.put("OUT_TIME",  out.getOutTime());
        // 차량번호 
        params.put("CAR_NO", 	out.getCarNumber());
        // 주차종류 
        params.put("PARK_TYPE", parkType);
        // 연락처
        params.put("TEL", 		out.getTel());
        // 주차면
        params.put("CD_AREA",   btnParkPosition.getText().toString().replaceAll("면", ""));
        
        params.put("CAR_OWNER",	out.getCarOwner());
        params.put("STATE_CD",  out.getStateCode());
        params.put("YET_CD",    out.getYetCode());
        
        // 현금 영수증 관련
        params.put("CASH_APPROVAL_NO", 	 cashApprovalNo);
        params.put("CASH_APPROVAL_DATE", cashApprovalDate);
        
        // 카드 결제 관련
        params.put("TRANSACTION_NO", 	out.getTransactionNo());
        params.put("APPROVAL_NO",    	out.getApprovalNo());
        params.put("APPROVAL_DATE",  	out.getApprovalDate());
        params.put("CARD_NO",     		out.getCardNo());
        params.put("CARD_COMPANY", 	    out.getCardCompany());
        params.put("CATNUMBER",   	 	out.getCatnumber());

        params.put("REAL_AMT",    out.getRealAmt());
        params.put("CASH_AMT",    out.getCashAmt());
        params.put("CARD_AMT",    out.getCardAmt());
        
        // 출차시 서버로 선불금액을 보내지 않는다.
        params.put("ADV_AMT", 	  "0");
        
        params.put("DIS_CD",  	  cch.findCodeByKnCode("DC", tvChargeDiscount.getText().toString()));
        params.put("DIS_AMT", 	  out.getDicAmt());
        params.put("RETURN_AMT",  out.getReturnAmt());
        
        if("O2".equals(out.getStateCode())){
        	params.put("YET_AMT", out.getYetAmt());
        } else {
        	params.put("YET_AMT", "");
        }
	        
        params.put("TRUNC_AMT", 	 out.getTruncAmt());
        
//      xx 2015.02.03
//      params.put("OFF_AMT",        offAmt);        
        
        params.put("YET_RETURN", 	 out.getYetReturn());
        params.put("YET_RETURNCARD", out.getYetReturnCard());
        
        params.put("COUPON_AMT", 	 out.getCouponAmt());
        params.put("COUPON_DATA", 	 out.getCouponData());
        
        params.put("KN_COMMENT", 	 out.getComment());
        params.put("PICTURE_DEL", (out.isDeletePicture() ? "Y" : "N"));        
        
        showProgressDialog(false);
        
		String url = Constants.API_SERVER;
		Log.d(TAG, " apiParkOutCall url >>> " + url + " " + params);
		
		AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>();        
		cb.url(url).params(params) .type(JSONObject.class).weakHandler(this, "parkOutCallback").redirect(true).retry(3).fileCache(false).expire(-1);
		AQuery aq = new AQuery(this);
		aq.ajax(cb);
		
	}
	
	public void parkOutCallback(String url, JSONObject json, AjaxStatus status){
		
		closeProgressDialog();
		
		Log.d(TAG, " parkOutCallback  json ====== " +json);
		
		// successful ajax call          
        if(json != null){   
           String KN_RESULT = json.optString("KN_RESULT");
           if("1".equals(json.optString("CD_RESULT"))){
    		  // TODO 결제 후
    		  if (dialog != null) {
    			 dialog.dismiss();
    		  }

			  boolean isDeleteIo = Preferences.getValue(this, Constants.PREFERENCE_SETTING_DELETE_IO_IMG, false);
			  if (isDeleteIo) {
				  // 사진 삭제
				   deletePicture(carNo);
			  }
			  
			  // 현금 출차 시 영수증 출력
			  if(Util.isEmpty(out.getTransactionNo())){
				  type4Vo.setTitle("출차영수증");
				  type4Vo.setCouponAmt(out.getCouponAmt());	 
  				  PayData payData = new PayData(this, OUT_PRINT, IN_PRINT, TICKET_PRINT);
  				  payData.printDefault(PayData.PAY_DIALOG_TYPE_EXIT_ONLY, type4Vo, mPrintService);
  				  
				  payData.ImagePrint(empLogo_image);
				  try
				  {
					  payData.printText("\n\n");
				  }
				  catch(UnsupportedEncodingException e)
				  {
					  if(Constants.DEBUG_PRINT_LOG){
						  e.printStackTrace();
					  }else{
						  System.out.println("예외 발생");
					  }
				  }
  			  }
  			  
  			  MsgUtil.ToastMessage(this, KN_RESULT);
  			  finish();
           } else {
        	  MsgUtil.ToastMessage(this, KN_RESULT);
           }
        } else {
    	    MsgUtil.ToastMessage(this, Constants.DATA_FAIL, Toast.LENGTH_LONG);
        }
		
	}		
	
	private void apiParkIoCancelCall(String reason) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put(Constants.METHOD, 	 Constants.INTERFACEID_PARK_IN_CANCEL);
        params.put(Constants.API_FORMAT, Constants.JSON_FORMAT);		
        params.put("PIO_NUM", 	parkIO.getPioNum());
        params.put("CAR_NO", 	parkIO.getCarNo());
        params.put("CD_PARK", 	parkIO.getCdPark());
        params.put("EMP_CD", 	empCode);
        params.put("WORK_DAY",  search_date);
        params.put("COMMENT",   reason);
        params.put("CD_STATE", 	"IC");
		
		String url = Constants.API_SERVER;
		Log.d(TAG, " apiParkIoCancelCall url >>> " + url + " " + params.toString());
		
		AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>();        
		cb.url(url).params(params).type(JSONObject.class).weakHandler(this, "parkIoCancelCallback").redirect(true).retry(3).fileCache(false).expire(-1);
		AQuery aq = new AQuery(this);
		aq.ajax(cb);		
	}		
	
	public void parkIoCancelCallback(String url, JSONObject json, AjaxStatus status){
		Log.d(TAG, " parkIoCancelCallback  json ====== " +json);
		
		// successful ajax call          
	    if(json != null){   
	       String KN_RESULT = json.optString("KN_RESULT");
	       if("1".equals(json.optString("CD_RESULT"))){
	    	   MsgUtil.ToastMessage(this, KN_RESULT, Toast.LENGTH_LONG);
			   if (dialog != null) {
				   dialog.dismiss();
			   }
			   finish();
	       } else {
	    	   MsgUtil.ToastMessage(this, KN_RESULT, Toast.LENGTH_LONG);
	       }
	    } else {
	    	MsgUtil.ToastMessage(this, Constants.DATA_FAIL, Toast.LENGTH_LONG);
	    }
	}			
	
	/**
	 * 차량 미납 조회
	 */
	private void apiUnpayAttach(String carNo){
		showProgressDialog(false);
		String url = Constants.API_SERVER + "?" + Constants.METHOD +"="+ Constants.INTERFACEID_PARK_UNPAYATTACH_V2;
		String param = "&CD_PARK="+parkCode
					 + "&CAR_NO="+carNo
					 + "&"+Constants.API_FORMAT+"="+Constants.JSON_FORMAT;
		Log.d(TAG, " apiUnpayAttach url >>> " + url + param);
		
		AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>();        
		cb.url(url+param).type(JSONObject.class).weakHandler(this, "unpayAttachCallback").redirect(true).retry(3).fileCache(false).expire(-1);
		AQuery aq = new AQuery(this);
		aq.ajax(cb);		
	}		
	
	public void unpayAttachCallback(String url, JSONObject json, AjaxStatus status){
		
		closeProgressDialog();
		Log.d(TAG, " unpayAttachCallback  json ====== " +json);
		
		JSONArray jsonArr = null;
		
		// successful ajax call          
		if(json != null){   
			String KN_RESULT = json.optString("KN_RESULT");
			if("1".equals(json.optString("CD_RESULT"))){
			   jsonArr = json.optJSONArray("RESULT");
			   int unpayAmt = 0;
			   int Len = jsonArr.length();
			   try {
				   for(int i=0; i<Len; i++){
					    JSONObject res = jsonArr.getJSONObject(i);
						String CD_STATE = res.optString("CD_STATE");
						if ("Y".equalsIgnoreCase(CD_STATE)) {
							Toast.makeText(this, "압류 대상 차량 입니다.", Toast.LENGTH_LONG).show();
						}				
						unpayAmt += res.optInt("AMT",0);
				    } // end for 
				   
					// 미납금 설정
					tvUnPayment.setText(unpayAmt+"원");
				   
				} catch (JSONException e) {
				   if(Constants.DEBUG_PRINT_LOG){
					   e.printStackTrace();
				   }else{
					   System.out.println("예외 발생");
				   }
				}			   
			} else {
			//	MsgUtil.ToastMessage(this, KN_RESULT, Toast.LENGTH_LONG);
				tvUnPayment.setText("0원");
			}
		} else {
			MsgUtil.ToastMessage(this, Constants.DATA_FAIL, Toast.LENGTH_LONG);
		}
		
	}			
	
	private void apiParkIOPictureCall(){
		if(parkIO != null){
			String url = Constants.API_SERVER + "?" + Constants.METHOD +"="+ Constants.INTERFACEID_PARK_IO_PICTURE;
			String param = "&PIO_NUM="+parkIO.getPioNum()
						 + "&CD_PARK="+parkIO.getCdPark()
						 + "&"+Constants.API_FORMAT+"="+Constants.JSON_FORMAT;
	        Log.d(TAG, " apiParkIOPictureCall url >>> " + url + param);
	        
			AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>();        
			cb.url(url + param).type(JSONObject.class).weakHandler(this, "parkIOPictureCallback").redirect(true).retry(3).fileCache(false).expire(-1);
			AQuery aq = new AQuery(this);
			aq.ajax(cb); 	        
		}
	}		
	
	public void parkIOPictureCallback(String url, JSONObject json, AjaxStatus status){
		Log.d(TAG, " parkIOPictureCallback  json ====== " +json);
		
		JSONArray jsonArr = null;
		
		// successful ajax call          
        if(json != null){   
           if("1".equals(json.optString("CD_RESULT"))){
			  jsonArr = json.optJSONArray("PICTURE_LIST");
			  String pictureURL = "";
			  int Len = jsonArr.length();
			  for(int i=0; i<Len; i++){
				  try {
					JSONObject res = jsonArr.getJSONObject(i);
					String filePath = res.optString("FILE_PATH");
					String fileName = res.optString("FILE_NAME");
					if( i == 0){
						pictureURL = Constants.SERVER_HOST + filePath + fileName;
						Log.d(TAG, " pictureURL >>> " + pictureURL);
						break;
					}
				 } catch (JSONException e) {
					  if(Constants.DEBUG_PRINT_LOG){
						  e.printStackTrace();
					  }else{
						  System.out.println("예외 발생");
					  }
				 }
			  }
			  
			  if(!Util.isEmpty(pictureURL)){
				  ImageLoader.setImageUrlView(this, pictureURL, iv_image);
			  }
			  
           } else {
        	   iv_image.setImageDrawable(null);
           }
        } else {
        	iv_image.setImageDrawable(null);
    	    Toast.makeText(this, Constants.DATA_FAIL, Toast.LENGTH_LONG).show();
        }		
	}
		
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}
}
