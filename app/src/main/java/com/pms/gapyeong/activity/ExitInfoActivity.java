package com.pms.gapyeong.activity;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;
import android.app.AlertDialog;
import android.app.Dialog;
import android.bluetooth.BluetoothDevice;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.pms.gapyeong.common.Preferences;
import com.pms.gapyeong.vo.ExitPaymentData;
import com.pms.gapyeong.R;
import com.pms.gapyeong.common.Constants;
import com.pms.gapyeong.common.DeviceInfo;
import com.pms.gapyeong.common.MsgUtil;
import com.pms.gapyeong.common.Util;
import com.pms.gapyeong.database.CodeHelper;
import com.pms.gapyeong.pay.PayData;
import com.pms.gapyeong.pay.PayVo;
import com.pms.gapyeong.vo.ParkIO;
import com.pms.gapyeong.vo.ReceiptType4Vo;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

/**
 * 출차 정보 화면
 */
public class ExitInfoActivity extends BaseActivity {

    public static final int DIALOG_PARK_AREA = 660;

    private Button btn_car_no_1;
    private Button btn_car_no_2;
    private Button btn_car_no_3;
    private Button btn_car_no_4;
    private Button btn_car_no_manual;

    private TextView tvPhoneNumber;
    private TextView tvParkPosition;
    private TextView tvParkType;
    private TextView tvChargeType;
    private TextView tvChargeDiscount;
    private TextView tvParkUseTime;
    private TextView tvEntrance;
    private TextView tvParkPayment;
    private TextView tvDisPayment;
    private TextView tvUnPayment;
    private TextView tvPrePayment;
    private TextView tvPayment;
    private TextView tvExitCarKind;
    private Button btn_out_cancel;
    private Button btn_print_bill;
    private Button btn_receipt;
    private TextView tvPayType;

    private ParkIO parkIO;
    private String parkIoName;

    private CodeHelper cch;
    private Dialog dialog;
    private ReceiptType4Vo type4Vo;
    private TextView tvCouponAmt;
    private String startTime;
    private String exitTime;
    private boolean bsCashReceipt = true;

    private String carNo = "";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.exit_info_layout);
    }

    @Override
    protected void initLayoutSetting() {
        super.initLayoutSetting();

        btn_car_no_1 = (Button) findViewById(R.id.btn_car_no_1);
        btn_car_no_2 = (Button) findViewById(R.id.btn_car_no_2);
        btn_car_no_3 = (Button) findViewById(R.id.btn_car_no_3);
        btn_car_no_4 = (Button) findViewById(R.id.btn_car_no_4);
        btn_car_no_manual = (Button) findViewById(R.id.btn_car_no_manual);

        tvPhoneNumber = (TextView) findViewById(R.id.tvPhoneNumber);
        tvParkPosition = (TextView) findViewById(R.id.tvParkPosition);
        tvParkType = (TextView) findViewById(R.id.tvParkType);
        tvChargeType = (TextView) findViewById(R.id.tvChargeType);
        tvChargeDiscount = (TextView) findViewById(R.id.tvChargeDiscount);
        tvParkUseTime = (TextView) findViewById(R.id.tvParkUseTime);
        tvEntrance = (TextView) findViewById(R.id.tvEntrance);
        tvParkPayment = (TextView) findViewById(R.id.tvParkPayment);
        tvDisPayment = (TextView) findViewById(R.id.tvDisPayment);
        tvUnPayment = (TextView) findViewById(R.id.tvUnPayment);
        tvPrePayment = (TextView) findViewById(R.id.tvPrePayment);
        tvPayment = (TextView) findViewById(R.id.tvPayment);
        tvExitCarKind = (TextView) findViewById(R.id.tvExitCarKind);
        tvCouponAmt = (TextView) findViewById(R.id.tvCouponAmt);
        btn_out_cancel = (Button) findViewById(R.id.btn_out_cancel);
        btn_print_bill = (Button) findViewById(R.id.btn_print_bill);
        btn_receipt = (Button) findViewById(R.id.btn_receipt);
        tvPayType = (TextView) findViewById(R.id.tvPayType);

        btn_out_cancel.setOnClickListener(this);
        btn_print_bill.setOnClickListener(this);
        btn_receipt.setOnClickListener(this);

        Intent i = getIntent();
        if (i != null) {

            String title = i.getStringExtra("title");
            if (title != null || !"".equals(title)) {
                ((TextView) findViewById(R.id.tv_top_title)).setText(title);
            } else {
                ((TextView) findViewById(R.id.tv_top_title)).setText("출차정보");
            }

            parkIO = (ParkIO) i.getSerializableExtra("parkIO");
            parkIoName = i.getStringExtra("parkIoName");

            if (parkIoName.equals("출차완료")) {
                btn_out_cancel.setVisibility(View.VISIBLE);
                btn_print_bill.setVisibility(View.VISIBLE);

                if ("02".equals(parkIO.getParkType()) || "04".equals(parkIO.getParkType())) {
                    btn_out_cancel.setEnabled(false);
                }

            } else if (parkIoName.equals("출차취소")) {
                btn_out_cancel.setVisibility(View.GONE);
                btn_print_bill.setVisibility(View.GONE);
            }

            cch = CodeHelper.getInstance(this);


            carNo = Util.isNVL(parkIO.getCarNo());
            setCarnoView(carNo, btn_car_no_1, btn_car_no_2, btn_car_no_3, btn_car_no_4, btn_car_no_manual);

            tvPhoneNumber.setText(parkIO.getTel());
            tvParkPosition.setText(parkIO.getCdArea());

            if (!("").equals(parkIO.getCardNo())) {
                bsCashReceipt = false;   //카드결제이거나 현금영수증 발행을 했다면..
            }

            if (!("").equals(parkIO.getCashApprovalNo())) {
                bsCashReceipt = false;
            }

            if (!bsCashReceipt) {
                btn_receipt.setVisibility(View.GONE);
            } else {
                if (Integer.valueOf(parkIO.getParkAmt()) > 0)
                    btn_receipt.setVisibility(View.VISIBLE);
                else
                    btn_receipt.setVisibility(View.GONE);
            }


            try {
                tvCouponAmt.setText(parkIO.getCouponAmt() + "원");
            } catch (NullPointerException e) {
                if (Constants.DEBUG_PRINT_LOG) {
                    e.printStackTrace();
                } else {
                    System.out.println("예외 발생");
                }
            }

            // 결제여부(미결제, 현금, 카드)
            if ("01".equals(parkIO.getParkType()) || "04".equals(parkIO.getParkType()) || "02".equals(parkIO.getParkType())) {
                if ("".equals(parkIO.getCardNo())) {
                    tvPayType.setText(" 현금");
                } else {
                    tvPayType.setText(" 카드");
                }
            } else {
                tvPayType.setText(" 미결제");
            }

            // 주차종류
            tvParkType.setText(cch.findCodeByCdCode("PY", parkIO.getParkType()));
            // 요금종류


            if ("2".equals(parkClass)) {
                setCommonItem("AM2", Constants.COMMON_TYPE_CHARGE_TYPE, tvChargeType);
            } else if ("3".equals(parkClass)) {
                setCommonItem("AM3", Constants.COMMON_TYPE_CHARGE_TYPE, tvChargeType);
            } else if ("4".equals(parkClass)) {
                setCommonItem("AM4", Constants.COMMON_TYPE_CHARGE_TYPE, tvChargeType);
            } else if ("5".equals(parkClass)) {
                setCommonItem("AM5", Constants.COMMON_TYPE_CHARGE_TYPE, tvChargeType);
            } else if ("6".equals(parkClass)) {
                setCommonItem("AM6", Constants.COMMON_TYPE_CHARGE_TYPE, tvChargeType);
            }

            //setCommonItem("AM2", Constants.COMMON_TYPE_CHARGE_TYPE, tvChargeType);

            // 요금할인
            tvChargeDiscount.setText(cch.findCodeByCdCode("DC", parkIO.getDisCd()));
            // 주차시간
            startTime = parkIO.getParkInDay();// parkIO.getPioNumber().substring(parkIO.getPioNumber().lastIndexOf("-") + 1);
            exitTime = parkIO.getParkOutDay();// Util.getYmdhms("HH:mm:ss");

            String s = "";
            String e = "";
            try {
                s = startTime.substring(8, 10) + ":" + startTime.substring(10, 12);
            } catch (NullPointerException ex) {
                System.out.println("NullPointerException 예외 발생");
            } catch (IndexOutOfBoundsException ex) {
                System.out.println("IndexOutOfBoundsException 예외 발생");
            }

            try {
                e = exitTime.substring(8, 10) + ":" + exitTime.substring(10, 12);
            } catch (NullPointerException ex) {
                System.out.println("NullPointerException 예외 발생");
            } catch (IndexOutOfBoundsException ex) {
                System.out.println("IndexOutOfBoundsException 예외 발생");
            }

            tvParkUseTime.setText(s + " ~ " + e);

            // 주차시간(분)
            tvEntrance.setText(minuteView(parkIO.getParkTime()));
            // 주차금액
            tvParkPayment.setText(parkIO.getParkAmt() + "원");
            // 할인금액
            tvDisPayment.setText(parkIO.getDisAmt() + "원");

            // 미수 금액은 결제시 더하지 않는다.
            int unpay = Integer.parseInt(parkIO.getYetAmt());
            int prePay = Integer.parseInt(parkIO.getAdvAmt());
//			int payment = Integer.parseInt(charge) - prePay;

            // 미납/선납금액
            tvPrePayment.setText(prePay + "원");
            tvUnPayment.setText(unpay + "원");

            // 총 납부금액(현금+카드)
            int realAmt = Util.sToi(parkIO.getRealAmt());
            // 환불금액
            int returnAmt = Util.sToi(parkIO.getReturnAmt());
            // 납부금액
            tvPayment.setText((realAmt - returnAmt) + "원");

            // 출차종류
            tvExitCarKind.setText(parkIoName);

            type4Vo = new ReceiptType4Vo(Util.getYmdhms("yyyy-MM-dd HH:mm:ss")    // 출력일자
                    , tvParkType.getText().toString()    // 주차종류 :
                    , tvEntrance.getText().toString()        // 주차시간 :
                    , tvParkPosition.getText().toString()        // 주차면 :
                    , tvChargeDiscount.getText().toString()        // 할인종류 :
                    , startTime        // 입차시간 :
                    , exitTime        // 출차시간 :
                    , tvPayment.getText().toString().replace(",", "").replace("원", "")            // 납부금액 :
                    , tvPrePayment.getText().toString().replace(",", "").replace("원", "")        // 선납금액 :
                    , tvParkPayment.getText().toString().replace(",", "").replace("원", "")        // 주차요금 :
                    , tvUnPayment.getText().toString().replace(",", "").replace("원", "")        // 미납금액 :
                    , tvDisPayment.getText().toString().replace(",", "").replace("원", "")        // 할인금액 :
                    , BIZ_NO                    // 사업자번호
                    , BIZ_NAME
                    , BIZ_TEL);

            type4Vo.setPioNum(parkIO.getPioNum());
            type4Vo.setCarNum(carNo);
            type4Vo.setParkName(parkName);
            type4Vo.setEmpName(empName);
            type4Vo.setEmpPhone(empPhone);
            type4Vo.setEmpTel(empTel);
            type4Vo.setCouponAmt(tvCouponAmt.getText().toString().replace(",", "").replace("원", ""));
            type4Vo.setConfirmCardNum(parkIO.getApprovalNo());
            type4Vo.setCardNo(parkIO.getCardNo());
            type4Vo.setConfirmCashReceipt(parkIO.getCashApprovalNo());
            type4Vo.setEmpBusiness_Tel(empBusiness_TEL);
            type4Vo.setOutPrint(Preferences.getValue(this, Constants.PREFERENCE_OUT_PRINT, ""));
        }
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);

        switch (v.getId()) {
            case R.id.btn_out_cancel:
                dialog = exitOutCancelDialog();
                break;

            case R.id.btn_print_bill:

                try {
                    type4Vo.setTitle("출차영수증");
                    PayData payData = new PayData(this, OUT_PRINT, IN_PRINT, TICKET_PRINT);
                    payData.printDefault(PayData.PAY_DIALOG_TYPE_EXIT_ONLY, type4Vo, mPrintService);
                    payData.ImagePrint(empLogo_image);
                    payData.printText("\n\n");
                    apiPrintUpdate(parkIO.getPioNum());
                } catch (UnsupportedEncodingException e) {
                    if (Constants.DEBUG_PRINT_LOG) {
                        e.printStackTrace();
                    } else {
                        System.out.println("예외 발생");
                    }
                } catch (Exception e) {
                    if (Constants.DEBUG_PRINT_LOG) {
                        e.printStackTrace();
                    } else {
                        System.out.println("예외 발생");
                    }
                }

                break;
            case R.id.btn_receipt:

                cashReceiptShowDialog(Util.sToi(tvPayment.getText().toString().replace(",", "").replace("원", "")), type4Vo, tvParkType.getText().toString(), "AFTER");
                break;
        }
    }

    @Override
    protected void request(int type, Object data) {

        if (type == PayData.PAY_DIALOG_TYPE_CASH_RECEIPT) {
            PayVo payVo = (PayVo) data;
            type4Vo.setConfirmCashReceipt(payVo.getCashApprovalNo());
            showProgressDialog();

            Map<String, Object> params = new HashMap<String, Object>();
            params.put(Constants.METHOD, Constants.INTERFACEID_CASH_RECEIPT_UPDATE);
            params.put(Constants.API_FORMAT, Constants.JSON_FORMAT);
            params.put("PIO_NUM", parkIO.getPioNum());
            params.put("CASH_APP_NO", payVo.getCashApprovalNo());
            params.put("CASH_APP_DATE", payVo.getCashApprovalDate());
            params.put("BILL_TYPE", "B1");


            String url = Constants.API_SERVER;
            Log.d(TAG, " apiCashReceiptCall url >>> " + url + " params ::: " + params.toString());

            AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>();
            cb.url(url).params(params).type(JSONObject.class).weakHandler(this, "cashReceiptCallback").redirect(true).retry(3).fileCache(false).expire(-1);
            AQuery aq = new AQuery(this);
            aq.ajax(cb);
        }
    }

    public void cashReceiptCallback(String url, JSONObject json, AjaxStatus status) {
        closeProgressDialog();
        Log.d(TAG, " parkOutCancelCallback  json ====== " + json);
        if (json != null) {
            String KN_RESULT = json.optString("KN_RESULT");
            //successful ajax call, show status code and json content
            if ("1".equals(json.optString("CD_RESULT"))) {
                btn_receipt.setVisibility(View.GONE);
                type4Vo.setTitle("출차영수증");
                PayData payData = new PayData(this, OUT_PRINT, IN_PRINT, TICKET_PRINT);
                payData.printDefault(PayData.PAY_DIALOG_TYPE_EXIT_ONLY, type4Vo, mPrintService);
                payData.ImagePrint(empLogo_image);
                try {
                    payData.printText("\n\n");
                } catch (UnsupportedEncodingException e) {
                    System.out.println("InterruptedException 예외 발생");
                }

                MsgUtil.ToastMessage(ExitInfoActivity.this, KN_RESULT, Toast.LENGTH_LONG);
                //finish();
            } else {
                //ajax error, show error code
                MsgUtil.ToastMessage(ExitInfoActivity.this, KN_RESULT, Toast.LENGTH_LONG);
            }
        } else {
            MsgUtil.ToastMessage(ExitInfoActivity.this, Constants.DATA_FAIL);
        }


    }

    @Override
    public void getSelectListDialogData(int tag, String data) {
        super.getSelectListDialogData(tag, data);

        // 주차면 다이얼로그
        if (tag == DIALOG_PARK_AREA) {
            apiParkOutAreaUpdateCall((String) data);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode,
                                    Intent intent) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, intent);

        // 결제취소가 정상적으로 이루어 졌으면 출차 취소 다이얼로그 호출
        if (resultCode == PayData.PAY_DIALOG_CANCEL_APPROVAL_EXIT) {
            String reason = edEtcreason.getText().toString();
            apiParkOutCancelCall(reason);
        } else {
            try {
                mPrintService.stop();
                if (mPrintService.getState() == 0) {

                    BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(mBluetoothAddress);
                    mPrintService.connect(device, true);
                }
            }  catch (IllegalArgumentException e) {
                System.out.println("IllegalArgumentException 예외 발생");
            } catch (NullPointerException e) {
                System.out.println("NullPointerException 예외 발생");
            }

            int Dialog_type = Integer.valueOf(Preferences.getValue(this, Constants.PREFERENCE_MCPAY_PARAMETER, ""));

            if (Dialog_type == Constants.COMMON_TYPE_CARD_PAYMENT_FAIL) {
                setToast("결제가 취소되었습니다. 다시 시도해 주세요.");
            } else if (Dialog_type == PayData.PAY_DIALOG_CANCEL_APPROVAL_EXIT)//출차취소
            {
                String reason = edEtcreason.getText().toString();
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    System.out.println("InterruptedException 예외 발생");
                }

                apiParkOutCancelCall(reason);
                InitPreferences();
            } else if (Dialog_type == PayData.PAY_DIALOG_TYPE_CASH_RECEIPT)  //현금영수증 발행
            {
                String approvalNo = Preferences.getValue(this, Constants.PREFERENCE_MCPAY_APPROVAL_NO, "");
                String approvalDate = Preferences.getValue(this, Constants.PREFERENCE_MCPAY_APPROVAL_DATE, "");
                String amt_tot = Preferences.getValue(this, Constants.PREFERENCE_MCPAY_TOTAL_AMT, "");

                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    System.out.println("InterruptedException 예외 발생");
                }


                PayVo payVo2 = new PayVo();  //현금영수증참조
                payVo2.setCashApprovalNo(approvalNo);
                payVo2.setCashApprovalDate(approvalDate);
                tvCashBill.setTag(payVo2);
                tvCashBill.setText("발행");
                InitPreferences();

                //cashReceiptRun(Util.sToi(amt_tot), g_vo);
                ExitPaymentData exitVo = new ExitPaymentData();
                exitVo.setCashApprovalNo(approvalNo);
                exitVo.setCashApprovalDate(approvalDate);
                request(PayData.PAY_DIALOG_TYPE_CASH_RECEIPT, exitVo);
                if (cashReceiptDialog != null) {
                    cashReceiptDialog.dismiss();
                    cashReceiptDialog = null;
                }
                g_vo = null;
                tvCashBill = null;
            }
        }
    }


    private TextView tvCancelReason;
    private EditText edEtcreason;

    public Dialog exitOutCancelDialog() {
        final Dialog cancelDialog;

        cancelDialog = new Dialog(this,
                android.R.style.Theme_Holo_Light_Dialog);

        cancelDialog.setContentView(R.layout.exit_out_cancel_dialog);
        cancelDialog.setTitle("출차취소");

        tvCancelReason = (TextView) cancelDialog.findViewById(R.id.cancel_reason);
        edEtcreason = (EditText) cancelDialog.findViewById(R.id.etc_reason);
        setCommonItem("C2", Constants.COMMON_TYPE_PARK_OUT_CANCEL_TYPE, tvCancelReason, 0);

        tvCancelReason.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                setCommonDialog("출차취소구분", "C2", Constants.COMMON_TYPE_PARK_OUT_CANCEL_TYPE, tvCancelReason);
            }
        });

        ((Button) cancelDialog.findViewById(R.id.btn_ok)).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                int len = edEtcreason.getText().length();
                len = 8;
                if (len < 6) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(ExitInfoActivity.this);
                    builder.setTitle("기타이유입력").setMessage("기타이유를 최소 5글자 이상 입력하세요.");
                    builder.setPositiveButton(R.string.btn_ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    AlertDialog alert = builder.create();
                    alert.setCancelable(false);
                    alert.show();

                } else {

                    if (!Util.isEmpty(parkIO.getApprovalNo()) && !Util.isEmpty(parkIO.getCardCompany())) {
                        type4Vo.setPioNum(parkIO.getPioNum());
                        // TODO 결제 취소..
                        goToPayCancelActivity(PayData.PAY_DIALOG_CANCEL_APPROVAL_EXIT, parkIO.getCardAmt(), parkIO.getApprovalNo(), parkIO.getApprovalDate(), type4Vo);
                    } else {
                        // 현금 영수증 취소
                        // goToCashReceiptCancelActivity(PayData.PAY_DIALOG_CANCEL_CASH_RECEIPT, parkIO.getCashAmt(), parkIO.getApprovalNo(), parkIO.getApprovalDate());
                        String reason = edEtcreason.getText().toString();
                        apiParkOutCancelCall(reason);
                    }
                }
            }
        });

        ((Button) cancelDialog.findViewById(R.id.btn_cancel)).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                cancelDialog.dismiss();
            }
        });

        cancelDialog.setOnCancelListener(new OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
            }
        });

        cancelDialog.show();
        return cancelDialog;
    }

    private void apiParkOutCancelCall(String comment) {
        showProgressDialog();

        Map<String, Object> params = new HashMap<String, Object>();
        params.put(Constants.METHOD, Constants.INTERFACEID_PARK_OUT_CANCEL);
        params.put(Constants.API_FORMAT, Constants.JSON_FORMAT);
        params.put("PIO_NUM", parkIO.getPioNum());
        params.put("CD_PARK", parkCode);
        params.put("EMP_CD", empCode);
        params.put("CAR_NO", parkIO.getCarNo());
        params.put("WORK_DAY", Util.getYmdhms("yyyyMMdd"));
        params.put("CD_STATE", "OC");
        params.put("CD_AREA", tvParkPosition.getText().toString());
        params.put("COMMENT", comment);

        String url = Constants.API_SERVER;
        Log.d(TAG, " apiParkOutCancelCall url >>> " + url + " params ::: " + params.toString());

        AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>();
        cb.url(url).params(params).type(JSONObject.class).weakHandler(this, "parkOutCancelCallback").redirect(true).retry(3).fileCache(false).expire(-1);
        AQuery aq = new AQuery(this);
        aq.ajax(cb);
    }

    public void parkOutCancelCallback(String url, JSONObject json, AjaxStatus status) {
        closeProgressDialog();
        Log.d(TAG, " parkOutCancelCallback  json ====== " + json);
        if (json != null) {
            String KN_RESULT = json.optString("KN_RESULT");
            //successful ajax call, show status code and json content
            if ("1".equals(json.optString("CD_RESULT"))) {
                MsgUtil.ToastMessage(ExitInfoActivity.this, KN_RESULT, Toast.LENGTH_LONG);
                finish();
            } else if ("5".equals(json.optString("CD_RESULT"))) { // 다른 차량이 입차되어 있을 경우 다른 주차면으로 이동
                MsgUtil.ToastMessage(ExitInfoActivity.this, KN_RESULT, Toast.LENGTH_LONG);
                apiParkIoListCall(Util.getEncodeStr("%"), "I1", today, today);
            } else {
                //ajax error, show error code
                MsgUtil.ToastMessage(ExitInfoActivity.this, KN_RESULT, Toast.LENGTH_LONG);
            }
        } else {
            MsgUtil.ToastMessage(ExitInfoActivity.this, Constants.DATA_FAIL);
        }

        if (dialog != null) {
            dialog.dismiss();
        }
    }

    /**
     * 서버에 입차되었는지 요청한다.
     *
     * @param //carNumber 요청할 자동차 번호
     */
    private void apiParkIoListCall(String carNo, String codeState, String startDay, String endDay) {
        showProgressDialog(false);

        String url = Constants.API_SERVER + "?" + Constants.METHOD + "=" + Constants.INTERFACEID_PARK_IO;
        String param = "&CD_PARK=" + parkCode
                + "&CAR_NO=" + carNo
                + "&CD_STATE=" + codeState
                + "&START_DAY=" + startDay
                + "&END_DAY=" + endDay
                + "&" + Constants.API_FORMAT + "=" + Constants.JSON_FORMAT;
        Log.d(TAG, " apiParkIoListCall url >>> " + url + param);

        AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>();
        cb.url(url + param).type(JSONObject.class).weakHandler(this, "parkIoListCallback").redirect(true).retry(3).fileCache(false).expire(-1);
        AQuery aq = new AQuery(this);
        aq.ajax(cb);
    }

    public void parkIoListCallback(String url, JSONObject json, AjaxStatus status) {
        closeProgressDialog();
        Log.d(TAG, " parkIoListCallback  json ====== " + json);

        ArrayList<ParkIO> list = new ArrayList<ParkIO>();

        // successful ajax call
        if (json != null) {
            String KN_RESULT = json.optString("KN_RESULT");
            if ("1".equals(json.optString("CD_RESULT"))) {
                JSONArray jsonArr = json.optJSONArray("RESULT");
                int Len = jsonArr.length();
                for (int i = 0; i < Len; i++) {
                    try {
                        JSONObject res = jsonArr.getJSONObject(i);
                        ParkIO info = new ParkIO(res.optString("PIO_NUM")
                                , res.optString("CD_PARK")
                                , res.optString("PIO_DAY")
                                , res.optString("CAR_NO")
                                , res.optString("CD_AREA")
                                , res.optString("PARK_TYPE")
                                , res.optString("DIS_CD")
                                , res.optString("TEL")
                                , res.optString("PARK_IN_AMT")
                                , res.optString("PARK_OUT_AMT")
                                , res.optString("PARK_AMT")
                                , res.optString("ADV_IN_AMT")
                                , res.optString("ADV_OUT_AMT")
                                , res.optString("ADV_AMT")
                                , res.optString("DIS_IN_AMT")
                                , res.optString("DIS_OUT_AMT")
                                , res.optString("DIS_AMT")
                                , res.optString("CASH_IN_AMT")
                                , res.optString("CASH_OUT_AMT")
                                , res.optString("CASH_AMT")
                                , res.optString("CARD_IN_AMT")
                                , res.optString("CARD_OUT_AMT")
                                , res.optString("CARD_AMT")
                                , res.optString("TRUNC_IN_AMT")
                                , res.optString("TRUNC_OUT_AMT")
                                , res.optString("TRUNC_AMT")
                                , res.optString("COUPON_AMT")
                                , res.optString("RETURN_AMT")
                                , res.optString("YET_AMT")
                                , res.optString("REAL_AMT")
                                , res.optString("PARK_TIME")
                                , res.optString("PARK_IN")
                                , res.optString("PARK_OUT")
                                , res.optString("CARD_NO")
                                , res.optString("CARD_COMPANY")
                                , res.optString("APPROVAL_NO")
                                , res.optString("APPROVAL_DATE")
                                , res.optString("CHARGE_TYPE")
                                , res.optString("KN_COMMENT"));
                        list.add(info);
                    } catch (JSONException e) {
                        System.out.println("JSONException 예외 발생");
                    }
                }  // end for

            } else {
                MsgUtil.ToastMessage(ExitInfoActivity.this, KN_RESULT, Toast.LENGTH_LONG);
                return;
            }
        } else {
            MsgUtil.ToastMessage(ExitInfoActivity.this, Constants.DATA_FAIL, Toast.LENGTH_LONG);
        }

        closeProgressDialog();

        statusBoardHelper.setParkIo(list);
        Log.d(TAG, "parkIO size : " + list);

        basicListDialog(DIALOG_PARK_AREA, "주차면", statusBoardHelper.getEmptyNumberList(), null);
    }

    private void apiParkOutAreaUpdateCall(String cd_area) {
        showProgressDialog();
        String url = Constants.API_SERVER + "?" + Constants.METHOD + "=" + Constants.INTERFACEID_PARK_OUT_AREA_UPDATE
                + "&PIO_NUM=" + parkIO.getPioNum()        //	PIO_NUM:주차번호
                + "&CD_PARK=" + parkCode                    //	CD_PARK :주차장
                + "&CAR_NO=" + parkIO.getCarNo()            //	CAR_NO : 차량번호
                + "&CD_AREA=" + cd_area                    //	CD_AREA : 주차면
                + "&EMP_CD=" + empCode                    //	EMP_CD : 등록자
                + "&WORK_DAY=" + today                    //	WORK_DAY : 주차일
                + "&" + Constants.API_FORMAT + "=" + Constants.JSON_FORMAT;
        Log.d(TAG, " apiParkOutAreaUpdateCall url >>> " + url);

        AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>();
        cb.url(url).type(JSONObject.class).weakHandler(this, "parkOutAreaUpdateCallback").redirect(true).retry(3).fileCache(false).expire(-1);
        AQuery aq = new AQuery(this);
        aq.ajax(cb);
    }

    public void parkOutAreaUpdateCallback(String url, JSONObject json, AjaxStatus status) {
        closeProgressDialog();
        Log.d(TAG, " apiParkOutAreaUpdateCall  json ====== " + json);
        if (json != null) {
            String KN_RESULT = json.optString("KN_RESULT");
            //successful ajax call, show status code and json content
            if ("1".equals(json.optString("CD_RESULT"))) {
                MsgUtil.ToastMessage(ExitInfoActivity.this, KN_RESULT);
                String CD_AREA = json.optString("CD_AREA");
                tvParkPosition.setText(CD_AREA);
                // 주차면 수정 후 취소 처리...
                String reason = edEtcreason.getText().toString();
                apiParkOutCancelCall(reason);
            } else {
                //ajax error, show error code
                MsgUtil.ToastMessage(ExitInfoActivity.this, KN_RESULT);
            }
        } else {
            MsgUtil.ToastMessage(ExitInfoActivity.this, Constants.DATA_FAIL);
        }
    }

    protected void apiPrintUpdate(String pionum) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put(Constants.METHOD, Constants.INTERFACEID_PRINT_UPDATE);
        params.put(Constants.API_FORMAT, Constants.JSON_FORMAT);
        params.put("PIO_NUM", pionum);

        showProgressDialog(false);
        String url = Constants.API_SERVER;

        AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>();
        cb.url(url).params(params).type(JSONObject.class).weakHandler(this, "printUpdateCallback").redirect(true).retry(3).fileCache(false).expire(-1);
        AQuery aq = new AQuery(this);
        aq.ajax(cb);
    }

    public void printUpdateCallback(String url, JSONObject json, AjaxStatus status) {

        closeProgressDialog();

        Log.d(TAG, " printUpdateCallback  json ====== " + json);

        // successful ajax call
        if (json != null) {
            String KN_RESULT = json.optString("KN_RESULT");
            if ("1".equals(json.optString("CD_RESULT"))) {
                MsgUtil.ToastMessage(this, KN_RESULT);
            } else {
                MsgUtil.ToastMessage(this, KN_RESULT);
            }
        } else {
            MsgUtil.ToastMessage(this, Constants.DATA_FAIL, Toast.LENGTH_LONG);
        }

    }

    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
    }

}
