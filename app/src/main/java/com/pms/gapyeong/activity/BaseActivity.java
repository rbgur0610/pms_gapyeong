package com.pms.gapyeong.activity;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.pms.gapyeong.R;
import com.pms.gapyeong.adapter.CouponAdapter;
import com.pms.gapyeong.common.Constants;
import com.pms.gapyeong.common.DeviceInfo;
import com.pms.gapyeong.common.MsgUtil;
import com.pms.gapyeong.common.Preferences;
import com.pms.gapyeong.common.Util;
import com.pms.gapyeong.database.CodeHelper;
import com.pms.gapyeong.database.PictureHelper;
import com.pms.gapyeong.database.Preference;
import com.pms.gapyeong.database.StatusBoardHelper;
import com.pms.gapyeong.pay.McPayActivity;
import com.pms.gapyeong.pay.PayData;
import com.pms.gapyeong.pay.PayVo;
import com.pms.gapyeong.printer.BluetoothPrintService;
import com.pms.gapyeong.vo.ExitPaymentData;
import com.pms.gapyeong.vo.ParkIO;
import com.pms.gapyeong.vo.PictureInfo;
import com.pms.gapyeong.vo.ReceiptType10Vo;
import com.pms.gapyeong.vo.ReceiptType11Vo;
import com.pms.gapyeong.vo.ReceiptType13Vo;
import com.pms.gapyeong.vo.ReceiptType4Vo;
import com.pms.gapyeong.vo.ReceiptVo;
import com.woosim.printer.WoosimService;

/**
 * 모든 액티비티들이 상속 받을 기본 Activity
 */
public abstract class BaseActivity extends Activity implements OnClickListener, OnCheckedChangeListener {

    private static ArrayList<Activity> ActList = new ArrayList<Activity>();

    public static String TAG = "";

    protected static String today;                // 오늘 날짜.
    protected static String parkCode;            // 주차장 코드
    protected static String parkName;                        // 주차장이름
    static String parkClass;                    // 주차장급지
    static String parkCdstate;                    // 주차장 cd_state
    protected static String empCode;                        // 주차요원 코드
    static String empName;                        // 주차요원명
    static String empSystemVersion;                // 주차요원 앱 버전
    static String empPhone;                        // 주차요원 폰 번호
    static String empTel;                        // 주차요원 유선번호
    static String empBank_MSG;
    static String empAccount_No;
    static String empKN_ACCOUNT;
    static String empKN_BANK;
    static String empBusiness_TEL;
    static byte[] empLogo_image;
    static String workStartTime;                // 근무 시작 시간
    static String workEndTime;                    // 근무 종료시간
    static String serviceStartTime;                // 무료 주차 시작 시간
    static String serviceEndTime;                // 무료 주차 종료시간
    protected static String empGroupCD;
    static String empTicket_print;
    public static String catNumber;
    static String ticketAmt = "0";  // 정기권 금액
    static int dayAmt = 0;    // 일일권 금액
    static int parkMaxAmt = 0;    // 주차요금 최대금액
    static String parkAMtype = "M2-1";
    static String inService;
    static String outService;
    static String returnService;
    static String baseSection;
    static String baseAmt;
    static String repeatSection;
    static String repeatAmt;
    static String truncMethod;
    static String truncUnit;
    protected static String advBoundTime = "0";
    protected static String BANK_MSG = "";
    protected static String BIZ_NAME = "";
    protected static String BIZ_BANK_MSG = "";
    protected static String BIZ_BANK = "";
    protected static String BIZ_ACCOUNT_NO = "";
    protected static String BIZ_ACCOUNT_NAME = "";
    protected static String BIZ_COPY_RIGHT = "";
    protected static String BIZ_TEL = "";
    protected static String BIZ_NO = "";
    protected static String IN_PRINT = "";
    protected static String OUT_PRINT = "";
    protected static String TICKET_PRINT = "";
    protected static String DEVICE_DPI = "";
    protected static ReceiptVo gReceiptVo = null;

    protected static JSONArray codeJsonArr;

    protected static StatusBoardHelper statusBoardHelper;    // 현환판 DB
    public Context mContext;
    private static ArrayList<Map<String, String>> scheduleList = new ArrayList<Map<String, String>>();
    private Button btn_cash;
    private Button btn_unpayAver;

    public static ArrayList<Map<String, String>> getScheduleList() {
        if (scheduleList == null) scheduleList = new ArrayList<>();
        return scheduleList;
    }

    public static void setScheduleList(ArrayList<Map<String, String>> scheduleList) {
        BaseActivity.scheduleList = scheduleList;
    }

    protected ProgressDialog progressDialog;

    public CodeHelper cch;
    public static int disAdj = 100;
    // Name of the connected device
    public String mConnectedDeviceName = null;
    protected static BluetoothAdapter mBluetoothAdapter = null;
    protected static String mBluetoothAddress = null;

    // Member object for the print services
    public static BluetoothPrintService mPrintService = null;
    public static WoosimService mWoosim = null;

    // Intent request codes
    public static final int REQUEST_CONNECT_DEVICE_SECURE = 1;
    public static final int REQUEST_CONNECT_DEVICE_INSECURE = 2;
    public static final int REQUEST_ENABLE_BT = 3;

    // Message types sent from the BluetoothPrintService Handler
    public static final int MESSAGE_DEVICE_NAME = 1;
    public static final int MESSAGE_TOAST = 2;
    public static final int MESSAGE_READ = 3;

    private ArrayList<ParkIO> glstParkIO;

    /**
     * @return : baseActivity를 extends한 액티비티들이 실행될때마다 실행되며 이때 태그및 현재 액티비티값들을 설정해준다.
     * @date : 2012.04.10
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        today = Util.getYmdhms("yyyyMMdd");
        TAG = this.getClass().getSimpleName();
        cch = CodeHelper.getInstance(this);
        Log.d(Constants.TAG, TAG);
        getActivity(this);
    }

    private void getActivity(Activity act) {
        if (!ActList.contains(act)) {
            ActList.add(act);
        }
    }

    @Override
    public void onClick(View v) {

    }

    /**
     * @return : onCreate이후 발생하는 이벤트 이며 initLayoutSetting(레이아웃설정)을 셋팅해준다.
     * @date : 2012.04.04
     */
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {

        super.onPostCreate(savedInstanceState);
        initLayoutSetting();
    }

    /**
     * @return : 레이아웃 설정을 하는 뷰들을 셋팅하는곳 BaseActivity를 extends한 Activity에서 onCreate이후 발생한다.
     * @date : 2012.04.06
     */
    protected void initLayoutSetting() {
    }

    /**
     * @return : 매인 화면 뒤로가기 누를시에 종료 팝업을 띄우기 위해 if문 씀
     * @date : 2012.04.09
     */
    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        Log.e("THIS ACTIVITY===", TAG);
        if (TAG.equals("LoginActivity") || TAG.equals("MainActivity")) {
            twoButtonDialog(Constants.MAINACTIVITY_DESTROY_ACTIVITY, getStr(R.string.title_exit), getStr(R.string.msg_exit_app)).show();
        } else {
            super.onBackPressed();
            finish();
            overridePendingTransition(0, 0);
        }
    }

    public void setToast(String msg) {
        Toast toastMsg = Toast.makeText(this, msg, Toast.LENGTH_SHORT);
        toastMsg.show();
    }

    /**
     * @return : 리소스에서 스트링 가져오기
     * @date : 2012.04.20
     */
    protected String getStr(int Rid) {
        return this.getResources().getString(Rid).toString();
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {

    }

    protected Handler mHandler = new Handler() {
        @SuppressLint("HandlerLeak")
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            Log.e("msg", String.valueOf(msg.what));
            switch (msg.what) {
                default:
                    HandlerListener(msg);
                    break;
            }
        }
    };

    /**
     * @param : int MSGID 메세지 아이디는 핸들러 부를때 msg.what값입니다.
     * @return : 핸들러 이벤트가 행해졌을때 받아오는 리스너 함수 입니다.
     * @date : 2012.04.04
     */
    protected void HandlerListener(Message msg) {
    }

    public void showProgressDialog() {
        showProgressDialog(ProgressDialog.STYLE_SPINNER, "");
    }

    public void showProgressDialog(boolean isClose) {
        showProgressDialog(ProgressDialog.STYLE_SPINNER, "", isClose);
    }

    public void showProgressDialog(int style, String text) {
        showProgressDialog(style, text, true);
    }

    public void showProgressDialog(int style, String text, boolean isClose) {
        if (isClose) {
            closeProgressDialog();
        } else {
            if (progressDialog != null && progressDialog.isShowing())
                return;
        }

        String txt = "";
        if (DeviceInfo.checkPlatformVersion()) {
            progressDialog = new ProgressDialog(this, ProgressDialog.THEME_HOLO_DARK);
        } else {
            progressDialog = new ProgressDialog(this);
        }

        if (text != null && !"".equals(text)) {
            txt = text;
        } else {
            txt = "확인 중...";
        }

        progressDialog.setMessage(txt);
        progressDialog.setCancelable(false);
        progressDialog.setProgressStyle(style);

        progressDialog.show();

    }

    public void closeProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();
        progressDialog = null;
    }

    public Dialog basicListDialog(final int Tag, String title, final ArrayList<String> mList, final TextView btnView) {
        return basicListDialog(Tag, title, mList, btnView, null);
    }


    public Dialog basicListDialog(final int Tag, String title, final ArrayList<ParkIO> mList, String _para) {

        ArrayList<String> list = new ArrayList<String>();

        glstParkIO = mList;
        int Len = glstParkIO.size();

        for (int i = 0; i < Len; i++) {
            list.add(glstParkIO.get(i).getCarNo());
        }

        return basicListDialog(Tag, title, list, null, null);
    }

    public Dialog basicListDialog(final int Tag, String title, final ArrayList<String> mList, final TextView btnView, final Object targetObj) {
        final Dialog mBasicDialog;

        mBasicDialog = new Dialog(this,
                android.R.style.Theme_Holo_Light_Dialog);

        mBasicDialog.setContentView(R.layout.basic_list_dialog);
        mBasicDialog.setTitle(title);
        // mBasicDialog.setCancelable(false);
        ListView mDialogList = (ListView) mBasicDialog
                .findViewById(R.id.Lv_dialog);

        ArrayList<String> strList = new ArrayList<String>();

        for (int i = 0; i < mList.size(); i++) {
            strList.add(mList.get(i));
        }

        ArrayAdapter<String> mBasicAdapter = new ArrayAdapter<String>(this, R.layout.choice_list_item, strList);
        mDialogList.setAdapter(mBasicAdapter);
        mDialogList.setChoiceMode(ListView.CHOICE_MODE_SINGLE);

        mBasicDialog.setOnCancelListener(new OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                // TODO Auto-generated method stub
            }
        });

        mDialogList.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int pos,
                                    long arg3) {
                // TODO Auto-generated method stub
                mBasicDialog.dismiss();
                int thisCount = 0;
                if (btnView != null) {
                    if (Tag != Constants.COMMON_TYPE_DDAM_CHARGE_TYPE)    ///< 일일권 요금 종류는 생략
                        btnView.setText(mList.get(pos));
                } else {
                    if (Tag == Constants.COMMON_TYPE_DDAM_CARNO_FIND) {
                        for (int i = 0; i < glstParkIO.size(); i++) {
                            if (glstParkIO.get(i).getCarNo() == mList.get(pos)) {
                                thisCount = i;
                            }
                        }

                        Intent intent = new Intent(BaseActivity.this, ExitCommitActivity.class);
                        intent.putExtra("parkIO", glstParkIO.get(thisCount));
                        startActivity(intent);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

                        glstParkIO.clear();
                        glstParkIO = null;
                    }
                }

                if (targetObj != null) {
                    getSelectListDialogData(Tag, mList.get(pos), btnView, targetObj);
                } else {
                    getSelectListDialogData(Tag, mList.get(pos));
                }
            }
        });

        // 주차면 이동 시 백버튼 닫기... || 일일권 요금 종류 팝업
        if (Tag == 660 || Tag == Constants.COMMON_TYPE_DDAM_CHARGE_TYPE) {
            mBasicDialog.setCancelable(false);
        }
        mBasicDialog.show();
        return mBasicDialog;
    }

    public void getSelectListDialogData(int tag, String data, TextView btnView, Object targetObj) {
    }

    public void getSelectListDialogData(int tag, String data) {
    }

    /**
     * @param : int tag 다이얼로그ID 테그
     * @param : String title 다이얼로그 타이틀 설정
     * @param : String message 다이얼로그 메세지 설정
     * @return : 아래 확인 취소 버튼 이벤트를 변수 지정없이 설정
     * @date : 2012.04.06
     */
    public Dialog twoButtonDialog(final int tag, String title, String message) {
        return twoButtonDialog(tag, title, message, true);
    }

    public Dialog twoButtonDialog(final int tag, String title, String message, boolean isTwoButton) {
        AlertDialog.Builder builder;
        if (DeviceInfo.checkPlatformVersion()) {
            builder = new AlertDialog.Builder(this, AlertDialog.THEME_HOLO_LIGHT);
        } else {
            builder = new AlertDialog.Builder(this);
        }

        builder.setCancelable(false);
        builder.setTitle(title);
        builder.setMessage(message);
        //		builder.setView(myView);
        builder.setPositiveButton(R.string.btn_ok,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub

                        if (tag == Constants.MAINACTIVITY_DESTROY_ACTIVITY) {
                            try {
                                setDestroyList();
                                StatusBoardHelper.destroy();
                            } catch (NullPointerException e) {
                                if (Constants.DEBUG_PRINT_LOG) {
                                    e.printStackTrace();
                                } else {
                                    System.out.println("예외 발생");
                                }
                            }
                        } else {
                            onBasicDlgItemSelected(tag, true);
                        }
                    }
                });

        if (isTwoButton) {
            builder.setNegativeButton(R.string.btn_cancel, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    onBasicDlgItemSelected(tag, false);
                }
            });
        }

        builder.setOnCancelListener(new OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                if (dialog != null) {
                    dialog.cancel();
                    dialog.dismiss();
                }

            }
        });


        return builder.create();
    }

    /**
     * @return : 다이얼로그 ID를 반환 받아 콜백 이벤트를 사용할 수 있다.
     * @date : 2012.04.06
     */
    public void onBasicDlgItemSelected(int tag, boolean choice) {
    }


    public void setCommonItem(String codeClass, int type, TextView btn) {
        setCommonItem(codeClass, type, btn, 0);
    }

    /**
     * 코드 설정 정보가 변경되면 인덱스로 처리하는 부분에 문제가 생기므로 @deprecated 처리함
     */
    @deprecated
    public void setCommonItem(String codeClass, int type, TextView btn, int index) {
        if (codeJsonArr == null)
            return;

        int Len = codeJsonArr.length();
        for (int i = 0, j = 0; i < Len; i++) {
            try {
                JSONObject res = codeJsonArr.getJSONObject(i);
                if (codeClass.equals(res.optString("CD_CLASS"))) {
                    Log.d(TAG, " code >>> " + codeClass + " CD_CLASS >>> " + res.optString("CD_CLASS") + " CD_CODE >>>> " + res.optString("CD_CODE") + " KN_CODE >>>> " + res.optString("KN_CODE"));
                    if (j == index) {
                        btn.setText(res.optString("KN_CODE"));
                        break;
                    }
                    j++;
                }
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                if (Constants.DEBUG_PRINT_LOG) {
                    e.printStackTrace();
                } else {
                    System.out.println("예외 발생");
                }
            }
        }  // end for

    }

    // codeClass 및 code 비교
    public void setCommonItem(String codeClass, int type, TextView btn, String code) {
        if (codeJsonArr == null)
            return;

        int Len = codeJsonArr.length();
        for (int i = 0; i < Len; i++) {
            try {
                JSONObject res = codeJsonArr.getJSONObject(i);
                if (codeClass.equals(res.optString("CD_CLASS"))) {
                    // Log.d(TAG, " CD_CODE >>>> " + res.optString("CD_CODE") +  " KN_CODE >>>> " + res.optString("KN_CODE"));
                    if (code.equals(res.optString("CD_CODE"))) {
                        btn.setText(res.optString("KN_CODE"));
                        break;
                    }
                }
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                if (Constants.DEBUG_PRINT_LOG) {
                    e.printStackTrace();
                } else {
                    System.out.println("예외 발생");
                }
            }
        }  // end for
    }

    public void setCommonDialog(String title, String codeClass, int type, TextView btn) {
        setCommonDialog(title, codeClass, type, btn, null);
    }


    public void setCommonDialog_parkType(String title, String codeClass, int type, TextView btn, Object targetObj) {
        if (codeJsonArr == null)
            return;

        ArrayList<String> dList = new ArrayList<String>();
        int Len = codeJsonArr.length();
        for (int i = 0; i < Len; i++) {
            try {
                JSONObject res = codeJsonArr.getJSONObject(i);
                if (codeClass.equals(res.optString("CD_CLASS"))) {
                    Log.d(TAG, " CD_CODE >>>> " + res.optString("CD_CODE") + " KN_CODE >>>> " + res.optString("KN_CODE") + "~~" + codeClass);
                    if (("02").equals(res.optString("CD_CODE")) || ("04").equals(res.optString("CD_CODE"))) {
                        dList.add(res.optString("KN_CODE"));
                    }
                }
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                if (Constants.DEBUG_PRINT_LOG) {
                    e.printStackTrace();
                } else {
                    System.out.println("예외 발생");
                }
            }
        }  // end for

        if (targetObj != null) {
            basicListDialog(type, title, dList, btn, targetObj);
        } else {
            basicListDialog(type, title, dList, btn);
        }
    }


    public void setCommonDialog_DAY(String title, String codeClass, int type, TextView btn, Object targetObj) {
        if (codeJsonArr == null)
            return;

        ArrayList<String> dList = new ArrayList<String>();
        int Len = codeJsonArr.length();
        for (int i = 0; i < Len; i++) {
            try {
                JSONObject res = codeJsonArr.getJSONObject(i);
                if (codeClass.equals(res.optString("CD_CLASS"))) {
                    //   Log.d(TAG, " CD_CODE >>>> " + res.optString("CD_CODE") +  " KN_CODE >>>> " + res.optString("KN_CODE"));
                    if (res.optString("CD_CODE").equals("01") || res.optString("CD_CODE").equals("04") || res.optString("CD_CODE").equals("19") || res.optString("CD_CODE").equals("20"))
                        dList.add(res.optString("KN_CODE"));
                }
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                if (Constants.DEBUG_PRINT_LOG) {
                    e.printStackTrace();
                } else {
                    System.out.println("예외 발생");
                }
            }
        }  // end for

        if (targetObj != null) {
            basicListDialog(type, title, dList, btn, targetObj);
        } else {
            basicListDialog(type, title, dList, btn);
        }
    }


    public void setCommonDialog(String title, String codeClass, int type, TextView btn, Object targetObj) {
        if (codeJsonArr == null)
            return;

        ArrayList<String> dList = new ArrayList<String>();
        int Len = codeJsonArr.length();
        for (int i = 0; i < Len; i++) {
            try {
                JSONObject res = codeJsonArr.getJSONObject(i);
                if (codeClass.equals(res.optString("CD_CLASS"))) {
                    Log.d(TAG, " CD_CODE >>>> " + res.optString("CD_CODE") + " KN_CODE >>>> " + res.optString("KN_CODE") + "~~" + codeClass);

                    if ("DC".equals(res.optString("CD_CLASS")) && ("13".equals(res.optString("CD_CODE")) || "14".equals(res.optString("CD_CODE")))) {
                        Log.e("CD_CLASS DC 13,14", "PASS");
                    } else {
                        dList.add(res.optString("KN_CODE"));
                    }


                }
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                if (Constants.DEBUG_PRINT_LOG) {
                    e.printStackTrace();
                } else {
                    System.out.println("예외 발생");
                }
            }
        }  // end for

        if (targetObj != null) {
            basicListDialog(type, title, dList, btn, targetObj);
        } else {
            basicListDialog(type, title, dList, btn);
        }
    }

    protected void deletePicture(String carNumber) {

        PictureHelper pictureHelper = PictureHelper.getInstance(this);
        ArrayList<PictureInfo> list = (ArrayList<PictureInfo>) pictureHelper
                .getSearchList(today, carNumber, Constants.PICTURE_TYPE_PP);
        Log.i("test", "carnumber[" + carNumber + "] count :" + list.size());

        for (PictureInfo info : list) {
            Log.i("test", "Delete pic : " + info.getPath());
            File f = new File(info.getPath());
            if (f.delete()) {
                PictureHelper helper = PictureHelper.getInstance(this);
                helper.delete(info.getPath());
            }
        }
    }

    protected void setCarnoView(String carno, Button btn_car_no_1, Button btn_car_no_2, Button btn_car_no_3, Button btn_car_no_4, Button btn_car_no_manual) {

        String car_no_1 = "";
        String car_no_2 = "";
        String car_no_3 = "";
        String car_no_4 = "";
        String car_no_manual = "";

        if (carno.length() == 7) {
            car_no_1 = "";
            car_no_2 = carno.substring(0, 2);
            car_no_3 = carno.substring(2, 3);
            car_no_4 = carno.substring(3, 7);
            car_no_manual = "";
            setCarnoView(car_no_1, car_no_2, car_no_3, car_no_4, car_no_manual,
                    btn_car_no_1, btn_car_no_2, btn_car_no_3, btn_car_no_4, btn_car_no_manual, Constants.CAR_NORMAL);
        } else if (carno.length() == 8) {
            car_no_1 = "";
            car_no_2 = carno.substring(0, 3);
            car_no_3 = carno.substring(3, 4);
            car_no_4 = carno.substring(4, 8);
            car_no_manual = "";
            setCarnoView(car_no_1, car_no_2, car_no_3, car_no_4, car_no_manual,
                    btn_car_no_1, btn_car_no_2, btn_car_no_3, btn_car_no_4, btn_car_no_manual, Constants.CAR_NORMAL);
        } else if (carno.length() == 9) {
            car_no_1 = carno.substring(0, 2);
            car_no_2 = carno.substring(2, 4);
            car_no_3 = carno.substring(4, 5);
            car_no_4 = carno.substring(5, 9);
            car_no_manual = "";
            setCarnoView(car_no_1, car_no_2, car_no_3, car_no_4, car_no_manual,
                    btn_car_no_1, btn_car_no_2, btn_car_no_3, btn_car_no_4, btn_car_no_manual, Constants.CAR_NORMAL);
        } else {
            car_no_1 = "";
            car_no_2 = "";
            car_no_3 = "";
            car_no_4 = "";
            car_no_manual = carno;
            setCarnoView(car_no_1, car_no_2, car_no_3, car_no_4, car_no_manual,
                    btn_car_no_1, btn_car_no_2, btn_car_no_3, btn_car_no_4, btn_car_no_manual, Constants.CAR_MANUAL);
        }
    }

    protected void setCarnoView(String carno_1, String carno_2, String carno_3, String carno_4, String carno_manual,
                                Button btn_car_no_1, Button btn_car_no_2, Button btn_car_no_3, Button btn_car_no_4, Button btn_car_no_manual,
                                String mode) {

        btn_car_no_1.setText(carno_1);
        btn_car_no_2.setText(carno_2);
        btn_car_no_3.setText(carno_3);
        btn_car_no_4.setText(carno_4);
        btn_car_no_manual.setText(carno_manual);

        if (Constants.CAR_NORMAL.equals(mode)) {
            btn_car_no_1.setVisibility(View.VISIBLE);
            btn_car_no_2.setVisibility(View.VISIBLE);
            btn_car_no_3.setVisibility(View.VISIBLE);
            btn_car_no_4.setVisibility(View.VISIBLE);
            btn_car_no_manual.setVisibility(View.GONE);
        } else if (Constants.CAR_MANUAL.equals(mode)) {
            btn_car_no_1.setVisibility(View.GONE);
            btn_car_no_2.setVisibility(View.GONE);
            btn_car_no_3.setVisibility(View.GONE);
            btn_car_no_4.setVisibility(View.GONE);
            btn_car_no_manual.setVisibility(View.VISIBLE);
        }


        Constants.KEY_CARNO_1 = carno_1;
        Constants.KEY_CARNO_2 = carno_2;
        Constants.KEY_CARNO_3 = carno_3;
        Constants.KEY_CARNO_4 = carno_4;
    }

    protected void goToMainActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
        finish();
    }

    protected void goToActivityInputCarNumber(int requestCode, String type, int index) {
        Intent intent = new Intent(this, InputCarNumerActivity.class);
        intent.putExtra(Constants.CAR_INPUT_MODE, type);
        intent.putExtra("index", "" + index);
        startActivityForResult(intent, requestCode);
    }

    protected void goToPictureView(String pioNum, String carNo, String pictureType, String parkCode, String empCode) {
        Intent i = new Intent(this, CarPictureViewActivity.class);
        i.putExtra("pioNum", pioNum);
        i.putExtra("carNo", carNo);
        i.putExtra("pictureType", pictureType);
        i.putExtra("parkCode", parkCode);
        i.putExtra("empCode", empCode);
        startActivityForResult(i, Constants.RESULT_CAR_PICTURE);
    }

    protected void goToPictureViewUnpay(String pioNum, String carNo, String pictureType, String parkCode, String empCode) {
        Intent i = new Intent(this, CarPictureViewActivity.class);
        i.putExtra("pioNum", pioNum);
        i.putExtra("carNo", carNo);
        i.putExtra("pictureType", pictureType);
        i.putExtra("parkCode", parkCode);
        i.putExtra("mode", "fix");
        startActivityForResult(i, Constants.RESULT_CAR_PICTURE);
    }

    protected void goToTicketPictureView(String ticketNo, String carNo, String pictureType, String parkCode, String empCode) {
        Intent i = new Intent(this, TicketPictureViewActivity.class);
        i.putExtra("ticketNo", ticketNo);
        i.putExtra("carNo", carNo);
        i.putExtra("pictureType", pictureType);
        i.putExtra("parkCode", parkCode);
        i.putExtra("empCode", empCode);
        startActivityForResult(i, Constants.RESULT_CAR_PICTURE);
    }

    public void goToUnpaidManagerActivity(String carNum) {
        Intent i = new Intent(this, UnpaidManagerActivity.class);
        i.putExtra("finish", true);
        i.putExtra("carNo", carNum);
        startActivityForResult(i, 7777);
    }

    //******** 현금영수증 (McPay)   ***********************//
    private void doCashAuthPay(String strApprovalNo, boolean type, ReceiptVo vo, int money, String goodsname, int tic) // 현금영수증승인요청
    {
        mPrintService.stop();
        Intent i = new Intent(this, McPayActivity.class);
        i.putExtra("SHOW_DIALOG_TYPE", PayData.PAY_DIALOG_TYPE_CASH_RECEIPT);
        i.putExtra("PAYMENT_MONEY", String.valueOf(money));
        i.putExtra("PARK_TYPE", "");
        i.putExtra("ReceiptVo", vo);
        i.putExtra("TRANSACTION_NO", "");
        i.putExtra("CASHRECEIPT_NO", strApprovalNo);
        i.putExtra("CashTradeType", type);
        startActivityForResult(i, PayData.PAY_DIALOG_TYPE_CASH_RECEIPT);

        g_vo = vo;
    }


    //******** 신용카드 결제 (McPay)   ***********************//
    protected void goToPayActivity(int dType, String money, int monthly, String parkType, ReceiptVo vo) {
        //Intent i = new Intent(this, PayActivity.class);
        mPrintService.stop();
        gReceiptVo = vo;
        Intent i = new Intent(this, McPayActivity.class);
        i.putExtra("SHOW_DIALOG_TYPE", dType);
        i.putExtra("PAYMENT_MONEY", money);
        i.putExtra("PARK_TYPE", parkType);
        i.putExtra("ReceiptVo", vo);
        i.putExtra("TRANSACTION_NO", "");
        i.putExtra("MONTHLY_PAY", monthly);
        startActivityForResult(i, dType);
    }


    //******** 신용카드 결제  취소 (Mcpay)  ***********************//
    protected void goToPayCancelActivity(int dType, String money, String approvalNo, String approvalDate, ReceiptVo vo) {
        //Intent i = new Intent(this, PayActivity.class);
        mPrintService.stop();
        gReceiptVo = vo;
        Intent i = new Intent(this, McPayActivity.class);
        i.putExtra("SHOW_DIALOG_TYPE", dType);
        i.putExtra("PAYMENT_MONEY", money);
        i.putExtra("APPROVAL_NO", approvalNo);
        i.putExtra("APPROVAL_DATE", approvalDate);
        i.putExtra("ReceiptVo", vo);
        Log.d(TAG, " approvalNo   ::: " + approvalNo);
        Log.d(TAG, " approvalDate ::: " + approvalDate);
        startActivityForResult(i, dType);
    }


    protected void goToExitCommitActivity(String _para) {
		/*
    	Intent i = new Intent(this, ExitCommitActivity.class);
		i.putExtra("ParkIO", _para);
		startActivity(i);
		i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |Intent.FLAG_ACTIVITY_SINGLE_TOP);
		*/
    }


    @deprecated
    protected void goToCashReceiptActivity(int dType, String cashAmt) {
    }

    @deprecated
    protected void goToCashReceiptCancelActivity(int dType, String cashAmt, String approvalNo, String approvalDate) {
    }

    public static String makePIONumber(String codePark, String empCode, String parkDay) {
        StringBuffer sb = new StringBuffer();
        sb.append("PA").append(codePark).append("-")
                .append(empCode).append("-")
                .append(parkDay).append("-")
                .append(Util.getYmdhms("HHmmss"));
        return sb.toString();
    }

    public static String makeTicketNumber(String codePark, String empCode, String parkDay) {
        StringBuffer sb = new StringBuffer();
        sb.append("TA").append(codePark).append("-")
                .append(empCode).append("-")
                .append(parkDay).append("-")
                .append(Util.getYmdhms("HHmmss"));
        return sb.toString();
    }

    public static String makeCouponNumber(String codePark, String empCode, String parkDay) {
        StringBuffer sb = new StringBuffer();
        sb.append("CO").append(codePark).append("-")
                .append(empCode).append("-")
                .append(parkDay).append("-")
                .append(Util.getYmdhms("HHmmss"));
        return sb.toString();
    }

    /**
     * @return : 해당 어플의 켜져있는 모든 activity를 종료 시킨다./카테고리 초기화/잠금화면 종료 시간 저장
     * @date : 2012.04.10
     */
    protected void setDestroyList() {
        if (ActList == null) return;

        HashSet<Activity> hs = new HashSet<Activity>(ActList);
        Iterator<Activity> it = hs.iterator();
        while (it.hasNext()) {
            Activity mAct = it.next();
            Log.e(TAG, mAct.getLocalClassName());
            mAct.finish();
            mAct.overridePendingTransition(0, 0);
        }
    }

    @Override
    public void startActivity(Intent intent) {
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        super.startActivity(intent);
        overridePendingTransition(0, 0);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, final Intent intent) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, intent);

        Bundle eB = null;

        switch (resultCode) {
            case Constants.COMMON_TYPE_CARD_PAYMENT_FAIL:
                Log.d(TAG, " Exception ::: COMMON_TYPE_CARD_PAYMENT_FAIL");
                break;

            case PayData.PAY_DIALOG_TYPE_EXIT_CARD:
            case PayData.PAY_DIALOG_TYPE_PREPAY_CARD:
            case PayData.PAY_DIALOG_TYPE_UNPAY_CARD:
            case PayData.PAY_DIALOG_TYPE_TICKET_CARD:
                String transactionNo = "";
                String approvalNo = "";
                String approvalDate = "";
                String cardNo = "";
                String cardAmt = "";
                String cardCompany = "";
//			String pioNum 	     = "";
//			String carNum 	     = "";			  
                try {
                    if (intent != null) {
                        eB = intent.getExtras();
                        transactionNo = Util.replaceNullIntent(eB, "transactionNo");
                        approvalNo = Util.replaceNullIntent(eB, "approvalNo");
                        approvalDate = Util.replaceNullIntent(eB, "approvalDate");
                        cardNo = Util.replaceNullIntent(eB, "cardNo");
                        cardCompany = Util.replaceNullIntent(eB, "cardCompany");
                        cardAmt = Util.isNVL(Util.replaceNullIntent(eB, "cardAmt"), "0");

                        if (cardNo.length() >= 12) {
                            cardNo = cardNo.substring(0, 4) + cardNo.substring(12);
                        }
                    }
                } catch (NullPointerException e) {
                    System.out.println("예외 발생");
                }

                // 서버 결제 로그 저장 step2
                String url = Constants.API_SERVER + "?" + Constants.METHOD + "=" + Constants.INTERFACEID_LOG_PARAMETER;
                String param = "&transactionNo=" + transactionNo
                        + "&approvalNo=" + approvalNo
                        + "&approvalDate=" + approvalDate
                        + "&cardNo=" + cardNo
                        + "&cardCompany=" + cardCompany
                        + "&cardAmt=" + cardAmt
                        + "&class=" + this.getClass().getSimpleName()
                        + "&type=pay_step2"
                        + "&" + Constants.API_FORMAT + "=" + Constants.JSON_FORMAT;
                Log.d(TAG, " apiParkLogCall step 2 url >>> " + url + param);

                AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>();
                cb.url(url + param).type(JSONObject.class).weakHandler(this, "").redirect(true).retry(3).fileCache(false).expire(-1);
                AQuery aq = new AQuery(this);
                aq.ajax(cb);

                if (exitDialog != null) {
                    exitDialog.dismiss();
                    ExitPaymentData exitVo = new ExitPaymentData();
                    exitVo.setTransactionNo(transactionNo);
                    exitVo.setApprovalNo(approvalNo);
                    exitVo.setApprovalDate(approvalDate);
                    exitVo.setCardNo(cardNo);
                    exitVo.setCardCompany(cardCompany);
                    exitVo.setCardAmt(cardAmt);
                    exitVo.setCashAmt("0");
                    exitVo.setCatNumber(catNumber);

                    request(PayData.PAY_DIALOG_TYPE_EXIT_CARD, exitVo);
                }

                if (prepayDialog != null) {
                    prepayDialog.dismiss();
                    PayVo payVo = new PayVo();
                    payVo.setTransactionNo(transactionNo);
                    payVo.setApprovalNo(approvalNo);
                    payVo.setApprovalDate(approvalDate);
                    payVo.setCardNo(cardNo);
                    payVo.setCardCompany(cardCompany);
                    payVo.setCardAmt(cardAmt);
                    payVo.setCashAmt("0");
                    payVo.setCatNumber(catNumber);
                    request(PayData.PAY_DIALOG_TYPE_PREPAY_CARD, payVo);
                }

                if (unpayDialog != null) {
                    unpayDialog.dismiss();
                    PayVo payVo = new PayVo();
                    payVo.setTransactionNo(transactionNo);
                    payVo.setApprovalNo(approvalNo);
                    payVo.setApprovalDate(approvalDate);
                    payVo.setCardNo(cardNo);
                    payVo.setCardCompany(cardCompany);
                    payVo.setCardAmt(cardAmt);
                    payVo.setCashAmt("0");
                    payVo.setCatNumber(catNumber);

                    request(PayData.PAY_DIALOG_TYPE_UNPAY_CARD, payVo);
                }

                if (ticketDialog != null) {
                    ticketDialog.dismiss();
                    PayVo payVo = new PayVo();
                    payVo.setTransactionNo(transactionNo);
                    payVo.setApprovalNo(approvalNo);
                    payVo.setApprovalDate(approvalDate);
                    payVo.setCardNo(cardNo);
                    payVo.setCardCompany(cardCompany);
                    payVo.setCardAmt(cardAmt);
                    payVo.setCashAmt("0");
                    payVo.setCatNumber(catNumber);

                    request(PayData.PAY_DIALOG_TYPE_TICKET_CARD, payVo);
                }
                break;
            case PayData.PAY_DIALOG_TYPE_CASH_RECEIPT:
                String cashApprovalNo = "";
                String cashApprovalDate = "";
                try {
                    if (intent != null) {
                        eB = intent.getExtras();
                        cashApprovalNo = Util.replaceNullIntent(eB, "cashApprovalNo");
                        cashApprovalDate = Util.replaceNullIntent(eB, "cashApprovalDate");
                    }
                    if (!Util.isEmpty(cashApprovalNo)) {
                        PayVo payVo = new PayVo();
                        payVo.setCashApprovalNo(cashApprovalNo);
                        payVo.setCashApprovalDate(cashApprovalDate);
                        tvCashBill.setTag(payVo);
                        tvCashBill.setText("발행");
                    }
                } catch (NullPointerException e) {
                    System.out.println("예외 발생");
                }
                break;
            case PayData.PAY_DIALOG_CANCEL_APPROVAL_EXIT:
                ExitPaymentData epd = new ExitPaymentData();
                request(PayData.PAY_DIALOG_CANCEL_APPROVAL_EXIT, epd);
                break;

            case PayData.PAY_DIALOG_CANCEL:
                setToast("취소되었습니다.");
                break;

        }
    }

    /**********************************************************************************************
     * 결재 화면
     **********************************************************************************************/

    public Dialog exitDialog;
    public Dialog prepayDialog;
    public Dialog unpayDialog;
    public Dialog ticketDialog;
    public Dialog cashReceiptDialog;
    public ReceiptVo g_vo;
    private String g_cashRecipt_route = "DIRECT";

    // 총 쿠폰수량
    private Button btn_coupon_qty;
    // 총 쿠폰금액
    public TextView tv_coupon_totalamt;
    // 납부금액
    private TextView tvPayAmt;

    // 미수금액
    private TextView tvUnpayAmt;

    // 결제금액
    public TextView tvPayTotalAmt;
    public Button btnPayCard;//쿠폰추가
    // 현금영수증 발행 여부
    public TextView tvCashBill;

    private EditText edSetNumber;
    private boolean gType = false;

    /**
     * 다이얼로그의 콜백 함수..
     * 하위 액티비티에서 오버라이딩 해서 사용
     *
     * @param data
     */
    protected void request(int type, Object data) {

    }
    private int orgExitPayment = 0;
    private boolean isAddDis = false;
    private int ADJ_AMT = 0;
    public Dialog exitDialog(final String parkType
            , final String payAmt        // 납부요금
            , final ReceiptVo vo
            , final String unPayAmt) {

        exitDialog = new Dialog(this,
                android.R.style.Theme_Holo_Light_Dialog);
        exitDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        exitDialog.setContentView(R.layout.pay_dialog_exit);

        orgExitPayment = Integer.parseInt(payAmt.replace("원", "").trim());

        tvPayAmt = (TextView) exitDialog.findViewById(R.id.tvPayAmt);
        tvPayTotalAmt = (TextView) exitDialog.findViewById(R.id.tvPayTotalAmt);
        btn_coupon_qty = (Button) exitDialog.findViewById(R.id.btn_coupon_qty);
        tv_coupon_totalamt = (TextView) exitDialog.findViewById(R.id.tv_coupon_totalamt);
        tvUnpayAmt = (TextView) exitDialog.findViewById(R.id.tvUnpayAmt);
        Button btn_cash = (Button) exitDialog.findViewById(R.id.btn_cash);
        btnPayCard = (Button) exitDialog.findViewById(R.id.btn_card);
        Button btn_cancel = (Button) exitDialog.findViewById(R.id.btn_cancel);
        final Button btn_add_dis = (Button) exitDialog.findViewById(R.id.btn_add_dis);
        Preferences.putValue(mContext, Constants.PREFERENCE_UNPAY_AMT, "0");
        btn_unpayAver = (Button) exitDialog.findViewById(R.id.btn_unpayAver);
        btn_unpayAver.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                btn_unpayAver.setSelected(!btn_unpayAver.isSelected());


                btn_add_dis.setText("제휴 X");
                btn_add_dis.setBackgroundResource(R.drawable.btn_basic_press);
                isAddDis = false;
                ADJ_AMT = 0;


                if (btn_unpayAver.isSelected()) {
                    Toast.makeText(mContext, "미수금이 합산 되었습니다.", Toast.LENGTH_LONG).show();
                    orgExitPayment += Integer.parseInt(unPayAmt);
                    tvPayTotalAmt.setTextColor(Color.RED);
                    Preferences.putValue(mContext, Constants.PREFERENCE_UNPAY_AMT, unPayAmt);
                } else {
                    Toast.makeText(mContext, "미수금이 합산이 취소 되었습니다.", Toast.LENGTH_LONG).show();
                    orgExitPayment -= Integer.parseInt(unPayAmt);
                    tvPayTotalAmt.setTextColor(Color.BLACK);
                    Preferences.putValue(mContext, Constants.PREFERENCE_UNPAY_AMT, "0");
                }

                Preferences.putValue(mContext, Constants.PREFERENCE_ADJUST_AMT, String.valueOf(ADJ_AMT));
                tvPayTotalAmt.setText(String.valueOf(orgExitPayment));
            }
        });


        if ("".equals(unPayAmt)||"0".equals(unPayAmt)) {
            btn_unpayAver.setVisibility(View.GONE);
        } else {
            btn_unpayAver.setVisibility(View.VISIBLE);
        }


        btn_add_dis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int couponAmt = Util.sToi(tv_coupon_totalamt.getText().toString().replace("원", ""));
                double disAmt = orgExitPayment - couponAmt;
                if (disAmt <= 0) {
                    return;
                }


                if (!isAddDis) {
                    btn_add_dis.setText("제휴 O");
                    btn_add_dis.setBackgroundResource(R.color.table_row_red_text);
                    isAddDis = true;

                    int disAdjustAmt = discountChargeRate(String.valueOf(disAmt), "5");
                    double diff = Math.floor((disAmt - disAdjustAmt) / disAdj) * disAdj;
                    ADJ_AMT = (int) (disAmt - diff);
                    disAmt = diff;
                } else {
                    btn_add_dis.setText("제휴 X");
                    btn_add_dis.setBackgroundResource(R.drawable.btn_basic_press);
                    isAddDis = false;
                    ADJ_AMT = 0;
                }

                Preferences.putValue(mContext, Constants.PREFERENCE_ADJUST_AMT, String.valueOf(ADJ_AMT));
                tvPayTotalAmt.setText(String.valueOf((int) disAmt));
            }
        });


        if (Constants.isCardAuth) {
            btnPayCard.setEnabled(true);
        } else {
            btnPayCard.setEnabled(false);
        }


        if (orgExitPayment <= 0) {
            btn_cash.setText("확인");
            btnPayCard.setVisibility(View.GONE);
        }

        tvPayAmt.setText(payAmt);
        tvPayTotalAmt.setText("" + orgExitPayment);
        tvUnpayAmt.setText("        " + Util.addComma(unPayAmt));

        btn_coupon_qty.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                btn_add_dis.setText("제휴 X");
                btn_add_dis.setBackgroundResource(R.drawable.btn_basic_press);
                isAddDis = false;
                ADJ_AMT = 0;
                Preferences.putValue(mContext, Constants.PREFERENCE_ADJUST_AMT, String.valueOf(ADJ_AMT));
                tvPayTotalAmt.setText(String.valueOf((int) orgExitPayment));

                // 쿠폰 다이어로그
                couponDialog(orgExitPayment);
            }
        });

        btn_cash.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((Button) v).getText().toString().equals("현금")) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(BaseActivity.this);
                    builder.setTitle("결제방식").setMessage("결제금액 : " + tvPayTotalAmt.getText() + "\n현금으로 결제하시겠습니까?");
                    builder.setPositiveButton(R.string.btn_ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            cashReceiptDialog(orgExitPayment, vo, parkType);
                            dialog.dismiss();
                        }
                    }).setNegativeButton(R.string.btn_cancel, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    AlertDialog alert = builder.create();
                    alert.show();

                } else {
                    cashReceiptDialog(orgExitPayment, vo, parkType);
                }
            }
        });

        btnPayCard.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(BaseActivity.this);
                builder.setTitle("결제방식").setMessage("결제금액 : " + tvPayTotalAmt.getText() + "\n신용카드로 결제하시겠습니까?");
                builder.setPositiveButton(R.string.btn_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ((ReceiptType4Vo) vo).setCouponAmt(tv_coupon_totalamt.getText().toString());
                        goToPayActivity(PayData.PAY_DIALOG_TYPE_EXIT_CARD, tvPayTotalAmt.getText().toString().replaceAll("원", ""), 0, parkType, vo);
                        dialog.dismiss();
                    }
                }).setNegativeButton(R.string.btn_cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                AlertDialog alert = builder.create();
                alert.show();


            }
        });

        btn_cancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                MsgUtil.ToastMessage(BaseActivity.this, "취소되었습니다.");
                exitDialog.dismiss();
                exitDialog = null;
            }
        });

        exitDialog.show();
        return exitDialog;
    }

    public void cashReceiptDialog(final int orgPayment, final ReceiptVo vo, String goodsname) {
        // 현금 영수증 사용여부... 사용 안 하면 바로 결제처리
        if (Constants.isCashReceipt) {
            if (orgPayment <= 0) {
                cashReceiptRun(orgPayment, vo);
            } else {
                cashReceiptShowDialog(orgPayment, vo, goodsname, "DIRECT");
            }
        } else {
            cashReceiptRun(orgPayment, vo);
        }
    }

    //현금 영수증 Dialog
    public void cashReceiptShowDialog(final int orgPayment, final ReceiptVo vo, final String goodsname, final String _tic) {

        g_cashRecipt_route = _tic;

        cashReceiptDialog = new Dialog(this,
                android.R.style.Theme_Holo_Light_Dialog);


        //cashReceiptDialog.setContentView(R.layout.pay_dialog_cash_receipt_);
        cashReceiptDialog.setContentView(R.layout.dialog_cash_receipt);
        cashReceiptDialog.setTitle("현금영수증");

        Button btnCashreceiptConfirm = (Button) cashReceiptDialog.findViewById(R.id.btnCashreceiptConfirm);
        Button btnCashreceiptCancel = (Button) cashReceiptDialog.findViewById(R.id.btnCashreceiptCancel);
        Button btn_cancel = (Button) cashReceiptDialog.findViewById(R.id.btn_cancel);
        TextView tvCash = (TextView) cashReceiptDialog.findViewById(R.id.tvCash);
        tvCash.setText(Util.addComma(String.valueOf(orgPayment)) + "원");
        edSetNumber = (EditText) cashReceiptDialog.findViewById(R.id.edSetNumber);
        RadioGroup rg_cash = (RadioGroup) cashReceiptDialog.findViewById(R.id.rg_cash);
        edSetNumber.setHint("전화번호를 입력하세요.");
        tvCashBill = (TextView) cashReceiptDialog.findViewById(R.id.tvCashBill);
        tvCashBill.setText("미발행");

        gType = true;
        rg_cash.setOnCheckedChangeListener(new android.widget.RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // TODO Auto-generated method stub
                switch (checkedId) {
                    case R.id.rb_phone:
                        gType = true;
                        edSetNumber.setHint("전화번호를 입력하세요.");
                        break;
                    case R.id.rb_card:
                        gType = false;
                        edSetNumber.setHint("사업자번호를 입력하세요.");
                        break;
                }
            }
        });
        ;
		
		/*
		tvCashBill        = (TextView) cashReceiptDialog.findViewById(R.id.tvCashBill);
		Button btn_ok 	  = (Button) cashReceiptDialog.findViewById(R.id.btn_ok);
		Button btn_cancel = (Button) cashReceiptDialog.findViewById(R.id.btn_cancel);
		
		tvCashBill.setText("미발행");
		tvCashBill.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				goToCashReceiptActivity(PayData.PAY_DIALOG_TYPE_CASH_RECEIPT, tvPayTotalAmt.getText().toString().replace("원",""));
			}
		});
		*/
        btnCashreceiptConfirm.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (edSetNumber.getText().toString().equals("")) {
                    setToast("입력된 값이 없습니다. 다시 입력해 주세요.");
                    return;
                }

                doCashAuthPay(edSetNumber.getText().toString(), gType, vo, orgPayment, goodsname, 0);

            }
        });

        btnCashreceiptCancel.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (edSetNumber.getText().toString().equals("")) {
                    setToast("입력된 값이 없습니다. 다시 입력해 주세요.");
                    return;
                }
                doCashAuthPay(edSetNumber.getText().toString(), gType, vo, orgPayment, goodsname, 1);

            }
        });

        btn_cancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                //MsgUtil.ToastMessage(BaseActivity.this, "취소되었습니다.");
                if (("DIRECT").equals(g_cashRecipt_route)) {
                    cashReceiptRun(orgPayment, vo);
                }
                cashReceiptDialog.dismiss();
                cashReceiptDialog = null;

            }
        });

        cashReceiptDialog.show();
    }

    private void doCashAuthPay2(String strApprovalNo, boolean type, ReceiptVo vo, int money, String goodsname, int tic) // 현금영수증승인요청
    {

        Map<String, Object> params = new HashMap<String, Object>();
        params.put(Constants.API_FORMAT, Constants.JSON_FORMAT);
        params.put("id", "ANDROID");
        params.put("act", "PAY_ACTION");
        params.put("EMP_CD", new Preference(this).getString(Constants.PREFERENCE_LOGIN_ID, ""));
        params.put("PWD", new Preference(this).getString(Constants.PREFERENCE_LOGIN_PWD, ""));
        params.put("DEVICE_ID", Util.getDeviceID(this));

        if (tic == 0) {
            //[[[[공통정보설정]]]]
            //주문번호
            params.put("ordr_idxx", Util.getYmdhms("yyyyMMdd_HHmmss") + "_" + vo.getCarNum());
            //상품명
            params.put("good_name", goodsname);
            // 주문자명
            params.put("buyr_name", vo.getCarNum());
            // 요청종류 승인(pay)
            params.put("req_tx", "pay");


            params.put("trad_time", Util.getYmdhms("yyyyMMddHHmmss"));
            if (type)
                params.put("tr_code", "0");
            else
                params.put("tr_code", "1");
            params.put("id_info", strApprovalNo);
            params.put("amt_tot", money);
            params.put("amt_sup", money);
            params.put("amt_svc", 0);
            params.put("amt_tax", 0);
            params.put("corp_type", "0");
        }

        g_vo = vo;

        //showProgressDialog(false);
        //String url = Constants.PAY_SERVER;
        String url = Constants.CASH_RECEIPT_SERVER;
        //Log.d(TAG, " apiAuthPaymentCall url >>> " + url);
        Log.d(TAG, " apiAuthPaymentCall url >>> " + url + " params ::: " + params);


        AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>();
        cb.url(url).params(params).type(JSONObject.class).weakHandler(this, "authCashreceiptCallback").redirect(true).retry(3).fileCache(false).expire(-1);
        AQuery aq = new AQuery(this);
        aq.ajax(cb);

    }

    public void authCashreceiptCallback(String url, JSONObject json, AjaxStatus status) {

        closeProgressDialog();

        Log.d(TAG, " authPaymentCallback url >>> " + url);
        Log.d(TAG, " authPaymentCallback  json ====== " + json);

        // successful ajax call
        if (json != null) {
            String res_msg = json.optString("res_msg");
            if ("0000".equals(json.optString("res_cd"))) {
                PayVo payVo = new PayVo();  //현금영수증참조
                payVo.setCashApprovalNo(json.optString("receipt_no"));
                payVo.setCashApprovalDate(json.optString("app_time"));
                tvCashBill.setTag(payVo);
                tvCashBill.setText("발행");
                ExitPaymentData exitVo = new ExitPaymentData();

                if (("DIRECT").equals(g_cashRecipt_route)) {
                    cashReceiptRun(Util.sToi(json.optString("amt_tot")), g_vo);
                } else {
                    exitVo.setCashApprovalNo(json.optString("receipt_no"));
                    exitVo.setCashApprovalDate(json.optString("app_time"));
                    request(PayData.PAY_DIALOG_TYPE_CASH_RECEIPT, exitVo);
                    if (cashReceiptDialog != null) {
                        cashReceiptDialog.dismiss();
                        cashReceiptDialog = null;
                    }
                    g_vo = null;
                    tvCashBill = null;
                }
		/*
           } else if("RCF8".equals(json.optString("res_cd"))){
        	   ExitPaymentData exitVo = new ExitPaymentData();
        	   exitVo.setCashApprovalNo(json.optString("receipt_no"));
        	   exitVo.setCashApprovalDate(json.optString("app_time"));
        	   request(PayData.PAY_DIALOG_TYPE_CASH_RECEIPT, exitVo);
        	   if(cashReceiptDialog != null) {
				cashReceiptDialog.dismiss();
				cashReceiptDialog = null;
        	   }
			   g_vo = null;
			   tvCashBill = null;	
		  */
            } else {
                // 승인요청 버튼 활성화
                // btn_sign.setEnabled(true);
                AlertDialog.Builder alert = new AlertDialog.Builder(this);
                alert.setTitle("");
                alert.setMessage(res_msg);
                alert.setPositiveButton(R.string.btn_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //dialog.dismiss();
                        //activity.finish();
                    }
                });
                alert.show();

                g_vo = null;
                return;
            }
        } else {
            MsgUtil.ToastMessage(this, Constants.DATA_FAIL, Toast.LENGTH_LONG);
        }

    }

    public void cashReceiptRun(final int orgPayment, final ReceiptVo vo) {
        // 출차등록에서 현금영수증 처리
        if (exitDialog != null) {

            String payAmt = tvPayTotalAmt.getText().toString().replace("원", "");
            int couponAmt = Util.sToi(tv_coupon_totalamt.getText().toString().replace("원", ""));
            int sumTotal = Util.sToi(payAmt) + couponAmt;

//			Log.d(TAG, " orgPayment >>> " + orgPayment);
//			Log.d(TAG, " payAmt >>> " + payAmt);
//			Log.d(TAG, " couponAmt >>> " + couponAmt);
//			Log.d(TAG, " sumTotal >>> " + sumTotal);

            if (orgPayment != 0 && sumTotal == 0) {
                setToast("입금된 금액이 없습니다. 다시 확인해 주세요.");
                return;
            } else if (sumTotal < orgPayment) {
                setToast("결제하려는 금액이 납부금액보다 작습니다.결제금액을 확인해 주세요.");
                return;
            }
            /**XXX 이전버전 2015.10.31 작업
             * 쿠폰금액 결제 로직이 추가되어 주석처리...
             } else if (sumTotal > orgPayment) {
             setToast("결제하려는 금액이 납부금액보다 큽니다.결제금액을 확인해 주세요.");
             return;
             }
             ***/

            ExitPaymentData exitVo = new ExitPaymentData();
            exitVo.setCashAmt(payAmt);
            exitVo.setTransactionNo("");
            exitVo.setApprovalNo("");
            exitVo.setApprovalDate("");
            exitVo.setCardNo("");
            exitVo.setCardCompany("");
            exitVo.setCardAmt("0");
            exitVo.setCouponAmt(tv_coupon_totalamt.getText().toString());
            if (orgPayment < 0) {
                exitVo.setReturnAmt(Util.toStr(Math.abs(orgPayment)));
            }

            // 영수증 관련 정보 수정
            if (vo instanceof ReceiptType4Vo) {
                ((ReceiptType4Vo) vo).setCouponAmt(exitVo.getCouponAmt());
                ((ReceiptType4Vo) vo).setPayAmt("" + payAmt); //위값은 납부금액으로 설정되어있네요
                ((ReceiptType4Vo) vo).setPrint(OUT_PRINT);

                if (tvCashBill != null && tvCashBill.getText().toString().equals("발행")) {
                    if (tvCashBill.getTag() != null) {
                        PayVo billVo = (PayVo) tvCashBill.getTag();
                        if (billVo != null) {
                            exitVo.setCashApprovalNo(billVo.getCashApprovalNo());
                            exitVo.setCashApprovalDate(billVo.getCashApprovalDate());
                            exitVo.setCatNumber(catNumber);
                            ((ReceiptType4Vo) vo).setConfirmCashReceipt(billVo.getCashApprovalNo());
                            cashReceiptDialog.dismiss();
                            cashReceiptDialog = null;
                            g_vo = null;
                            tvCashBill = null;
                        }
                    }
                } // end if 발행
            }

            request(PayData.PAY_DIALOG_TYPE_EXIT_CASH, exitVo);
            exitDialog.dismiss();
            exitDialog = null;
        }

        // 선납금등록에서 현금영수증 처리
        if (prepayDialog != null) {
            PayVo payVo = new PayVo();
            payVo.setCashAmt("" + orgPayment);
            payVo.setTransactionNo("");
            payVo.setApprovalNo("");
            payVo.setApprovalDate("");
            payVo.setCardNo("");
            payVo.setCardCompany("");
            payVo.setCardAmt("0");
            payVo.setCouponAmt(tv_coupon_totalamt.getText().toString().replace("원", "").replace(",", "")); //쿠폰추가

            if (tvCashBill != null && tvCashBill.getText().toString().equals("발행")) {
                if (tvCashBill.getTag() != null) {
                    PayVo billVo = (PayVo) tvCashBill.getTag();
                    if (billVo != null) {
                        payVo.setCashApprovalNo(billVo.getCashApprovalNo());
                        payVo.setCashApprovalDate(billVo.getCashApprovalDate());
                        payVo.setCatNumber(catNumber);
                        ((ReceiptType13Vo) vo).setCashReceipt(billVo.getCashApprovalNo());
                        cashReceiptDialog.dismiss();
                        cashReceiptDialog = null;
                        g_vo = null;
                        tvCashBill = null;
                    }
                }
            } // end if 발행
            request(PayData.PAY_DIALOG_TYPE_PREPAY_CASH, payVo);
            prepayDialog.dismiss();
            prepayDialog = null;
        }

        // 미수환수에서 현금영수증 처리
        if (unpayDialog != null) {

            String cashAmt = tvPayTotalAmt.getText().toString().replace("원", "");
            int sumTotal = Util.sToi(cashAmt);

            PayVo payVo = new PayVo();
            payVo.setCashAmt("" + sumTotal);
            payVo.setTransactionNo("");
            payVo.setApprovalNo("");
            payVo.setApprovalDate("");
            payVo.setCardNo("");
            payVo.setCardCompany("");
            payVo.setCardAmt("0");
            payVo.setCouponAmt(tv_coupon_totalamt.getText().toString().replace("원", "").replace(",", ""));


            if (sumTotal > 0) {
                if (sumTotal + Integer.valueOf(payVo.getCouponAmt()) < orgPayment) {
                    setToast("결제하려는 금액이 납부금액보다 작습니다.결제금액을 확인해 주세요.");
                    return;
                } else if (sumTotal > orgPayment) {
                    setToast("결제하려는 금액이 납부금액보다 큽니다.결제금액을 확인해 주세요.");
                    return;
                }
            }

            if (vo instanceof ReceiptType10Vo) {
                ((ReceiptType10Vo) vo).setCouponAmt(payVo.getCouponAmt());
                ((ReceiptType10Vo) vo).setTotalInputAmt(String.valueOf(orgPayment));

                if (tvCashBill != null && tvCashBill.getText().toString().equals("발행")) {
                    if (tvCashBill.getTag() != null) {
                        PayVo billVo = (PayVo) tvCashBill.getTag();
                        if (billVo != null) {
                            payVo.setCashApprovalNo(billVo.getCashApprovalNo());
                            payVo.setCashApprovalDate(billVo.getCashApprovalDate());
                            payVo.setCatNumber(catNumber);
                            ((ReceiptType10Vo) vo).setConfirmCashReceipt(billVo.getCashApprovalNo());
                            cashReceiptDialog.dismiss();
                            cashReceiptDialog = null;
                            g_vo = null;
                            tvCashBill = null;
                        }
                    }
                } // end if 발행
            }

            if (!((ReceiptType10Vo) vo).getDealType().equals("통합결제"))  //통합결제는
            {
                PayData payData = new PayData(BaseActivity.this, OUT_PRINT, IN_PRINT, TICKET_PRINT);

                //if(Constants.isReceiptPrint){
                payData.printDefault(PayData.PAY_DIALOG_TYPE_UNPAY_SUCCESS, vo, mPrintService);
                payData.ImagePrint(empLogo_image);
                try {
                    payData.printText("\n\n");
                } catch (UnsupportedEncodingException e) {
                    System.out.println("UnsupportedEncodingException 예외 발생");
                }
                //}
            }

            request(PayData.PAY_DIALOG_TYPE_UNPAY_CASH, payVo);
            unpayDialog.dismiss();
            unpayDialog = null;
        }

        // 정기권등록에서 현금영수증 처리
        if (ticketDialog != null) {
            PayVo payVo = new PayVo();
            payVo.setCashAmt("" + orgPayment);
            payVo.setTransactionNo("");
            payVo.setApprovalNo("");
            payVo.setApprovalDate("");
            payVo.setCardNo("");
            payVo.setCardCompany("");
            payVo.setCardAmt("0");

            String cashAmt = tvPayTotalAmt.getText().toString().replace("원", "");
            int sumTotal = Util.sToi(cashAmt);

            if (orgPayment != 0 && sumTotal == 0) {
                setToast("입금된 금액이 없습니다. 다시 확인해 주세요.");
                return;
            } else if (sumTotal < orgPayment) {
                setToast("결제하려는 금액이 납부금액보다 작습니다.결제금액을 확인해 주세요.");
                return;
            } else if (sumTotal > orgPayment) {
                setToast("결제하려는 금액이 납부금액보다 큽니다.결제금액을 확인해 주세요.");
                return;
            }

            if (tvCashBill != null && tvCashBill.getText().toString().equals("발행")) {
                if (tvCashBill.getTag() != null) {
                    PayVo billVo = (PayVo) tvCashBill.getTag();
                    if (billVo != null) {
                        payVo.setCashApprovalNo(billVo.getCashApprovalNo());
                        payVo.setCashApprovalDate(billVo.getCashApprovalDate());
                        payVo.setCatNumber(catNumber);
                        ((ReceiptType11Vo) vo).setConfirmCashReceipt(billVo.getCashApprovalNo());
                        cashReceiptDialog.dismiss();
                        cashReceiptDialog = null;
                        g_vo = null;
                        tvCashBill = null;
                    }
                }
            } // end if 발행
            request(PayData.PAY_DIALOG_TYPE_TICKET_CASH, payVo);
            ticketDialog.dismiss();
            ticketDialog = null;
        }
    }

    protected void couponDialog(final int orgPayment) {
        final Dialog couponDialog;
        couponDialog = new Dialog(this,
                android.R.style.Theme_Holo_Light_Dialog);

        couponDialog.setContentView(R.layout.dialog_coupon);
        couponDialog.setTitle("쿠폰");

        Button btn_ok = (Button) couponDialog.findViewById(R.id.btn_ok);
        Button btn_cancel = (Button) couponDialog.findViewById(R.id.btn_cancel);
        final TextView tvTotalAmt = (TextView) couponDialog.findViewById(R.id.tvTotalAmt);
        ListView Lv_dialog = (ListView) couponDialog.findViewById(R.id.Lv_dialog);

        ArrayList<JSONObject> arrayList = new ArrayList<JSONObject>();
        int Len = codeJsonArr.length();
        for (int i = 0; i < Len; i++) {
            try {
                JSONObject res = codeJsonArr.getJSONObject(i);
                if ("T1".equals(res.optString("CD_CLASS"))) {
                    //   Log.d(TAG, " CD_CODE >>>> " + res.optString("CD_CODE") +  " KN_CODE >>>> " + res.optString("KN_CODE"));
                    arrayList.add(res);
                }
            } catch (JSONException e) {
                if (Constants.DEBUG_PRINT_LOG) {
                    e.printStackTrace();
                } else {
                    System.out.println("예외 발생");
                }
            }
        }  // end for

        final CouponAdapter couponAdapter = new CouponAdapter(this, tvTotalAmt, arrayList, "BUTTON");
        Lv_dialog.setAdapter(couponAdapter);

        btn_ok.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                /**XXX 이전버전 2015.10.31 작업
                 if(orgPayment < Util.sToi(tvTotalAmt.getText().toString().replace("원","")))
                 {
                 MsgUtil.ToastMessage(BaseActivity.this, "쿠폰금액이 결제금액보다 큽니다.");
                 return;
                 }
                 **/
                // 총 쿠폰 수량
                btn_coupon_qty.setText(couponAdapter.getCouponQtyTotal() + "개");
                // 총 쿠폰 금액
                tv_coupon_totalamt.setText(tvTotalAmt.getText().toString());

                String couponData = couponAdapter.getCouponQueryString();
                int payTotalAmt = orgPayment - Integer.parseInt(tvTotalAmt.getText().toString().replace("원", ""));
                // 납부금액이 마이너스이면 0원으로 셋팅...
                if (payTotalAmt < 0) {
                    payTotalAmt = 0;
                } else { // 쿠폰으로 할인 받을 시 50원 단위는 올림으로 셋팅...
                    int truncAmt = payTotalAmt % Util.sToi(truncUnit);
                    if (truncAmt > 0) {
                        payTotalAmt = payTotalAmt + (Util.sToi(truncUnit) - truncAmt);
                    }
                }
                // 총 납부 금액
                tvPayTotalAmt.setText("" + payTotalAmt);

                if (Integer.valueOf(payTotalAmt) == 0) {  //쿠폰추가
                    btnPayCard.setEnabled(false);
                    btn_cash.setEnabled(true);
                } else {
                    btnPayCard.setEnabled(true);
                    btn_cash.setEnabled(false);
                }


                request(PayData.PAY_DIALOG_TYPE_COUPON, couponData);
                couponDialog.dismiss();
            }
        });

        btn_cancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                setToast("취소되었습니다.");
                couponDialog.dismiss();
            }
        });
        couponDialog.show();
    }

    private int orgPrePayment = 0;

    public Dialog prepayDialog(final String parkType        // 주차종류
            , final String preAmt            // 선납금액
            , final ReceiptVo vo) {

        if (prepayDialog != null) {
            prepayDialog.dismiss();
            prepayDialog = null;
        }

        prepayDialog = new Dialog(this, android.R.style.Theme_Holo_Light_Dialog);
        prepayDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        prepayDialog.setContentView(R.layout.pay_dialog_prepay);
        TextView tv_pre_title = (TextView) prepayDialog.findViewById(R.id.tv_pre_title);
        TextView tvUnpayAmt = (TextView) prepayDialog.findViewById(R.id.tvUnpayAmt);
        final Button btn_add_dis = (Button) prepayDialog.findViewById(R.id.btn_add_dis);
        tvPayAmt = (TextView) prepayDialog.findViewById(R.id.tvPayAmt);
        tvPayTotalAmt = (TextView) prepayDialog.findViewById(R.id.tvPayTotalAmt);
        btn_coupon_qty = (Button) prepayDialog.findViewById(R.id.btn_coupon_qty);
        tv_coupon_totalamt = (TextView) prepayDialog.findViewById(R.id.tv_coupon_totalamt);

        final String unPayAmt = ((ReceiptType13Vo) vo).getTotalUnpay();
        tvUnpayAmt.setText(Util.addComma(unPayAmt) + "원");

        orgPrePayment = Integer.parseInt(preAmt.replace("원", "").trim());

        Preferences.putValue(mContext, Constants.PREFERENCE_UNPAY_AMT, "0");
        btn_unpayAver = (Button) prepayDialog.findViewById(R.id.btn_unpayAver);
        btn_unpayAver.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                btn_unpayAver.setSelected(!btn_unpayAver.isSelected());


                btn_add_dis.setText("제휴 X");
                btn_add_dis.setBackgroundResource(R.drawable.btn_basic_press);
                isAddDis = false;
                ADJ_AMT = 0;


                if (btn_unpayAver.isSelected()) {
                    Toast.makeText(mContext, "미수금이 합산 되었습니다.", Toast.LENGTH_LONG).show();
                    orgPrePayment += Integer.parseInt(unPayAmt);
                    tvPayTotalAmt.setTextColor(Color.RED);
                    Preferences.putValue(mContext, Constants.PREFERENCE_UNPAY_AMT, unPayAmt);

                } else {
                    Toast.makeText(mContext, "미수금이 합산이 취소 되었습니다.", Toast.LENGTH_LONG).show();
                    orgPrePayment -= Integer.parseInt(unPayAmt);
                    tvPayTotalAmt.setTextColor(Color.BLACK);
                    Preferences.putValue(mContext, Constants.PREFERENCE_UNPAY_AMT, "0");
                }


                Preferences.putValue(mContext, Constants.PREFERENCE_ADJUST_AMT, String.valueOf(ADJ_AMT));
                tvPayTotalAmt.setText(String.valueOf(orgPrePayment));
            }
        });

        if ("".equals(unPayAmt)||"0".equals(unPayAmt)) {
            btn_unpayAver.setVisibility(View.GONE);
        } else {
            btn_unpayAver.setVisibility(View.VISIBLE);
        }


        if (("쿠폰판매").equals(parkType)) {
            tv_pre_title.setText("쿠폰판매");
        } else {
            tv_pre_title.setText("주차선납금계산");
        }


        btn_add_dis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int couponAmt = Util.sToi(tv_coupon_totalamt.getText().toString().replace("원", ""));
                int disAmt = orgPrePayment - couponAmt;
                if (disAmt <= 0) {
                    return;
                }


                if (!isAddDis) {
                    btn_add_dis.setText("제휴 O");
                    btn_add_dis.setBackgroundResource(R.color.table_row_red_text);
                    isAddDis = true;
                    int disAdjustAmt = discountChargeRate(String.valueOf(disAmt), "5");
                    double diff = Math.floor((disAmt - disAdjustAmt) / disAdj) * disAdj;
                    ADJ_AMT = (int) (disAmt - diff);
                    disAmt = (int) diff;
                } else {
                    btn_add_dis.setText("제휴 X");
                    btn_add_dis.setBackgroundResource(R.drawable.btn_basic_press);
                    isAddDis = false;
                    ADJ_AMT = 0;
                }

                Preferences.putValue(mContext, Constants.PREFERENCE_ADJUST_AMT, String.valueOf(ADJ_AMT));
                tvPayTotalAmt.setText(String.valueOf(disAmt));
            }
        });


        tvPayAmt.setText(preAmt + "원");
        tvPayTotalAmt.setText("" + preAmt);

        Button btn_cash = (Button) prepayDialog.findViewById(R.id.btn_cash);
        //Button btn_card 	    = (Button) 	 prepayDialog.findViewById(R.id.btn_card);
        btnPayCard = (Button) prepayDialog.findViewById(R.id.btn_card);
        Button btn_cancel = (Button) prepayDialog.findViewById(R.id.btn_cancel);

        if (Constants.isCardAuth) {
            btnPayCard.setEnabled(true);
        } else {
            btnPayCard.setEnabled(false);
        }

        final int orgPayment = Integer.parseInt(preAmt.replace("원", "").trim());

        if (orgPayment <= 0) {
            btn_cash.setText("확인");
            btnPayCard.setVisibility(View.GONE);
        }

        btn_coupon_qty.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                btn_add_dis.setText("제휴 X");
                btn_add_dis.setBackgroundResource(R.drawable.btn_basic_press);
                isAddDis = false;
                ADJ_AMT = 0;
                Preferences.putValue(mContext, Constants.PREFERENCE_ADJUST_AMT, String.valueOf(ADJ_AMT));
                tvPayTotalAmt.setText(String.valueOf(preAmt));

                // 쿠폰 다이어로그
                couponDialog(orgPayment);
            }
        });

        btn_cash.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((Button) v).getText().toString().equals("현금")) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(BaseActivity.this);
                    builder.setTitle("결제방식").setMessage("결제금액 : " + tvPayTotalAmt.getText() + "\n현금으로 결제하시겠습니까?");
                    builder.setPositiveButton(R.string.btn_ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // 선납금등록에서 현금영수증 처리
                            cashReceiptDialog(Util.sToi(preAmt), vo, parkType);
                            dialog.dismiss();
                        }
                    }).setNegativeButton(R.string.btn_cancel, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    AlertDialog alert = builder.create();
                    alert.show();

                } else {
                    // 선납금등록에서 현금영수증 처리
                    cashReceiptDialog(Util.sToi(preAmt), vo, parkType);
                }
            }
        });

        btnPayCard.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                //((ReceiptType13Vo)vo).setPrintBill(true);
                Log.d("111", "tv_coupon_totalamt : " + tv_coupon_totalamt.getText().toString() + "     tvPayTotalAmt : " + tvPayTotalAmt.getText().toString() + "     vo  :  " + ((ReceiptType13Vo) vo).getPrepayAmt());
                ((ReceiptType13Vo) vo).setCouponAmt(tv_coupon_totalamt.getText().toString());

                goToPayActivity(PayData.PAY_DIALOG_TYPE_PREPAY_CARD, tvPayTotalAmt.getText().toString().replace("원", "").replace(",", ""), 0, parkType, vo);
            }
        });

        btn_cancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                MsgUtil.ToastMessage(BaseActivity.this, "취소되었습니다.");
                prepayDialog.dismiss();
                prepayDialog = null;
            }
        });

        prepayDialog.show();
        return prepayDialog;
    }


    public Dialog unpayDialog(final String parkType, final String unpayAmt, final ReceiptType10Vo vo) {

        unpayDialog = new Dialog(this, android.R.style.Theme_Holo_Light_Dialog);

        unpayDialog.setContentView(R.layout.pay_dialog_unpay);

        btn_cash = (Button) unpayDialog.findViewById(R.id.btn_cash);
        btnPayCard = (Button) unpayDialog.findViewById(R.id.btn_card);
        Button btn_cancel = (Button) unpayDialog.findViewById(R.id.btn_cancel);
        btn_coupon_qty = (Button) unpayDialog.findViewById(R.id.btn_coupon_qty);
        tv_coupon_totalamt = (TextView) unpayDialog.findViewById(R.id.tv_coupon_totalamt);
        LinearLayout layout_coupon = (LinearLayout) unpayDialog.findViewById(R.id.layout_coupon);
        if (parkType.equals("통합결제")) {
            unpayDialog.setTitle("통합결제");
            layout_coupon.setVisibility(View.GONE);
        } else {
            unpayDialog.setTitle("미수금입금-결제");
            layout_coupon.setVisibility(View.VISIBLE);

        }

        if (Constants.isCardAuth) {
            btnPayCard.setEnabled(true);
        } else {
            btnPayCard.setEnabled(false);
        }

        TextView tvUnpayAmt = (TextView) unpayDialog.findViewById(R.id.tvUnpayAmt);
        tvPayAmt = (TextView) unpayDialog.findViewById(R.id.tvPayAmt);
        tvPayTotalAmt = (TextView) unpayDialog.findViewById(R.id.tvPayTotalAmt);

        tvUnpayAmt.setText(Util.addComma(unpayAmt) + "원");
        tvPayAmt.setText(Util.addComma(unpayAmt) + "원");
        tvPayTotalAmt.setText("" + unpayAmt);

        if (unpayAmt.equals("0")) {
            btnPayCard.setEnabled(false);
            btn_cash.setEnabled(true);
        } else {
            btnPayCard.setEnabled(true);
            btn_cash.setEnabled(false);
        }

        final int orgPayment = Integer.parseInt(unpayAmt.replace("원", "").trim());

        btn_coupon_qty.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // 쿠폰 다이어로그
                couponDialog(orgPayment);
            }
        });

        btn_cash.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // 미수환수에서 현금영수증 처리
                //cashReceiptDialog(Util.sToi(unpayAmt), vo, parkType);

                if (((Button) v).getText().toString().equals("현금")) {
                    cashReceiptDialog(Util.sToi(unpayAmt), vo, parkType);
                } else {
                    // 선납금등록에서 현금영수증 처리
                    cashReceiptDialog(Util.sToi(unpayAmt), vo, parkType);
                }
            }
        });

        btnPayCard.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                ((ReceiptType10Vo) vo).setCouponAmt(tv_coupon_totalamt.getText().toString());
                ((ReceiptType10Vo) vo).setTotalInputAmt(String.valueOf(unpayAmt));
                goToPayActivity(PayData.PAY_DIALOG_TYPE_UNPAY_CARD, tvPayTotalAmt.getText().toString().replace("원", "").replace(",", ""), 0, parkType, vo);
            }
        });

        btn_cancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                setToast("취소되었습니다.");
                unpayDialog.dismiss();
                unpayDialog = null;
            }
        });

        unpayDialog.show();
        return unpayDialog;
    }

    public Dialog ticketDialog(final String parkType
            , final String ticketAmt
            , final String unPayAmt
            , final int payAmt
            , final ReceiptVo vo) {

        ticketDialog = new Dialog(this, android.R.style.Theme_Holo_Light_Dialog);

        ticketDialog.setContentView(R.layout.pay_dialog_ticket);
        ticketDialog.setTitle("정기권결제");


        final Spinner mSpinner = (Spinner) ticketDialog.findViewById(R.id.sp_month_pay);

        ArrayAdapter<String> mSpinnerAdapter = new ArrayAdapter<String>(this,
                R.layout.spinner_txt, (String[]) getResources().getStringArray(R.array.monthly_pay));
        // Spinner 클릭시 DropDown 모양을 설정 할 수 있다.
        mSpinnerAdapter.setDropDownViewResource
                (android.R.layout.simple_spinner_dropdown_item);
        // 스피너에 어답터를 연결 시켜 준다.
        mSpinner.setAdapter(mSpinnerAdapter);


        btn_cash = (Button) ticketDialog.findViewById(R.id.btn_cash);
        final Button btn_card = (Button) ticketDialog.findViewById(R.id.btn_card);
        Button btn_cancel = (Button) ticketDialog.findViewById(R.id.btn_cancel);

        if (Constants.isCardAuth) {
            btn_card.setEnabled(true);
        } else {
            btn_card.setEnabled(false);
        }

        final int orgPayment = payAmt;

        if (orgPayment <= 0) {
            btn_cash.setText("확인");
            btn_card.setVisibility(View.GONE);
        } else {
            btn_cash.setEnabled(false);
        }

        TextView tvTicketAmt = (TextView) ticketDialog.findViewById(R.id.tvTicketAmt);
        TextView tvUnpayAmt = (TextView) ticketDialog.findViewById(R.id.tvUnpayAmt);

        tvPayAmt = (TextView) ticketDialog.findViewById(R.id.tvPayAmt);
        tvPayTotalAmt = (TextView) ticketDialog.findViewById(R.id.tvPayTotalAmt);

        tvTicketAmt.setText(Util.addComma(ticketAmt) + "원");
        tvUnpayAmt.setText(Util.addComma(unPayAmt) + "원");

        tvPayAmt.setText(Util.addComma(payAmt) + "원");
        tvPayTotalAmt.setText("" + payAmt);

        btn_cash.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((Button) v).getText().toString().equals("현금")) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(BaseActivity.this);
                    builder.setTitle("결제방식").setMessage("결제금액 : " + tvPayTotalAmt.getText() + "\n현금으로 결제하시겠습니까?");
                    builder.setPositiveButton(R.string.btn_ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // 정기권등록에서 현금영수증 처리
                            cashReceiptDialog(payAmt, vo, parkType);
                            dialog.dismiss();
                        }
                    }).setNegativeButton(R.string.btn_cancel, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    AlertDialog alert = builder.create();
                    alert.setCancelable(false);
                    alert.show();
                } else {
                    // 정기권등록에서 현금영수증 처리
                    cashReceiptDialog(payAmt, vo, parkType);
                }
            }
        });

        btn_card.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                if (payAmt >= 50000 && mSpinner.getVisibility() == View.GONE) {
                    btn_card.setText("확인");
                    btn_cash.setVisibility(View.GONE);
                    mSpinner.setVisibility(View.VISIBLE);
                    setToast("할부기간 설정이 가능합니다.");
                } else {

                    int monthly = 0;
                    if (mSpinner != null && mSpinner.getVisibility() == View.VISIBLE) {
                        monthly = mSpinner.getSelectedItemPosition();
                        if (monthly > 0) monthly += 1;
                    }
                    goToPayActivity(PayData.PAY_DIALOG_TYPE_TICKET_CARD, "" + payAmt, monthly, parkType, vo);
                }
            }
        });

        btn_cancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                setToast("취소되었습니다.");
                if (mSpinner.getVisibility() == View.VISIBLE) {
                    btn_cash.setVisibility(View.VISIBLE);
                    mSpinner.setSelection(0);
                    mSpinner.setVisibility(View.GONE);
                    btn_card.setText("카드");
                } else {
                    ticketDialog.dismiss();
                    ticketDialog = null;
                }

            }
        });
        ticketDialog.show();
        return ticketDialog;
    }

    public void minusTimeCheck(String sTime, String eTime, TextView v) {
        String getDay = Util.getYmdhms("yyyyMMdd");

        String date1 = getDay + sTime + "00";
        String date2 = getDay + eTime + "00";

        Log.i("test", "date1 : " + date1);
        Log.i("test", "date2 : " + date2);
        String parkMin = minuteCalc(date1, date2);
        v.setTag(parkMin);
        v.setText(minuteView(parkMin));
    }

    /**
     * 주차시간 계산
     *
     * @param //startTime 20140401123200 (yyyyMMddHHmmss)
     * @param //endTime   20140401123200 (yyyyMMddHHmmss)
     * @return
     */
    public String minuteCalc(String startDate, String endDate) {

        startDate = startDate.replaceAll("-", "").replaceAll("/", "").replaceAll(":", "").replaceAll(" ", "");
        endDate = endDate.replaceAll("-", "").replaceAll("/", "").replaceAll(":", "").replaceAll(" ", "");

        String nowDate = Util.getYmdhms("yyyyMMdd");

        long wStartTime = Long.parseLong(nowDate + workStartTime.replace(":", ""));
        long wEndTime = Long.parseLong(nowDate + workEndTime.replace(":", ""));

        String sTimeText = startDate.substring(8, 12);
        String eTimeText = endDate.substring(8, 12);

        long sTime = Long.parseLong(startDate.substring(0, 12));
        long eTime = Long.parseLong(endDate.substring(0, 12));

        Log.i(TAG, "minuteCalc startDate : " + startDate + ", endTime : " + endDate);
        Log.i(TAG, "minuteCalc wStartTime : " + wStartTime + ", wEndTime : " + wEndTime);
        Log.i(TAG, "minuteCalc sTimeText : " + sTimeText + ", eTimeText : " + eTimeText);
        Log.i(TAG, "minuteCalc sTime : " + sTime + ", eTime : " + eTime);

        if (Long.parseLong(startDate) >= Long.parseLong(endDate)) {
            Log.i(TAG, "return 1");
            return "0";    // 시작시간이 종료시간보다 크면
        }

        if (sTime < wStartTime && eTime < wStartTime) {
            Log.i(TAG, "return 2");
            return "0";    // 시작 시간이 근무 시작시간보다 작고, 종료 시간이 근무 시작시간보다 작은 경우
        } else if (sTime > wEndTime) {
            Log.i(TAG, "return 3");
            return "0";    // 시작시간이 근무 종료 시간보다 큰 경우
        }

        // 주차시작시간이 현재일이고 근무시작시간보다 작은 경우 근무시작시간으로 셋팅
        if (nowDate.equals(startDate.substring(0, 8)) && sTime < wStartTime) {
            sTimeText = workStartTime.replace(":", "");
            Log.i(TAG, "minuteCalc sTimeText >>> " + sTimeText);
        }

        // 주차종료시간이 현재일이고 근무종료시간보다 큰 경우 근무종료시간으로 셋팅
        if (nowDate.equals(endDate.substring(0, 8)) && eTime > wEndTime) {
            eTimeText = workEndTime.replace(":", "");
            Log.i(TAG, "minuteCalc eTimeText >>> " + eTimeText);
        }

        Log.i(TAG, "minuteCalc after sTimeText : " + sTimeText + ", eTimeText : " + eTimeText);

        String start_date = startDate.substring(0, 8) + sTimeText + "00";
        String end_date = endDate.substring(0, 8) + eTimeText + "00";

        Log.i(TAG, "minuteCalc start_date : " + start_date);
        Log.i(TAG, "minuteCalc end_date   : " + end_date);

        /***
         * 무료 주차 시간 계산
         */
        String parkTime = "";
        String serviceTime = "";
        int iSvcStartTime = Util.sToi(Util.isNVL(serviceStartTime, "0"));
        int iSvcEndTime = Util.sToi(Util.isNVL(serviceEndTime, "0"));

        if (iSvcStartTime != 0 && iSvcEndTime != 0) {
            Log.d(TAG, "minuteCalc service org time : " + Util.getMinute(today + serviceStartTime + "00", today + serviceEndTime + "00"));
        }

        sTime = Long.parseLong(startDate.substring(8, startDate.length() - 2));
        eTime = Long.parseLong(endDate.substring(8, startDate.length() - 2));

        if (iSvcStartTime == 0 || iSvcEndTime == 0) { // 무료주차시작시간 또는 무료주차종료시간이 없다면 무료주차시간 적용 안 함
            Log.d(TAG, " service time case 1 !!! ");
            parkTime = Util.getMinute(start_date, end_date);
            Log.d(TAG, " parkTime ::: " + parkTime);
        } else if (sTime >= iSvcStartTime && eTime <= iSvcEndTime) { // 입차시간이 무료주차시작시간보다 같거나크고 출차시간이 무료주차종료시간보다 같거나 작다면 무료주차구간(0분)
            Log.d(TAG, " service time case 2 !!! ");
            parkTime = "0";
            Log.d(TAG, " parkTime ::: " + parkTime);
        } else if ((sTime <= iSvcStartTime && eTime <= iSvcStartTime)
                || (sTime >= iSvcEndTime && eTime >= iSvcEndTime)) { // 입차시간과 출차시간이 무료주차시작시간보다 같거나 작고 무료주차종료시간보다 같거나크다면 무료주차시간 적용 안 함
            Log.d(TAG, " service time case 3 !!! " + sTime + "~" + iSvcStartTime + "~" + eTime + "~" + iSvcEndTime);
            parkTime = Util.getMinute(start_date, end_date);
            Log.d(TAG, " parkTime ::: " + parkTime);
        } else if (sTime <= iSvcStartTime && eTime <= iSvcEndTime) { // 입차시간이 무료주차시작시간보다 같거나작고  출차시간이 무료주차종료시간보다 같거나작다면 무료주차시간적용
            Log.d(TAG, " service time case 4 !!! ");
            parkTime = Util.getMinute(start_date, end_date);
            serviceTime = Util.getMinute(today + serviceStartTime + "00", end_date);
            Log.d(TAG, " parkTime ::: " + parkTime + " serviceTime ::: " + serviceTime);
            parkTime = "" + (Util.sToi(parkTime) - Util.sToi(serviceTime));
            Log.d(TAG, " parkTime  >>> " + parkTime);
        } else if (sTime <= iSvcStartTime && eTime > iSvcEndTime) { // 입차시간이 무료주차시작시간보다 같거나작고  출차시간이 무료주차종료시간보다 크다면 무료주차시간적용
            Log.d(TAG, " service time case 5 !!! ");
            parkTime = Util.getMinute(start_date, end_date);
            serviceTime = Util.getMinute(today + serviceStartTime + "00", today + serviceEndTime + "00");
            Log.d(TAG, " parkTime ::: " + parkTime + " serviceTime ::: " + serviceTime);
            parkTime = "" + (Util.sToi(parkTime) - Util.sToi(serviceTime));
            Log.d(TAG, " parkTime >>> " + parkTime);
        } else if (sTime >= iSvcStartTime && eTime > iSvcEndTime) { // 입차시간이 무료주차시작시간보다 같거나크고  출차시간이 무료주차종료시간보다 크다면 무료주차시간적용
            Log.d(TAG, " service time case 6 !!! ");
            parkTime = Util.getMinute(start_date, end_date);
            serviceTime = Util.getMinute(start_date, today + serviceEndTime + "00");
            Log.d(TAG, " parkTime ::: " + parkTime + " serviceTime ::: " + serviceTime);
            parkTime = "" + (Util.sToi(parkTime) - Util.sToi(serviceTime));
            Log.d(TAG, " parkTime >>> " + parkTime);
        }

        return parkTime;

    }

    /**
     * 주차시간 계산
     *
     * @param //startTime 20140401123200 (yyyyMMddHHmmss)
     * @param //endTime   20140401123200 (yyyyMMddHHmmss)
     * @return
     */
    public String minuteCalc_Return(String startDate, String endDate,
                                    String m_workStartTime, String m_workEndTime
            , String m_serviceStartTime, String m_serviceEndTime, String m_today
    ) {

        startDate = startDate.replaceAll("-", "").replaceAll("/", "").replaceAll(":", "").replaceAll(" ", "");
        endDate = endDate.replaceAll("-", "").replaceAll("/", "").replaceAll(":", "").replaceAll(" ", "");

        String nowDate = Util.getYmdhms("yyyyMMdd");


        long wStartTime = Long.parseLong(m_today);
        if (m_workStartTime != null) {
            wStartTime = Long.parseLong(m_today + m_workStartTime.replace(":", ""));
        }


        long wEndTime = Long.parseLong(m_today);
        if (m_workEndTime != null) {
            wEndTime = Long.parseLong(m_today + m_workEndTime.replace(":", ""));
        }


        String sTimeText = startDate.substring(8, 12);
        String eTimeText = endDate.substring(8, 12);

        long sTime = Long.parseLong(startDate.substring(0, 12));
        long eTime = Long.parseLong(endDate.substring(0, 12));

        // 주차시작시간이 현재일이고 근무시작시간보다 작은 경우 근무시작시간으로 셋팅
        if (m_workStartTime != null && nowDate.equals(startDate.substring(0, 8)) && sTime < wStartTime) {
            sTimeText = m_workStartTime.replace(":", "");
            Log.i(TAG, "minuteCalc sTimeText >>> " + sTimeText);
        }

        // 주차종료시간이 현재일이고 근무종료시간보다 큰 경우 근무종료시간으로 셋팅
        if (m_workEndTime != null && nowDate.equals(endDate.substring(0, 8)) && eTime > wEndTime) {
            eTimeText = m_workEndTime.replace(":", "");
            Log.i(TAG, "minuteCalc eTimeText >>> " + eTimeText);
        }

        String start_date = startDate.substring(0, 8) + sTimeText + "00";
        String end_date = endDate.substring(0, 8) + eTimeText + "00";


        /***
         * 무료 주차 시간 계산
         */
        String parkTime = "";
        String serviceTime = "";
        int iSvcStartTime = Util.sToi(Util.isNVL(m_serviceStartTime, "0"));
        int iSvcEndTime = Util.sToi(Util.isNVL(m_serviceEndTime, "0"));


        //sTime = Long.parseLong(startDate.substring(8, startDate.length()-2));
        //eTime = Long.parseLong(endDate.substring(8, startDate.length()-2));

        if (iSvcStartTime == 0 || iSvcEndTime == 0) { // 무료주차시작시간 또는 무료주차종료시간이 없다면 무료주차시간 적용 안 함
            parkTime = Util.getMinute(start_date, end_date);
        } else if (sTime >= iSvcStartTime && eTime <= iSvcEndTime) { // 입차시간이 무료주차시작시간보다 같거나크고 출차시간이 무료주차종료시간보다 같거나 작다면 무료주차구간(0분)
            parkTime = "0";
        } else if ((sTime <= iSvcStartTime && eTime <= iSvcStartTime)
                || (sTime >= iSvcEndTime && eTime >= iSvcEndTime)) { // 입차시간과 출차시간이 무료주차시작시간보다 같거나 작고 무료주차종료시간보다 같거나크다면 무료주차시간 적용 안 함
            parkTime = Util.getMinute(start_date, end_date);
        } else if (sTime <= iSvcStartTime && eTime <= iSvcEndTime) { // 입차시간이 무료주차시작시간보다 같거나작고  출차시간이 무료주차종료시간보다 같거나작다면 무료주차시간적용
            parkTime = Util.getMinute(start_date, end_date);
            serviceTime = Util.getMinute(m_today + m_serviceStartTime + "00", end_date);
            parkTime = "" + (Util.sToi(parkTime) - Util.sToi(serviceTime));
        } else if (sTime <= iSvcStartTime && eTime > iSvcEndTime) { // 입차시간이 무료주차시작시간보다 같거나작고  출차시간이 무료주차종료시간보다 크다면 무료주차시간적용
            parkTime = Util.getMinute(start_date, end_date);
            serviceTime = Util.getMinute(m_today + m_serviceStartTime + "00", m_today + m_serviceEndTime + "00");
            parkTime = "" + (Util.sToi(parkTime) - Util.sToi(serviceTime));
        } else if (sTime >= iSvcStartTime && eTime > iSvcEndTime) { // 입차시간이 무료주차시작시간보다 같거나크고  출차시간이 무료주차종료시간보다 크다면 무료주차시간적용
            parkTime = Util.getMinute(start_date, end_date);
            serviceTime = Util.getMinute(start_date, m_today + m_serviceEndTime + "00");
            parkTime = "" + (Util.sToi(parkTime) - Util.sToi(serviceTime));
        }

        return parkTime;

    }


    /**
     * 주차시간 표시
     *
     * @param //startTime 20140401123200 (yyyyMMddHHmmss)
     * @param //endTime   20140401123200 (yyyyMMddHHmmss)
     * @return
     */
    public String minuteView(String parkMin) {
        int hh = Util.sToi(parkMin) / 60;
        int mm = Util.sToi(parkMin) % 60;
        return Util.addZero(hh) + ":" + Util.addZero(mm);
    }

    /**
     * 숫자만 넘겨줘야함
     *
     * @param total    전체 금액
     * @param discount 할인율 예) 장애인할인 50% > 50
     * @return dis가 0이면 할인없음 > dis = 0
     */
    static int discountChargeRate(String total, String discount) {
        double dis = Util.getDouble(discount);
        double charge = Util.getDouble(total);

        Log.d(TAG, "setParkAmt discountChargeRate 11 >> " + total);
        Log.d(TAG, "setParkAmt discountChargeRate 22 >> " + dis);
        Log.d(TAG, "setParkAmt discountChargeRate 33 >> " + charge);

        if ("0".equals(total)) {
            return 0;
        }

        if (dis > 0) {
            int result = 0;
            result = (int) (charge * (dis / 100.0));
            Log.d(TAG, "charge : " + charge);
            Log.d(TAG, "case dis 1 : " + result);
            return result;
        } else if (dis < 0) {
            /***
             Log.d(TAG, " dis : " + dis);
             Log.d(TAG, " timeCharge : " + timeCharge);
             Log.d(TAG, " charge : " + charge);
             Log.d(TAG, " (int)(charge - timeCharge): " +  ((int)(charge - timeCharge)) );
             Log.d(TAG, " Math.abs(dis) : " +  Math.abs(dis) );
             Log.d(TAG, " (Math.abs(dis) / 100.0) : " +  (Math.abs(dis) / 100.0) );
             Log.d(TAG, " (int)(charge - timeCharge) * (Math.abs(dis) / 100.0)): " +  ((int)(charge - timeCharge) * (Math.abs(dis) / 100.0)) );
             Log.d(TAG, " (int)(charge - timeCharge) * (Math.abs(dis) / 100.0)) * -1 : " +  (((int)(charge - timeCharge) * (Math.abs(dis) / 100.0)) * -1) );
             Log.d(TAG, " (int)(timeCharge + ((charge - timeCharge) * (Math.abs(dis) / 100.0)) * -1) : " +  ((int)(timeCharge + ((charge - timeCharge) * (Math.abs(dis) / 100.0)) * -1)) );
             ***/
            int result = (int) ((charge) * (Math.abs(dis) / 100.0) * -1);
            Log.d(TAG, "case dis 2 : " + result);
            return result;
        } else {
            Log.d(TAG, "case dis 3 : ");
            return 0;
        }
    }

    /**
     * 주차요금 최대금액 셋팅
     *
     * @param amt - 주차금액
     * @return
     */
    static String setParkMaxAmt(String amt) {
        return "" + setParkMaxAmt(Util.sToi(amt));
    }

    static int setParkMaxAmt(int amt) {
        int tempAmt = amt;
        // 주차요금 최대금액이 넘어가면 최대금액으로 설정
        if (parkMaxAmt > 0) {
            if (parkMaxAmt < tempAmt) {
                tempAmt = parkMaxAmt;
            }
        }
        return tempAmt;
    }

    static int setParkMaxAmt(int amt, int m_parkMaxAmt) {
        int tempAmt = amt;
        // 주차요금 최대금액이 넘어가면 최대금액으로 설정
        if (m_parkMaxAmt > 0) {
            if (m_parkMaxAmt < tempAmt) {
                tempAmt = m_parkMaxAmt;
            }
        }
        return tempAmt;
    }

    /**
     * 요금종류 셋팅
     *
     * @param chargeTypeCode
     */
    static void setParkChargeType(String chargeTypeCode) {
        //코드정보 CD_CLASS==AM 일때 DIS_AMT, CD_VALUE 추출 및 세팅
        int Len = codeJsonArr.length();
        for (int i = 0; i < Len; i++) {
            try {
                JSONObject res = codeJsonArr.getJSONObject(i);
                if (res.optString("CD_CLASS").startsWith("AM")) {


                    //if(("일일권").equals(res.optString("KN_CODE"))) {
                    //	dayAmt    	  = res.optInt("CD_VALUE2",0);
                    //}

                    if (chargeTypeCode.equals(res.optString("CD_CODE"))) {
//						Log.d(TAG, " DIS_AMT >>>> " + res.optString("DIS_AMT") +  " CD_VALUE >>>> " + res.optString("CD_VALUE"));
                        // 기본구간
                        baseSection = res.optString("BASE_TIME", "0");
                        // 기본금액
                        baseAmt = res.optString("BASE_AMT", "0");
                        // 반복구간
                        repeatSection = res.optString("DIS_TIME", "0");
                        // 반복금액
                        repeatAmt = res.optString("DIS_AMT", "0");
                        // 주차최대금액
                        parkMaxAmt = res.optInt("CD_VALUE", 0);
                        // 일일권 금액
                        dayAmt = res.optInt("CD_VALUE2", 0);
                        // 정기권 금액
                        ticketAmt = res.optString("CD_VALUE3", "0");
                        break;
                    }


                }
            } catch (JSONException e) {
                System.out.println("JSONException 예외 발생");
            }
        } // end for
    }

    /**
     * 총 주차요금. (할인 또는 절삭되지 않은 요금)
     *
     * @param min            총 시간 예) 240분 >240
     * @param chargeTypeCode 요금종류 예) 소형 - 5분 150원
     * @param parkTypeCode   주차종류 예) 시간권(후불), 일일권, 정기권, 시간권(선불)
     * @return
     */
    static String parkCharge(String min, String chargeTypeCode, String parkTypeCode) {
        int chargeAmt = 0;
        try {

            // 요금종류 셋팅
            setParkChargeType(chargeTypeCode);

            int minute = (int) Util.getDouble(min);
            // 입차서비스
            minute -= Integer.parseInt(inService);
            if (minute <= 0) return "0";

            // 출차서비스
            minute -= Integer.parseInt(outService);
            if (minute <= 0) return "0";
            Log.d(TAG, " step0 >>> " + minute);
            // 기본구간
            minute -= Integer.parseInt(baseSection);
            if (minute <= 0) return "" + Integer.parseInt(baseAmt);

            minute -= 30;
            if (minute <= 0) return "" + 1000;
            //int totalService = Integer.parseInt(inService) + Integer.parseInt(outService) + Integer.parseInt(baseSection) + 30;
            //int checkTime = (120 - totalService) / 10;
            //Log.d(TAG, " checkTime >>> " + checkTime );


            // 반복구간
            int parkTime = (int) ((minute - 1) / Integer.parseInt(repeatSection)) + 1;


            // 2급지인 주차장인 경우
			/*
			if("2".equals(parkClass)){
				if(parkTime <= checkTime){  // 과천시 2시간 이하 주차는 기본금액으로 과금
					chargeAmt = Integer.parseInt(baseAmt) + parkTime * Integer.parseInt(repeatAmt);
					Log.d(TAG, " chargeAmt - case 1 ::: " + chargeAmt);
				} else { // 과천시 2시간 초과면 추가금액으로 과금
					int parkTimeAfter  = parkTime - checkTime;
					Log.d(TAG, " parkTimeAfter ::: " + parkTimeAfter);
					chargeAmt = Integer.parseInt(baseAmt) + checkTime * Integer.parseInt(repeatAmt);
					Log.d(TAG, " chargeAmt - case 2_1 :::" + chargeAmt);
					chargeAmt += parkTimeAfter * 400;
					Log.d(TAG, " chargeAmt - case 2_2 :::" + chargeAmt);
				}
			} else {  // 다른 급지인 경우 기본 금액으로 과금
				chargeAmt = Integer.parseInt(baseAmt) + parkTime * Integer.parseInt(repeatAmt);
				Log.d(TAG, " chargeAmt - case 3 ::: " + chargeAmt);
			}
			*/

            //chargeAmt = Integer.parseInt(baseAmt) + parkTime * Integer.parseInt(repeatAmt);
            chargeAmt = 1000 + parkTime * Integer.parseInt(repeatAmt);
            Log.d(TAG, " chargeAmt - case 3 ::: " + chargeAmt);


            if ("01".equals(parkTypeCode)) {
                // 2015.11.12 - 시간권 후불은 3급지만 주차 최대 금액 적용...
                if ("3".equals(parkClass)) {
                    Log.d(TAG, " chargeAmt as is === " + chargeAmt);
                    // 주차요금 최대금액이 넘어가면 최대금액으로 설정
                    chargeAmt = setParkMaxAmt(chargeAmt);
                    Log.d(TAG, " chargeAmt to be === " + chargeAmt);
                } else {
                    // 2015.11.08 - 시간권 후불은 주차 최대 금액 적용 안 함...
                    Log.d(TAG, " chargeAmt  === " + chargeAmt);
                    chargeAmt = setParkMaxAmt(chargeAmt);
                }

            } else {
                Log.d(TAG, " chargeAmt as is ::: " + chargeAmt);
                // 주차요금 최대금액이 넘어가면 최대금액으로 설정
                chargeAmt = setParkMaxAmt(chargeAmt);
                Log.d(TAG, " chargeAmt to be ::: " + chargeAmt);
            }

        } catch (NullPointerException e) {
            if (Constants.DEBUG_PRINT_LOG) {
                e.printStackTrace();
            } else {
                System.out.println("예외 발생");
            }
        }

        Log.d(TAG, "chargeAmt : " + chargeAmt);
        return String.valueOf(chargeAmt);
    }


    static String parkCharge_distime(String min, String chargeTypeCode, String parkTypeCode, int disTime) {
        int chargeAmt = 0;
        try {
            TAG = "111";
            Log.d(TAG, " 111 >>> " + min);
            Log.d(TAG, " 222 >>> " + chargeTypeCode);
            Log.d(TAG, " 333 >>> " + parkTypeCode);
            Log.d(TAG, " inService >>> " + inService);
            Log.d(TAG, " outService >>> " + outService);

            // 요금종류 셋팅
            setParkChargeType(chargeTypeCode);

            int minute = (int) Util.getDouble(min);

            minute -= Integer.parseInt(inService);
            if (minute <= 0) return "0";

            // 출차서비스
            minute -= Integer.parseInt(outService);
            if (minute <= 0) return "0";

            Log.d(TAG, " minute >>> " + (minute - 1));
            Log.d(TAG, " repeatSection >>> " + Integer.parseInt(repeatSection));
            Log.d(TAG, " step3 >>> " + ((minute - 1) / Integer.parseInt(repeatSection)));

            // 반복구간
            int parkTime = (int) ((minute - 1) / Integer.parseInt(repeatSection)) + 1;


            Log.d(TAG, " parkTime >>> " + parkTime);
            Log.d(TAG, " baseAmt >>> " + baseAmt);
            Log.d(TAG, " repeatAmt >>> " + repeatAmt);

            chargeAmt = parkTime * Integer.parseInt(repeatAmt);

            chargeAmt = setParkMaxAmt(chargeAmt);

        } catch (NullPointerException e) {
            if (Constants.DEBUG_PRINT_LOG) {
                e.printStackTrace();
            } else {
                System.out.println("예외 발생");
            }
        }

        Log.d(TAG, "chargeAmt========== : " + chargeAmt);
        return String.valueOf(chargeAmt);
    }


    /**
     * 총 주차요금. (할인 또는 절삭되지 않은 요금)
     *
     * @param min            총 시간 예) 240분 >240
     * @param chargeTypeCode 요금종류 예) 소형 - 5분 150원
     * @param parkTypeCode   주차종류 예) 시간권(후불), 일일권, 정기권, 시간권(선불)
     * @return
     */
    static String parkCharge(String min, String chargeTypeCode, String parkTypeCode,
                             String m_inService, String m_outService, String m_baseSection, String m_baseAmt,
                             String m_repeatSection, String m_repeatAmt, int m_setParkMax
    ) {
        int chargeAmt = 0;
        try {

            // 요금종류 셋팅
            //setParkChargeType(chargeTypeCode);
            int minute = (int) Util.getDouble(min);
            // 입차서비스
            minute -= Integer.parseInt(m_inService);
            if (minute <= 0) return "0";
            // 출차서비스
            minute -= Integer.parseInt(m_outService);
            if (minute <= 0) return "0";
            // 기본구간
            minute -= Integer.parseInt(m_baseSection);
            if (minute <= 0) return "" + Integer.parseInt(m_baseAmt);

            minute -= 30;
            if (minute <= 0) return "" + 1000;
            // 반복구간
            int parkTime = (int) ((minute - 1) / Integer.parseInt(m_repeatSection)) + 1;


            //chargeAmt = Integer.parseInt(baseAmt) + parkTime * Integer.parseInt(repeatAmt);
            chargeAmt = 1000 + parkTime * Integer.parseInt(m_repeatAmt);

            if ("01".equals(parkTypeCode)) {
                // 2015.11.12 - 시간권 후불은 3급지만 주차 최대 금액 적용...
                if ("3".equals(parkClass)) {
                    // 주차요금 최대금액이 넘어가면 최대금액으로 설정
                    chargeAmt = setParkMaxAmt(chargeAmt, m_setParkMax);
                } else {
                    // 2015.11.08 - 시간권 후불은 주차 최대 금액 적용 안 함...
                    chargeAmt = setParkMaxAmt(chargeAmt, m_setParkMax);
                }

            } else {
                // 주차요금 최대금액이 넘어가면 최대금액으로 설정
                chargeAmt = setParkMaxAmt(chargeAmt, m_setParkMax);
            }

        } catch (NullPointerException e) {
            if (Constants.DEBUG_PRINT_LOG) {
                e.printStackTrace();
            } else {
                System.out.println("예외 발생");
            }
        }

        return String.valueOf(chargeAmt);
    }

    public void InitPreferences() {
        Preferences.putValue(this, Constants.PREFERENCE_MCPAY_TRANSACTION_NO, "");
        Preferences.putValue(this, Constants.PREFERENCE_MCPAY_APPROVAL_NO, "");
        Preferences.putValue(this, Constants.PREFERENCE_MCPAY_APPROVAL_DATE, "");
        Preferences.putValue(this, Constants.PREFERENCE_MCPAY_CARD_NO, "");
        Preferences.putValue(this, Constants.PREFERENCE_MCPAY_CARD_COMPANY, "");
        Preferences.putValue(this, Constants.PREFERENCE_MCPAY_TOTAL_AMT, "");

    }


    public void changeStatusBarColor(String color) {
        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.parseColor(color));
        }
    }
}

