package com.pms.gapyeong.activity;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Random;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.bluetooth.BluetoothDevice;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.pms.gapyeong.R;
import com.pms.gapyeong.common.CalendarInfo;
import com.pms.gapyeong.common.Constants;
import com.pms.gapyeong.common.DeviceInfo;
import com.pms.gapyeong.common.ImageLoader;
import com.pms.gapyeong.common.MsgUtil;
import com.pms.gapyeong.common.Preferences;
import com.pms.gapyeong.common.Util;
import com.pms.gapyeong.database.CodeHelper;
import com.pms.gapyeong.database.PictureHelper;
import com.pms.gapyeong.pay.PayData;
import com.pms.gapyeong.pay.PayVo;
import com.pms.gapyeong.vo.ExitPaymentData;
import com.pms.gapyeong.vo.ParkIO;
import com.pms.gapyeong.vo.PictureInfo;
import com.pms.gapyeong.vo.ReceiptType13Vo;
import com.pms.gapyeong.vo.ReceiptType4Vo;
import com.pms.gapyeong.vo.ReceiptType5Vo;
import com.pms.gapyeong.vo.UnPayManagerItem;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

public class ExitCommitActivity extends ExitAbstractActivity {

    private static final int DIALOG_PARK_BOARD = 500;//serviceStartTime

    private Button btn_top_left;
    private TextView tv_top_title;

    private int mYear;
    private int mMonth;
    private int mDay;

    private Button btn_impdate;


    private Button btnOutCommit;
    private Button btnEntranceModify;
    private Button btnUnPay;
    private Button btnInprint;
    private Button btn_car_no_1;
    private Button btn_car_no_2;
    private Button btn_car_no_3;
    private Button btn_car_no_4;
    private Button btn_car_no_manual;

    private EditText etPhoneNumber;
    private Button btnParkPosition;

    private TextView tvParkType;
    private Button btn_sel_park_type;
    private TextView tvChargeType;
    private TextView tvChargeDiscount;
    private TextView tvPayType;

    private TextView tvParkInTime;
    private TextView tvParkOutTime;
    private TextView tvEntrance;

    private TextView tvParkPayment;
    private TextView tvDiscountAmt;
    private Button btn_add_dis;
    private boolean isAddDis = false;

    private TextView tvUnPayment;
    private TextView tvPrePayment;
    private TextView tvPaymentAmt;
    private TextView tvPaymentAmtMinus;

    private ImageView iv_image;

    private ParkIO parkIO;
    private String exitTime;

    private ReceiptType4Vo type4Vo;

    // 변경 전 차량번호
    private String carNoOrg = "";
    // 변경된 차량번호(등록 된 차량번호)
    private String carNo = "";

    private String discountAmt = "0";
    private String gOutTime = "";
    private int truncAmt = 0;

    private boolean isUnpayExit = false;

    private Bundle chgExtras;
    private ReceiptType13Vo type13Vo;
    private boolean isSelbuttonStatus = true;/* 	YOON-1.09 */
    private int ADJ_AMT = 0;

    private Button btnExpectFee; //2016-11-10 출차등록화면에 출차시간 우측에 "확인"버튼 추가하여 선택시 마감시간까지의 주차요금이 팝업으로 떠서 마감시간까지 주차시의 주차요금이 보여지고 "닫기"선택시 창이 닫히는 확인용 팝업 추가요청

    private PictureHelper pictureHelper = PictureHelper.getInstance(this);

    private String gOrdr_idxx = "";
    private String gVcnt_account = "";
    private String gIpgm_date = "";
    private String gBank_name = "";
    private TextView tvMemo;
    private int isAddDisCnt=0;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.exit_commit_layout);
        unPayPinNums = new StringBuffer();
//        String date = Util.getYmdhms("yyyy-MM-dd");

        Calendar cDate = Util.nextMonth(1);

//        mYear = Integer.parseInt(date.split("-")[0]);
//        mMonth = Integer.parseInt(date.split("-")[1]);
//
//        if (mMonth == 12) {
//            mYear = mYear + 1;
//            mMonth = 1;
//        } else {
//            mMonth = mMonth + 1;
//        }
//        //mDay   = Integer.parseInt(date.split("-")[2]);
//        mDay = 5;

        mYear = cDate.get(Calendar.YEAR);
        mMonth= cDate.get(Calendar.MONTH)+1;
//        mDay = cDate.getActualMaximum(Calendar.DAY_OF_MONTH);
        mDay = 5;

        cch = CodeHelper.getInstance(this);
    }

    @Override
    protected void initLayoutSetting() {
        super.initLayoutSetting();
        tvMemo = (TextView) findViewById(R.id.tvMemo);
        btn_top_left = (Button) findViewById(R.id.btn_top_left);
        btn_top_left.setVisibility(View.VISIBLE);
        btnInprint = (Button) findViewById(R.id.btn_top_inprint);
        btnInprint.setVisibility(View.VISIBLE);

        btn_top_left.setOnClickListener(this);
        tv_top_title = (TextView) findViewById(R.id.tv_top_title);
        tv_top_title.setText("출차등록");

        btnOutCommit = (Button) findViewById(R.id.btnOutCommit);
        btnEntranceModify = (Button) findViewById(R.id.btnEntranceModify);
        btnUnPay = (Button) findViewById(R.id.btnunpay);

        btn_car_no_1 = (Button) findViewById(R.id.btn_car_no_1);
        btn_car_no_2 = (Button) findViewById(R.id.btn_car_no_2);
        btn_car_no_3 = (Button) findViewById(R.id.btn_car_no_3);
        btn_car_no_4 = (Button) findViewById(R.id.btn_car_no_4);
        btn_car_no_manual = (Button) findViewById(R.id.btn_car_no_manual);
        btnExpectFee = (Button) findViewById(R.id.btn_expect_fee);

        etPhoneNumber = (EditText) findViewById(R.id.etPhoneNumber);
        btnParkPosition = (Button) findViewById(R.id.btnParkPosition);

        tvParkType = (TextView) findViewById(R.id.tvParkType);
        btn_sel_park_type = (Button) findViewById(R.id.btn_sel_park_type);
        tvChargeType = (TextView) findViewById(R.id.tvChargeType);
        tvChargeDiscount = (TextView) findViewById(R.id.tvChargeDiscount);

        tvParkInTime = (TextView) findViewById(R.id.tvParkInTime);
        tvParkOutTime = (TextView) findViewById(R.id.tvParkOutTime);
        tvEntrance = (TextView) findViewById(R.id.tvEntrance);
        tvParkPayment = (TextView) findViewById(R.id.tvParkPayment);
        tvDiscountAmt = (TextView) findViewById(R.id.tvDiscountAmt);
        btn_add_dis = (Button) findViewById(R.id.btn_add_dis);
        tvUnPayment = (TextView) findViewById(R.id.tvUnPayment);
        tvPrePayment = (TextView) findViewById(R.id.tvPrePayment);
        tvPaymentAmt = (TextView) findViewById(R.id.tvPaymentAmt);
        tvPaymentAmtMinus = (TextView) findViewById(R.id.tvPaymentAmtMinus);
        tvPayType = (TextView) findViewById(R.id.tvPayType);
        iv_image = (ImageView) findViewById(R.id.iv_image);
        btn_car_no_1.setOnClickListener(this);
        btn_car_no_2.setOnClickListener(this);
        btn_car_no_3.setOnClickListener(this);
        btn_car_no_4.setOnClickListener(this);
        btn_car_no_manual.setOnClickListener(this);

        btnOutCommit.setOnClickListener(this);
        btnEntranceModify.setOnClickListener(this);
        btnUnPay.setOnClickListener(this);
        tvUnPayment.setOnClickListener(this);
        tvParkOutTime.setOnClickListener(this);


        tvParkPayment.setTag("0");

        btnParkPosition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                apiParkIoListCall(Util.getEncodeStr("%"), "I1", today, today);
            }
        });

        btnInprint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                apiParkIoListCall_Print("I1");
            }
        });

        btn_sel_park_type.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // String parkType = cch.findCodeByKnCode("PY", tvParkType.getText().toString());

                /* 	YOON-1.09 :  변경버튼 수정(선불입차 시에는 변경버튼 비활성화)
                 * isSelbuttonStatus = true 일때에만 변경버튼 활성화 : 후불입차시
                 * 시작*/
				/*
				if(isSelbuttonStatus)
				{				
					setCommonDialog_parkType("주차종류","PY", Constants.COMMON_TYPE_PARK_TYPE, null, null);
				}
				*/

                if (isSelbuttonStatus) {
                    String workTime = workStartTime.substring(0, 2)
                            + ":"
                            + workStartTime.substring(2)
                            + "~"
                            + workEndTime.substring(0, 2)
                            + ":"
                            + workEndTime.substring(2);

                    type13Vo = new ReceiptType13Vo("입차영수증"        // 타이틀
                            , Util.getYmdhms("yyyy-MM-dd HH:mm:ss")    // 출력일자
                            , btnParkPosition.getText().toString().replaceAll("면", "")            // 주차면
                            , ""                // 주차종류
                            , ""                // 요금종류
                            , ""                // 요금할인
                            , tvUnPayment.getText().toString().replaceAll("원", "")    // 총미수금액
                            , ""    // 선납금액
                            , ""    // 시작시간
                            , ""    // 출차예정시간
                            , ""    // 총 분
                            , BIZ_NO            // 사업자번호
                            , BIZ_NAME            // 사업자명
                            , BIZ_TEL, empTicket_print, workTime);            // 사업자연락처

                    type13Vo.setPioNum(parkIO.getPioNum());
                    type13Vo.setCarNum(carNo);
                    type13Vo.setApprovalNo(parkIO.getApprovalNo());
                    type13Vo.setParkName(parkName);
                    type13Vo.setEmpName(empName);
                    type13Vo.setEmpPhone(empPhone);
                    type13Vo.setEmpTel(empTel);
                    type13Vo.setAccount_No(empAccount_No);
                    type13Vo.setBank_MSG(empBank_MSG);
                    type13Vo.setKN_ACCOUNT(empKN_ACCOUNT);
                    type13Vo.setKN_BANK(empKN_BANK);
                    type13Vo.setEmpBusiness_Tel(empBusiness_TEL);
                    type13Vo.setOutPrint(Preferences.getValue(ExitCommitActivity.this, Constants.PREFERENCE_OUT_PRINT, ""));
                    parkTypeChangeDialog(parkIO.getCdPark(), tvChargeType.getText().toString(), tvChargeDiscount.getText().toString(), tvParkType.getText().toString()
                            , tvParkInTime.getText().toString(), tvParkOutTime.getText().toString(), type13Vo);
                }


            }
        });

        btn_add_dis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isAddDisCnt<2) {
//                    btn_add_dis.setBackgroundResource(R.drawable.common_02);
                    isAddDisCnt++;
                    isAddDis = true;
                    Toast.makeText(getApplicationContext(),"5분 조정되었습니다.",Toast.LENGTH_SHORT).show();
                } else {
//                    btn_add_dis.setBackgroundResource(R.drawable.btn_basic_press);
                    isAddDis = false;
                    isAddDisCnt = 0;
                    Toast.makeText(getApplicationContext(),"기본값으로 조정되었습니다.",Toast.LENGTH_SHORT).show();
                    tvParkOutTime.setText(gOutTime);
                }

                setParkAmt(true);
            }
        });

        /**
         tvParkType.setOnClickListener(new View.OnClickListener() {
        @Override public void onClick(View v) {
        setCommonDialog("주차종류","PY", Constants.COMMON_TYPE_PARK_TYPE, tvParkType);
        }
        });
         **/
        //tvChargeType.setText
        tvChargeType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String parkType = cch.findCodeByKnCode("PY", tvParkType.getText().toString());
                // 일일권일 경우
                if (parkType.equals("02")) {
                    MsgUtil.ToastMessage(ExitCommitActivity.this, tvParkType.getText().toString() + " 인 경우 요금종류를 변경할 수 없습니다.");
                    return;
                } else {

                    if ("2".equals(parkClass)) {
                        setCommonDialog("요금종류", "AM2", Constants.COMMON_TYPE_CHARGE_TYPE, tvChargeType);
                    } else if ("3".equals(parkClass)) {
                        setCommonDialog("요금종류", "AM3", Constants.COMMON_TYPE_CHARGE_TYPE, tvChargeType);
                    } else if ("4".equals(parkClass)) {
                        setCommonDialog("요금종류", "AM4", Constants.COMMON_TYPE_CHARGE_TYPE, tvChargeType);
                    } else if ("5".equals(parkClass)) {
                        setCommonDialog("요금종류", "AM5", Constants.COMMON_TYPE_CHARGE_TYPE, tvChargeType);
                    } else if ("6".equals(parkClass)) {
                        setCommonDialog("요금종류", "AM6", Constants.COMMON_TYPE_CHARGE_TYPE, tvChargeType);
                    }

                    //setCommonDialog("요금종류","AM2", Constants.COMMON_TYPE_CHARGE_TYPE, tvChargeType);
                }
            }
        });

        tvChargeDiscount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String parkType = cch.findCodeByKnCode("PY", tvParkType.getText().toString());
                // 일일권 또는 선납금일 경우
                if (parkType.equals("02") || parkType.equals("04")) {
                    MsgUtil.ToastMessage(ExitCommitActivity.this, tvParkType.getText().toString() + " 은 요금할인을 변경할 수 없습니다.");
                    return;
                } else {
                    setCommonDialog("요금할인", "DC", Constants.COMMON_TYPE_CHARGE_DISCOUNT, tvChargeDiscount);
                }
            }
        });

        iv_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (parkIO != null) {
                    goToPictureView(parkIO.getPioNum(), parkIO.getCarNo(), Constants.PICTURE_TYPE_O1, parkIO.getCdPark(), empCode);
                }
            }
        });

        btnExpectFee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Dialog dialog = new Dialog(ExitCommitActivity.this);

                dialog.setContentView(R.layout.dialog_common_alert);

                dialog.setTitle("마감시간까지 금액 환산");
                TextView text = (TextView) dialog.findViewById(R.id.alert_msg); // 해당 설정 파일내에서 id 가 text 인 TextView 를 찾는다.
                String nowDate = Util.getYmdhms("yyyyMMdd");
                text.setText(String.format("%,d", getExpectParkAmt(nowDate + workEndTime.replace(":", ""))) + "원");
                Button button = (Button) dialog.findViewById(R.id.btn_ok);
                button.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();     //닫기
                    }
                });
                dialog.show();
				
				/*
				dialog.setO("확인", new DialogInterface.OnClickListener() {
				    @Override
				    public void onClick(DialogInterface dialog, int which) {
				    dialog.dismiss();     //닫기
				    }
				});
				
				alert.setTitle("마감시간까지 금액 환산");
				alert.setMessage();
				AlertDialog dialog = alert.show();
				TextView messageText = (TextView)dialog.findViewById(android.R.id.message);
		        messageText.setGravity(Gravity.CENTER);
		        */

            }
        });


        Intent intent = getIntent();
        if (intent != null) {
            parkIO = (ParkIO) intent.getSerializableExtra("parkIO");
            Log.i(TAG, "parkIO : " + parkIO);

            tvMemo.setText(parkIO.getKn_comment());

            if ("2".equals(parkClass)) {
                setCommonItem("AM2", Constants.COMMON_TYPE_CHARGE_TYPE, tvChargeType, "M2-1");
            } else if ("3".equals(parkClass)) {
                setCommonItem("AM3", Constants.COMMON_TYPE_CHARGE_TYPE, tvChargeType, "M3-1");
            } else if ("4".equals(parkClass)) {
                setCommonItem("AM4", Constants.COMMON_TYPE_CHARGE_TYPE, tvChargeType, "M4-1");
            } else if ("5".equals(parkClass)) {
                setCommonItem("AM5", Constants.COMMON_TYPE_CHARGE_TYPE, tvChargeType, "M5-1");
            } else if ("6".equals(parkClass)) {
                setCommonItem("AM6", Constants.COMMON_TYPE_CHARGE_TYPE, tvChargeType, "M6-1");
            }

            if ("01".equals(parkIO.getParkType())) {
                tvPayType.setText(" 미결제");
            } else if ("04".equals(parkIO.getParkType()) || "02".equals(parkIO.getParkType())) {
                if ("".equals(parkIO.getCardNo())) {
                    tvPayType.setText(" 현금");
                } else {
                    tvPayType.setText(" 카드");
                }
            } else {
                tvPayType.setText(" 미결제");
            }

            //setCommonItem("AM2", Constants.COMMON_TYPE_CHARGE_TYPE, tvChargeType, parkAMtype);

            carNoOrg = parkIO.getCarNo();
            carNo = parkIO.getCarNo();
            setCarnoView(carNo, btn_car_no_1, btn_car_no_2, btn_car_no_3, btn_car_no_4, btn_car_no_manual);

            Constants.KEY_CARNO_1 = btn_car_no_1.getText().toString();
            Constants.KEY_CARNO_2 = btn_car_no_2.getText().toString();
            Constants.KEY_CARNO_3 = btn_car_no_3.getText().toString();
            Constants.KEY_CARNO_4 = btn_car_no_4.getText().toString();
            Constants.KEY_CARNO_MANUAL = btn_car_no_manual.getText().toString();

            etPhoneNumber.setText(parkIO.getTel());
            btnParkPosition.setText(parkIO.getCdArea() + "면");

            tvParkType.setText(cch.findCodeByCdCode("PY", parkIO.getParkType()));

            /* 	YOON-1.09
             *  선불입차시 (04) 변경버튼 비활성 화 구문 추가
             * 시작 */
            if ("04".equals(parkIO.getParkType()) || "02".equals(parkIO.getParkType())) {
                btn_sel_park_type.setBackgroundResource(R.drawable.common_02);
                btnEntranceModify.setBackgroundResource(R.drawable.common_02);
                isSelbuttonStatus = false;
            } else {
                btn_sel_park_type.setBackgroundResource(R.drawable.common_00);
                isSelbuttonStatus = true;
            }
            /*끝*/

            tvChargeDiscount.setText(cch.findCodeByCdCode("DC", parkIO.getDisCd()));

            Log.d(TAG, " Constants.HOLD_TIME >>> " + Constants.HOLD_TIME);
            if (!Util.isEmpty(Constants.HOLD_TIME)) {
                exitTime = Constants.HOLD_TIME;
            } else {
                exitTime = Util.getYmdhms("HH:mm:ss");
            }
            // Log.d(TAG, " exitTime >>> " + exitTime);
            Log.d(TAG, parkIO.getParkType());

            tvParkInTime.setText(Util.dateString(parkIO.getParkInDay()));

            if (Integer.valueOf(exitTime.substring(0, 5).replace(":", "")) > Util.sToi(workEndTime)) {
                exitTime = workEndTime.substring(0, 2) + ":" + workEndTime.substring(2, 4) + ":00";
            }


            tvParkOutTime.setText(Util.dateString(Util.getYmdhms("yyyyMMdd")) + " " + exitTime.substring(0, 5));
            gOutTime = tvParkOutTime.getText().toString();
            // 선불금
            tvPrePayment.setText(parkIO.getAdvAmt() + "원");

            apiUnpayAttach(parkIO.getCarNo());
        }

        connectedDevice();

    }

    private void connectedDevice() {
        try {
            if (mPrintService.getState() == 0) {
                BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(mBluetoothAddress);
                // Attempt to connect to the device
                mPrintService.connect(device, true);
            }
        } catch (IllegalArgumentException e) {
            System.out.println("IllegalArgumentException 예외 발생");
        } catch (NullPointerException e) {
            System.out.println("NullPointerException 예외 발생");
        }
    }

    @Override
    public void onClick(View v) {

        // TODO Auto-generated method stub
        super.onClick(v);
        Boolean bsRet = false;
        cch = CodeHelper.getInstance(this);

        switch (v.getId()) {
            case R.id.btn_car_no_1:
                goToActivityInputCarNumber(Constants.RESULT_INPUT_CAR_NUMBER, Constants.CAR_UPDATE, 1);
                break;
            case R.id.btn_car_no_2:
                goToActivityInputCarNumber(Constants.RESULT_INPUT_CAR_NUMBER, Constants.CAR_UPDATE, 2);
                break;
            case R.id.btn_car_no_3:
                goToActivityInputCarNumber(Constants.RESULT_INPUT_CAR_NUMBER, Constants.CAR_UPDATE, 3);
                break;
            case R.id.btn_car_no_4:
                goToActivityInputCarNumber(Constants.RESULT_INPUT_CAR_NUMBER, Constants.CAR_UPDATE, 4);
                break;
            case R.id.btn_car_no_manual:
                goToActivityInputCarNumber(Constants.RESULT_INPUT_CAR_NUMBER, Constants.CAR_UPDATE, 5);
                break;
            case R.id.btn_top_left:
                finish();
                break;

            case R.id.btnOutCommit:
//                try {
//
//
//                    // 할인금액 조회
//                    String disRate = Util.isNVL(cch.findCodeByDisRate("DC", tvChargeDiscount.getText().toString()), "0");
//                    if (!"0".equals(disRate) && !is4picSave(parkIO.getPioNum())) {
//                        goToPictureView(parkIO.getPioNum(), parkIO.getCarNo(), Constants.PICTURE_TYPE_DIS, parkIO.getCdPark(), empCode);
//                        Toast.makeText(ExitCommitActivity.this, "할인을 선택한 경우 사진등록이 필요합니다.", Toast.LENGTH_LONG).show();
//                        return;
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }


                int charge = Integer.parseInt(tvPaymentAmt.getText().toString().replace(",", "").replace("원", ""));
                type4Vo = new ReceiptType4Vo(Util.getYmdhms("yyyy-MM-dd HH:mm:ss")    // 출력일자
                        , tvParkType.getText().toString()            // 주차종류 :
                        , tvEntrance.getText().toString()            // 주차시간 :
                        , btnParkPosition.getText().toString()        // 주차면 :
                        , tvChargeDiscount.getText().toString()        // 할인종류 :
                        , (parkIO.getParkInDay().substring(8, 10) + ":" + parkIO.getParkInDay().substring(10, 12) + ":" + parkIO.getParkInDay().substring(12))        // 입차시간 :
                        , exitTime        // 출차시간 :
                        , "" + charge        // 납부금액 :
                        , tvPrePayment.getText().toString().replace(",", "").replace("원", "")        // 선납금액 :
                        , tvParkPayment.getText().toString().replace(",", "").replace("원", "")        // 주차요금 :
                        , tvUnPayment.getText().toString().replace(",", "").replace("원", "")        // 미납금액 :
                        , tvDiscountAmt.getText().toString().replace(",", "").replace("원", "")        // 할인금액 :
                        , BIZ_NO                    // 사업자번호
                        , BIZ_NAME
                        , BIZ_TEL);

                type4Vo.setPioNum(parkIO.getPioNum());
                type4Vo.setCarNum(carNo);
                type4Vo.setParkName(parkName);
                type4Vo.setEmpName(empName);
                type4Vo.setEmpPhone(empPhone);
                type4Vo.setEmpTel(empTel);
                type4Vo.setEmpBusiness_Tel(empBusiness_TEL);
                type4Vo.setOutPrint(Preferences.getValue(this, Constants.PREFERENCE_OUT_PRINT, ""));
                if (("04").equals(parkIO.getParkType())) {
                    if (!("0").equals(parkIO.getCardAmt())) {
                        if (Integer.valueOf((tvPaymentAmt.getText().toString().replace(",", "").replace("원", ""))) < 0) {
                            bsRet = true;
                        }
                    }
                }

                if (bsRet) {
                    //String payAmt = String.valueOf(Integer.valueOf(parkIO.getCardAmt()) - Integer.valueOf(parkIO.getCouponAmt()));
                    goToPayCancelActivity(PayData.PAY_DIALOG_CANCEL_APPROVAL_CANCEL, parkIO.getCardAmt(), parkIO.getApprovalNo(), parkIO.getApprovalDate(), type4Vo);
                    //apiParkFeeCancelCall(parkIO.getPioNum());
                } else {
                    exitDialog(tvParkType.getText().toString()
                            , tvPaymentAmt.getText().toString()
                            , type4Vo, tvUnPayment.getText().toString().replaceAll("원", ""));
                }
			/*
		    exitDialog(tvParkType.getText().toString()
					 , tvPaymentAmt.getText().toString()
					 , type4Vo);
			*/
                break;

            case R.id.btnunpay:
                exitUnpayDialog(mYear, mMonth, mDay);
                break;

            case R.id.btnEntranceModify:
                if (isSelbuttonStatus)
                    goEntranceModify();
                break;

            case R.id.tvUnPayment:
                if (!tvUnPayment.getText().toString().replaceAll("원", "").equals("0"))
                    goToUnpaidManagerActivity(carNo);
                break;
            case R.id.tvParkOutTime:
                if (Constants.isSetOutTime) {
                    DialogTimePicker();
                }
                break;
        }
    }

    @Override
    protected void request(int type, Object data) {


        if (type == PayData.PAY_DIALOG_TYPE_COUPON) {
            couponData = (String) data;
        } else if (type == PayData.PAY_DIALOG_TYPE_EXIT_CASH || type == PayData.PAY_DIALOG_TYPE_EXIT_CARD) {
            ExitPaymentData epd = (ExitPaymentData) data;
            if (out == null) {
                initParkOut(parkIO.getPioNum(), parkIO.getCdPark(), exitTime, carNo
                        , parkIO.getParkType(), parkIO.getTel(), tvPrePayment.getText().toString().replaceAll("원", "").trim()
                        , parkIO.getDisCd(), ""
                        , tvChargeType.getText().toString());

            }

            if (type == PayData.PAY_DIALOG_TYPE_EXIT_CARD) {
                Log.d("111", "----------------------------" + parkIO.getPioNum());
                apiPrintUpdate(parkIO.getPioNum());
            }

            apiParkOutCall(epd, tvParkType.getText().toString(), btnParkPosition.getText().toString().replaceAll("면", "")
                    , tvChargeDiscount.getText().toString(), tvParkPayment.getTag().toString(), tvPaymentAmt.getText().toString().replaceAll("원", ""), discountAmt, "" + truncAmt, ADJ_AMT);
        } else if (type == PayData.PAY_DIALOG_TYPE_PARK_CHANGE_ADV) {
            chgExtras = (Bundle) data;
        } else if (type == PayData.PAY_DIALOG_TYPE_PARK_CHANGE_LATER) {
            chgExtras = (Bundle) data;
            // 이전 선불결제에서 카드 결제는 결제 취소 후 진행...
            if (!Util.isEmpty(parkIO.getApprovalNo()) && !Util.isEmpty(parkIO.getCardCompany())) {
                type4Vo = new ReceiptType4Vo(Util.getYmdhms("yyyy-MM-dd HH:mm:ss")    // 출력일자
                        , cch.findCodeByCdCode("PY", parkIO.getParkType())            // 주차종류 :
                        , tvEntrance.getText().toString()            // 주차시간 :
                        , btnParkPosition.getText().toString().replaceAll("면", "")        // 주차면 :
                        , cch.findCodeByCdCode("DC", parkIO.getDisCd())    // 할인종류 :
                        , parkIO.getParkInDay()        // 입차시간 :
                        , parkIO.getParkOutDay()    // 출차시간 :
                        , parkIO.getCardAmt()        // 납부금액 :
                        , parkIO.getAdvAmt()        // 선납금액 :
                        , parkIO.getParkAmt()        // 주차요금 :
                        , parkIO.getYetAmt()        // 미납금액 :
                        , parkIO.getDisAmt()        // 할인금액 :
                        , BIZ_NO                    // 사업자번호
                        , BIZ_NAME
                        , BIZ_TEL);

                type4Vo.setPioNum(parkIO.getPioNum());
                type4Vo.setCarNum(carNo);
                type4Vo.setParkName(parkName);
                type4Vo.setEmpName(empName);
                type4Vo.setEmpPhone(empPhone);
                type4Vo.setEmpTel(empTel);
                type4Vo.setEmpBusiness_Tel(empBusiness_TEL);
                type4Vo.setOutPrint(Preferences.getValue(this, Constants.PREFERENCE_OUT_PRINT, ""));
                // TODO 결제 취소..
                goToPayCancelActivity(PayData.PAY_DIALOG_CANCEL_APPROVAL_CHANGE, parkIO.getCardAmt(), parkIO.getApprovalNo(), parkIO.getApprovalDate(), type4Vo);
            } else {
                apiParkTypeChangeUpdateCall(parkIO.getPioNum(), null, chgExtras);
            }
        } else if (type == PayData.PAY_DIALOG_TYPE_PREPAY_CASH || type == PayData.PAY_DIALOG_TYPE_PREPAY_CARD) {
            PayVo payVo = (PayVo) data;
            if (type == PayData.PAY_DIALOG_TYPE_PREPAY_CASH) {
                type13Vo.setCashReceipt(payVo.getCashApprovalNo());
                type13Vo.setCardNo(payVo.getCardNo());
                type13Vo.setCardCompany(payVo.getCardCompany());
                type13Vo.setApprovalNo(payVo.getApprovalNo());
                // 일반 입차 영수증일 경우 결제 관련은 출력하지 않는다.
                if ("01".equals(type13Vo.getParkType()) || "03".equals(type13Vo.getParkType())) {
                    type13Vo.setPrintBill(true);
                }
                PayData payData = new PayData(this, OUT_PRINT, IN_PRINT, TICKET_PRINT);
                payData.printDefault(PayData.PAY_DIALOG_TYPE_ENTRANCE_TIME, type13Vo, mPrintService);
            }
            apiParkTypeChangeUpdateCall(parkIO.getPioNum(), payVo, chgExtras);
        }

    }

    @Override
    protected void requestApi(String type, Object data) {

        if (type == Constants.INTERFACEID_PARK_IO) {
            ArrayList<ParkIO> list = (ArrayList<ParkIO>) data;
            statusBoardHelper.setParkIo(list);
            basicListDialog(DIALOG_PARK_BOARD, "주차면", statusBoardHelper.getEmptyNumberList(), btnParkPosition);
        } else if (type == Constants.INTERFACEID_PARK_OUT) {
            boolean isDeleteIo = Preferences.getValue(this, Constants.PREFERENCE_SETTING_DELETE_IO_IMG, false);
            if (isDeleteIo) {
                // 사진 삭제
                deletePicture(carNoOrg);
            }

            // 미수 출차 시
            if (isUnpayExit) {
                // TODO 미수 요청 후 영수증 처리
                Log.d("333", "2222-------------------------");
                apiParkUnpayListCall("K2", carNo, parkCode, today, today);
                return;
            }
            // 현금 출차 시 영수증 출력
            if (Util.isEmpty(out.getTransactionNo())) {
                if (!("정기권").equals(tvParkType.getText().toString())) { //정기권은 영수증 출력 안함
                    type4Vo.setTitle("출차영수증");
                    type4Vo.setCouponAmt(out.getCouponAmt());
                    PayData payData = new PayData(this, OUT_PRINT, IN_PRINT, TICKET_PRINT);

                    if (Constants.isReceiptPrint) {
                        payData.printDefault(PayData.PAY_DIALOG_TYPE_EXIT_ONLY, type4Vo, mPrintService); //출차시 영수증 출력 주석
                        payData.ImagePrint(empLogo_image);
                        try {
                            payData.printText("\n\n");
                        } catch (UnsupportedEncodingException e) {
                            System.out.println("UnsupportedEncodingException 예외 발생");
                        }
                    }
                }
            }

            startActivity(new Intent(this, StatusBoardActivity.class));
            finish();
        } else if (type == Constants.INTERFACEID_PARK_UNPAYATTACH_V2) {
            String unpayAmt = (String) data;
            // 미납금 설정
            tvUnPayment.setText(unpayAmt);

            setParkAmt(false);

            apiParkIOPictureCall(parkIO.getPioNum(), parkIO.getCdPark());
        } else if (type == Constants.INTERFACEID_PARK_IO_PICTURE) {
            /**
             * 2015.11.13 - 출차화면 사진조회는 서버가 아닌 로컬에서 조회되도록 변경...
             String pictureURL = (String) data;
             if(!Util.isEmpty(pictureURL)){
             ImageLoader.setImageUrlView(ExitCommitActivity.this, pictureURL, iv_image);
             } else {
             iv_image.setImageDrawable(null);
             }
             **/
            String pictureURL = (String) data;
            ArrayList<PictureInfo> list = (ArrayList<PictureInfo>) pictureHelper.getSearchPioNumList(parkIO.getPioNum());
            iv_image.setImageDrawable(null);
            if (!Util.isEmpty(pictureURL) && list != null && list.size() >= 0) {
                for (PictureInfo info : list) {
                    String localImgPath = info.getPath();
                    File imgFile = new File(localImgPath);
                    if (imgFile.exists()) {
                        ImageLoader.setImageView(localImgPath, iv_image);
                        break;
                    }
                }
            }
        } else if (type == Constants.INTERFACEID_PARKUNPAY_INFO) {


            ArrayList<UnPayManagerItem> unPayList = (ArrayList<UnPayManagerItem>) data;
            if (unPayList == null) {
                return;
            }
            int Len = unPayList.size();
            boolean isReceipUnpay = Preferences.getValue(ExitCommitActivity.this, Constants.PREFERENCE_PRINT_UNPAY_ENABLE, true);
            for (int i = 0; i < Len; i++) {
                UnPayManagerItem info = unPayList.get(i);
                if (carNo.equals(info.getCarNo()) && out.getPioNumber().equals(info.getPioNum())) {

                    if (isReceipUnpay) {
                        String title = "미수청구서";
                        ReceiptType5Vo type5Vo = new ReceiptType5Vo(title
                                , Util.getYmdhms("yyyy-MM-dd HH:mm:ss")            // 출력일자
                                , out.getChargeTypeStr()
                                , cch.findCodeByCdCode("DC", out.getDicCode())
                                , info.getSumAmt()    //총미납금
                                , BIZ_NAME
                                , BIZ_BANK_MSG
                                , BIZ_BANK
                                , BIZ_ACCOUNT_NO
                                , BIZ_ACCOUNT_NAME
                                , BIZ_COPY_RIGHT
                                , BIZ_NO
                                , info.getKnPark()//주차장명
                                , info.getParkIn()//입차시간
                                , info.getParkOut()//출차시간
                                , empGroupCD
                        );

                        type5Vo.setPioNum(info.getPioNum());
                        type5Vo.setCarNum(carNo);
                        type5Vo.setParkName(parkName);
                        type5Vo.setEmpName(empName);
                        type5Vo.setEmpPhone(empPhone);
                        type5Vo.setEmpTel(empTel);
                        type5Vo.setEmpBusiness_Tel(empBusiness_TEL);
                        type5Vo.setOutPrint(Preferences.getValue(this, Constants.PREFERENCE_OUT_PRINT, ""));

                        if(!"".equals(gVcnt_account)){
                            type5Vo.setKN_BANK(gBank_name);
                            type5Vo.setAccount_No(gVcnt_account);
                            type5Vo.setBank_MSG("납부기한 : " + gIpgm_date.substring(0, 4) + "-" + gIpgm_date.substring(4, 6) + "-" + gIpgm_date.substring(6, 8));
                            type5Vo.setKN_ACCOUNT("가평군청");

                        }else{
                            type5Vo.setAccount_No(empAccount_No);
                            type5Vo.setBank_MSG(empBank_MSG);
                            type5Vo.setKN_ACCOUNT(empKN_ACCOUNT);
                            type5Vo.setKN_BANK(empKN_BANK);
                        }

                        PayData payData = new PayData(this, OUT_PRINT, IN_PRINT, TICKET_PRINT);
                        payData.printDefault(PayData.PAY_DIALOG_TYPE_UNPAY_BILL, type5Vo, mPrintService);
                        payData.ImagePrint(empLogo_image);
                        try {
                            payData.printText("\n\n");
                        } catch (UnsupportedEncodingException e) {
                            if(Constants.DEBUG_PRINT_LOG){
                                e.printStackTrace();
                            }else{
                                System.out.println("예외 발생");
                            }
                        }
                    }

                    Preferences.putValue(ExitCommitActivity.this, Constants.PREFERENCE_PRINT_UNPAY_ENABLE, true);
                    startActivity(new Intent(this, StatusBoardActivity.class));
                    finish();
                    break;
                }
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode,
                                    Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);

        String transactionNo = "";
        String approvalNo = "";
        String approvalDate = "";
        String cardNo = "";
        String cardCompany = "";
        String cardAmt = "";

        switch (requestCode) {

            case Constants.RESULT_INPUT_CAR_NUMBER:
                if (resultCode != RESULT_OK) {
                    return;
                }

                if (data != null) { // 번호 재입력 시
                    String carInputMode = Util.isNVL(data.getStringExtra(Constants.CAR_INPUT_MODE));
                    if (Constants.CAR_NORMAL.equals(carInputMode)) {
                        carNo = Constants.KEY_CARNO_1 + Constants.KEY_CARNO_2 + Constants.KEY_CARNO_3 + Constants.KEY_CARNO_4;

                        setCarnoView(Constants.KEY_CARNO_1, Constants.KEY_CARNO_2, Constants.KEY_CARNO_3, Constants.KEY_CARNO_4, "",
                                btn_car_no_1, btn_car_no_2, btn_car_no_3, btn_car_no_4, btn_car_no_manual, Constants.CAR_NORMAL);

                    } else if (Constants.CAR_MANUAL.equals(carInputMode)) {
                        carNo = Constants.KEY_CARNO_MANUAL;

                        setCarnoView("", "", "", "", Constants.KEY_CARNO_MANUAL,
                                btn_car_no_1, btn_car_no_2, btn_car_no_3, btn_car_no_4, btn_car_no_manual, Constants.CAR_MANUAL);

                    }

                    // 변경 된 차량번호로 셋팅...
                    parkIO.setCarNo(carNo);

                    apiUnpayAttach(carNo);
                }
            case Constants.RESULT_CAR_PICTURE:
                Log.d(TAG, " RESULT_CAR_PICTURE ::: " + Constants.RESULT_CAR_PICTURE);
                apiParkIOPictureCall(parkIO.getPioNum(), parkIO.getCdPark());
                break;

            case PayData.PAY_DIALOG_CANCEL_APPROVAL_CHANGE: // 카드 결제 취소 후 주차종류 업데이트...
                if (resultCode == PayData.PAY_DIALOG_CANCEL_APPROVAL_CHANGE) {
                    parkIO.setParkType("01");
                    tvParkType.setText(cch.findCodeByCdCode("PY", parkIO.getParkType()));
                    apiParkTypeChangeUpdateCall(parkIO.getPioNum(), null, chgExtras);
                }
                break;
            case 7777: // 미수환수 처리 시 금액을 0원으로 처리
                if (resultCode == RESULT_OK) {
                    if (("1").equals(data.getStringExtra("RESULT_PAY_OK"))) {
                        tvUnPayment.setText("0원");
                    }
                }

                break;
		  /*	  
		  case PayData.PAY_DIALOG_CANCEL_APPROVAL_CANCEL : // 카드 결제 취소 후 주차종류 업데이트...
			  if(resultCode == PayData.PAY_DIALOG_CANCEL_APPROVAL_CANCEL){
	   			  parkIO.setParkType("01");  
	   			  tvParkType.setText(cch.findCodeByCdCode("PY", parkIO.getParkType()));
				  
				  apiParkFeeCancelCall(parkIO.getPioNum());
			  }
			  break;
		    */
            case PayData.PAY_DIALOG_CANCEL_APPROVAL_CANCEL: // 카드 결제 취소 후 주차종류 업데이트...
                transactionNo = "";
                approvalNo = "";
                approvalDate = "";
                cardNo = "";
                cardAmt = "";
                cardCompany = "";

                try {
                    mPrintService.stop();
                    if (mPrintService.getState() == 0) {

                        BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(mBluetoothAddress);
                        mPrintService.connect(device, true);
                    }
                } catch (IllegalArgumentException e) {
                    System.out.println("IllegalArgumentException 예외 발생");
                } catch (NullPointerException e) {
                    System.out.println("NullPointerException 예외 발생");
                }


                transactionNo = Preferences.getValue(this, Constants.PREFERENCE_MCPAY_TRANSACTION_NO, "");
                approvalNo = Preferences.getValue(this, Constants.PREFERENCE_MCPAY_APPROVAL_NO, "");
                approvalDate = Preferences.getValue(this, Constants.PREFERENCE_MCPAY_APPROVAL_DATE, "");
                cardNo = Preferences.getValue(this, Constants.PREFERENCE_MCPAY_CARD_NO, "");
                cardCompany = Preferences.getValue(this, Constants.PREFERENCE_MCPAY_CARD_COMPANY, "");
                cardAmt = Preferences.getValue(this, Constants.PREFERENCE_MCPAY_TOTAL_AMT, "");
                if (transactionNo == "" || cardNo == "" || approvalNo == "") {
                    Log.d("111", "Card error");
                    return;
                }

                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    System.out.println("InterruptedException 예외 발생");
                }

                Bundle eB2 = new Bundle();
                eB2.putString("transactionNo", "");
                eB2.putString("approvalNo", "");
                eB2.putString("approvalDate", "");
                eB2.putString("cardNo", "");
                eB2.putString("cardCompany", "");
                eB2.putString("cardAmt", cardAmt);
                eB2.putString("pioNum", gReceiptVo.getPioNum());
                eB2.putString("carNum", gReceiptVo.getCarNum());
                PayData payData2 = new PayData(this, OUT_PRINT, IN_PRINT, TICKET_PRINT);
                payData2.printPaymentCancel(PayData.PAY_DIALOG_CANCEL_APPROVAL_CANCEL, gReceiptVo, mPrintService, eB2, null);

                parkIO.setParkType("01");
                tvParkType.setText(cch.findCodeByCdCode("PY", parkIO.getParkType()));

                apiParkFeeCancelCall(parkIO.getPioNum());
                break;
            case PayData.PAY_DIALOG_TYPE_EXIT_CARD:  //후불 카드 출차
                transactionNo = "";
                approvalNo = "";
                approvalDate = "";
                cardNo = "";
                cardAmt = "";
                cardCompany = "";
                try {
                    try {
                        mPrintService.stop();
                        if (mPrintService.getState() == 0) {

                            BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(mBluetoothAddress);
                            mPrintService.connect(device, true);
                        }
                    }  catch (IllegalArgumentException e) {
                        System.out.println("IllegalArgumentException 예외 발생");
                    } catch (NullPointerException e) {
                        System.out.println("NullPointerException 예외 발생");
                    }

                    if (data != null) {
                        Bundle eB = data.getExtras();
                        if (eB != null) {
                            /**
                             Log.d(TAG, " eB >>>> " + eB );
                             for (String key : eB.keySet()) {
                             Log.d(TAG, " eB :::: " + key + " => " + eB.get(key) );
                             }
                             **/
                        }

                        transactionNo = Util.replaceNullIntent(eB, "transactionNo");
                        approvalNo = Util.replaceNullIntent(eB, "approvalNo");
                        approvalDate = Util.replaceNullIntent(eB, "approvalDate");
                        cardNo = Util.replaceNullIntent(eB, "cardNo");
                        cardCompany = Util.replaceNullIntent(eB, "cardCompany");
                        cardAmt = Util.isNVL(Util.replaceNullIntent(eB, "cardAmt"), "0");
                    }

                    transactionNo = Preferences.getValue(this, Constants.PREFERENCE_MCPAY_TRANSACTION_NO, "");
                    approvalNo = Preferences.getValue(this, Constants.PREFERENCE_MCPAY_APPROVAL_NO, "");
                    approvalDate = Preferences.getValue(this, Constants.PREFERENCE_MCPAY_APPROVAL_DATE, "");
                    cardNo = Preferences.getValue(this, Constants.PREFERENCE_MCPAY_CARD_NO, "");
                    cardCompany = Preferences.getValue(this, Constants.PREFERENCE_MCPAY_CARD_COMPANY, "");
                    cardAmt = Preferences.getValue(this, Constants.PREFERENCE_MCPAY_TOTAL_AMT, "");

                    if (transactionNo == "") {
                        Log.d("111", "Card error");
                        return;
                    }
                    //prepayDialog.dismiss();
                    cardAmt = tvPaymentAmt.getText().toString().replaceAll("원", "");

                    if (cardNo.length() >= 12) {
                        cardNo = cardNo.substring(0, 4) + cardNo.substring(12);
                    }
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        System.out.println("InterruptedException 예외 발생");
                    }

                    Bundle eB = new Bundle();
                    eB.putString("transactionNo", transactionNo);
                    eB.putString("approvalNo", approvalNo);
                    eB.putString("approvalDate", approvalDate);
                    eB.putString("cardNo", cardNo);
                    eB.putString("cardCompany", cardCompany);
                    eB.putString("cardAmt", cardAmt);
                    eB.putString("pioNum", gReceiptVo.getPioNum());
                    eB.putString("carNum", gReceiptVo.getCarNum());
                    PayData payData = new PayData(this, OUT_PRINT, IN_PRINT, TICKET_PRINT);


                    if (Integer.valueOf(cardAmt) > 0) {
                        payData.printPayment(PayData.PAY_DIALOG_TYPE_EXIT_CARD, gReceiptVo, mPrintService, eB, null);
                        payData.ImagePrint(empLogo_image);
                        try {
                            payData.printText("\n\n");
                        } catch (UnsupportedEncodingException e) {
                            System.out.println("UnsupportedEncodingException 예외 발생");
                        }
                    }


                } catch (NullPointerException e) {
                    if(Constants.DEBUG_PRINT_LOG){
                        e.printStackTrace();
                    }else{
                        System.out.println("예외 발생");
                    }
                }

                InitPreferences();

                // 서버 결제 로그 저장 step2
                String url = Constants.API_SERVER + "?" + Constants.METHOD + "=" + Constants.INTERFACEID_LOG_PARAMETER;
                String param = "&transactionNo=" + transactionNo
                        + "&approvalNo=" + approvalNo
                        + "&approvalDate=" + approvalDate
                        + "&cardNo=" + cardNo
                        + "&cardCompany=" + cardCompany
                        + "&cardAmt=" + cardAmt
                        + "&class=" + this.getClass().getSimpleName()
                        + "&type=pay_step2"
                        + "&" + Constants.API_FORMAT + "=" + Constants.JSON_FORMAT;

                TAG = "111";
                Log.d(TAG, " apiParkLogCall step 2 url >>> " + url + param);

                AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>();
                cb.url(url + param).type(JSONObject.class).weakHandler(this, "").redirect(true).retry(3).fileCache(false).expire(-1);
                AQuery aq = new AQuery(this);
                aq.ajax(cb);

                if (exitDialog != null) {
                    exitDialog.dismiss();
                }
                ExitPaymentData exitVo = new ExitPaymentData();
                exitVo.setTransactionNo(transactionNo);
                exitVo.setApprovalNo(approvalNo);
                exitVo.setApprovalDate(approvalDate);
                exitVo.setCardNo(cardNo);
                exitVo.setCardCompany(cardCompany);
                exitVo.setCardAmt(cardAmt);
                exitVo.setCashAmt("0");
                exitVo.setCatNumber(catNumber);

                request(PayData.PAY_DIALOG_TYPE_EXIT_CARD, exitVo);
                break;
            case PayData.PAY_DIALOG_TYPE_CASH_RECEIPT:
                try {
                    mPrintService.stop();
                    if (mPrintService.getState() == 0) {

                        BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(mBluetoothAddress);
                        mPrintService.connect(device, true);
                    }
                } catch (IllegalArgumentException e) {
                    System.out.println("IllegalArgumentException 예외 발생");
                } catch (NullPointerException e) {
                    System.out.println("NullPointerException 예외 발생");
                }

                approvalNo = Preferences.getValue(this, Constants.PREFERENCE_MCPAY_APPROVAL_NO, "");
                approvalDate = Preferences.getValue(this, Constants.PREFERENCE_MCPAY_APPROVAL_DATE, "");
                String amt_tot = Preferences.getValue(this, Constants.PREFERENCE_MCPAY_TOTAL_AMT, "");

                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    System.out.println("InterruptedException 예외 발생");
                }

                PayVo payVo = new PayVo();  //현금영수증참조
                payVo.setCashApprovalNo(approvalNo);
                payVo.setCashApprovalDate(approvalDate);
                tvCashBill.setTag(payVo);
                tvCashBill.setText("발행");
                InitPreferences();
                cashReceiptRun(Util.sToi(amt_tot), g_vo);
                break;
            case PayData.PAY_DIALOG_TYPE_PREPAY_CARD:
                Log.d(TAG, " requestCode >>> " + requestCode);
                transactionNo = "";
                approvalNo = "";
                approvalDate = "";
                cardNo = "";
                cardAmt = "";
                cardCompany = "";

                try {
                    try {
                        mPrintService.stop();
                        if (mPrintService.getState() == 0) {

                            BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(mBluetoothAddress);
                            mPrintService.connect(device, true);
                        }
                    } catch (IllegalArgumentException e) {
                        System.out.println("IllegalArgumentException 예외 발생");
                    } catch (NullPointerException e) {
                        System.out.println("NullPointerException 예외 발생");
                    }

                    transactionNo = Preferences.getValue(this, Constants.PREFERENCE_MCPAY_TRANSACTION_NO, "");
                    approvalNo = Preferences.getValue(this, Constants.PREFERENCE_MCPAY_APPROVAL_NO, "");
                    approvalDate = Preferences.getValue(this, Constants.PREFERENCE_MCPAY_APPROVAL_DATE, "");
                    cardNo = Preferences.getValue(this, Constants.PREFERENCE_MCPAY_CARD_NO, "");
                    cardCompany = Preferences.getValue(this, Constants.PREFERENCE_MCPAY_CARD_COMPANY, "");
                    cardAmt = Preferences.getValue(this, Constants.PREFERENCE_MCPAY_TOTAL_AMT, "");

                    if (transactionNo.equals("") || cardNo.equals("") || approvalNo.equals("")) {
                        Log.d("111", "Card error");
                        return;
                    }

                    if (cardNo.length() >= 12) {
                        cardNo = cardNo.substring(0, 4) + cardNo.substring(12);
                    }

                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        System.out.println("InterruptedException 예외 발생");
                    }

                    Bundle eB = new Bundle();
                    eB.putString("transactionNo", transactionNo);
                    eB.putString("approvalNo", approvalNo);
                    eB.putString("approvalDate", approvalDate);
                    eB.putString("cardNo", cardNo);
                    eB.putString("cardCompany", cardCompany);
                    eB.putString("cardAmt", cardAmt);
                    eB.putString("pioNum", gReceiptVo.getPioNum());
                    eB.putString("carNum", gReceiptVo.getCarNum());
                    PayData payData = new PayData(this, OUT_PRINT, IN_PRINT, TICKET_PRINT);
                    payData.printPayment(PayData.PAY_DIALOG_TYPE_PREPAY_CARD, gReceiptVo, mPrintService, eB, null);
                    payData.ImagePrint(empLogo_image);
                    try {
                        payData.printText("\n\n");
                    } catch (UnsupportedEncodingException e) {
                        System.out.println("예외 발생");
                    }


                } catch (NullPointerException e) {
                    if(Constants.DEBUG_PRINT_LOG){
                        e.printStackTrace();
                    }else{
                        System.out.println("예외 발생");
                    }
                }

                InitPreferences();

                // 서버 결제 로그 저장 step2
                String url2 = Constants.API_SERVER + "?" + Constants.METHOD + "=" + Constants.INTERFACEID_LOG_PARAMETER;
                String param2 = "&transactionNo=" + transactionNo
                        + "&approvalNo=" + approvalNo
                        + "&approvalDate=" + approvalDate
                        + "&cardNo=" + cardNo
                        + "&cardCompany=" + cardCompany
                        + "&cardAmt=" + cardAmt
                        + "&class=" + this.getClass().getSimpleName()
                        + "&type=pay_step2"
                        + "&" + Constants.API_FORMAT + "=" + Constants.JSON_FORMAT;

                TAG = "111";
                Log.d(TAG, " apiParkLogCall step 2 url >>> " + url2 + param2);

                AjaxCallback<JSONObject> cb2 = new AjaxCallback<JSONObject>();
                cb2.url(url2 + param2).type(JSONObject.class).weakHandler(this, "").redirect(true).retry(3).fileCache(false).expire(-1);
                AQuery aq2 = new AQuery(this);
                aq2.ajax(cb2);

                //prepayDialog.dismiss();
                PayVo payVo2 = new PayVo();
                payVo2.setTransactionNo(transactionNo);
                payVo2.setApprovalNo(approvalNo);
                payVo2.setApprovalDate(approvalDate);
                payVo2.setCardNo(cardNo);
                payVo2.setCardCompany(cardCompany);
                payVo2.setCardAmt(cardAmt);
                payVo2.setCashAmt("0");
                payVo2.setCatNumber(catNumber);
                request(PayData.PAY_DIALOG_TYPE_PREPAY_CARD, payVo2);
                break;

        }
    }

    private void exitPark() {
        int charge = Integer.parseInt(tvPaymentAmt.getText().toString().replace(",", "").replace("원", ""));
        type4Vo = new ReceiptType4Vo(Util.getYmdhms("yyyy-MM-dd HH:mm:ss")    // 출력일자
                , tvParkType.getText().toString()            // 주차종류 :
                , tvEntrance.getText().toString()            // 주차시간 :
                , btnParkPosition.getText().toString()        // 주차면 :
                , tvChargeDiscount.getText().toString()        // 할인종류 :
                , (parkIO.getParkInDay().substring(8, 10) + ":" + parkIO.getParkInDay().substring(10, 12) + ":" + parkIO.getParkInDay().substring(12))        // 입차시간 :
                , exitTime        // 출차시간 :
                , "" + charge        // 납부금액 :
                , tvPrePayment.getText().toString().replace(",", "").replace("원", "")        // 선납금액 :
                , tvParkPayment.getText().toString().replace(",", "").replace("원", "")        // 주차요금 :
                , tvUnPayment.getText().toString().replace(",", "").replace("원", "")        // 미납금액 :
                , tvDiscountAmt.getText().toString().replace(",", "").replace("원", "")        // 할인금액 :
                , BIZ_NO                    // 사업자번호
                , BIZ_NAME
                , BIZ_TEL);

        type4Vo.setPioNum(parkIO.getPioNum());
        type4Vo.setCarNum(carNo);
        type4Vo.setParkName(parkName);
        type4Vo.setEmpName(empName);
        type4Vo.setEmpPhone(empPhone);
        type4Vo.setEmpTel(empTel);
        type4Vo.setEmpBusiness_Tel(empBusiness_TEL);
        type4Vo.setOutPrint(Preferences.getValue(this, Constants.PREFERENCE_OUT_PRINT, ""));

        exitDialog(tvParkType.getText().toString()
                , tvPaymentAmt.getText().toString()
                , type4Vo, tvUnPayment.getText().toString().replaceAll("원", ""));
    }

    @Override
    public void getSelectListDialogData(int tag, String data) {
        super.getSelectListDialogData(tag, data);
        Log.d(TAG, "getSelectListDialogData() >>>>>> : " + tag);
        switch (tag) {
            case DIALOG_PARK_BOARD:
                btnParkPosition.setText(data + "면");
                break;
            case Constants.COMMON_TYPE_CHARGE_DISCOUNT:
                setParkAmt(false);
                break;

            case Constants.COMMON_TYPE_CHARGE_TYPE:
                Log.i(TAG, "dis : " + data);
                setParkAmt(false);
                break;
            //case Constants.COMMON_TYPE_PARK_TYPE:
            //	Log.i(TAG, "dis : " + data);
            //	changedParktype(data);
            //	break;
            case Constants.COMMON_TYPE_PARK_FEE_CANCEL:
                tvPrePayment.setText("0원");
                //tvPaymentAmt.setText(tvParkPayment.getText().toString());
                //exitPark();
                apiParkIoListCall_Refresh();
                break;
        }
    }
	
	/*
	private void changedParktype(String _data) {
		if(("일일권").equals(_data)) {
			setParkAmt(false);
		} else {

			if(isSelbuttonStatus)
			{
				type13Vo = new ReceiptType13Vo("입차영수증"		// 타이틀
						, Util.getYmdhms("yyyy-MM-dd HH:mm:ss")	// 출력일자
						, btnParkPosition.getText().toString().replaceAll("면", "")			// 주차면
						, ""				// 주차종류
						, ""				// 요금종류
						, ""				// 요금할인
						, tvUnPayment.getText().toString().replaceAll("원", "")	// 총미수금액
						, ""	// 선납금액
						, ""	// 시작시간
						, ""	// 출차예정시간
						, ""	// 총 분
						, BIZ_NO			// 사업자번호
						, BIZ_NAME			// 사업자명
						, BIZ_TEL);			// 사업자연락처
				
				type13Vo.setPioNum(parkIO.getPioNum());
				type13Vo.setCarNum(carNo);
				type13Vo.setApprovalNo(parkIO.getApprovalNo());
				type13Vo.setParkName(parkName);
				type13Vo.setEmpName(empName);
				type13Vo.setEmpPhone(empPhone);
				type13Vo.setEmpTel(empTel);				
				
			 	parkTypeChangeDialog( tvChargeType.getText().toString(), tvChargeDiscount.getText().toString(), tvParkType.getText().toString()
			 						, tvParkInTime.getText().toString(), tvParkOutTime.getText().toString(), type13Vo );
			}
			
			
					
		}
	
	}
	*/


    private void apiParkIoListCall_Refresh() {

        String url = Constants.API_SERVER + "?" + Constants.METHOD + "=" + Constants.INTERFACEID_PARK_IO;
        String param = "&CD_PARK=" + parkCode
                + "&CD_STATE=I1"
                + "&CAR_NO=" + carNo
                + "&START_DAY=" + today
                + "&END_DAY=" + today
                + "&" + Constants.API_FORMAT + "=" + Constants.JSON_FORMAT;

        Log.d("333", " apiParkIoListCall_Refresh url >>> " + url + param);
        AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>();
        cb.url(url + param).type(JSONObject.class).weakHandler(this, "apiParkIoListCall_Refresh").redirect(true).retry(3).fileCache(false).expire(-1);
        AQuery aq = new AQuery(this);
        aq.ajax(cb);
    }

    public void apiParkIoListCall_Refresh(String url, JSONObject json, AjaxStatus status) {
        //closeProgressDialog();
        JSONArray jsonArr = null;
        ParkIO info;
        // successful ajax call
        if (json != null) {
            Log.d("333", " apiParkIoListCall_Refresh json >>> " + json.toString());
            if ("1".equals(json.optString("CD_RESULT"))) {
                jsonArr = json.optJSONArray("RESULT");

                int Len = jsonArr.length();

                try {
                    for (int i = 0; i < Len; i++) {
                        JSONObject res = jsonArr.getJSONObject(i);
                        info = new ParkIO(res.optString("PIO_NUM")
                                , res.optString("CD_PARK")
                                , res.optString("PIO_DAY")
                                , res.optString("CAR_NO")
                                , res.optString("CD_AREA")
                                , res.optString("PARK_TYPE")
                                , res.optString("DIS_CD")
                                , res.optString("TEL")
                                , res.optString("PARK_IN_AMT")
                                , res.optString("PARK_OUT_AMT")
                                , res.optString("PARK_AMT")
                                , res.optString("ADV_IN_AMT")
                                , res.optString("ADV_OUT_AMT")
                                , res.optString("ADV_AMT")
                                , res.optString("DIS_IN_AMT")
                                , res.optString("DIS_OUT_AMT")
                                , res.optString("DIS_AMT")
                                , res.optString("CASH_IN_AMT")
                                , res.optString("CASH_OUT_AMT")
                                , res.optString("CASH_AMT")
                                , res.optString("CARD_IN_AMT")
                                , res.optString("CARD_OUT_AMT")
                                , res.optString("CARD_AMT")
                                , res.optString("TRUNC_IN_AMT")
                                , res.optString("TRUNC_OUT_AMT")
                                , res.optString("TRUNC_AMT")
                                , res.optString("COUPON_AMT")
                                , res.optString("RETURN_AMT")
                                , res.optString("YET_AMT")
                                , res.optString("REAL_AMT")
                                , res.optString("PARK_TIME")
                                , res.optString("PARK_IN")
                                , res.optString("PARK_OUT")
                                , res.optString("CARD_NO")
                                , res.optString("CARD_COMPANY")
                                , res.optString("APPROVAL_NO")
                                , res.optString("APPROVAL_DATE")
                                , res.optString("CHARGE_TYPE")
                                , res.optString("KN_COMMENT"));

                        parkIO = info;
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    Len = 0;
                }

                if (Len == 1) {

                    //todo 홍규혁 할인금액 예외처리
                    parkIO.setParkAmt(tvParkPayment.getText().toString().replaceAll("원", "").toString());
                    setParkAmt(false);
                    //tvPayment.settvParkPayment
//				  tvPaymentAmt.setText(tvParkPayment.getText().toString());
                    exitPark();
                }


            }
        } else {
            Toast.makeText(this, Constants.DATA_FAIL, Toast.LENGTH_LONG).show();
        }

    }

    int getExpectParkAmt(String date2) {
        // 할인금액 초기화
        discountAmt = "0";

        // 절삭금액 초기화
        truncAmt = 0;
        String parkAmt = parkIO.getParkInAmt();
        String date1 = parkIO.getParkInDay();
        //String date2 = tvParkOutTime.getText().toString().replaceAll(" ", "").replaceAll("/", "").replaceAll("-", "").replaceAll(":", "").trim();
        String retday = "";
		/*
		if(isAddDis){ //조정버튼 클릭 시 5분 차감
			retday =Util.addMinusDate(tvParkOutTime.getText().toString()+":00", -5, 0).substring(0, 16);
			tvParkOutTime.setText(retday);
			date2 = retday.replaceAll(" ", "").replaceAll("/", "").replaceAll("-", "").replaceAll(":", "").trim();
		}
		*/
        if (date2.length() <= 12) {
            date2 = date2 + "00";
        }


        //###############################################################
        //  STEP 1 : 주차 시간 계산
        //###############################################################
        Log.d(TAG, "date1 : " + date1 + ", date2 : " + date2);
        String parkMin = minuteCalc(date1, date2);
        Log.d(TAG, " parkMin >>> " + parkMin);


        //###############################################################
        //  STEP 2 : 할인 시간 계산
        //###############################################################
        // 요금할인 정보
        String discountName = tvChargeDiscount.getText().toString();

        // 할인시간 설정
        int disTime = Util.sToi(cch.findCodeByDisTime("DC", discountName), 0);
        if (disTime > 0) {
            parkMin = "" + (Util.sToi(parkMin, 0) - disTime);
            if ((Util.sToi(parkMin, 0) < 0)) {
                parkMin = "0";
            }
        }
        // 주차시간 표시
        //tvEntrance.setText(minuteView(parkMin));


        //###############################################################
        //  STEP 3 : 할인 금액 계산
        //###############################################################

        // 주차종류
        //String org = tvParkPayment.getText().toString().replace("원", "");
        String parkTypeCode = cch.findCodeByKnCode("PY", tvParkType.getText().toString());

        // 요금종류코드
        String chargeCode = "";

        if ("2".equals(parkClass)) {
            chargeCode = cch.findCodeByKnCode("AM2", tvChargeType.getText().toString());
        } else if ("3".equals(parkClass)) {
            chargeCode = cch.findCodeByKnCode("AM3", tvChargeType.getText().toString());
        } else if ("4".equals(parkClass)) {
            chargeCode = cch.findCodeByKnCode("AM4", tvChargeType.getText().toString());
        } else if ("5".equals(parkClass)) {
            chargeCode = cch.findCodeByKnCode("AM5", tvChargeType.getText().toString());
        } else if ("6".equals(parkClass)) {
            chargeCode = cch.findCodeByKnCode("AM6", tvChargeType.getText().toString());
        }

        //chargeCode = cch.findCodeByKnCode("AM2", tvChargeType.getText().toString());

        // 주차금액 계산
        String total = parkCharge(parkMin, chargeCode, parkTypeCode);
        String charge = "0";

        if ("02".equals(parkTypeCode)) {  // 일일권
            total = "0";  // 일일권은 출차시 0원
            charge = "0";
            discountAmt = "0";  // 일일권은 할인을 하지 않는다.
            truncAmt = 0;
            // 과금 된 실제 주차금액 표시
            //tvParkPayment.setText(total + "원");
            // 할인금액 표시
            //tvDiscountAmt.setText(discountAmt + "원");
        } else if ("03".equals(parkTypeCode)) { // 정기권
            total = "0";  // 정기권은 출차시 0원
            charge = "0";
            discountAmt = "0";  // 정기권은 할인을 하지 않는다.
            truncAmt = 0;
            // 과금 된 실제 주차금액 표시
            //tvParkPayment.setText(total + "원");
            // 할인금액 표시
            //tvDiscountAmt.setText(discountAmt + "원");
        } else {

            // 과금 된 실제 주차금액 표시
            tvParkPayment.setText(total + "원");
            Log.d(TAG, " parkIO.getAdvInAmt() >>> " + parkIO.getAdvInAmt());
            Log.d(TAG, " parkIO.getDisInAmt() >>> " + parkIO.getDisInAmt());
            Log.d(TAG, " parkIO.getTruncInAmt() >>> " + parkIO.getTruncInAmt());

            if ("04".equals(parkTypeCode)) {  // 시간권 선불 : 총 주차요금 = 입차 시 선납금액 - 입차 시 할인금액  - 입차 시 절삭금액 차감
                Log.d(TAG, " total as is >>> " + total);


                total = "" + (Util.sToi(total) - Util.sToi(parkIO.getAdvInAmt()) - Util.sToi(parkIO.getDisInAmt()) - Util.sToi(parkIO.getTruncInAmt()));
                if (Util.sToi(parkIO.getAdvInAmt()) == 0) {
                    total = "0";
                }
                Log.d(TAG, " total to be >>> " + total);
            }

            // 주차금액 셋팅
            charge = total;

            if (Util.sToi(total) >= 0) {

                int tempDisAmt1 = 0;
                int tempDisAmt2 = 0;

                // 할인금액 조회
                String disAmt = Util.isNVL(cch.findCodeByDisAmt("DC", discountName), "0");
                if (!"0".equals(disAmt)) {
                    // 할인금액 설정
                    tempDisAmt1 = Util.sToi(disAmt);
                    charge = "" + (Util.sToi(charge) - tempDisAmt1);
                    Log.d(TAG, "setParkAmt ::: charge >>> " + charge + "  tempDisAmt1 : " + tempDisAmt1);
                }

                // 추가 민원 할인 금액이 발생하면...
                //if(isAddDis){
                //	charge = "" + (Util.sToi(charge) - 700);
                //}

                // 할인률 조회
                String disRate = cch.findCodeByDisRate("DC", discountName);
                if (!"0".equals(disRate)) {
                    // 할인률에 따른 할인금액 설정
                    tempDisAmt2 = Util.sToi(Util.isNVL("" + discountChargeRate(charge, disRate), "0"));
                    charge = "" + (Util.sToi(charge) - tempDisAmt2);
                    Log.d(TAG, "setParkAmt ::: charge >>> " + charge + "  tempDisAmt2 : " + tempDisAmt2);
                }

                // 총 할인금액
                discountAmt = "" + (tempDisAmt1 + tempDisAmt2);
                Log.d(TAG, " setParkAmt >>> charge : " + charge + " discountAmt : " + discountAmt);

                // 할인금액 표시
                if ("04".equals(parkTypeCode)) {  // 시간권 선불
                    //tvDiscountAmt.setText((Util.sToi(parkIO.getDisInAmt()) + Util.sToi(discountAmt)) + "원");
                    // 할인금액 = 할인금액 - 입차 시 절삭금액
                    discountAmt = "" + (Util.sToi(discountAmt) - Util.sToi(parkIO.getTruncInAmt()));
                    // 주차금액 = 주차금액 + 입차 시 절삭금액
                    charge = "" + (Util.sToi(charge) + Util.sToi(parkIO.getTruncInAmt()));
                } else {
                    //tvDiscountAmt.setText(discountAmt + "원");
                }

                if (Util.sToi(charge, 0) < 0) {
                    charge = "0";
                }

                //홍규혁 할인금액 재설정
//                tvParkPayment.setText(charge + "원");

            } else { // 환불 발생 건

                // 할인률 조회
                String disRate = cch.findCodeByDisRate("DC", discountName);
                String disAmt = "0";
                String total2 = "0";
                // 할인률 100% 적용이면
                if ("100".equals(disRate)) {
                    charge = "0";
                    discountAmt = total;
                } else {
				/*	
				  if(!"0".equals(disRate)){
					// 할인률에 따른 할인금액 설정
					discountAmt = Util.isNVL(""+discountChargeRate(total, disRate),"0");
				  }
				  
				  Log.d(TAG, " setParkAmt >>> return total  : " + total  + " discountAmt : " + discountAmt);
				  charge = Util.toStr((Util.sToi(total) - Util.sToi(discountAmt)));
				  Log.d(TAG, " setParkAmt >>> return charge : " + charge + " discountAmt : " + discountAmt);
				*/
                }

                // 할인금액 표시 - 환불은 선불권에서만 발생으로  입차 시 할인금액 합산(차감)
                //tvDiscountAmt.setText((Util.sToi(parkIO.getDisInAmt()) + Util.sToi(discountAmt)) + "원");

            }  // end if total
        } // end if parkTypeCode

        Log.d(TAG, " total  <<<>>> " + total);
        Log.d(TAG, " charge <<<>>> " + charge);
        Log.d(TAG, " discountAmt  <<<>>> " + discountAmt);

        // 주차원금 셋팅
        tvParkPayment.setTag(total);


        //###############################################################
        //  STEP 4 : 납부 금액 계산
        //###############################################################

        int payment = 0;

        // 일일권 또는 정기권일 경우 주차요금 및 납부금액은 계산하지 않는다.
        if ("02".equals(parkTypeCode) || "03".equals(parkTypeCode)) {
            //tvParkPayment.setText("0원");
            //tvDiscountAmt.setText("0원");
            //tvPaymentAmt.setText("0원");
            //tvPaymentAmtMinus.setText("0원");
        } else { // 시간권 후불 또는 선불
            // 납부금액 = 주차요금
            payment = Util.sToi(charge);
        }

        //###############################################################
        //  STEP 6 : 조정버튼 클릭 시에만
        //###############################################################
		/*
		if(adj_flag)  
		{
			if(isAddDis) {
				if(Util.sToi(org) > Util.sToi(total)) {
					ADJ_AMT = Util.sToi(org) - Util.sToi(total);
				} else {
					ADJ_AMT = 0;
				}
			} else {
				ADJ_AMT = 0;
			}

		}			
		*/
        //###############################################################
        //  STEP 5 : 절삭 금액 계산
        //###############################################################

        if (!"0".equals(truncUnit)) {
            truncAmt = payment % Util.sToi(truncUnit);
        } else {
            truncAmt = 0;
        }

        payment = payment - truncAmt;

        // 절삭금액은 +금액으로 표시(미환불 금액)
        truncAmt = Math.abs(truncAmt);
		
		/*
		// 절삭금액 표시
		// 총 금액 = 표시 된 주차요금 - 표시 된 할인금액 - 표시 된 선납금액 		
		int tvTotalAmt = Util.sToi(tvParkPayment.getText().toString().replaceAll("원", "")) 
				       - Util.sToi(tvDiscountAmt.getText().toString().replaceAll("원", ""))
				       - Util.sToi(tvPrePayment.getText().toString().replaceAll("원", ""));
		int tvTruncAmt = 0;
		if (!"0".equals(truncUnit)) {
			tvTruncAmt = tvTotalAmt % Util.sToi(truncUnit);
		} else {
			tvTruncAmt = 0;
		}
		tvPaymentAmtMinus.setText(tvTruncAmt + "원");
		tvPaymentAmt.setText(payment + "원");
		
		*/
        //tvPaymentAmtMinus.setText(truncAmt + "원");
        //tvPaymentAmt.setText(payment + "원");


        if (payment == 0) {
            btnUnPay.setEnabled(false);
        } else {
            btnUnPay.setEnabled(true);
        }
        if (Util.sToi(tvPrePayment.getText().toString().replaceAll("원", "").trim()) > 0) {
            if (Util.sToi(tvPaymentAmt.getText().toString().replaceAll("원", "").trim()) < 0) {
                btnUnPay.setEnabled(false);
            }
        }
        return payment;

    }


    void setParkAmt(boolean adj_flag) {

        // 할인금액 초기화
        discountAmt = "0";

        // 절삭금액 초기화
        truncAmt = 0;

        String date1 = parkIO.getParkInDay();
        String date2 = tvParkOutTime.getText().toString().replaceAll(" ", "").replaceAll("/", "").replaceAll("-", "").replaceAll(":", "").trim();
        String retday = "";

        if (isAddDis) { //조정버튼 클릭 시 5분 차감
            retday = Util.addMinusDate(tvParkOutTime.getText().toString() + ":00", -5, 0).substring(0, 16);
            tvParkOutTime.setText(retday);
            date2 = retday.replaceAll(" ", "").replaceAll("/", "").replaceAll("-", "").replaceAll(":", "").trim();
        }

        if (date2.length() <= 12) {
            date2 = date2 + "00";
        }


        //###############################################################
        //  STEP 1 : 주차 시간 계산
        //###############################################################
        Log.d(TAG, "date1 : " + date1 + ", date2 : " + date2);
        String parkMin = minuteCalc(date1, date2);
        Log.d(TAG, " parkMin >>> " + parkMin);


        //###############################################################
        //  STEP 2 : 할인 시간 계산
        //###############################################################
        // 요금할인 정보
        String discountName = tvChargeDiscount.getText().toString();

        // 할인시간 설정
        int disTime = Util.sToi(cch.findCodeByDisTime("DC", discountName), 0);
        if (disTime > 0) {
//			Log.d(TAG, "parkMin 2 : " + parkMin);
            parkMin = "" + (Util.sToi(parkMin, 0) - disTime);
//			Log.d(TAG, "parkMin 3 : " + parkMin);
            if ((Util.sToi(parkMin, 0) < 0)) {
//				Log.d(TAG, "parkMin 4 : " + parkMin);
                parkMin = "0";
            }
        }
        // 주차시간 표시
        tvEntrance.setText(minuteView(parkMin));


        //###############################################################
        //  STEP 3 : 할인 금액 계산
        //###############################################################

        // 주차종류
        String org = tvParkPayment.getText().toString().replace("원", "");
        String parkTypeCode = cch.findCodeByKnCode("PY", tvParkType.getText().toString());

        // 요금종류코드
        String chargeCode = "";

        if ("2".equals(parkClass)) {
            chargeCode = cch.findCodeByKnCode("AM2", tvChargeType.getText().toString());
        } else if ("3".equals(parkClass)) {
            chargeCode = cch.findCodeByKnCode("AM3", tvChargeType.getText().toString());
        } else if ("4".equals(parkClass)) {
            chargeCode = cch.findCodeByKnCode("AM4", tvChargeType.getText().toString());
        } else if ("5".equals(parkClass)) {
            chargeCode = cch.findCodeByKnCode("AM5", tvChargeType.getText().toString());
        } else if ("6".equals(parkClass)) {
            chargeCode = cch.findCodeByKnCode("AM6", tvChargeType.getText().toString());
        }


        // 주차금액 계산
        String total = parkCharge(parkMin, chargeCode, parkTypeCode);
        String charge = "0";

        if ("02".equals(parkTypeCode)) {  // 일일권
            total = "0";  // 일일권은 출차시 0원
            charge = "0";
            discountAmt = "0";  // 일일권은 할인을 하지 않는다.
            truncAmt = 0;
            // 과금 된 실제 주차금액 표시
            tvParkPayment.setText(total + "원");
            // 할인금액 표시
            tvDiscountAmt.setText(discountAmt + "원");
        } else if ("03".equals(parkTypeCode)) { // 정기권
            total = "0";  // 정기권은 출차시 0원
            charge = "0";
            discountAmt = "0";  // 정기권은 할인을 하지 않는다.
            truncAmt = 0;
            // 과금 된 실제 주차금액 표시
            tvParkPayment.setText(total + "원");
            // 할인금액 표시
            tvDiscountAmt.setText(discountAmt + "원");
        } else {

            // 과금 된 실제 주차금액 표시
            tvParkPayment.setText(total + "원");

            if ("04".equals(parkTypeCode)) {  // 시간권 선불 : 총 주차요금 = 입차 시 선납금액 - 입차 시 할인금액  - 입차 시 절삭금액 차감
                Log.d(TAG, " total as is >>> " + total);
                total = "" + (Util.sToi(total) - Util.sToi(parkIO.getAdvInAmt()) - Util.sToi(parkIO.getDisInAmt()) - Util.sToi(parkIO.getTruncInAmt()));
                Log.d(TAG, " total to be >>> " + total);
            }

            // 주차금액 셋팅
            charge = total;

            if (Util.sToi(total) > 0) {

                int tempDisAmt1 = 0;
                int tempDisAmt2 = 0;

                // 할인금액 조회
                String disAmt = Util.isNVL(cch.findCodeByDisAmt("DC", discountName), "0");
                if (!"0".equals(disAmt)) {
                    // 할인금액 설정
                    tempDisAmt1 = Util.sToi(disAmt);
                    charge = "" + (Util.sToi(charge) - tempDisAmt1);
                    Log.d(TAG, "setParkAmt ::: charge >>> " + charge + "  tempDisAmt1 : " + tempDisAmt1);
                }

                // 추가 민원 할인 금액이 발생하면...
                //if(isAddDis){
                //	charge = "" + (Util.sToi(charge) - 700);
                //}

                // 할인률 조회
                String disRate = cch.findCodeByDisRate("DC", discountName);
                if (!"0".equals(disRate)) {
                    // 할인률에 따른 할인금액 설정
                    tempDisAmt2 = Util.sToi(Util.isNVL("" + discountChargeRate(charge, disRate), "0"));
                    charge = "" + (Util.sToi(charge) - tempDisAmt2);
                    Log.d(TAG, "setParkAmt ::: charge >>> " + charge + "  tempDisAmt2 : " + tempDisAmt2);
                }

                // 총 할인금액
                discountAmt = "" + (tempDisAmt1 + tempDisAmt2);
                Log.d(TAG, " setParkAmt >>> charge : " + charge + " discountAmt : " + discountAmt);

                // 할인금액 표시
                if ("04".equals(parkTypeCode)) {  // 시간권 선불
                    tvDiscountAmt.setText((Util.sToi(parkIO.getDisInAmt()) + Util.sToi(discountAmt)) + "원");
                    // 할인금액 = 할인금액 - 입차 시 절삭금액
                    discountAmt = "" + (Util.sToi(discountAmt) - Util.sToi(parkIO.getTruncInAmt()));
                    // 주차금액 = 주차금액 + 입차 시 절삭금액
                    charge = "" + (Util.sToi(charge) + Util.sToi(parkIO.getTruncInAmt()));
                } else {
                    tvDiscountAmt.setText(discountAmt + "원");
                }

                if (Util.sToi(charge, 0) < 0) {
                    charge = "0";
                }

            } else { // 환불 발생 건

                // 할인률 조회
                String disRate = cch.findCodeByDisRate("DC", discountName);

                // 할인률 100% 적용이면
                if ("100".equals(disRate)) {
                    charge = "0";
                    discountAmt = total;
                } else {
                    if (!"0".equals(disRate)) {
                        // 할인률에 따른 할인금액 설정
                        discountAmt = Util.isNVL("" + discountChargeRate(total, disRate), "0");
                    }

                    Log.d(TAG, " setParkAmt >>> return total  : " + total + " discountAmt : " + discountAmt);
                    charge = Util.toStr((Util.sToi(total) - Util.sToi(discountAmt)));
                    Log.d(TAG, " setParkAmt >>> return charge : " + charge + " discountAmt : " + discountAmt);
                }

                // 할인금액 표시 - 환불은 선불권에서만 발생으로  입차 시 할인금액 합산(차감)
                tvDiscountAmt.setText((Util.sToi(parkIO.getDisInAmt()) + Util.sToi(discountAmt)) + "원");

            }  // end if total


        } // end if parkTypeCode

        Log.d(TAG, " total  <<<>>> " + total);
        Log.d(TAG, " charge <<<>>> " + charge);
        Log.d(TAG, " discountAmt  <<<>>> " + discountAmt);

        // 주차원금 셋팅
        tvParkPayment.setTag(total);


        //###############################################################
        //  STEP 4 : 납부 금액 계산
        //###############################################################

        int payment = 0;

        // 일일권 또는 정기권일 경우 주차요금 및 납부금액은 계산하지 않는다.
        if ("02".equals(parkTypeCode) || "03".equals(parkTypeCode)) {
            tvParkPayment.setText("0원");
            tvDiscountAmt.setText("0원");
            tvPaymentAmt.setText("0원");
            tvPaymentAmtMinus.setText("0원");
        } else { // 시간권 후불 또는 선불
            // 납부금액 = 주차요금
            payment = Util.sToi(charge);
        }

        //###############################################################
        //  STEP 6 : 조정버튼 클릭 시에만
        //###############################################################
        if (adj_flag) {
            if (isAddDis) {
                if (Util.sToi(org) > Util.sToi(total)) {
                    ADJ_AMT = Util.sToi(org) - Util.sToi(total);
                } else {
                    ADJ_AMT = 0;
                }
            } else {
                ADJ_AMT = 0;
            }

        }

        //###############################################################
        //  STEP 5 : 절삭 금액 계산
        //###############################################################

        if (!"0".equals(truncUnit)) {
            truncAmt = payment % Util.sToi(truncUnit);
        } else {
            truncAmt = 0;
        }

        payment = payment - truncAmt;

        // 절삭금액은 +금액으로 표시(미환불 금액)
        truncAmt = Math.abs(truncAmt);


        // 절삭금액 표시
        // 총 금액 = 표시 된 주차요금 - 표시 된 할인금액 - 표시 된 선납금액
        int tvTotalAmt = Util.sToi(tvParkPayment.getText().toString().replaceAll("원", ""))
                - Util.sToi(tvDiscountAmt.getText().toString().replaceAll("원", ""))
                - Util.sToi(tvPrePayment.getText().toString().replaceAll("원", ""));
        int tvTruncAmt = 0;
        if (!"0".equals(truncUnit)) {
            tvTruncAmt = tvTotalAmt % Util.sToi(truncUnit);
        } else {
            tvTruncAmt = 0;
        }
        tvPaymentAmtMinus.setText(tvTruncAmt + "원");
        tvPaymentAmt.setText(payment + "원");


        //tvPaymentAmtMinus.setText(truncAmt + "원");
        //tvPaymentAmt.setText(payment + "원");


        if (payment == 0) {
            btnUnPay.setEnabled(false);
        } else {
            btnUnPay.setEnabled(true);
        }

    }

    public Dialog exitUnpayDialog(int yyear, int mon, int dday) {
        final Dialog exitUnpayDialog;

        cch = CodeHelper.getInstance(this);

        final TextView tvUnpayReason;

        exitUnpayDialog = new Dialog(this, android.R.style.Theme_Holo_Light_Dialog);

        exitUnpayDialog.setContentView(R.layout.exit_unpay_dialog);
        exitUnpayDialog.setTitle("미수출차");

        final Button bts_impdate = (Button) exitUnpayDialog.findViewById(R.id.btn_impdate);
        bts_impdate.setText(yyear + Util.addZero(mon) + Util.addZero(dday));

        ((Button) exitUnpayDialog.findViewById(R.id.btn_impdate)).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                new calendarDlg(exitUnpayDialog.getContext(), bts_impdate).show();
            }
        });

        tvUnpayReason = (TextView) exitUnpayDialog.findViewById(R.id.unpay_reason);
        Switch sw_receip = exitUnpayDialog.findViewById(R.id.sw_receip);
        Preferences.putValue(ExitCommitActivity.this, Constants.PREFERENCE_PRINT_UNPAY_ENABLE, false);
        sw_receip.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Log.e("isChecked", isChecked + "");
                if(isChecked){
                    MsgUtil.ToastMessage(ExitCommitActivity.this, "영수증이 출력됩니다.");
                }else{
                    MsgUtil.ToastMessage(ExitCommitActivity.this, "영수증이 출력되지 않습니다.");
                }
                Preferences.putValue(ExitCommitActivity.this, Constants.PREFERENCE_PRINT_UNPAY_ENABLE, isChecked);
            }
        });


        final EditText unpay_name = (EditText) exitUnpayDialog.findViewById(R.id.unpay_name);
        final EditText unpay_phoneNumber = (EditText) exitUnpayDialog.findViewById(R.id.unpay_phoneNumber);
        final Button btn_bank = (Button) exitUnpayDialog.findViewById(R.id.btn_bank);
        unpay_phoneNumber.setText(parkIO.getTel());
        setCommonItem("P3", Constants.COMMON_TYPE_UNPAID_TYPE, tvUnpayReason, 0);
//        setCommonItem("BA", Constants.COMMON_TYPE_UNPAID_TYPE, btn_bank, "11");
        tvUnpayReason.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // 미수사유 선택시 팝업 미수종류 팝업 발생하도록
                setCommonDialog("미수유형", "P3", Constants.COMMON_TYPE_UNPAID_TYPE, tvUnpayReason);
            }
        });
        btn_bank.setText("농협");
//        btn_bank.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                // 미수사유 선택시 팝업 미수종류 팝업 발생하도록
//                setCommonDialog("은행종류", "BA", Constants.COMMON_TYPE_UNPAID_TYPE, btn_bank);
//            }
//        });

        ((EditText) exitUnpayDialog.findViewById(R.id.unpay_phoneNumber)).setText(parkIO.getTel());
        ((Button) exitUnpayDialog.findViewById(R.id.btn_ok)).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if ("".equals(tvUnpayReason.getText().toString())) {
                    Toast.makeText(ExitCommitActivity.this, "미수유형을 선택해 주세요", Toast.LENGTH_LONG).show();
                    return;
                }

                if (out == null) {
                    initParkOut(parkIO.getPioNum(), parkIO.getCdPark(), exitTime, carNo
                            , parkIO.getParkType(), parkIO.getTel(), tvPrePayment.getText().toString().replaceAll("원", "").trim()
                            , parkIO.getDisCd(), ""
                            , tvChargeType.getText().toString());
                }

                //String reason = ((EditText)exitUnpayDialog.findViewById(R.id.etc_reason)).getText().toString();

                //out.setComment(reason);
                //out.setOutTime(Util.getYmdhms("HH:mm:ss"));
                out.setComment("");
                out.setOutTime(exitTime);
                out.setYetAmt(tvPaymentAmt.getText().toString().replaceAll("원", ""));
                out.setDicCode(cch.findCodeByKnCode("DC", tvChargeDiscount.getText().toString()));

                // 미수 코드 셋팅
                out.setStateCode("O2");

                // 미수 상세 코드 셋팅
                out.setYetCode(cch.findCodeByKnCode("P3", tvUnpayReason.getText().toString()));

                out.setCarOwner(unpay_name.getText().toString());
                out.setTel(unpay_phoneNumber.getText().toString());

                if (exitUnpayDialog != null)
                    exitUnpayDialog.dismiss();

                // 미수 출차 플래그
                isUnpayExit = true;
//                apiParkOutCall(null, tvParkType.getText().toString(), btnParkPosition.getText().toString().replaceAll("면", "")
//                        , tvChargeDiscount.getText().toString(), tvParkPayment.getTag().toString(), tvPaymentAmt.getText().toString().replaceAll("원", "").toString(), discountAmt, "" + truncAmt, ADJ_AMT);



                ProcessVcnt(btn_bank.getText().toString(), bts_impdate.getText().toString());
            }
        });

        ((Button) exitUnpayDialog.findViewById(R.id.btn_cancel)).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (exitUnpayDialog != null)
                    exitUnpayDialog.dismiss();
            }
        });

        exitUnpayDialog.setOnCancelListener(new OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
            }
        });

        exitUnpayDialog.show();
        return exitUnpayDialog;
    }

    public void ProcessVcnt(String btnbank, String impdate) {
        String date = Util.getYmdhms("yyyy-MM-dd");
        int m_Year = Integer.parseInt(date.split("-")[0]);
        int m_Month = Integer.parseInt(date.split("-")[1]);
        int m_Day = Integer.parseInt(date.split("-")[2]);

        SimpleDateFormat formatter = new SimpleDateFormat("HHmmss", Locale.KOREA);
        Date currentTime = new Date();
        String dTime = formatter.format(currentTime);
        String time = dTime.substring(0, 2) + "" + dTime.substring(2, 4) + "" + dTime.substring(4, 6);
        Random rand = new Random();
        rand.setSeed(new Date().getTime());
        int min = 10000000, max = 99999999;
        int randomNum = rand.nextInt(max - min + 1) + min;

        gOrdr_idxx = m_Year + Util.addZero(m_Month) + Util.addZero(m_Day) + "" + time + "" + randomNum;
        gBank_name = btnbank;

        String url = Constants.SERVER_HOST+"/road/service.vcnt";
        String car_number = btn_car_no_1.getText().toString() + "" + btn_car_no_2.getText().toString() + "" + btn_car_no_3.getText().toString() + "" + btn_car_no_4.getText().toString();
        String param = "?id=APP"
                + "&act=PAY_ACTION"
                + "&pio_num=" + out.getPioNumber()
                + "&req_tx=pay"
                + "&currency=WON"
                + "&pay_method=VCNT"
                + "&good_name=U1"
                + "&good_mny=" + out.getYetAmt().replaceAll(",", "")
                + "&ordr_idxx=" + gOrdr_idxx
                + "&ipgm_name=" + car_number
                + "&ipgm_bank=" + "BK" + cch.findCodeByKnCode("BA", btnbank)
                + "&ipgm_date=" + impdate + "235959";
        Log.d("333", " =============apiParkIoListCall url >>> " + url + param);

        AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>();
        cb.url(url + param).type(JSONObject.class).weakHandler(this, "callBackVcnt").redirect(true).retry(3).fileCache(false).expire(-1);
        AQuery aq = new AQuery(this);
        aq.ajax(cb);
    }

    public void callBackVcnt(String url, JSONObject json, AjaxStatus status) {
        JSONArray jsonArr = null;
        // successful ajax call
        gVcnt_account = "";
        gIpgm_date = "";

        url = Constants.API_SERVER + "?" + Constants.METHOD + "=" + Constants.INTERFACEID_UNPAY_VCNT;
        String param = "&ordr_idxx=" + gOrdr_idxx
                + "&" + Constants.API_FORMAT + "=" + Constants.JSON_FORMAT;
        Log.d("333", " INTERFACEID_UNPAY_VCNT url >>> " + url + param);

        AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>();
        cb.url(url + param).type(JSONObject.class).weakHandler(this, "callbackGetVcntInfo").redirect(true).retry(3).fileCache(false).expire(-1);
        AQuery aq = new AQuery(this);
        aq.ajax(cb);

		/*
		apiParkOutCall(null, tvParkType.getText().toString(), btnParkPosition.getText().toString().replaceAll("면", "")
				, tvChargeDiscount.getText().toString(), tvParkPayment.getTag().toString(), tvPaymentAmt.getText().toString().replaceAll("원", "").toString(), discountAmt, ""+truncAmt, ADJ_AMT);
		*/
    }

    public void callbackGetVcntInfo(String url, JSONObject json, AjaxStatus status) {

        JSONArray jsonArr = null;
        // successful ajax call
        if (json != null) {
            if ("1".equals(json.optString("CD_RESULT"))) {
                jsonArr = json.optJSONArray("RESULT");

                int Len = jsonArr.length();
                try {
                    for (int i = 0; i < Len; i++) {
                        JSONObject res = jsonArr.getJSONObject(i);
                        Log.d("333", res.optString("account"));
                        Log.d("333", res.optString("ipgm_date"));
                        gVcnt_account = subStringBankAcc(res.optString("account"));
                        gIpgm_date = res.optString("ipgm_date");
                    }

                    apiParkOutCall(null, tvParkType.getText().toString(), btnParkPosition.getText().toString().replaceAll("면", "")
                            , tvChargeDiscount.getText().toString(), tvParkPayment.getTag().toString(), tvPaymentAmt.getText().toString().replaceAll("원", "").toString(), discountAmt, "" + truncAmt, ADJ_AMT);

                } catch (JSONException e) {
                    Toast.makeText(this, Constants.DATA_FAIL, Toast.LENGTH_LONG).show();
                }

            } else {
                Toast.makeText(this, Constants.DATA_FAIL, Toast.LENGTH_LONG).show();
            }
        } else {
            Toast.makeText(this, Constants.DATA_FAIL, Toast.LENGTH_LONG).show();
        }
    }

    private String subStringBankAcc(String acc){
        StringBuffer account = new StringBuffer();
        if(acc.length()>0){
            int idx = 0;
            for(int i = 0; i<acc.length();i++){
                if(i > 3 &&i%4==0){
                    account.append(acc, i-4, i);
                    account.append("-");
                    idx = i;
                }else if(i==acc.length()-1){
                    account.append(acc, idx, acc.length());
                }
            }
        }
        return account.toString();
    }


    private void goEntranceModify() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("입차수정").setMessage("입차정보를 수정하시겠습니까?");
        builder.setPositiveButton(R.string.btn_ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                apiParkIoUpdateCall(parkIO.getPioNum(), carNo, etPhoneNumber.getText().toString(), btnParkPosition.getText().toString().replaceAll("면", "")
                        , parkIO.getParkInDay(), tvParkOutTime.getText().toString(), tvChargeDiscount.getText().toString(), tvParkType.getText().toString()
                        , tvChargeType.getText().toString());
                dialog.dismiss();
            }
        }).setNegativeButton(R.string.btn_cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alert = builder.create();
        alert.setCancelable(false);
        alert.show();
    }

    public void DialogTimePicker() {

        TimePickerDialog.OnTimeSetListener mTimeSetListener = new TimePickerDialog.OnTimeSetListener() {
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

                String t = (hourOfDay < 10 ? "0" + hourOfDay : hourOfDay) + ":" + (minute < 10 ? "0" + minute : minute);

                int startTime = Util.sToi(workStartTime.replace(":", ""));
                int endTime = Util.sToi(workEndTime.replace(":", ""));
                int dTime = Util.sToi(t.replace(":", ""));

                if (dTime < startTime || dTime > endTime) {
                    // 근무시간체크 주석...
                    // MsgUtil.ToastMessage(ExitCommitActivity.this, ("선택한 시간은 근무시간이 아닙니다.다시 선택해 주세요\n시작시간 : " + workStartTime + ", 종료시간 : " + workEndTime), Toast.LENGTH_LONG);
                }

                String inDate = tvParkInTime.getText().toString().replace("-", "").replace(" ", "").replace(":", "").trim();
                String outDate = tvParkOutTime.getText().toString().replace("-", "").replace(" ", "").replace(":", "").trim();
                if (inDate.length() < 12 || outDate.length() < 12) {
                    MsgUtil.ToastMessage(ExitCommitActivity.this, "시간 형식이 잘못되었습니다.");
                    return;
                }

                String inDay = inDate.substring(0, 8);
                String inTime = inDate.substring(8, 12);
                String outDay = outDate.substring(0, 8);
                String outTime = outDate.substring(8, 12);

                // 입차시간이 선택한 출차시간보다 클 경우...
                if (Util.sToi(inTime) > dTime) {
                    MsgUtil.ToastMessage(ExitCommitActivity.this, "입차시간이 출차시간보다 큽니다.");
                    return;
                }

                outDate = Util.dateString(outDay + t);

                tvParkOutTime.setText(outDate);

                exitTime = t + ":00";

                setParkAmt(false);

            }
        };

        String parkOutDay = tvParkOutTime.getText().toString().replaceAll(" ", "").replaceAll("/", "").replaceAll("-", "").replaceAll(":", "").trim();
        String timeHH = parkOutDay.substring(8, 10);
        String timeMi = parkOutDay.substring(10, 12);

        TimePickerDialog timePicker = new TimePickerDialog(this, R.style.CustomDatePickerDialog, mTimeSetListener, Integer.parseInt(timeHH), Integer.parseInt(timeMi), false);
        timePicker.show();

    }

    //parkIoResult
    private void apiParkIoListCall_Print(String _tic) {
        showProgressDialog();
        String url = Constants.API_SERVER + "?" + Constants.METHOD + "=" + Constants.INTERFACEID_PARK_IO;
        String car_number = btn_car_no_1.getText().toString() + "" + btn_car_no_2.getText().toString() + "" + btn_car_no_3.getText().toString() + "" + btn_car_no_4.getText().toString();
        String param = "&CD_PARK=" + parkCode
                + "&CD_STATE=" + _tic
                + "&CAR_NO=" + car_number
                + "&START_DAY=" + today
                + "&END_DAY=" + today
                + "&" + Constants.API_FORMAT + "=" + Constants.JSON_FORMAT;
        Log.d(TAG, " apiParkIoListCall url >>> " + url + param);

        AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>();
        cb.url(url + param).type(JSONObject.class).weakHandler(this, "parkIoListCallback_Print").redirect(true).retry(3).fileCache(false).expire(-1);
        AQuery aq = new AQuery(this);
        aq.ajax(cb);
    }

    public void parkIoListCallback_Print(String url, JSONObject json, AjaxStatus status) {
        closeProgressDialog();

        JSONArray jsonArr = null;
        String title = "";
        String totalTime = "";
        String outTime = "";

        // successful ajax call
        if (json != null) {
            if ("1".equals(json.optString("CD_RESULT"))) {
                jsonArr = json.optJSONArray("RESULT");

                int Len = jsonArr.length();
                try {
                    for (int i = 0; i < Len; i++) {

                        JSONObject res2 = jsonArr.getJSONObject(i);
                        String parkType = cch.findCodeByCdCode("PY", res2.optString("PARK_TYPE"));
                        String disCd = cch.findCodeByCdCode("DC", res2.optString("DIS_CD"));

                        if ("01".equals(res2.optString("PARK_TYPE"))) {    // 시간권(후불), // 정기권
                            title = "주차증";
                            totalTime = "";
                            outTime = "";
                        } else if ("03".equals(res2.optString("PARK_TYPE"))) {    // 일일권, 시간권(선불)
                            title = "주차증";
                            totalTime = "";
                            outTime = "";
                        } else if ("02".equals(res2.optString("PARK_TYPE"))) {    // 일일권, 시간권(선불)
                            title = "선불영수증";
                            totalTime = minTohour(Integer.valueOf(res2.optString("PARK_TIME")));
                            outTime = res2.optString("PARK_OUT");
                        } else if ("04".equals(res2.optString("PARK_TYPE"))) {    // 일일권, 시간권(선불)
                            title = "선불영수증";
                            totalTime = minTohour(Integer.valueOf(res2.optString("PARK_TIME")));
                            outTime = res2.optString("PARK_OUT");
                        }

                        String workTime = workStartTime.substring(0, 2)
                                + ":"
                                + workStartTime.substring(2)
                                + "~"
                                + workEndTime.substring(0, 2)
                                + ":"
                                + workEndTime.substring(2);

                        //입차영수증
                        ReceiptType13Vo type13Vo = new ReceiptType13Vo(title        // 타이틀
                                , Util.getYmdhms("yyyy-MM-dd HH:mm:ss")    // 출력일자
                                , res2.optString("CD_AREA")            // 주차면
                                , parkType                // 주차종류
                                , res2.optString("CHARGE_TYPE")            // 요금종류
                                , disCd        // 요금할인
                                , res2.optString("YET_AMT")    // 총미수금액
                                , res2.optString("ADV_IN_AMT")                                // 선납금액
                                , res2.optString("PARK_IN")        // 시작시간
                                , outTime        // 출차예정시간
                                , totalTime    // 총 분
                                , BIZ_NO            // 사업자번호
                                , BIZ_NAME            // 사업자명
                                , BIZ_TEL, empTicket_print, workTime);            // 사업자연락처

                        type13Vo.setPioNum(res2.optString("PIO_NUM"));
                        type13Vo.setCarNum(res2.optString("CAR_NO"));
                        type13Vo.setParkName(parkName);
                        type13Vo.setEmpName(empName);
                        type13Vo.setEmpPhone(empPhone);
                        type13Vo.setEmpTel(empTel);
                        type13Vo.setAccount_No(empAccount_No);
                        type13Vo.setBank_MSG(empBank_MSG);
                        type13Vo.setKN_ACCOUNT(empKN_ACCOUNT);
                        type13Vo.setKN_BANK(empKN_BANK);
                        type13Vo.setEmpBusiness_Tel(empBusiness_TEL);
                        if (("시간권(후불)").equals(parkType)) {
                            type13Vo.setPrintBill(true);
                        }


                        type13Vo.setCashReceipt(res2.optString("CASH_APPROVAL_NO"));
                        type13Vo.setCardNo(res2.optString("CARD_NO"));
                        type13Vo.setCardCompany(res2.optString("CARD_COMPANY"));
                        type13Vo.setApprovalNo(res2.optString("APPROVAL_NO"));
                        type13Vo.setOutPrint(Preferences.getValue(this, Constants.PREFERENCE_OUT_PRINT, ""));

                        PayData payData = new PayData(this, OUT_PRINT, IN_PRINT, TICKET_PRINT);
                        payData.printDefault(PayData.PAY_DIALOG_TYPE_ENTRANCE_TIME, type13Vo, mPrintService);
                        payData.ImagePrint(empLogo_image);
                        try {
                            payData.printText("\n\n");
                        } catch (UnsupportedEncodingException e) {
                            System.out.println("UnsupportedEncodingException 예외 발생");
                        }

                    }
                } catch (JSONException e) {
                    Toast.makeText(this, Constants.DATA_FAIL, Toast.LENGTH_LONG).show();
                }

            } else {
                Toast.makeText(this, Constants.DATA_FAIL, Toast.LENGTH_LONG).show();
            }
        } else {
            Toast.makeText(this, Constants.DATA_FAIL, Toast.LENGTH_LONG).show();
        }
    }

    protected void apiPrintUpdate(String pionum) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put(Constants.METHOD, Constants.INTERFACEID_PRINT_UPDATE);
        params.put(Constants.API_FORMAT, Constants.JSON_FORMAT);
        params.put("PIO_NUM", pionum);

        showProgressDialog(false);
        String url = Constants.API_SERVER;

        AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>();
        cb.url(url).params(params).type(JSONObject.class).weakHandler(this, "printUpdateCallback").redirect(true).retry(3).fileCache(false).expire(-1);
        AQuery aq = new AQuery(this);
        aq.ajax(cb);
    }

    public void printUpdateCallback(String url, JSONObject json, AjaxStatus status) {

        closeProgressDialog();

        Log.d(TAG, " printUpdateCallback  json ====== " + json);

        // successful ajax call
        if (json != null) {
            String KN_RESULT = json.optString("KN_RESULT");
            if ("1".equals(json.optString("CD_RESULT"))) {
                MsgUtil.ToastMessage(this, KN_RESULT);
            } else {
                MsgUtil.ToastMessage(this, KN_RESULT);
            }
        } else {
            MsgUtil.ToastMessage(this, Constants.DATA_FAIL, Toast.LENGTH_LONG);
        }

    }

    private String minTohour(int _min) {
        int first = Integer.valueOf(_min / 60);
        int second = _min % 60;
        String f = "";
        String e = "";
        if (first < 10) {
            f = "0" + "" + String.valueOf(first);
        } else {
            f = String.valueOf(first);
        }

        if (second < 10) {
            e = "0" + "" + String.valueOf(second);
        } else {
            e = String.valueOf(second);
        }

        return f + ":" + e;
    }

    Calendar calendar = Calendar.getInstance();
    private String mDate;

    public class calendarDlg extends Dialog {

        public class myCalendar extends CalendarInfo {
            private Button btn_target;

            public myCalendar(Context context, LinearLayout layout, Button _btn) {
                super(context, layout);
                btn_target = _btn;
            }

            @Override
            public void myClickEvent(int yyyy, int MM, int dd) {
                mYear = yyyy;
                mMonth = MM + 1;
                mDay = dd;
                mDate = mYear + Util.addZero(mMonth) + Util.addZero(mDay);

                btn_target.setText(mDate);
                ExitCommitActivity.calendarDlg.this.cancel();
                super.myClickEvent(yyyy, MM, dd);
            }
        }

        TextView tvs[];
        Button btns[];

        public calendarDlg(Context context, Button btnCal) {

            super(context, android.R.style.Theme_Holo_Light_Dialog_NoActionBar);
            setContentView(R.layout.calendar_layout);

            LinearLayout lv = (LinearLayout) findViewById(R.id.calendar_ilayout);

            tvs = new TextView[3];
            tvs[0] = (TextView) findViewById(R.id.tv1);
            tvs[1] = (TextView) findViewById(R.id.tv2);
            tvs[2] = null;

            btns = new Button[4];
            btns[0] = null;
            btns[1] = null;
            btns[2] = (Button) findViewById(R.id.Button03);
            btns[3] = (Button) findViewById(R.id.Button04);

            myCalendar cal = new myCalendar(context, lv, btnCal);

            cal.setControl(btns);
            cal.setViewTarget(tvs);
            cal.initCalendar(btnCal.getText().toString());
        }
    }


    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
    }

}