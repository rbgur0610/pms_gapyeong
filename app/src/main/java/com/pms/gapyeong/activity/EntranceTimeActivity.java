package com.pms.gapyeong.activity;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.pms.gapyeong.common.Preferences;
import com.pms.gapyeong.R;
import com.pms.gapyeong.common.Constants;
import com.pms.gapyeong.common.ImageLoader;
import com.pms.gapyeong.common.MsgUtil;
import com.pms.gapyeong.common.Util;
import com.pms.gapyeong.database.CodeHelper;
import com.pms.gapyeong.database.PictureHelper;
import com.pms.gapyeong.pay.PayData;
import com.pms.gapyeong.pay.PayVo;
import com.pms.gapyeong.vo.ParkIO;
import com.pms.gapyeong.vo.PictureInfo;
import com.pms.gapyeong.vo.ReceiptType13Vo;

public class EntranceTimeActivity extends BaseActivity implements OnCheckedChangeListener {

    public static final int DIALOG_PARK_BOARD = 500;
    public final static int PREPAY_TYPE = 600;
    private StringBuffer unPayPinNums = new StringBuffer();
    private ImageView iv_image;

    private Button btn_car_no_1;
    private Button btn_car_no_2;
    private Button btn_car_no_3;
    private Button btn_car_no_4;
    private Button btn_car_no_manual;

    private EditText etPhoneNumber;
    private Button btnParkPosition;
    private Button btnParkType;
    private Button btnChargeType;
    private Button btnChargeDiscount;
    private TextView btnPrePayment;
    private TextView tvEntranceTime;
    private CheckBox checkEntrance;
    private Button btnEntranceCommit;
    private LinearLayout exitExpectedTimeLayout;
    private TextView exitExpectedTime;
    private TextView exitExpectedTimeMin;

    private String pioNum;

    // 변경된 차량번호(등록 된 차량번호)
    private String carNo = "";

    private String boardNumber;
    private Button btn_top_left;
    private TextView tv_top_title;
    private String thisTime;
    private Button btnUnPayment;
    boolean isBoardNumber;

    ReceiptType13Vo type13Vo;

    boolean isDayTicket;
    boolean isMonTicket;

    private String defaultEndTime;
    private String defaultStartTime;
    private String parkAmt = "0";
    private String discountAmt = "0";
    private int truncAmt = 0;
    private String gCouponData = ""; //쿠폰추가

    private PictureHelper pictureHelper = PictureHelper.getInstance(this);
    private String parkOutData;
    private EditText edMemo;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.entrance_time_layout);
        unPayPinNums = new StringBuffer();
        cch = CodeHelper.getInstance(this);
    }

    @Override
    protected void initLayoutSetting() {
        super.initLayoutSetting();

        btn_top_left = (Button) findViewById(R.id.btn_top_left);
        btn_top_left.setVisibility(View.VISIBLE);
        btn_top_left.setOnClickListener(this);
        tv_top_title = (TextView) findViewById(R.id.tv_top_title);
        edMemo = (EditText) findViewById(R.id.edMemo);
        btnEntranceCommit = (Button) findViewById(R.id.btnEntranceCommit);
        tvEntranceTime = (TextView) findViewById(R.id.tvEntranceTime);
        btnChargeDiscount = (Button) findViewById(R.id.btnChargeDiscount);
        btnChargeType = (Button) findViewById(R.id.btnChargeType);
        btnParkType = (Button) findViewById(R.id.btnParkType);
        btnParkPosition = (Button) findViewById(R.id.btnParkPosition);
        etPhoneNumber = (EditText) findViewById(R.id.etPhoneNumber);
        checkEntrance = (CheckBox) findViewById(R.id.checkEntrance);
        btnPrePayment = (Button) findViewById(R.id.btnPrePayment);
        btnUnPayment = (Button) findViewById(R.id.btnUnPayment);

        btn_car_no_1 = (Button) findViewById(R.id.btn_car_no_1);
        btn_car_no_2 = (Button) findViewById(R.id.btn_car_no_2);
        btn_car_no_3 = (Button) findViewById(R.id.btn_car_no_3);
        btn_car_no_4 = (Button) findViewById(R.id.btn_car_no_4);
        btn_car_no_manual = (Button) findViewById(R.id.btn_car_no_manual);

        iv_image = (ImageView) findViewById(R.id.iv_image);
        exitExpectedTimeLayout = (LinearLayout) findViewById(R.id.exit_expected_time_layout);
        exitExpectedTime = (TextView) findViewById(R.id.exit_expected_time);
        exitExpectedTimeMin = (TextView) findViewById(R.id.exit_expected_time_min);

        btn_car_no_1.setOnClickListener(this);
        btn_car_no_2.setOnClickListener(this);
        btn_car_no_3.setOnClickListener(this);
        btn_car_no_4.setOnClickListener(this);
        btn_car_no_manual.setOnClickListener(this);

        btnUnPayment.setOnClickListener(this);
        btnPrePayment.setOnClickListener(this);
        btnParkType.setOnClickListener(this);
        btnEntranceCommit.setOnClickListener(this);
        btnChargeType.setOnClickListener(this);
        btnChargeDiscount.setOnClickListener(this);
        btnParkPosition.setOnClickListener(this);
        checkEntrance.setOnCheckedChangeListener(this);
        iv_image.setOnClickListener(this);


        thisTime = Util.getYmdhms("HHmm").substring(0, 2) + ":" + Util.getYmdhms("HHmm").substring(2);
        tvEntranceTime.setText(thisTime);


        if ("2".equals(parkClass)) {
            setCommonItem("AM2", Constants.COMMON_TYPE_CHARGE_TYPE, btnChargeType);
        } else if ("3".equals(parkClass)) {
            setCommonItem("AM3", Constants.COMMON_TYPE_CHARGE_TYPE, btnChargeType);
        } else if ("4".equals(parkClass)) {
            setCommonItem("AM4", Constants.COMMON_TYPE_CHARGE_TYPE, btnChargeType);
        } else if ("5".equals(parkClass)) {
            setCommonItem("AM5", Constants.COMMON_TYPE_CHARGE_TYPE, btnChargeType);
        } else if ("6".equals(parkClass)) {
            setCommonItem("AM6", Constants.COMMON_TYPE_CHARGE_TYPE, btnChargeType);
        }

        //setCommonItem("AM2", Constants.COMMON_TYPE_CHARGE_TYPE, btnChargeType);

        tv_top_title.setText("입차등록");

        setCommonItem("PY", Constants.COMMON_TYPE_PARK_TYPE, btnParkType);
        setCommonItem("DC", Constants.COMMON_TYPE_CHARGE_DISCOUNT, btnChargeDiscount);


        Intent intent = getIntent();
        if (intent != null) {

            String carInputMode = intent.getStringExtra(Constants.CAR_INPUT_MODE);

            if (Constants.CAR_NORMAL.equals(carInputMode)) {
                carNo = Constants.KEY_CARNO_1 + Constants.KEY_CARNO_2 + Constants.KEY_CARNO_3 + Constants.KEY_CARNO_4;
                pioNum = makePIONumber(parkCode, empCode, today);
                boardNumber = Constants.STATUS_BOARD;

                setCarnoView(Constants.KEY_CARNO_1, Constants.KEY_CARNO_2, Constants.KEY_CARNO_3, Constants.KEY_CARNO_4, "",
                        btn_car_no_1, btn_car_no_2, btn_car_no_3, btn_car_no_4, btn_car_no_manual, Constants.CAR_NORMAL);

                apiParkClose();

            } else if (Constants.CAR_MANUAL.equals(carInputMode)) {
                carNo = Constants.KEY_CARNO_MANUAL;
                pioNum = makePIONumber(parkCode, empCode, today);
                boardNumber = Constants.STATUS_BOARD;

                setCarnoView(carNo, btn_car_no_1, btn_car_no_2, btn_car_no_3, btn_car_no_4, btn_car_no_manual);

                apiParkClose();

            } else if (Constants.CAR_AUTO.equals(carInputMode)) {
                carNo = Constants.AUTO_CAR_NUMBER;
                // 번호인식에서 생성한 주차번호 셋팅
                pioNum = Constants.AUTO_PIO_NUMBER;
                boardNumber = intent.getStringExtra("CD_AREA");

                setCarnoView(carNo, btn_car_no_1, btn_car_no_2, btn_car_no_3, btn_car_no_4, btn_car_no_manual);

                // 이미지 리사이즈...
                ImageLoader.resizeImage2(Constants.AUTO_CAR_IMG_PATH + "/" + Constants.AUTO_CAR_IMG_NAME);

                // 로컬 사진 저장
                pictureHelper.insert(pioNum, Constants.AUTO_CAR_IMG_PATH + "/" + Constants.AUTO_CAR_IMG_NAME, parkCode, carNo, Constants.PICTURE_TYPE_P1, Util.getYmdhms("yyyyMMdd"));

                // 서버 사진 저장
                apiParkPictureUploadCall(Constants.AUTO_CAR_IMG_PATH + "/" + Constants.AUTO_CAR_IMG_NAME);
            }

        }


        String parkType = cch.findCodeByKnCode("PY", btnParkType.getText().toString());
        if ("02".equals(parkType) || "04".equals(parkType)) {    // 일일권, 시간권(선불)
            setParkTypeView(parkType);
        }

        btnParkPosition.setText(boardNumber + "면");

        btnUnPayment.setText("0원");

        exitExpectedTimeMin.setTag("");
        exitExpectedTimeMin.setText("");
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.btn_car_no_1:
                goToActivityInputCarNumber(Constants.RESULT_INPUT_CAR_NUMBER, Constants.CAR_UPDATE, 1);
                break;
            case R.id.btn_car_no_2:
                goToActivityInputCarNumber(Constants.RESULT_INPUT_CAR_NUMBER, Constants.CAR_UPDATE, 2);
                break;
            case R.id.btn_car_no_3:
                goToActivityInputCarNumber(Constants.RESULT_INPUT_CAR_NUMBER, Constants.CAR_UPDATE, 3);
                break;
            case R.id.btn_car_no_4:
                goToActivityInputCarNumber(Constants.RESULT_INPUT_CAR_NUMBER, Constants.CAR_UPDATE, 4);
                break;
            case R.id.btn_car_no_manual:
                goToActivityInputCarNumber(Constants.RESULT_INPUT_CAR_NUMBER, Constants.CAR_UPDATE, 5);
                break;

            case R.id.btnUnPayment:
                if (!btnUnPayment.getText().toString().replaceAll("원", "").equals("0"))
                    goToUnpaidManagerActivity(carNo);
                break;
            case R.id.btnPrePayment:

                btnParkType.setTag(btnParkType.getText().toString());

                // 선불 가능 시간 체크
                if (!isAdvTimeValidate()) {
                    return;
                }
                // 일일권이 아닐경우에만 선납금 입금창 뜨도록 수정
                if (!"일일권".equals(btnParkType.getText().toString())) {
                    prepayEditDialog(btnChargeType.getText().toString()
                            , btnChargeDiscount.getText().toString()
                            , btnParkType.getText().toString()
                            , tvEntranceTime.getText().toString());
                }
                break;
            case R.id.btn_top_left:
                finish();
                break;

            case R.id.btnEntranceCommit:
                if (carNo == null) {
                    Toast.makeText(EntranceTimeActivity.this, "차량번호가 입력되지 않았습니다. 다시 입력해주세요.", Toast.LENGTH_LONG).show();
                    return;
                }

                if (!isValidPicture()) {
                    return;
                }

                // 할인금액 조회
                String disRate = Util.isNVL(cch.findCodeByDisRate("DC", btnChargeDiscount.getText().toString()), "0");
                if (!"0".equals(disRate) && !is4picSave()) {
                    goToPictureView(pioNum, carNo, Constants.PICTURE_TYPE_DIS, parkCode, empCode);
                    Toast.makeText(EntranceTimeActivity.this, "할인을 선택한 경우 사진등록이 필요합니다.", Toast.LENGTH_LONG).show();
                    return;
                }

                String title = "";
                String parkType = cch.findCodeByKnCode("PY", btnParkType.getText().toString());
                String prePayment = btnPrePayment.getText().toString().replaceAll("원", "");

                // 일일권 또는 선불금 차량일 경우 선납금액 체크
                /***
                 if(parkType.equals("02") || parkType.equals("04")){
                 if(Util.sToi(prePayment) < 1){
                 MsgUtil.ToastMessage(this, btnParkType.getText().toString() + " 금액이 없습니다.\n다시 확인해주세요.");
                 return;
                 }
                 }
                 ***/

                // 일일권 차량이 재 입차시 또는 시간권 후불일 경우 결제를 하지 않음
                if (isDayTicket || "01".equals(parkType)) {
                    // 입차 등록 시만 출력
                    printResult(null);   //입차 프린트
                    apiParkInCall(null);
                } else {

                    if ("04".equals(parkType)) {
                        title = "선불영수증";
                    } else {
                        title = "주차증";
                    }

                    String out_time = "";
                    if ("03".equals(parkType)) {
                        out_time = workEndTime;
                    } else {
                        out_time = exitExpectedTime.getText().toString();
                    }


                    try {
                        String workTime = workStartTime.substring(0, 2)
                                + ":"
                                + workStartTime.substring(2)
                                + "~"
                                + workEndTime.substring(0, 2)
                                + ":"
                                + workEndTime.substring(2);

                        ReceiptType13Vo type13Vo = new ReceiptType13Vo(title        // 타이틀
                                , Util.getYmdhms("yyyy-MM-dd HH:mm:ss")    // 출력일자
                                , btnParkPosition.getText().toString().replaceAll("면", "")            // 주차면
                                , parkType                                        // 주차종류
                                , btnChargeType.getText().toString()            // 요금종류
                                , btnChargeDiscount.getText().toString()        // 요금할인
                                , btnUnPayment.getText().toString().replaceAll("원", "")    // 총미수금액
                                , prePayment                                // 선납금액
                                , tvEntranceTime.getText().toString()        // 시작시간
                                , exitExpectedTime.getText().toString()        // 출차예정시간
                                , exitExpectedTimeMin.getTag().toString()    // 총 분
                                , BIZ_NO            // 사업자번호
                                , BIZ_NAME            // 사업자명
                                , BIZ_TEL, empTicket_print, workTime);            // 사업자연락처

                        type13Vo.setPioNum(pioNum);
                        type13Vo.setCarNum(carNo);
                        type13Vo.setParkName(parkName);
                        type13Vo.setEmpName(empName);
                        type13Vo.setEmpPhone(empPhone);
                        type13Vo.setEmpTel(empTel);
                        type13Vo.setAccount_No(empAccount_No);
                        type13Vo.setBank_MSG(empBank_MSG);
                        type13Vo.setKN_ACCOUNT(empKN_ACCOUNT);
                        type13Vo.setKN_BANK(empKN_BANK);
                        type13Vo.setEmpBusiness_Tel(empBusiness_TEL);
                        type13Vo.setOutPrint(Preferences.getValue(this, Constants.PREFERENCE_OUT_PRINT, ""));
                        prepayDialog(btnParkType.getText().toString()  // 주차종류
                                , prePayment                       // 선납금액
                                , type13Vo);
                    } catch (NullPointerException e) {
                        if (Constants.DEBUG_PRINT_LOG) {
                            e.printStackTrace();
                        } else {
                            System.out.println("예외 발생");
                        }
                    }
                }
                break;
            case R.id.btnChargeDiscount:
                setCommonDialog("요금할인", "DC", Constants.COMMON_TYPE_CHARGE_DISCOUNT, btnChargeDiscount);
                break;
            case R.id.btnParkType:
                btnParkType.setTag(btnParkType.getText().toString());
                setCommonDialog("주차종류", "PY", Constants.COMMON_TYPE_PARK_TYPE, btnParkType);
                break;
            case R.id.btnParkPosition:
                isBoardNumber = true;
                apiParkIoListCall(Util.getEncodeStr("%"), "I1", today, today);
                break;

            case R.id.btnChargeType:

                if ("2".equals(parkClass)) {
                    setCommonDialog("요금종류", "AM2", Constants.COMMON_TYPE_CHARGE_TYPE, btnChargeType);
                } else if ("3".equals(parkClass)) {
                    setCommonDialog("요금종류", "AM3", Constants.COMMON_TYPE_CHARGE_TYPE, btnChargeType);
                } else if ("4".equals(parkClass)) {
                    setCommonDialog("요금종류", "AM4", Constants.COMMON_TYPE_CHARGE_TYPE, btnChargeType);
                } else if ("5".equals(parkClass)) {
                    setCommonDialog("요금종류", "AM5", Constants.COMMON_TYPE_CHARGE_TYPE, btnChargeType);
                } else if ("6".equals(parkClass)) {
                    setCommonDialog("요금종류", "AM6", Constants.COMMON_TYPE_CHARGE_TYPE, btnChargeType);
                }

                //setCommonDialog("요금종류","AM2", Constants.COMMON_TYPE_CHARGE_TYPE, btnChargeType);

                break;

            case R.id.iv_image:
                goToPictureView(pioNum, carNo, Constants.PICTURE_TYPE_P1, parkCode, empCode);
                break;
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void getSelectListDialogData(int tag, String data) {
        switch (tag) {
            case Constants.COMMON_TYPE_CHARGE_DISCOUNT_PREPAY:    // 주차선납금계산
            {
                minusTimeCheck(prepay_tvStime.getText().toString().replace(":", ""), prepay_tvEtime.getText().toString().replace(":", ""), prepay_tvParkTime);
                String time = prepay_tvParkTime.getTag().toString().replaceAll("분", "");
                setParkAmtPrepay(time);
                break;
            }

            case Constants.COMMON_TYPE_PARK_TYPE:
                setParkTypeView(data);
                /**XXX 이전버전 2015.10.31 작업
                 if(data.equals("일일권"))
                 setCommonDialog("일일권 요금종류","DDAM", Constants.COMMON_TYPE_DDAM_CHARGE_TYPE, btnParkType);
                 else
                 setParkTypeView(data);
                 **/
                break;
            /**XXX 이전버전 2015.10.31 작업
             case Constants.COMMON_TYPE_DDAM_CHARGE_TYPE:	///< 일일권 요금종류 (DDAM)
             {
             btnPrePayment.setText(data);
             break;
             }
             **/
            case DIALOG_PARK_BOARD:
                btnParkPosition.setText(data + "면");
                break;

            case Constants.COMMON_TYPE_CHARGE_DISCOUNT:
                Log.i(TAG, "dis : " + data + ", visi : " + exitExpectedTimeLayout.getVisibility());
                if ("일일권".equals(btnParkType.getText().toString())) {
                    Toast.makeText(getApplicationContext(), "일일권은 요금 할인이 적용되지 않습니다.", Toast.LENGTH_SHORT).show();
                    setCommonItem("DC", Constants.COMMON_TYPE_CHARGE_DISCOUNT, btnChargeDiscount);
                } else {
                    String start_time = tvEntranceTime.getText().toString().replaceAll(":", "");
                    String end_time = exitExpectedTime.getText().toString().replaceAll(":", "");
                    if (Util.isEmpty(start_time) || start_time.length() < 4) {
                        start_time = "0000";
                    }
                    if (Util.isEmpty(end_time) || end_time.length() < 4) {
                        end_time = "0000";
                    }
                    String date1 = today + start_time + "00";
                    String date2 = Util.getYmdhms("yyyyMMdd") + end_time + "00";
                    String parkMin = minuteCalc(date1, date2);
                    setParkAmt(parkMin);
                }
                break;

            case Constants.COMMON_TYPE_CHARGE_TYPE:    ///< 입차등록 > 요금종류
                Log.i(TAG, "dis : " + data + ", visi : " + exitExpectedTimeLayout.getVisibility());
                String charge_start_time = tvEntranceTime.getText().toString().replaceAll(":", "");
                String charge_end_time = exitExpectedTime.getText().toString().replaceAll(":", "");
                if (Util.isEmpty(charge_start_time) || charge_start_time.length() < 4) {
                    charge_start_time = "0000";
                }
                if (Util.isEmpty(charge_end_time) || charge_end_time.length() < 4) {
                    charge_end_time = "0000";
                }
                String charge_date1 = today + charge_start_time + "00";
                String charge_date2 = Util.getYmdhms("yyyyMMdd") + charge_end_time + "00";
                String charge_parkMin = minuteCalc(charge_date1, charge_date2);
                setParkAmt(charge_parkMin);
                break;

            case Constants.COMMON_TYPE_POPUP_CHARGE_TYPE:    ///< 주차선납금계산(popup) > 요금종류
                Log.i(TAG, "(pop)dis : " + data + ", visi : " + exitExpectedTimeLayout.getVisibility());

                btnChargeType.setText(data);
                minusTimeCheck(prepay_tvStime.getText().toString().replace(":", ""), prepay_tvEtime.getText().toString().replace(":", ""), prepay_tvParkTime);
                String time = prepay_tvParkTime.getTag().toString().replaceAll("분", "");
                setParkAmtPrepay(time);
                break;
        }

    }

    private boolean isAdvTimeValidate() {
        // 선불 결제 시간이 있을 경우
        if (!"0".equals(advBoundTime)) {
            int endHour = Integer.parseInt(workEndTime.substring(0, 2));
            int endMin = Integer.parseInt(workEndTime.substring(2));

            Log.d(TAG, " workEndTime ::: " + workEndTime);
            Log.d(TAG, " endHour ::: " + endHour + " endMin >> " + endMin);
            int endMiunte = (endHour * 60) + endMin;
            Log.d(TAG, " endMiunte ::: " + endMiunte + " advBoundTime >> " + advBoundTime);
            endMiunte -= Integer.parseInt(advBoundTime);

            int dTime = Integer.parseInt(Util.getYmdhms("HH"));
            int dMin = Integer.parseInt(Util.getYmdhms("HHmm").substring(2));
            int dMiunte = (dTime * 60) + dMin;

            Log.d(TAG, "dMiunte : " + dMiunte + ", endMiunte : " + endMiunte);
            if (dMiunte < endMiunte) {
                int h = endMiunte / 60;
                int m = endMiunte % 60;
                String str = String.format("선납가능한 시간이 아닙니다.\n선납 가능한 시간: %02d:%02d ~ %02d:%02d", h, m, endHour, endMin);
                twoButtonDialog(1, "알림", str, false).show();

                if (btnParkType.getTag() != null) {
                    btnParkType.setText(btnParkType.getTag().toString());
                }

                return false;
            }
        }
        return true;
    }

    private void setParkTypeView(String data) {
        String parkType = cch.findCodeByKnCode("PY", data);
        if ("01".equals(parkType)    // 시간권(후불)
                //|| "02".equals(parkType) // 일일권
                || "03".equals(parkType)) {    // 정기권
            // 시간권(후불), 정기권일 경우 하단을 가리고 선납금액을 지운다.
            exitExpectedTimeLayout.setVisibility(View.GONE);
            exitExpectedTime.setText("");
            exitExpectedTimeMin.setTag("");
            exitExpectedTimeMin.setText("");
            btnPrePayment.setText("0원");

            // 주차원금 초기화
            parkAmt = "0";

            // 할인금액 초기화
            discountAmt = "0";

            // 절삭금액 초기화
            truncAmt = 0;

            if ("02".equals(parkType)) {    // 일일권
                setParkAmt("0");
            }

        } else {    // 선불권

            if ("04".equals(parkType)) {    // 시간권(선불)
                // 선불 가능 시간 체크
                if (!isAdvTimeValidate()) {
                    return;
                }
            }

            exitExpectedTimeLayout.setVisibility(View.VISIBLE);
            exitExpectedTime.setText(workEndTime.substring(0, 2) + ":" + workEndTime.substring(2));

            String date1 = today + tvEntranceTime.getText().toString().replaceAll(":", "") + "00";
            String date2 = Util.getYmdhms("yyyyMMdd") + workEndTime + "00";
            String parkMin = minuteCalc(date1, date2);
            exitExpectedTimeMin.setTag(parkMin);
            exitExpectedTimeMin.setText(minuteView(parkMin));

            if ("02".equals(parkType)) {    // 일일권
                setParkAmt("0");
            } else {
                setParkAmt(parkMin);
            }
        }
    }

    @Override
    protected void request(int type, Object data) {
        super.request(type, data);
        // 선불결제 팝업 후 콜백 함수(현금결재일경우)
        if (type == PayData.PAY_DIALOG_TYPE_COUPON) { //쿠폰추가
            gCouponData = (String) data;
        } else if (type == PayData.PAY_DIALOG_TYPE_PREPAY_CASH) {
            PayVo payVo = (PayVo) data;
            if (Constants.isReceiptPrint) {
                printResult(payVo);
            }
            apiParkInCall(payVo);
        } else if (type == PayData.PAY_DIALOG_TYPE_PREPAY_CARD) {
            PayVo payVo = (PayVo) data;
            // 선납금 결재 후 서버에 요청
            // 결재 승인 완료 후 납부 금액, 카드번호, 승인번호를 저장
            //printResult(payVo);
            apiParkInCall(payVo);
        }
    }

    @Override
    public void onBasicDlgItemSelected(int tag, boolean choice) {
        switch (tag) {
            case 9999:
                finish();
                break;

            case 10:
                apiParkIoListCall(carNo, "I1", today, today);
                break;
        }
    }

    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();

        Log.d(TAG, " onResume ");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        Log.d(TAG, " requestCode >>> " + requestCode);

        switch (requestCode) {
            case Constants.RESULT_INPUT_CAR_NUMBER:
                if (resultCode != RESULT_OK) {
                    return;
                }

                if (data != null) { // 번호 재입력 시
                    String carInputMode = Util.isNVL(data.getStringExtra(Constants.CAR_INPUT_MODE));
                    if (Constants.CAR_NORMAL.equals(carInputMode)) {
                        carNo = Constants.KEY_CARNO_1 + Constants.KEY_CARNO_2 + Constants.KEY_CARNO_3 + Constants.KEY_CARNO_4;

                        setCarnoView(Constants.KEY_CARNO_1, Constants.KEY_CARNO_2, Constants.KEY_CARNO_3, Constants.KEY_CARNO_4, "",
                                btn_car_no_1, btn_car_no_2, btn_car_no_3, btn_car_no_4, btn_car_no_manual, Constants.CAR_NORMAL);

                    } else if (Constants.CAR_MANUAL.equals(carInputMode)) {
                        carNo = Constants.KEY_CARNO_MANUAL;

                        setCarnoView("", "", "", "", Constants.KEY_CARNO_MANUAL,
                                btn_car_no_1, btn_car_no_2, btn_car_no_3, btn_car_no_4, btn_car_no_manual, Constants.CAR_MANUAL);

                    }

                    apiUnpayAttachV2(carNo);
                }
                break;

            case Constants.RESULT_CAR_PICTURE:
                Log.d(TAG, " RESULT_CAR_PICTURE ::: " + Constants.RESULT_CAR_PICTURE);
                setParkImageView();
                break;
            case PayData.PAY_DIALOG_TYPE_PREPAY_CARD:
                Log.d(TAG, " requestCode >>> " + requestCode);

                try {
                    Log.d("111", "mPrintService.getState() : " + mPrintService.getState() + "~" + mBluetoothAddress);
                    mPrintService.stop();

                    if (mPrintService.getState() == 0) {

                        BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(mBluetoothAddress);
                        mPrintService.connect(device, true);
                    }
                } catch (IllegalArgumentException e) {
                    System.out.println("IllegalArgumentException 예외 발생");
                } catch (NullPointerException e) {
                    System.out.println("NullPointerException 예외 발생");
                }

                reqPrepay();
                break;
            case PayData.PAY_DIALOG_TYPE_CASH_RECEIPT:  // 현금영수증
                try {
                    mPrintService.stop();
                    if (mPrintService.getState() == 0) {

                        BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(mBluetoothAddress);
                        mPrintService.connect(device, true);
                    }
                } catch (IllegalArgumentException e) {
                    System.out.println("IllegalArgumentException 예외 발생");
                } catch (NullPointerException e) {
                    System.out.println("NullPointerException 예외 발생");
                }

                String approvalNo = Preferences.getValue(this, Constants.PREFERENCE_MCPAY_APPROVAL_NO, "");
                String approvalDate = Preferences.getValue(this, Constants.PREFERENCE_MCPAY_APPROVAL_DATE, "");
                String amt_tot = Preferences.getValue(this, Constants.PREFERENCE_MCPAY_TOTAL_AMT, "");

                PayVo payVo2 = new PayVo();  //현금영수증참조
                payVo2.setCashApprovalNo(approvalNo);
                payVo2.setCashApprovalDate(approvalDate);
                tvCashBill.setTag(payVo2);
                tvCashBill.setText("발행");
                InitPreferences();
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    if (Constants.DEBUG_PRINT_LOG) {
                        e.printStackTrace();
                    } else {
                        System.out.println("예외 발생");
                    }
                }
                cashReceiptRun(Util.sToi(amt_tot), g_vo);
                break;


        }
    }

    private void reqPrepay() {

        try {
            String transactionNo = Preferences.getValue(this, Constants.PREFERENCE_MCPAY_TRANSACTION_NO, "");
            String approvalNo = Preferences.getValue(this, Constants.PREFERENCE_MCPAY_APPROVAL_NO, "");
            String approvalDate = Preferences.getValue(this, Constants.PREFERENCE_MCPAY_APPROVAL_DATE, "");
            String cardNo = Preferences.getValue(this, Constants.PREFERENCE_MCPAY_CARD_NO, "");
            String cardCompany = Preferences.getValue(this, Constants.PREFERENCE_MCPAY_CARD_COMPANY, "");
            String cardAmt = Preferences.getValue(this, Constants.PREFERENCE_MCPAY_TOTAL_AMT, "");

            if (transactionNo == "" || cardNo == "" || approvalNo == "") {
                Log.d("111", "Card error");
                return;
            }

            if (cardNo.length() >= 12) {
                cardNo = cardNo.substring(0, 4) + cardNo.substring(12);
            }

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                if (Constants.DEBUG_PRINT_LOG) {
                    e.printStackTrace();
                } else {
                    System.out.println("예외 발생");
                }
            }

            Bundle eB = new Bundle();
            eB.putString("transactionNo", transactionNo);
            eB.putString("approvalNo", approvalNo);
            eB.putString("approvalDate", approvalDate);
            eB.putString("cardNo", cardNo);
            eB.putString("cardCompany", cardCompany);
            eB.putString("cardAmt", cardAmt);
            eB.putString("pioNum", gReceiptVo.getPioNum());
            eB.putString("carNum", gReceiptVo.getCarNum());


            PayData payData = new PayData(this, OUT_PRINT, IN_PRINT, TICKET_PRINT);
            payData.printPayment(PayData.PAY_DIALOG_TYPE_PREPAY_CARD, gReceiptVo, mPrintService, eB, null);
            payData.ImagePrint(empLogo_image);
            try {
                payData.printText("\n\n");
            } catch (UnsupportedEncodingException e) {
                if (Constants.DEBUG_PRINT_LOG) {
                    e.printStackTrace();
                } else {
                    System.out.println("예외 발생");
                }
            }


            Preferences.putValue(this, Constants.PREFERENCE_MCPAY_TRANSACTION_NO, "");
            Preferences.putValue(this, Constants.PREFERENCE_MCPAY_APPROVAL_NO, "");
            Preferences.putValue(this, Constants.PREFERENCE_MCPAY_APPROVAL_DATE, "");
            Preferences.putValue(this, Constants.PREFERENCE_MCPAY_CARD_NO, "");
            Preferences.putValue(this, Constants.PREFERENCE_MCPAY_CARD_COMPANY, "");
            Preferences.putValue(this, Constants.PREFERENCE_MCPAY_TOTAL_AMT, "");

            // 서버 결제 로그 저장 step2
            String url = Constants.API_SERVER + "?" + Constants.METHOD + "=" + Constants.INTERFACEID_LOG_PARAMETER;
            String param = "&transactionNo=" + transactionNo
                    + "&approvalNo=" + approvalNo
                    + "&approvalDate=" + approvalDate
                    + "&cardNo=" + cardNo
                    + "&cardCompany=" + cardCompany
                    + "&cardAmt=" + cardAmt
                    + "&class=" + this.getClass().getSimpleName()
                    + "&type=pay_step2"
                    + "&" + Constants.API_FORMAT + "=" + Constants.JSON_FORMAT;

            Log.d(TAG, " apiParkLogCall step 2 url >>> " + url + param);

            AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>();
            cb.url(url + param).type(JSONObject.class).weakHandler(this, "").redirect(true).retry(3).fileCache(false).expire(-1);
            AQuery aq = new AQuery(this);
            aq.ajax(cb);

            //prepayDialog.dismiss();
            PayVo payVo = new PayVo();
            payVo.setTransactionNo(transactionNo);
            payVo.setApprovalNo(approvalNo);
            payVo.setApprovalDate(approvalDate);
            payVo.setCardNo(cardNo);
            payVo.setCardCompany(cardCompany);
            payVo.setCardAmt(cardAmt);
            payVo.setCashAmt("0");
            payVo.setCatNumber(catNumber);
            request(PayData.PAY_DIALOG_TYPE_PREPAY_CARD, payVo);

        } catch (NullPointerException e) {
            if (Constants.DEBUG_PRINT_LOG) {
                e.printStackTrace();
            } else {
                System.out.println("예외 발생");
            }
        }
    }


    private void printResult(PayVo payVo) {

        if (isMonTicket) {
            return;
        }

        // 입차 등록
        String parkType = cch.findCodeByKnCode("PY", btnParkType.getText().toString());
        String title = "";

        if ("01".equals(parkType) || "03".equals(parkType)) {    // 시간권(후불), // 정기권
            title = "주차증";
        } else if ("02".equals(parkType) || "04".equals(parkType)) {    // 일일권, 시간권(선불)
            title = "선불영수증";
        }

        if (payVo == null) {
            payVo = new PayVo();
            payVo.setCashAmt("0");
            payVo.setCardAmt("0");
        }

        int cardAmt = Util.sToi(payVo.getCardAmt());
        int cashAmt = Util.sToi(payVo.getCashAmt());
        int coupon = Integer.valueOf(payVo.getCouponAmt());
        String prePayment = "0";
        //홍규혁 쿠폰 계산필요없음
//		if(coupon > 0) {
//			prePayment = (cardAmt > 0 ? "" + (cardAmt-coupon) : "" + (cashAmt-coupon));
//		} else  {
        prePayment = (cardAmt > 0 ? "" + cardAmt : "" + cashAmt);
//		}
        int unPay = Math.abs(Integer.parseInt(parkAmt) - Integer.parseInt(prePayment) + Integer.parseInt(btnUnPayment.getText().toString().replaceAll("원", "")) - Integer.parseInt(discountAmt) - truncAmt);

        String out_time = "";
        if ("03".equals(parkType)) {
            out_time = workEndTime;
        } else {
            out_time = exitExpectedTime.getText().toString();
        }

        String workTime = workStartTime.substring(0, 2)
                + ":"
                + workStartTime.substring(2)
                + "~"
                + workEndTime.substring(0, 2)
                + ":"
                + workEndTime.substring(2);

        type13Vo = new ReceiptType13Vo(title    // 타이틀
                , Util.getYmdhms("yyyy-MM-dd HH:mm:ss")    // 출력일자
                , btnParkPosition.getText().toString().replaceAll("면", "")        // 주차면
                , btnParkType.getText().toString()            // 주차종류
                , btnChargeType.getText().toString()        // 요금종류
                , btnChargeDiscount.getText().toString()    // 요금할인
                , "" + unPay        // 총미수금액
                , prePayment    // 선납금액
                , Util.getYmdhms("yyyyMMdd") + tvEntranceTime.getText().toString()    // 입차시간
                , exitExpectedTime.getText().toString()        // 출차예정시간
                , exitExpectedTimeMin.getText().toString()    // 총 분
                , BIZ_NO            // 사업자번호
                , BIZ_NAME            // 사업자명
                , BIZ_TEL, empTicket_print, workTime);            // 사업자연락처


        type13Vo.setPioNum(pioNum);
        type13Vo.setCarNum(carNo);
        type13Vo.setParkName(parkName);
        type13Vo.setEmpName(empName);
        type13Vo.setEmpPhone(empPhone);
        type13Vo.setEmpTel(empTel);
        type13Vo.setAccount_No(empAccount_No);
        type13Vo.setBank_MSG(empBank_MSG);
        type13Vo.setKN_ACCOUNT(empKN_ACCOUNT);
        type13Vo.setKN_BANK(empKN_BANK);
        type13Vo.setEmpBusiness_Tel(empBusiness_TEL);
        type13Vo.setCashReceipt(payVo.getCashApprovalNo());
        type13Vo.setCardNo(payVo.getCardNo());
        type13Vo.setCardCompany(payVo.getCardCompany());
        type13Vo.setApprovalNo(payVo.getApprovalNo());
        type13Vo.setOutPrint(Preferences.getValue(this, Constants.PREFERENCE_OUT_PRINT, ""));
        // 일반 입차 영수증일 경우 결제 관련은 출력하지 않는다.
        if ("01".equals(parkType) || "03".equals(parkType)) {
            type13Vo.setPrintBill(true);
        }


        PayData payData = new PayData(this, OUT_PRINT, IN_PRINT, TICKET_PRINT);
        payData.printDefault(PayData.PAY_DIALOG_TYPE_ENTRANCE_TIME, type13Vo, mPrintService);
        payData.ImagePrint(empLogo_image);
        try {
            payData.printText("\n\n");
        } catch (UnsupportedEncodingException e) {
            if (Constants.DEBUG_PRINT_LOG) {
                e.printStackTrace();
            } else {
                System.out.println("예외 발생");
            }
        }
    }

    // 입차 등록(수정)
    private void apiParkInCall(PayVo payVo) {
        if (!isValidPicture()) {
            return;
        }

        if (payVo == null) {
            payVo = new PayVo();
            payVo.setCashAmt("0");
            payVo.setCardAmt("0");
        }

        if (Util.isEmpty(btnParkPosition.getText().toString())) {
            MsgUtil.ToastMessage(EntranceTimeActivity.this, "주차면 정보가 없습니다.", Toast.LENGTH_LONG);
            return;
        }

        int cardAmt = 0;

        int cashAmt = 0;
        int coupon = Integer.valueOf(payVo.getCouponAmt());
        if (coupon > 0) {
            if (Util.sToi(payVo.getCardAmt()) > 0) {
                cardAmt = Util.sToi(payVo.getCardAmt());
            }

            cashAmt = Util.sToi(payVo.getCashAmt());
            if (cashAmt > coupon) {
                cashAmt = cashAmt - coupon;
            } else {
                cashAmt = 0;
            }
        } else {
            cardAmt = Util.sToi(payVo.getCardAmt());

            if (cardAmt > 0) {
                if (gCouponData != null) {
                    if (gCouponData.length() > 0) {
                        String[] arr = gCouponData.split("\\^");
                        for (int i = 0; i < arr.length; i++) {
                            if (arr[i].length() > 0) {
                                String[] temp = arr[i].split("\\$");
                                String temp_amt = Util.isNVL(temp[3], "0");
                                if (temp_amt.length() > 0) {
                                    temp_amt = temp_amt.substring(temp_amt.indexOf("=") + 1, temp_amt.length());
                                    coupon += Util.sToi(temp_amt);
                                }
                            }
                        }
                    }
                }
            }

            cashAmt = Util.sToi(payVo.getCashAmt());
        }

        String prePayment = (cardAmt > 0 ? "" + (cardAmt + coupon) : "" + (cashAmt + coupon));


        Log.d(TAG, " prePayment>>> " + prePayment);

        String parkTypeCode = cch.findCodeByKnCode("PY", btnParkType.getText().toString());
        String discountTypeCode = cch.findCodeByKnCode("DC", btnChargeDiscount.getText().toString());

        Map<String, Object> params = new HashMap<String, Object>();

        // method 셋팅 - 입차등록
        params.put(Constants.METHOD, Constants.INTERFACEID_PARK_IO_COMMIT_UNPAY);
        // format
        params.put(Constants.API_FORMAT, Constants.JSON_FORMAT);
        // 26자리 주차번호 BaseActivity.makepioNum(cdPark, empCode, parkDay)
        params.put("PIO_NUM", (pioNum == null ? BaseActivity.makePIONumber(parkCode, empCode, today) : pioNum));
        // 주차장코드(3자리)
        params.put("CD_PARK", parkCode);
        // 주차면
        params.put("CD_AREA", btnParkPosition.getText().toString().replaceAll("면", ""));
        // 주차일 (8자리)
        params.put("PARK_DAY", today);
        // 입차일
        params.put("IN_DATE", Util.getYmdhms("yyyy-MM-dd"));

        // 요금종류
        String chargeTypeCode = "";

        if ("2".equals(parkClass)) {
            chargeTypeCode = cch.findCodeByKnCode("AM2", btnChargeType.getText().toString());
        } else if ("3".equals(parkClass)) {
            chargeTypeCode = cch.findCodeByKnCode("AM3", btnChargeType.getText().toString());
        } else if ("4".equals(parkClass)) {
            chargeTypeCode = cch.findCodeByKnCode("AM4", btnChargeType.getText().toString());
        } else if ("5".equals(parkClass)) {
            chargeTypeCode = cch.findCodeByKnCode("AM5", btnChargeType.getText().toString());
        } else if ("6".equals(parkClass)) {
            chargeTypeCode = cch.findCodeByKnCode("AM6", btnChargeType.getText().toString());
        }

        //chargeTypeCode = cch.findCodeByKnCode("AM2", btnChargeType.getText().toString());

        params.put("CHARGE_TYPE", chargeTypeCode);

        String IN_TIME = tvEntranceTime.getText().toString();
        if (Util.isEmpty(IN_TIME)) {
            IN_TIME = IN_TIME + "00";
        } else {
            IN_TIME = IN_TIME + ":00";
        }
        // 주차 입차시간
        params.put("IN_TIME", IN_TIME);

        String OUT_TIME = exitExpectedTime.getText().toString();
        if (Util.isEmpty(OUT_TIME)) {
            OUT_TIME = OUT_TIME + "00";
        } else {
            OUT_TIME = OUT_TIME + ":00";
        }
        // 주차 출차시간
        params.put("OUT_TIME", OUT_TIME);

        // 차량 번호
        params.put("CAR_NO", carNo);
        // 주차형태 02
        params.put("PARK_TYPE", parkTypeCode);
        // 할인 코드
        params.put("DIS_CD", discountTypeCode);

        // 현금 영수증 관련
        params.put("CASH_APPROVAL_NO", payVo.getCashApprovalNo());
        params.put("CASH_APPROVAL_DATE", payVo.getCashApprovalDate());
        params.put("CATNUMBER", payVo.getCatNumber());

        // 카드 결제 관련
        params.put("TRANSACTION_NO", payVo.getTransactionNo());
        params.put("APPROVAL_NO", payVo.getApprovalNo());
        params.put("APPROVAL_DATE", payVo.getApprovalDate());
        params.put("CARD_NO", payVo.getCardNo());
        params.put("CARD_COMPANY", payVo.getCardCompany());

        String unpayAmt =  Preferences.getValue(this, Constants.PREFERENCE_UNPAY_AMT, "0");

        if(!"0".equals(unpayAmt)&&!"".equals(unPayPinNums.toString())){
            //미수관련
            params.put("UNPAY_PIO_NUM", unPayPinNums);
            params.put("UNPAY_DIS_AMT", discountAmt);
            params.put("UNPAY_DIS_CD", discountTypeCode);
            params.put("UNPAY_PARK_OUT", OUT_TIME);
        }



        // 미납 금액 - 입차시 미납 금액은 0
        params.put("YET_AMT", unpayAmt);

        if (parkTypeCode.equals("01") || parkTypeCode.equals("03")) {  // 시간권(후불)은  출차등록 시 금액 계산 정기권은 금액계산없이 입차
            // realamt 원금
            params.put("REAL_AMT", 0);
            // 현금 금액 = 선불결제화면에서 직접입력한 값입력
            params.put("CASH_AMT", 0);
            // 카드 금액 = 선불결제화면에서 카드결재금액
            params.put("CARD_AMT", 0);
            // 선납
            params.put("ADV_AMT", 0);
            // 할인 금액 = 할인코드에 맞는 총합
            params.put("DIS_AMT", 0);
            // 절삭 금액 = 100단위 삭제
            params.put("TRUNC_AMT", 0);
            // 선불권 처리 방법
            params.put("ADV_METHOD", "");
        } else {
            // 주차 원금
            params.put("REAL_AMT", parkAmt);
            // 현금 금액
            params.put("CASH_AMT", "" + cashAmt);
            // 카드 금액
            params.put("CARD_AMT", "" + cardAmt);
            // 선납 금액
            params.put("ADV_AMT", "" + prePayment);
            // 할인 금액 = 할인코드에 맞는 총합
            params.put("DIS_AMT", "" + discountAmt);
            // 절삭 금액 = 100단위 삭제
            params.put("TRUNC_AMT", "" + truncAmt);

            // 선불권  처리방법
            if ("04".equals(parkTypeCode)) {
                // 선불권 차량 입차
                params.put("ADV_METHOD", "ADV0");
            }
        }


        //쿠폰추가
        String real_amt = "0";
        //쿠폰금액이 주차원금을 넘었을 경우 쿠폰금액을 주차원금으로... Yoon-1.09
        if (Integer.valueOf(cashAmt) > 0) {
            real_amt = String.valueOf(cashAmt);
        } else {
            real_amt = String.valueOf(cardAmt);
        }

        if (payVo.getCouponAmt() != "") {
            if (Integer.valueOf(real_amt) > 0) {
                if (Integer.valueOf(real_amt) < Integer.valueOf(payVo.getCouponAmt())) {
                    params.put("COUPON_AMT", real_amt);
                } else {
                    params.put("COUPON_AMT", payVo.getCouponAmt());
                }
            } else {
                params.put("COUPON_AMT", payVo.getCouponAmt());
            }
        }
        params.put("COUPON_DATA", gCouponData);

        // 환불 금액 - 입차시 환불 금액은 0
        params.put("RETURN_AMT", "0");


//      xx 2015.02.03        
        // 차감금액
//        params.put("OFF_AMT", offAmt);

        // 연락처
        params.put("TEL", etPhoneNumber.getText().toString());
        // I1(입차)
        params.put("STATE_CD", "I1");
        // 등록사원(4자리)
        params.put("EMP_CD", empCode);
        // commnet
        params.put("KN_COMMENT", edMemo.getText().toString());

        showProgressDialog(false);

        String url = Constants.API_SERVER;
        Log.d(TAG, " apiParkInResult url >>> " + url + " " + params);

        AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>();
        cb.url(url).params(params).type(JSONObject.class).weakHandler(this, "parkInCallback").redirect(true).retry(3).fileCache(false).expire(-1);
        AQuery aq = new AQuery(this);
        aq.ajax(cb);

    }

    public void parkInCallback(String url, JSONObject json, AjaxStatus status) {
        Log.d(TAG, " parkInResultCallback  json ====== " + json);
        closeProgressDialog();
        // successful ajax call
        if (json != null) {
            String KN_RESULT = json.optString("KN_RESULT");
            if ("1".equals(json.optString("CD_RESULT"))) {
                MsgUtil.ToastMessage(EntranceTimeActivity.this, KN_RESULT, Toast.LENGTH_LONG);
                Intent intent = new Intent(this, StatusBoardActivity.class);
                //intent.putExtra("currentPage", "3");
                startActivity(intent);
                finish();
            } else {
                MsgUtil.ToastMessage(EntranceTimeActivity.this, KN_RESULT, Toast.LENGTH_LONG);
            }
        } else {
            MsgUtil.ToastMessage(EntranceTimeActivity.this, Constants.DATA_FAIL, Toast.LENGTH_LONG);
        }
    }


    /**
     * 주차장 마감 체크
     */
    private void apiParkClose() {
        showProgressDialog(false);
        String url = Constants.API_SERVER + "?" + Constants.METHOD + "=" + Constants.INTERFACEID_PARK_CLOSE;
        String param = "&CD_PARK=" + parkCode
                + "&WORK_DAY=" + today
                + "&" + Constants.API_FORMAT + "=" + Constants.JSON_FORMAT;
        Log.d(TAG, " apiParkClose url >>> " + url + param);

        AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>();
        cb.url(url + param).type(JSONObject.class).weakHandler(this, "parkCloseCallback").redirect(true).retry(3).fileCache(false).expire(-1);
        AQuery aq = new AQuery(this);
        aq.ajax(cb);
    }

    public void parkCloseCallback(String url, JSONObject json, AjaxStatus status) {

        closeProgressDialog();
        Log.d(TAG, " parkCloseCallback  json ====== " + json);

        // successful ajax call
        if (json != null) {
            if ("C".equals(json.optString("CD_STATE"))) {
                // 마감이 완료되었다는 팝업 표시 후 종료
                MsgUtil.ToastMessage(this, "주차장 영업시간이 마감되었습니다.", Toast.LENGTH_LONG);
                goToMainActivity();
            } else {
                // 차에 대한 미납금 조회
                apiUnpayAttachV2(carNo);
            }
        } else {
            MsgUtil.ToastMessage(this, Constants.DATA_FAIL, Toast.LENGTH_LONG);
        }

    }

    /**
     * 차량 미납 조회
     */
    private void apiUnpayAttachV2(String carNo) {
        //showProgressDialog(false);
        String url = Constants.API_SERVER + "?" + Constants.METHOD + "=" + Constants.INTERFACEID_PARK_UNPAYATTACH_V3;
        String param = "&CD_PARK=" + parkCode
                + "&CAR_NO=" + carNo
                + "&" + Constants.API_FORMAT + "=" + Constants.JSON_FORMAT;
        Log.d(TAG, " apiUnpayAttachV2 url >>> " + url + param);

        AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>();
        cb.url(url + param).type(JSONObject.class).weakHandler(this, "unpayAttachV2Callback").redirect(true).retry(3).fileCache(false).expire(-1);
        AQuery aq = new AQuery(this);
        aq.ajax(cb);
    }

    public void unpayAttachV2Callback(String url, JSONObject json, AjaxStatus status) {
        unPayPinNums = new StringBuffer();
        //closeProgressDialog();
        Log.d(TAG, " unpayAttachV2Callback  json ====== " + json);

        boolean bTicket = false;

        JSONArray jsonArr = null;

        // successful ajax call
        if (json != null) {
            String KN_RESULT = json.optString("KN_RESULT");
            if ("1".equals(json.optString("CD_RESULT"))) {
                jsonArr = json.optJSONArray("RESULT");
                int unpayAmt = 0;
                int Len = jsonArr.length();
                try {
                    for (int i = 0; i < Len; i++) {
                        JSONObject res = jsonArr.getJSONObject(i);
                        if ("Y".equalsIgnoreCase(res.optString("CD_STATE"))) {
                            MsgUtil.ToastMessage(this, "압류 대상 차량 입니다.", Toast.LENGTH_LONG);
                        }

                        // 미납금 조회에서 IS_DAYTICKET, IS_MONTICKET 여부를 가져온다.
                        if ("Y".equals(res.optString("IS_DAY_YN"))) {
                            isDayTicket = true;
                        }
						
						/*
						if("Y".equals(res.optString("IS_MONTH_YN"))){
							isMonTicket = true;
						}
						*/
                        if ("05".equals(res.optString("CD_TYPE")) || "44".equals(res.optString("CD_TYPE"))) {
                            isMonTicket = true;
                            parkOutData = res.optString("PARK_OUT");
                        }

                        if (isDayTicket) {
                            setCommonItem("PY", Constants.COMMON_TYPE_PARK_TYPE, btnParkType, "02");    // 두번째가 일일권

                            exitExpectedTimeLayout.setVisibility(View.VISIBLE);
                            String date1 = today + thisTime.replaceAll(":", "") + "00";
                            String date2 = today + workEndTime + "00";
                            String parkMin = minuteCalc(date1, date2);
                            exitExpectedTimeMin.setTag(parkMin);
                            exitExpectedTimeMin.setText(minuteView(parkMin));
                            exitExpectedTime.setText(workEndTime.substring(0, 2) + ":" + workEndTime.substring(2));

                            bTicket = true;
                            twoButtonDialog(10, "알림", "일일권 차량 입니다.", false).show();
                            break;
                        }

                        if (isMonTicket) {
                            setCommonItem("PY", Constants.COMMON_TYPE_PARK_TYPE, btnParkType, "03");    // 세번째가 정기권
                            twoButtonDialog(10, "알림", "정기권 차량 입니다.\n\n종료일 : " + returnDate(parkOutData), false).show();
                            bTicket = true;
                            break;
                        }

                        if(i==0){
                            unPayPinNums.append(res.optString("PIN_NUM"));
                        }else {
                            unPayPinNums.append(",").append(res.optString("PIN_NUM"));
                        }

                        unpayAmt += Util.sToi(res.optString("AMT"));
                    } // end for
                } catch (JSONException e) {
                    if (Constants.DEBUG_PRINT_LOG) {
                        e.printStackTrace();
                    } else {
                        System.out.println("예외 발생");
                    }


                }
                // 미납금 설정
                btnUnPayment.setText(unpayAmt + "원");

            } else {
                btnUnPayment.setText("0원");
                //	MsgUtil.ToastMessage(this, KN_RESULT, Toast.LENGTH_LONG);
            }

            // 일일권 정기권 차량이 아니면 주차 현황 조회(선불금 및 주차종류)
            if (!bTicket) {
                apiParkIoListCall(Util.getEncodeStr("%"), "I1", today, today);
            }
        } else {
            MsgUtil.ToastMessage(this, Constants.DATA_FAIL, Toast.LENGTH_LONG);
        }

    }

    private String returnDate(String date) {
        if (date != null && date.length() > 7) {
            String rDate = date.substring(0, 4) + "년 " + date.substring(4, 6) + "월 " + date.substring(6, 8) + "일";
            return rDate;
        }

        return "";
    }

    public void setParkImageView() {
        List<PictureInfo> list = pictureHelper.getSearchPioNumList(pioNum);
        if (list == null) {
            return;
        }
        for (PictureInfo info : list) {
            String localImgPath = info.getPath();
            Log.d(TAG, " localImgPath >>> " + localImgPath);
            File imgFile = new File(localImgPath);
            if (imgFile.exists()) {
                iv_image.setTag(localImgPath);
                ImageLoader.setImageView(localImgPath, iv_image);
                break;
            }
        }
    }

    private void apiParkPictureUploadCall(String imagePath) {

        Map<String, Object> params = new HashMap<String, Object>();
        params.put(Constants.API_FORMAT, Constants.JSON_FORMAT);
        params.put("PIO_NUM", pioNum);
        params.put("CD_PARK", parkCode);
        params.put("CAR_NO", carNo);
        params.put("CD_TYPE", Constants.PICTURE_TYPE_P1);
        params.put("EMP_CD", empCode);

        Log.d(TAG, " imagePath >>> " + imagePath + " empCode >>> " + empCode);

        //Alternatively, put a File or InputStream instead of byte[]
        File file = new File(imagePath);
        params.put("IMAGE_FILE", file);

        String url = Constants.API_SERVER + "?" + Constants.METHOD + "=" + Constants.INTERFACEID_PARK_PICTURE_UPLOAD;
        Log.d(TAG, " apiParkPictureUploadCall url >>> " + url + " params ::: " + params);

        AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>();
        cb.url(url).params(params).type(JSONObject.class).weakHandler(this, "parkPictureUploadCallback").redirect(true).retry(3).fileCache(false).expire(-1);
        AQuery aq = new AQuery(this);
        aq.ajax(cb);

    }

    public void parkPictureUploadCallback(String url, JSONObject json, AjaxStatus status) {
        Log.d(TAG, " parkPictureUploadCallback  json ====== " + json);
        // successful ajax call
        if (json != null) {
            String KN_RESULT = json.optString("KN_RESULT");
            if ("1".equals(json.optString("CD_RESULT"))) {
                MsgUtil.ToastMessage(this, KN_RESULT);
                setParkImageView();
            } else {
                MsgUtil.ToastMessage(this, KN_RESULT);
            }
        } else {
            MsgUtil.ToastMessage(this, Constants.DATA_FAIL, Toast.LENGTH_LONG);
        }

        // 업로드 후 순차적 실행
        apiParkClose();

    }

    public boolean isValidPicture() { // 입차 사진 유효성 체크
        if (Constants.isCarPicture) {
            // 4급지는 안 찍어도 됨	 cd_park 가 138 과 143 , 200 의 경우는 기본 1장이 있어야 입차처리 되는 부분은 제외요청
            if ("200".equals(parkCode) || "138".equals(parkCode) || "143".equals(parkCode)) {
                return true;
            } else {
                // 입차 사진 필수 빠짐... 일부 공단
                ArrayList<PictureInfo> list = (ArrayList<PictureInfo>) pictureHelper.getSearchPioNumList(pioNum);
                if (list == null || list.size() <= 0) {
                    MsgUtil.AlertDialog(EntranceTimeActivity.this, "알림", "최소한 입차사진 1장이 필요 합니다.\n입차사진촬영 후 이용해 주시기 바랍니다.");
                    return false;
                } else {
                    return true;
                }
            }
        } else {
            return true;
        }
    }

    /**
     * 서버에 입차되었는지 요청한다.
     *
     * @param carNumber 요청할 자동차 번호
     */
    private void apiParkIoListCall(String carNumber, String codeState, String startDay, String endDay) {
        //showProgressDialog(false);

        String url = Constants.API_SERVER + "?" + Constants.METHOD + "=" + Constants.INTERFACEID_PARK_IO;
        String param = "&CD_PARK=" + parkCode
                + "&CAR_NO=" + carNumber
                + "&CD_STATE=" + codeState
                + "&START_DAY=" + startDay
                + "&END_DAY=" + endDay
                + "&" + Constants.API_FORMAT + "=" + Constants.JSON_FORMAT;
        Log.d(TAG, " apiParkIoListCall url >>> " + url + param);

        AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>();
        cb.url(url + param).type(JSONObject.class).weakHandler(this, "parkIoListCallback").redirect(true).retry(3).fileCache(false).expire(-1);
        AQuery aq = new AQuery(this);
        aq.ajax(cb);
    }

    public void parkIoListCallback(String url, JSONObject json, AjaxStatus status) {
        //closeProgressDialog();
        Log.d(TAG, " parkIoListCallback  json ====== " + json);

        ArrayList<ParkIO> list = new ArrayList<ParkIO>();

        // successful ajax call
        if (json != null) {
            String KN_RESULT = json.optString("KN_RESULT");
            if ("1".equals(json.optString("CD_RESULT"))) {

                JSONArray jsonArr = json.optJSONArray("RESULT");
                int Len = jsonArr.length();
                for (int i = 0; i < Len; i++) {
                    try {
                        JSONObject res = jsonArr.getJSONObject(i);
                        ParkIO info = new ParkIO(res.optString("PIO_NUM")
                                , res.optString("CD_PARK")
                                , res.optString("PIO_DAY")
                                , res.optString("CAR_NO")
                                , res.optString("CD_AREA")
                                , res.optString("PARK_TYPE")
                                , res.optString("DIS_CD")
                                , res.optString("TEL")
                                , res.optString("PARK_IN_AMT")
                                , res.optString("PARK_OUT_AMT")
                                , res.optString("PARK_AMT")
                                , res.optString("ADV_IN_AMT")
                                , res.optString("ADV_OUT_AMT")
                                , res.optString("ADV_AMT")
                                , res.optString("DIS_IN_AMT")
                                , res.optString("DIS_OUT_AMT")
                                , res.optString("DIS_AMT")
                                , res.optString("CASH_IN_AMT")
                                , res.optString("CASH_OUT_AMT")
                                , res.optString("CASH_AMT")
                                , res.optString("CARD_IN_AMT")
                                , res.optString("CARD_OUT_AMT")
                                , res.optString("CARD_AMT")
                                , res.optString("TRUNC_IN_AMT")
                                , res.optString("TRUNC_OUT_AMT")
                                , res.optString("TRUNC_AMT")
                                , res.optString("COUPON_AMT")
                                , res.optString("RETURN_AMT")
                                , res.optString("YET_AMT")
                                , res.optString("REAL_AMT")
                                , res.optString("PARK_TIME")
                                , res.optString("PARK_IN")
                                , res.optString("PARK_OUT")
                                , res.optString("CARD_NO")
                                , res.optString("CARD_COMPANY")
                                , res.optString("APPROVAL_NO")
                                , res.optString("APPROVAL_DATE")
                                , res.optString("CHARGE_TYPE")
                                , res.optString("KN_COMMENT"));

                        list.add(info);

                    } catch (JSONException e) {
                        if (Constants.DEBUG_PRINT_LOG) {
                            e.printStackTrace();
                        } else {
                            System.out.println("예외 발생");
                        }
                    }
                }  // end for

            } else {
                // MsgUtil.ToastMessage(EntranceTimeActivity.this, KN_RESULT, Toast.LENGTH_LONG);
            }
        } else {
            MsgUtil.ToastMessage(EntranceTimeActivity.this, Constants.DATA_FAIL, Toast.LENGTH_LONG);
        }

        statusBoardHelper.setParkIo(list);
        Log.d(TAG, "parkIO size : " + list.size());

        if (isBoardNumber) {
            basicListDialog(DIALOG_PARK_BOARD, "주차면", statusBoardHelper.getEmptyNumberList(), btnParkPosition);
            isBoardNumber = false;
        } else {
            // 주차 할 공간이 없음.
            if (statusBoardHelper.getMaxCount() <= list.size()) {
                twoButtonDialog(9999, "알림", "주차 가능한 주차면이 없습니다.", false).show();
                return;
            }

            int Len = list.size();

            boolean isFind = false;
            if (Len > 0) {
                for (int i = 0; i < Len; i++) {
                    ParkIO io = list.get(i);
                    Log.i(TAG, "io.getcarNo() : " + io.getCarNo());
                    if (carNo.equals(io.getCarNo())) {
                        pioNum = io.getPioNum();
                        Log.i(TAG, "pioNum : " + pioNum);
                        btnParkType.setText(cch.findCodeByCdCode("PY", io.getParkType()));
                        btnPrePayment.setText(io.getAdvAmt() + "원");
                        etPhoneNumber.setText(io.getTel());
                        btnParkPosition.setText(io.getCdArea() + "면");
                        isFind = true;
                        thisTime = io.getParkInDay().substring(8, 10) + ":" + io.getParkInDay().substring(10, 12);
                        tvEntranceTime.setText(thisTime);

                        btnChargeDiscount.setText(cch.findCodeByCdCode("DC", io.getDisCd()));

                        // 일일권이거나 선불권일 경우
                        if ("02".equals(io.getParkType()) || "04".equals(io.getParkType())) {
                            Log.i(TAG, "일일권 또는 선불권 type : " + io.getParkType());
                            Log.i(TAG, "일일권 또는 선불권 time : " + io.getParkOutDay());
                            exitExpectedTimeLayout.setVisibility(View.VISIBLE);
                            exitExpectedTime.setText(io.getParkOutDay().substring(8, 10) + ":" + io.getParkOutDay().substring(10, 12));
                            String date1 = today + tvEntranceTime.getText().toString().replaceAll(":", "") + "00";
                            String date2 = today + exitExpectedTime.getText().toString().replaceAll(":", "") + "00";
                            String parkMin = minuteCalc(date1, date2);
                            exitExpectedTimeMin.setTag(parkMin);
                            exitExpectedTimeMin.setText(minuteView(parkMin));
                        }
                    } // end if carNo
                } // end for
            } // end if

            if (isFind == false) {
                btnPrePayment.setText("0" + "원");
                if (boardNumber == null || "".equals(boardNumber)) {
                    btnParkPosition.setText(statusBoardHelper.getEmptyNumber() + "면");
                }
            }

        }
    }

    @Override
    public void onCheckedChanged(CompoundButton v, boolean isChecked) {

        switch (v.getId()) {
            case R.id.checkEntrance:
                if (isChecked) {
                    String startTime = workStartTime.substring(0, 2) + ":" + workStartTime.substring(2);
                    tvEntranceTime.setText(startTime);
                } else {
                    tvEntranceTime.setText(thisTime);
                }

                if (exitExpectedTimeLayout.getVisibility() == View.VISIBLE) {
                    String date1 = today + tvEntranceTime.getText().toString().replaceAll(":", "") + "00";
                    String date2 = today + exitExpectedTime.getText().toString().replaceAll(":", "") + "00";
                    String parkMin = minuteCalc(date1, date2);
                    exitExpectedTimeMin.setTag(parkMin);
                    exitExpectedTimeMin.setText(minuteView(parkMin));
                    setParkAmt(parkMin);
                }
                break;
        }
    }

    private void setParkAmtPrepay(String parkMin) {

        // 주차원금 초기화
        parkAmt = "0";

        // 할인금액 초기화
        discountAmt = "0";

        // 절삭금액 초기화
        truncAmt = 0;


        //###############################################################
        //  STEP 1 : 주차 시간 계산
        //###############################################################
        // Log.d(TAG, "date1 : " + date1 + ", date2 : " + date2);
        Log.d(TAG, " parkMin >>> " + parkMin);


        //###############################################################
        //  STEP 2 : 할인 시간 계산
        //###############################################################
        // 요금할인 정보
        String discountName = prepay_tvChargeDiscount.getText().toString();
        Log.d(TAG, " setParkAmtPrepay ::: discountName  >>> " + discountName);

        // 할인시간 설정
        int disTime = Util.sToi(cch.findCodeByDisTime("DC", discountName), 0);
        Log.d(TAG, " setParkAmtPrepay ::: disTime  >>> " + disTime);
        if (disTime > 0) {
            parkMin = "" + (Util.sToi(parkMin, 0) - disTime);
            if ((Util.sToi(parkMin, 0) < 0)) {
                parkMin = "0";
            }
        }


        //###############################################################
        //  STEP 3 : 할인 금액 계산
        //###############################################################
        // 주차종류
        String parkTypeCode = cch.findCodeByKnCode("PY", prepay_tvParkType.getText().toString());
        Log.d(TAG, " setParkAmtPrepay ::: parkTypeCode  >>> " + parkTypeCode);

        final TextView prepay_tvChargeType = (TextView) prepayEditDialog.findViewById(R.id.prepay_tvChargeType);

        // 요금종류코드
        String chargeTypeCode = "";

        if ("2".equals(parkClass)) {
            chargeTypeCode = cch.findCodeByKnCode("AM2", prepay_tvChargeType.getText().toString());
        } else if ("3".equals(parkClass)) {
            chargeTypeCode = cch.findCodeByKnCode("AM3", prepay_tvChargeType.getText().toString());
        } else if ("4".equals(parkClass)) {
            chargeTypeCode = cch.findCodeByKnCode("AM4", prepay_tvChargeType.getText().toString());
        } else if ("5".equals(parkClass)) {
            chargeTypeCode = cch.findCodeByKnCode("AM5", prepay_tvChargeType.getText().toString());
        } else if ("6".equals(parkClass)) {
            chargeTypeCode = cch.findCodeByKnCode("AM6", prepay_tvChargeType.getText().toString());
        }

        //chargeTypeCode = cch.findCodeByKnCode("AM2", prepay_tvChargeType.getText().toString());
        // 주차금액 계산
        String total = parkCharge(parkMin, chargeTypeCode, parkTypeCode);
        String charge = "0";

        Log.d(TAG, " setParkAmtPrepay ::: total >>> " + total);

        if ("02".equals(parkTypeCode)) {  // 일일권
            total = "" + dayAmt; // 일일권은 입차시 일일권 금액으로 셋팅
            charge = total;
            parkAmt = total;
            discountAmt = "0";         // 일일권은 할인을 하지 않는다.
            truncAmt = 0;
        } else if ("03".equals(parkTypeCode)) { // 정기권
            total = "0";         // 정기권은 입차시 0원
            charge = "0";
            parkAmt = "0";         // 정기권은 할인을 하지 않는다.
            discountAmt = "0";
            truncAmt = 0;
        } else {
            // 주차원금 셋팅
            charge = total;
            parkAmt = charge;

            int tempDisAmt1 = 0;
            int tempDisAmt2 = 0;

            // 할인금액 조회
            String disAmt = Util.isNVL(cch.findCodeByDisAmt("DC", discountName), "0");
            if (!"0".equals(disAmt)) {
                // 할인금액 설정
                tempDisAmt1 = Util.sToi(disAmt);
                charge = "" + (Util.sToi(charge) - tempDisAmt1);
                Log.d(TAG, "setParkAmtPrepay ::: charge >>> " + charge + "  tempDisAmt1 : " + tempDisAmt1);
            }

            // 할인률 조회
            String disRate = cch.findCodeByDisRate("DC", discountName);
            if (!"0".equals(disRate)) {
                // 할인률에 따른 할인금액 설정
                tempDisAmt2 = Util.sToi(Util.isNVL("" + discountChargeRate(charge, disRate), "0"));
                charge = "" + (Util.sToi(charge) - tempDisAmt2);
                Log.d(TAG, "setParkAmtPrepay ::: charge >>> " + charge + "  tempDisAmt2 : " + tempDisAmt2);
            }

            // 총 할인금액
            discountAmt = "" + (tempDisAmt1 + tempDisAmt2);
            Log.d(TAG, " setParkAmtPrepay >>> charge : " + charge + " discountAmt : " + discountAmt);

            if (Util.sToi(charge, 0) < 0) {
                charge = "0";
            }

        } // end if parkTypeCode

        Log.d(TAG, " setParkAmtPrepay ::: charge >>> " + charge);
        Log.d(TAG, " setParkAmtPrepay ::: parkAmt >>> " + parkAmt);
        Log.d(TAG, " setParkAmtPrepay ::: discountAmt >>> " + discountAmt);


        //###############################################################
        //  STEP 4 : 납부 금액 계산
        //###############################################################
        int payment = Integer.parseInt(charge);


        //###############################################################
        //  STEP 5 : 절삭 금액 계산
        //###############################################################
//		Log.d(TAG, " setParkAmtPrepay 11 ::: payment >>> " + payment);	
//		Log.d(TAG, " setParkAmtPrepay 22 ::: truncUnit >>> " + truncUnit);	
        if (!"0".equals(truncUnit)) {
            truncAmt = payment % Util.sToi(truncUnit);
        } else {
            truncAmt = 0;
        }
//		Log.d(TAG, " setParkAmtPrepay 33 ::: truncAmt >>> " + truncAmt);	
//		Log.d(TAG, " setParkAmtPrepay 44 ::: payment >>> " + payment);	

        //** 과천시 할인금액이 없으면 절삭 적용
        // 주차할인코드
        String disCode = cch.findCodeByKnCode("DC", discountName);
        if ("01".equals(disCode)) {
            payment = payment - truncAmt;
        } else { // 할인금액이 있으면 올림 적용
            if (truncAmt > 0) {
                // 납부금액은 절삭 올림 적용
                payment = payment + (Util.sToi(truncUnit) - truncAmt);
                // 할인금액은 절삭 차감
                discountAmt = "" + (Util.sToi(discountAmt) - (Util.sToi(truncUnit) - truncAmt));
                truncAmt = 0;
            }
        }


        prepay_tvParkAmt.setText(payment + "원");
    }


    private void setParkAmt(String parkMin) {

        // 주차원금 초기화
        parkAmt = "0";

        // 할인금액 초기화
        discountAmt = "0";

        // 절삭금액 초기화
        truncAmt = 0;


        //###############################################################
        //  STEP 1 : 주차 시간 계산
        //###############################################################
        // Log.d(TAG, "date1 : " + date1 + ", date2 : " + date2);
        Log.d(TAG, " parkMin >>> " + parkMin);


        //###############################################################
        //  STEP 2 : 할인 시간 계산
        //###############################################################
        // 요금할인 정보
        String discountName = btnChargeDiscount.getText().toString();
        Log.d(TAG, " setParkAmt ::: discountName  >>> " + discountName);

        // 할인시간 설정
        int disTime = Util.sToi(cch.findCodeByDisTime("DC", discountName), 0);
        Log.d(TAG, " setParkAmt ::: disTime  >>> " + disTime);
        if (disTime > 0) {
            parkMin = "" + (Util.sToi(parkMin, 0) - disTime);
            if ((Util.sToi(parkMin, 0) < 0)) {
                parkMin = "0";
            }
        }


        //###############################################################
        //  STEP 3 : 할인 금액 계산
        //###############################################################
        // 주차종류
        String parkTypeCode = cch.findCodeByKnCode("PY", btnParkType.getText().toString());
        Log.d(TAG, " setParkAmt ::: parkTypeCode  >>> " + parkTypeCode);

        // 요금종류코드
        String chargeCode = "";

        if ("2".equals(parkClass)) {
            chargeCode = cch.findCodeByKnCode("AM2", btnChargeType.getText().toString());
        } else if ("3".equals(parkClass)) {
            chargeCode = cch.findCodeByKnCode("AM3", btnChargeType.getText().toString());
        } else if ("4".equals(parkClass)) {
            chargeCode = cch.findCodeByKnCode("AM4", btnChargeType.getText().toString());
        } else if ("5".equals(parkClass)) {
            chargeCode = cch.findCodeByKnCode("AM5", btnChargeType.getText().toString());
        } else if ("6".equals(parkClass)) {
            chargeCode = cch.findCodeByKnCode("AM6", btnChargeType.getText().toString());
        }

        //chargeCode = cch.findCodeByKnCode("AM2", btnChargeType.getText().toString());

        // 주차금액 계산
        String total = parkCharge(parkMin, chargeCode, parkTypeCode);
        String charge = "0";

        Log.d(TAG, " setParkAmt ::: total >>> " + total);

        if ("02".equals(parkTypeCode)) {  // 일일권
            total = "" + dayAmt; // 일일권은 입차시 일일권 금액으로 셋팅
            charge = total;
            parkAmt = total;
            discountAmt = "0";         // 일일권은 할인을 하지 않는다.
            truncAmt = 0;
        } else if ("03".equals(parkTypeCode)) { // 정기권
            total = "0";         // 정기권은 입차시 0원
            charge = "0";
            parkAmt = "0";         // 정기권은 할인을 하지 않는다.
            discountAmt = "0";
            truncAmt = 0;
        } else {
            // 주차원금 셋팅
            charge = total;
            parkAmt = charge;

            int tempDisAmt1 = 0;
            int tempDisAmt2 = 0;

            // 할인금액 조회
            String disAmt = Util.isNVL(cch.findCodeByDisAmt("DC", discountName), "0");
            if (!"0".equals(disAmt)) {
                // 할인금액 설정
                tempDisAmt1 = Util.sToi(disAmt);
                charge = "" + (Util.sToi(charge) - tempDisAmt1);
                Log.d(TAG, "setParkAmt ::: charge >>> " + charge + "  tempDisAmt1 : " + tempDisAmt1);
            }

            // 할인률 조회
            String disRate = cch.findCodeByDisRate("DC", discountName);
            if (!"0".equals(disRate)) {
                // 할인률에 따른 할인금액 설정
                tempDisAmt2 = Util.sToi(Util.isNVL("" + discountChargeRate(charge, disRate), "0"));
                charge = "" + (Util.sToi(charge) - tempDisAmt2);
                Log.d(TAG, "setParkAmt ::: charge >>> " + charge + "  tempDisAmt2 : " + tempDisAmt2);
            }

            // 총 할인금액
            discountAmt = "" + (tempDisAmt1 + tempDisAmt2);
            Log.d(TAG, " setParkAmt >>> charge : " + charge + " discountAmt : " + discountAmt);

            if (Util.sToi(charge, 0) < 0) {
                charge = "0";
            }

        } // end if parkTypeCode

        Log.d(TAG, " setParkAmt ::: charge >>> " + charge);
        Log.d(TAG, " setParkAmt ::: parkAmt >>> " + parkAmt);
        Log.d(TAG, " setParkAmt ::: discountAmt >>> " + discountAmt);


        //###############################################################
        //  STEP 4 : 납부 금액 계산
        //###############################################################
        int payment = Integer.parseInt(charge);


        //###############################################################
        //  STEP 5 : 절삭 금액 계산
        //###############################################################
        if (!"0".equals(truncUnit)) {
            truncAmt = payment % Integer.parseInt(truncUnit);
        } else {
            truncAmt = 0;
        }

        //** 과천시 할인금액이 없으면 절삭 적용
        // 주차할인코드
        String disCode = cch.findCodeByKnCode("DC", discountName);
        if ("01".equals(disCode)) {
            payment = payment - truncAmt;
        } else { // 할인금액이 있으면 올림 적용
            if (truncAmt > 0) {
                // 납부금액은 절삭 올림 적용
                payment = payment + (Util.sToi(truncUnit) - truncAmt);
                // 할인금액은 절삭 차감
                discountAmt = "" + (Util.sToi(discountAmt) - (Util.sToi(truncUnit) - truncAmt));
                truncAmt = 0;
            }
        }
        Log.d(TAG, " setParkAmt ::: payment >>> " + payment);

        btnPrePayment.setText(payment + "원");
    }


    private Dialog prepayEditDialog;
    private TextView prepay_tvStime;
    private TextView prepay_tvEtime;
    private TextView prepay_tvParkType;
    private TextView prepay_tvParkTime;
    private TextView prepay_tvChargeDiscount;
    private TextView prepay_tvParkAmt;

    private Dialog prepayEditDialog(final String chargeType, final String chargeDiscount, String parkType, String startTime) {

        prepayEditDialog = new Dialog(this, android.R.style.Theme_Holo_Light_Dialog);

        prepayEditDialog.setContentView(R.layout.dialog_prepayedit);
        prepayEditDialog.setTitle("주차선납금계산");
        prepay_tvStime = (TextView) prepayEditDialog.findViewById(R.id.prepay_tvStime);
        prepay_tvEtime = (TextView) prepayEditDialog.findViewById(R.id.prepay_tvEtime);
        CheckBox prepay_checkTime = (CheckBox) prepayEditDialog.findViewById(R.id.prepay_checkTime);
        prepay_tvParkType = (TextView) prepayEditDialog.findViewById(R.id.prepay_tvParkType);
        prepay_tvParkTime = (TextView) prepayEditDialog.findViewById(R.id.prepay_tvParkTime);
        final TextView prepay_tvChargeType = (TextView) prepayEditDialog.findViewById(R.id.prepay_tvChargeType);
        prepay_tvChargeDiscount = (TextView) prepayEditDialog.findViewById(R.id.prepay_tvChargeDiscount);
        prepay_tvParkAmt = (TextView) prepayEditDialog.findViewById(R.id.prepay_tvParkAmt);
        Button btn_ok = (Button) prepayEditDialog.findViewById(R.id.btn_ok);
        Button btn_cancel = (Button) prepayEditDialog.findViewById(R.id.btn_cancel);

        prepay_tvChargeDiscount.setText(chargeDiscount);
        prepay_tvChargeType.setText(chargeType);
        prepay_tvParkType.setText("시간권(선불)");

        prepay_tvChargeDiscount.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                setCommonDialog("요금할인", "DC", Constants.COMMON_TYPE_CHARGE_DISCOUNT_PREPAY, prepay_tvChargeDiscount);
            }
        });

        prepay_tvChargeType.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {


                if ("2".equals(parkClass)) {
                    setCommonDialog("요금종류", "AM2", Constants.COMMON_TYPE_POPUP_CHARGE_TYPE, prepay_tvChargeType);
                } else if ("3".equals(parkClass)) {
                    setCommonDialog("요금종류", "AM3", Constants.COMMON_TYPE_POPUP_CHARGE_TYPE, prepay_tvChargeType);
                } else if ("4".equals(parkClass)) {
                    setCommonDialog("요금종류", "AM4", Constants.COMMON_TYPE_POPUP_CHARGE_TYPE, prepay_tvChargeType);
                } else if ("5".equals(parkClass)) {
                    setCommonDialog("요금종류", "AM5", Constants.COMMON_TYPE_POPUP_CHARGE_TYPE, prepay_tvChargeType);
                } else if ("6".equals(parkClass)) {
                    setCommonDialog("요금종류", "AM6", Constants.COMMON_TYPE_POPUP_CHARGE_TYPE, prepay_tvChargeType);
                }

                //setCommonDialog("요금종류","AM2", Constants.COMMON_TYPE_POPUP_CHARGE_TYPE, prepay_tvChargeType);

            }
        });

        prepay_checkTime.setOnCheckedChangeListener(new android.widget.CheckBox.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                int endTime = Integer.parseInt(workEndTime.replace(":", ""));
                int defaultTime = Integer.parseInt(defaultEndTime.replace(":", ""));

                if (isChecked) {
                    prepay_tvEtime.setText(workEndTime.substring(0, 2) + ":" + workEndTime.substring(2));
                } else {
                    if (defaultTime > endTime)
                        prepay_tvEtime.setText(workEndTime.substring(0, 2) + ":" + workEndTime.substring(2));
                    else
                        prepay_tvEtime.setText(defaultEndTime);
                }

                minusTimeCheck(prepay_tvStime.getText().toString().replace(":", ""), prepay_tvEtime.getText().toString().replace(":", ""), prepay_tvParkTime);
                String time = prepay_tvParkTime.getTag().toString().replaceAll("분", "");
                Log.d(TAG, "time !!!::: " + time);
                setParkAmtPrepay(time);
            }
        });

        int hour = Integer.parseInt(startTime.substring(0, 2)) + 2;

        String endTime = String.valueOf((hour < 10 ? "0" + hour : hour)) + ":" + Util.getYmdhms("mm");
        int nEndTime = Integer.parseInt(endTime.replaceAll(":", ""));
        int nwEndTime = Integer.parseInt(workEndTime);
        Log.i(TAG, "endTime : " + endTime + ", nwEndTime : " + nwEndTime);
        if (nEndTime > nwEndTime) {
            endTime = workEndTime.substring(0, 2) + ":" + workEndTime.substring(2);
        }

        prepay_tvStime.setText(startTime);
        prepay_tvEtime.setText(endTime);

        defaultEndTime = prepay_tvEtime.getText().toString();

        minusTimeCheck(prepay_tvStime.getText().toString().replace(":", ""), prepay_tvEtime.getText().toString().replace(":", ""), prepay_tvParkTime);
        String time = prepay_tvParkTime.getTag().toString().replaceAll("분", "");
        Log.e(TAG, "time ::: " + time);
        setParkAmtPrepay(time);

        prepay_tvStime.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {//입차시간 변경되지 못하게.
                /* 	YOON-1.09 : 입차시간 수정 할 수 없도록
                 * return 추가
                 * DialogTimePicker 주석처리 */
                return;
                //DialogTimePicker(prepay_tvStime,PREPAY_TYPE);
            }
        });

        prepay_tvEtime.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogTimePicker(prepay_tvEtime, PREPAY_TYPE);
            }
        });

        btn_ok.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                String startTime = prepay_tvStime.getText().toString();
                String endTime = prepay_tvEtime.getText().toString();

                if (startTime.equals(endTime)) {
                    MsgUtil.ToastMessage(EntranceTimeActivity.this, "시작시간과 종료시간이 동일합니다.\n다시 선택해 주세요.", Toast.LENGTH_LONG);
                    return;
                }

                String date1 = today + startTime.replaceAll(":", "") + "00";
                String date2 = today + endTime.replaceAll(":", "") + "00";

                String parkMin = minuteCalc(date1, date2);

                exitExpectedTimeMin.setTag(parkMin);
                exitExpectedTimeMin.setText(minuteView(parkMin));
                exitExpectedTimeLayout.setVisibility(View.VISIBLE);
                exitExpectedTime.setText(endTime);

                btnPrePayment.setText(prepay_tvParkAmt.getText().toString());
                setCommonItem("PY", Constants.COMMON_TYPE_PARK_TYPE, btnParkType, "04");
                btnChargeDiscount.setText(prepay_tvChargeDiscount.getText().toString());
                tvEntranceTime.setText(startTime);

                prepayEditDialog.dismiss();
            }
        });

        btn_cancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                setToast("취소되었습니다.");
                prepayEditDialog.dismiss();
            }
        });

        prepayEditDialog.show();
        return prepayEditDialog;
    }

    private boolean is4picSave() {
        List<PictureInfo> list = pictureHelper.getSearchPioNumList(pioNum);
        if (list != null) {

            for (PictureInfo data : list) {
                if (data.getPath().contains("_4")) {
                    return true;
                }
            }
        }
        return false;
    }

    public void DialogTimePicker(final TextView tv, final int Tag) {

        TimePickerDialog.OnTimeSetListener mTimeSetListener = new TimePickerDialog.OnTimeSetListener() {
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

                String t = (hourOfDay < 10 ? "0" + hourOfDay : hourOfDay) + ":" + (minute < 10 ? "0" + minute : minute);

                int startWorkTime = Integer.parseInt(workStartTime.replace(":", ""));
                int endWorkTime = Integer.parseInt(workEndTime.replace(":", ""));
                int dTime = Integer.parseInt(t.replace(":", ""));

                if (dTime < startWorkTime || dTime > endWorkTime) {
                    MsgUtil.ToastMessage(EntranceTimeActivity.this, ("선택한 시간은 근무시간이 아닙니다.\n다시 선택해 주세요.\n시작시간 : " + workStartTime + ", 종료시간 : " + workEndTime), Toast.LENGTH_LONG);

                    if (tv == prepay_tvStime) {
                        defaultStartTime = workStartTime.substring(0, 2) + ":" + workStartTime.substring(2);
                        tv.setText(defaultStartTime);
                    } else {
                        defaultEndTime = workEndTime.substring(0, 2) + ":" + workEndTime.substring(2);
                        tv.setText(defaultEndTime);
                    }
                    return;
                }

                if (tv == prepay_tvStime) {
                    defaultStartTime = t;

                    // 선택한 시작시간이 종료시간보다 클 경우 자동으로 종료 시간을 근무 종료 시간으로 설정
                    if (dTime > Integer.parseInt(prepay_tvEtime.getText().toString().replace(":", "").trim())) {
                        prepay_tvEtime.setText(workEndTime.substring(0, 2) + ":" + workEndTime.substring(2));
                    }

                } else {
                    defaultEndTime = t;
                    // 선택한 종료시간이 시작시간보다 작을 경우 자동으로 시작 시간을 근무 시작 시간으로 설정
                    if (dTime < Integer.parseInt(prepay_tvStime.getText().toString().replace(":", "").trim())) {
                        prepay_tvStime.setText(workStartTime.substring(0, 2) + ":" + workEndTime.substring(2));
                    }
                }
                tv.setText(t);

                switch (Tag) {
                    case PREPAY_TYPE:    // 선납금 팝업
                        minusTimeCheck(prepay_tvStime.getText().toString().replace(":", ""), prepay_tvEtime.getText().toString().replace(":", ""), prepay_tvParkTime);
                        String time = prepay_tvParkTime.getTag().toString().replaceAll("분", "");
                        Log.d(TAG, " time >>>@@@ " + time);
                        setParkAmtPrepay(time);
                        break;
                }
            }
        };

        String timeStr = "";
        String timeHH = "";
        String timeMi = "";
        if (tv == prepay_tvStime) {
            timeStr = prepay_tvStime.getText().toString().replaceAll(":", "");
        } else {
            timeStr = prepay_tvEtime.getText().toString().replaceAll(":", "");
        }
        timeHH = timeStr.substring(0, 2);
        timeMi = timeStr.substring(2);

        TimePickerDialog timePicker = new TimePickerDialog(this, R.style.CustomDatePickerDialog,
                mTimeSetListener,
                Integer.parseInt(timeHH),
                Integer.parseInt(timeMi), false);
        timePicker.show();
    }
}