package com.pms.gapyeong.activity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.pms.gapyeong.R;
import com.pms.gapyeong.common.Constants;
import com.pms.gapyeong.common.DeviceInfo;
import com.pms.gapyeong.common.MsgUtil;
import com.pms.gapyeong.common.Preferences;
import com.pms.gapyeong.common.Util;
import com.pms.gapyeong.database.PictureHelper;
import com.pms.gapyeong.pay.PayData;
import com.pms.gapyeong.pay.PayVo;
import com.pms.gapyeong.vo.ExitPaymentData;
import com.pms.gapyeong.vo.ParkIO;
import com.pms.gapyeong.vo.ParkOut;
import com.pms.gapyeong.vo.PictureInfo;
import com.pms.gapyeong.vo.ReceiptType13Vo;
import com.pms.gapyeong.vo.UnPayManagerItem;

public class ExitAbstractActivity extends BaseActivity {
	
	protected ParkOut out;
	protected String couponData;
	
	private Dialog parkTypeChangeDialog;
	private String mData;
	public int gUnpayAmt = 0;
	public PictureHelper pictureHelper = PictureHelper.getInstance(this);
	protected StringBuffer unPayPinNums = new StringBuffer();

	/**
	 * API 콜백 함수..
	 * 하위 액티비티에서 오버라이딩 해서 사용
	 * @param data
	 */
	protected void requestApi(String type, Object data) {
		
	}
	
	protected void initParkOut(String pioNum, String cdPark, String exitTime, String carNo
							 , String parkType, String tel, String prePaymentAmt, String disCode, String truncAmt
							 , String chargeTypeStr) {
		
		boolean isPicDel = Preferences.getValue(this,Constants.PREFERENCE_SETTING_DELETE_SERVER_IMG, false);
		
		out = new ParkOut(pioNum, 	 	 //	PIO_NUM : 주차번호
						  cdPark, 	 	 //	CD_PARK: 주차장번호
						  empCode, 	 	 //	EMP_CD : 근무자코드
						  today, 	 	 //	WORK_DAY: 날짜
						  exitTime,  	 //	OUT_TIME : 출차시간(8)
						  carNo, 	 	 //	CAR_NO : 차량번호
						  parkType,	 	 //	PARK_TYPE : 주차유형
						  tel, 		 	 //	TEL : 연락처(변경가능)
						  "O1",      	 //	STATE_CD : O1 (입차 ?)
						  prePaymentAmt, //	ADV_AMT : 선불금
						  "B1", 		 //	BILL_TYPE : 결재방법
						  disCode,		 //	DIC_CODE : 할인코드
						  "0", 					//	REAL_AMT : 납부금액
						  "", 					//	CARD_NO: 카드번호
						  "", 					//	APPROVAL_NO : 승인번호
						  catNumber, 			//	CATNUMBER :12자리
						  "0",	 				//	RETURN_AMT : 환불금액
						  "0", 					//	DIC_AMT : 할인금액
						  "0", 					//	CASH_AMT :현금
						  "0", 					//	CARD_AMT : 카드금액
						  "", 					//	YET_AMT :미납금액
						  truncAmt,				//	TRUNC_AMT:절삭금액
						  "",					//  YET_RETURN:미환불금액
						  "", 					
						  "",					
						  "",					// YET_RETURNCARD:미환불카드금액
						  isPicDel);
		
		out.setChargeTypeStr(chargeTypeStr);
		
	}	
	
	
	/**
	 * 서버에 차량이 입차되었는지 요청한다.
	 */
	protected void apiParkIoListCall(String carNo, String codeState, String startDay, String endDay){
		showProgressDialog(false);
		
		String url = Constants.API_SERVER + "?" + Constants.METHOD +"="+ Constants.INTERFACEID_PARK_IO;
		String param = "&CD_PARK="+parkCode
					 + "&CAR_NO="+carNo
					 + "&CD_STATE="+codeState
					 + "&START_DAY="+startDay
					 + "&END_DAY="+endDay
					 + "&"+Constants.API_FORMAT+"="+Constants.JSON_FORMAT;
		Log.d(TAG, " apiParkIoListCall url--------- >>> " + url + param);
		
		AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>();        
		cb.url(url + param).type(JSONObject.class).weakHandler(this, "parkIoListCallback").redirect(true).retry(3).fileCache(false).expire(-1);
		AQuery aq = new AQuery(this);
		aq.ajax(cb);
	}			
	
	public void parkIoListCallback(String url, JSONObject json, AjaxStatus status){
    	closeProgressDialog();
		Log.d("111", " parkIoListCallback  json ====== " +json);
		
		ArrayList<ParkIO> list = new ArrayList<ParkIO>();
		
		// successful ajax call          
	    if(json != null){   
	       String KN_RESULT = json.optString("KN_RESULT");
	       if("1".equals(json.optString("CD_RESULT"))){
	    	   JSONArray jsonArr  = json.optJSONArray("RESULT");
			   int Len = jsonArr.length();
			   for(int i=0; i<Len; i++){
				  try {
					 JSONObject res = jsonArr.getJSONObject(i);
					  ParkIO info = new ParkIO(res.optString("PIO_NUM") 
										   , res.optString("CD_PARK")
										   , res.optString("PIO_DAY")
										   , res.optString("CAR_NO")
										   , res.optString("CD_AREA")
										   , res.optString("PARK_TYPE")
										   , res.optString("DIS_CD")
										   , res.optString("TEL")
										   , res.optString("PARK_IN_AMT")
										   , res.optString("PARK_OUT_AMT")
										   , res.optString("PARK_AMT")
										   , res.optString("ADV_IN_AMT")
										   , res.optString("ADV_OUT_AMT")
										   , res.optString("ADV_AMT")
										   , res.optString("DIS_IN_AMT")
										   , res.optString("DIS_OUT_AMT")
										   , res.optString("DIS_AMT")
										   , res.optString("CASH_IN_AMT")
										   , res.optString("CASH_OUT_AMT")
										   , res.optString("CASH_AMT")
										   , res.optString("CARD_IN_AMT")									   
										   , res.optString("CARD_OUT_AMT")									   
										   , res.optString("CARD_AMT")									   
										   , res.optString("TRUNC_IN_AMT")									   
										   , res.optString("TRUNC_OUT_AMT")									   
										   , res.optString("TRUNC_AMT")									   
										   , res.optString("COUPON_AMT")
										   , res.optString("RETURN_AMT")
										   , res.optString("YET_AMT") 
										   , res.optString("REAL_AMT")
										   , res.optString("PARK_TIME")
										   , res.optString("PARK_IN")
										   , res.optString("PARK_OUT")
										   , res.optString("CARD_NO")
										   , res.optString("CARD_COMPANY")
										   , res.optString("APPROVAL_NO")
										   , res.optString("APPROVAL_DATE")
										   , res.optString("CHARGE_TYPE")
							  , res.optString("KN_COMMENT") );
					    info.setCashApprovalNo(res.optString("CASH_APPROVAL_NO"));
					    info.setCashApprovalDate(res.optString("CASH_APPROVAL_DATE"));
						list.add(info);
				  } catch (JSONException e) {
					  System.out.println("JSONException 예외 발생");
				  }
			   }  // end for
			   
	       } else {
	    	  MsgUtil.ToastMessage(this, KN_RESULT, Toast.LENGTH_LONG);
			  return ;			    	   
	       }
	    } else {
	    	MsgUtil.ToastMessage(this, Constants.DATA_FAIL, Toast.LENGTH_LONG);
	    }  
	    
		closeProgressDialog();
		
		requestApi(Constants.INTERFACEID_PARK_IO, list);	
		
	}	
	
	protected void apiParkIoUpdateCall(String pioNum, String carNo, String tel, String cdArea
							   	     , String parkInDay, String parkOutDay, String discountType, String parkType
							   	     , String chargeType) {
		
        Map<String, Object> params = new HashMap<String, Object>();
        
        // method 셋팅...
		params.put(Constants.METHOD, Constants.INTERFACEID_PARK_IO_UPDATE);
        // format
        params.put(Constants.API_FORMAT, Constants.JSON_FORMAT);		
        // 주차번호 
        params.put("PIO_NUM", 	pioNum);
        // 주차장코드
        params.put("CD_PARK", 	parkCode);
        // 사번코드
        params.put("EMP_CD", 	empCode);        
        // 차량번호
        params.put("CAR_NO", 	carNo);        
        // 연락처
        params.put("TEL", 	    tel);
        // 주차면
        params.put("CD_AREA", 	cdArea);
        // 요금종류
        //params.put("CHARGE_TYPE", cch.findCodeByKnCode("AM2", chargeType));
        
		if("2".equals(parkClass)){
			params.put("CHARGE_TYPE", cch.findCodeByKnCode("AM2", chargeType));
		} else if("3".equals(parkClass)){
			params.put("CHARGE_TYPE", cch.findCodeByKnCode("AM3", chargeType));
		} else if("4".equals(parkClass)){
			params.put("CHARGE_TYPE", cch.findCodeByKnCode("AM4", chargeType));
		} else if("5".equals(parkClass)){
			params.put("CHARGE_TYPE", cch.findCodeByKnCode("AM5", chargeType));
		} else if("6".equals(parkClass)){
			params.put("CHARGE_TYPE", cch.findCodeByKnCode("AM6", chargeType));
		}
		
        parkInDay = parkInDay.replaceAll(" ", "").replaceAll("/", "").replaceAll("-", "").replaceAll(":", "");
        
        String IN_DATE = "";
        String IN_TIME = "";
		if(parkInDay.length() >= 14){
			IN_DATE = parkInDay.substring(0, 4) 
					+ "-" + parkInDay.substring(4, 6) 
					+ "-" + parkInDay.substring(6, 8);
			IN_TIME = parkInDay.substring(8, 10)
					+ ":" + parkInDay.substring(10, 12) 
					+ ":" + parkInDay.substring(12, 14);
		} else if(parkInDay.length() >= 12){
			IN_DATE = parkInDay.substring(0, 4) 
					+ "-" + parkInDay.substring(4, 6) 
					+ "-" + parkInDay.substring(6, 8);
			IN_TIME = parkInDay.substring(8, 10)
					+ ":" + parkInDay.substring(10, 12) 
					+ ":" + "00";
		} else {
			MsgUtil.ToastMessage(this, "입차시간 형식이 잘못되었습니다.");
			return;
		}
        
        // 입차일자
        params.put("IN_DATE", IN_DATE);		
        // 입차시간
        params.put("IN_TIME", IN_TIME);		
        
        parkOutDay = parkOutDay.replaceAll(" ", "").replaceAll("/", "").replaceAll("-", "").replaceAll(":", "").trim();
        String OUT_DATE = "";
        String OUT_TIME = "";
		if(parkOutDay.length() >= 14){
			OUT_DATE = parkOutDay.substring(0, 4) 
					 + "-" + parkOutDay.substring(4, 6) 
					 + "-" + parkOutDay.substring(6, 8);
			OUT_TIME = parkOutDay.substring(8, 10)
					 + ":" + parkOutDay.substring(10, 12) 
					 + ":" + parkOutDay.substring(12, 14);
		} else if(parkInDay.length() >= 12){
			OUT_DATE = parkOutDay.substring(0, 4) 
					 + "-" + parkOutDay.substring(4, 6) 
					 + "-" + parkOutDay.substring(6, 8);
			OUT_TIME = parkOutDay.substring(8, 10)
					 + ":" + parkOutDay.substring(10, 12) 
					 + ":" + "00";
		} else {
			MsgUtil.ToastMessage(this, "출차시간 형식이 잘못되었습니다.");
			return;
		}
        		
        // 출차일자
        params.put("OUT_DATE",  OUT_DATE);
        // 출차시간
        params.put("OUT_TIME",  OUT_TIME);
        
        // 할인코드 
        params.put("DIS_CD", cch.findCodeByKnCode("DC", discountType));
        
        showProgressDialog(false);
		String url = Constants.API_SERVER;
		Log.d(TAG, " apiParkIoUpdateCall url >>> " + url + " params ::: " + params);
		
		AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>();        
		cb.url(url).params(params) .type(JSONObject.class).weakHandler(this, "parkIoUpdateCallback").redirect(true).retry(3).fileCache(false).expire(-1);
		AQuery aq = new AQuery(this);
		aq.ajax(cb);		
		
	}
	
	public void parkIoUpdateCallback(String url, JSONObject json, AjaxStatus status){
		Log.d(TAG, " parkUpdateCallback  json ====== " +json);
		closeProgressDialog();
		// successful ajax call          
        if(json != null){   
           String KN_RESULT = json.optString("KN_RESULT");
           if("1".equals(json.optString("CD_RESULT"))){
        	   MsgUtil.ToastMessage(this, KN_RESULT);
           } else {
        	   MsgUtil.ToastMessage(this, KN_RESULT);
           }
        } else {
        	MsgUtil.ToastMessage(this, Constants.DATA_FAIL, Toast.LENGTH_LONG);
        }
	}				
	
	protected void apiParkFeeCancelCall(String pioNum) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(Constants.METHOD, Constants.INTERFACEID_PARK_FEE_CANCEL_UPDATE);
		// format
		params.put(Constants.API_FORMAT, Constants.JSON_FORMAT);		
		// 주차번호 
		params.put("PIO_NUM", 	pioNum);
		// 주차장코드
		params.put("CD_PARK", 	parkCode);
		// 사번코드
		params.put("EMP_CD", 	empCode);		
		showProgressDialog(false);
		String url = Constants.API_SERVER;
		Log.d("111", " parkFeeCancelUpdateResult url >>> " + url + " params ::: " + params);
		
		AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>();        
		cb.url(url).params(params) .type(JSONObject.class).weakHandler(this, "parkFeeCancelUpdateCallback").redirect(true).retry(3).fileCache(false).expire(-1);
		AQuery aq = new AQuery(this);
		aq.ajax(cb);		
		
	}
	
	public void parkFeeCancelUpdateCallback(String url, JSONObject json, AjaxStatus status){
		Log.d("111", " parkTypeChangeUpdateCallback  json ====== " +json);
		closeProgressDialog();
		// successful ajax call          
		if(json != null){   
			String KN_RESULT = json.optString("KN_RESULT");
			if("1".equals(json.optString("CD_RESULT"))){
				MsgUtil.ToastMessage(this, KN_RESULT);
				getSelectListDialogData(Constants.COMMON_TYPE_PARK_FEE_CANCEL, "");
				//finish();
			} else {
				MsgUtil.ToastMessage(this, KN_RESULT);
			}
		} else {
			MsgUtil.ToastMessage(this, Constants.DATA_FAIL, Toast.LENGTH_LONG);
		}
	}	
	
	protected void apiParkTypeChangeUpdateCall(String pioNum, PayVo payVo, Bundle extras) {
	
		if(payVo == null){
			payVo = new PayVo(); 
			payVo.setCashAmt("0");			
			payVo.setCardAmt("0");
		}
		
		Map<String, Object> params = new HashMap<String, Object>();
		
		// method 셋팅...
		params.put(Constants.METHOD, Constants.INTERFACEID_PARK_TYPE_CHANGE_UPDATE);
		// format
		params.put(Constants.API_FORMAT, Constants.JSON_FORMAT);		
		// 주차번호 
		params.put("PIO_NUM", 	pioNum);
		// 주차장코드
		params.put("CD_PARK", 	parkCode);
		// 사번코드
		params.put("EMP_CD", 	empCode);        
		
		// 입차일자
		params.put("IN_DATE",   extras.get("IN_DATE"));		
		// 입차시간
		params.put("IN_TIME",   extras.get("IN_TIME"));		
		
		// 출차일자
		params.put("OUT_DATE",  extras.get("OUT_DATE"));
		// 출차시간
		params.put("OUT_TIME",  extras.get("OUT_TIME"));
		
		// 할인코드 
		params.put("DIS_CD", 	extras.get("DISCOUNT_TYPE"));
		// 요금종류 
		params.put("CHARGE_TYPE", extras.get("CHARGE_TYPE"));
		
        String parkType   = extras.get("PARK_TYPE").toString();
        
        
		// 주차종류 
		params.put("PARK_TYPE", parkType);
		
		if(parkType.equals("01")){  // 시간권(후불)은  출차등록 시 금액 계산
			// r주차원금
			params.put("REAL_AMT", 0);
			// 현금 금액
			params.put("CASH_AMT", 0);
			// 카드 금액
			params.put("CARD_AMT", 0);
			// 선납 금액
			params.put("ADV_AMT",  0);
			// 할인 금액
			params.put("DIS_AMT",  0);
			// 절삭 금액         
			params.put("TRUNC_AMT", 0);	
			// 선불권 처리 방법
			params.put("ADV_METHOD", "");			
		} else {
			// 주차원금
			params.put("REAL_AMT",    	extras.get("parkAmt"));
			
			// 할인금액
			params.put("DIS_AMT",    	extras.get("discountAmt"));
			
			// 절삭금액
			params.put("TRUNC_AMT",    	extras.get("truncAmt"));

			int cardAmt = 0;
			int cashAmt = 0;

			//String prePayment = (cardAmt > 0 ? ""+cardAmt : ""+cashAmt);
			int coupon = Integer.valueOf(payVo.getCouponAmt());
			if(coupon > 0)
			{
				if(Util.sToi(payVo.getCardAmt()) > 0) {
					cardAmt = Util.sToi(payVo.getCardAmt());
				}

				cashAmt = Util.sToi(payVo.getCashAmt());
				if(cashAmt > coupon) {
					cashAmt = cashAmt - coupon;
				} else {
					cashAmt = 0;
				}
			}
			else
			{
				cardAmt = Util.sToi(payVo.getCardAmt());

				if(cardAmt > 0) {
					if (couponData != null) {
						if (couponData.length() > 0) {
							String[] arr = couponData.split("\\^");
							for (int i = 0; i < arr.length; i++) {
								if (arr[i].length() > 0) {
									String[] temp = arr[i].split("\\$");
									String temp_amt = Util.isNVL(temp[3], "0");
									if (temp_amt.length() > 0) {
										temp_amt = temp_amt.substring(temp_amt.indexOf("=") + 1, temp_amt.length());
										coupon += Util.sToi(temp_amt);
									}
								}
							}
						}
					}
				}
				cashAmt = Util.sToi(payVo.getCashAmt());
			}

			String prePayment = (cardAmt > 0 ? ""+(cardAmt+coupon) : ""+(cashAmt+coupon));
			
			// 현금 금액
			params.put("CASH_AMT", ""+cashAmt);
			// 카드 금액
			params.put("CARD_AMT", ""+cardAmt);
			// 선납 금액
			params.put("ADV_AMT", prePayment);

			String real_amt = "0";
			//쿠폰금액이 주차원금을 넘었을 경우 쿠폰금액을 주차원금으로... Yoon-1.09
			if(Integer.valueOf(cashAmt) >0)
			{
				real_amt = String.valueOf(cashAmt);
			}
			else
			{
				real_amt = String.valueOf(cardAmt);
			}

			if(payVo.getCouponAmt() != "")
			{
				if(Integer.valueOf(real_amt) > 0) {
					if(Integer.valueOf(real_amt) < Integer.valueOf(payVo.getCouponAmt())) {
						params.put("COUPON_AMT", 	 real_amt);
					} else {
						params.put("COUPON_AMT", 	 payVo.getCouponAmt());
					}
				} else {
					params.put("COUPON_AMT", 	 payVo.getCouponAmt());
				}
			}
			params.put("COUPON_DATA", 	 couponData);


	        // 현금 영수증 관련
	        params.put("CASH_APPROVAL_NO", 	 payVo.getCashApprovalNo());
	        params.put("CASH_APPROVAL_DATE", payVo.getCashApprovalDate());
	        params.put("CATNUMBER",   	 	 payVo.getCatNumber());			
	        
	        // 카드 결제 관련
	        params.put("TRANSACTION_NO", 	payVo.getTransactionNo());
	        params.put("APPROVAL_NO",    	payVo.getApprovalNo());
	        params.put("APPROVAL_DATE",  	payVo.getApprovalDate());
	        params.put("CARD_NO",     		payVo.getCardNo());
	        params.put("CARD_COMPANY", 	    payVo.getCardCompany());
			
			// 선불권 처리 방법
			params.put("ADV_METHOD", "ADV0");			
		}
		
		showProgressDialog(false);
		String url = Constants.API_SERVER;
		Log.d(TAG, " apiParkTypeChangeUpdateCall url >>> " + url + " params ::: " + params);
		
		AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>();        
		cb.url(url).params(params) .type(JSONObject.class).weakHandler(this, "parkTypeChangeUpdateCallback").redirect(true).retry(3).fileCache(false).expire(-1);
		AQuery aq = new AQuery(this);
		aq.ajax(cb);		
		
	}
	
	public void parkTypeChangeUpdateCallback(String url, JSONObject json, AjaxStatus status){
		Log.d(TAG, " parkTypeChangeUpdateCallback  json ====== " +json);
		closeProgressDialog();
		// successful ajax call          
		if(json != null){   
			String KN_RESULT = json.optString("KN_RESULT");
			if("1".equals(json.optString("CD_RESULT"))){
				MsgUtil.ToastMessage(this, KN_RESULT);
				finish();
			} else {
				MsgUtil.ToastMessage(this, KN_RESULT);
			}
		} else {
			MsgUtil.ToastMessage(this, Constants.DATA_FAIL, Toast.LENGTH_LONG);
		}
	}				

	protected void apiParkOutCall(ExitPaymentData epd, String parkType, String parkPosition, String discountType
								, String realAmt, String paymentAmt, String discountAmt, String truncAmt, int ADJ_AMT){
		
		if (couponData != null &&  couponData.length() > 1) {
			out.setCouponData(couponData);
		}

		String parkTypeCode     = cch.findCodeByKnCode("PY", parkType);
		String discountTypeCode = cch.findCodeByKnCode("DC", discountType);
		
		out.setTruncAmt(truncAmt);
		out.setDicAmt(discountAmt);
		out.setRealAmt(realAmt);
		
		String cashApprovalNo   = "";
		String cashApprovalDate = "";
		if (epd != null) {
			out.setTransactionNo(epd.getTransactionNo());
			out.setApprovalNo(epd.getApprovalNo());
			out.setApprovalDate(epd.getApprovalDate());
			out.setCardNo(epd.getCardNo());
			out.setCardCompany(epd.getCardCompany());
			out.setCardAmt(epd.getCardAmt());
			out.setCashAmt(epd.getCashAmt());
			out.setReturnAmt(epd.getReturnAmt());
			out.setCouponAmt(epd.getCouponAmt());
			
			cashApprovalNo   = epd.getCashApprovalNo();
			cashApprovalDate = epd.getCashApprovalDate();
			out.setCatnumber(epd.getCatNumber());
		}
		
		/**xx 2015.02.03
		int offAmt =  Util.sToi(out.getRealAmt(),0) - Util.sToi(out.getCashAmt(),0)   - Util.sToi(out.getCardAmt(),0) 
				    - Util.sToi(out.getDicAmt(),0)  - Util.sToi(out.getReturnAmt(),0) - Util.sToi(out.getYetAmt(),0)  
				    - Util.sToi(out.getYetReturn(),0) - Util.sToi(out.getYetReturnCard(),0) - truncAmt;
		**/
		
        Map<String, Object> params = new HashMap<String, Object>();
        params.put(Constants.METHOD, 	 Constants.INTERFACEID_PARK_OUT);
        params.put(Constants.API_FORMAT, Constants.JSON_FORMAT);		
        params.put("PIO_NUM", 	out.getPioNumber());
        params.put("CD_PARK", 	out.getParkCode());
        params.put("EMP_CD", 	out.getEmpCode());
        params.put("WORK_DAY",  out.getWorkDay());
        
        // 출차일
        params.put("OUT_DATE",  Util.getYmdhms("yyyy-MM-dd"));
        // 출차시간
        params.put("OUT_TIME",  out.getOutTime());
        // 차량번호 
        params.put("CAR_NO", 	out.getCarNumber());
        // 주차종류 
        params.put("PARK_TYPE", parkTypeCode);
        // 연락처
        params.put("TEL", 	    out.getTel());
        // 주차면
        params.put("CD_AREA",   parkPosition);

        // 요금종류
        //params.put("CHARGE_TYPE", cch.findCodeByKnCode("AM2", out.getChargeTypeStr()));
        
		if("2".equals(parkClass)){
			params.put("CHARGE_TYPE", cch.findCodeByKnCode("AM2", out.getChargeTypeStr()));
		} else if("3".equals(parkClass)){
			params.put("CHARGE_TYPE", cch.findCodeByKnCode("AM3", out.getChargeTypeStr()));
		} else if("4".equals(parkClass)){
			params.put("CHARGE_TYPE", cch.findCodeByKnCode("AM4", out.getChargeTypeStr()));
		} else if("5".equals(parkClass)){
			params.put("CHARGE_TYPE", cch.findCodeByKnCode("AM5", out.getChargeTypeStr()));
		} else if("6".equals(parkClass)){
			params.put("CHARGE_TYPE", cch.findCodeByKnCode("AM6", out.getChargeTypeStr()));
		}

        params.put("CAR_OWNER",	out.getCarOwner());
        params.put("STATE_CD",  out.getStateCode());
        params.put("YET_CD",    out.getYetCode());
        
        // 현금 영수증 관련
        params.put("CASH_APPROVAL_NO", 	 cashApprovalNo);
        params.put("CASH_APPROVAL_DATE", cashApprovalDate);
        params.put("CATNUMBER",   	 	 out.getCatnumber());
        
        // 카드 결제 관련
        params.put("TRANSACTION_NO", 	out.getTransactionNo());
        params.put("APPROVAL_NO",    	out.getApprovalNo());
        params.put("APPROVAL_DATE",  	out.getApprovalDate());
        params.put("CARD_NO",     		out.getCardNo());
        params.put("CARD_COMPANY", 	    out.getCardCompany());
        
        params.put("REAL_AMT",    		out.getRealAmt());
        params.put("CASH_AMT",    	 	out.getCashAmt());

		if (couponData != null &&  couponData.length() > 1) {
			int coupon = 0;
			String[] arr = couponData.split("\\^");
			for (int i = 0; i < arr.length; i++) {
				if (arr[i].length() > 0) {
					String[] temp = arr[i].split("\\$");
					String temp_amt = Util.isNVL(temp[3], "0");
					if (temp_amt.length() > 0) {
						temp_amt = temp_amt.substring(temp_amt.indexOf("=") + 1, temp_amt.length());
						coupon += Util.sToi(temp_amt);
					}
				}
			}
			String cardPrice = String.valueOf(Integer.valueOf(out.getCardAmt()) - coupon);
			params.put("CARD_AMT",    	 	cardPrice);
		} else {
			params.put("CARD_AMT",    	 	out.getCardAmt());
		}

		if("04".equals(parkTypeCode)){
			int payAmt = Util.sToi(paymentAmt);
			if(payAmt < 0){
				// 환불금액 발생 
				params.put("ADV_METHOD", "ADV1");
			} else if(payAmt > 0){
				// 추가요금 발생 
				params.put("ADV_METHOD", "ADV2");
			} else if(payAmt == 0){
				String disRate = cch.findCodeByDisRate("DC", discountType);
				if("100".equals(disRate)){
					// 선불권 할인률 100% 적용됨
					params.put("ADV_METHOD", "ADV4");
				} else {
					// 선불권 차량 추가발생금없이 0원 출(입)차 
					params.put("ADV_METHOD", "ADV3");
				}				
			}
		}        
        
        params.put("DIS_CD",  	  discountTypeCode);
        params.put("DIS_AMT", 	  out.getDicAmt());
        params.put("RETURN_AMT",  out.getReturnAmt());


		String unpayAmt =  Preferences.getValue(mContext, Constants.PREFERENCE_UNPAY_AMT, "0");
		if(!"0".equals(unpayAmt)&&!"".equals(unPayPinNums.toString())){
			params.put("UNPAY_PIO_NUM", unPayPinNums);
			params.put("UNPAY_DIS_AMT", discountAmt);
			params.put("UNPAY_DIS_CD", discountTypeCode);
			params.put("UNPAY_PARK_OUT", out.getOutTime());
		}


        if("O2".equals(out.getStateCode())){
        	params.put("YET_AMT", out.getYetAmt());
        }else{
        	params.put("YET_AMT", "");
        }
        
        params.put("TRUNC_AMT",      out.getTruncAmt());
        
//      xx 2015.02.03
//      params.put("OFF_AMT",        offAmt);
        
        params.put("YET_RETURN",     out.getYetReturn());
        params.put("YET_RETURNCARD", out.getYetReturnCard());
        //Log.d("COUPON_AMT---------", out.getCouponAmt());
        Log.d("111", out.getCouponData());
        params.put("ADJ_AMT", 	String.valueOf(ADJ_AMT));
        //쿠폰금액이 주차원금을 넘었을 경우 쿠폰금액을 주차원금으로... Yoon-1.09
        
        if(out.getCouponAmt() != "")
        {
        	if(Integer.valueOf(out.getRealAmt()) > 0) {
		        if(Integer.valueOf(out.getRealAmt()) < Integer.valueOf(out.getCouponAmt())) {
		        	params.put("COUPON_AMT", 	 out.getRealAmt());
		        } else {
		        	params.put("COUPON_AMT", 	 out.getCouponAmt());
		        }
        	} else {
        		params.put("COUPON_AMT", 	 out.getCouponAmt());
        	}
        }
        params.put("COUPON_DATA", 	 out.getCouponData());
        
        params.put("KN_COMMENT", 	 out.getComment());
        params.put("PICTURE_DEL", 	(out.isDeletePicture() ? "Y" : "N"));    
        
        
        showProgressDialog(false);
		String url = Constants.API_SERVER;
		Log.d(TAG, " parkOutCallback  url ====== " +url+"  "+params);
		AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>();        
		cb.url(url).params(params) .type(JSONObject.class).weakHandler(this, "parkOutCallback").redirect(true).retry(3).fileCache(false).expire(-1);
		AQuery aq = new AQuery(this);
		aq.ajax(cb);		
		
	}		
	
	public void parkOutCallback(String url, JSONObject json, AjaxStatus status){
		
		closeProgressDialog();
		Log.d(TAG, " parkOutCallback  json ====== " +json);
		
		// successful ajax call          
        if(json != null){   
           String KN_RESULT = json.optString("KN_RESULT");
           if("1".equals(json.optString("CD_RESULT"))){
        	  MsgUtil.ToastMessage(this, KN_RESULT);
        	  requestApi(Constants.INTERFACEID_PARK_OUT, null);	
           } else {
        	   MsgUtil.ToastMessage(this, KN_RESULT);
           }
        } else {
    	    MsgUtil.ToastMessage(this, Constants.DATA_FAIL, Toast.LENGTH_LONG);
        }
		
	}	
	
	
	/**
	 * 차량 미납 조회
	 */
	protected void apiUnpayAttach(String carNo){
		showProgressDialog(false);
		String url = Constants.API_SERVER + "?" + Constants.METHOD +"="+ Constants.INTERFACEID_PARK_UNPAYATTACH_V3;
		String param = "&CD_PARK="+parkCode
					 + "&CAR_NO="+carNo
					 + "&"+Constants.API_FORMAT+"="+Constants.JSON_FORMAT;
		
		Log.d(TAG, " apiUnpayAttach url >>> " + url + param);
		
		AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>();        
		cb.url(url + param).type(JSONObject.class).weakHandler(this, "unpayAttachCallback").redirect(true).retry(3).fileCache(false).expire(-1);
		AQuery aq = new AQuery(this);
		aq.ajax(cb); 		
	}		
	
	public void unpayAttachCallback(String url, JSONObject json, AjaxStatus status){
		unPayPinNums = new StringBuffer();
		closeProgressDialog();
		Log.d(TAG, " unpayAttachCallback  json ====== " +json);
		
		JSONArray jsonArr = null;
		
		int unpayAmt = 0;
		
		// successful ajax call          
		if(json != null){   
			String KN_RESULT = json.optString("KN_RESULT");
			if("1".equals(json.optString("CD_RESULT"))){
			   jsonArr = json.optJSONArray("RESULT");
			   int Len = jsonArr.length();
			   try {
				   for(int i=0; i<Len; i++){
					    JSONObject res = jsonArr.getJSONObject(i);
						String CD_STATE = res.optString("CD_STATE");
						if ("Y".equalsIgnoreCase(CD_STATE)) {
							Toast.makeText(this, "압류 대상 차량 입니다.", Toast.LENGTH_LONG).show();
						}


					   if(i==0){
						   unPayPinNums.append(res.optString("PIN_NUM"));
					   }else {
						   unPayPinNums.append(",").append(res.optString("PIN_NUM"));
					   }


						unpayAmt += res.optInt("AMT",0);
				    } // end for 
				   
				} catch (JSONException e) {
				   System.out.println("JSONException 예외 발생");
				}			   
			}
		} else {
			MsgUtil.ToastMessage(this, Constants.DATA_FAIL, Toast.LENGTH_LONG);
		}
		
		requestApi(Constants.INTERFACEID_PARK_UNPAYATTACH_V2, unpayAmt+"원");
		
	}				
	
	protected void apiParkIOPictureCall(String pioNum, String cdPark){
			String url = Constants.API_SERVER + "?" + Constants.METHOD +"="+ Constants.INTERFACEID_PARK_IO_PICTURE;
			String param = "&PIO_NUM="+pioNum
						 + "&CD_PARK="+cdPark
						 + "&"+Constants.API_FORMAT+"="+Constants.JSON_FORMAT;
	        Log.d(TAG, " apiParkIOPictureCall url >>> " + url + param);
	        
			AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>();        
			cb.url(url + param).type(JSONObject.class).weakHandler(this, "parkIOPicCallback").redirect(true).retry(3).fileCache(false).expire(-1);
			AQuery aq = new AQuery(this);
			aq.ajax(cb); 	        
	}		
	
	public void parkIOPicCallback(String url, JSONObject json, AjaxStatus status){
		Log.d(TAG, " parkIOPicCallback  json ====== " +json);
		
		JSONArray jsonArr = null;
		
		String pictureURL = "";
		
		// successful ajax call          
        if(json != null){   
           if("1".equals(json.optString("CD_RESULT"))){
			  jsonArr = json.optJSONArray("PICTURE_LIST");
			  int Len = jsonArr.length();
			  for(int i=0; i<Len; i++){
				  try {
					JSONObject res = jsonArr.getJSONObject(i);
					String filePath = res.optString("FILE_PATH");
					String fileName = res.optString("FILE_NAME");
					if( i == 0){
						pictureURL = Constants.SERVER_HOST + filePath + fileName;
						Log.d(TAG, " pictureURL >>> " + pictureURL);
						break;
					}
				 } catch (JSONException e) {
					  System.out.println("JSONException 예외 발생");
				 }
			  }
           }
        } else {
    	    Toast.makeText(this, Constants.DATA_FAIL, Toast.LENGTH_LONG).show();
        }		
        
        
        requestApi(Constants.INTERFACEID_PARK_IO_PICTURE, pictureURL);
	}	

	protected void apiParkUnpayListCall(String unPayType, String carNo, String cdPark, String startDay, String EndDay ){
		showProgressDialog();
		
		String startDate = startDay;
		String endDate   = EndDay;
		
		if(Util.isEmpty(carNo)){
			carNo = Util.getEncodeStr("%");
		} else {
			startDate = "20010101";
		}
		
		String url = Constants.API_SERVER + "?" + Constants.METHOD +"="+ Constants.INTERFACEID_PARKUNPAY_INFO;
		String param = "&CAR_NO="+carNo
					 + "&CD_GUBUN="+unPayType
					 + "&CD_PARK="+parkCode
					 + "&START_DATE="+startDate
					 + "&END_DATE="+endDate
					 + "&"+Constants.API_FORMAT+"="+Constants.JSON_FORMAT;
		Log.d(TAG, " apiParkUnpayListCall url >>> " + url + param);
		
		AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>();        
		cb.url(url+param).type(JSONObject.class).weakHandler(this, "parkUnpayListCallback").redirect(true).retry(3).fileCache(false).expire(-1);
		AQuery aq = new AQuery(this);
		aq.ajax(cb); 		
	}		
	
	public void parkUnpayListCallback(String url, JSONObject json, AjaxStatus status){

		Log.d("333", "33333-------------------------");
		Log.d(TAG, " parkUnpayListCallback  json ====== " +json);
		
		ArrayList<UnPayManagerItem> unPayList = new ArrayList<UnPayManagerItem>();
		
		// successful ajax call          
	    if(json != null){   
	       String KN_RESULT = json.optString("KN_RESULT");
	       if("1".equals(json.optString("CD_RESULT"))){
	    	   
	    	 JSONArray jsonArr = json.optJSONArray("RESULT");
	    	 int Len = jsonArr.length();
	    	  
	    	 for(int i=0; i<Len; i++){
    		   try {
				  JSONObject res = jsonArr.getJSONObject(i);
				  UnPayManagerItem info = new UnPayManagerItem(
											  res.optString("PIO_NUM")
											, res.optString("CD_PARK")
											, res.optString("KN_PARK")
											, res.optString("PARK_IN")
											, res.optString("PARK_OUT")
											, res.optString("AMT")
											, res.optString("PIO_DAY")
											, res.optString("DIS_CD")
											, res.optString("PARK_TYPE")
											, res.optString("CD_GUBUN")
											, res.optString("ADD_AMT")
											, res.optString("SUM_AMT")
											, res.optString("CD_STATE")
											, res.optString("CAR_STATE")
											, res.optString("KN_RESULT")
											, res.optString("CAR_NO")
											, res.optString("PARK_INOUT")
											, res.optString("SUBMIT_AMT")
											, res.optString("SUBMIT_DATE")
											, res.optString("OFF_AMT")
											, res.optString("CHANGE_DIS")
											, res.optString("GROUP_CD"));
										
				      unPayList.add(info);
				  
				 } catch (JSONException e) {
				   System.out.println("JSONException 예외 발생");
				 }
	    		 
	    	  } // end for

	       } else {
	    	   MsgUtil.ToastMessage(this, KN_RESULT, Toast.LENGTH_LONG);
	       }
	    } else {
	    	MsgUtil.ToastMessage(this, Constants.DATA_FAIL, Toast.LENGTH_LONG);
	    }
	    
	    closeProgressDialog();
	    
	    requestApi(Constants.INTERFACEID_PARKUNPAY_INFO, unPayList);	
		
	}				
	
	
	private String mParkType 	 = "";
	private String mDiscountType = "";
	private String mParkMin 	 = "";
	
	protected void parkTypeChangeDialog(final String cdpark,final String chargeType, final String discountType, final String parkType
									  , final String startTime, final String endTime
									  , final ReceiptType13Vo	type13Vo) {
		
//		final Dialog parkTypeChangeDialog;
		
			parkTypeChangeDialog = new Dialog(this, android.R.style.Theme_Holo_Light_Dialog);

		parkTypeChangeDialog.setContentView(R.layout.dialog_change_park_type);
		parkTypeChangeDialog.setTitle("주차종류 변경");
		
		final TextView chg_tvStime        = (TextView) parkTypeChangeDialog.findViewById(R.id.chg_tvStime);
		final TextView chg_tvEtime        = (TextView) parkTypeChangeDialog.findViewById(R.id.chg_tvEtime);
		CheckBox chg_checkTime  	      = (CheckBox) parkTypeChangeDialog.findViewById(R.id.chg_checkTime);
		final TextView chg_tvParkType 	  = (TextView) parkTypeChangeDialog.findViewById(R.id.chg_tvParkType);
		final TextView chg_tvParkTime     = (TextView) parkTypeChangeDialog.findViewById(R.id.chg_tvParkTime);
		final TextView chg_tvChargeType  		  = (TextView) parkTypeChangeDialog.findViewById(R.id.chg_tvChargeType);
		final TextView chg_tvDiscountType = (TextView) parkTypeChangeDialog.findViewById(R.id.chg_tvDiscountType);
		final TextView chg_tvParkAmt  	  = (TextView) parkTypeChangeDialog.findViewById(R.id.chg_tvParkAmt);
		
		Button btn_ok 	  = (Button)   parkTypeChangeDialog.findViewById(R.id.btn_ok);
		Button btn_cancel = (Button)   parkTypeChangeDialog.findViewById(R.id.btn_cancel);
		
		chg_tvChargeType.setText(chargeType);
		mDiscountType = discountType;
		chg_tvDiscountType.setText(mDiscountType);
		String parkTypeCode = cch.findCodeByKnCode("PY", parkType);
		if("01".equals(parkTypeCode)){  // 시간권 후불 -> 시간권 선불 
			chg_tvParkType.setText(cch.findCodeByCdCode("PY", "04"));
			chg_tvParkType.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					
					/* YOON-1.09 : 후불->선불 변경 시 주차종류 변경 금지 
					 * return 추가
					 * setCommonDialog 주석처리 */
					//return;
					//setCommonDialog("주차종류","PY", Constants.COMMON_TYPE_PARK_TYPE, chg_tvParkType, chg_tvParkAmt);
					setCommonDialog_parkType("주차종류","PY", Constants.COMMON_TYPE_PARK_TYPE, chg_tvParkType, chg_tvParkAmt);
				}
			});
		} else { // 시간권 선불 -> 시간권 후불 
			chg_tvParkType.setText(cch.findCodeByCdCode("PY", "01"));
		}
		mParkType = chg_tvParkType.getText().toString();
	//	Log.d(TAG, " mParkType >>> " + mParkType);
	//	Log.d(TAG, " mDiscountType >>> " + mDiscountType);
		
		String inDate  = startTime.replaceAll("-", "").replaceAll(" ", "").replaceAll(":", "").trim();
		String outDate = endTime.replaceAll("-", "").replaceAll(" ", "").replaceAll(":", "").trim();
		if(inDate.length() < 12 || outDate.length() < 12){
			MsgUtil.ToastMessage(this, "시간 형식이 잘못되었습니다.");
			return;
		}
		
		/* Yoon-1.09 : 후불->선불 변경 시 주차시간 기본 1시간으로 세팅
		 * 시작
		 * */
		
		String hStartTime = startTime.substring(11, 16);
		int hour = Integer.parseInt(hStartTime.substring(0, 2))+1;
		String min = startTime.substring(14, 16);
		String endTime2 = String.valueOf((hour < 10 ? "0" + hour : hour))+":"+ min;
		int nEndTime = Integer.parseInt(endTime2.replaceAll(":", ""));
		int nwEndTime = Integer.parseInt(workEndTime);
		Log.i(TAG, "endTime : " + endTime + ", nwEndTime : " + nwEndTime);
		if (nEndTime > nwEndTime) {
			endTime2 = workEndTime.substring(0, 2) + ":" + workEndTime.substring(2);
		}
		String inDay   = inDate.substring(0,8);
		String inTime  = inDate.substring(8,10)+":"+inDate.substring(10,12);
		String outDay  = outDate.substring(0,8);
		String outTime = outDate.substring(8,10)+":"+outDate.substring(10,12);
		
		chg_tvStime.setText(inTime);
		//chg_tvEtime.setText(outTime);
		//chg_tvEtime.setTag(outTime);

		//변경버튼 클릭 시 마감체크박스가 체크되고, 마감시간 Yoon
		//chg_tvEtime.setText(endTime2);
		//chg_tvEtime.setTag(endTime2);
		chg_checkTime.setChecked(true);
		chg_tvEtime.setText(workEndTime.substring(0,2) + ":" + workEndTime.substring(2));
		chg_tvEtime.setTag(endTime2);
		
		/* 끝 */
		
		minusTimeCheck(chg_tvStime.getText().toString().replace(":",""), chg_tvEtime.getText().toString().replace(":",""),chg_tvParkTime);
		mParkMin = chg_tvParkTime.getTag().toString().replaceAll("분","");
		
		setParkAmtChange(chg_tvParkAmt);		
		
		chg_checkTime.setOnCheckedChangeListener(new android.widget.CheckBox.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if (isChecked) {
					String end_time = workEndTime.substring(0,2) + ":" + workEndTime.substring(2);
					chg_tvEtime.setText(end_time);
				} else {
					chg_tvEtime.setText(chg_tvEtime.getTag().toString());
				}
				
				minusTimeCheck(chg_tvStime.getText().toString().replace(":",""), chg_tvEtime.getText().toString().replace(":",""),chg_tvParkTime);
				mParkMin = chg_tvParkTime.getTag().toString().replaceAll("분","");
				
				setParkAmtChange(chg_tvParkAmt);		
			}
		});
		
		chg_tvChargeType.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				//setCommonDialog("요금종류","AM2", Constants.COMMON_TYPE_POPUP_CHARGE_TYPE, chg_tvChargeType, chg_tvParkAmt);
				
				if("2".equals(parkClass)){
					setCommonDialog("요금종류","AM2", Constants.COMMON_TYPE_POPUP_CHARGE_TYPE, chg_tvChargeType, chg_tvParkAmt);
				} else if("3".equals(parkClass)){
					setCommonDialog("요금종류","AM3", Constants.COMMON_TYPE_POPUP_CHARGE_TYPE, chg_tvChargeType, chg_tvParkAmt);
				} else if("4".equals(parkClass)){
					setCommonDialog("요금종류","AM4", Constants.COMMON_TYPE_POPUP_CHARGE_TYPE, chg_tvChargeType, chg_tvParkAmt);
				} else if("5".equals(parkClass)){
					setCommonDialog("요금종류","AM5", Constants.COMMON_TYPE_POPUP_CHARGE_TYPE, chg_tvChargeType, chg_tvParkAmt);
				} else if("6".equals(parkClass)){
					setCommonDialog("요금종류","AM6", Constants.COMMON_TYPE_POPUP_CHARGE_TYPE, chg_tvChargeType, chg_tvParkAmt);
				}
				
			}
		});
		
		chg_tvDiscountType.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(mParkType.equals("일일권")) {
					setCommonDialog_DAY("요금할인", "DC", Constants.COMMON_TYPE_CHARGE_DISCOUNT_CHANGE, chg_tvDiscountType, chg_tvParkAmt);
				} else {
					setCommonDialog("요금할인", "DC", Constants.COMMON_TYPE_CHARGE_DISCOUNT_CHANGE, chg_tvDiscountType, chg_tvParkAmt);
				}
			}
		});


		
		chg_tvStime.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				/* 	YOON-1.09 : 입차시간 수정 할 수 없도록
				 * return 추가  
				 * DialogChangeTimePicker 주석처리 */
				return;
				//DialogChangeTimePicker(chg_tvStime, chg_tvEtime, chg_tvParkTime, chg_tvParkAmt, 1);
			}
		});
		
		chg_tvEtime.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				DialogChangeTimePicker(chg_tvStime, chg_tvEtime, chg_tvParkTime, chg_tvParkAmt, 2);
			}
		});
		
		btn_ok.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				try {
					// 할인금액 조회
					String disRate = Util.isNVL(cch.findCodeByDisRate("DC", mDiscountType), "0");
					if (!"0".equals(disRate) && !is4picSave(type13Vo.getPioNum())) {
						goToPictureView(type13Vo.getPioNum(), type13Vo.getCarNum(), Constants.PICTURE_TYPE_DIS, cdpark, empCode);
						Toast.makeText(ExitAbstractActivity.this, "할인을 선택한 경우 사진등록이 필요합니다.", Toast.LENGTH_LONG).show();
						return;
					}
				} catch (NullPointerException e) {
					if(Constants.DEBUG_PRINT_LOG){
						e.printStackTrace();
					}else{
						System.out.println("예외 발생");
					}
				}


				Bundle extras = (Bundle) chg_tvParkAmt.getTag();
				String parkTypeCode = cch.findCodeByKnCode("PY", mParkType);
				
				String chargeTypeCode = "";
				
				if("2".equals(parkClass)){
					chargeTypeCode = cch.findCodeByKnCode("AM2", chg_tvChargeType.getText().toString());
				} else if("3".equals(parkClass)){
					chargeTypeCode = cch.findCodeByKnCode("AM3", chg_tvChargeType.getText().toString());
				} else if("4".equals(parkClass)){
					chargeTypeCode = cch.findCodeByKnCode("AM4", chg_tvChargeType.getText().toString());
				} else if("5".equals(parkClass)){
					chargeTypeCode = cch.findCodeByKnCode("AM5", chg_tvChargeType.getText().toString());
				} else if("6".equals(parkClass)){
					chargeTypeCode = cch.findCodeByKnCode("AM6", chg_tvChargeType.getText().toString());
				}	
				
				String inDate  = startTime.replaceAll("-", "").replaceAll(" ", "").replaceAll(":", "").trim();
				String outDate = endTime.replaceAll("-", "").replaceAll(" ", "").replaceAll(":", "").trim();
				
				String inDay   = inDate.substring(0,8);
				String inTime  = chg_tvStime.getText().toString()+":00";
				String outDay  = outDate.substring(0,8);
				String outTime = chg_tvEtime.getText().toString()+":00";
				
				extras.putString("IN_DATE",  inDay);
				extras.putString("IN_TIME",  inTime);
				extras.putString("OUT_DATE", outDay);
				extras.putString("OUT_TIME", outTime);
				extras.putString("PARK_TYPE", parkTypeCode);
				extras.putString("CHARGE_TYPE", chargeTypeCode);
				Log.d(TAG, " CHARGE_TYPE ::::::: " + chargeTypeCode);
			//	Log.d(TAG, " mDiscountType ::::::: " + mDiscountType);
				String discountTypeCode = cch.findCodeByKnCode("DC", mDiscountType);
			//	Log.d(TAG, " discountTypeCode ::::::: " + discountTypeCode);
				extras.putString("DISCOUNT_TYPE", discountTypeCode);
			//	Log.d(TAG, " extras >>> " +extras);
				
			
				if(parkTypeCode.equals("01")){
					request(PayData.PAY_DIALOG_TYPE_PARK_CHANGE_LATER, extras);
				} else {
					type13Vo.setParkType(mParkType);
					type13Vo.setAmtType(chg_tvChargeType.getText().toString());
					type13Vo.setDisTypeName(mDiscountType);
					type13Vo.setPrepayAmt(extras.get("payment").toString());
					type13Vo.setInTime(inDay+" "+chg_tvStime.getText().toString());
					type13Vo.setOutTime(outDay+" "+chg_tvEtime.getText().toString());
					type13Vo.setTotalMin(chg_tvParkTime.getText().toString());
					request(PayData.PAY_DIALOG_TYPE_PARK_CHANGE_ADV, extras);
					prepayDialog(parkType, extras.get("payment").toString(), type13Vo);
				}
				
				parkTypeChangeDialog.dismiss();
			}
		});

		btn_cancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				MsgUtil.ToastMessage(getApplicationContext(), "취소되었습니다.");
//				request(PayData.PAY_DIALOG_TYPE_PARK_CHANGE_CANCEL, null);
				parkTypeChangeDialog.dismiss();
			}
		});

		parkTypeChangeDialog.show();
		
	}
	
	private void setParkAmtChange(final TextView tvParkAmt){
		
		// 주차원금 초기화
		String parkAmt      = "0";
		
		// 할인금액 초기화
		String discountAmt  = "0";
		
		// 절삭금액 초기화
		String truncAmt     = "0";
		
		
		//###############################################################
		//  STEP 1 : 주차 시간 계산
		//###############################################################
		// Log.d(TAG, "date1 : " + date1 + ", date2 : " + date2);
		String parkMin = mParkMin;
		Log.d(TAG, " parkMin >>> " + parkMin);
		
		
		//###############################################################
		//  STEP 2 : 할인 시간 계산
		//###############################################################
		// 요금할인 정보 
		String discountName = mDiscountType;
		
		// 할인시간 설정
		int disTime  = Util.sToi(cch.findCodeByDisTime("DC", discountName),0);
		if(disTime > 0){
//			Log.d(TAG, "parkMin 2 : " + parkMin);
			parkMin 	 = ""+(Util.sToi(parkMin,0) - disTime);
//			Log.d(TAG, "parkMin 3 : " + parkMin);			
			if((Util.sToi(parkMin,0) < 0)){
//				Log.d(TAG, "parkMin 4 : " + parkMin);
				parkMin = "0";
			}
		}				
		
		
		//###############################################################
		//  STEP 3 : 할인 금액 계산
		//###############################################################
		
		// 주차종류
		String parkTypeCode = cch.findCodeByKnCode("PY", mParkType);
		Log.d(TAG, " setParkAmtChange ::: parkTypeCode  >>> " + parkTypeCode);

		TextView chg_tvChargeType = (TextView) parkTypeChangeDialog.findViewById(R.id.chg_tvChargeType);
		
		// 요금종류코드
		String chargeCode = "";
		
		if("2".equals(parkClass)){
			chargeCode = cch.findCodeByKnCode("AM2", chg_tvChargeType.getText().toString());
		} else if("3".equals(parkClass)){
			chargeCode = cch.findCodeByKnCode("AM3", chg_tvChargeType.getText().toString());
		} else if("4".equals(parkClass)){
			chargeCode = cch.findCodeByKnCode("AM4", chg_tvChargeType.getText().toString());
		} else if("5".equals(parkClass)){
			chargeCode = cch.findCodeByKnCode("AM5", chg_tvChargeType.getText().toString());
		} else if("6".equals(parkClass)){
			chargeCode = cch.findCodeByKnCode("AM6", chg_tvChargeType.getText().toString());
		}
		
		//chargeCode = cch.findCodeByKnCode("AM2", chg_tvChargeType.getText().toString());
		
		// 주차금액 계산
		String total = "0";
//		if(disTime > 0) {
//			total = parkCharge_distime(parkMin, chargeCode, parkTypeCode, disTime);
//		} else {
			total = parkCharge(parkMin, chargeCode, parkTypeCode);
//		}
		String charge = "0";
		
		Log.d(TAG, " setParkAmtChange ::: total >>> " + total);
		
		if ("02".equals(parkTypeCode)) {  // 일일권
			total  	    = ""+dayAmt;	// 일일권은 입차시 일일권 금액으로 셋팅 
			charge 	    = total;		
			parkAmt	    = total;
			discountAmt = "0";			// 일일권은 할인을 하지 않는다.
			truncAmt    = "0";
		} else if ("03".equals(parkTypeCode)) { // 정기권 
			total  		= "0";			// 정기권은 입차시 0원
			charge      = "0";
			parkAmt	    = "0";	     	// 정기권은 할인을 하지 않는다.
			discountAmt = "0";
			truncAmt    = "0";
		} else {	
			// 주차원금 셋팅 
			charge  = total;
			parkAmt = charge;					
			
			int tempDisAmt1 = 0;
			int tempDisAmt2 = 0;
			
			// 할인금액 조회
			String disAmt = Util.isNVL(cch.findCodeByDisAmt("DC", discountName),"0");
			if(!"0".equals(disAmt)){
				// 할인금액 설정
				tempDisAmt1 = Util.sToi(disAmt);
				charge = "" + (Util.sToi(charge) - tempDisAmt1);
				Log.d(TAG, "setParkAmtChange ::: charge >>> " + charge + "  tempDisAmt1 : " + tempDisAmt1);
			}
			
			// 할인률 조회
			String disRate = cch.findCodeByDisRate("DC", discountName);
			if(!"0".equals(disRate)){
				// 할인률에 따른 할인금액 설정
				tempDisAmt2 = Util.sToi(Util.isNVL(""+discountChargeRate(charge, disRate),"0"));
				charge = "" + (Util.sToi(charge) - tempDisAmt2);
				Log.d(TAG, "setParkAmtChange ::: charge >>> " + charge + "  tempDisAmt2 : " + tempDisAmt2);
			}			
			
			// 총 할인금액
			discountAmt = ""+(tempDisAmt1 + tempDisAmt2);
			Log.d(TAG, " setParkAmtChange >>> charge : " + charge + " discountAmt : " + discountAmt);
			
			if(Util.sToi(charge,0) < 0){
				charge = "0";
			}
			
		} // end if parkTypeCode
		
		Log.d(TAG, " setParkAmtChange ::: charge >>> " + charge);
		Log.d(TAG, " setParkAmtChange ::: parkAmt >>> " + parkAmt);		
		Log.d(TAG, " setParkAmtChange ::: discountAmt >>> " + discountAmt);		

		
		//###############################################################
		//  STEP 4 : 납부 금액 계산
		//###############################################################		
		int payment = Util.sToi(charge);
		
		
		//###############################################################
		//  STEP 5 : 절삭 금액 계산
		//###############################################################			

		
		//** 과천시 할인금액이 없으면 절삭 적용
		// 주차할인코드
		/*
		String disCode = cch.findCodeByKnCode("DC", discountName);
		if("01".equals(disCode)){
			payment = payment - Util.sToi(truncAmt);
		} else { // 할인금액이 있으면 올림 적용
			if(Util.sToi(truncAmt) > 0){
				// 납부금액은 절삭 올림 적용
				payment = payment + (Util.sToi(truncUnit) - Util.sToi(truncAmt));
				// 할인금액은 절삭 차감 
				discountAmt = "" + (Util.sToi(discountAmt) - (Util.sToi(truncUnit) - Util.sToi(truncAmt)));
				truncAmt = "0";
			}
		}
		*/

		if (!"0".equals(truncUnit)) {
			truncAmt = "" + payment % Util.sToi(truncUnit);
		} else {
			truncAmt = "0";
		}
		payment = payment - Util.sToi(truncAmt);		
		
		Log.d(TAG, " setParkAmtChange ::: payment >>> " + payment);	
		
		Bundle extras = new Bundle();
		extras.putString("payment",      ""+payment);
		extras.putString("parkAmt",      parkAmt);
		extras.putString("discountAmt",  discountAmt);
		extras.putString("truncAmt",     truncAmt);
		
		tvParkAmt.setTag(extras);  	  
		tvParkAmt.setText(Util.addComma(payment));
		
	}

	private void daySetParkAmt(final TextView tvParkAmt, String discountName)
	{

		String chargeClassCode = "AM2";
		if("2".equals(parkClass)){
			chargeClassCode = "AM2";
		} else if("3".equals(parkClass)){
			chargeClassCode = "AM3";
		} else if("4".equals(parkClass)){
			chargeClassCode = "AM4";
		} else if("5".equals(parkClass)){
			chargeClassCode = "AM5";
		}

		final TextView chg_tvChargeType  		  = (TextView) parkTypeChangeDialog.findViewById(R.id.chg_tvChargeType);
		//final TextView chg_tvDiscountType = (TextView) parkTypeChangeDialog.findViewById(R.id.chg_tvDiscountType);
		//final TextView chg_tvParkAmt  	  = (TextView) parkTypeChangeDialog.findViewById(R.id.chg_tvParkAmt);

		String code = cch.findCodeByKnCode(chargeClassCode, chg_tvChargeType.getText().toString());

		String amt = cch.findCodeValue2(chargeClassCode, code);
		String parkamt = amt;
		//chg_tvDiscountType.setText(mDiscountType);

		int tempDisAmt1 = 0;
		int tempDisAmt2 = 0;

		// 할인금액 조회
		String disAmt = Util.isNVL(cch.findCodeByDisAmt("DC", discountName),"0");
		if(!"0".equals(disAmt)){
			// 할인금액 설정
			tempDisAmt1 = Util.sToi(disAmt);
			amt = "" + (Util.sToi(amt) - tempDisAmt1);
		}

		// 할인률 조회
		String disRate = cch.findCodeByDisRate("DC", discountName);
		if(!"0".equals(disRate)){
			// 할인률에 따른 할인금액 설정
			tempDisAmt2 = Util.sToi(Util.isNVL(""+discountChargeRate(amt, disRate),"0"));
			amt = "" + (Util.sToi(amt) - tempDisAmt2);
		}

		// 총 할인금액
		String discountAmt = ""+(tempDisAmt1 + tempDisAmt2);

		if(Util.sToi(amt,0) < 0){
			amt = "0";
		}

		//chg_tvDiscountType.setText(discountAmt + "원");

		Bundle extras = new Bundle();
		extras.putString("payment",      amt);
		extras.putString("parkAmt",      parkamt);
		extras.putString("discountAmt",  discountAmt);
		extras.putString("truncAmt",     "0");

		tvParkAmt.setTag(extras);
		tvParkAmt.setText(amt + "원");

	}

	
	@Override
	public void getSelectListDialogData(int tag, String data, TextView btnView, Object targetObj) {
		super.getSelectListDialogData(tag, data);
		Log.d(TAG,"getSelectListDialogData() 333 : " + tag + " targetObj >>> " + targetObj);
		
		TextView chg_tvParkType 	  = (TextView) parkTypeChangeDialog.findViewById(R.id.chg_tvParkType);
		TextView chg_tvDiscountType   = (TextView) parkTypeChangeDialog.findViewById(R.id.chg_tvDiscountType);
		TextView chg_tvParkAmt  	  = (TextView) parkTypeChangeDialog.findViewById(R.id.chg_tvParkAmt);
		switch (tag) {
		  case Constants.COMMON_TYPE_CHARGE_DISCOUNT_CHANGE :
			if("일일권".equals(chg_tvParkType.getText().toString())){
				//Toast.makeText(getApplicationContext(), "일일권은 요금 할인이 적용되지 않습니다.", Toast.LENGTH_LONG).show();
				//setCommonItem("DC", Constants.COMMON_TYPE_CHARGE_DISCOUNT, chg_tvDiscountType);
				if(targetObj != null) {
					daySetParkAmt((TextView) targetObj, data);
				}
			}else{
				mDiscountType = data;
				if(targetObj != null){
					setParkAmtChange((TextView) targetObj);
				}
			}
			break;	
		  case Constants.COMMON_TYPE_POPUP_CHARGE_TYPE:	///< 요금종류
				setParkAmtChange((TextView) targetObj);
			break;
				
		  case Constants.COMMON_TYPE_PARK_TYPE:	///< 주차종류

				mParkType = chg_tvParkType.getText().toString();
			  if("일일권".equals(chg_tvParkType.getText().toString())) {
				  daySetParkAmt((TextView) targetObj, data);
			  } else {
				  setParkAmtChange((TextView) targetObj);
			  }
			/**XXX 이전버전 2015.11.03 작업					
			if(data.equals("일일권"))
				setCommonDialog("일일권 요금종류","DDAM", Constants.COMMON_TYPE_DDAM_CHARGE_TYPE, chg_tvParkType, (TextView) targetObj);
			else
				setParkAmtChange((TextView) targetObj);
			**/
			break;
		}
	}
	
	private void DialogChangeTimePicker(final TextView tvStartTime, final TextView tvEndTime, final TextView tvParkTime, final TextView tvParkAmt, final int type) {

		TimePickerDialog.OnTimeSetListener mTimeSetListener = new TimePickerDialog.OnTimeSetListener() {
			public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
				
				String time = Util.addZero(hourOfDay) +":"+ Util.addZero(minute);
				
				int startWorkTime = Integer.parseInt(workStartTime.replace(":", ""));
				int endWorkTime   = Integer.parseInt(workEndTime.replace(":", ""));
				int iTime	      = Integer.parseInt(time.replace(":", ""));
				
				if (iTime < startWorkTime || iTime > endWorkTime) {
					MsgUtil.ToastMessage(getApplicationContext(), ("선택한 시간은 근무시간이 아닙니다.다시 선택해 주세요\n시작시간 : " + workStartTime + ", 종료시간 : " + workEndTime));
					/**
					if(type == 1){
						tvStart.setText(workStartTime.substring(0, 2) + ":" + workStartTime.substring(2));
					} else {
						tvEnd.setText(workEndTime.substring(0, 2) + ":" + workEndTime.substring(2));
					}
					setParkAmtChange(tvParkAmt);
					**/
					return ;
				}
				
				if (type == 1) {
					// 선택한 시작시간이 종료시간보다 클 경우 자동으로 종료 시간을 근무 종료 시간으로 설정
					if (iTime > Util.sToi(tvEndTime.getText().toString().replace(":", "").trim())) {
						tvEndTime.setText(workEndTime.substring(0, 2) + ":" + workEndTime.substring(2));
					}
					
					tvStartTime.setText(time);
				} else {
					// 선택한 종료시간이 시작시간보다 작을 경우 자동으로 시작 시간을 근무 시작 시간으로 설정
					if (iTime < Util.sToi(tvStartTime.getText().toString().replace(":", "").trim())) {
						tvStartTime.setText(workStartTime.substring(0, 2) + ":" + workEndTime.substring(2));
					}
					
					tvEndTime.setText(time);
				}
				
				minusTimeCheck(tvStartTime.getText().toString().replace(":",""), tvEndTime.getText().toString().replace(":",""),tvParkTime);
				mParkMin = tvParkTime.getTag().toString().replaceAll("분","");
				
				setParkAmtChange(tvParkAmt);					
				
			}
		};
		
		String timeStr = "";
		String timeHH  = "";
		String timeMi  = "";
		if (type == 1) {
			timeStr = tvStartTime.getText().toString().replaceAll(":", "");
		} else {
			timeStr = tvEndTime.getText().toString().replaceAll(":", "");
		}		
		timeHH  = timeStr.substring(0, 2);
		timeMi  = timeStr.substring(2);
		
		TimePickerDialog timePicker = new TimePickerDialog(this, R.style.CustomDatePickerDialog,
													 	   mTimeSetListener,
													 	   Integer.parseInt(timeHH), 
													 	   Integer.parseInt(timeMi),  false);
		timePicker.show();
	}

	public boolean is4picSave(String pioNum) {

			List<PictureInfo> list = pictureHelper.getSearchPioNumList(pioNum);
			if (list != null) {

				for (PictureInfo data : list) {
					if (data.getPath().contains("_4")) {
						return true;
					}
				}
			}
		return false;
	}


}