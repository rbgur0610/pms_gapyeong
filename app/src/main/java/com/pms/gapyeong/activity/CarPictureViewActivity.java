package com.pms.gapyeong.activity;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.pms.gapyeong.R;
import com.pms.gapyeong.common.Camera;
import com.pms.gapyeong.common.Constants;
import com.pms.gapyeong.common.FileUtil;
import com.pms.gapyeong.common.ImageLoader;
import com.pms.gapyeong.common.MsgUtil;
import com.pms.gapyeong.common.Util;
import com.pms.gapyeong.database.PictureHelper;
import com.pms.gapyeong.vo.PictureInfo;

public class CarPictureViewActivity extends Activity {

	private ImageView iv_image1;
	private ImageView iv_image2;
	private ImageView iv_image3;
	private ImageView iv_image4;
	
	private Camera camera;
	
	private String pioNum;
	private String carNo;
	private String pictureType;
	private String parkCode;
	private String empCode;
	
	private int    iType = 0;
	
	private ArrayList<PictureInfo> pictureList = new ArrayList<PictureInfo>();
	
	private PictureHelper pictureHelper = PictureHelper.getInstance(this);

	private String TAG = this.getClass().getSimpleName();
	
	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.car_picture_view_activity);
		
		camera = new Camera(this);
		
		Intent intent = getIntent();
		if (intent != null) {
			pioNum = Util.isNVL(intent.getStringExtra("pioNum"));
			carNo  = Util.isNVL(intent.getStringExtra("carNo"));
			pictureType = Util.isNVL(intent.getStringExtra("pictureType"));
			parkCode    = Util.isNVL(intent.getStringExtra("parkCode"));
			empCode     = Util.isNVL(intent.getStringExtra("empCode"));

			if (Constants.PICTURE_TYPE_DIS.equals(pictureType)) {
				iType = 4;
				takePicture(iType);
			}
		}
		
		Log.d(TAG, " pioNum >>> " + pioNum);
		Log.d(TAG, " carNo >>> " + carNo);
		Log.d(TAG, " pictureType >>> " + pictureType);
		Log.d(TAG, " pictureList >>> " + pictureList);
		
		iv_image1 = (ImageView) findViewById(R.id.iv_image1);
		iv_image2 = (ImageView) findViewById(R.id.iv_image2);
		iv_image3 = (ImageView) findViewById(R.id.iv_image3);
		iv_image4 = (ImageView) findViewById(R.id.iv_image4);
		
		iv_image1.setTag("");
		iv_image2.setTag("");
		iv_image3.setTag("");
		iv_image4.setTag("");
		
		iv_image1.setOnClickListener(new View.OnClickListener() {
        	@Override
        	public void onClick(View v) {
        		iType = 1;
        		if(Util.isEmpty(v.getTag().toString())){
        			takePicture(iType);
        		} else {
					showDialogChoice(v.getTag().toString());
        		}
        	}
        });
		
		iv_image2.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				iType = 2;
				if(Util.isEmpty(v.getTag().toString())){
					takePicture(iType);
				} else {
					showDialogChoice(v.getTag().toString());
				}
			}
		});
		
		iv_image3.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				iType = 3;
				if(Util.isEmpty(v.getTag().toString())){
					takePicture(iType);
				} else {
					showDialogChoice(v.getTag().toString());
				}
			}
		});
		
		iv_image4.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				iType = 4;
				if(Util.isEmpty(v.getTag().toString())){
					takePicture(iType);
				} else {
					showDialogChoice(v.getTag().toString());
				}
			}
		});


		Button btn_cancel = (Button) findViewById(R.id.btn_cancel);
		btn_cancel.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});


		apiParkIOPictureCall();

	}


	protected void showDialogChoice(final String imageURL){
		AlertDialog.Builder builder = new AlertDialog.Builder(CarPictureViewActivity.this);
		builder.setTitle("사진관리");
		builder.setMessage("재촬영 하시겠습니까?");
		builder.setPositiveButton("재촬영", new DialogInterface.OnClickListener()
		{
				@Override
				public void onClick(DialogInterface dialog, int which) {
					takePicture(iType);
					dialog.dismiss();
				}
		}).setNeutralButton("확대보기", new DialogInterface.OnClickListener()
			{
				@Override
				public void onClick(DialogInterface dialog, int which) {
					viewPicture(imageURL);
					dialog.dismiss();
				}
		}).setNegativeButton("삭제", new DialogInterface.OnClickListener()
			{
				@Override
				public void onClick(DialogInterface dialog, int which) {
					deleteDialog(imageURL);
					dialog.dismiss();
				}
		});		
		AlertDialog alert = builder.create();
//		alert.setCancelable(false);
	    alert.show();			
	}
	
	protected void deleteDialog(final String imageURL){
		AlertDialog.Builder builder = new AlertDialog.Builder(CarPictureViewActivity.this);
		builder.setTitle("사진관리");
		builder.setMessage("정말로 사진을 삭제하시겠습니까?");
		builder.setPositiveButton(R.string.btn_ok, new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which) {
				deletePicture(imageURL);
				dialog.dismiss();
			}
		}).setNegativeButton(R.string.btn_cancel, new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});		
		AlertDialog alert = builder.create();
//		alert.setCancelable(false);
		alert.show();			
	}
	
	protected void takePicture(int index){
		camera.takePhoto(pioNum + "_" + index);
	}
	
	protected void viewPicture(String imageURL){
		Log.d(TAG, " imageURL >>> " + imageURL);
		Intent intent = new Intent(CarPictureViewActivity.this, CarPictureViewDetailActivity.class);
		intent.putExtra("imageURL", imageURL);
		startActivity(intent);		
	}
	
	protected void deletePicture(String imageURL){
		
		switch(iType){
			case 1 :
				iv_image1.setTag("");
				iv_image1.setImageDrawable(null);
				break;
			case 2 :
				iv_image2.setTag("");
				iv_image2.setImageDrawable(null);
				break;
			case 3 :
				iv_image3.setTag("");
				iv_image3.setImageDrawable(null);
				break;
			case 4 :
				iv_image4.setTag("");
				iv_image4.setImageDrawable(null);
				break;
		}
		
		Log.d(TAG, "pictureList : " + pictureList);
		String fileName = FileUtil.fileNameOnly(imageURL);
		Log.d(TAG, "fileName : " + fileName);
		List<PictureInfo> list = pictureHelper.getSearchPioNumList(pioNum);
		if(list != null){
		  for(PictureInfo info : list){
			 String localImgPath = info.getPath();
		     if(fileName.equals(FileUtil.fileNameOnly(localImgPath))){
		 		File f = new File(localImgPath);
				if (f.delete()) {
					Log.i(TAG, "delete image : " + localImgPath);
					pictureHelper.delete(pioNum, localImgPath);
					pictureList.remove(info);
				}		    	 
		     }
		  }
		}
		
		apiParkPictureDeleteCall(imageURL);
	}
	
	public void setParkImageView(JSONArray jsonArr){
		
	 	   if(pictureList != null){
			   pictureList.clear();
		   }else{
			   pictureList = new ArrayList<>();
		   }
		
		   int Len = jsonArr.length();
		   for(int i=0; i<Len; i++){
			  try {
				JSONObject res = jsonArr.getJSONObject(i);
				String filePath = res.optString("FILE_PATH");
				String fileName = res.optString("FILE_NAME");
				Log.d(TAG, res.optString("FILE_PATH"));
				Log.d(TAG, res.optString("FILE_NAME"));
				
		    	PictureInfo pictureInfo = new PictureInfo();
		    	pictureInfo.setPath(filePath + fileName);
		    	pictureList.add(pictureInfo);
		    	
				int index = pictureInfo.getPath().lastIndexOf("_") + 1;
				String tempStr = pictureInfo.getPath().substring(index, index+1);
				String pictureURL = pictureInfo.getPath();
				Log.d(TAG, " tempStr >>> " + tempStr + " pictureURL ::: " + pictureURL);
				if("1".equals(tempStr)){
					 iv_image1.setTag(pictureURL);
					 ImageLoader.setImageUrlView(this, Constants.SERVER_HOST + pictureURL, iv_image1);
				} else if("2".equals(tempStr)){
					 iv_image2.setTag(pictureURL);
					 ImageLoader.setImageUrlView(this, Constants.SERVER_HOST + pictureURL, iv_image2);
				} else if("3".equals(tempStr)){
					 iv_image3.setTag(pictureURL);
					 ImageLoader.setImageUrlView(this, Constants.SERVER_HOST + pictureURL, iv_image3);
				} else if("4".equals(tempStr)){
					 iv_image4.setTag(pictureURL);
					 ImageLoader.setImageUrlView(this, Constants.SERVER_HOST + pictureURL, iv_image4);
				}			    	
			 } catch (JSONException e) {
				// TODO Auto-generated catch block
				  if(Constants.DEBUG_PRINT_LOG){
					  e.printStackTrace();
				  }else{
					  System.out.println("예외 발생");
				  }
			 }
		  }
	}				

	private void apiParkIOPictureCall(){
		String url = Constants.API_SERVER + "?" + Constants.METHOD +"="+ Constants.INTERFACEID_PARK_IO_PICTURE;
		String param = "&PIO_NUM="+pioNum
					 + "&CD_PARK="+parkCode
					 + "&"+Constants.API_FORMAT+"="+Constants.JSON_FORMAT;
        Log.d(TAG, " apiParkIOPictureCall url >>> " + url + param);
        
		AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>();        
		cb.url(url + param).type(JSONObject.class).weakHandler(this, "parkIOPictureCallback").redirect(true).retry(3).fileCache(false).expire(-1);
		AQuery aq = new AQuery(this);
		aq.ajax(cb); 			
	}		
	

	public void parkIOPictureCallback(String url, JSONObject json, AjaxStatus status){
		
		Log.d(TAG, " parkIOPictureCallback  json ====== " +json);
		JSONArray jsonArr = null;
		// successful ajax call          
	    if(json != null){   
	       if("1".equals(json.optString("CD_RESULT"))){
			   jsonArr = json.optJSONArray("PICTURE_LIST");
			   setParkImageView(jsonArr);
	       }
	    } else {
		    MsgUtil.ToastMessage(CarPictureViewActivity.this, Constants.DATA_FAIL, Toast.LENGTH_LONG);
	    }            
        
	}		
		
	private void apiParkPictureUploadCall(String imagePath){
		
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("PIO_NUM", pioNum);
        params.put("CD_PARK", parkCode);
        params.put("CAR_NO",  carNo);
        params.put("CD_TYPE", pictureType);
        params.put("EMP_CD",  empCode);
        params.put(Constants.API_FORMAT, Constants.JSON_FORMAT);
        
        Log.d(TAG, " imagePath >>> " + imagePath);
        Log.d(TAG, " empCode >>> " + empCode);
        //Alternatively, put a File or InputStream instead of byte[]
        File file = new File(imagePath);    
        params.put("IMAGE_FILE", file);        
        
        String url = Constants.API_SERVER + "?" + Constants.METHOD +"="+ Constants.INTERFACEID_PARK_PICTURE_UPLOAD;
        Log.d(TAG, " apiParkPictureUploadCall url >>> " + url + " params ::: " + params);
		AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>();        
		cb.url(url).params(params).type(JSONObject.class).weakHandler(this, "parkPictureUploadCallback").redirect(true).retry(3).fileCache(false).expire(-1);
		AQuery aq = new AQuery(this);
		aq.ajax(cb);
		
	}		
	
	public void parkPictureUploadCallback(String url, JSONObject json, AjaxStatus status){
		Log.d(TAG, " parkPictureUploadCallback  json ====== " +json);
		// successful ajax call          
        if(json != null){   
           String KN_RESULT = json.optString("KN_RESULT");
           if("1".equals(json.optString("CD_RESULT"))){
        	   MsgUtil.ToastMessage(this, KN_RESULT);
        	   JSONArray jsonArr = json.optJSONArray("PICTURE_LIST");
			   setParkImageView(jsonArr);
           } else {
        	   MsgUtil.ToastMessage(this, KN_RESULT);
           }
        } else {
    	    MsgUtil.ToastMessage(this, Constants.DATA_FAIL, Toast.LENGTH_LONG);
        }
	}			
	
	private void apiParkPictureDeleteCall(String imageURL){
		
		String fileName = FileUtil.fileNameOnly(imageURL);
		Log.d(TAG, " fileName >>> " + fileName );
		
        String url = Constants.API_SERVER + "?" + Constants.METHOD +"="+ Constants.INTERFACEID_PARK_PICTURE_DELETE;
		String param = "&IMAGE_FILE="+fileName
					 + "&PIO_NUM="+pioNum
					 + "&CD_PARK="+parkCode
					 + "&"+Constants.API_FORMAT+"="+Constants.JSON_FORMAT;        
		
        Log.d(TAG, " apiParkPictureDeleteCall url >>> " + url + param);
        
		AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>();        
		cb.url(url + param).type(JSONObject.class).weakHandler(this, "parkPictureDeleteCallback").redirect(true).retry(3).fileCache(false).expire(-1);
		AQuery aq = new AQuery(this);
		aq.ajax(cb);
	}		
	
	public void parkPictureDeleteCallback(String url, JSONObject json, AjaxStatus status){
		Log.d(TAG, " parkPictureDeleteCallback  json ====== " +json);
		
		// successful ajax call          
        if(json != null){   
        	String KN_RESULT = json.optString("KN_RESULT");
//           if("1".equals(json.optString("CD_RESULT"))){
        	   MsgUtil.ToastMessage(this, KN_RESULT);
        	   JSONArray jsonArr = json.optJSONArray("PICTURE_LIST");
        	   setParkImageView(jsonArr);        	   
//           } else 
        } else {
    	    MsgUtil.ToastMessage(this, Constants.DATA_FAIL, Toast.LENGTH_LONG);
        }
        
	}			
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		
		super.onActivityResult(requestCode, resultCode, data);
		
		Log.d(TAG, " requestCode >>> " + requestCode);
		switch (requestCode) {
		  case Camera.TAKE_PICTURE :
			  
				if(resultCode == RESULT_CANCELED){
					return;			
				}			  
				  
				String imagePath = camera.imagePath;
				if (imagePath == null) {
					MsgUtil.ToastMessage(this, "사진촬영 에러. 사진을 다시 촬영해 주세요", Toast.LENGTH_LONG);
					return ;
				}
				
				// 이미지 리사이즈...
				ImageLoader.resizeImage(imagePath);
				
				Log.d(TAG, " onActivityResult pictureList ::: " + pictureList);
				
				// 로컬 사진 저장
				pictureHelper.delete(pioNum, imagePath);
				pictureHelper.insert(pioNum, imagePath, parkCode, carNo, pictureType, Util.getYmdhms("yyyyMMdd"));
				// 서버 사진 저장 
				apiParkPictureUploadCall(imagePath);
			break;
			
			case Camera.SELECT_CROP_IMAGE:
				if(resultCode == RESULT_CANCELED){
					return;			
				}					
				camera.selectCropResult(data);
			break;			
		} // end switch
		
	}
	
}
