package com.pms.gapyeong.adapter;

import java.util.List;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.pms.gapyeong.R;
import com.pms.gapyeong.database.CodeHelper;
import com.pms.gapyeong.vo.ParkIO;

public class ExitManagerAdapter extends ArrayAdapter<ParkIO> {
	
	int resId;
	String title;
	
	public ExitManagerAdapter(Context context, int textViewResourceId,
			List<ParkIO> objects, String title) {
		super(context, textViewResourceId, objects);
		
		resId = textViewResourceId;
		this.title = title;
	}


	@Override
	public int getCount() {
		return super.getCount();
	}
	
	@Override
	public ParkIO getItem(int position) {
		return super.getItem(position);
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ExitManagerHolder holder = null;
		ParkIO item = getItem(position);
		
		if (convertView == null) {
			holder = new ExitManagerHolder();
			
			LinearLayout ll = (LinearLayout) View.inflate(getContext(), resId, null);
			
			holder.tv_date = (TextView) ll.findViewById(R.id.tv_date);
			holder.tv_car_number = (TextView) ll.findViewById(R.id.tv_car_number);
			holder.tv_input_time = (TextView) ll.findViewById(R.id.tv_input_time);
			holder.tv_park_type = (TextView) ll.findViewById(R.id.tv_park_type);
			
			convertView = ll;
			
		} else {
			holder = (ExitManagerHolder) convertView.getTag();
		}
		
		holder.tv_date.setText(item.getPioDay().substring(2, 4) + "-" + item.getPioDay().substring(4, 6) + "-" + item.getPioDay().substring(6, 8));
		holder.tv_car_number.setText(item.getCarNo());
		
		if ("출차완료".equals(title)) {
			holder.tv_input_time.setText(item.getParkAmt());
			CodeHelper cch = CodeHelper.getInstance(getContext());
			holder.tv_park_type.setText(cch.findCodeByCdCode("PY", item.getParkType()));
			
		} else if ("출차취소".equals(title)) {
			holder.tv_input_time.setText(item.getParkAmt());			
			CodeHelper cch = CodeHelper.getInstance(getContext());
			holder.tv_park_type.setText(cch.findCodeByCdCode("PY", item.getParkType()));
			
		} else if ("입차취소".equals(title)) {
			StringBuffer sb = new StringBuffer();
			sb.append(item.getParkInDay().substring(8, 10)).append(":").append(item.getParkInDay().substring(10, 12));
			
			holder.tv_input_time.setText(sb.toString());
			CodeHelper cch = CodeHelper.getInstance(getContext());
			holder.tv_park_type.setText(cch.findCodeByCdCode("PY", item.getParkType()));
			
		} else {
			StringBuffer sb = new StringBuffer();
			sb.append(item.getParkInDay().substring(8, 10)).append(":").append(item.getParkInDay().substring(10, 12));
			
			holder.tv_input_time.setText(sb.toString());
			CodeHelper cch = CodeHelper.getInstance(getContext());
			holder.tv_park_type.setText(cch.findCodeByCdCode("PY", item.getParkType()));
		}
		
		convertView.setTag(holder);
		
		return convertView;
	}

	private static class ExitManagerHolder {
		TextView tv_date;
		TextView tv_car_number;
		TextView tv_input_time;
		TextView tv_park_type;
	}
}


