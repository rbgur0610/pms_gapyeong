package com.pms.gapyeong.adapter;

import java.util.List;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.pms.gapyeong.R;
import com.pms.gapyeong.common.ImageLoader;
import com.pms.gapyeong.vo.PictureInfo;

public class ImageLoaderAdapter extends ArrayAdapter<PictureInfo> {
	
	int resId;
	ImageLoader loader = new ImageLoader();
	OnCheckedChangeListener checkListener;

	public void setCheckListener(OnCheckedChangeListener checkListener) {
		this.checkListener = checkListener;
	}

	public ImageLoaderAdapter(Context context, int textViewResourceId,
			List<PictureInfo> objects) {
		super(context, textViewResourceId, objects);
		
		resId = textViewResourceId;
	}
	
	@Override
	public int getCount() {
		return super.getCount();
	}
	
	@Override
	public PictureInfo getItem(int position) {
		return super.getItem(position);
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		PictureInfo item = getItem(position);
		
		if (convertView == null) {
			holder = new ViewHolder();
			
			LinearLayout ll = (LinearLayout) View.inflate(getContext(), resId, null);
			
			holder.checkBox = (CheckBox) ll.findViewById(R.id.check);
			holder.imageView = (ImageView) ll.findViewById(R.id.image);
			holder.textView = (TextView) ll.findViewById(R.id.text);
			
			holder.checkBox.setOnCheckedChangeListener(checkListener);
			
			convertView = ll;
			
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		Log.e("test", "position : " + position + ", item : " + item);
		holder.checkBox.setTag(position);
		holder.imageView.setImageBitmap(null);
		
		loader.loadBitmap(item.getPath(), holder.imageView);
		holder.textView.setText(item.getCarNo());
		convertView.setTag(holder);
		holder.checkBox.setChecked(item.isChecked());
		
		return convertView;
	}
	
	
	private static class ViewHolder {
		ImageView imageView;
		CheckBox checkBox;
		TextView textView;
	}
}
