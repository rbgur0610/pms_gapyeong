package com.pms.gapyeong.adapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.pms.gapyeong.R;
import com.pms.gapyeong.activity.AutoCarNumberActivity;
import com.pms.gapyeong.activity.ExitCommitActivity;
import com.pms.gapyeong.activity.InputCarNumerActivity;
import com.pms.gapyeong.common.Constants;
import com.pms.gapyeong.common.Preferences;
import com.pms.gapyeong.common.Util;
import com.pms.gapyeong.database.CodeHelper;
import com.pms.gapyeong.database.StatusBoardHelper;
import com.pms.gapyeong.vo.ParkIO;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class StatusBoardAdapter extends PagerAdapter {

    private String TAG = this.getClass().getSimpleName();

    private LayoutInflater mInflater;
    private Context mContext;
    private int totalPageCount;
    private StatusBoardHelper statusBoardHelper;
    private String workStartTime;                // 근무 시작 시간
    private String workEndTime;                    // 근무 종료시간
    private String serviceStartTime;                // 무료 주차 시작 시간
    private String serviceEndTime;                // 무료 주차 종료시간
    public CodeHelper cch;

    public StatusBoardAdapter(Context c, StatusBoardHelper statusBoardHelper, int totalPageCount) {
        super();
        mInflater = LayoutInflater.from(c);
        mContext = c;
        this.statusBoardHelper = statusBoardHelper;
        this.totalPageCount = totalPageCount;
        cch = CodeHelper.getInstance(c);
    }

    public void setStatusDate(String workStartTime, String workEndTime, String serviceStartTime, String serviceEndTime) {
        this.workStartTime = workStartTime;
        this.workEndTime = workEndTime;
        this.serviceStartTime = serviceStartTime;
        this.serviceEndTime = serviceEndTime;
    }


    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return totalPageCount;
    }

    @Override
    public Object instantiateItem(View pager, int position) {

        View view = null;
        LinearLayout bodyLayout;

//		Log.d(TAG, " position >>> " + position);

        int BOARD_SIZE = Preferences.getValue(mContext, Constants.PREFERENCE_SETTING_STATUS_BOARD_SIZE, Constants.SETTING_STATUS_BOARD_SIZE_15);
//		Log.d(TAG, " BOARD_SIZE >>> " + BOARD_SIZE);

        int rowCount = BOARD_SIZE / Constants.STATUS_BOARD_COLUMN_SIZE;
//		Log.d(TAG, " STATUS_BOARD_COLUMN_SIZE >>> " + Constants.STATUS_BOARD_COLUMN_SIZE);
//		Log.d(TAG, " rowCount >>> " + rowCount);

        view = mInflater.inflate(R.layout.status_body_activity, null);

        bodyLayout = (LinearLayout) view.findViewById(R.id.body_layout);
        LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, 0, (float) 1.0);

        int index = ((position + 1) - 1) * BOARD_SIZE;
//		Log.d(TAG, " index === " + index);

        for (int i = 0, j = 0; i < rowCount; i++, j = j + 2) {
            View v = mInflater.inflate(R.layout.status_board_row, null);
            TextView car_number1 = (TextView) v.findViewById(R.id.car_number1);
            TextView board_number1 = (TextView) v.findViewById(R.id.board_number1);
            TextView car_number2 = (TextView) v.findViewById(R.id.car_number2);
            TextView board_number2 = (TextView) v.findViewById(R.id.board_number2);
            TextView car_number3 = (TextView) v.findViewById(R.id.car_number3);
            TextView board_number3 = (TextView) v.findViewById(R.id.board_number3);
            TextView car_time01 = (TextView) v.findViewById(R.id.car_time01);
            TextView car_time02 = (TextView) v.findViewById(R.id.car_time02);
            TextView car_time03 = (TextView) v.findViewById(R.id.car_time03);

            int row1 = index + i + (j + 1);
            int row2 = index + i + (j + 2);
            int row3 = index + i + (j + 3);

            RelativeLayout column1 = (RelativeLayout) v.findViewById(R.id.column1);
            RelativeLayout column2 = (RelativeLayout) v.findViewById(R.id.column2);
            RelativeLayout column3 = (RelativeLayout) v.findViewById(R.id.column3);

            if (row1 <= Constants.STATUS_BOARD_TOTAL_AREA) {
                JSONObject json = getParkArea(row1);
                if (json != null) {
                    board_number1.setText("" + row1);
                    board_number1.setVisibility(View.VISIBLE);
                    car_number1.setTag(json);
                    car_number1.setText(renewCarnoText(Util.isNVL(json.optString("CAR_NO"))));
//					car_time01.setText(diffTime(Util.isNVL(json.optString("PARK_IN"))));
                    setParkInOutTime(Util.isNVL(json.optString("PARK_IN")), Util.isNVL(json.optString("DIS_CD")), car_time01);
                    setResourceColor(json, board_number1, car_number1, column1, json.optString("PARK_TYPE"), json.optString("CARD_NO").equals("") ? false : true);
                    board_number1.setTextColor(mContext.getResources().getColor(R.color.White));
                    car_number1.setTextColor(mContext.getResources().getColor(R.color.White));
                } else {
                    car_number1.setText("" + row1);
                    setResourceColor(null, board_number1, car_number1, column1, "", false);
                }
            } else {
                board_number1.setText("");
                board_number1.setVisibility(View.GONE);
                car_number1.setText("");

                setResourceColor(null, board_number1, car_number1, column1, "FF", false);
            }

            if (row2 <= Constants.STATUS_BOARD_TOTAL_AREA) {
                JSONObject json = getParkArea(row2);
                if (json != null) {
                    board_number2.setText("" + row2);
                    board_number2.setVisibility(View.VISIBLE);
                    car_number2.setTag(json);
                    car_number2.setText(renewCarnoText(Util.isNVL(json.optString("CAR_NO"))));
//					car_time02.setText(diffTime(Util.isNVL(json.optString("PARK_IN"))));
                    setParkInOutTime(Util.isNVL(json.optString("PARK_IN")), Util.isNVL(json.optString("DIS_CD")), car_time02);
                    setResourceColor(json, board_number2, car_number2, column2, json.optString("PARK_TYPE"), json.optString("CARD_NO").equals("") ? false : true);
                    board_number2.setTextColor(mContext.getResources().getColor(R.color.White));
                    car_number2.setTextColor(mContext.getResources().getColor(R.color.White));

                } else {
                    car_number2.setText("" + row2);
                    setResourceColor(null, board_number2, car_number2, column2, "", false);
                }
            } else {
                board_number2.setText("");
                board_number2.setVisibility(View.GONE);
                car_number2.setText("");

                setResourceColor(null, board_number2, car_number2, column2, "FF", false);
            }

            if (row3 <= Constants.STATUS_BOARD_TOTAL_AREA) {
                JSONObject json = getParkArea(row3);
                if (json != null) {
                    board_number3.setText("" + row3);
                    board_number3.setVisibility(View.VISIBLE);
                    car_number3.setTag(json);
                    car_number3.setText(renewCarnoText(Util.isNVL(json.optString("CAR_NO"))));
//					car_time03.setText(diffTime(Util.isNVL(json.optString("PARK_IN"))));
                    setParkInOutTime(Util.isNVL(json.optString("PARK_IN")), Util.isNVL(json.optString("DIS_CD")), car_time03);
                    setResourceColor(json, board_number3, car_number3, column3, json.optString("PARK_TYPE"), json.optString("CARD_NO").equals("") ? false : true);
                    board_number3.setTextColor(mContext.getResources().getColor(R.color.White));
                    car_number3.setTextColor(mContext.getResources().getColor(R.color.White));

                } else {
                    car_number3.setText("" + row3);
                    setResourceColor(null, board_number3, car_number3, column3, "", false);
                }
            } else {
                board_number3.setText("");
                board_number3.setVisibility(View.GONE);
                car_number3.setText("");

                setResourceColor(null, board_number3, car_number3, column3, "FF", false);
            }

            if (!Util.isEmpty(car_number1.getText().toString())) {
                car_number1.setOnClickListener(listener);
            }
            if (!Util.isEmpty(car_number2.getText().toString())) {
                car_number2.setOnClickListener(listener);
            }
            if (!Util.isEmpty(car_number3.getText().toString())) {
                car_number3.setOnClickListener(listener);
            }
            bodyLayout.addView(v, lp);
        }

        ((ViewPager) pager).addView(view, 0);

        return view;
    }

    private String diffTime(String inTime) {
        if (inTime != null && !"".equals(inTime)) {
            try {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
                Date systemDate = Calendar.getInstance().getTime();
                String myDate = sdf.format(systemDate);

                Date Date1 = sdf.parse(myDate);
                Date Date2 = sdf.parse(inTime);

                long millse = Date1.getTime() - Date2.getTime();
                long mills = Math.abs(millse);

                int Hours = (int) (mills / (1000 * 60 * 60));
                int Mins = (int) (mills / (1000 * 60)) % 60;
                long Secs = (int) (mills / 1000) % 60;

                String diff = "";

                if (Hours > 0) {
                    diff = "[" + Hours + "시간" + Mins + "분]";
                } else {
                    diff = "[" + Mins + "분]";
                }

                return diff;
            } catch (NullPointerException e) {
                System.out.println("NullPointerException 예외 발생");
            } catch (ParseException e) {
                System.out.println("ParseException 예외 발생");
            }
        }
        return "";
    }

    public boolean isColorParkType(String parkCd) {
        boolean isColor = false;
        if ("101".equals(parkCd) || "102".equals(parkCd) || "103".equals(parkCd) || "146".equals(parkCd) || "147".equals(parkCd) || "148".equals(parkCd) || "149".equals(parkCd) || "114".equals(parkCd) || "116".equals(parkCd)) {
            isColor = true;
        }
        return isColor;
    }

    private String renewCarnoText(String carno) {
        String strCarNo = "";
        if (carno.length() >= 9) {
            String carno_1 = carno.substring(0, 4);
            String carno_2 = "\n" + carno.substring(4, carno.length());
            strCarNo = carno_1 + carno_2;
        } else {
            strCarNo = carno;
        }
        return strCarNo;
    }

    private void setResourceColor(JSONObject json, TextView board_number, TextView car_number, RelativeLayout column, String type, boolean isCard) {
        if (Constants.isHOLD) {
            board_number.setTextColor(mContext.getResources().getColor(R.color.White));
            car_number.setTextColor(mContext.getResources().getColor(R.color.White));
            column.setBackgroundResource(getBackgroundHold(type));
        } else {
            board_number.setTextColor(mContext.getResources().getColor(R.color.Black));
            car_number.setTextColor(mContext.getResources().getColor(R.color.Black));
            column.setBackgroundResource(getBackground(json, type, isCard));
        }


        int boardSize = Preferences.getValue(mContext, Constants.PREFERENCE_SETTING_STATUS_BOARD_SIZE, Constants.SETTING_STATUS_BOARD_SIZE_15);
        int textSize = 20;
        String carNum = car_number.getText().toString();

        switch (boardSize) {
            case Constants.SETTING_STATUS_BOARD_SIZE_9:
            case Constants.SETTING_STATUS_BOARD_SIZE_12:
            case Constants.SETTING_STATUS_BOARD_SIZE_15:
            case Constants.SETTING_STATUS_BOARD_SIZE_18:
                textSize = 26;
                break;
            case Constants.SETTING_STATUS_BOARD_SIZE_21:
                if (carNum.length() > 4) {
                    carNum = carNum.substring(carNum.length() - 4);
                }

                textSize = 26;
                break;
            case Constants.SETTING_STATUS_BOARD_SIZE_24:
                if (carNum.length() > 4) {
                    carNum = carNum.substring(carNum.length() - 4);
                }

                textSize = 24;
                break;
        }
        car_number.setText(carNum);
        car_number.setTextSize(TypedValue.COMPLEX_UNIT_DIP, textSize);
    }

    private int getBackground(JSONObject json, String type, boolean isCard) {
        int resId = R.drawable.btn_gradation_empty;
        if ("01".equals(type)) {
            if (json == null) {
                resId = R.drawable.btn_gradation_normal;
            } else {
                String park_cd = json.optString("CD_PARK");
                String prePayAmt = json.optString("ADV_IN_AMT");

                String park_in = json.optString("PARK_IN");
                String current_time = Util.getDayTime();
                long diff = 0;
                try {
                    diff = Util.diffOfMinutes(park_in, current_time);
                }catch (NullPointerException e) {
                    System.out.println("NullPointerException 예외 발생");
                } catch (ParseException e) {
                    System.out.println("ParseException 예외 발생");
                }
//				Log.e("STATUS_BOARD_TOTAL_AREA","park_in : "+park_in+"     current_time : "+current_time+"     diff : "+diff+"     park_cd : "+park_cd);

//		if(isColorParkType(park_cd)&&prePayAmt.equals("0")&&diff>120){
                if (prePayAmt.equals("0") && diff > 120) {
                    resId = R.drawable.btn_gradation_purple;
                } else {
                    resId = R.drawable.btn_gradation_normal;
                }
            }

        } else if ("02".equals(type) || "04".equals(type)) {
            resId = isCard ? R.drawable.btn_gradation_prepayment_card : R.drawable.btn_gradation_prepayment;
        } else if ("03".equals(type)) {
            resId = R.drawable.btn_gradation_ticket;
        } else if ("FF".equals(type)) {
            resId = R.drawable.btn_gradation_white;
        }
        return resId;
    }


    private int getBackgroundHold(String type) {
        int resId = R.drawable.btn_gradation_hold_empty;
        if ("01".equals(type)) {
            resId = R.drawable.btn_gradation_hold_normal;
        } else if ("02".equals(type) || "04".equals(type)) {
            resId = R.drawable.btn_gradation_hold_prepayment;
        } else if ("03".equals(type)) {
            resId = R.drawable.btn_gradation_hold_ticket;
        } else if ("FF".equals(type)) {
            resId = R.drawable.btn_gradation_white;
        }
        return resId;
    }

    @Override
    public int getItemPosition(Object item) {
        return POSITION_NONE;   // notifyDataSetChanged
    }

    @Override
    public void destroyItem(View pager, int position, Object view) {
        ((ViewPager) pager).removeView((View) view);
    }

    @Override
    public boolean isViewFromObject(View view, Object obj) {
        // TODO Auto-generated method stub
        return view == obj;
    }

    View.OnClickListener listener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent i = null;
            JSONObject json = (JSONObject) v.getTag();

            if (json != null) {
                ParkIO parkIO = new ParkIO(json.optString("PIO_NUM")
                        , json.optString("CD_PARK")
                        , json.optString("PIO_DAY")
                        , json.optString("CAR_NO")
                        , json.optString("CD_AREA")
                        , json.optString("PARK_TYPE")
                        , json.optString("DIS_CD")
                        , json.optString("TEL")
                        , json.optString("PARK_IN_AMT")
                        , json.optString("PARK_OUT_AMT")
                        , json.optString("PARK_AMT")
                        , json.optString("ADV_IN_AMT")
                        , json.optString("ADV_OUT_AMT")
                        , json.optString("ADV_AMT")
                        , json.optString("DIS_IN_AMT")
                        , json.optString("DIS_OUT_AMT")
                        , json.optString("DIS_AMT")
                        , json.optString("CASH_IN_AMT")
                        , json.optString("CASH_OUT_AMT")
                        , json.optString("CASH_AMT")
                        , json.optString("CARD_IN_AMT")
                        , json.optString("CARD_OUT_AMT")
                        , json.optString("CARD_AMT")
                        , json.optString("TRUNC_IN_AMT")
                        , json.optString("TRUNC_OUT_AMT")
                        , json.optString("TRUNC_AMT")
                        , json.optString("COUPON_AMT")
                        , json.optString("RETURN_AMT")
                        , json.optString("YET_AMT")
                        , json.optString("REAL_AMT")
                        , json.optString("PARK_TIME")
                        , json.optString("PARK_IN")
                        , json.optString("PARK_OUT")
                        , json.optString("CARD_NO")
                        , json.optString("CARD_COMPANY")
                        , json.optString("APPROVAL_NO")
                        , json.optString("APPROVAL_DATE")
                        , json.optString("CHARGE_TYPE")
                        , json.optString("KN_COMMENT"));
                i = new Intent(mContext, ExitCommitActivity.class);
                i.putExtra("parkIO", parkIO);
                Log.d(TAG, " boardNumber ExitCommitActivity ::: " + ((TextView) v).getText().toString());
                mContext.startActivity(i);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                        | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                ((Activity) mContext).overridePendingTransition(0, 0);
            } else {
                // 초기화
                Constants.KEY_CARNO_1 = "";
                Constants.KEY_CARNO_2 = "";
                Constants.KEY_CARNO_3 = "";
                Constants.KEY_CARNO_4 = "";
                Constants.KEY_CARNO_MANUAL = "";
                Constants.STATUS_BOARD = ((TextView) v).getText().toString();

                //input carnum
//                startInputCarNumber(Integer.parseInt(Constants.STATUS_BOARD));

                //auto carnum
                startAutoCarNumber(Integer.parseInt(Constants.STATUS_BOARD));
            }


        }
    };

    private void startInputCarNumber(int carArea) {

        try {

            Intent i = new Intent(mContext, InputCarNumerActivity.class);
            i.putExtra(Constants.CAR_INPUT_MODE, "INCAR");
            i.putExtra("CD_AREA", String.valueOf(carArea));
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            mContext.startActivity(i);
            ((Activity) mContext).overridePendingTransition(0, 0);
        } catch (NullPointerException e) {
            System.out.println("NullPointerException 예외 발생");
        }
    }

    private void startAutoCarNumber(int carArea) {
        Intent i = new Intent(mContext, AutoCarNumberActivity.class);
        i.putExtra(Constants.CAR_INPUT_MODE, "INCAR");
        i.putExtra("CD_AREA", String.valueOf(carArea));
        mContext.startActivity(i);
    }


    private JSONObject getParkArea(int area) {

        JSONArray jsonArr = statusBoardHelper.getParkIoJsonArr();

        if (jsonArr != null) {
            JSONObject res = null;
            try {
                int Len = jsonArr.length();
                for (int i = 0; i < Len; i++) {
                    res = jsonArr.getJSONObject(i);
                    if (area == res.optInt("CD_AREA", 0)) {
                        break;
                    } else {
                        res = null;
                    }
                }
                return res;
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                if (Constants.DEBUG_PRINT_LOG) {
                    e.printStackTrace();
                } else {
                    System.out.println("예외 발생");
                }
                return null;
            }
        } else {
            return null;
        }
    }


    private void setParkInOutTime(String ParkInDay, String getDisCd, TextView tv) {
        String exitTime;
        Log.d(TAG, " Constants.HOLD_TIME >>> " + Constants.HOLD_TIME);
        if (!Util.isEmpty(Constants.HOLD_TIME)) {
            exitTime = Constants.HOLD_TIME;
        } else {
            exitTime = Util.getYmdhms("HHmmss");
        }
        if (Integer.parseInt(exitTime.substring(0, 5)) > Util.sToi(workEndTime + "00")) {
            exitTime = workEndTime + "00";
        }

        String date1 = ParkInDay;
        String date2 = Util.getYmdhms("yyyyMMdd") + exitTime;
        String retday = "";

        if (date2.length() <= 12) {
            date2 = date2 + "00";
        }


        //###############################################################
        //  STEP 1 : 주차 시간 계산
        //###############################################################
        Log.d(TAG, "date1 : " + date1 + ", date2 : " + date2);
        String parkMin = minuteCalc(date1, date2);
        Log.d(TAG, " parkMin >>> " + parkMin);


        //###############################################################
        //  STEP 2 : 할인 시간 계산
        //###############################################################
        // 요금할인 정보
        String discountName = cch.findCodeByCdCode("DC", getDisCd);

        // 할인시간 설정
        int disTime = Util.sToi(cch.findCodeByDisTime("DC", discountName), 0);
        if (disTime > 0) {
            Log.d(TAG, "parkMin 2 : " + parkMin);
            parkMin = "" + (Util.sToi(parkMin, 0) - disTime);
            Log.d(TAG, "parkMin 3 : " + parkMin);
            if ((Util.sToi(parkMin, 0) < 0)) {
                Log.d(TAG, "parkMin 4 : " + parkMin);
                parkMin = "0";
            }
        }
        // 주차시간 표시
        tv.setText(minuteView(parkMin));
    }


    /**
     * 주차시간 표시
     *
     * @param //startTime 20140401123200 (yyyyMMddHHmmss)
     * @param //endTime   20140401123200 (yyyyMMddHHmmss)
     * @return
     */
    public String minuteView(String parkMin) {
        int hh = Util.sToi(parkMin) / 60;
        int mm = Util.sToi(parkMin) % 60;
        return Util.addZero(hh) + ":" + Util.addZero(mm);
    }

    /**
     * 주차시간 계산
     *
     * @param //startTime 20140401123200 (yyyyMMddHHmmss)
     * @param //endTime   20140401123200 (yyyyMMddHHmmss)
     * @return
     */
    public String minuteCalc(String startDate, String endDate) {
        String today = Util.getYmdhms("yyyyMMdd");
        startDate = startDate.replaceAll("-", "").replaceAll("/", "").replaceAll(":", "").replaceAll(" ", "");
        endDate = endDate.replaceAll("-", "").replaceAll("/", "").replaceAll(":", "").replaceAll(" ", "");

        String nowDate = Util.getYmdhms("yyyyMMdd");

        long wStartTime = Long.parseLong(nowDate + workStartTime.replace(":", ""));
        long wEndTime = Long.parseLong(nowDate + workEndTime.replace(":", ""));

        String sTimeText = startDate.substring(8, 12);
        String eTimeText = endDate.substring(8, 12);

        long sTime = Long.parseLong(startDate.substring(0, 12));
        long eTime = Long.parseLong(endDate.substring(0, 12));

        Log.i(TAG, "minuteCalc startDate : " + startDate + ", endTime : " + endDate);
        Log.i(TAG, "minuteCalc wStartTime : " + wStartTime + ", wEndTime : " + wEndTime);
        Log.i(TAG, "minuteCalc sTimeText : " + sTimeText + ", eTimeText : " + eTimeText);
        Log.i(TAG, "minuteCalc sTime : " + sTime + ", eTime : " + eTime);

        if (Long.parseLong(startDate) >= Long.parseLong(endDate)) {
            Log.i(TAG, "return 1");
            return "0";    // 시작시간이 종료시간보다 크면
        }

        if (sTime < wStartTime && eTime < wStartTime) {
            Log.i(TAG, "return 2");
            return "0";    // 시작 시간이 근무 시작시간보다 작고, 종료 시간이 근무 시작시간보다 작은 경우
        } else if (sTime > wEndTime) {
            Log.i(TAG, "return 3");
            return "0";    // 시작시간이 근무 종료 시간보다 큰 경우
        }

        // 주차시작시간이 현재일이고 근무시작시간보다 작은 경우 근무시작시간으로 셋팅
        if (nowDate.equals(startDate.substring(0, 8)) && sTime < wStartTime) {
            sTimeText = workStartTime.replace(":", "");
            Log.i(TAG, "minuteCalc sTimeText >>> " + sTimeText);
        }

        // 주차종료시간이 현재일이고 근무종료시간보다 큰 경우 근무종료시간으로 셋팅
        if (nowDate.equals(endDate.substring(0, 8)) && eTime > wEndTime) {
            eTimeText = workEndTime.replace(":", "");
            Log.i(TAG, "minuteCalc eTimeText >>> " + eTimeText);
        }

        Log.i(TAG, "minuteCalc after sTimeText : " + sTimeText + ", eTimeText : " + eTimeText);

        String start_date = startDate.substring(0, 8) + sTimeText + "00";
        String end_date = endDate.substring(0, 8) + eTimeText + "00";

        Log.i(TAG, "minuteCalc start_date : " + start_date);
        Log.i(TAG, "minuteCalc end_date   : " + end_date);

        /***
         * 무료 주차 시간 계산
         */
        String parkTime = "";
        String serviceTime = "";
        int iSvcStartTime = Util.sToi(Util.isNVL(serviceStartTime, "0"));
        int iSvcEndTime = Util.sToi(Util.isNVL(serviceEndTime, "0"));

        if (iSvcStartTime != 0 && iSvcEndTime != 0) {
            Log.d(TAG, "minuteCalc service org time : " + Util.getMinute(today + serviceStartTime + "00", today + serviceEndTime + "00"));
        }

        sTime = Long.parseLong(startDate.substring(8, startDate.length() - 2));
        eTime = Long.parseLong(endDate.substring(8, startDate.length() - 2));

        if (iSvcStartTime == 0 || iSvcEndTime == 0) { // 무료주차시작시간 또는 무료주차종료시간이 없다면 무료주차시간 적용 안 함
            Log.d(TAG, " service time case 1 !!! ");
            parkTime = Util.getMinute(start_date, end_date);
            Log.d(TAG, " parkTime ::: " + parkTime);
        } else if (sTime >= iSvcStartTime && eTime <= iSvcEndTime) { // 입차시간이 무료주차시작시간보다 같거나크고 출차시간이 무료주차종료시간보다 같거나 작다면 무료주차구간(0분)
            Log.d(TAG, " service time case 2 !!! ");
            parkTime = "0";
            Log.d(TAG, " parkTime ::: " + parkTime);
        } else if ((sTime <= iSvcStartTime && eTime <= iSvcStartTime)
                || (sTime >= iSvcEndTime && eTime >= iSvcEndTime)) { // 입차시간과 출차시간이 무료주차시작시간보다 같거나 작고 무료주차종료시간보다 같거나크다면 무료주차시간 적용 안 함
            Log.d(TAG, " service time case 3 !!! " + sTime + "~" + iSvcStartTime + "~" + eTime + "~" + iSvcEndTime);
            parkTime = Util.getMinute(start_date, end_date);
            Log.d(TAG, " parkTime ::: " + parkTime);
        } else if (sTime <= iSvcStartTime && eTime <= iSvcEndTime) { // 입차시간이 무료주차시작시간보다 같거나작고  출차시간이 무료주차종료시간보다 같거나작다면 무료주차시간적용
            Log.d(TAG, " service time case 4 !!! ");
            parkTime = Util.getMinute(start_date, end_date);
            serviceTime = Util.getMinute(today + serviceStartTime + "00", end_date);
            Log.d(TAG, " parkTime ::: " + parkTime + " serviceTime ::: " + serviceTime);
            parkTime = "" + (Util.sToi(parkTime) - Util.sToi(serviceTime));
            Log.d(TAG, " parkTime  >>> " + parkTime);
        } else if (sTime <= iSvcStartTime && eTime > iSvcEndTime) { // 입차시간이 무료주차시작시간보다 같거나작고  출차시간이 무료주차종료시간보다 크다면 무료주차시간적용
            Log.d(TAG, " service time case 5 !!! ");
            parkTime = Util.getMinute(start_date, end_date);
            serviceTime = Util.getMinute(today + serviceStartTime + "00", today + serviceEndTime + "00");
            Log.d(TAG, " parkTime ::: " + parkTime + " serviceTime ::: " + serviceTime);
            parkTime = "" + (Util.sToi(parkTime) - Util.sToi(serviceTime));
            Log.d(TAG, " parkTime >>> " + parkTime);
        } else if (sTime >= iSvcStartTime && eTime > iSvcEndTime) { // 입차시간이 무료주차시작시간보다 같거나크고  출차시간이 무료주차종료시간보다 크다면 무료주차시간적용
            Log.d(TAG, " service time case 6 !!! ");
            parkTime = Util.getMinute(start_date, end_date);
            serviceTime = Util.getMinute(start_date, today + serviceEndTime + "00");
            Log.d(TAG, " parkTime ::: " + parkTime + " serviceTime ::: " + serviceTime);
            parkTime = "" + (Util.sToi(parkTime) - Util.sToi(serviceTime));
            Log.d(TAG, " parkTime >>> " + parkTime);
        }

        return parkTime;

    }
}

