package com.pms.gapyeong.adapter;

import java.util.ArrayList;

import org.json.JSONObject;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.pms.gapyeong.R;

public class CouponAdapter extends BaseAdapter {

	private static LayoutInflater mInflater = null;
	private ArrayList<JSONObject> arrayList;
	private TextView tvSum;
	int[] chargeList;
	int[] qtyData;
	private String couponType;
	private InputMethodManager ipm;

	public CouponAdapter(Context context, TextView sum, ArrayList<JSONObject> arrayList, String _type) {
		super();
		mInflater = LayoutInflater.from(context);
		this.tvSum 		= sum;
		this.arrayList  = arrayList;
		this.chargeList = new int[arrayList.size()];
		this.qtyData    = new int[arrayList.size()];
		this.couponType  = _type;
	}

	public View getView(final int position, View convertView, ViewGroup parent) {
		View row = convertView;
		row = mInflater.inflate(R.layout.dialog_coupon_item, parent, false);
		final TextView tvPreTicketSum	= (TextView) row.findViewById(R.id.tvPreTicketSum);
		TextView tvPreTicket 		= (TextView) row.findViewById(R.id.tvPreTicket);

		final JSONObject json = arrayList.get(position);

		tvPreTicket.setText(json.optString("KN_CODE"));
		EditText edPreTicketQty 	= (EditText) row.findViewById(R.id.edPreTicketQty);
		Button btnAdd = (Button)row.findViewById(R.id.btn_add);
		Button btnMinus = (Button)row.findViewById(R.id.btn_minus);
		if("TEXT".equals(this.couponType)) { // EDIT TEXT 사용시
			edPreTicketQty.setVisibility(View.VISIBLE);
			btnAdd.setVisibility(View.GONE);
			btnMinus.setVisibility(View.GONE);
			edPreTicketQty.addTextChangedListener(new TextWatcher() {
				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count) {
					// TODO Auto-generated method stub
					if(s.toString().equals("")){
						qtyData[position] = 0;
					}else{
						qtyData[position] = Integer.parseInt(s.toString());
					}
					float Sum = qtyData[position] * Float.parseFloat(json.optString("DIS_AMT","0"));
					Log.d("coupon", "============ position : " + position + ", amt : " + json.optString("DIS_AMT","0") + ", Sum : " + Sum + ", qtyData[position] : " +qtyData[position]);
					chargeList[position] = (int)Sum;
					tvPreTicketSum.setText(String.valueOf(chargeList[position]));
					setTotalSum();
				}

				@Override
				public void beforeTextChanged(CharSequence s, int start, int count,
											  int after) {
					// TODO Auto-generated method stub

				}

				@Override
				public void afterTextChanged(Editable s) {
					// TODO Auto-generated method stub
				}
			});
		} else {  // +,- 버튼 사용 시

			edPreTicketQty.setVisibility(View.GONE);
			btnAdd.setVisibility(View.VISIBLE);
			btnMinus.setVisibility(View.VISIBLE);

			btnAdd.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					if("".equals(qtyData[position])) {
						qtyData[position] = 0;
					} else {
						qtyData[position] = qtyData[position] + 1;
					}
					float Sum = qtyData[position] * Float.parseFloat(json.optString("DIS_AMT","0"));
					chargeList[position] = (int)Sum;
					tvPreTicketSum.setText(String.valueOf(chargeList[position]));
					setTotalSum();

				}
			});

			btnMinus.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					if("".equals(qtyData[position])) {
						qtyData[position] = 0;
					} else {
						if( qtyData[position] > 0) {
							qtyData[position] = qtyData[position] -1;
						} else {
							qtyData[position] = 0;
						}
					}
					float Sum = qtyData[position] * Float.parseFloat(json.optString("DIS_AMT","0"));
					chargeList[position] = (int)Sum;
					tvPreTicketSum.setText(String.valueOf(chargeList[position]));
					setTotalSum();
				}
			});
		}




		return row;
	}

	private InputMethodManager getSystemService(String inputMethodService) {
		// TODO Auto-generated method stub
		return null;
	}

	public int getCouponQtyTotal(){
		int total = 0;
		for (int i = 0; i < qtyData.length; i++) {
			if(qtyData[i]>0)
				total++;
		}
		return total;
	}

	public String getCouponQueryString(){
		StringBuffer sb = new StringBuffer();
		int pos = 0;
		for (int i = 0; i < qtyData.length; i++) {
			if(qtyData[i]>0)
			{
				JSONObject json = arrayList.get(i);
				sb.append("^COUPON_NO="+String.valueOf(pos)+"$COUPON_TYPE="+json.optString("CD_CODE")+"$COUPON_QTY="+qtyData[i]+"$COUPON_AMT="+chargeList[i]);
				pos++;
			}
		}
		return sb.toString();
	}

	public int[] getQtyData(){
		return qtyData;
	}

	public int[] getChargeList(){
		return chargeList;
	}

	private void setTotalSum(){
		int totalAmt = 0;
		int Len = arrayList.size();
		for (int i = 0; i < Len; i++) {
			totalAmt+=chargeList[i];
		}
		tvSum.setText(String.valueOf(totalAmt)+"원");
	}

	public int getCount() {
		if (arrayList == null)
			return 0;
		return arrayList.size();
	}

	public long getItemId(int position) {
		return 0;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

}