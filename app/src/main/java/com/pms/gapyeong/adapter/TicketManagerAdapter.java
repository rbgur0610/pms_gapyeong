package com.pms.gapyeong.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.pms.gapyeong.R;
import com.pms.gapyeong.vo.ParkTicketInfoItem;

public class TicketManagerAdapter extends BaseAdapter {
	private static LayoutInflater mInflater = null;
	private ArrayList<ParkTicketInfoItem> mBean;

	public TicketManagerAdapter(Context context, ArrayList<ParkTicketInfoItem> sapList) {
		super();
		mInflater = LayoutInflater.from(context);
		mBean = sapList;
	}

	class ViewWrapper {

		View base;
		TextView File_Name = null;
		private TextView tvTicketUseDate;
		private TextView tvPayAmt;
		private TextView tvTicketAmt;
		private TextView tvCarNum;
		private TextView tvUploadDate;

		ViewWrapper(View base) {
			this.base = base;
		}

		TextView tvUploadDate() {
			if (tvUploadDate == null) {

				tvUploadDate = (TextView) base.findViewById(R.id.tvUploadDate);
			}
			return tvUploadDate;
		}
		
		TextView tvCarNum() {
			if (tvCarNum == null) {

				tvCarNum = (TextView) base.findViewById(R.id.tvCarNum);
			}
			return tvCarNum;
		}
		
		TextView tvTicketAmt() {
			if (tvTicketAmt == null) {

				tvTicketAmt = (TextView) base.findViewById(R.id.tvTicketAmt);
			}
			return tvTicketAmt;
		}
		
		TextView tvPayAmt() {
			if (tvPayAmt == null) {

				tvPayAmt = (TextView) base.findViewById(R.id.tvPayAmt);
			}
			return tvPayAmt;
		}
		
		TextView tvTicketUseDate() {
			if (tvTicketUseDate == null) {

				tvTicketUseDate = (TextView) base.findViewById(R.id.tvTicketUseDate);
			}
			return tvTicketUseDate;
		}
		
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
		ViewWrapper wrapper = null;
		row = mInflater.inflate(R.layout.ticket_manageritem_layout, parent, false);

		wrapper = new ViewWrapper(row);
		wrapper.tvUploadDate().setText(mBean.get(position).getTicketDay());
		wrapper.tvCarNum().setText(mBean.get(position).getCarNo());
		wrapper.tvTicketAmt().setText(mBean.get(position).getTicketAmt());
		wrapper.tvPayAmt().setText(mBean.get(position).getPayAmt());
		wrapper.tvTicketUseDate().setText(mBean.get(position).getStartDay()+"\n"+mBean.get(position).getEndDay());
		return row;
	}

	public int getCount() {
		if (mBean == null)
			return 0;
		return mBean.size();
	}

	public long getItemId(int position) {
		return 0;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}
}