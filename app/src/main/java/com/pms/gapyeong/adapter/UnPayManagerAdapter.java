package com.pms.gapyeong.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;

import com.pms.gapyeong.R;
import com.pms.gapyeong.vo.UnPayManagerItem;

public class UnPayManagerAdapter extends BaseAdapter {
	private static LayoutInflater mInflater = null;
	private ArrayList<UnPayManagerItem> mBean;
	private String searchCode="";
	OnCheckedChangeListener checkListener;
	private boolean gCheckboxVisible = true;
	
    public void setCheckListener(OnCheckedChangeListener checkListener) {
        this.checkListener = checkListener;
    }
	
	public UnPayManagerAdapter(Context context, ArrayList<UnPayManagerItem> unPayList, String searchCode) {
		super();
		mInflater = LayoutInflater.from(context);
		mBean = unPayList;
		this.searchCode = searchCode;
	}

	public void VisibleCheckbox(boolean bsCheck)
	{
		gCheckboxVisible = bsCheck;
	}
	
	class ViewWrapper {

		View base;
		TextView File_Name = null;
		private TextView tvUnPayDate;
		private TextView tvUnPayReason;
		private TextView tvUnPayAmt;
		private TextView tvCarNum;
		private CheckBox chkCarnum;

		ViewWrapper(View base) {
			this.base = base;
		}
		
		CheckBox chkCarnum() {
			if (chkCarnum == null) {

				chkCarnum = (CheckBox) base.findViewById(R.id.chk_car);

			}
			
			return chkCarnum;
		}

		TextView tvUnPayDate() {
			if (tvUnPayDate == null) {

				tvUnPayDate = (TextView) base.findViewById(R.id.tvUnPayDate);
			}
			return tvUnPayDate;
		}
		
		TextView tvCarNum() {
			if (tvCarNum == null) {

				tvCarNum = (TextView) base.findViewById(R.id.tvCarNum);
			}
			return tvCarNum;
		}
		
		TextView tvUnPayAmt() {
			if (tvUnPayAmt == null) {

				tvUnPayAmt = (TextView) base.findViewById(R.id.tvUnPayAmt);
			}
			return tvUnPayAmt;
		}
		
		TextView tvUnPayReason() {
			if (tvUnPayReason == null) {

				tvUnPayReason = (TextView) base.findViewById(R.id.tvUnPayReason);
			}
			return tvUnPayReason;
		}
		
		
	}

	
	/**
	COUNT :미납건수
	PIO_NUM : 주차번호
	CD_PARK : 주차장코드
	KN_PARK : 주차장명
	PARK_IN : 입차시간
	PARK_OUT : 출차시간
	AMT : 미납금액
	PIO_DAY : 주차일(20140105)
	DIS_CD : 할인코드(20140105)
	PARK_TYPE: 요금종류(일일권)
	CD_GUBUN: 미납종류(미출,도주)
	ADD_AMT : 가산금(20140105)
	SUM_AMT : 미납금누계총액(20140105)
	CD_STATE : 상태코드(2013.12.31)
	CAR_STATE : Y (압류대상), N (정상) (2014.01.08)
	KN_RESULT : 압류대상 또는 정상(2014.01.08)
	
	
 <PIO_NUM>PO101-4058-20140208-130906</PIO_NUM>
 <CD_PARK>101</CD_PARK>
 <KN_PARK>홍대서측1     </KN_PARK>
 <CAR_NO>61조9440</CAR_NO>
 <PARK_IN>2014020813:08:00</PARK_IN>
 <PARK_OUT>2014020814:44:00</PARK_OUT>
 <PARK_INOUT>13:08 ~ 14:44, 96분</PARK_INOUT>
 <AMT>2500</AMT>
 <PIO_DAY>20140208</PIO_DAY>
 <DIS_CD>02</DIS_CD>
 <PARK_TYPE>01</PARK_TYPE>
 <CD_GUBUN>도주</CD_GUBUN>
 <ADD_AMT></ADD_AMT>
 <SUM_AMT></SUM_AMT>
 <CD_STATE>O2</CD_STATE>
 <CAR_STATE>R</CAR_STATE>
 <KN_RESULT>정상</KN_RESULT>

	 */
	
	
	//TODO 미납일/차번호/미납사유  와 검색종류에 따른 타이틀 예외처리 시나리오
	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
		ViewWrapper wrapper = null;
		if(gCheckboxVisible)
		{
			row = mInflater.inflate(R.layout.unpay_manageritem_layout, parent, false);
		}
		else
		{
			row = mInflater.inflate(R.layout.unpay_manageritem_layout2, parent, false);
		}

		wrapper = new ViewWrapper(row);
		wrapper.tvUnPayDate().setText(mBean.get(position).getPioDay());
		wrapper.tvCarNum().setText(mBean.get(position).getCarNo());
		if(searchCode.equals("K3")){
			wrapper.tvUnPayAmt().setText(mBean.get(position).getSumAmt()+"\n"+mBean.get(position).getSubmitAmt());
		}else{
			wrapper.tvUnPayAmt().setText(mBean.get(position).getSumAmt());
		}
		
		wrapper.tvUnPayReason().setText(mBean.get(position).getCdGubun());

		//if(!gCheckboxVisible)
		//{

			wrapper.chkCarnum().setTag(position);
			wrapper.chkCarnum().setOnCheckedChangeListener(checkListener);
			wrapper.chkCarnum().setChecked(mBean.get(position).getChecked());
		//}
		//wrapper.chkCarnum().setChecked(true);
		row.setTag(wrapper);
		return row;
	}

	public int getCount() {
		if (mBean == null)
			return 0;
		return mBean.size();
	}

	public long getItemId(int position) {
		return 0;
	}

	@Override
	public UnPayManagerItem getItem(int position) {
		// TODO Auto-generated method stub
		return mBean.get(position);
	}

	
}