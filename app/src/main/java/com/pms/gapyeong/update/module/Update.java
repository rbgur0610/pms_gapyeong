package com.pms.gapyeong.update.module;

import android.util.Log;

import com.pms.gapyeong.common.Constants;
import com.pms.gapyeong.update.config.DownloadKey;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by hugeterry(http://hugeterry.cn)
 */
public class Update extends Thread {

    private String result;
    private String url = DownloadKey.apkUrl;

    public void run() {
        try {
            URL httpUrl = new URL(url);

            HttpURLConnection conn = (HttpURLConnection) httpUrl
                    .openConnection();
            conn.setRequestMethod("GET");
            conn.setReadTimeout(3000);

            if (conn.getResponseCode() == 200) {
                BufferedReader reader = new BufferedReader(new InputStreamReader(
                        conn.getInputStream()));
                StringBuffer sb = new StringBuffer();
                String str;

                while ((str = reader.readLine()) != null) {
                    sb.append(str);
                }
                result = new String(sb.toString().getBytes(), "utf-8");

                interpretingData(result);
            }
        } catch (MalformedURLException e) {
            if(Constants.DEBUG_PRINT_LOG){
                e.printStackTrace();
            }else{
                System.out.println("예외 발생");
            }
        } catch (IOException e) {
            if(Constants.DEBUG_PRINT_LOG){
                e.printStackTrace();
            }else{
                System.out.println("예외 발생");
            }
        }
    }

    private void interpretingData(String result) {
        try {
            JSONObject object = new JSONObject(result);
            DownloadKey.changeLog = object.getString("changelog");
            DownloadKey.version = object.getString("versionShort");
            DownloadKey.apkUrl = object.getString("installUrl");
            Log.i("UpdateFun TAG",
                    String.format("ChangeLog:%s, Version:%s, ApkDownloadUrl:%s",
                            DownloadKey.changeLog, DownloadKey.version, DownloadKey.apkUrl));
        } catch (JSONException e) {
            if(Constants.DEBUG_PRINT_LOG){
                e.printStackTrace();
            }else{
                System.out.println("예외 발생");
            }
        }
    }

}
