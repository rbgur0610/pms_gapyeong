package com.pms.gapyeong.database;

import java.util.ArrayList;
import java.util.List;

import android.util.Log;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.pms.gapyeong.common.Constants;
import com.pms.gapyeong.common.Util;
import com.pms.gapyeong.vo.PictureInfo;

public class PictureHelper extends SQLiteOpenHelper {
	
	private static final String TAG = PictureHelper.class.getSimpleName();
	
	public static final String DATABASE_NAME 	= "PICTURE_DB";
    public static final int    DATABASE_VERSION = 2;
    public static final String TBL_INFORMATION  = "picture_info";
    
    
    public static final String _ID = "_id";
    
    /**
     * 주차번호
     */    
    public static final String PIO_NUM = "pio_num";
    
    /**
     * 사진 저장 장소
     */
    public static final String PICTURE_PATH = "path";
    
    /**
     * 주차장 코드
     */
    public static final String CD_PARK = "cd_park";
    /**
     * 자동차 번호
     */
    public static final String CAR_NO = "car_no";
    
    /**
     * 사진 타입(입차, 출차, 미납??....)
     */
    public static final String PICTURE_TYPE = "type";
    
    /**
     * 사진 촬영 날짜 
     * yyyyMMdd
     */
    public static final String DATE = "date";
    
    public static final String CREATE_TBL_APPS = "CREATE TABLE "
									           + TBL_INFORMATION + " (" 
									           + _ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
									           + PIO_NUM 	  + " TEXT, 	 	 "
									           + PICTURE_PATH + " TEXT NOT NULL, "
									           + PICTURE_TYPE + " TEXT NOT NULL, "
									           + CD_PARK 	  + " TEXT NOT NULL, "
									           + CAR_NO 	  + " TEXT NOT NULL, "
									           + DATE 		  + " TEXT NOT NULL, " 
									           + " UNIQUE( " + PIO_NUM + "," + PICTURE_PATH + " )); ";
    
    private volatile static PictureHelper helper;

    public static PictureHelper getInstance(Context context) {
        if (helper == null) {
            
            synchronized (PictureHelper.class) {
                if (helper == null) {
                    helper = new PictureHelper(context);
                }
            }
        }
        return helper;
    }
    
    public PictureHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TBL_APPS);
        Log.v(TAG, CREATE_TBL_APPS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        // Logs that the database is being upgraded
        Log.w(TAG, "Upgrading database from version " + oldVersion + " to " + newVersion + ", which will destroy all old data");

        // Kills the table and existing data
        db.execSQL("DROP TABLE IF EXISTS " + TBL_INFORMATION);

        // Recreates the database with a new version
        onCreate(db);
    }
    
    /**
     * DB 에 Data 를 추가 한다.
     * @param pioNum		주차번호
     * @param path			사진 저장 장소
     * @param cdPark		자동차 번호
     * @param carNo			자동차 번호
     * @param pictureType	사진 타입(입차, 출차...)
     * @param date			사진 촬영 날짜 (yyyyMMdd)
     * @return 				insert 한 개수
     */
    public long insert(String pioNum, String path, String cdPark, String carNo, String pictureType, String date) {
    	long result = 0;
    	SQLiteDatabase db = helper.getWritableDatabase();
    	ContentValues value = new ContentValues();
    	
    	value.put(PIO_NUM, pioNum);
    	value.put(PICTURE_PATH, path);
    	value.put(CD_PARK, cdPark);
    	value.put(CAR_NO, carNo);
    	value.put(PICTURE_TYPE, pictureType);
    	value.put(DATE, date);
    	
    	Log.d(TAG, " valueSet >>> " + value.valueSet());
    	
    	result = db.insert(TBL_INFORMATION, null, value);
    	
    	db.close();
        return result;
    }
    
    /**
     * @return cursor instance
     */
    public List<PictureInfo> getSearchList(String date, String carNo, String type) {
    	ArrayList<PictureInfo> list = new ArrayList<>();
    	SQLiteDatabase db = helper.getReadableDatabase();
    	
    	StringBuffer where = new StringBuffer("");
    	where.append(" WHERE 1 = 1 ");
    	
    	if (date != null && !"".equals(date)) {
    		where.append(" AND " + DATE + " = '" + date + "'");
    	}
    	
    	if (carNo != null && !"".equals(carNo)) {
    		where.append(" AND " + CAR_NO + " LIKE '%" + carNo + "%'");
    	}
    	
    	if (type != null && !"".equals(type) && !Constants.PICTURE_TYPE_PP.equals(type)) {
    		where.append(" AND " + PICTURE_TYPE + " = '" + type + "'");
    	}
    	
    	Cursor c = db.rawQuery("SELECT * FROM " + TBL_INFORMATION + " " + where.toString(), null);
    	if (c == null || c.getCount() <= 0)
    		return list;//null이 아닌 0사이즈 리스트 반환
    	
    	list = new ArrayList<PictureInfo>(c.getCount());
    	Log.i(TAG, "select size : " + c.getCount());
    	
    	c.moveToFirst();
    	while (!c.isAfterLast()) {
    		PictureInfo item = new PictureInfo(  c.getString(c.getColumnIndex(_ID))
    										   , c.getString(c.getColumnIndex(PIO_NUM))
							    			   , c.getString(c.getColumnIndex(PICTURE_PATH))
							    			   , c.getString(c.getColumnIndex(CD_PARK))
							    			   , c.getString(c.getColumnIndex(CAR_NO))
							    			   , c.getString(c.getColumnIndex(PICTURE_TYPE))
							    			   , c.getString(c.getColumnIndex(DATE))
							    			  );
    		list.add(item);
    		c.moveToNext();
    	}
    	c.close();
    	return list;
    }
    
    /**
     * @return cursor instance
     */
    public List<PictureInfo> getSearchPioNumList(String pioNum) {
    	ArrayList<PictureInfo> list;
    	SQLiteDatabase db = helper.getReadableDatabase();
    	
    	Cursor c = db.rawQuery(" SELECT * FROM " + TBL_INFORMATION 
    						 + "  WHERE " + PIO_NUM + " = '" + pioNum + "'"
    						 + "  ORDER BY " + PICTURE_PATH
    						 , null);
    	if (c == null || c.getCount() <= 0)
    		return null;
    	
    	list = new ArrayList<PictureInfo>(c.getCount());
    	Log.i(TAG, "select size : " + c.getCount());
    	
    	c.moveToFirst();
    	while (!c.isAfterLast()) {
    		PictureInfo item = new PictureInfo(  c.getString(c.getColumnIndex(_ID))
							    			   , c.getString(c.getColumnIndex(PIO_NUM))
							    			   , c.getString(c.getColumnIndex(PICTURE_PATH))
							    			   , c.getString(c.getColumnIndex(CD_PARK))
							    			   , c.getString(c.getColumnIndex(CAR_NO))
							    			   , c.getString(c.getColumnIndex(PICTURE_TYPE))
							    			   , c.getString(c.getColumnIndex(DATE))
							    			   );
    		list.add(item);
    		c.moveToNext();
    	}
    	c.close();
    	return list;
    }
    
    /**
     * 해당 항목을 삭제 한다.
     * @param path 	사진파일
     * @return 		true : 삭제 성공, false : 삭제 실패
     */
    public boolean delete(String path) { 
    	boolean result = false;
    	
    	if (path == null)
    		throw new NullPointerException("path : " + path);
    	
    	SQLiteDatabase db = helper.getWritableDatabase();
    	result = db.delete(TBL_INFORMATION, PICTURE_PATH + "=" + "'" + path + "'", null) > 0;
    	Log.d(TAG, "delete : " + result);
    	db.close();
    	
        return result;
    } 
    
    /**
     * 해당 항목을 삭제 한다.
     * @param pioNum	주차번호
     * @param path 		사진파일
     * @return 			true : 삭제 성공, false : 삭제 실패
     */
    public boolean delete(String pioNum, String path) { 
    	boolean result = false;
    	
    	if (Util.isEmpty(pioNum) || Util.isEmpty(path)){
    		throw new NullPointerException(" pioNum : " + pioNum + " path : " + path);
    	}
    	
    	StringBuffer where = new StringBuffer("");
    	where.append(PIO_NUM + "=" + "'" + pioNum + "'");
    	where.append(" AND " + PICTURE_PATH + "=" + "'" + path  + "'");
    	
    	SQLiteDatabase db = helper.getWritableDatabase();
    	result = db.delete(TBL_INFORMATION, where.toString(), null) > 0;
    	Log.d(TAG, "delete : " + result);
    	db.close();
    	
    	return result;
    } 
    
    /**
     * 해당 항목을 삭제 한다.
     * @return 		true : 삭제 성공, false : 삭제 실패
     */
    public boolean deleteDate(String date) { 
    	boolean result = false;
    	
    	if (date == null)
    		throw new NullPointerException("date : " + date);
    	
    	SQLiteDatabase db = helper.getWritableDatabase();
    	result = db.delete(TBL_INFORMATION, DATE + "=" + "'" + date + "'", null) > 0;
    	Log.d(TAG, "delete : " + result);
    	db.close();
    	
    	return result;
    }     
    
    /**
     * 모든 항목을 삭제 한다.
     * @return 		true : 삭제 성공, false : 삭제 실패
     */
    public boolean deleteAll() { 
    	boolean result = false;
    	SQLiteDatabase db = helper.getWritableDatabase();
    	result = db.delete(TBL_INFORMATION, "", null) > 0;
    	Log.d(TAG, "delete : " + result);
    	db.close();
    	return result;
    }     

}
