package com.pms.gapyeong.database;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

/**
 * Created by songsanghyeon on 2016. 9. 1..
 */
public class Preference {
    private final String PREF_MAIN = "pref.setting";
    Context mContext;
    protected SharedPreferences mSharedPreferences;
    private AES256Cipher aesInstance;

    public Preference(Context appContext) {
        mContext = appContext;
        mSharedPreferences = mContext.getSharedPreferences(PREF_MAIN, Context.MODE_PRIVATE);
        aesInstance = AES256Cipher.getInstance();
    }

    public int getInt(String key) {
        int value = 0;
        try {
            value = Integer.parseInt(aesInstance.AES_Decode(mSharedPreferences.getString(key, "0")));
        } catch (NumberFormatException e) {
            System.out.println("NumberFormatException 예외 발생");
        } catch (UnsupportedEncodingException e) {
            System.out.println("UnsupportedEncodingException 예외 발생");
        } catch (BadPaddingException e) {
            System.out.println("BadPaddingException 예외 발생");
        } catch (NoSuchAlgorithmException e) {
            System.out.println("NoSuchAlgorithmException 예외 발생");
        } catch (InvalidKeyException e) {
            System.out.println("InvalidKeyException 예외 발생");
        } catch (InvalidAlgorithmParameterException e) {
            System.out.println("InvalidAlgorithmParameterException 예외 발생");
        } catch (NoSuchPaddingException e) {
            System.out.println("NoSuchPaddingException 예외 발생");
        } catch (IllegalBlockSizeException e) {
            System.out.println("IllegalBlockSizeException 예외 발생");
        }
        return value;
    }

    public int getInt(String key, int value) {
        int returnValue = 0;
        try {
            returnValue = Integer.parseInt(aesInstance.AES_Decode(mSharedPreferences.getString(key, String.valueOf(value))));
        } catch (NumberFormatException e) {
            System.out.println("NumberFormatException 예외 발생");
        } catch (UnsupportedEncodingException e) {
            System.out.println("UnsupportedEncodingException 예외 발생");
        } catch (BadPaddingException e) {
            System.out.println("BadPaddingException 예외 발생");
        } catch (NoSuchAlgorithmException e) {
            System.out.println("NoSuchAlgorithmException 예외 발생");
        } catch (InvalidKeyException e) {
            System.out.println("InvalidKeyException 예외 발생");
        } catch (InvalidAlgorithmParameterException e) {
            System.out.println("InvalidAlgorithmParameterException 예외 발생");
        } catch (NoSuchPaddingException e) {
            System.out.println("NoSuchPaddingException 예외 발생");
        } catch (IllegalBlockSizeException e) {
            System.out.println("IllegalBlockSizeException 예외 발생");
        }


        return returnValue;
    }

    public long getLong(String key) {
        long value = 0;
        try {
            value = Long.parseLong(aesInstance.AES_Decode(mSharedPreferences.getString(key, "0")));
        } catch (NumberFormatException e) {
            System.out.println("NumberFormatException 예외 발생");
        } catch (UnsupportedEncodingException e) {
            System.out.println("UnsupportedEncodingException 예외 발생");
        } catch (BadPaddingException e) {
            System.out.println("BadPaddingException 예외 발생");
        } catch (NoSuchAlgorithmException e) {
            System.out.println("NoSuchAlgorithmException 예외 발생");
        } catch (InvalidKeyException e) {
            System.out.println("InvalidKeyException 예외 발생");
        } catch (InvalidAlgorithmParameterException e) {
            System.out.println("InvalidAlgorithmParameterException 예외 발생");
        } catch (NoSuchPaddingException e) {
            System.out.println("NoSuchPaddingException 예외 발생");
        } catch (IllegalBlockSizeException e) {
            System.out.println("IllegalBlockSizeException 예외 발생");
        }


        return value;
    }

    public String getString(String key) {
        String value = mSharedPreferences.getString(key, "");
        String decodeValue = "";
        try {
            decodeValue = aesInstance.AES_Decode(value);
        } catch (NumberFormatException e) {
            System.out.println("NumberFormatException 예외 발생");
        } catch (UnsupportedEncodingException e) {
            System.out.println("UnsupportedEncodingException 예외 발생");
        } catch (BadPaddingException e) {
            System.out.println("BadPaddingException 예외 발생");
        } catch (NoSuchAlgorithmException e) {
            System.out.println("NoSuchAlgorithmException 예외 발생");
        } catch (InvalidKeyException e) {
            System.out.println("InvalidKeyException 예외 발생");
        } catch (InvalidAlgorithmParameterException e) {
            System.out.println("InvalidAlgorithmParameterException 예외 발생");
        } catch (NoSuchPaddingException e) {
            System.out.println("NoSuchPaddingException 예외 발생");
        } catch (IllegalBlockSizeException e) {
            System.out.println("IllegalBlockSizeException 예외 발생");
        }


        if (decodeValue.equalsIgnoreCase("null")) {
            return "";
        } else {
            return decodeValue;
        }
    }


    public String getString(String key, String defaultMsg) {
        String value = mSharedPreferences.getString(key, defaultMsg);
        String decodeValue = "";
        try {
            decodeValue = aesInstance.AES_Decode(value);
        } catch (NumberFormatException e) {
            System.out.println("NumberFormatException 예외 발생");
        } catch (UnsupportedEncodingException e) {
            System.out.println("UnsupportedEncodingException 예외 발생");
        } catch (BadPaddingException e) {
            System.out.println("BadPaddingException 예외 발생");
        } catch (NoSuchAlgorithmException e) {
            System.out.println("NoSuchAlgorithmException 예외 발생");
        } catch (InvalidKeyException e) {
            System.out.println("InvalidKeyException 예외 발생");
        } catch (InvalidAlgorithmParameterException e) {
            System.out.println("InvalidAlgorithmParameterException 예외 발생");
        } catch (NoSuchPaddingException e) {
            System.out.println("NoSuchPaddingException 예외 발생");
        } catch (IllegalBlockSizeException e) {
            System.out.println("IllegalBlockSizeException 예외 발생");
        }


        try {
            if (decodeValue.equalsIgnoreCase("null")) {
                return "";
            } else if ("".equals(decodeValue) || !"".equals(defaultMsg)) {
                return defaultMsg;
            } else {
                return decodeValue;
            }
        } catch (NullPointerException e) {
            System.out.println("NullPointerException 예외 발생");
            return "";
        }
    }

    public double getDouble(String key) {
        double value = 0;
        try {
            value = Double.parseDouble(aesInstance.AES_Decode(mSharedPreferences.getString(key, "0")));
        } catch (NumberFormatException e) {
            System.out.println("NumberFormatException 예외 발생");
        } catch (UnsupportedEncodingException e) {
            System.out.println("UnsupportedEncodingException 예외 발생");
        } catch (BadPaddingException e) {
            System.out.println("BadPaddingException 예외 발생");
        } catch (NoSuchAlgorithmException e) {
            System.out.println("NoSuchAlgorithmException 예외 발생");
        } catch (InvalidKeyException e) {
            System.out.println("InvalidKeyException 예외 발생");
        } catch (InvalidAlgorithmParameterException e) {
            System.out.println("InvalidAlgorithmParameterException 예외 발생");
        } catch (NoSuchPaddingException e) {
            System.out.println("NoSuchPaddingException 예외 발생");
        } catch (IllegalBlockSizeException e) {
            System.out.println("IllegalBlockSizeException 예외 발생");
        }
        return value;
    }

    @SuppressLint("NewApi")
    public void putInt(String key, int value) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        String strValue = "";
        try {
            strValue = aesInstance.AES_Encode(String.valueOf(value));
        } catch (NumberFormatException e) {
            System.out.println("NumberFormatException 예외 발생");
        } catch (UnsupportedEncodingException e) {
            System.out.println("UnsupportedEncodingException 예외 발생");
        } catch (BadPaddingException e) {
            System.out.println("BadPaddingException 예외 발생");
        } catch (NoSuchAlgorithmException e) {
            System.out.println("NoSuchAlgorithmException 예외 발생");
        } catch (InvalidKeyException e) {
            System.out.println("InvalidKeyException 예외 발생");
        } catch (InvalidAlgorithmParameterException e) {
            System.out.println("InvalidAlgorithmParameterException 예외 발생");
        } catch (NoSuchPaddingException e) {
            System.out.println("NoSuchPaddingException 예외 발생");
        } catch (IllegalBlockSizeException e) {
            System.out.println("IllegalBlockSizeException 예외 발생");
        }

        editor.putString(key, strValue);
        editor.apply();
    }

    public void putLong(String key, long value) {
        try {
            putString(key, String.valueOf(value));
        } catch (NullPointerException e) {
            System.out.println("NullPointerException 예외 발생");
        }
    }

    public void putDouble(String key, double value) {
        putString(key, String.valueOf(value));
    }

    public void putString(String key, String value) {

        SharedPreferences.Editor editor = mSharedPreferences.edit();
        try {
            value = aesInstance.AES_Encode(String.valueOf(value));
        } catch (NumberFormatException e) {
            System.out.println("NumberFormatException 예외 발생");
        } catch (UnsupportedEncodingException e) {
            System.out.println("UnsupportedEncodingException 예외 발생");
        } catch (BadPaddingException e) {
            System.out.println("BadPaddingException 예외 발생");
        } catch (NoSuchAlgorithmException e) {
            System.out.println("NoSuchAlgorithmException 예외 발생");
        } catch (InvalidKeyException e) {
            System.out.println("InvalidKeyException 예외 발생");
        } catch (InvalidAlgorithmParameterException e) {
            System.out.println("InvalidAlgorithmParameterException 예외 발생");
        } catch (NoSuchPaddingException e) {
            System.out.println("NoSuchPaddingException 예외 발생");
        } catch (IllegalBlockSizeException e) {
            System.out.println("IllegalBlockSizeException 예외 발생");
        }


        editor.putString(key, value);
        editor.apply();
    }

    public void putList(String key, ArrayList<String> marray) {
        String[] mystringlist = marray.toArray(new String[marray.size()]);
        putString(key, TextUtils.join("‚‗‚", mystringlist));
    }

    public void addList(String key, String marray) {

        try {
            String[] mylist = TextUtils.split(aesInstance.AES_Decode(mSharedPreferences.getString(key, "")), "‚‗‚");
            ArrayList<String> gottenlist = new ArrayList<String>(Arrays.asList(mylist));

            SharedPreferences.Editor editor = mSharedPreferences.edit();
            gottenlist.add(marray);

            editor.putString(key, aesInstance.AES_Encode(TextUtils.join("‚‗‚", gottenlist)));
            editor.apply();
        } catch (NumberFormatException e) {
            System.out.println("NumberFormatException 예외 발생");
        } catch (UnsupportedEncodingException e) {
            System.out.println("UnsupportedEncodingException 예외 발생");
        } catch (BadPaddingException e) {
            System.out.println("BadPaddingException 예외 발생");
        } catch (NoSuchAlgorithmException e) {
            System.out.println("NoSuchAlgorithmException 예외 발생");
        } catch (InvalidKeyException e) {
            System.out.println("InvalidKeyException 예외 발생");
        } catch (InvalidAlgorithmParameterException e) {
            System.out.println("InvalidAlgorithmParameterException 예외 발생");
        } catch (NoSuchPaddingException e) {
            System.out.println("NoSuchPaddingException 예외 발생");
        } catch (IllegalBlockSizeException e) {
            System.out.println("IllegalBlockSizeException 예외 발생");
        }

    }

    public void changeList(String key, String marray) {
        try {
            String[] mylist = TextUtils.split(aesInstance.AES_Decode(mSharedPreferences.getString(key, "")), "‚‗‚");
            ArrayList<String> gottenlist = new ArrayList<String>(Arrays.asList(mylist));
            SharedPreferences.Editor editor = mSharedPreferences.edit();
            String userId = marray.split("\\|\\|")[0];
            for (int i = 0; i < gottenlist.size(); i++) {
                if (gottenlist.get(i).split("\\|\\|")[0].equals(userId)) {
                    gottenlist.set(i, marray);
                }
            }
            editor.putString(key, aesInstance.AES_Encode(TextUtils.join("‚‗‚", gottenlist)));
            editor.apply();
        } catch (NumberFormatException e) {
            System.out.println("NumberFormatException 예외 발생");
        } catch (UnsupportedEncodingException e) {
            System.out.println("UnsupportedEncodingException 예외 발생");
        } catch (BadPaddingException e) {
            System.out.println("BadPaddingException 예외 발생");
        } catch (NoSuchAlgorithmException e) {
            System.out.println("NoSuchAlgorithmException 예외 발생");
        } catch (InvalidKeyException e) {
            System.out.println("InvalidKeyException 예외 발생");
        } catch (InvalidAlgorithmParameterException e) {
            System.out.println("InvalidAlgorithmParameterException 예외 발생");
        } catch (NoSuchPaddingException e) {
            System.out.println("NoSuchPaddingException 예외 발생");
        } catch (IllegalBlockSizeException e) {
            System.out.println("IllegalBlockSizeException 예외 발생");
        }

    }

    public void replaceOrAddList(String key, String marray) {
        // 암호화 이전 기존로직
        try {
            boolean checking = false;

            String[] mylist = TextUtils.split(aesInstance.AES_Decode(mSharedPreferences.getString(key, "")), "‚‗‚");
            ArrayList<String> gottenlist = new ArrayList<String>(Arrays.asList(mylist));
            SharedPreferences.Editor editor = mSharedPreferences.edit();

            String userId = marray.split("\\|\\|")[0];
            if (isValidString(userId)) {
                for (int i = 0; i < gottenlist.size(); i++) {
                    if (gottenlist.get(i).split("\\|\\|")[0].equals(userId)) {
                        checking = true;
                        gottenlist.set(i, marray);
                    }
                }

                if (!checking) {
                    gottenlist.add(marray);
                }

                editor.putString(key, aesInstance.AES_Encode(TextUtils.join("‚‗‚", gottenlist)));
                editor.apply();
            }
        } catch (NumberFormatException e) {
            System.out.println("NumberFormatException 예외 발생");
        } catch (UnsupportedEncodingException e) {
            System.out.println("UnsupportedEncodingException 예외 발생");
        } catch (BadPaddingException e) {
            System.out.println("BadPaddingException 예외 발생");
        } catch (NoSuchAlgorithmException e) {
            System.out.println("NoSuchAlgorithmException 예외 발생");
        } catch (InvalidKeyException e) {
            System.out.println("InvalidKeyException 예외 발생");
        } catch (InvalidAlgorithmParameterException e) {
            System.out.println("InvalidAlgorithmParameterException 예외 발생");
        } catch (NoSuchPaddingException e) {
            System.out.println("NoSuchPaddingException 예외 발생");
        } catch (IllegalBlockSizeException e) {
            System.out.println("IllegalBlockSizeException 예외 발생");
        }


    }

    public static boolean isValidString(String str) {
        try {
            return str != null && str.length() > 0;
        } catch (NullPointerException e) {
            System.out.println("NullPointerException 예외 발생");
            return false;
        }
    }

    public void cleanList(String key) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(key, "");
        editor.apply();
    }

    public ArrayList<String> getList(String key) {
        // 복호화 이전 기존로직
        ArrayList<String> gottenlist = new ArrayList<>();
        try {
            String[] mylist = TextUtils.split(aesInstance.AES_Decode(mSharedPreferences.getString(key, "")), "‚‗‚");
            gottenlist = new ArrayList<String>(Arrays.asList(mylist));
        } catch (NumberFormatException e) {
            System.out.println("NumberFormatException 예외 발생");
        } catch (UnsupportedEncodingException e) {
            System.out.println("UnsupportedEncodingException 예외 발생");
        } catch (BadPaddingException e) {
            System.out.println("BadPaddingException 예외 발생");
        } catch (NoSuchAlgorithmException e) {
            System.out.println("NoSuchAlgorithmException 예외 발생");
        } catch (InvalidKeyException e) {
            System.out.println("InvalidKeyException 예외 발생");
        } catch (InvalidAlgorithmParameterException e) {
            System.out.println("InvalidAlgorithmParameterException 예외 발생");
        } catch (NoSuchPaddingException e) {
            System.out.println("NoSuchPaddingException 예외 발생");
        } catch (IllegalBlockSizeException e) {
            System.out.println("IllegalBlockSizeException 예외 발생");
        }
        return gottenlist;
    }

    public void putListInt(String key, ArrayList<Integer> marray, Context context) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        Integer[] mystringlist = marray.toArray(new Integer[marray.size()]);
        editor.putString(key, TextUtils.join("‚‗‚", mystringlist));
        editor.apply();
    }

    public ArrayList<Integer> getListInt(String key,
                                         Context context) {
        String[] mylist = TextUtils
                .split(mSharedPreferences.getString(key, ""), "‚‗‚");
        ArrayList<String> gottenlist = new ArrayList<String>(
                Arrays.asList(mylist));
        ArrayList<Integer> gottenlist2 = new ArrayList<Integer>();
        for (int i = 0; i < gottenlist.size(); i++) {
            gottenlist2.add(Integer.parseInt(gottenlist.get(i)));
        }

        return gottenlist2;
    }

    public void putListBoolean(String key, ArrayList<Boolean> marray) {
        ArrayList<String> origList = new ArrayList<String>();
        for (Boolean b : marray) {
            if (b == true) {
                origList.add("true");
            } else {
                origList.add("false");
            }
        }
        putList(key, origList);
    }

    public ArrayList<Boolean> getListBoolean(String key) {
        ArrayList<String> origList = getList(key);
        ArrayList<Boolean> mBools = new ArrayList<Boolean>();
        for (String b : origList) {
            if (b.equals("true")) {
                mBools.add(true);
            } else {
                mBools.add(false);
            }
        }
        return mBools;
    }

    public void putBoolean(String key, boolean value) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putBoolean(key, value);
        editor.apply();
    }

    public boolean getBoolean(String key) {
        return mSharedPreferences.getBoolean(key, false);
    }

    // Default True
    public boolean getBooleanReverse(String key) {
        return mSharedPreferences.getBoolean(key, true);
    }

    public void putFloat(String key, float value) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putFloat(key, value);
        editor.apply();
    }

    public float getFloat(String key) {
        return mSharedPreferences.getFloat(key, 0f);
    }

    public void remove(String key) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.remove(key);
        editor.apply();
    }

    public Boolean deleteImage(String path) {
        File tobedeletedImage = new File(path);
        Boolean isDeleted = tobedeletedImage.delete();
        return isDeleted;
    }

    public void clear() {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.clear();
        editor.apply();
    }

    public Map<String, ?> getAll() {
        return mSharedPreferences.getAll();
    }

    public void registerOnSharedPreferenceChangeListener(SharedPreferences.OnSharedPreferenceChangeListener listener) {
        mSharedPreferences.registerOnSharedPreferenceChangeListener(listener);
    }

    public void unregisterOnSharedPreferenceChangeListener(SharedPreferences.OnSharedPreferenceChangeListener listener) {
        mSharedPreferences.unregisterOnSharedPreferenceChangeListener(listener);
    }


}
