package com.pms.gapyeong.database;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.pms.gapyeong.common.Util;
import com.pms.gapyeong.vo.ParkIO;
import com.pms.gapyeong.vo.StatusBoard;

public class StatusBoardHelper {

    private static final String TAG = StatusBoardHelper.class.getSimpleName();

    private JSONArray mJsonArr;

    private ArrayList<StatusBoard> boardList;
    private volatile static StatusBoardHelper helper;
    private int maxCount;

    public int getMaxCount() {
        return maxCount;
    }

    public void setMaxCount(int maxCount) {
        this.maxCount = maxCount;
    }

    public static StatusBoardHelper getInstance(int maxCount) {
        if (helper == null) {

            synchronized (StatusBoardHelper.class) {
                if (helper == null) {
                    helper = new StatusBoardHelper(maxCount);
                }
            }
        }
        return helper;
    }


    public static void destroy() {
        helper = null;
    }

    public static StatusBoardHelper getInstance() {
        return helper;
    }

    public StatusBoardHelper(int maxCount) {
        this.maxCount = maxCount;

        if (boardList == null)
            boardList = new ArrayList<StatusBoard>(maxCount);

        for (int i = 0; i < maxCount; i++) {
            boardList.add(new StatusBoard());
        }
    }

    public void clear() {
        if (boardList != null) {
            boardList.clear();
        } else {
            boardList = new ArrayList<>();
        }

        for (int i = 0; i < maxCount; i++) {
            boardList.add(new StatusBoard());
        }
    }

    public void setParkIo(ArrayList<ParkIO> ioList) {

        if (ioList != null && ioList.size() > 0) {
            for (int i = 0; i < ioList.size(); i++) {
                ParkIO io = ioList.get(i);

                // boardList 는 board 의 인덱스로 번호를 매기고 있어서
                // 실제 code Area 번호 - 1은  boardList 의 인덱스 이다.
                if (!Util.isEmpty(io.getCdArea())) {
                    boardList.get(Util.sToi(io.getCdArea()) - 1).setParkIO(io);
                }
            }
        }
    }

    public ArrayList<String> getEmptyNumberList() {
        ArrayList<String> list = new ArrayList<String>();

        for (int i = 0; i < maxCount; i++) {
            if (boardList.get(i).isEmpty()) {
                list.add(new String("" + (i + 1)));
            }
        }

        return list;
    }

    public int getEmptyNumber() {
        for (int i = 0; i < maxCount; i++) {
            if (boardList.get(i).isEmpty()) {
                return i + 1;
            }
        }

        return -1;
    }

    public List<StatusBoard> getList() {
        return boardList;
    }

    ///////// NEW
    public void setParkIoJson(JSONArray jsonArr) {

        if (jsonArr != null) {
            mJsonArr = jsonArr;
        } else {
            mJsonArr = null;
            return;
        }

        int Len = mJsonArr.length();

        ArrayList<ParkIO> list = new ArrayList<ParkIO>();

        // 초기화
        clear();

        try {
            for (int i = 0; i < Len; i++) {
                JSONObject res = mJsonArr.getJSONObject(i);
                ParkIO info = new ParkIO(res.optString("PIO_NUM")
                        , res.optString("CD_PARK")
                        , res.optString("PIO_DAY")
                        , res.optString("CAR_NO")
                        , res.optString("CD_AREA")
                        , res.optString("PARK_TYPE")
                        , res.optString("DIS_CD")
                        , res.optString("TEL")
                        , res.optString("PARK_IN_AMT")
                        , res.optString("PARK_OUT_AMT")
                        , res.optString("PARK_AMT")
                        , res.optString("ADV_IN_AMT")
                        , res.optString("ADV_OUT_AMT")
                        , res.optString("ADV_AMT")
                        , res.optString("DIS_IN_AMT")
                        , res.optString("DIS_OUT_AMT")
                        , res.optString("DIS_AMT")
                        , res.optString("CASH_IN_AMT")
                        , res.optString("CASH_OUT_AMT")
                        , res.optString("CASH_AMT")
                        , res.optString("CARD_IN_AMT")
                        , res.optString("CARD_OUT_AMT")
                        , res.optString("CARD_AMT")
                        , res.optString("TRUNC_IN_AMT")
                        , res.optString("TRUNC_OUT_AMT")
                        , res.optString("TRUNC_AMT")
                        , res.optString("COUPON_AMT")
                        , res.optString("RETURN_AMT")
                        , res.optString("YET_AMT")
                        , res.optString("REAL_AMT")
                        , res.optString("PARK_TIME")
                        , res.optString("PARK_IN")
                        , res.optString("PARK_OUT")
                        , res.optString("CARD_NO")
                        , res.optString("CARD_COMPANY")
                        , res.optString("APPROVAL_NO")
                        , res.optString("APPROVAL_DATE")
                        , res.optString("CHARGE_TYPE")
                        , res.optString("KN_COMMENT"));
                list.add(info);
                // Log.d(TAG, " CarNumber >>> " + info.getCarNumber() + " info >>> " + info.toString());
            }
        } catch (JSONException e) {
            System.out.println("JSONException 예외 발생");
        }

        if (list != null && list.size() > 0) {
            setParkIo(list);
        }

    }

    public JSONArray getParkIoJsonArr() {
        return mJsonArr;
    }

    public JSONObject getParkIoJson(int index) {
        if (mJsonArr != null) {
            try {
                return mJsonArr.getJSONObject(index);
            } catch (JSONException e) {
                System.out.println("JSONException 예외 발생");
                return null;
            }
        } else {
            return null;
        }
    }

}
