package com.pms.gapyeong.database;

import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.pms.gapyeong.common.Constants;

public class CodeHelper extends SQLiteOpenHelper {
private static final String TAG = CodeHelper.class.getSimpleName();
	
	public static final String DATABASE_NAME 	= "CODE_DB";
    public static final int    DATABASE_VERSION = 4;
    public static final String TBL_INFORMATION  = "common_code";
    
    public static final String _ID 		= "_id";
    public static final String CD_CLASS = "cd_class";
    public static final String KN_CLASS = "kn_class";
    public static final String CD_CODE  = "cd_code";
    public static final String KN_CODE  = "kn_code";
    public static final String CD_VALUE = "cd_value";
    public static final String CD_VALUE2 = "cd_value2";
    public static final String CD_VALUE3 = "cd_value3";
    public static final String DIS_RATE = "dis_rate";
    public static final String DIS_AMT  = "dis_amt";
    public static final String DIS_TIME = "dis_time";
    public static final String CREATE_TBL_APPS = "CREATE TABLE "
									            + TBL_INFORMATION + " (" 
									            + _ID + " INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, "
									            + CD_CLASS + " TEXT NOT NULL, "
									            + KN_CLASS + " TEXT,  "
									            + CD_CODE  + " TEXT NOT NULL, "
									            + KN_CODE  + " TEXT,  "
									            + CD_VALUE + " TEXT,  "
									            + CD_VALUE2 + " TEXT,  "
									            + CD_VALUE3 + " TEXT,  "
									            + DIS_RATE + " TEXT,  "
									            + DIS_AMT  + " TEXT,  "
									            + DIS_TIME + " TEXT,  "
									            + " UNIQUE ( cd_class, cd_code ) ); ";
    
    private volatile static CodeHelper helper;

    public static CodeHelper getInstance(Context context) {
        if (helper == null) {
            
            synchronized (CodeHelper.class) {
                if (helper == null) {
                    helper = new CodeHelper(context);
                }
            }
        }
        return helper;
    }
    
    public CodeHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    
	@Override
	public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TBL_APPS);
        Log.v(TAG, CREATE_TBL_APPS);
    }

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        // Logs that the database is being upgraded
        Log.w(TAG, "Upgrading database from version " + oldVersion + " to " + newVersion + ", which will destroy all old data");

        // Kills the table and existing datas
        db.execSQL("DROP TABLE IF EXISTS " + TBL_INFORMATION);

        // Recreates the database with a new version
        onCreate(db);
    }

	/**
	 * DB 에 Data 를 추가 한다.
	 * @param cdClass			공통코드 유형
	 * @param knClass			공통코드 이름
	 * @param code				CommonCode 객체
	 * @return 					insert 한 개수
	 */
    public long insert(String cdClass, String knClass, String cdCode,  String knCode,
    				   String cdValue, String cdValue2, String cdValue3, String disRate, 
    				   String disAmt, String disTime) {
    	
    	long result = 0;
    	SQLiteDatabase db = helper.getWritableDatabase();
    	ContentValues value = new ContentValues();
    	
    	value.put(CD_CLASS,  cdClass);
    	value.put(KN_CLASS,  knClass);
    	value.put(CD_CODE,   cdCode);
    	value.put(KN_CODE,   knCode);
    	value.put(CD_VALUE,  cdValue);
    	value.put(CD_VALUE2, cdValue2);
    	value.put(CD_VALUE3, cdValue3);
    	value.put(DIS_RATE,  disRate);
    	value.put(DIS_AMT,   disAmt);
    	value.put(DIS_TIME,  disTime);
    	
    	result = db.insert(TBL_INFORMATION, null, value);
    	db.close();
    	return result;
    }
    
    public boolean isExist() {
    	boolean result = false; 
    	SQLiteDatabase db = getWritableDatabase();
        Cursor c = db.rawQuery("SELECT * FROM " + TBL_INFORMATION, null);
        result = (c.getCount() > 0 ? true : false);
        c.close();
        return result;
    }
    
    public void dropTable() {
    	SQLiteDatabase db = getWritableDatabase();
    	db.execSQL("DROP TABLE IF EXISTS " + TBL_INFORMATION);
    	onCreate(db);
    }
    
    /**
     * DB 에서 cdClass 의 code list 를 가져온다.
     * @return
     */
    public ArrayList<JSONObject> getList() {
    	ArrayList<JSONObject> list = new ArrayList<JSONObject>();
    	SQLiteDatabase db = helper.getReadableDatabase();
    	
    	Cursor c = db.rawQuery("select Distinct " + CD_CLASS 
    							+ " from " + TBL_INFORMATION, null);
    	if (c == null || c.getCount() <= 0)
    		return null;
    	
    	Log.e(TAG, "list : " + c.getCount());
    	c.moveToFirst();
    	while (!c.isAfterLast()) {
    		StringBuffer where = new StringBuffer(" where " + CD_CLASS + " = " + "'" + c.getString(c.getColumnIndex(CD_CLASS)) + "'");
        	
        	Cursor itemCursor = db.rawQuery("SELECT * FROM " + TBL_INFORMATION + where.toString(), null);
        	if (itemCursor == null || itemCursor.getCount() <= 0)
        		return null;
        	
        	itemCursor.moveToFirst();
        	
        	
        	while (!itemCursor.isAfterLast()) {
        		try {
        			JSONObject json = new JSONObject();
					json.put("CD_CODE",  itemCursor.getString(itemCursor.getColumnIndex(CD_CODE)));
					json.put("KN_CODE",  itemCursor.getString(itemCursor.getColumnIndex(KN_CODE)));
					json.put("CD_VALUE", itemCursor.getString(itemCursor.getColumnIndex(CD_VALUE)));
					json.put("DIS_RATE", itemCursor.getString(itemCursor.getColumnIndex(DIS_RATE)));
					json.put("DIS_AMT",  itemCursor.getString(itemCursor.getColumnIndex(DIS_AMT)));
					json.put("DIS_TIME", itemCursor.getString(itemCursor.getColumnIndex(DIS_TIME)));
					list.add(json);
					itemCursor.moveToNext();
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					if(Constants.DEBUG_PRINT_LOG){
						e.printStackTrace();
					}else{
						System.out.println("예외 발생");
					}
				}
			}
    		itemCursor.close();
    		c.moveToNext();
    	}
    	
    	c.close();
    	return list;
    }
    
    public ArrayList<JSONObject> getList(String codeClass) {
    	ArrayList<JSONObject> list = new ArrayList<JSONObject>();
    	SQLiteDatabase db = helper.getReadableDatabase();
    	
		StringBuffer where = new StringBuffer(" where " + CD_CLASS + " = " + "'" + codeClass + "'");
    	
    	Cursor itemCursor = db.rawQuery("SELECT * FROM " + TBL_INFORMATION + where.toString(), null);
    	if (itemCursor == null)
    		return null;
    	
    	if (itemCursor.getCount() <= 0) {
    		itemCursor.close();
    		return null;
    	}
    	
    	itemCursor.moveToFirst();
    	
    	while (!itemCursor.isAfterLast()) {
    		try {
    			JSONObject json = new JSONObject();
    			json.put("CD_CODE",  itemCursor.getString(itemCursor.getColumnIndex(CD_CODE)));
				json.put("KN_CODE",  itemCursor.getString(itemCursor.getColumnIndex(KN_CODE)));
				json.put("CD_VALUE", itemCursor.getString(itemCursor.getColumnIndex(CD_VALUE)));
				json.put("DIS_RATE", itemCursor.getString(itemCursor.getColumnIndex(DIS_RATE)));
				json.put("DIS_AMT",  itemCursor.getString(itemCursor.getColumnIndex(DIS_AMT)));
				json.put("DIS_TIME", itemCursor.getString(itemCursor.getColumnIndex(DIS_TIME)));
				list.add(json);
				itemCursor.moveToNext();
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				if(Constants.DEBUG_PRINT_LOG){
					e.printStackTrace();
				}else{
					System.out.println("예외 발생");
				}
			}
		}
    	
    	itemCursor.close();
    	return list;
    }
    
    public String findCodeByKnCode(String codeClass, String knCode) {
    	if(knCode.equals(""))
    		return "";
    	
    	
    	String result = "";
    	SQLiteDatabase db = helper.getReadableDatabase();
    	
    	Log.d(TAG, "codeClass : " + codeClass + ",knCode : " + knCode);
		StringBuffer where = new StringBuffer(" where " + CD_CLASS + " = " + "'" + codeClass + "'");
    	
    	Cursor itemCursor = db.rawQuery("SELECT * FROM " + TBL_INFORMATION + where.toString(), null);
    	if (itemCursor == null)
    		return "";
    	
    	if (itemCursor.getCount() <= 0) {
    		itemCursor.close();
    		return "";
    	}
    	
    	itemCursor.moveToFirst();
    	while (!itemCursor.isAfterLast()) {
    		if (knCode.equals(itemCursor.getString(itemCursor.getColumnIndex(KN_CODE)))) {
    			result = itemCursor.getString(itemCursor.getColumnIndex(CD_CODE));
    			Log.e(TAG, "result : " + result);
    			break;
    		}
    		itemCursor.moveToNext();
		}
    	
    	itemCursor.close();
    	return result;
    }
    
    public String findCodeByCdCode(String codeClass, String cdCode) {
    	
    	if(cdCode.equals(""))
    		return "";
    	
    	String result = "";
    	SQLiteDatabase db = helper.getReadableDatabase();
    	
    	
    	Log.e(TAG, "codeClass : " + codeClass + ",knCode : " + cdCode);
		StringBuffer where = new StringBuffer(" where " + CD_CLASS + " = " + "'" + codeClass + "'");
   		where.append(" AND " + CD_CODE + " = " + "'" + cdCode + "'");
    	
    	Cursor itemCursor = db.rawQuery("SELECT * FROM " + TBL_INFORMATION + where.toString(), null);
    	if (itemCursor == null)
    		return "";
    	
    	if (itemCursor.getCount() <= 0) {
    		itemCursor.close();
    		return "";
    	}
    	
    	if(itemCursor.moveToFirst()) {
     	   do {
     		   result = itemCursor.getString(itemCursor.getColumnIndex(KN_CODE));
     		   Log.d(TAG, " findCodeByCdCode result : " + result);
     	   } while(itemCursor.moveToNext()); 
     	}   
    	
    	itemCursor.moveToFirst();
    	
    	itemCursor.close();
    	return result;
    }
    
    public String findCodeByDisRate(String codeClass, String knCode) {
    	if(knCode.equals(""))
    		return "";
    	
    	String result = "";
    	SQLiteDatabase db = helper.getReadableDatabase();
    	
    	Log.e(TAG, "codeClass : " + codeClass + ",knCode : " + knCode);
    	StringBuffer where = new StringBuffer(" where " + CD_CLASS + " = " + "'" + codeClass + "'");
    	
    	Cursor itemCursor = db.rawQuery("SELECT * FROM " + TBL_INFORMATION + where.toString(), null);
    	if (itemCursor == null)
    		return "";
    	
    	if (itemCursor.getCount() <= 0) {
    		itemCursor.close();
    		return "";
    	}
    	
    	itemCursor.moveToFirst();
    	while (!itemCursor.isAfterLast()) {
    		if (knCode.equals(itemCursor.getString(itemCursor.getColumnIndex(KN_CODE)))) {
    			result = itemCursor.getString(itemCursor.getColumnIndex(DIS_RATE));
    			Log.d(TAG, " findCodeByDisRate result : " + result);
    			break;
    		}
    		itemCursor.moveToNext();
    	}
    	itemCursor.close();
    	return result;
    }
    
    public String searchCodeDisRate(String codeClass, String cdCode) {
    	if(cdCode.equals(""))
    		return "";
    	
    	String result = "";
    	SQLiteDatabase db = helper.getReadableDatabase();
    	
    	Log.d(TAG, "codeClass : " + codeClass + ",cdCode : " + cdCode);
    	StringBuffer where = new StringBuffer(" where " + CD_CLASS + " = " + "'" + codeClass + "'");
    	where.append(" AND " + CD_CODE + " = " + "'" + cdCode + "'");
    	
    	Cursor itemCursor = db.rawQuery("SELECT * FROM " + TBL_INFORMATION + where.toString(), null);
    	if (itemCursor == null)
    		return "";
    	
    	if (itemCursor.getCount() <= 0) {
    		itemCursor.close();
    		return "";
    	}
    	
    	if(itemCursor.moveToFirst()) {
     	   do {
     		   result = itemCursor.getString(itemCursor.getColumnIndex(DIS_RATE));
     		   Log.d(TAG, " searchCodeDisRate result : " + result);
     	   } while(itemCursor.moveToNext()); 
     	}    	
    	
    	return result;
    }
    
    public String findCodeByDisAmt(String codeClass, String knCode) {
    	if(knCode.equals(""))
    		return "";
    	
    	String result = "";
    	SQLiteDatabase db = helper.getReadableDatabase();
    	
    	Log.e(TAG, "codeClass : " + codeClass + ",knCode : " + knCode);
    	StringBuffer where = new StringBuffer(" where " + CD_CLASS + " = " + "'" + codeClass + "'");
    	
    	Cursor itemCursor = db.rawQuery("SELECT * FROM " + TBL_INFORMATION + where.toString(), null);
    	if (itemCursor == null)
    		return "";
    	
    	if (itemCursor.getCount() <= 0) {
    		itemCursor.close();
    		return "";
    	}
    	
    	itemCursor.moveToFirst();
    	while (!itemCursor.isAfterLast()) {
    		if (knCode.equals(itemCursor.getString(itemCursor.getColumnIndex(KN_CODE)))) {
    			result = itemCursor.getString(itemCursor.getColumnIndex(DIS_AMT));
    			Log.d(TAG, " findCodeByDisAmt result : " + result);
    			break;
    		}
    		itemCursor.moveToNext();
    	}
    	itemCursor.close();
    	return result;
    }
    
    public String searchCodeDisAmt(String codeClass, String cdCode) {
    	if(cdCode.equals(""))
    		return "";
    	
    	String result = "";
    	SQLiteDatabase db = helper.getReadableDatabase();
    	
    	Log.d(TAG, "codeClass : " + codeClass + ",cdCode : " + cdCode);
    	StringBuffer where = new StringBuffer(" where " + CD_CLASS + " = " + "'" + codeClass + "'");
    	where.append(" AND " + CD_CODE + " = " + "'" + cdCode + "'");
    	    	
    	Cursor itemCursor = db.rawQuery("SELECT * FROM " + TBL_INFORMATION + where.toString(), null);
    	if (itemCursor == null)
    		return "";
    	
    	if (itemCursor.getCount() <= 0) {
    		itemCursor.close();
    		return "";
    	}
    	
    	if(itemCursor.moveToFirst()) {
     	   do {
     		   result = itemCursor.getString(itemCursor.getColumnIndex(DIS_AMT));
     		   Log.d(TAG, " searchCodeDisAmt result : " + result);
     	   } while(itemCursor.moveToNext()); 
     	}
    	
    	itemCursor.close();
    	return result;
    }
    
    
    public String findCodeByDisTime(String codeClass, String knCode) {
       	if (knCode.equals(""))
    		return "";
       	
    	String result = "";
    	SQLiteDatabase db = helper.getReadableDatabase();
    	
    	Log.e(TAG, "codeClass : " + codeClass + ",knCode : " + knCode);
		StringBuffer where = new StringBuffer(" where " + CD_CLASS + " = " + "'" + codeClass + "'");
   		where.append(" AND " + KN_CODE + " = " + "'" + knCode + "'");
    	
    	Cursor itemCursor = db.rawQuery("SELECT * FROM " + TBL_INFORMATION + where.toString(), null);
    	if (itemCursor == null)
    		return "";
    	
    	if (itemCursor.getCount() <= 0) {
    		itemCursor.close();
    		return "";
    	}
    	
    	itemCursor.moveToFirst();
    	while (!itemCursor.isAfterLast()) {
    		if (knCode.equals(itemCursor.getString(itemCursor.getColumnIndex(KN_CODE)))) {
    			result = itemCursor.getString(itemCursor.getColumnIndex(DIS_TIME));
    			Log.d(TAG, "result : " + result);
    			break;
    		}
    		itemCursor.moveToNext();
		}
    	
    	itemCursor.close();
    	return result;
    }
    
    public String searchCodeDisTime(String codeClass, String cdCode) {
    	if (cdCode.equals(""))
    		return "";
    	
    	String result = "";
    	SQLiteDatabase db = helper.getReadableDatabase();
    	
    	Log.e(TAG, "codeClass : " + codeClass + ",cdCode : " + cdCode);
    	StringBuffer where = new StringBuffer(" where " + CD_CLASS + " = " + "'" + codeClass + "'");
    	where.append(" AND " + CD_CODE + " = " + "'" + cdCode + "'");
    	
    	Cursor itemCursor = db.rawQuery("SELECT * FROM " + TBL_INFORMATION + where.toString(), null);
    	if (itemCursor == null)
    		return "";
    	
    	if (itemCursor.getCount() <= 0) {
    		itemCursor.close();
    		return "";
    	}
    	
    	if(itemCursor.moveToFirst()) {
    	   do {
    		   result = itemCursor.getString(itemCursor.getColumnIndex(DIS_TIME));
    		   Log.d(TAG, "searchCodeDisTime result : " + result);
    	   } while(itemCursor.moveToNext()); 
    	}
    	
    	itemCursor.close();
    	return result;
    }
    
    public String findCodeValue(String codeClass, String cdCode) {
    	if(cdCode.equals(""))
    		return "";
    	
    	String result = "";
    	SQLiteDatabase db = helper.getReadableDatabase();
    	
    	Log.e(TAG, "codeClass : " + codeClass + ",cdCode : " + cdCode);
		StringBuffer where = new StringBuffer(" where " + CD_CLASS + " = " + "'" + codeClass + "'");
		where.append(" AND " + CD_CODE + " = " + "'" + cdCode + "'");
		
    	Cursor itemCursor = db.rawQuery("SELECT * FROM " + TBL_INFORMATION + where.toString(), null);
    	if (itemCursor == null)
    		return "";
    	
    	if (itemCursor.getCount() <= 0) {
    		itemCursor.close();
    		return "";
    	}
    	
    	if(itemCursor.moveToFirst()) {
     	   do {
     		   result = itemCursor.getString(itemCursor.getColumnIndex(CD_VALUE));
     		   Log.d(TAG, "findCodeValue result : " + result);
     	   } while(itemCursor.moveToNext()); 
     	}    	
    	
    	itemCursor.close();
    	return result;		
		
    }

	public String findCodeValue2(String codeClass, String cdCode) {
		if(cdCode.equals(""))
			return "";

		String result = "";
		SQLiteDatabase db = helper.getReadableDatabase();

		Log.e(TAG, "codeClass : " + codeClass + ",cdCode : " + cdCode);
		StringBuffer where = new StringBuffer(" where " + CD_CLASS + " = " + "'" + codeClass + "'");
		where.append(" AND " + CD_CODE + " = " + "'" + cdCode + "'");

		Cursor itemCursor = db.rawQuery("SELECT * FROM " + TBL_INFORMATION + where.toString(), null);
		if (itemCursor == null)
			return "";

		if (itemCursor.getCount() <= 0) {
			itemCursor.close();
			return "";
		}

		if(itemCursor.moveToFirst()) {
			do {
				result = itemCursor.getString(itemCursor.getColumnIndex(CD_VALUE2));
				Log.d(TAG, "findCodeValue result : " + result);
			} while(itemCursor.moveToNext());
		}

		itemCursor.close();
		return result;

	}
    
}
