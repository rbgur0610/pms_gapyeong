package com.pms.gapyeong.database;

import android.os.Build;
import android.text.TextUtils;
import android.util.Base64;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class AES256Cipher {
    private static volatile AES256Cipher INSTANCE;

    final static String secretKey = "12345678901234567890123456789012";
    static String IV = "";

    private AES256Cipher(){
        IV = secretKey.substring(0,16);
    }

    public static AES256Cipher getInstance(){
        if(INSTANCE == null){
            synchronized (AES256Cipher.class){
                if(INSTANCE == null)
                    INSTANCE = new AES256Cipher();
            }
        }
        return INSTANCE;
    }
    // 2018-07-25 jhlee AES 암호화
    public String AES_Encode(String str) throws UnsupportedEncodingException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidAlgorithmParameterException,
            InvalidKeyException, IllegalBlockSizeException, BadPaddingException {

        if(TextUtils.isEmpty(str))
            return "";

        byte[] keyData = secretKey.getBytes();

        SecretKey secretKey = new SecretKeySpec(keyData,"AES");
        Cipher c = Cipher.getInstance("AES/CBC/PKCS5Padding");
        c.init(Cipher.ENCRYPT_MODE,secretKey, new IvParameterSpec(IV.getBytes()));
        byte[] encrypted = new byte[0];
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            encrypted = c.doFinal(str.getBytes(StandardCharsets.UTF_8));
        }else{
            encrypted = c.doFinal(str.getBytes("UTF-8"));
        }
        String enStr = new String(Base64.encode(encrypted,0));

        return enStr;
    }

    // 2018-07-25 jhlee AES 복호화
    public String AES_Decode(String str) throws UnsupportedEncodingException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidAlgorithmParameterException,
            InvalidKeyException, IllegalBlockSizeException, BadPaddingException {

        if(TextUtils.isEmpty(str))
            return "";

        byte[] keyData = secretKey.getBytes();

        SecretKey secretKey = new SecretKeySpec(keyData,"AES");

        Cipher c = Cipher.getInstance("AES/CBC/PKCS5Padding");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            c.init(Cipher.DECRYPT_MODE,secretKey, new IvParameterSpec(IV.getBytes(StandardCharsets.UTF_8)));
        }else{
            c.init(Cipher.DECRYPT_MODE,secretKey, new IvParameterSpec(IV.getBytes("UTF-8")));
        }

        byte[] byteStr = Base64.decode(str.getBytes(),0);

        String deStr = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            deStr = new String(c.doFinal(byteStr), StandardCharsets.UTF_8);
        }else{
            deStr = new String(c.doFinal(byteStr), "UTF-8");
        }

        return deStr;
    }
}
