package com.pms.gapyeong.common;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.pie.common.PieUtil;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

public class Util extends PieUtil {
	
    public static String getYmdhms(String pattern) {
    	// yyyy-MM-dd HH:mm:ss
    	pattern = isEmpty(pattern) ? "yyyy-MM-dd" : pattern;
    	java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat(pattern, new Locale("ko", "KOREA"));
    	return formatter.format(new Date());
    }
	public static String getDeviceID(Context context){
		String USER_DEVICE_ID = DeviceInfo.getDeviceId(context, context.getContentResolver());
		Log.d("111", " USER_DEVICE_ID >>>::: " + USER_DEVICE_ID);
//		USER_DEVICE_ID = "00000000-0060-2297-ffff-ffffb37760f1";
		USER_DEVICE_ID = "00000000-7497-2631-0033-c5870033c587";
		return  USER_DEVICE_ID;
	}

	public static String getYmdhms(String pattern, Date date) {
    	//yyyy-MM-dd HH:mm:ss
    	pattern = isEmpty(pattern) ? "yyyy-MM-dd" : pattern;
    	java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat(pattern, new Locale("ko", "KOREA"));
    	return formatter.format(date);
    }	
    
    public static String preDay() {
		Calendar currentDate = Calendar.getInstance();
		currentDate.add(Calendar.DATE, -1);
		return getYmdhms("yyyy-MM-dd", currentDate.getTime());
    }




    public static String preDay(String pattern) {
    	return getYmdhms(pattern);
    }	    
	
    public static String toDay() {
    	return getYmdhms("yyyy-MM-dd");
    }
    
    public static String toDay(String pattern) {
    	return getYmdhms(pattern);
    }
    
    public static String toDayTime(){
    	return getYmdhms("yyyy-MM-dd HH:mm");
    }

	public static String getDayTime(){
		return getYmdhms("yyyyMMddHHmmss");
	}

	public static Calendar nextMonth(int value) {
		Calendar currentDate = Calendar.getInstance();
		currentDate.add(Calendar.MONTH, value);
		return currentDate;
	}

	public static String nextDay() {
		Calendar currentDate = Calendar.getInstance();
		currentDate.add(Calendar.DATE, +1);
		return getYmdhms("yyyy-MM-dd", currentDate.getTime());
    }
    
    public static String nextDay(String pattern) {
		Calendar currentDate = Calendar.getInstance();
		currentDate.add(Calendar.DATE, +1);
		return getYmdhms(pattern, currentDate.getTime());    	
    }

    public static String addDay(int addDay) {
		Calendar currentDate = Calendar.getInstance();
		currentDate.add(Calendar.DATE, addDay);
		return getYmdhms("yyyy-MM-dd", currentDate.getTime());    
    }
    
    public static String addDay(int addDay,String pattern) {
		Calendar currentDate = Calendar.getInstance();
		currentDate.add(Calendar.DATE, addDay);
		return getYmdhms(pattern, currentDate.getTime());  		
    }
        
	public static long diffOfDate(String begin, String end)
	{
		 try {  
			java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss", new Locale("ko", "KOREA"));
			java.util.Date beginDate = null;
			java.util.Date endDate = null;
			beginDate = formatter.parse(begin);
			endDate   = formatter.parse(end);
			long diff = endDate.getTime() - beginDate.getTime();
			long diffDays = diff / (24 * 60 * 60 * 1000);		
			return diffDays;
		 } catch (ParseException e) {
			 if(Constants.DEBUG_PRINT_LOG){
				 e.printStackTrace();
			 }else{
				 System.out.println("예외 발생");
			 }
			return 0;
		 }		  
	}    
    
    public static long diffOfMinutes(String begin, String end) throws ParseException
    {
    	SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss", new Locale("ko", "KOREA"));
      	Date beginDate = formatter.parse(begin);
      	Date endDate   = formatter.parse(end);
      	return (endDate.getTime() - beginDate.getTime()) / (60 * 1000);
    }	
    
    public static long diffOfTime(String begin, String end) throws Exception
    {
    	SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss", new Locale("ko", "KOREA"));
    	Date beginDate = formatter.parse(begin);
    	Date endDate   = formatter.parse(end);
    	return (endDate.getTime() - beginDate.getTime()) / (60 * 1000);
    }	    	
	
    public static String addMinusDate(String dd, int count, int tic) {
		String today = "";
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", new Locale("ko", "KOREA"));
		Date date;
		try {
			date = sdf.parse(dd);
			cal.setTime(date);
			switch(tic) {
			case 0:
				cal.add(Calendar.MINUTE, count);
				break;
			case 1:
				cal.add(Calendar.HOUR, count);
				break;
			case 2:
				cal.add(Calendar.MONTH, count);
				break;
			}
			today = sdf.format(cal.getTime());
		} catch(ParseException e) {
			if(Constants.DEBUG_PRINT_LOG){
				e.printStackTrace();
			}else{
				System.out.println("예외 발생");
			}
		}
		return today;
    }
   
    
    public static String getMinute(String date1, String date2)
    {
        String result = "0";
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss", new Locale("ko", "KOREA"));
        try
        {
            java.util.Date d1 = sdf.parse(date1);
            java.util.Date d2 = sdf.parse(date2);
            Calendar c1 = Calendar.getInstance();
            Calendar c2 = Calendar.getInstance();
            c1.setTime(d1);
            c2.setTime(d2);
            result = Long.toString((c2.getTimeInMillis() - c1.getTimeInMillis()) / 60000L);
        }
        catch(ParseException e)
        {
			if(Constants.DEBUG_PRINT_LOG){
				e.printStackTrace();
			}else{
				System.out.println("예외 발생");
			}
        }
        return result;
    }
	
	public static String cardnumString(String src) {
		if (isEmpty(src)) {
			return "";
		} else {
			String cardnum = src.replaceAll(" ", "").replaceAll("-", "");
			if(src.length() >= 16){
				cardnum = cardnum.substring(0, 4)  + "-"
						+ "****" + "-" 
						+ "****" + "-"
						+ cardnum.substring(12,16);
			} else if(src.length() == 8){
				cardnum = cardnum.substring(0, 4)  + "-"
						+ "****" + "-" 
						+ "****" + "-"
						+ cardnum.substring(4);
			}
			return cardnum;
		}
	}	    
    
	public static String dateString(String src) {
		if (isEmpty(src)) {
			return "";
		} else {
			String date = src.replaceAll(" ", "").replaceAll("/", "").replaceAll("-", "").replaceAll(":", "");
			if(date.length() >= 12){
				date = date.substring(0, 4) + "-"
					 + date.substring(4, 6) + "-" 
					 + date.substring(6, 8) + " "
					 + date.substring(8, 10) + ":"
					 + date.substring(10, 12);
			} else if(date.length() == 8){
				date = date.substring(0, 4) + "-"
					 + date.substring(4, 6) + "-" 
					 + date.substring(6, 8);
			} else if(date.length() == 6){
				date = date.substring(0, 4) + "-"
					 + date.substring(4, 6);
			} else {
				date = src;
			}
			return date;			
		}
	}
	
	public static String dateStringExt(String src) {
		if (isEmpty(src)) {
			return "";
		} else {
			String date = src.replaceAll(" ", "").replaceAll("/", "").replaceAll("-", "").replaceAll(":", "");
			if(date.length() >= 14){
				date = date.substring(0, 4) + "-"
					 + date.substring(4, 6) + "-" 
					 + date.substring(6, 8) + " "
					 + date.substring(8, 10) + ":"
					 + date.substring(10, 12) + ":"
					 + date.substring(12, 14);
			} else if(date.length() >= 12){
				date = date.substring(0, 4) + "-"
					 + date.substring(4, 6) + "-" 
					 + date.substring(6, 8) + " "
					 + date.substring(8, 10) + ":"
					 + date.substring(10, 12);	
			} else if(date.length() == 8){
				date = date.substring(0, 4) + "-"
					 + date.substring(4, 6) + "-" 
					 + date.substring(6, 8);
			} else if(date.length() == 6){
				date = date.substring(0, 4) + "-"
					 + date.substring(4, 6);
			} else {
				date = src;
			}
			return date;			
		}
	}
	
	public static String timeString(String src) {
		if (isEmpty(src)) {
			return "";
		} else {
			String date = src.replaceAll(" ", "").replaceAll(":", "");
			if(date.length() == 6){
				date = date.substring(0, 2) + ":"
					 + date.substring(2, 4) + ":"
					 + date.substring(4, 6);
			} else if(date.length() == 4){
				date = date.substring(0, 2) + ":"
						+ date.substring(2, 4);
			} else {
				date = src;
			}
			return date;			
		}
	}    
    
	   // 숫자인지 체크
    public boolean isNumberic(String str) {
        Pattern pattern = Pattern.compile("[+-]?\\d+");
        return pattern.matcher(str).matches();
    }	
	
	  public static boolean PasswrodValidate(final String pwd) {
		  // 패스워드 패턴 체크... 영문 숫자 조합 대소문자 8자이상 구분
		  final String PASSWROD_PATTERN = "^(?=.*[a-zA-Z]+)(?=.*[!@#$%^*+=-]|.*[0-9]+).{8,20}$";
		  Pattern pattern = Pattern.compile(PASSWROD_PATTERN);
		  Matcher matcher = pattern.matcher(pwd);
		  return matcher.matches();
	  }
	  
	  public static boolean EmailValidate(final String email) {
		  final String EMAIL_PATTERN = "^[_a-z0-9-]+(.[_a-z0-9-]+)*@(?:\\w+\\.)+\\w+$";
		  Pattern p = Pattern.compile(EMAIL_PATTERN);
		  Matcher matcher = p.matcher(email);
		  return matcher.matches();
	  }
    
	  public static ArrayList<String> emailList ()
	  {
		  ArrayList<String> List = new ArrayList<String>();  		  
		  List.add("naver.com");
		  List.add("daum.net");
		  List.add("hanmail.net");
		  List.add("nate.com");
		  List.add("gmail.com");
		  List.add("chol.com");
		  List.add("dreamwiz.com");
		  List.add("empal.com");
		  List.add("직접입력");
		  return List;
	  }		  
	  
	  public static ArrayList<String> yearList ()
	  {
		  ArrayList<String> List = new ArrayList<String>();  
		  for(int i=1950; i<=2013; i++){
			  List.add(""+i);
		  }
		  return List;
	  }		  
	  
	  public static ArrayList<String> mmList ()
	  {
		  ArrayList<String> List = new ArrayList<String>();  
		  for(int i=01; i<=12; i++){
			  List.add(""+i);
		  }
		  return List;
	  }		  
	  
	  public static ArrayList<String> ddList ()
	  {
		  ArrayList<String> List = new ArrayList<String>();  
		  for(int i=01; i<=31; i++){
			  List.add(""+i);
		  }
		  return List;
	  }		  
	  
	  public static Typeface loadTypeface(Activity activty){
		  final String TYPEFACE_NAME = "RIXGOM.TTF";
		  if(activty != null) {
			  return Typeface.createFromAsset(activty.getAssets(), TYPEFACE_NAME);
		  } else {
			  return null;
		  }
	  }		  
	  	  
	  // 특수 문자 제거
	  public static String removeEtcStr(String str){       
	      String match = "[^\uAC00-\uD7A3xfe0-9a-zA-Z\\s]";
	      str =str.replaceAll(match,"");
	      return str;
	  }
	  
	  // 공백 제거
	  public static String removeSpaceStr(String str){       
		  String match = "\\p{Space}";
		  str =str.replaceAll(match,"");
		  return str;
	  }
	  
	  //  공백 및 특수 문자 제거
	  public static String removeSpaceEtcStr(String str){       
		  str = removeSpaceStr(removeEtcStr(str));
		  return str;
	  }	  
	  
	  public static String getEncodeReplace(String str) {
		  if(!isEmpty(str)){
			 return str.replaceAll("%23", "#")
					   .replaceAll("%2B", "+")
				       .replaceAll("%26", "&")
				       .replaceAll("%25", "%")
				       .replaceAll("%20", " ");
		  } else {
			  return  str;
		  }
	  }
	  
	  public static String getEncodeStr(String str) {
		return getEncodeStr(str, "UTF-8");
	  }	  
	  
	  public static String getEncodeStr(String str, String charset) {
		  String encodeStr = str;
		  if(!isEmpty(encodeStr)){
			  try {
				return  URLEncoder.encode(encodeStr, charset);
			} catch (UnsupportedEncodingException e) {
				  if(Constants.DEBUG_PRINT_LOG){
					  e.printStackTrace();
				  }else{
					  System.out.println("예외 발생");
				  }
			}  
		  } else {
			  return  encodeStr;
		  }
		  return encodeStr;
	  }	  
	  
	  public static String getEncodeImageUrl(String url) throws UnsupportedEncodingException {
		  String path =  url.substring(0, url.lastIndexOf("/")+1);
		  String name =  url.substring(url.lastIndexOf("/")+1, url.length());
		  return path + getEncodeReplace(getEncodeStr(name));
	  }	  
	  
	  // 네트워크 상태 체크 함수 ::: 0 - 둘 중 하나 , 1 - 3G, 2 - WIFI
	  public static boolean isNetworkConnected(Context context){
		  return isNetworkConnected(context, 0);
	  }
	
	  public static boolean isNetworkConnected(Context context, int state){
	      boolean isConnected = false;

	      ConnectivityManager manager = 
	        (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
	    
	      NetworkInfo mobile = manager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
	      NetworkInfo wifi = manager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

	      if(state == 0) {
		    	if (mobile.isConnected() || wifi.isConnected()){
		    		isConnected = true;
		    	}
	      } else if(state == 1) {
		    	if (mobile.isConnected()){
		    		isConnected = true;
		    	}
	      } else if(state == 2) {
		    	if (wifi.isConnected()){
		    		isConnected = true;
		    	}
	      }
	    
	      return isConnected;
	  }	  
	  
	  public static void soundAssetPlay (Context context, String filename)
	  {
		  MediaPlayer player;
		    try {
		    	AssetFileDescriptor afd = context.getAssets().openFd(filename);
		    	player = new MediaPlayer();
//				player.setDataSource(afd.getFileDescriptor());
				player.setDataSource(afd.getFileDescriptor(),afd.getStartOffset(),afd.getLength());
				player.prepare();
				player.setVolume(1, 1);
				player.start();
			} catch (IllegalArgumentException e) {
				if(Constants.DEBUG_PRINT_LOG){
					e.printStackTrace();
				}else{
					System.out.println("예외 발생");
				}
			} catch (IllegalStateException e) {
				if(Constants.DEBUG_PRINT_LOG){
					e.printStackTrace();
				}else{
					System.out.println("예외 발생");
				}
			} catch (IOException e) {
				if(Constants.DEBUG_PRINT_LOG){
					e.printStackTrace();
				}else{
					System.out.println("예외 발생");
				}
			}		  
	  }
	  
	  public static void soundMediaPlay (String filename)
	  {
			MediaPlayer player; 
			player = new MediaPlayer();
			 
			//sd 카드에 있는경우
			//단말상의 패키지 안에 음원이 있을경우 리소스(res/raw)폴더에 넣어 R.java에 등록 후 사용하는것이 좋고 sd카드에 음원이 있을경우 다음과
			//같이 사용한다
			try {
				player.setDataSource(filename);
				player.prepare();
				player.setVolume(1, 1);
				player.start();
			} catch (IllegalArgumentException e) {
				if(Constants.DEBUG_PRINT_LOG){
					e.printStackTrace();
				}else{
					System.out.println("예외 발생");
				}
			} catch (IllegalStateException e) {
				if(Constants.DEBUG_PRINT_LOG){
					e.printStackTrace();
				}else{
					System.out.println("예외 발생");
				}
			} catch (IOException e) {
				if(Constants.DEBUG_PRINT_LOG){
					e.printStackTrace();
				}else{
					System.out.println("예외 발생");
				}
			}
	  }
	  
	  public static void openWebView (Activity activity, String url)
	  {
		  	Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
		  	activity.startActivity(intent);
	  }	  

	  
	  
		/**
	     * 양쪽으로 자리수만큼 문자 채우기
	     *
	     * @param   str         원본 문자열
	     * @param   size        총 문자열 사이즈(리턴받을 결과의 문자열 크기)
	     * @param   strFillText 원본 문자열 외에 남는 사이즈만큼을 채울 문자
	     * @return  
	     */
	    public static String getCPad(String str, int size, String strFillText) {
	        int intPadPos = 0;
	        for(int i = (str.getBytes()).length; i < size; i++) {
	            if(intPadPos == 0) {
	                str += strFillText;
	                intPadPos = 1;
	            } else {
	                str = strFillText + str;
	                intPadPos = 0;
	            }
	        }
	        return str;
	    }
	 

	    /**
	     * 왼쪽으로 자리수만큼 문자 채우기
	     *
	     * @param   str         원본 문자열
	     * @param   size        총 문자열 사이즈(리턴받을 결과의 문자열 크기)
	     * @param   strFillText 원본 문자열 외에 남는 사이즈만큼을 채울 문자
	     * @return  
	     */
	    public static String getLPad(String str, int size, String strFillText) {
	        for(int i = (str.getBytes()).length; i < size; i++) {
	            str = strFillText + str;
	        }
	        return str;
	    }
	 

	    /**
	     * 오른쪽으로 자리수만큼 문자 채우기
	     *
	     * @param   str         원본 문자열
	     * @param   size        총 문자열 사이즈(리턴받을 결과의 문자열 크기)
	     * @param   strFillText 원본 문자열 외에 남는 사이즈만큼을 채울 문자
	     * @return  
	     */
	    public static String getRPad(String str, int size, String strFillText) {
	        for(int i = (str.getBytes()).length; i < size; i++) {
	            str += strFillText;
	        }
	        return str;
	    }

	    public static String getDate(int year, int mon, int day) {
	    	String y, m, d;
	    	y = String.valueOf(year);
	    	m = String.valueOf(mon);
	    	d = String.valueOf(day);
	    	return y + (m.length() <= 1 ? (0+m) : m) + (d.length() <= 1 ? (0+d) : d);  
	    }
	    
		public static double getDouble(String val){
			if(isEmpty(val)){
				return 0;
			}
			return Double.parseDouble(val);
		}		
		
		public static String addZero(int val){
			if(String.valueOf(val).length()==1){
				return "0"+String.valueOf(val);
			}
			return String.valueOf(val);
		}
		
		public static String replaceMoney(String val){
			String money = "";
			try{
				if (val == null || val.equals("")){
					val = "0";
				}
				
				val = val.replace("원","").replace(",","");
				
				money = java.text.NumberFormat.getNumberInstance().format(Integer.parseInt(val.trim()));
			}catch (NullPointerException e) {
				money = val;
				if(Constants.DEBUG_PRINT_LOG){
					e.printStackTrace();
				}else{
					System.out.println("예외 발생");
				}
			}
			return money;
		}
		
		public static String replaceMoney(int val){
			String str = String.valueOf(val);
			
			String money = "";
			try{
				if(str.equals("")){
					str = "0";
				}
				
				money = java.text.NumberFormat.getNumberInstance().format(Integer.parseInt(str));
			}catch (NullPointerException e) {
				money = str;
				if(Constants.DEBUG_PRINT_LOG){
					e.printStackTrace();
				}else{
					System.out.println("예외 발생");
				}
			}
			return money;
		}
		
		public static String replaceNullIntent(Bundle eB,String key){
			if(eB == null || eB.getString(key) == null)
				return "";
			
			return eB.getString(key);
		}		
		

}
