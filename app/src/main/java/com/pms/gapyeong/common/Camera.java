package com.pms.gapyeong.common;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import android.app.Activity;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.MediaStore.Images;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.pms.gapyeong.R;

public class Camera {
	
	public final static int TAKE_PICTURE 	  = 0;
	public final static int SELECT_IMAGE 	  = 1;
	public final static int CROP_IMAGE 		  = 2;
	public final static int SELECT_CROP_IMAGE = 3;
	
	private Activity actContext;
	private static Uri outputFileUri;
	
	public Camera(Activity ctx) {
		actContext = ctx;
	}
	
	public static String imagePath;

	/**
	 * 카메라에서 촬영한 이미지 패스 셋팅 
	 * @param filename
	 */
	public void takePhoto(String filename) {
		imagePath = "";
		if (hasStorage(true)) {

			Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

			String sdPath = Constants.IMG_SAVE_PATH + "/" + Util.getYmdhms("yyyyMMdd");
			File file = new File(sdPath);
			if (!file.exists()) {
				file.mkdir();
			}

			File f = new File(file.getAbsolutePath() + "/" + filename + ".jpg");
//			outputFileUri = Uri.fromFile(f);

			outputFileUri = FileProvider.getUriForFile(actContext, actContext.getApplicationContext().getPackageName() + ".provider", f);

			imagePath = f.getAbsolutePath();

			Log.e("imagePath  ",imagePath.toString());

			intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
			intent.putExtra("return-data", true);
			if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP) {
				intent.setClipData(ClipData.newRawUri("", outputFileUri));
				intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION|Intent.FLAG_GRANT_READ_URI_PERMISSION);
			}
			actContext.startActivityForResult(intent, TAKE_PICTURE);
		} else {
			Toast.makeText(actContext,actContext.getResources().getString(R.string.msg_fail_take_picture),Toast.LENGTH_LONG).show();
		}
	}

	public void selectImage() {
		Intent intent = new Intent();
		intent.setType("image/*");
		intent.setAction(Intent.ACTION_GET_CONTENT);
		actContext.startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_CROP_IMAGE);
	}
	
	private boolean hasStorage(boolean requireWriteAccess) {
		String state = Environment.getExternalStorageState();
		if (Environment.MEDIA_MOUNTED.equals(state)) {
			if (requireWriteAccess) {
				boolean writable = checkFsWritable();
				return writable;
			} else {
				return true;
			}
		} else if (!requireWriteAccess && Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
			return true;
		}
		return false;
	}

	private boolean checkFsWritable() {
		// Create a temporary file to see whether a volume is really writeable.
		// It's important not to put it in the root directory which may have a
		// limit on the number of files.
		 
		String directoryName = Constants.IMG_SAVE_PATH + "/" + Util.getYmdhms("yyyyMMdd");
		File directory = new File(directoryName);
		if (!directory.isDirectory()) {
			if (!directory.mkdirs()) {
				return false;
			}
		}
		
		File f = new File(directoryName, ".probe");
		
		try {
			// Remove stale file if any
			if (f.exists()) {
				f.delete();
			}
			if (!f.createNewFile()) {
				return false;
			}
			f.delete();
			return true;
		} catch (IOException ex) {
			return false;
		}
	}
	
	public Uri getTakePhotoUri() {
		return outputFileUri;
	}
	
	private int mOutputXSize = 350;
	private int mOutputYSize = 350;
	private ArrayList<ImageInfo> mTitleArray;
	
	public void takePictureResult(){
		try {
			File f = new File(outputFileUri.getPath());
			if(f!=null){
				outputFileUri = Uri.parse(android.provider.MediaStore.Images.Media.insertImage(actContext.getContentResolver(), f.getAbsolutePath(), null, null));
				Intent intent = new Intent("com.android.camera.action.CROP",outputFileUri);
				intent.putExtra("crop", "true");
				intent.putExtra("aspectX", 1);
				intent.putExtra("aspectY", 1);
				intent.putExtra("outputX", mOutputXSize);
				intent.putExtra("outputY", mOutputYSize);
				intent.putExtra("return-data", true);
				actContext.startActivityForResult(intent, CROP_IMAGE);
			}
			
		} catch (FileNotFoundException e) {
			if(Constants.DEBUG_PRINT_LOG){
				e.printStackTrace();
			}else{
				System.out.println("예외 발생");
			}
		} catch (Exception e) {
			if(Constants.DEBUG_PRINT_LOG){
				e.printStackTrace();
			}else{
				System.out.println("예외 발생");
			}
		}
	}
	
	//앨범에서 선택
	public void selectCropResult(Intent data){
		try {
			if (data!= null) {
				outputFileUri = data.getData();
				Intent intent = new Intent("com.android.camera.action.CROP", outputFileUri);
				if (data.getStringExtra("mimeType") != null) {
					intent.setDataAndType(data.getData(), data.getStringExtra("mimeType"));
				}
				intent.putExtra("crop", "true");
				intent.putExtra("aspectX", 1);
				intent.putExtra("aspectY", 1);
				intent.putExtra("outputX", 350);
				intent.putExtra("outputY", 350);
				intent.putExtra("return-data", true);
				actContext.startActivityForResult(intent, CROP_IMAGE);
			}

		} catch (OutOfMemoryError e) {
			if(Constants.DEBUG_PRINT_LOG){
				e.printStackTrace();
			}else{
				System.out.println("예외 발생");
			}
		} catch (Exception e) {
			if(Constants.DEBUG_PRINT_LOG){
				e.printStackTrace();
			}else{
				System.out.println("예외 발생");
			}
		}
	}
	
	public void selectImageResult(Intent data,View v){
		
		try {
			if (data.getData() != null) {
				outputFileUri = data.getData();
				Bitmap photo = Images.Media.getBitmap(actContext.getContentResolver(), data.getData());

				if (photo != null) {
					v.setBackgroundDrawable(ImageLoader.bitmapToDrawable(photo));
					photo.recycle();
				}
			}else{
				Log.e("data..null","null...");
			}
		} catch (FileNotFoundException e) {
			if(Constants.DEBUG_PRINT_LOG){
				e.printStackTrace();
			}else{
				System.out.println("예외 발생");
			}
		} catch (IOException e) {
			if(Constants.DEBUG_PRINT_LOG){
				e.printStackTrace();
			}else{
				System.out.println("예외 발생");
			}
		} catch (OutOfMemoryError e) {
			if(Constants.DEBUG_PRINT_LOG){
				e.printStackTrace();
			}else{
				System.out.println("예외 발생");
			}
			outputFileUri = data.getData();
			Intent intent = new Intent("com.android.camera.action.CROP",outputFileUri);
			if (data.getStringExtra("mimeType") != null) {
				intent.setDataAndType(data.getData(), data.getStringExtra("mimeType"));
			}
			intent.putExtra("crop", "true");
			intent.putExtra("aspectX", 1);
			intent.putExtra("aspectY", 1);
			intent.putExtra("outputX", 350);
			intent.putExtra("outputY", 350);
			intent.putExtra("return-data", true);
			actContext.startActivityForResult(intent, CROP_IMAGE);
		} catch (Exception e) {
			if(Constants.DEBUG_PRINT_LOG){
				e.printStackTrace();
			}else{
				System.out.println("예외 발생");
			}
		}
	}
	
	class ImageInfo{
		/**	버킷의 이름 */
		String BUCKET_DISPLAY_NAME ="";
		/**	버킷 ID */
		String BUCKET_ID ="";
		/** 촬영날짜. 1/1000초 단위 */
		String DATE_TAKEN ="";
		/**	Image에 대한 설명 */
		String DESCRIPTION ="";
		/**	공개 여부 */
		String IS_PRIVATE ="";
		/**	위도 */
		String LATITUDE ="";
		/**	경도 */
		String LONGITUDE ="";
		/**	작은 썸네일 */
		String MINI_THUMB_MAGIC ="";
		/**	사진의 방향. 0, 90, 180, 270 */
		String ORIENTATION ="";
		/**	피카사에서 매기는 ID */
		String PICASA_ID ="";
		/**	레코드의 PK */
		String _ID ="";
		/** 데이터 스트림. 파일의 경로 */
		String DATA ="";
		/** 파일표시명 */
		String DISPLAY_NAME ="";
		/**	 제목 */
		String TITLE ="";
		/**	파일의 크기 */
		String SIZE ="";
		/**	마임 타입 */
		String MIME_TYPE ="";
		/**최후 갱신 날짜. 초단위 */
		String DATE_MODIFIED ="";
		/**추가 날짜. 초단위*/
		String DATE_ADDED ="";
	}
	
	 public ArrayList<ImageInfo> getImagesInfo (Context ctx)
	    {
		 
		 mTitleArray = new ArrayList<ImageInfo>();
		 ImageInfo ImageInfo = new ImageInfo();

	    	Cursor mManagedCursor;
	    	
	    	mManagedCursor = ctx.getContentResolver().query(Images.Media.EXTERNAL_CONTENT_URI , null, null, null, null) ;
	    	
	    	if(mManagedCursor != null)
	    	{
	    		mManagedCursor.moveToFirst();
	    		
	    		int nSize = mManagedCursor.getColumnCount();
	    		
	    		while (true)
	    		{
	    			ImageInfo.BUCKET_DISPLAY_NAME =
	    				mManagedCursor.getString(
	    						mManagedCursor.getColumnIndex(
	    								Images.ImageColumns.BUCKET_DISPLAY_NAME)); // 버킷의 이름
	    			ImageInfo.BUCKET_ID =
	    				mManagedCursor.getString(
	    						mManagedCursor.getColumnIndex(
	    								Images.ImageColumns.BUCKET_ID)); // 버킷 ID
	    			ImageInfo.DATE_TAKEN =
	    				mManagedCursor.getString(
	    						mManagedCursor.getColumnIndex(
	    								Images.ImageColumns.DATE_TAKEN)); // 촬영날짜. 1/1000초 단위
	    			ImageInfo.DESCRIPTION =
	    				mManagedCursor.getString(
	    						mManagedCursor.getColumnIndex(
	    								Images.ImageColumns.DESCRIPTION)); // Image에 대한 설명
	    			ImageInfo.IS_PRIVATE =
	    				mManagedCursor.getString(
	    						mManagedCursor.getColumnIndex(
	    								Images.ImageColumns.IS_PRIVATE)); // 공개 여부
	    			ImageInfo.LATITUDE = 
	    				mManagedCursor.getString(
	    						mManagedCursor.getColumnIndex(
	    								Images.ImageColumns.LATITUDE)); // 위도
	    			ImageInfo.LONGITUDE =
	    				mManagedCursor.getString(
	    						mManagedCursor.getColumnIndex(
	    								Images.ImageColumns.LONGITUDE)); // 경도
	    			ImageInfo.MINI_THUMB_MAGIC =
	    				mManagedCursor.getString(
	    						mManagedCursor.getColumnIndex(
	    								Images.ImageColumns.MINI_THUMB_MAGIC)); // 작은 썸네일
	    			ImageInfo.ORIENTATION = 
	    				mManagedCursor.getString(
	    						mManagedCursor.getColumnIndex(
	    								Images.ImageColumns.ORIENTATION)); // 사진의 방향. 0, 90, 180, 270
	    			ImageInfo.PICASA_ID =
	    				mManagedCursor.getString(
	    						mManagedCursor.getColumnIndex(
	    								Images.ImageColumns.PICASA_ID)); // 피카사에서 매기는 ID
	    			ImageInfo._ID =
	    				mManagedCursor.getString(
	    						mManagedCursor.getColumnIndex(
	    								Images.ImageColumns._ID)); // 레코드의 PK
	    			ImageInfo.DATA = 
	    				mManagedCursor.getString(
	    						mManagedCursor.getColumnIndex(
	    								Images.ImageColumns.DATA)); // 데이터 스트림. 파일의 경로
	    			ImageInfo.TITLE =
	    				mManagedCursor.getString(
	    						mManagedCursor.getColumnIndex(
	    								Images.ImageColumns.TITLE)); // 제목
	    			ImageInfo.SIZE = 
	    				mManagedCursor.getString(
	    						mManagedCursor.getColumnIndex(
	    								Images.ImageColumns.SIZE)); // 파일의 크기
	    			ImageInfo.MIME_TYPE =
	    				mManagedCursor.getString(
	    						mManagedCursor.getColumnIndex(
	    								Images.ImageColumns.MIME_TYPE)); // 마임 타입
	    			ImageInfo.DISPLAY_NAME =
	    				mManagedCursor.getString(
	    						mManagedCursor.getColumnIndex(
	    								Images.ImageColumns.DISPLAY_NAME)); // 파일 표시명
	    			ImageInfo.DATE_MODIFIED =
	    				mManagedCursor.getString(
	    						mManagedCursor.getColumnIndex(Images.ImageColumns.DATE_MODIFIED)); // 최후 갱신 날짜. 초단위
	    			ImageInfo.DATE_ADDED =
	    				mManagedCursor.getString(
	    						mManagedCursor.getColumnIndex(
	    								Images.ImageColumns.DATE_ADDED)); // 추가 날짜. 초단위
	    			
	    			mTitleArray.add(ImageInfo);
	    			
	    			if (mManagedCursor.isLast())
	    			{
	    				break;
	    			}
	    			else
	    			{
	    				mManagedCursor.moveToNext();
	    			}
	    		}
	    	}
	    	
	    	
	    	return mTitleArray;
	    }

}

/*
 * startIntent result exemple

	private final static int TAKE_PICTURE = 0;
	private final static int SELECT_IMAGE = 1;
	private final static int CROP_IMAGE = 2;
	private final static int SELECT_CROP_IMAGE = 3;
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		
		switch (requestCode) {
		case TAKE_PICTURE:
			cUtil.takePictureResult();
			break;
		case SELECT_CROP_IMAGE:
			cUtil.selectCropResult(data);
			break;
		case CROP_IMAGE:
			cUtil.cropImageResult(data,btn_camera);
			break;
		};
	}
	
 */

