package com.pms.gapyeong.common;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.util.Log;

import java.util.Iterator;
import java.util.List;
@SuppressLint("WrongConstant")
public class ApplicationInfo {

    static final String TAG = ApplicationInfo.class.getSimpleName();

    public static String getAppVersion(Context context) {
        String version = "";
        String name = "";
        name = context.getPackageName();
        try {
            PackageInfo i = context.getPackageManager().getPackageInfo(name, 0);
            version = i.versionName != null ? i.versionName : "";
        } catch (android.content.pm.PackageManager.NameNotFoundException e) {
            if(Constants.DEBUG_PRINT_LOG){
                e.printStackTrace();
            }else{
                System.out.println("예외 발생");
            }
            version = "";
        }
        return version;
    }

    public static String getAppVersion(Context context, String packageName) {
        String version = "";
        String name = "";
        if (packageName == null || "".equals(packageName))
            name = context.getPackageName();
        else
            name = packageName;
        try {
            PackageInfo i = context.getPackageManager().getPackageInfo(name, 0);
            version = i.versionName != null ? i.versionName : "";
        } catch (android.content.pm.PackageManager.NameNotFoundException e) {
            if(Constants.DEBUG_PRINT_LOG){
                e.printStackTrace();
            }else{
                System.out.println("예외 발생");
            }
            version = "";
        }
        return version;
    }

    public static String getSDKVersion(Context cont) {
        int version = android.os.Build.VERSION.SDK_INT;
        String ret = String.valueOf(version);
        return ret;
    }

    public static String getAppPackageName(Context context) {
        String packageName = "";
        try {
            PackageInfo i = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            packageName = i.packageName;
        } catch (android.content.pm.PackageManager.NameNotFoundException namenotfoundexception) {
            System.out.println("NameNotFoundException 예외 발생");
        }
        return packageName;
    }

    public static boolean isInstalledPackage(Context context, String packageName) {
        PackageManager mamager = context.getPackageManager();
        PackageInfo info = null;
        try {
            info = mamager.getPackageInfo(packageName, 0);
            Log.d(TAG, (new StringBuilder("info : ")).append(info != null).toString());
        } catch (NameNotFoundException e) {
            if(Constants.DEBUG_PRINT_LOG){
                e.printStackTrace();
            }else{
                System.out.println("예외 발생");
            }
        }

        return info != null;
    }


    public static boolean isRunningActivity(Context context, String activityName) {
        ActivityManager activityManager = (ActivityManager) context.getSystemService("activity");
        List info = activityManager.getRunningTasks(20);
        for (Iterator iterator = info.iterator(); iterator.hasNext(); ) {
            android.app.ActivityManager.RunningTaskInfo runningTaskInfo = (android.app.ActivityManager.RunningTaskInfo) iterator.next();
            if (runningTaskInfo.topActivity.getClassName().equals(activityName))
                return true;
        }

        return false;
    }

    public static boolean isRunningBaseActivity(Context context, String baseActivityName) {
        ActivityManager activityManager = (ActivityManager) context.getSystemService("activity");
        List info = activityManager.getRunningTasks(20);
        for (Iterator iterator = info.iterator(); iterator.hasNext(); ) {
            android.app.ActivityManager.RunningTaskInfo runningTaskInfo = (android.app.ActivityManager.RunningTaskInfo) iterator.next();
            if (runningTaskInfo.baseActivity.getClassName().equals(baseActivityName))
                return true;
        }

        return false;
    }

    public static String getTopActivityName(Context context) {
        ActivityManager activityManager = (ActivityManager) context.getSystemService("activity");
        android.app.ActivityManager.RunningTaskInfo runningTaskInfo = (android.app.ActivityManager.RunningTaskInfo) activityManager.getRunningTasks(1).get(0);
        return runningTaskInfo.topActivity.getClassName();
    }

    public static boolean isTopActivity(Context context, String activityName) {
        return activityName.equals(getTopActivityName(context));
    }

    public static int getNumRunningTask(Context context, String className) {
        ActivityManager activityManager = (ActivityManager) context.getSystemService("activity");
        List info = activityManager.getRunningTasks(20);
        int taskIndex = 0;
        for (Iterator iterator = info.iterator(); iterator.hasNext(); ) {
            android.app.ActivityManager.RunningTaskInfo runningTaskInfo = (android.app.ActivityManager.RunningTaskInfo) iterator.next();
            if (runningTaskInfo.topActivity.getClassName().equals(className))
                return taskIndex;
            taskIndex++;
        }

        return -1;
    }

    public static boolean isApplacationTop(Context context, String appPackageName) {
        ActivityManager activityManager = (ActivityManager) context.getSystemService("activity");
        List info = activityManager.getRunningTasks(1);
        Iterator iterator = info.iterator();
        android.app.ActivityManager.RunningTaskInfo runningTaskInfo = (android.app.ActivityManager.RunningTaskInfo) iterator.next();
        return runningTaskInfo.topActivity.getPackageName().equals(appPackageName);
    }

    //  앱 설치되어있는제 체크
    public static Boolean isAppExist(Activity activity, String packageName) {
        PackageManager pm = activity.getPackageManager();
        boolean isExist = false;
        try {
            pm.getPackageInfo(packageName, PackageManager.GET_ACTIVITIES);
            isExist = true;
        } catch (NameNotFoundException e) {
            if(Constants.DEBUG_PRINT_LOG){
                e.printStackTrace();
            }else{
                System.out.println("예외 발생");
            }
            isExist = false;
        }
        return isExist;
    }


}
