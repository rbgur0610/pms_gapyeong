package com.pms.gapyeong.common;

import android.os.Environment;

import com.pie.common.PieConstants;

public class Constants extends PieConstants {

    public static final String APK_FILE = "/pms_gapyeong_v3.apk";

    public static final String TAG = "pms_gapyeong_dev_v3"; // 기본태그

    public static boolean DEBUG_PRINT_LOG = false;

//	public static final String SERVER_HOST = "http://gccs.dev.parkingstyle.co.kr:6211"; // 개발서버 호스트

    public static final String SERVER_HOST = "https://gapyeong.parkingstyle.co.kr:7020";
    public static final String API_SERVER = SERVER_HOST + "/gapyeong_api_v3/serviceApi.do";
    public static final String PAY_SERVER = SERVER_HOST + "/road/service.pay"; // 개발 결제서버 호스트
    public static final String CASH_RECEIPT_SERVER = SERVER_HOST + "/road/service.receipt"; // 개발 결제서버 호스트

    public static final boolean isDevMode = false;   // 개발 모드 여부
    public static final boolean isCarPicture = true;   // 입차사진 필수여부 (true:필수,false:선택)
    public static final boolean isTicketPicture = false;   // 정기권사진 필수여부 (true:필수,false:선택)
    public static final boolean isWorkPicture = false;   // 출근사진 사용여부(true:사용,false:미사용)
    public static final String isWorkPictureStartTime = "0700"; // 출근사진 촬영 시작시간
    public static final String isWorkPictureEndTime = "1600"; // 출근사진 촬영 종료시간
    public static final boolean isParkInCancel = false;   // 입차취소 가능여부  (true:입차취소가능,false:입차취소불가)
    public static final boolean isTicketCancel = true;   // 정기권취소 가능여부(true:정기권취소가능,false:정기권취소불가)
    public static final boolean isCashReceipt = false;  // 현금 영수증 사용여부(true:사용,false:미사용)
    public static final boolean isCardAuth = true;   // 카드 결제 사용여부(true:사용,false:미사용)
    public static final boolean isSetOutTime = false;  // 출차 시간 조정여부(true:가능,false:불가)
    public static final boolean isReceiptPrint = true;

    public static final String IMG_SAVE_PATH = Environment.getExternalStorageDirectory().getAbsolutePath() + "/PMS/IMAGE";

    // 조회 할 리스트 갯수
    public static final int LIST_SIZE = 50;

    public static final String DATA_EMPTY = "조회 된 데이터가 없습니다.";
    public static final String DATA_FAIL = "데이터 조회에 실패하였습니다.";
    public static final String RE_SHOOT_FAIL = "재촬영이 불가입니다.";

    public static final int MAINACTIVITY_DESTROY_ACTIVITY = 0x2200 + 7;

    public static final String PREFERENCE_LOGIN_ID = "USER_ID";
    public static final String PREFERENCE_LOGIN_PWD = "USER_PWD";
    public static final String PREFERENCE_SETTING_STATUS_BOARD_SIZE = "setting_board_size";
    public static final String PREFERENCE_SETTING_INPUT_PICTURE = "input_picture";
    public static final String PREFERENCE_SETTING_EXIT_PICTURE = "exit_picture";
    public static final String PREFERENCE_SETTING_REGISTER_COMMUTATION_TICKET = "register_commutation_ticket";
    public static final String PREFERENCE_SETTING_DELETE_APP_IMG = "delete_app_img";
    public static final String PREFERENCE_SETTING_DELETE_IO_IMG = "delete_io_img";
    public static final String PREFERENCE_SETTING_DELETE_SERVER_IMG = "delete_server_img";

    public static final String PREFERENCE_MCPAY_TRANSACTION_NO = "TRANSACTION_NO";
    public static final String PREFERENCE_MCPAY_APPROVAL_NO = "APPROVAL_NO";
    public static final String PREFERENCE_MCPAY_APPROVAL_DATE = "APPROVAL_DATE";
    public static final String PREFERENCE_MCPAY_CARD_NO = "CARD_NO";
    public static final String PREFERENCE_MCPAY_CARD_COMPANY = "CARD_COMPANY";
    public static final String PREFERENCE_MCPAY_TOTAL_AMT = "MCPAY_TOTAL_AMT";
    public static final String PREFERENCE_MCPAY_PARAMETER = "PAYDATA_PARAMETER";
    public static final String PREFERENCE_PRINT_UNPAY_ENABLE = "PRINT_UNPAY_ENABLE";
    public static final String PREFERENCE_OUT_PRINT = "PREFERENCE_OUT_PRINT";
    public static final String PREFERENCE_ADJUST_AMT = "ADJUST_AMT";
    public static final String PREFERENCE_UNPAY_AMT = "_UNPAY_AMT";

    public static final int SETTING_STATUS_BOARD_SIZE_9 = 9;
    public static final int SETTING_STATUS_BOARD_SIZE_12 = 12;
    public static final int SETTING_STATUS_BOARD_SIZE_15 = 15;
    public static final int SETTING_STATUS_BOARD_SIZE_18 = 18;
    public static final int SETTING_STATUS_BOARD_SIZE_21 = 21;
    public static final int SETTING_STATUS_BOARD_SIZE_24 = 24;


    public static final int STATUS_BOARD_COLUMN_SIZE = 3;
    public static int STATUS_BOARD_TOTAL_AREA = 0;

    // 입차 차량번호 입력 모드 (NORMAL : 일반, MANUAL : 수동, AUTO : 번호인식, UPDATE : 차량번호수정, TICKET : 정기권)
    public static final String CAR_NORMAL = "NORMAL";
    public static final String CAR_MANUAL = "MANUAL";
    public static final String CAR_AUTO = "AUTO";
    public static final String CAR_UPDATE = "UPDATE";
    public static final String CAR_TICKET = "TICKET";

    // 입차 차량번호 입력 모드 키
    public static final String CAR_INPUT_MODE = "car_input_mode";

    // 번호 인식 시에 저장 된 주차번호
    public static String AUTO_PIO_NUMBER = "";
    // 번호 인식 시에 저장 된 차량번호
    public static String AUTO_CAR_NUMBER = "";
    // 번호 인식 시에 저장 된 이미지 패스
    public static String AUTO_CAR_IMG_PATH = "";
    // 번호 인식 시에 저장 된 이미지 파일
    public static String AUTO_CAR_IMG_NAME = "";

    // 키보드 입력 차량번호
    public static String KEY_CARNO_1 = "";
    public static String KEY_CARNO_2 = "";
    public static String KEY_CARNO_3 = "";
    public static String KEY_CARNO_4 = "";
    public static String KEY_CARNO_MANUAL = "";

    // 현황판 선택 구간
    public static String STATUS_BOARD = "";

    // 홀드 모드
    public static boolean isHOLD = false;
    // 홀드 시간
    public static String HOLD_TIME = "";

    public static final int RESULT_AUTO_CAR_NUMBER = 1000;
    public static final int RESULT_INPUT_CAR_NUMBER = 1001;
    public static final int RESULT_CAR_PICTURE = 1111;

    /**
     * interface id type
     */
    public static final String INTERFACEID_LOGIN = "loginV3Result";
    public static final String INTERFACEID_SCHEDULE_CHECK = "scheduleCheckResult";
    public static final String INTERFACEID_COMMONCODE = "codeClassResult";
    public static final String INTERFACEID_PARKINFO = "parkInfoResult";
    public static final String INTERFACEID_PARKDETAIL = "parkDetailResult";
    public static final String INTERFACEID_PARKUNPAY_INFO = "parkUnpayInfoResult";
    public static final String INTERFACEID_PARK_OUT = "parkOut_UnpayResult";
    public static final String INTERFACEID_PARK_OUT_CANCEL = "parkOutCancelResult";
    public static final String INTERFACEID_CASH_RECEIPT_UPDATE = "parkReciptUpdateResult";
    public static final String INTERFACEID_PRINT_UPDATE = "printUpdateResult";
    public static final String INTERFACEID_COUPON_SELL_COMMIT = "couponSellResult";
    public static final String INTERFACEID_PARK_UNPAY_PAY = "parkUnpayPayResult";
    public static final String INTERFACEID_PARK_UNPAY_PAY_NEW = "parkUnpayPayResult_new";
    public static final String INTERFACEID_PARK_UNPAYATTACH_V2 = "parkUnpayAttachV2Result";
    public static final String INTERFACEID_PARK_UNPAYATTACH_V3 = "parkUnpayAttachV4Result";

    public static final String INTERFACEID_UNPAY_VCNT = "getUnpayVcntResult";

    public static final String INTERFACEID_PARK_IO = "parkIoResult";

    public static final String INTERFACEID_PARK_IO_UPDATE = "parkIoUpdateResult";
    public static final String INTERFACEID_PARK_TYPE_CHANGE_UPDATE = "parkTypeChangeUpdateResult";
    public static final String INTERFACEID_PARK_FEE_CANCEL_UPDATE = "parkFeeCancelUpdateResult";
    public static final String INTERFACEID_PARK_IO_COMMIT = "parkInResult";
    public static final String INTERFACEID_PARK_IO_COMMIT_NEW = "parkInResult_new";
    public static final String INTERFACEID_PARK_IO_COMMIT_UNPAY 	   	   = "parkIn_UnpayResult_new";


    public static final String INTERFACEID_PARK_IN_CANCEL = "parkInCancelResult";

    public static final String INTERFACEID_PARK_CLOSE = "parkCloseResult";
    public static final String INTERFACEID_PARK_CLOSE_UPDATE = "parkCloseUpdateResult";
    public static final String INTERFACEID_PARK_CLOSE_AUTO_UPDATE = "parkCloseAutoUpdateResult";

    public static final String INTERFACEID_PARK_TICKET = "parkTicketResult";
    public static final String INTERFACEID_PARK_TICKET_INFO = "parkTicketInfoResult";
    public static final String INTERFACEID_PARK_TICKET_CANCEL = "parkTicketCancelResult";
    public static final String INTERFACEID_PARK_TICKET_RETURN = "parkTicketReturnResult";
    public static final String INTERFACEID_PARK_TICKET_UPDATE = "parkTicketUpdateResult";

    public static final String INTERFACEID_PARK_CLOSE_STATUS_DAY = "parkCloseStatusDayResult";
    public static final String INTERFACEID_PARK_CLOSE_STATUS_DAY_EMPCD = "parkCloseStatusDayResult_EMPCD";
    public static final String INTERFACEID_PARK_CLOSE_STATUS_MON = "parkCloseStatusMonResult";
    public static final String INTERFACEID_PARK_CLOSE_STATUS_MON_EMPCD = "parkCloseStatusMonResult_EMPCD";

    public static final String INTERFACEID_PARK_OUT_AREA_UPDATE = "parkOutAreaUpdateResult";

    public static final String INTERFACEID_PARK_IO_PICTURE = "parkIoPictureResult";
    public static final String INTERFACEID_PARK_PICTURE_UPLOAD = "parkPictureUploadResult";
    public static final String INTERFACEID_PARK_PICTURE_DELETE = "parkPictureDeleteResult";

    public static final String INTERFACEID_PARK_TICKET_PICTURE = "parkTicketPictureResult";
    public static final String INTERFACEID_PARK_TICKET_PICTURE_UPLOAD = "parkTicketPictureUploadResult";
    public static final String INTERFACEID_PARK_TICKET_PICTURE_UPLOAD2 = "parkTicketPictureUploadResult_extend"; //Yoon-1.10
    public static final String INTERFACEID_PARK_TICKET_PICTURE_DELETE = "parkTicketPictureDeleteResult";

    public static final String INTERFACEID_WORK_PICTURE_UPLOAD = "workPictureUploadResult";

    public static final String INTERFACEID_LOG_PARAMETER = "logParameter";

    public static final String PICTURE_TYPE_P1 = "P1"; // 입차사진
    public static final String PICTURE_TYPE_O1 = "O1"; // 출차 정보 사진
    public static final String PICTURE_TYPE_R1 = "R1"; // 정기권 사진
    public static final String PICTURE_TYPE_T1 = "T1"; // 출근정보 사진
    public static final String PICTURE_TYPE_T2 = "T2"; // 퇴근정보 사진
    public static final String PICTURE_TYPE_PP = "PP"; // 전체 사진
    public static final String PICTURE_TYPE_DIS = "DIS"; // 전체 사진
    // 이미지 확장자 체크
    public static final String IMG_EXT = "jpg|jpeg|png|bmp|gif|tiff";

    /**
     * common type
     */
    public static final int COMMON_TYPE = 0x111123;
    public static final int COMMON_TYPE_TIME_DC = COMMON_TYPE + 1;
    public static final int COMMON_TYPE_TICKET_PRICE = COMMON_TYPE + 2;
    public static final int COMMON_TYPE_PARK_TYPE = COMMON_TYPE + 3;
    public static final int COMMON_TYPE_CHARGE_TYPE = COMMON_TYPE + 4;
    public static final int COMMON_TYPE_CHARGE_DISCOUNT = COMMON_TYPE + 5;
    public static final int COMMON_TYPE_TICKET_TYPE = COMMON_TYPE + 6;
    public static final int COMMON_TYPE_UNPAID_TYPE = COMMON_TYPE + 7;
    public static final int COMMON_TYPE_PARK_STATUS = COMMON_TYPE + 8;
    public static final int COMMON_TYPE_PICTURE_TYPE = COMMON_TYPE + 9;
    public static final int COMMON_TYPE_PARK_OUT_CANCEL_TYPE = COMMON_TYPE + 10;
    public static final int COMMON_TYPE_CHARGE_DISCOUNT_PREPAY = COMMON_TYPE + 11;
    public static final int COMMON_TYPE_CANCEL = COMMON_TYPE + 12;
    public static final int COMMON_TYPE_PARK_TYPE_EXIT_CHANGE = COMMON_TYPE + 13;
    public static final int COMMON_TYPE_CHARGE_DISCOUNT_CHANGE = COMMON_TYPE + 14;
    public static final int COMMON_TYPE_POPUP_CHARGE_TYPE = COMMON_TYPE + 15;
    public static final int COMMON_TYPE_DDAM_CHARGE_TYPE = COMMON_TYPE + 16;
    public static final int COMMON_TYPE_DDAM_CARNO_FIND = COMMON_TYPE + 17;  //2015.12.02 Yoon
    public static final int COMMON_TYPE_CLOSE_PICTURE = COMMON_TYPE + 18;  //2017.01.18 Yoon
    public static final int COMMON_TYPE_PARK_FEE_CANCEL = COMMON_TYPE + 19;  //2016.1.10 Yoon
    public static final int COMMON_TYPE_CARD_PAYMENT_FAIL = COMMON_TYPE + 20;

}
