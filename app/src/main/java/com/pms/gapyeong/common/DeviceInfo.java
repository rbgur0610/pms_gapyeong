package com.pms.gapyeong.common;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.util.DisplayMetrics;
import android.view.WindowManager;

import com.pie.common.PieDeviceInfo;

public class DeviceInfo extends PieDeviceInfo
{

    public static boolean isTablet(Context context)
    {
        Configuration config = context.getResources().getConfiguration();
        return (config.screenLayout & 4) == 4;
    }

    public static int getWidth(Context context)
    {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        WindowManager windowManager = (WindowManager)context.getSystemService("window");
        windowManager.getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;
        return width;
    }

    public static int getHeight(Context context)
    {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        WindowManager windowManager = (WindowManager)context.getSystemService("window");
        windowManager.getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        return height;
    }

    public static float getDisplayMetricsDensity(Context context)
    {
        return context.getResources().getDisplayMetrics().density;
    }

    public static int getPixel(Context context, int p)
    {
        float den = getDisplayMetricsDensity(context);
        if(den != 1.0F)
            return (int)((double)((float)p * den) + 0.5D);
        else
            return p;
    }

    public static android.util.DisplayMetrics getScreen(Activity act)
    {
		android.util.DisplayMetrics metrics = new android.util.DisplayMetrics();
		act.getWindowManager().getDefaultDisplay().getMetrics(metrics);
		return metrics;
	}
    
}
