package com.pms.gapyeong.common;

import android.app.Dialog;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.WindowManager.LayoutParams;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.pms.gapyeong.R;

public class CustomKeyboard extends View implements OnClickListener, OnLongClickListener {

    private Button[] hangulButton;
    private String[] hangulKeyboard;
    private LinearLayout keyboardRootLayout;
    private LinearLayout[] keyboardLayout;
    private EditText et_keyboard1;
    private EditText et_keyboard2;
    private EditText et_keyboard3;
    private EditText et_keyboard4;
    private FrameLayout frameLayout;
    private Context mContext;
    private String[] numberKeyboard;
    private Button[] numberButton;
    private String[] sectorKeyboard;
    private Button[] sectorButton;

    private String inputMode;

    private int[] mHanguType = {
            R.array.keyboard_hangul_shift1, R.array.keyboard_hangul_shift2, R.array.keyboard_hangul_shift3
            , R.array.keyboard_hangul_shift4, R.array.keyboard_hangul_shift5, R.array.keyboard_hangul_shift6
            , R.array.keyboard_hangul_shift7, R.array.keyboard_hangul_shift8, R.array.keyboard_hangul_shift9
            , R.array.keyboard_hangul_shift10, R.array.keyboard_hangul_shift11, R.array.keyboard_hangul_shift12
            , R.array.keyboard_hangul_shift13, R.array.keyboard_hangul_shift14
    };

    public static final int KEYBOARD_TYPE_SECTOR = 0x1111;
    public static final int KEYBOARD_TYPE_SECTOR_ETC = 0x1112;
    public static final int KEYBOARD_TYPE_FRONT_NUMBER = 0x1113;
    public static final int KEYBOARD_TYPE_BACK_NUMBER = 0x1114;
    public static final int KEYBOARD_TYPE_HANGUL = 0x1115;
    public static final int KEYBOARD_TYPE_HANGUL_SHIFT = 0x1116;

    private boolean isInputThree = false;
    private String TAG = this.getClass().getSimpleName();

    public CustomKeyboard(Context context, FrameLayout layout,
                          EditText ed1, EditText ed2, EditText ed3, EditText ed4,
                          KeyboardDoneListener listener, String index) {

        super(context);

        mContext = context;

        frameLayout = layout;

        this.listener = listener;

        this.et_keyboard1 = ed1;
        this.et_keyboard2 = ed2;
        this.et_keyboard3 = ed3;
        this.et_keyboard4 = ed4;

        this.inputMode = index;

        et_keyboard1.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                et_keyboard1.setText("");
                inputMode = "";
                setKeyboardType(KEYBOARD_TYPE_SECTOR);
            }
        });

        et_keyboard2.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                et_keyboard2.setText("");
                inputMode = "";
                setKeyboardType(KEYBOARD_TYPE_FRONT_NUMBER);
            }
        });

        et_keyboard3.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                et_keyboard3.setText("");
                inputMode = "";
                setKeyboardType(KEYBOARD_TYPE_HANGUL);
            }
        });

        et_keyboard4.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                et_keyboard4.setText("");
                inputMode = "";
                setKeyboardType(KEYBOARD_TYPE_BACK_NUMBER);
            }
        });

        keyboardRootLayout = new LinearLayout(context);
        keyboardRootLayout.setOrientation(LinearLayout.VERTICAL);

        Log.d(TAG, " inputMode >>> " + inputMode);

        if (inputMode.equals("1")) {
            setKeyboardType(KEYBOARD_TYPE_SECTOR);
        } else if (inputMode.equals("2")) {
            setKeyboardType(KEYBOARD_TYPE_FRONT_NUMBER);
        } else if (inputMode.equals("3")) {
            setKeyboardType(KEYBOARD_TYPE_HANGUL);
        } else if (inputMode.equals("4")) {
            setKeyboardType(KEYBOARD_TYPE_BACK_NUMBER);
        } else {
            setKeyboardType(KEYBOARD_TYPE_FRONT_NUMBER);
            showcustomDialog(Constants.KEY_CARNO_MANUAL);
        }

//		setNumberFrontKeyBoardLayout(context);
//		if((Constants.CAR_UPDATE.equals(inputMode) || Constants.CAR_TICKET.equals(inputMode)) && !Util.isEmpty(Constants.KEY_CARNO_MANUAL)){
//			showcustomDialog(Constants.KEY_CARNO_MANUAL);
//		}

    }

    public void setInputThree(boolean isInputThree ){
        this.isInputThree = isInputThree;
    }

    public void setKeyboardType(int type) {

        keyboardRootLayout.removeAllViews();
        frameLayout.removeAllViews();

        switch (type) {
            case KEYBOARD_TYPE_FRONT_NUMBER:
                setNumberFrontKeyBoardLayout(mContext);
                break;
            case KEYBOARD_TYPE_BACK_NUMBER:
                setNumberBackKeyBoardLayout(mContext);
                break;
            case KEYBOARD_TYPE_HANGUL:
                setHangulKeyBoardLayout(mContext);
                break;
            case KEYBOARD_TYPE_SECTOR:
                setSectorKeyBoardLayout(mContext);
                break;
            case KEYBOARD_TYPE_SECTOR_ETC:
                setSectorEtcKeyBoardLayout(mContext);
                break;
        }
    }

    public void setHangulShiftKeyboardType(int hanguType) {
        keyboardRootLayout.removeAllViews();
        frameLayout.removeAllViews();
        setHangulShiftKeyBoardLayout(mContext, hanguType);
    }

    public void showKeyboard() {
        frameLayout.setVisibility(View.VISIBLE);
    }

    public void hideKeyboard() {
        frameLayout.setVisibility(View.INVISIBLE);
    }

    // 전국 키보드 선택
    public void setSectorKeyBoardLayout(Context context) {

        Resources res = getResources();
        sectorKeyboard = res.getStringArray(R.array.sector_keyboard);

        keyboardLayout = new LinearLayout[5];
        sectorButton = new Button[sectorKeyboard.length];

        int keyboardLen = keyboardLayout.length;
        for (int i = 0; i < keyboardLen; i++) {
            keyboardLayout[i] = new LinearLayout(context);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
            params.weight = 1.0f;
            keyboardLayout[i].setLayoutParams(params);
        }

        int sectorLen = sectorKeyboard.length;
        for (int i = 0; i < sectorLen; i++) {
            int index = i / 4;
            sectorButton[i] = new Button(context);
            sectorButton[i].setTag(sectorKeyboard[i]);
            sectorButton[i].setText(sectorKeyboard[i]);
            sectorButton[i].setGravity(Gravity.CENTER);
            sectorButton[i].setTypeface(sectorButton[i].getTypeface(), Typeface.BOLD);
            sectorButton[i].setTextSize(30);
            if (i == (sectorLen - 1) || i == (sectorLen - 2)) {
                sectorButton[i].setTextColor(getResources().getColor(R.color.Red));
            }

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
            params.weight = 1.0f;

            sectorButton[i].setLayoutParams(params);
            if (!Util.isEmpty(sectorKeyboard[i])) {
                sectorButton[i].setOnClickListener(sectorListener);
            }
            keyboardLayout[index].setOrientation(LinearLayout.HORIZONTAL);
            keyboardLayout[index].addView(sectorButton[i]);
        }

        for (int i = 0; i < keyboardLayout.length; i++) {
            keyboardRootLayout.addView(keyboardLayout[i]);
        }

        frameLayout.addView(keyboardRootLayout);
    }

    // 전국 특수 키보드 선택
    public void setSectorEtcKeyBoardLayout(Context context) {

        Resources res = getResources();
        sectorKeyboard = res.getStringArray(R.array.sector_etc_keyboard);

        keyboardLayout = new LinearLayout[5];
        sectorButton = new Button[sectorKeyboard.length];

        int keyboardLen = keyboardLayout.length;
        for (int i = 0; i < keyboardLen; i++) {
            keyboardLayout[i] = new LinearLayout(context);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
            params.weight = 1.0f;
            keyboardLayout[i].setLayoutParams(params);
        }

        int sectorLen = sectorKeyboard.length;
        for (int i = 0; i < sectorLen; i++) {
            int index = i / 4;
            sectorButton[i] = new Button(context);
            sectorButton[i].setTag(sectorKeyboard[i]);
            sectorButton[i].setText(sectorKeyboard[i]);
            sectorButton[i].setGravity(Gravity.CENTER);
            sectorButton[i].setTypeface(sectorButton[i].getTypeface(), Typeface.BOLD);
            sectorButton[i].setTextSize(30);

            if (i == (sectorLen - 1) || i == (sectorLen - 2)) {
                sectorButton[i].setTextColor(getResources().getColor(R.color.Red));
            }

            if (Util.isEmpty(sectorKeyboard[i])) {
                sectorButton[i].setBackgroundColor(getResources().getColor(R.color.White));
            }

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
            params.weight = 1.0f;

            sectorButton[i].setLayoutParams(params);
            if (!Util.isEmpty(sectorKeyboard[i])) {
                sectorButton[i].setOnClickListener(sectorEtcListener);
            }
            keyboardLayout[index].setOrientation(LinearLayout.HORIZONTAL);
            keyboardLayout[index].addView(sectorButton[i]);
        }

        for (int i = 0; i < keyboardLayout.length; i++) {
            keyboardRootLayout.addView(keyboardLayout[i]);
        }

        frameLayout.addView(keyboardRootLayout);
    }

    // 앞 넘버 키보드 선택
    public void setNumberFrontKeyBoardLayout(Context context) {

        Resources res = getResources();
        numberKeyboard = res.getStringArray(R.array.number_front_keyboard);

        keyboardLayout = new LinearLayout[4];
        numberButton = new Button[numberKeyboard.length];

        for (int i = 0; i < keyboardLayout.length; i++) {
            keyboardLayout[i] = new LinearLayout(context);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
            params.weight = 1.0f;
            keyboardLayout[i].setLayoutParams(params);
        }

        int numberLen = numberKeyboard.length;
        for (int i = 0; i < numberLen; i++) {
            int index = i / 3;
            numberButton[i] = new Button(context);
            numberButton[i].setTag(numberKeyboard[i]);
            numberButton[i].setText(numberKeyboard[i]);
            numberButton[i].setGravity(Gravity.CENTER);
            numberButton[i].setTypeface(numberButton[i].getTypeface(), Typeface.BOLD);
            numberButton[i].setTextSize(45);

            if (i == (numberLen - 1) || i == (numberLen - 3)) {
                numberButton[i].setTextColor(getResources().getColor(R.color.Red));
            }

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
            params.weight = 1.0f;

            numberButton[i].setLayoutParams(params);
            if (!Util.isEmpty(numberKeyboard[i])) {
                numberButton[i].setOnClickListener(numberFrontListener);
            }
            keyboardLayout[index].setOrientation(LinearLayout.HORIZONTAL);
            keyboardLayout[index].addView(numberButton[i]);
        }

        for (int i = 0; i < keyboardLayout.length; i++) {
            keyboardRootLayout.addView(keyboardLayout[i]);
        }

        frameLayout.addView(keyboardRootLayout);
    }

    // 뒤 넘버 키보드 선택
    public void setNumberBackKeyBoardLayout(Context context) {

        Resources res = getResources();
        numberKeyboard = res.getStringArray(R.array.number_back_keyboard);

        keyboardLayout = new LinearLayout[4];
        numberButton = new Button[numberKeyboard.length];

        for (int i = 0; i < keyboardLayout.length; i++) {
            keyboardLayout[i] = new LinearLayout(context);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
            params.weight = 1.0f;
            keyboardLayout[i].setLayoutParams(params);
        }

        int numberLen = numberKeyboard.length;
        for (int i = 0; i < numberLen; i++) {
            int index = i / 3;
            numberButton[i] = new Button(context);
            numberButton[i].setTag(numberKeyboard[i]);
            numberButton[i].setText(numberKeyboard[i]);
            numberButton[i].setGravity(Gravity.CENTER);
            numberButton[i].setTypeface(numberButton[i].getTypeface(), Typeface.BOLD);

            if ("Clear".equals(numberKeyboard[i])) {
                numberButton[i].setTextSize(39);
            } else {
                numberButton[i].setTextSize(45);
            }

            if (i == (numberLen - 1) || i == (numberLen - 3)) {
                numberButton[i].setTextColor(getResources().getColor(R.color.Red));
            }

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
            params.weight = 1.0f;

            numberButton[i].setLayoutParams(params);
            if (!Util.isEmpty(numberKeyboard[i])) {
                numberButton[i].setOnClickListener(numberBackListener);
            }
            keyboardLayout[index].setOrientation(LinearLayout.HORIZONTAL);
            keyboardLayout[index].addView(numberButton[i]);
        }

        for (int i = 0; i < keyboardLayout.length; i++) {
            keyboardRootLayout.addView(keyboardLayout[i]);
        }

        frameLayout.addView(keyboardRootLayout);
    }

    public void setHangulKeyBoardLayout(Context context) {

        Resources res = getResources();
        hangulKeyboard = res.getStringArray(R.array.keyboard_hangul);

        keyboardLayout = new LinearLayout[4];
        hangulButton = new Button[hangulKeyboard.length];

        for (int i = 0; i < keyboardLayout.length; i++) {
            keyboardLayout[i] = new LinearLayout(context);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
            params.weight = 1.0f;
            keyboardLayout[i].setLayoutParams(params);
        }

        int hangulLen = hangulKeyboard.length;
        for (int i = 0; i < hangulLen; i++) {
            int index = i / 4;
            hangulButton[i] = new Button(context);
            hangulButton[i].setTag(i);
            hangulButton[i].setText(hangulKeyboard[i]);
            hangulButton[i].setGravity(Gravity.CENTER);
            hangulButton[i].setTypeface(hangulButton[i].getTypeface(), Typeface.BOLD);
            hangulButton[i].setTextSize(45);

            if (Util.isEmpty(hangulKeyboard[i])) {
                hangulButton[i].setBackgroundColor(getResources().getColor(R.color.White));
            }

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
            params.weight = 1.0f;

            hangulButton[i].setLayoutParams(params);
            if (!Util.isEmpty(hangulKeyboard[i])) {
                hangulButton[i].setOnClickListener(hangulListener);
            }
            keyboardLayout[index].setOrientation(LinearLayout.HORIZONTAL);
            keyboardLayout[index].addView(hangulButton[i]);
        }

        for (int i = 0; i < keyboardLayout.length; i++) {
            keyboardRootLayout.addView(keyboardLayout[i]);
        }

        frameLayout.addView(keyboardRootLayout);
    }

    public void setHangulShiftKeyBoardLayout(Context context, int hanguType) {

        if (mHanguType.length < hanguType) {
            Log.d(TAG, " NOT VALID HANGUL ");
            return;
        }

        Resources res = getResources();
        hangulKeyboard = res.getStringArray(mHanguType[hanguType]);

        keyboardLayout = new LinearLayout[4];
        hangulButton = new Button[hangulKeyboard.length];

        for (int i = 0; i < keyboardLayout.length; i++) {
            keyboardLayout[i] = new LinearLayout(context);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
            params.weight = 1.0f;
            keyboardLayout[i].setLayoutParams(params);
        }

        int hangulLen = hangulKeyboard.length;
        for (int i = 0; i < hangulLen; i++) {
            int index = i / 3;
            hangulButton[i] = new Button(context);
            hangulButton[i].setTag(hangulKeyboard[i]);
            hangulButton[i].setText(hangulKeyboard[i]);
            hangulButton[i].setGravity(Gravity.CENTER);
            hangulButton[i].setTypeface(hangulButton[i].getTypeface(), Typeface.BOLD);
            hangulButton[i].setTextSize(45);

            if (i == (hangulLen - 1)) {
                hangulButton[i].setTextColor(getResources().getColor(R.color.Red));
            }

            if (Util.isEmpty(hangulKeyboard[i])) {
                hangulButton[i].setBackgroundColor(getResources().getColor(R.color.White));
            }

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
            params.weight = 1.0f;

            hangulButton[i].setLayoutParams(params);
            if (!Util.isEmpty(hangulKeyboard[i])) {
                hangulButton[i].setOnClickListener(hangulShiftListener);
            }
            keyboardLayout[index].setOrientation(LinearLayout.HORIZONTAL);
            keyboardLayout[index].addView(hangulButton[i]);
        }

        for (int i = 0; i < keyboardLayout.length; i++) {
            keyboardRootLayout.addView(keyboardLayout[i]);
        }

        frameLayout.addView(keyboardRootLayout);
    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
    }

    @Override
    public boolean onLongClick(View v) {
        // TODO Auto-generated method stub
        if (v.getTag().toString().equals("←")) {
//			et_keyboard.setText(inputText.toString());
//			et_keyboard.setSelection(inputText.length());
        }
        return false;
    }


    View.OnClickListener sectorListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            String keystr = v.getTag().toString();
            if (keystr.equals("특수")) {
                setKeyboardType(KEYBOARD_TYPE_SECTOR_ETC);
            } else if (keystr.equals("수동")) {
                showcustomDialog("");
            } else {
                et_keyboard1.setText(keystr);
                nextKeyBoardStep();
            }
        }
    };

    View.OnClickListener sectorEtcListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            String keystr = v.getTag().toString();
            if (keystr.equals("←")) {
                setKeyboardType(KEYBOARD_TYPE_SECTOR);
            } else if (keystr.equals("수동")) {
                showcustomDialog("");
            } else {
                // 전국번호 셋팅하도록 변경
                et_keyboard1.setText(keystr);
                nextKeyBoardStep();
//				showcustomDialog(keystr);
            }
        }
    };

    View.OnClickListener numberFrontListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            String keystr = v.getTag().toString();
            if (keystr.equals("수동")) {
                showcustomDialog("");
            } else if (keystr.equals("←")) {
                String keyboardstr = et_keyboard2.getText().toString();
                if (!Util.isEmpty(keyboardstr)) {
                    et_keyboard2.setText(keyboardstr.substring(0, keyboardstr.length() - 1));
                }
            } else {
                // 차량번호 수정 시 이전 입력번호 삭제...
                if ("2".equals(inputMode)) {
                    et_keyboard2.setText("");
                    inputMode = "";
                }

                String orgstr = et_keyboard2.getText().toString();
                String keyboardstr = orgstr;

                if (isInputThree) {
                    if (orgstr.length() < 3) {
                        et_keyboard2.setText(orgstr + keystr);
                        keyboardstr = et_keyboard2.getText().toString();
                    }
                    if (keyboardstr.length() >= 3) {
                        nextKeyBoardStep();
                    }
                } else {
                    if (orgstr.length() < 2) {
                        et_keyboard2.setText(orgstr + keystr);
                        keyboardstr = et_keyboard2.getText().toString();
                    }
                    if (keyboardstr.length() >= 2) {
                        nextKeyBoardStep();
                    }
                }


            }
        }
    };

    View.OnClickListener numberBackListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            String keystr = v.getTag().toString();
            if (keystr.equals("Clear")) {
                et_keyboard4.setText("");
            } else if (keystr.equals("←")) {
                String keyboardstr = et_keyboard4.getText().toString();
                if (!Util.isEmpty(keyboardstr)) {
                    et_keyboard4.setText(keyboardstr.substring(0, keyboardstr.length() - 1));
                }
            } else {
                // 차량번호 수정 시 이전 입력번호 삭제...
                if ("4".equals(inputMode)) {
                    et_keyboard4.setText("");
                    inputMode = "";
                }

                String orgstr = et_keyboard4.getText().toString();
                String keyboardstr = orgstr;
                if (orgstr.length() < 4) {
                    et_keyboard4.setText(orgstr + keystr);
                    keyboardstr = et_keyboard4.getText().toString();
                }
                if (keyboardstr.length() >= 4) {
                    nextKeyBoardStep();
                }
            }
        }
    };

    View.OnClickListener hangulListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            int hangulkey = Util.sToi(v.getTag().toString());
            setHangulShiftKeyboardType(hangulkey);
        }
    };

    View.OnClickListener hangulShiftListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            String keystr = v.getTag().toString();
            if (keystr.equals("←")) {
                setKeyboardType(KEYBOARD_TYPE_HANGUL);
            } else {
                et_keyboard3.setText(keystr);
                nextKeyBoardStep();
            }
        }
    };

    public void nextKeyBoardStep() {
        Log.d(TAG, " nextKeyBoardStep >>> ");

        String keyboardstr1 = et_keyboard1.getText().toString();
        String keyboardstr2 = et_keyboard2.getText().toString();
        String keyboardstr3 = et_keyboard3.getText().toString();
        String keyboardstr4 = et_keyboard4.getText().toString();

        if (Util.isEmpty(keyboardstr2)) {
            if (isInputThree && keyboardstr2.length() < 3) {
                setKeyboardType(KEYBOARD_TYPE_FRONT_NUMBER);
            } else if (!isInputThree && keyboardstr2.length() < 2) {
                setKeyboardType(KEYBOARD_TYPE_FRONT_NUMBER);
            }
        } else if (Util.isEmpty(keyboardstr3)) {
            setKeyboardType(KEYBOARD_TYPE_HANGUL);
        } else if (Util.isEmpty(keyboardstr4) || keyboardstr4.length() < 4) {
            setKeyboardType(KEYBOARD_TYPE_BACK_NUMBER);
        } else {
            listener.onDone(keyboardstr1, keyboardstr2, keyboardstr3, keyboardstr4, "");
        }
    }

    public void showcustomDialog(String keystr) {
        final Dialog customDialog;
            customDialog = new Dialog(mContext, android.R.style.Theme_Holo_Light_Dialog);

        customDialog.setContentView(R.layout.dialog_manual_carno);
        customDialog.setTitle("차량번호 수동입력");
        final EditText manual_car_no = (EditText) customDialog.findViewById(R.id.manual_car_no);
        Button btn_ok = (Button) customDialog.findViewById(R.id.btn_ok);
        Button btn_cancel = (Button) customDialog.findViewById(R.id.btn_cancel);

        manual_car_no.setText(keystr);

        btn_ok.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Log.d(TAG, manual_car_no.getText().toString());
                String car_no = manual_car_no.getText().toString();
                if (Util.isEmpty(car_no)) {
                    MsgUtil.ToastMessage(mContext, "차량번호를 입력해주세요.");
                } else {
                    listener.onDone("", "", "", "", car_no);
                    customDialog.dismiss();
                }
            }
        });
        btn_cancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                customDialog.dismiss();
            }
        });

        customDialog.show();
    }

    public void setListener(KeyboardDoneListener listener) {
        this.listener = listener;
    }

    private KeyboardDoneListener listener;

    public interface KeyboardDoneListener {
        public void onDone(String str1, String str2, String str3, String str4, String stretc);

        public void onDelete();

        public void onEtc();
    }

}
