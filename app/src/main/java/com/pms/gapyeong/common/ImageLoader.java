package com.pms.gapyeong.common;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.ref.SoftReference;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.concurrent.ConcurrentHashMap;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxStatus;
import com.androidquery.callback.BitmapAjaxCallback;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Bitmap.Config;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;

public class ImageLoader {

    private static String TAG = ImageLoader.class.getSimpleName();

    public void loadBitmap(String path, ImageView imageView) {
        imageView.setImageDrawable(null);
        imageView.setImageBitmap(null);
        imageView.setBackgroundDrawable(null);

        resetPurgeTimer();

        Bitmap bitmap = getBitmapFromCache(path);
        if (bitmap == null) {
            if (path == null) {
                return;
            }

            BitmapLoaderTask task = new BitmapLoaderTask(imageView);
            task.execute(path);
        } else {
            imageView.setImageBitmap(bitmap);
        }
    }

    class BitmapLoaderTask extends AsyncTask<String, Void, Bitmap> {
        private String path;
        private final WeakReference<ImageView> imageViewReference;

        public BitmapLoaderTask(ImageView imageView) {
            imageViewReference = new WeakReference<ImageView>(imageView);
        }

        @Override
        protected void onPreExecute() {
            ImageView imageView = imageViewReference.get();
            imageView.setImageBitmap(null);
        }

        @Override
        protected Bitmap doInBackground(String... params) {
            Bitmap smallphoto = null;
            try {
                path = params[0];
                Bitmap photo = BitmapFactory.decodeFile(path);
                smallphoto = Bitmap.createScaledBitmap(photo, 160, 160, true);
                photo.recycle();
                photo = null;
            } catch (NullPointerException e) {
                if(Constants.DEBUG_PRINT_LOG){
                    e.printStackTrace();
                }else{
                    System.out.println("예외 발생");
                }
            }

            return smallphoto;
        }

        /**
         * Once the image is downloaded, associates it to the imageView
         */
        @Override
        protected void onPostExecute(Bitmap bitmap) {
            if (isCancelled()) {
                bitmap.recycle();
                bitmap = null;
            }

            if (bitmap != null) {
                addBitmapToCache(path, bitmap);
            }

            if (imageViewReference != null) {
                ImageView imageView = imageViewReference.get();
                if (imageView != null) {
                    imageView.setImageBitmap(bitmap);
                    imageView.setScaleType(ScaleType.FIT_XY);
                }
            }
        }
    }


    /*
     * Cache-related fields and methods.
     *
     * We use a hard and a soft cache. A soft reference cache is too
     * aggressively cleared by the Garbage Collector.
     */

    private static final int HARD_CACHE_CAPACITY = 10;
    private static final int DELAY_BEFORE_PURGE = 60 * 1000; // in milliseconds

    // Hard cache, with a fixed maximum capacity and a life duration
    private final HashMap<String, Bitmap> sHardBitmapCache = new LinkedHashMap<String, Bitmap>(
            HARD_CACHE_CAPACITY / 2, 0.75f, true) {
        @Override
        protected boolean removeEldestEntry(
                LinkedHashMap.Entry<String, Bitmap> eldest) {
            if (size() > HARD_CACHE_CAPACITY) {
                // Entries push-out of hard reference cache are transferred to
                // soft reference cache
                sSoftBitmapCache.put(eldest.getKey(),
                        new SoftReference<Bitmap>(eldest.getValue()));
                return true;
            } else
                return false;
        }
    };

    // Soft cache for bitmaps kicked out of hard cache
    private final static ConcurrentHashMap<String, SoftReference<Bitmap>> sSoftBitmapCache = new ConcurrentHashMap<String, SoftReference<Bitmap>>(
            HARD_CACHE_CAPACITY / 2);

    private final Handler purgeHandler = new Handler();

    private final Runnable purger = new Runnable() {
        public void run() {
            clearCache();
        }
    };

    /**
     * Adds this bitmap to the cache.
     *
     * @param bitmap The newly downloaded bitmap.
     */
    private void addBitmapToCache(String url, Bitmap bitmap) {
        if (bitmap != null) {
            synchronized (sHardBitmapCache) {
                sHardBitmapCache.put(url, bitmap);
            }
        }
    }

    /**
     * @param url The URL of the image that will be retrieved from the cache.
     * @return The cached bitmap or null if it was not found.
     */
    private Bitmap getBitmapFromCache(String url) {
        // First try the hard reference cache
        synchronized (sHardBitmapCache) {
            final Bitmap bitmap = sHardBitmapCache.get(url);
            if (bitmap != null) {
                // Bitmap found in hard cache
                // Move element to first position, so that it is removed last
                sHardBitmapCache.remove(url);
                sHardBitmapCache.put(url, bitmap);
                return bitmap;
            }
        }

        // Then try the soft reference cache
        SoftReference<Bitmap> bitmapReference = sSoftBitmapCache.get(url);
        if (bitmapReference != null) {
            final Bitmap bitmap = bitmapReference.get();
            if (bitmap != null) {
                // Bitmap found in soft cache
                return bitmap;
            } else {
                // Soft reference has been Garbage Collected
                sSoftBitmapCache.remove(url);
            }
        }

        return null;
    }

    /**
     * Clears the image cache used internally to improve performance. Note that
     * for memory efficiency reasons, the cache will automatically be cleared
     * after a certain inactivity delay.
     */
    public void clearCache() {
        sHardBitmapCache.clear();
        sSoftBitmapCache.clear();
    }

    /**
     * Allow a new delay before the automatic cache clear is done.
     */
    private void resetPurgeTimer() {
        purgeHandler.removeCallbacks(purger);
        purgeHandler.postDelayed(purger, DELAY_BEFORE_PURGE);
    }


    /*************************************************
     // ImageUtil Start
     *************************************************/
    /**
     * 저장된 이미지 imageView 에 셋팅
     */
    public static synchronized void setImageUrlView(final Context context, String pictureURL, ImageView v) {
        AQuery aq = new AQuery(context);
        aq.id(v).image(pictureURL, true, false, 0, 0, new BitmapAjaxCallback() {
            @Override
            public void callback(String url, ImageView iv, Bitmap bm, AjaxStatus status) {
                if (bm != null) {
                    iv.setImageBitmap(bm);
                }
            }
        });
    }

    /**
     * 저장된 이미지 imageView 에 셋팅
     */
    public static synchronized void setImageView(String savePath, ImageView v) {
        Bitmap bitmap = null;
        try {

            File f = new File(savePath);
            if (!FileUtil.isFile(savePath)) {
                Log.d(TAG, " imafe file not valid ::: " + savePath);
                return;
            }

            BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
            bitmap = BitmapFactory.decodeFile(f.getAbsolutePath(), bitmapOptions);
//	        bitmap.recycle();

            //이미지 회전
            ExifInterface exif = new ExifInterface(savePath);
            int exifOrientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
            int exifDegree = ImageLoader.exifOrientationToDegrees(exifOrientation);
            bitmap = ImageLoader.rotate(bitmap, exifDegree);

            v.setImageBitmap(bitmap);
            v.setScaleType(ScaleType.FIT_XY);

        } catch (IOException e) {
            if(Constants.DEBUG_PRINT_LOG){
                e.printStackTrace();
            }else{
                System.out.println("예외 발생");
            }
        }
    }

    /**
     * 저장된 이미지 리사이즈 후 imageView 에 셋팅
     */
    public static synchronized void resizeImageView(String savePath, ImageView v) {
        Bitmap bitmap = null;
        try {

            File f = new File(savePath);
            if (!FileUtil.isFile(savePath)) {
                Log.d(TAG, " imafe file not valid ::: " + savePath);
                return;
            }

            BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
            bitmapOptions.inSampleSize = 4;
            bitmap = BitmapFactory.decodeFile(f.getAbsolutePath(), bitmapOptions);
//			bitmap.recycle();

            //이미지 회전
            ExifInterface exif = new ExifInterface(savePath);
            int exifOrientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
            int exifDegree = ImageLoader.exifOrientationToDegrees(exifOrientation);
            bitmap = ImageLoader.rotate(bitmap, exifDegree);

            // 파일 사이즈 변환
            FileOutputStream fos = new FileOutputStream(f.getAbsolutePath());
            Bitmap resize = Bitmap.createScaledBitmap(bitmap, bitmap.getWidth() / 3, bitmap.getHeight() / 3, true);
            resize.compress(Bitmap.CompressFormat.PNG, 100, fos);
            fos.close();

            v.setImageBitmap(resize);
            v.setScaleType(ScaleType.FIT_XY);

        } catch (IOException e) {
            if(Constants.DEBUG_PRINT_LOG){
                e.printStackTrace();
            }else{
                System.out.println("예외 발생");
            }
        }
    }

    /**
     * 저장된 이미지 리사이즈
     */
    public static synchronized void resizeImage(String savePath) {
        Bitmap bitmap = null;
        try {

            File f = new File(savePath);
            if (!FileUtil.isFile(savePath)) {
                Log.d(TAG, " imafe file not valid ::: " + savePath);
                return;
            }

            BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
            bitmapOptions.inSampleSize = 4;
            bitmap = BitmapFactory.decodeFile(f.getAbsolutePath(), bitmapOptions);
//			bitmap.recycle();

            //이미지 회전
            ExifInterface exif = new ExifInterface(savePath);
            int exifOrientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
            int exifDegree = ImageLoader.exifOrientationToDegrees(exifOrientation);
            bitmap = ImageLoader.rotate(bitmap, exifDegree);

            // 파일 사이즈 변환
            FileOutputStream fos = new FileOutputStream(f.getAbsolutePath());
            Bitmap resize = Bitmap.createScaledBitmap(bitmap, bitmap.getWidth() / 3, bitmap.getHeight() / 3, true);
            resize.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            fos.close();
            Log.d("111", "------------");

        } catch (IOException e) {
            if(Constants.DEBUG_PRINT_LOG){
                e.printStackTrace();
            }else{
                System.out.println("예외 발생");
            }
        }
    }

    public static synchronized void resizeImage2(String savePath) {
        Bitmap bitmap = null;
        try {

            File f = new File(savePath);
            if (!FileUtil.isFile(savePath)) {
                Log.d(TAG, " imafe file not valid ::: " + savePath);
                return;
            }

            BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
            bitmapOptions.inSampleSize = 2;
            bitmap = BitmapFactory.decodeFile(f.getAbsolutePath(), bitmapOptions);
//			bitmap.recycle();

            //이미지 회전
            ExifInterface exif = new ExifInterface(savePath);
            int exifOrientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
            int exifDegree = ImageLoader.exifOrientationToDegrees(exifOrientation);
            bitmap = ImageLoader.rotate(bitmap, exifDegree);

            // 파일 사이즈 변환
            FileOutputStream fos = new FileOutputStream(f.getAbsolutePath());
            Bitmap resize = Bitmap.createScaledBitmap(bitmap, bitmap.getWidth(), bitmap.getHeight(), true);
            resize.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            fos.close();

        } catch (IOException e) {
            if(Constants.DEBUG_PRINT_LOG){
                e.printStackTrace();
            }else{
                System.out.println("예외 발생");
            }
        }
    }


    public static synchronized Drawable bitmapToDrawable(Bitmap bitmap) {
        Drawable drawable = new BitmapDrawable(bitmap);
        return drawable;
    }

    public static synchronized Bitmap fileToBitmap(String fileName) {
        Bitmap bitmap = null;
        if (FileUtil.isFile(fileName)) {
            bitmap = BitmapFactory.decodeFile(fileName);
        }
        return bitmap;
    }

    public static synchronized Bitmap fileToBitmap(String fileName, int sampleSize) {
        Bitmap bitmap = null;
        if (FileUtil.isFile(fileName)) {
            BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
            bitmapOptions.inSampleSize = sampleSize;
            bitmap = BitmapFactory.decodeFile(fileName, bitmapOptions);
        }
        return bitmap;
    }

    public static synchronized void Bitmap2File(Bitmap bitmap, String strPath) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        FileOutputStream fileoutstream = null;
        try {
             fileoutstream = new FileOutputStream(strPath);
            fileoutstream.write(stream.toByteArray());

            // System.gc();
        } catch (FileNotFoundException e) {
            if(Constants.DEBUG_PRINT_LOG){
                e.printStackTrace();
            }else{
                System.out.println("예외 발생");
            }
        } catch (IOException e) {
            if(Constants.DEBUG_PRINT_LOG){
                e.printStackTrace();
            }else{
                System.out.println("예외 발생");
            }
        }finally {
            try {
                fileoutstream.close();
            } catch (IOException e) {
                System.out.println("예외 발생");
            }
        }
    }

    public static synchronized void Bitmap2File(Context context, Bitmap bitmap, String strPath) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);

        FileOutputStream fos;

        // Create a new output file stream that's private to this application.
        try {
            fos = context.openFileOutput(strPath, Context.MODE_PRIVATE);
            fos.write(stream.toByteArray());
            fos.close();
        } catch (FileNotFoundException e) {
            if(Constants.DEBUG_PRINT_LOG){
                e.printStackTrace();
            }else{
                System.out.println("예외 발생");
            }
        } catch (IOException e) {
            if(Constants.DEBUG_PRINT_LOG){
                e.printStackTrace();
            }else{
                System.out.println("예외 발생");
            }
        }
    }

    /**
     * 이미지를 회전시킵니다.
     *
     * @param bitmap  비트맵 이미지
     * @param degrees 회전 각도
     * @return 회전된 이미지
     */
    public static synchronized Bitmap rotate(Bitmap bitmap, int degrees) {
        if (degrees != 0 && bitmap != null) {
            Matrix m = new Matrix();
            m.setRotate(degrees, (float) bitmap.getWidth() / 3, (float) bitmap.getHeight() / 3);
            try {
                Bitmap converted = Bitmap.createBitmap(bitmap, 0, 0,
                        bitmap.getWidth(), bitmap.getHeight(), m, true);
                if (bitmap != converted) {
                    bitmap.recycle();
                    bitmap = converted;
                }
            } catch (OutOfMemoryError ex) {
                if(Constants.DEBUG_PRINT_LOG){
                    ex.printStackTrace();
                }else{
                    System.out.println("예외 발생");
                }
                // 메모리가 부족하여 회전을 시키지 못할 경우 그냥 원본을 반환합니다.
            }
        }
        return bitmap;
    }


    /**
     * EXIF정보를 회전각도로 변환하는 메서드
     *
     * @param exifOrientation - EXIF 회전각도
     * @return 실제 각도
     */
    public static int exifOrientationToDegrees(int exifOrientation) {

        Log.e("exifOrientation", exifOrientation + "");

        if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_90) {
            return 90;
        } else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_180) {
            return 180;
        } else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_270) {
            return 270;
        }
        return 0;
    }

    /**
     * 이미지를 라운딩 처리한다.
     *
     * @param bitmap
     * @return
     */
    public static Bitmap getRoundedCorner(Bitmap bitmap) {
        return getRoundedCorner(bitmap, 8);
    }

    public static Bitmap getRoundedCorner(Bitmap bitmap, float roundPx) {
        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final RectF rectF = new RectF(rect);

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

        paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        return output;
    }


}
