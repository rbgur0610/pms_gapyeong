package com.pms.gapyeong.common;

import android.content.Context;
import android.content.SharedPreferences;

public class Preferences
{

    public static String getValue(Context context, String args, String key, String defaultValue)
    {
        SharedPreferences prefs = context.getSharedPreferences(args, 0);
        return prefs.getString(key, defaultValue);
    }

    public static int getValue(Context context, String args, String key, int defaultValue)
    {
        SharedPreferences prefs = context.getSharedPreferences(args, 0);
        return prefs.getInt(key, defaultValue);
    }

    public static boolean getValue(Context context, String args, String key, boolean defaultValue)
    {
        SharedPreferences prefs = context.getSharedPreferences(args, 0);
        return prefs.getBoolean(key, defaultValue);
    }

    public static long getValue(Context context, String args, String key, long defaultValue)
    {
        SharedPreferences prefs = context.getSharedPreferences(args, 0);
        return prefs.getLong(key, defaultValue);
    }

    public static boolean putValue(Context context, String args, String key, String value)
    {
        SharedPreferences prefs = context.getSharedPreferences(args, 0);
        android.content.SharedPreferences.Editor ed = prefs.edit();
        ed.putString(key, value);
        return ed.commit();
    }

    public static boolean putValue(Context context, String args, String key, int value)
    {
        SharedPreferences prefs = context.getSharedPreferences(args, 0);
        android.content.SharedPreferences.Editor ed = prefs.edit();
        ed.putInt(key, value);
        return ed.commit();
    }

    public static boolean putValue(Context context, String args, String key, long value)
    {
        SharedPreferences prefs = context.getSharedPreferences(args, 0);
        android.content.SharedPreferences.Editor ed = prefs.edit();
        ed.putLong(key, value);
        return ed.commit();
    }

    public static boolean putValue(Context context, String args, String key, boolean value)
    {
        SharedPreferences prefs = context.getSharedPreferences(args, 0);
        android.content.SharedPreferences.Editor ed = prefs.edit();
        ed.putBoolean(key, value);
        return ed.commit();
    }

    public static String getValue(Context context, String key, String defaultValue)
    {
        SharedPreferences prefs = context.getSharedPreferences(PREFERENCES_NAME, 0);
        return prefs.getString(key, defaultValue);
    }

    public static int getValue(Context context, String key, int defaultValue)
    {
        SharedPreferences prefs = context.getSharedPreferences(PREFERENCES_NAME, 0);
        return prefs.getInt(key, defaultValue);
    }

    public static boolean getValue(Context context, String key, boolean defaultValue)
    {
        SharedPreferences prefs = context.getSharedPreferences(PREFERENCES_NAME, 0);
        return prefs.getBoolean(key, defaultValue);
    }

    public static long getValue(Context context, String key, long defaultValue)
    {
        SharedPreferences prefs = context.getSharedPreferences(PREFERENCES_NAME, 0);
        return prefs.getLong(key, defaultValue);
    }

    public static boolean putValue(Context context, String key, String value)
    {
        SharedPreferences prefs = context.getSharedPreferences(PREFERENCES_NAME, 0);
        android.content.SharedPreferences.Editor ed = prefs.edit();
        ed.putString(key, value);
        return ed.commit();
    }

    public static boolean putValue(Context context, String key, int value)
    {
        SharedPreferences prefs = context.getSharedPreferences(PREFERENCES_NAME, 0);
        android.content.SharedPreferences.Editor ed = prefs.edit();
        ed.putInt(key, value);
        return ed.commit();
    }

    public static boolean putValue(Context context, String key, long value)
    {
        SharedPreferences prefs = context.getSharedPreferences(PREFERENCES_NAME, 0);
        android.content.SharedPreferences.Editor ed = prefs.edit();
        ed.putLong(key, value);
        return ed.commit();
    }

    public static boolean putValue(Context context, String key, boolean value)
    {
        SharedPreferences prefs = context.getSharedPreferences(PREFERENCES_NAME, 0);
        android.content.SharedPreferences.Editor ed = prefs.edit();
        ed.putBoolean(key, value);
        return ed.commit();
    }

    static String PREFERENCES_NAME = "common_setting";

}
