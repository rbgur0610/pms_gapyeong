package com.pms.gapyeong.common;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Environment;

import com.pms.gapyeong.R;

public class FileUtil {

    /**
     * 파일 존재 여부 체크
     *
     * @param fileName 저장파일
     * @return
     */
    public static boolean isFile(String fileName) {
        if (!Util.isEmpty(fileName)) {
            File file = new File(fileName);
            return file.isFile();
        } else return false;
    }

    /**
     * 전체 저장 경로에서 파일명 구하기
     *
     * @param fileName 저장파일
     * @return
     */
    public static String fileNameOnly(String fileName) {
        // String path =  fileName.substring(0, url.lastIndexOf("/")+1);
        String name = fileName.substring(fileName.lastIndexOf("/") + 1, fileName.length());
        return name;
    }

    /**
     * 파일 확장자 구하기
     *
     * @param fileName 저장파일
     * @return
     */
    public static String fileNameExt(String fileName) {
        String ext = fileName.substring(fileName.lastIndexOf('.') + 1, fileName.length());
        return Util.isNVL(ext).toLowerCase();
    }

    /**
     * 파일 확장자를 제외한 파일명 구하기
     *
     * @param fileName 저장파일
     * @return
     */
    public static String fileRemoveExt(String fileName) {
        String fileExcept = fileName.substring(0, fileName.lastIndexOf('.'));
        return Util.isNVL(fileExcept);
    }

    /**
     * 이미지 파일인지 확인
     *
     * @param fileName 저장파일
     * @return
     */
    public static boolean isImageFile(String fileName) {
        String ext = fileNameExt(fileName);
        return Constants.IMG_EXT.indexOf(Util.isNVL(ext).toLowerCase()) >= 0;
    }

    /**
     * 파일을 이동시켜주는 메소드
     *
     * @param inFilePath  - 현재 저장된 파일경로
     * @param inFileName  - 현재 저장된 파일명
     * @param outFilePath - 이동 시킬 파일경로
     * @param outFileName - 이동 시킬 파일명
     */
    public static String fileMove(String inFilePath, String inFileName, String outFilePath, String outFileName) {
        FileInputStream fis = null;
        FileOutputStream fos = null;
        try {
            File inFile = new File(inFilePath);
            //해당 디렉토리가 존재하지 않으면 디렉터리 생성
            if (!inFile.exists()) {
                inFile.mkdirs();
            }
            File outFile = new File(outFilePath);
            //해당 디렉토리가 존재하지 않으면 디렉터리 생성
            if (!outFile.exists()) {
                outFile.mkdirs();
            }

            inFileName = inFilePath + inFileName;
            String moveFileName = outFilePath + outFileName;

            File moveFile = new File(moveFileName);

            if (moveFile.exists()) {
                int i = 1;
                do {
                    String tempName = outFileName.substring(0, outFileName.lastIndexOf('.'));
                    String ext = outFileName.substring(outFileName.lastIndexOf('.') + 1, outFileName.length());
                    tempName = tempName + "[" + i + "]";
                    String newFileName = tempName + "." + ext;
                    moveFileName = outFilePath + newFileName;
                    File isfile = new File(moveFileName);
                    if (!isfile.exists()) {
                        outFileName = newFileName;
                        break;
                    }
                    i++;
                } while (true);
            }

            fis = new FileInputStream(inFileName);
            fos = new FileOutputStream(moveFileName);

            int data = 0;
            while ((data = fis.read()) != -1) {
                fos.write(data);
            }


            //복사한뒤 원본파일을 삭제함
            fileDelete(inFileName);
        } catch (IOException e) {
            if (Constants.DEBUG_PRINT_LOG) {
                e.printStackTrace();
            } else {
                System.out.println("예외 발생");
            }
        } finally {
            try {
                fis.close();
                fos.close();
            } catch (IOException e) {
                System.out.println("IOException 예외 발생");
            }

        }
        return outFileName;
    }

    /**
     * 파일을 삭제하는 메소드
     *
     * @param deleteFileName
     * @return
     */
    public static boolean fileDelete(String deleteFileName) {
        File file = new File(deleteFileName);
        return file.delete();
    }

    /**
     * 해당 디렉터리 삭제 (하위 디렉터리 및 파일까지 삭제)
     *
     * @param path
     * @return
     */
    public static boolean directoryDelete(File path) {
        if (!path.exists()) {
            return false;
        }

        File[] files = path.listFiles();
        if (files == null) return false;

        for (File file : files) {
            if (file.isDirectory()) {
                directoryDelete(file);
            } else {
                file.delete();
            }
        }

        return path.delete();
    }

    public static String getExternalCacheDirectory(Context context) {

        String extCacheDirPath = null;

        File cacheDir = context.getExternalCacheDir();
        // 외부 캐쉬 저장소
        if (cacheDir != null) {
            extCacheDirPath = cacheDir.getPath();
        } else {
            // context.getCacheDir()
            // 외부 캐쉬 저장소가 없다면 에러 메시 출력
            MsgUtil.AlertDialog(context, R.string.msg_media_error, R.string.btn_ok);
        }
        return extCacheDirPath;
    }

    public static String getExternalDownLoadDirectory() {

        File file = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);

        String extCacheDirPath = "";

        // 다운로드 저장소
        if (file != null) {
            extCacheDirPath = file.getAbsolutePath();
        }
        return extCacheDirPath;
    }

    public static boolean isExternalStorageAvailable() {
        boolean state = false;
        String extStorageState = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(extStorageState)) {
            state = true;
        }
        return state;
    }

    public static boolean isExternalStorageReadOnly() {
        boolean state = false;
        String extStorageState = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(extStorageState)) {
            state = true;
        }
        return state;
    }

    /*
     * An InputStream that skips the exact number of bytes provided, unless it reaches EOF.
     */
    public static class FlushedInputStream extends FilterInputStream {
        public FlushedInputStream(InputStream inputStream) {
            super(inputStream);
        }

        @Override
        public long skip(long n) throws IOException {
            long totalBytesSkipped = 0L;
            while (totalBytesSkipped < n) {
                long bytesSkipped = in.skip(n - totalBytesSkipped);
                if (bytesSkipped == 0L) {
                    int b = read();
                    if (b < 0) {
                        break;  // we reached EOF
                    } else {
                        bytesSkipped = 1; // we read one byte
                    }
                }
                totalBytesSkipped += bytesSkipped;
            }
            return totalBytesSkipped;
        }
    }

    public static void CopyStream(InputStream is, OutputStream os) {
        final int buffer_size = 1024;
        try {
            byte[] bytes = new byte[buffer_size];
            for (; ; ) {
                int count = is.read(bytes, 0, buffer_size);
                if (count == -1)
                    break;
                os.write(bytes, 0, count);
            }
        } catch (IOException e) {
            if (Constants.DEBUG_PRINT_LOG) {
                e.printStackTrace();
            } else {
                System.out.println("예외 발생");
            }
        } catch (Exception ex) {
            if (Constants.DEBUG_PRINT_LOG) {
                ex.printStackTrace();
            } else {
                System.out.println("예외 발생");
            }
        }
    }

}
