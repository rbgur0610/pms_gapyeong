package com.pms.gapyeong.common;

import java.lang.reflect.Field;
import java.util.Calendar;

import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.Context;
import android.view.View;
import android.widget.DatePicker;

public class DatePickerMonth {

    public interface onDateChangeCallBack {
        void onDateChangeCallBack(int year, int month, int day);
    }

    private onDateChangeCallBack mCallback;

    public void setOndateChangeCallBack(onDateChangeCallBack callback) {
        mCallback = callback;
    }

    private int mYear;
    private int mMonth;
    private int mDay = 1;
    private DatePickerDialog datePickerDialog;
    private Context mContext;

    public DatePickerMonth(Context ctx, int yy, int mm, int dd) {
        // TODO Auto-generated constructor stub
        mContext = ctx;
        if (yy == 0) {
            final Calendar c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
        } else {
            mYear = yy;
            mMonth = mm;
            mDay = dd;
        }

        datePickerDialog = this.customDatePicker();
        datePickerDialog.show();
    }


    DatePickerDialog.OnDateSetListener mDateSetListner = new OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {

            mYear = year;
            mMonth = monthOfYear;
            mDay = dayOfMonth;

            if (mCallback != null) {
                mCallback.onDateChangeCallBack(mYear, mMonth, mDay);
            }
            datePickerDialog.dismiss();
//			updateDate();
        }
    };


    public void updateDate() {
        int localMonth = (mMonth + 1);
        String monthString = localMonth < 10 ? "0" + localMonth : Integer
                .toString(localMonth);
        String localYear = Integer.toString(mYear).substring(2);
        String date = new StringBuilder().append(localYear).append("/").append(monthString).append("").toString();
        // Month is 0 based so add 1
        datePickerDialog.show();
    }

    private DatePickerDialog customDatePicker() {
        DatePickerDialog dpd = new DatePickerDialog(mContext, mDateSetListner,
                mYear, mMonth, mDay);
        try {

            Field[] datePickerDialogFields = dpd.getClass().getDeclaredFields();
            for (Field datePickerDialogField : datePickerDialogFields) {
                if (datePickerDialogField.getName().equals("mDatePicker")) {
                    datePickerDialogField.setAccessible(true);
                    DatePicker datePicker = (DatePicker) datePickerDialogField
                            .get(dpd);
                    Field datePickerFields[] = datePickerDialogField.getType()
                            .getDeclaredFields();
                    for (Field datePickerField : datePickerFields) {
                        if ("mDayPicker".equals(datePickerField.getName())
                                || "mDaySpinner".equals(datePickerField
                                .getName())) {
                            datePickerField.setAccessible(true);
                            Object dayPicker = new Object();
                            dayPicker = datePickerField.get(datePicker);
                            ((View) dayPicker).setVisibility(View.GONE);
                        }
                    }
                }

            }
        } catch (IllegalAccessException e) {
            if(Constants.DEBUG_PRINT_LOG){
                e.printStackTrace();
            }else{
                System.out.println("예외 발생");
            }
        } catch (Exception e) {
            if(Constants.DEBUG_PRINT_LOG){
                e.printStackTrace();
            }else{
                System.out.println("예외 발생");
            }
        }
        return dpd;
    }

}
