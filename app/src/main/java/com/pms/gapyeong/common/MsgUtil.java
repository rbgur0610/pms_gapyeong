package com.pms.gapyeong.common;

import com.pms.gapyeong.R;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.widget.Toast;

public class MsgUtil {

	  public static void ToastMessage (Context context, int message)
	  {
   	  Toast toast = Toast.makeText(context, message, Toast.LENGTH_SHORT); 
   	  toast.show();      		  
	  }
	  
	  public static void ToastMessage (Context context, String message)
	  {
		  Toast toast = Toast.makeText(context, message, Toast.LENGTH_SHORT); 
		  toast.show();      		  
	  }
	  
	  public static void ToastMessage (Context context, String message,int duration)
	  {
		  Toast toast = Toast.makeText(context, message, duration); 
		  toast.show();      		  
	  }	
	
	  public static void AlertDialog (Context context,int msg, int btnNm)
	  {
		  AlertDialog(context,R.string.title_basic, msg, btnNm);
	  }	
	  
	  public static void AlertDialog (Context context,int title, int msg, int btnNm)
	  {
		    new android.app.AlertDialog.Builder(context)
		    .setIcon(android.R.drawable.ic_dialog_alert)
		    .setTitle(title).setMessage(msg)
		    .setPositiveButton(btnNm, new DialogInterface.OnClickListener()
		    {
		        @Override
		        public void onClick( DialogInterface dialog, int which )
		        {
		        	  dialog.cancel();
		        }
		    }).show();			  
	  }	  
	  
	  public static void AlertDialog (Context context, String msg)
	  {
		  AlertDialog (context, "", msg);
	  }	  
	  
	  public static void AlertDialog (Context context,String title, String msg)
	  {
		  new android.app.AlertDialog.Builder(context)
		  .setIcon(android.R.drawable.ic_dialog_alert)
		  .setTitle(title).setMessage(msg)
		  .setPositiveButton(R.string.btn_ok, new DialogInterface.OnClickListener()
		  {
			  @Override
			  public void onClick( DialogInterface dialog, int which )
			  {
				  dialog.cancel();
			  }
		  }).show();			  
	  }	  
	  
	  public static void AlertDialogClose(final Activity activity, String msg) {
		 AlertDialog.Builder alert = new AlertDialog.Builder(activity);
		 alert.setTitle("");
		 alert.setMessage(msg);
		 alert.setPositiveButton(R.string.btn_ok, new DialogInterface.OnClickListener() 
		 {
	 		 @Override
			 public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
				activity.finish();
			 }
		 });
		 alert.show();	
	  }		  
	  
}
