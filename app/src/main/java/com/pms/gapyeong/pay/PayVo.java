package com.pms.gapyeong.pay;

import java.io.Serializable;

public class PayVo implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7986344188971358735L;
	
	private String cashAmt          = "0";  // 현금금액
	private String cashApprovalNo   = "";   // 현금 영수증 승인번호
	private String cashApprovalDate = "";   // 현금 영수증 승인일시
	
	private String transactionNo = "";   // KCP 거래번호
	private String approvalNo	 = "";   // 카드 결제 승인번호 
	private String approvalDate  = "";   // 카드 결제 승인일시
	private String cardNo	     = "";   // 카드번호
	private String cardCompany   = "";	 // 카드사명
	private String cardAmt 	     = "0";  // 카드금액
	
	private String advMethod   	 = "";   // 선불권 처리방법
	private String catNumber	 = "";   // 기계할당번호[캣넘버]
	private String truncAmt      = "0";
	private String couponAmt     = "0";
	
	public String getTruncAmt() {
		return truncAmt;
	}
	
	public void setTruncAmt(String _truncAmt) {
		this.truncAmt = _truncAmt;
	}	
	
	public String getCashAmt() {
		return cashAmt;
	}
	
	public void setCashAmt(String cashAmt) {
		this.cashAmt = cashAmt;
	}
	
	public String getCashApprovalNo() {
		return cashApprovalNo;
	}

	public void setCashApprovalNo(String cashApprovalNo) {
		this.cashApprovalNo = cashApprovalNo;
	}

	public String getCashApprovalDate() {
		return cashApprovalDate;
	}

	public void setCashApprovalDate(String cashApprovalDate) {
		this.cashApprovalDate = cashApprovalDate;
	}	
	
	public String getCardAmt() {
		return cardAmt;
	}
	
	public void setCardAmt(String cardAmt) {
		this.cardAmt = cardAmt;
	}
	
	public String getCardNo() {
		return cardNo;
	}
	
	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}
	
	public String getCardCompany() {
		return cardCompany;
	}
	
	public void setCardCompany(String cardCompany) {
		this.cardCompany = cardCompany;
	}
	
	public String getTransactionNo() {
		return transactionNo;
	}
	
	public void setTransactionNo(String transactionNo) {
		this.transactionNo = transactionNo;
	}
	
	public String getApprovalNo() {
		return approvalNo;
	}
	
	public void setApprovalNo(String approvalNo) {
		this.approvalNo = approvalNo;
	}
	
	public String getApprovalDate() {
		return approvalDate;
	}
	
	public void setApprovalDate(String approvalDate) {
		this.approvalDate = approvalDate;
	}
	
	public String getAdvMethod() {
		return advMethod;
	}

	public void setAdvMethod(String advMethod) {
		this.advMethod = advMethod;
	}	
	
	public String getCatNumber() {
		return catNumber;
	}
	
	public void setCatNumber(String catNumber) {
		this.catNumber = catNumber;
	}
	
	public void setCouponAmt(String coupon)
	{
		this.couponAmt = coupon;
	
	}
	
	public String getCouponAmt()
	{
		return this.couponAmt;
	}
	
}
