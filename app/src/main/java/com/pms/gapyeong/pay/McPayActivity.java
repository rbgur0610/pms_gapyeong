package com.pms.gapyeong.pay;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.pms.gapyeong.activity.BaseActivity;
import com.pms.gapyeong.common.Constants;
import com.pms.gapyeong.common.Preferences;
import com.pms.gapyeong.common.Util;
import com.pms.gapyeong.vo.ReceiptVo;


import com.mcpay.call.api.McPaymentAPI;
import com.mcpay.call.bean.PaymentInfoBean;
import com.mcpay.call.bean.ReturnDataBean;


public class McPayActivity extends BaseActivity {

    private McPaymentAPI mPaymentAPI = McPaymentAPI.getInstance();
    private PaymentInfoBean mPaymentInfoBean;
    private Context mContext = this;

    private ReturnDataBean mReturnDataBean;
    private TextView returnText;
    private ImageView imageSign;

    private final int CHECK_PAYMENTCODE_FAIL = 1; // 전문코드오류
    private final int CHECK_BUSSNO_FAIL = 2; // 사업자번호 오류
    private final int CHECK_PACKAGE_FAIL = 3; // 페키지네임 오류
    private final int CHECK_CLASS_FAIL = 4; // 클래스네임 오류
    private final int CHECK_PRINTYN_FAIL = 5; // 전표출력여부 오류
    private final int CHECK_AMOUNT_FAIL = 6; // 결제금액 오류
    private final int CHECK_TAX_FAIL = 7; // 세금 오류
    private final int CHECK_SVC_FAIL = 8; // 봉사료 오류
    private final int CHECK_INST_FAIL = 9; // 할부개월 오류
    private final int CHECK_ADDPRINTLEN_FAIL = 12; // 추가인쇄길이 오류
    private final int CHECK_APPROVALDATE_FAIL = 13; // 원거래승인일시 오류
    private final int CHECK_APPROVALNO_FAIL = 14; // 원거래승인번호 오류
    private final int CHECK_CASHTYPE_FAIL = 15; // 현금영수증 거래유형 오류
    private final int CHECK_CASHINFO_FAIL = 16; // 현금영수증 발급대상정보 오류
    private final int CHECK_CANCELREASON_FAIL = 17; // 취소사유 오류
    private final int CHECK_VERIFICATIONCODE_FAIL = 18; // 요청인증번호 오류
    private final int CHECK_TRADETYPE_FAIL = 19; // 결제구분 오류

    private final int REQUEST_CODE_FAIL = -1; // 전문코드 오류
    private final int REQUEST_CONTEXT_NULL = -2; // Context is null
    private final int REQUEST_BEAN_NULL = -3; // PaymentInfoBean is null
    private final int REQUEST_NOT_INSTALL = -4; // 엠씨페이IC 설치안됨

    private String verificationCode = ""; // 요청인증번호

    private final int PAYMENT_FINISH = 0x000022333;

    private ReceiptVo vo;

    private TextView m_txtInfoMsg;

    private TextView txtAuthno;
    private Bundle eB;

    private int DIALOG_TYPE = 0;
    private String PAYMENT_MONEY = "0";
    private String APPROVAL_NO = "";
    private String APPROVAL_DATE = "";
    private String CASHRECEIPT_NO = "";

    private String card_number = "";
    private String PARK_TYPE = "";
    private int returnCode = 1000;
    private String TradeInfo = "";
    private String Product = "";
    private String Buyer = "";
    private String charSet = "euc-kr";

    // 결제 관련 객체
    private PayData payData;

    boolean isSCR;
    private boolean CashTradeType;
    private int MONTHLY_PAY = 0;


    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        setTitle("결제하기");
        super.onCreate(savedInstanceState);

        Preferences.putValue(this, Constants.PREFERENCE_MCPAY_TRANSACTION_NO, "");
        Preferences.putValue(this, Constants.PREFERENCE_MCPAY_APPROVAL_NO, "");
        Preferences.putValue(this, Constants.PREFERENCE_MCPAY_APPROVAL_DATE, "");
        Preferences.putValue(this, Constants.PREFERENCE_MCPAY_CARD_NO, "");
        Preferences.putValue(this, Constants.PREFERENCE_MCPAY_CARD_COMPANY, "");
        Preferences.putValue(this, Constants.PREFERENCE_MCPAY_TOTAL_AMT, "");
        Preferences.putValue(this, Constants.PREFERENCE_MCPAY_PARAMETER, "");

        verificationCode = String.valueOf(System.currentTimeMillis());

        Intent i = getIntent();
        if (i != null) {
            MONTHLY_PAY = i.getIntExtra("MONTHLY_PAY", 0);
            CashTradeType = i.getBooleanExtra("CashTradeType", true);
            DIALOG_TYPE = i.getIntExtra("SHOW_DIALOG_TYPE", 0);
            PAYMENT_MONEY = i.getStringExtra("PAYMENT_MONEY");
            PAYMENT_MONEY = Util.isEmpty(PAYMENT_MONEY) ? "0" : PAYMENT_MONEY;
            PARK_TYPE = Util.isNVL(i.getStringExtra("PARK_TYPE"));
            APPROVAL_NO = i.getStringExtra("APPROVAL_NO");
            vo = (ReceiptVo) i.getSerializableExtra("ReceiptVo");

            if (vo.getCarNum().length() > 7) {
                Buyer = vo.getCarNum().substring(2, vo.getCarNum().length());
            } else {
                Buyer = vo.getCarNum();
            }

            mPaymentInfoBean = new PaymentInfoBean();
            payData = new PayData(this, OUT_PRINT, IN_PRINT, TICKET_PRINT);

            Preferences.putValue(this, Constants.PREFERENCE_MCPAY_TOTAL_AMT, PAYMENT_MONEY);
            Preferences.putValue(this, Constants.PREFERENCE_MCPAY_PARAMETER, String.valueOf(DIALOG_TYPE));


            if (DIALOG_TYPE == PayData.PAY_DIALOG_CANCEL_ENTRANCE_TIME
                    || DIALOG_TYPE == PayData.PAY_DIALOG_CANCEL_APPROVAL_EXIT
                    || DIALOG_TYPE == PayData.PAY_DIALOG_CANCEL_APPROVAL_CHANGE
                    || DIALOG_TYPE == PayData.PAY_DIALOG_CANCEL_APPROVAL_CANCEL) {

                if ("".equals(i.getStringExtra("APPROVAL_DATE"))) {
                    APPROVAL_DATE = i.getStringExtra("APPROVAL_DATE");
                } else {
                    APPROVAL_DATE = i.getStringExtra("APPROVAL_DATE").substring(0, 8);
                }
                CancelCreditCard(PAYMENT_MONEY);
            } else if (DIALOG_TYPE == PayData.PAY_DIALOG_TYPE_PREPAY_CARD
                    || DIALOG_TYPE == PayData.PAY_DIALOG_TYPE_UNPAY_CARD
                    || DIALOG_TYPE == PayData.PAY_DIALOG_TYPE_TICKET_CARD
                    || DIALOG_TYPE == PayData.PAY_DIALOG_TYPE_EXIT_CARD) {

                switch (DIALOG_TYPE)//전국
                {
                    case PayData.PAY_DIALOG_TYPE_PREPAY_CARD:
                    case PayData.PAY_DIALOG_TYPE_EXIT_CARD:
                        Product = "PARK1";
                        break;
                    case PayData.PAY_DIALOG_TYPE_UNPAY_CARD:
                        Product = "MISU";
                        break;
                    case PayData.PAY_DIALOG_TYPE_TICKET_CARD:
                        Product = "JUNG";
                        break;

                }
                AuthCreditCard(PAYMENT_MONEY);
            } else if (DIALOG_TYPE == PayData.PAY_DIALOG_TYPE_CASH_RECEIPT) {
                CASHRECEIPT_NO = i.getStringExtra("CASHRECEIPT_NO");
                AuthCashReceipt(PAYMENT_MONEY, CASHRECEIPT_NO);
            }

        }

    }

    // 공용입력 부분
    private void makeHeaderBean() {
        // 가맹점 사업자등록번호
        // 엠씨페이IC 결제프로그램에 등록된 사업자등록번호와 틀릴 경우 결제가 진행되지 않음(Max : 10 bytes)
        mPaymentInfoBean.setPaymentBussNo("1328300479");

        // 통보 메시지를 받을 프로그램의 Package명(Max : 50 bytes)
        mPaymentInfoBean.setPackageName("com.pms.gapyeong");

        // 통보 메시지를 받을 프로그램의 Class명(Max : 50 bytes)
        mPaymentInfoBean.setClassName("com.pms.gapyeong.pay.ReturnMcPayOn");
    }


    private void CancelCreditCard(String _money) {
        // 전문코드(Max : 3 bytes)
        makeHeaderBean();
        mPaymentInfoBean.setVerificationCode(verificationCode);
        mPaymentInfoBean.setPaymentCode("N02");

        // 결제프로그램에서 전표를 출력할지 여부 0 : 출력안함, 1 : 출력함(Max : 1 bytes)
        mPaymentInfoBean.setPaymentReceiptYn(0);

        // 원거래 승인시 통보받은 일자. 'YYYYMMDD' 형태의 문자열
        // ex) '20160614'(Max : 8 bytes)
        //mPaymentInfoBean.setVoidApprovalDate(APPROVAL_DATE);
        Log.d("111", APPROVAL_DATE.substring(2, APPROVAL_DATE.length()) + "~" + APPROVAL_NO + "~" + _money + "~" + verificationCode);
        mPaymentInfoBean.setVoidApprovalDate(APPROVAL_DATE.substring(2, APPROVAL_DATE.length()));
        // 원거래 승인시 통보받은 승인번호(Max : 12 bytes)
        mPaymentInfoBean.setVoidApprovalNo(APPROVAL_NO);
        // 승인취소할 총금액(판매금액+세금+봉사료)
        // (Max : 10 bytes)
        mPaymentInfoBean.setPaymentTotalPrice(Integer.valueOf(_money));

        // 승인취소할 부가세 등 세금(Max : 9 bytes)
        mPaymentInfoBean.setPaymentTax(0);

        // 봉사료. 사용안할 시에는 0(Max : 9 bytes)
        mPaymentInfoBean.setPaymentSvc(0);

        // 할부개월 일시불일 경우 0(Max : 2 bytes)
        mPaymentInfoBean.setPaymentInst(0);

        mPaymentInfoBean.setRequestVanFormat(requestVanFormat());
        callMcpayIc();
    }

    private void AuthCreditCard(String _money) {
        // 공용 입력 파라미터
        makeHeaderBean();

        mPaymentInfoBean.setVerificationCode(verificationCode);
        // 전문코드(Max : 3 bytes)
        mPaymentInfoBean.setPaymentCode("N01");

        // 결제프로그램에서 전표를 출력할지 여부 0 : 출력안함, 1 : 출력함(Max : 1 bytes)
        mPaymentInfoBean.setPaymentReceiptYn(0);

        // 결제할 총금액(판매금액+세금+봉사료)
        // (Max : 10 bytes)
        //mPaymentInfoBean.setPaymentTotalPrice(Integer.valueOf(_money));
        mPaymentInfoBean.setPaymentTotalPrice(Integer.valueOf(_money));

        // 부가세 등 세금(Max : 9 bytes)
        // 세금 0원 입력시 아래와 같은 세금계산방법으로 자동 진행됨.
        // 거래금액 = 결제할 총금액 - 봉사료
        // 세금 = (거래금액 * 10) / (100 + 10)
        mPaymentInfoBean.setPaymentTax(0);

        // 봉사료. 사용안할 시에는 0(Max : 9 bytes)
        mPaymentInfoBean.setPaymentSvc(0);

        // 할부개월 일시불일 경우 0(Max : 2 bytes)
        mPaymentInfoBean.setPaymentInst(MONTHLY_PAY);

        mPaymentInfoBean.setRequestVanFormat(requestVanFormat());

        callMcpayIc();

    }

    private static String cutFirstStrInByte(String str, int endIndex) {
        StringBuffer sbStr = new StringBuffer(endIndex);
        int iTotal = 0;
        for (char c : str.toCharArray()) {
            iTotal += String.valueOf(c).getBytes().length;
            if (iTotal > endIndex) {
                break;
            }
            sbStr.append(c);
        }
        return sbStr.toString();
    }


    private void AuthCashReceipt(String _money, String _approval) {
        // 전문코드(Max : 3 bytes)
        makeHeaderBean();
        mPaymentInfoBean.setVerificationCode(verificationCode);
        mPaymentInfoBean.setPaymentCode("N03");

        // 결제프로그램에서 전표를 출력할지 여부 0 : 출력안함, 1 : 출력함(Max : 1 bytes)
        mPaymentInfoBean.setPaymentReceiptYn(1);

        // 0 : 소득공제(개인대상), 1 : 지출증빙(업체대상)
        // (Max : 1 bytes)
        mPaymentInfoBean.setCashTradeType(CashTradeType ? 0 : 1);

        // 핸드폰번호, 주민등록번호, 사업자등록번호 등. (카드번호는 안됨)
        // 내용이 없을 경우 엠씨페이IC 결제프로그램에서 카드리딩, 직접입력을 받음
        // (Max : 20 bytes)
        mPaymentInfoBean.setCashTradeInfo(_approval);

        // 결제할 총금액(판매금액+세금+봉사료)
        // (Max : 10 bytes)
        mPaymentInfoBean.setPaymentTotalPrice(Integer.valueOf(_money));
        Log.d("111", _approval + "~" + _money);

        // 부가세 등 세금(Max : 9 bytes)
        // 세금 0원 입력시 아래와 같은 세금계산방법으로 자동 진행됨.
        // 거래금액 = 결제할 총금액 - 봉사료
        // 세금 = (거래금액 * 10) / (100 + 10)
        mPaymentInfoBean.setPaymentTax(0);

        // 봉사료. 사용안할 시에는 0(Max : 9 bytes)
        mPaymentInfoBean.setPaymentSvc(0);

        // 추가인쇄내용
        // 승인 전표에 별도로 인쇄되는 내용.
        // 프린터 인쇄폭은 32Byte,
        // 줄구분은 @@ 2 bytes 사용, 줄당 최대 글자수 32 bytes
        //mPaymentInfoBean.setPaymentPrintData("기사번호:758656@@주문번호:1023146"); // 예시
        //mPaymentInfoBean.setDataShopUseField(TradeInfo);
        // 거래번호(Max : 15 bytes)
        // 엠씨페이IC에 저장된 거래내역 중 같은 거래번호가 있는 경우 중복거래로 판단 저장된 거래내역 바로 리턴
        // 한글 & 특수문자를 제외한 15자리의 유니크한 값 지정
        // Ex) Trade1609010001
        // 거래요청 후 정상응답 받은 경우 다음 거래번호(Trade1609010002)는 변경된다.
        // 정상응답을 받지 못한 경우 거래번호(Trade16090100001)는 유지된다.
        //String tno = Util.getYmdhms("yyyyMMdd") + "" + Util.getYmdhms("HHmmss")+ "" +parkCode;

        //mPaymentInfoBean.setPaymentTradeNo(tno); // 예시
        mPaymentInfoBean.setRequestVanFormat(requestVanFormat());
        callMcpayIc();
    }

    private void CancelCashReceipt(String _money, String _approval) {
        // 전문코드(Max : 3 bytes)
        makeHeaderBean();
        mPaymentInfoBean.setVerificationCode(verificationCode);
        mPaymentInfoBean.setPaymentCode("N04");

        // 결제프로그램에서 전표를 출력할지 여부(0 : 출력안함, 1 : 출력함)
        // (Max : 1 bytes)
        mPaymentInfoBean.setPaymentReceiptYn(0);

        // 0 : 소득공제(개인대상), 1 : 지출증빙(업체대상)
        // (Max : 1 bytes)
        mPaymentInfoBean.setCashTradeType(0);

        // 핸드폰번호, 주민등록번호, 사업자등록번호 등. (카드번호는 안됨)
        // 내용이 없을 경우 엠씨페이IC 결제프로그램에서 카드리딩, 직접입력을 받음
        // (Max : 20 bytes)
        mPaymentInfoBean.setCashTradeInfo("");

        // 원거래 승인시 통보받은 일자. 'YYYYMMDD' 형태의 문자열
        // ex) '20160614'(Max : 8 bytes)
        mPaymentInfoBean.setVoidApprovalDate(APPROVAL_DATE);

        // 원거래 승인시 통보받은 승인번호(Max : 12 bytes)
        mPaymentInfoBean.setVoidApprovalNo(APPROVAL_NO);

        // 현금거래 취소 사유 코드(1 : 거래취소, 2 : 오류 발급, 3 : 기타)
        // (Max : 1 bytes)
        mPaymentInfoBean.setVoidCashReason(1);

        // 결제할 총금액(판매금액+세금+봉사료)
        // (Max : 10 bytes)
        mPaymentInfoBean.setPaymentTotalPrice(Integer.valueOf(_money));

        // 부가세 등 세금(Max : 9 bytes)
        mPaymentInfoBean.setPaymentTax(0);

        // 봉사료. 사용안할 시에는 0(Max : 9 bytes)
        mPaymentInfoBean.setPaymentSvc(0);

        // 추가인쇄내용
        // 승인 전표에 별도로 인쇄되는 내용.
        // 프린터 인쇄폭은 32Byte,
        // 줄구분은 @@ 2 bytes 사용, 줄당 최대 글자수 32 bytes
        //mPaymentInfoBean.setPaymentPrintData("기사번호:758656@@주문번호:1023146"); // 예시
        //mPaymentInfoBean.setDataShopUseField(TradeInfo);
        // 거래번호(Max : 15 bytes)
        // 엠씨페이IC에 저장된 거래내역 중 같은 거래번호가 있는 경우 중복거래로 판단 저장된 거래내역 바로 리턴
        // 한글 & 특수문자를 제외한 15자리의 유니크한 값 지정
        // Ex) Trade1609010001
        // 거래요청 후 정상응답 받은 경우 다음 거래번호(Trade1609010002)는 변경된다.
        // 정상응답을 받지 못한 경우 거래번호(Trade16090100001)는 유지된다.
        //String tno = Util.getYmdhms("yyyyMMdd") + "" + Util.getYmdhms("HHmmss")+ "" +parkCode;
        //mPaymentInfoBean.setPaymentTradeNo(tno); // 예시
        mPaymentInfoBean.setRequestVanFormat(requestVanFormat());
        callMcpayIc();
    }


    private String requestVanFormat() {
        /**
         * KCP 전송 포맷(타 VAN사는 사용 금지)
         * (KCP)PG 결제 승인/취소
         * 0001 + 전화번호(최대 20 bytes) + SI(구분자) + 주문자명(최대 30 bytes) +
         * SI(구분자) + 주문번호(최대 50 bytes) + SI(구분자) + 이메일(최대 50 bytes) +
         * SI(구분자) + 상품명(최대 100 bytes)
         */
        byte[] bRequestVanFormat = new byte[1024];
        int index = 0;

        byte[] bBuffer = ("0001").getBytes();
        System.arraycopy(bBuffer, 0, bRequestVanFormat, index, bBuffer.length);
        index += bBuffer.length;

        // 주문자명(최대 20 bytes)

        bBuffer = Buyer.getBytes();//("주문자명").getBytes();
        System.arraycopy(bBuffer, 0, bRequestVanFormat, index, bBuffer.length);
        index += bBuffer.length;

        // SI(구분자)
        bRequestVanFormat[index++] = (byte) 0x0F;

        // 주문자연락처(최대 30 bytes)

        //bBuffer = Buyer.getBytes();//("주문자명").getBytes();
        bBuffer = ("").getBytes();
        System.arraycopy(bBuffer, 0, bRequestVanFormat, index, bBuffer.length);
        index += bBuffer.length;

        // SI(구분자)
        bRequestVanFormat[index++] = (byte) 0x0F;

        // 주문번호(최대 50 bytes)
        bBuffer = verificationCode.getBytes();
        System.arraycopy(bBuffer, 0, bRequestVanFormat, index, bBuffer.length);
        index += bBuffer.length;

        // SI(구분자)
        bRequestVanFormat[index++] = (byte) 0x0F;

        // 이메일(최대 50 bytes)_승인결과 전달받을 이메일주소
        bBuffer = ("").getBytes();
        System.arraycopy(bBuffer, 0, bRequestVanFormat, index, bBuffer.length);
        index += bBuffer.length;

        // SI(구분자)
        bRequestVanFormat[index++] = (byte) 0x0F;

        // 상품명(최대 100 bytes)
        String product = parkName + "-" + empCode;
        bBuffer = product.getBytes();

        System.arraycopy(bBuffer, 0, bRequestVanFormat, index, bBuffer.length);
        index += bBuffer.length;

        byte[] requestVanFormat = new byte[index];
        System.arraycopy(bRequestVanFormat, 0, requestVanFormat, 0, index);

        return new String(requestVanFormat);
    }


    private void callMcpayIc() {
        int returnCode = mPaymentAPI.startRequestPayment(mContext, mPaymentInfoBean);

        if (returnCode == REQUEST_BEAN_NULL) {
            showToast("PaymentInfoBean is null!");
        } else if (returnCode == REQUEST_CODE_FAIL) {
            showToast("없는 전문코드 입니다.");
        } else if (returnCode == REQUEST_CONTEXT_NULL) {
            showToast("Context is null!");
        } else if (returnCode == REQUEST_NOT_INSTALL) {
            showToast("엠씨페이IC App이 존재하지 않습니다.!");
        } else if (returnCode == CHECK_PAYMENTCODE_FAIL) {
            showToast("전문코드를 정확히 입력해 주세요.");
        } else if (returnCode == CHECK_BUSSNO_FAIL) {
            showToast("사업자번호를 정확히 입력해 주세요.");
        } else if (returnCode == CHECK_PACKAGE_FAIL) {
            showToast("리턴받을 PackageName를 정확히 입력해 주세요.");
        } else if (returnCode == CHECK_CLASS_FAIL) {
            showToast("리턴받을 ClassName를 정확히 입력해 주세요.");
        } else if (returnCode == CHECK_PRINTYN_FAIL) {
            showToast("전표출력여부를 정확히 입력해 주세요.");
        } else if (returnCode == CHECK_AMOUNT_FAIL) {
            showToast("결제금액을 정확히 입력해 주세요.");
        } else if (returnCode == CHECK_TAX_FAIL) {
            showToast("세금을 정확히 입력해 주세요.");
        } else if (returnCode == CHECK_SVC_FAIL) {
            showToast("봉사료를 정확히 입력해 주세요.");
        } else if (returnCode == CHECK_INST_FAIL) {
            showToast("할부개월을 정확히 입력해 주세요.");
        } else if (returnCode == CHECK_ADDPRINTLEN_FAIL) {
            showToast("추가인쇄 최대길이는 800bytes 입니다.");
        } else if (returnCode == CHECK_APPROVALDATE_FAIL) {
            showToast("원거래승인일시 14자리를 정확히 입력해 주세요.");
        } else if (returnCode == CHECK_APPROVALNO_FAIL) {
            showToast("원거래승인번호를 정확히 입력해 주세요.");
        } else if (returnCode == CHECK_CASHTYPE_FAIL) {
            showToast("현금영수증 거래유형 '0'소득공제, '1'지출증빙 입니다.");
        } else if (returnCode == CHECK_CASHINFO_FAIL) {
            showToast("현금영수증 발급대상정보를 정확히 입력해 주세요.");
        } else if (returnCode == CHECK_CANCELREASON_FAIL) {
            showToast("현금영수증 취소사유를 정확히 입력해 주세요.");
        } else if (returnCode == CHECK_VERIFICATIONCODE_FAIL) {
            showToast("요청인증번호를 입력해 주세요.");
        } else if (returnCode == CHECK_TRADETYPE_FAIL) {
            showToast("setPaymentType 결제구분을 정확히 입력해 주세요.");
        }


    }

    private void showToast(String msg) {
        Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
    }


}
