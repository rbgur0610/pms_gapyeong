package com.pms.gapyeong.pay;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.mcpay.call.api.McPaymentAPI;
import com.mcpay.call.bean.ReturnDataBean;
import com.pms.gapyeong.R;
import com.pms.gapyeong.common.Constants;
import com.pms.gapyeong.common.Preferences;

public class ReturnMcPayOn extends Activity {
	
	/**
	 * 리턴받는 Activity AndroidManifest.xml에서 아래와 같이 옵션 적용
	 * <intent-filter>
           <action android:name="android.intent.action.MAIN" />
       </intent-filter>
	 */

	private Context mContext = this;
	private McPaymentAPI mPaymentAPI = McPaymentAPI.getInstance();
	private ReturnDataBean mReturnDataBean;
	private TextView returnText;
	private ImageView imageSign;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.returnmcpayon_layout);

		setTitle("엠씨페이온 외부연동 테스트");

		byte[] returnData = mPaymentAPI.getPaymentReturnData(mContext);
		
		mReturnDataBean = mPaymentAPI.parseReturnPayment(returnData);

		
		returnText = (TextView) findViewById(R.id.tv_return);
		imageSign = (ImageView)findViewById(R.id.image_sign);

		String resultData = "";
		
		Log.d("111", "####" + mReturnDataBean.getTradeResultCode() + "~"+ mReturnDataBean.getTradePaymentCode());
		
		if(mReturnDataBean.getTradeResultCode().equals("0000")){
			if(mReturnDataBean.getTradePaymentCode().equals("N01") || mReturnDataBean.getTradePaymentCode().equals("N02")){
				if(mReturnDataBean.getTradePaymentCode().equals("N01"))
					resultData = "[신용결제]";
				else
					resultData = "[신용취소]";
					
				resultData += "\n요청인증번호 : " + mReturnDataBean.getVerificationCode();
				resultData += "\nTID : " + mReturnDataBean.getTradePaymentTid();
				resultData += "\n승인일시 : " + mReturnDataBean.getTradeApprovalDate();
				resultData += "\n승인번호 : " + mReturnDataBean.getTradeApprovalNo();
				resultData += "\n결제금액 : " + mReturnDataBean.getTradeTotalAmount();
				resultData += "\n세금 : " + mReturnDataBean.getTradeTax();
				resultData += "\n봉사료 : " + mReturnDataBean.getTradeSvc();
				resultData += "\n카드명 : " + mReturnDataBean.getTradeCardName();
				resultData += "\n카드번호 : " + mReturnDataBean.getTradeCardNo();
				resultData += "\n카드입력모드 : " + mReturnDataBean.getTradeReadMode();
				resultData += "\n매입사명 : " + mReturnDataBean.getTradePurchaseName();
				resultData += "\n가맹점번호 : " + mReturnDataBean.getTradeShopNo();
				resultData += "\n알림 : " + mReturnDataBean.getTradeNoti();
				resultData += "\n가맹점명 : " + mReturnDataBean.getTradeShopName();
				resultData += "\n대표자명 : " + mReturnDataBean.getTradeCeoName();
				resultData += "\n가맹점전화번호 : " + mReturnDataBean.getTradeShopTelNo();
				resultData += "\n가맹점주소 : " + mReturnDataBean.getTradeShopAddress();
				resultData += "\nVAN이름 : " + mReturnDataBean.getTradeVanName();
				
			
				if(mReturnDataBean.getTradeSignLen() > 0){
					imageSign.setImageBitmap(byteArrayToBitmap(mReturnDataBean.getTradeSignData()));
				}
				
				Intent i = new Intent();
				
				Bundle eB = new Bundle();
	    		eB.putString("transactionNo", mReturnDataBean.getTradePaymentTid());
	    		eB.putString("approvalNo",    mReturnDataBean.getTradeApprovalNo());
	    		eB.putString("approvalDate",  mReturnDataBean.getTradeApprovalDate());
	    		eB.putString("cardNo",        mReturnDataBean.getTradeCardNo());
	    		eB.putString("cardCompany",   mReturnDataBean.getTradeCardName());
	    		eB.putString("cardAmt",	      mReturnDataBean.getTradeTotalAmount());
	    		eB.putString("pioNum",		  "");
	    		eB.putString("carNum",		  "");
				i.putExtras(eB);
				
			
				Preferences.putValue(this,Constants.PREFERENCE_MCPAY_TRANSACTION_NO,  mReturnDataBean.getTradePaymentTid());
				Preferences.putValue(this,Constants.PREFERENCE_MCPAY_APPROVAL_NO,  mReturnDataBean.getTradeApprovalNo());
				Preferences.putValue(this,Constants.PREFERENCE_MCPAY_APPROVAL_DATE,  mReturnDataBean.getTradeApprovalDate());
				Preferences.putValue(this,Constants.PREFERENCE_MCPAY_CARD_NO,  mReturnDataBean.getTradeCardNo());
				Preferences.putValue(this,Constants.PREFERENCE_MCPAY_CARD_COMPANY,  mReturnDataBean.getTradeCardName());
				//Preferences.putValue(this,Constants.PREFERENCE_MCPAY_TOTAL_AMT,  mReturnDataBean.getTradeTotalAmount());
				
				/*
				if(mReturnDataBean.getTradePaymentCode().equals("L01"))
				{
					this.setResult(PayData.PAY_DIALOG_TYPE_PREPAY_CARD, i);
				}
				else if(mReturnDataBean.getTradePaymentCode().equals("L02"))
				{
					this.setResult(PayData.PAY_DIALOG_CANCEL_APPROVAL_CANCEL, i);
				}
				*/
				finish();
				
			}else if(mReturnDataBean.getTradePaymentCode().equals("N03") || mReturnDataBean.getTradePaymentCode().equals("N05")){
				if(mReturnDataBean.getTradePaymentCode().equals("N03"))
					resultData = "[현금결제]";
				else
					resultData = "[현금취소]";
					
				resultData += "\n요청인증번호 : " + mReturnDataBean.getVerificationCode();
				resultData += "\nTID : " + mReturnDataBean.getTradePaymentTid();
				resultData += "\n승인일시 : " + mReturnDataBean.getTradeApprovalDate();
				resultData += "\n승인번호 : " + mReturnDataBean.getTradeApprovalNo();
				resultData += "\n결제금액 : " + mReturnDataBean.getTradeTotalAmount();
				resultData += "\n세금 : " + mReturnDataBean.getTradeTax();
				resultData += "\n봉사료 : " + mReturnDataBean.getTradeSvc();
				resultData += "\n개인정보 : " + mReturnDataBean.getTradeCashInfo();
				resultData += "\n카드입력모드 : " + mReturnDataBean.getTradeReadMode();
				resultData += "\n가맹점명 : " + mReturnDataBean.getTradeShopName();
				resultData += "\n대표자명 : " + mReturnDataBean.getTradeCeoName();
				resultData += "\n가맹점전화번호 : " + mReturnDataBean.getTradeShopTelNo();
				resultData += "\n가맹점주소 : " + mReturnDataBean.getTradeShopAddress();
				resultData += "\nVAN이름 : " + mReturnDataBean.getTradeVanName();
				
				Preferences.putValue(this,Constants.PREFERENCE_MCPAY_TRANSACTION_NO,  mReturnDataBean.getTradePaymentTid());
				Preferences.putValue(this,Constants.PREFERENCE_MCPAY_APPROVAL_NO,  mReturnDataBean.getTradeApprovalNo());
				Preferences.putValue(this,Constants.PREFERENCE_MCPAY_APPROVAL_DATE,  mReturnDataBean.getTradeApprovalDate());
				Preferences.putValue(this,Constants.PREFERENCE_MCPAY_CARD_NO,  "");
				Preferences.putValue(this,Constants.PREFERENCE_MCPAY_CARD_COMPANY,  "");
				finish();
				
				
			}else if(mReturnDataBean.getTradePaymentCode().equals("N05")){
				resultData = "[간이영수증]";
				
				resultData += "\nTID : " + mReturnDataBean.getTradePaymentTid();
				resultData += "\n승인일시 : " + mReturnDataBean.getTradeApprovalDate();
				resultData += "\n가맹점명 : " + mReturnDataBean.getTradeShopName();
				resultData += "\n대표자명 : " + mReturnDataBean.getTradeCeoName();
				resultData += "\n가맹점전화번호 : " + mReturnDataBean.getTradeShopTelNo();
				resultData += "\n가맹점주소 : " + mReturnDataBean.getTradeShopAddress();
				resultData += "\nVAN이름 : " + mReturnDataBean.getTradeVanName();
			}
		}else{
			resultData = "\n응답코드 : " + mReturnDataBean.getTradeResultCode();
			resultData += "\n응답메세지 : " + mReturnDataBean.getTradeResultMsg();
			Preferences.putValue(this,Constants.PREFERENCE_MCPAY_PARAMETER, String.valueOf(Constants.COMMON_TYPE_CARD_PAYMENT_FAIL));
			finish();
		}

		returnText.setText(resultData);
	}

	// 바이트배열 -> 비트맵
	private Bitmap byteArrayToBitmap(byte[] byteArray) {
		Bitmap bitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
		return bitmap;
	}
}
