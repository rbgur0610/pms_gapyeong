package com.pms.gapyeong.pay;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.pms.gapyeong.R;
import com.pms.gapyeong.activity.BaseActivity;
import com.pms.gapyeong.common.Constants;
import com.pms.gapyeong.common.DeviceInfo;
import com.pms.gapyeong.common.MsgUtil;
import com.pms.gapyeong.common.Preferences;
import com.pms.gapyeong.common.Util;
import com.pms.gapyeong.database.Preference;
import com.pms.gapyeong.printer.BluetoothPrintService;
import com.pms.gapyeong.vo.ReceiptVo;
import com.woosim.printer.WoosimImage;
import com.woosim.printer.WoosimService;

public class PayActivity extends BaseActivity implements OnClickListener {
	
	private final int PAYMENT_FINISH = 0x000022333;
	
	private ReceiptVo vo;

	private TextView m_txtInfoMsg;

	private TextView txtAuthno;
	private Bundle eB;
	
	private int    DIALOG_TYPE 	 = 0;
	private String PAYMENT_MONEY = "0";
	private String APPROVAL_NO   = "";
	
	private String card_number   = "";
	private String PARK_TYPE     = "";
	
	private ImageView  iv_card_reading;	
	private ImageView  iv_card_cancel;	
	
	// 승인 요청
	private Button btn_sign; 
	
	// 승인 요청 취소
	private Button btn_auth_cancel;
	
	// 취소
	private Button btn_cancel;
	
	// 결제 관련 객체
	private PayData payData;
	
	private byte[] empLogo_image;
	
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		setTitle("결제하기");
		super.onCreate(savedInstanceState);
	
		Intent i = getIntent();
		DIALOG_TYPE    = i.getIntExtra("SHOW_DIALOG_TYPE", 0);
		PAYMENT_MONEY  = i.getStringExtra("PAYMENT_MONEY");
		PAYMENT_MONEY  = Util.isEmpty(PAYMENT_MONEY) ? "0" : PAYMENT_MONEY;
		PARK_TYPE      = Util.isNVL(i.getStringExtra("PARK_TYPE"));
		APPROVAL_NO    = i.getStringExtra("APPROVAL_NO");
		vo = (ReceiptVo) i.getSerializableExtra("ReceiptVo");
		
		Log.i(TAG," DIALOG_TYPE >>> PAY ::: " + DIALOG_TYPE+"");
		Log.i(TAG," PAYMENT_MONEY >>> PAY ::: " + PAYMENT_MONEY);
		Log.i(TAG," pioNum >>> PAY ::: " + vo.getPioNum());
		Log.i(TAG," carNum >>> PAY ::: " + vo.getCarNum());
		
		setContentView(R.layout.pay_card_auth);

		setupView();		
		
		payData = new PayData(this, OUT_PRINT, IN_PRINT, TICKET_PRINT);
		
		switch (DIALOG_TYPE) {
			case PayData.PAY_DIALOG_CANCEL_ENTRANCE_TIME :	  // 입차 취소 시 결제 취소
			case PayData.PAY_DIALOG_CANCEL_APPROVAL_EXIT :	  // 출차 취소 시 결제 취소
			case PayData.PAY_DIALOG_CANCEL_APPROVAL_CHANGE :  // 주차종류 변경 시 카드 결제 취소
			case PayData.PAY_DIALOG_CANCEL_APPROVAL_CANCEL:
				setTitle("결제취소");
				// 승인취소 버튼 view
				btn_sign.setVisibility(View.GONE);
				btn_auth_cancel.setVisibility(View.VISIBLE); 
				btn_cancel.setVisibility(View.VISIBLE);  
				break;
			default : 
				setupPrint();
				break;				
		}
		
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inScaled = false;
		Bitmap bmp = BitmapFactory.decodeResource(getResources(), R.drawable.logo, options);
        
		if (bmp == null) {
          Log.e(TAG, "resource decoding is failed");
          return;
        
		}
        
		empLogo_image = WoosimImage.printBitmap(0, 0, 320, 108, bmp);
		bmp.recycle();
		
	}
	
	
	private void setupView() {

		m_txtInfoMsg = (TextView) findViewById(R.id.txt_sign_indicator);
		
		iv_card_reading = (ImageView) findViewById(R.id.iv_card_reading);
		iv_card_cancel  = (ImageView) findViewById(R.id.iv_card_cancel);
		
		txtAuthno = (TextView) findViewById(R.id.txtAuthno);
		
		btn_sign        = (Button) findViewById(R.id.btn_sign); 	   // 승인요청버튼
		btn_auth_cancel = (Button) findViewById(R.id.btn_auth_cancel); // 결제취소버튼 
		btn_cancel 	    = (Button) findViewById(R.id.btn_cancel);	   // 취소버튼 
		
		btn_sign.setOnClickListener(this); 		  // 승인요청
		
		btn_auth_cancel.setOnClickListener(this); // 결제취소
		
		btn_cancel.setOnClickListener(this); 	  // cancel

		btn_sign.setEnabled(false); 	     // 승인요청 비활성화
	//	카드리딩하지 않고 바로 취소 가능...
	//	btn_auth_cancel.setEnabled(false); // 승인요청취소 비활성화
		
		txtAuthno.setVisibility(View.GONE);
		
		Animation ani = AnimationUtils.loadAnimation(this, R.anim.animation);
		if (DIALOG_TYPE == PayData.PAY_DIALOG_CANCEL_ENTRANCE_TIME
			|| DIALOG_TYPE == PayData.PAY_DIALOG_CANCEL_APPROVAL_EXIT
			|| DIALOG_TYPE == PayData.PAY_DIALOG_CANCEL_APPROVAL_CHANGE
			|| DIALOG_TYPE== PayData.PAY_DIALOG_CANCEL_APPROVAL_CANCEL) {
			m_txtInfoMsg.setText("결제취소금액 : "+String.valueOf(PAYMENT_MONEY)+"원");
			btn_sign.setVisibility(View.GONE);
			iv_card_reading.setVisibility(View.GONE);
			iv_card_cancel.startAnimation(ani);
		} else {
			m_txtInfoMsg.setText("결제금액 : "+String.valueOf(PAYMENT_MONEY)+"원");
			iv_card_reading.startAnimation(ani);
			iv_card_cancel.setVisibility(View.GONE);
		}
		
	}
	
	@Override
	protected void initLayoutSetting() {
		// TODO Auto-generated method stub
		super.initLayoutSetting();
	}
	
	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		
		setMSRTripleTrackMode();
	}
	
	@Override
	protected void onRestart() {
		super.onRestart();
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if (mPrintService != null) {
			// Only if the state is STATE_NONE, do we know that we haven't
			// started already
			if (mPrintService.getState() == BluetoothPrintService.STATE_NONE) {
				// Start the Bluetooth print services
				mPrintService.start();
			}
		}			
	}	

	@Override
	protected void onPause() {
		super.onPause();
	}

	@Override
	protected void onStop() {
		super.onStop();
	}
	
	private void setupPrint() {
		mWoosim = new WoosimService(mHandler);		
	}		
	
	public void setMSRTripleTrackMode() {
		Log.e("start","setMSRTripleTrackMode");
		byte[] cmd_msr_tpltrack = { PayData.ESC, 0x4d, 0x46 }; // Set MSR triple track
		sendData(cmd_msr_tpltrack);
	}
	
	
	/**
	 * Print data.
	 * 
	 * @param data A byte array to print.
	 */
	private void sendData(byte[] data) {
		// Check that we're actually connected before trying printing

		if (mPrintService==null || mPrintService.getState() != BluetoothPrintService.STATE_CONNECTED) {
			MsgUtil.ToastMessage(this, R.string.not_connected);
			return;
		}

		// Check that there's actually something to send
		Log.e("data.length", String.valueOf(data.length));
		if (data.length > 0)
		{
			mPrintService.write(data);
//			finish();
		}
	}
	

	@Override
	public void onClick(View p_view) {
		if (p_view.getId() == R.id.btn_sign) // 승인요청
		{
			// Log.d(TAG, " card_number >>>!!!! " + card_number);
			if(Util.isEmpty(card_number))
			{
				MsgUtil.AlertDialog(this, "카드정보가 없습니다.\n카드리딩후 결제승인을 진행하실 수 있습니다.");
				return;
			}			
			Log.d(TAG, "승인요청");
			doSignAuthReqPay(PAYMENT_MONEY);
		} else if (p_view.getId() == R.id.btn_auth_cancel) // 승인취소 요청(1114)
		{
			Log.d(TAG, "승인취소 요청");
			doAuthSignCancel();
		} else if (p_view.getId() == R.id.btn_cancel) // 취소
		{
			Log.d(TAG, "취소버튼");
			this.setResult(PayData.PAY_DIALOG_CANCEL);
			finish();
		}
	}
	
	// 승인 요청
	private void doSignAuthReqPay(String money) {
		// Log.d(TAG, " card_number >>>> " + card_number);
		if(Util.isEmpty(card_number)){
			MsgUtil.ToastMessage(this, "카드번호가 유효하지 않습니다.");
			return;
		}
		apiAuthPaymentCall(money);
	}
	
	private void apiAuthPaymentCall(String money){
		
		String[] arrCard = card_number.split("=");
		if(arrCard.length < 2){
			MsgUtil.ToastMessage(this, "카드번호가 유효하지 않습니다.");
			return;
		}
		
		String card_no     = arrCard[0];
		String expiry_date = arrCard[1];
		
		String expiry_yy = expiry_date.substring(0,2);
		String expiry_mm = expiry_date.substring(2,4);

		
		//Log.d(TAG, " expiry_date >>>> " + expiry_date);
		//Log.d(TAG, " expiry_yy >>>> " + expiry_yy);
		//Log.d(TAG, " expiry_mm >>>> " + expiry_mm);
        Map<String, Object> params = new HashMap<String, Object>();
        params.put(Constants.API_FORMAT, Constants.JSON_FORMAT);		
        params.put("id", 	 			"ANDROID");
        params.put("act", 	 			"PAY_ACTION");
        params.put("EMP_CD", 			new Preference(this).getString(Constants.PREFERENCE_LOGIN_ID, ""));
        params.put("PWD", 				new Preference(this).getString(Constants.PREFERENCE_LOGIN_PWD, ""));
        params.put("DEVICE_ID", 	 	Util.getDeviceID(this));
        // 결제방법
        params.put("pay_method", 		"CARD");
        // 카드 결제방법
        params.put("card_pay_method", 	"SSL");
        // 요청종류 승인(pay)
        params.put("req_tx", 			"pay");
        // 결제 금액/화폐단위
        params.put("currency", 			"410");
        // 결제 금액
        params.put("good_mny", 			money);
        // 할부개월
        params.put("quota", 			"00");
        // 할부개월수 옵션 (고객이 선택할 수 있는 할부개월수의 최대값을 세팅, 세팅하지 않을 경우 일시불로 자동 세팅됨)
        params.put("quotaopt", 			"12");
        // 카드번호
        params.put("card_no", 			card_no);
        // 유효기간 년도
        params.put("expiry_yy", 		expiry_yy);
        // 유효기간 월
        params.put("expiry_mm", 		expiry_mm);
        // 주문번호
        params.put("ordr_idxx", 		Util.getYmdhms("yyyyMMdd_HHmmss")+"_"+vo.getCarNum());
        // 상품명
        params.put("good_name", 		PARK_TYPE);
        
        // 상품명
        params.put("GROUP_CD", 		    empGroupCD);        
        // 주문자명
        params.put("buyr_name", 		vo.getCarNum());
        
        showProgressDialog(false);
		String url = Constants.PAY_SERVER;
		Log.d(TAG, " apiAuthPaymentCall url >>> " + url);
		Log.d(TAG, " apiAuthPaymentCall url >>> " + url + " params ::: " + params);//1006-201-
			
		AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>();        
		cb.url(url).params(params) .type(JSONObject.class).weakHandler(this, "authPaymentCallback").redirect(true).retry(3).fileCache(false).expire(-1);
		AQuery aq = new AQuery(this);
		aq.ajax(cb);
				
	}		
	
	public void authPaymentCallback(String url, JSONObject json, AjaxStatus status){
		
		closeProgressDialog();
		
		Log.d(TAG, " authPaymentCallback url >>> " + url);		
	//	Log.d(TAG, " authPaymentCallback  json ====== " +json);		
		
		// successful ajax call          
        if(json != null){   
           String res_msg = json.optString("res_msg");
           if("0000".equals(json.optString("res_cd"))){
	       	 	eB = new Bundle();
	    		eB.putString("transactionNo", json.optString("tno"));
	    		eB.putString("approvalNo",    json.optString("app_no"));
	    		eB.putString("approvalDate",  json.optString("app_time"));
	    		eB.putString("cardNo",        card_number.split("=")[0]);
	    		eB.putString("cardCompany",   json.optString("card_name"));
	    		eB.putString("cardAmt",	      json.optString("good_mny"));
	    		eB.putString("pioNum",		  vo.getPioNum());
	    		eB.putString("carNum",		  vo.getCarNum());
	    		
	    		//Yoon 선불결제 프린트 주석처리
	    		payData.printPayment(DIALOG_TYPE, vo, mPrintService, eB, null);
				payData.ImagePrint(empLogo_image);
				try
				{
					  payData.printText("\n\n");
				}
				catch(UnsupportedEncodingException e)
				{
					if(Constants.DEBUG_PRINT_LOG){
						e.printStackTrace();
					}else{
						System.out.println("예외 발생");
					}
				}
	    		mHandler.sendEmptyMessage(PAYMENT_FINISH);
	    		
           } else {
  	    	 // 승인요청 버튼 활성화
  	    	 // btn_sign.setEnabled(true);		    	
  	    	 MsgUtil.AlertDialogClose(this, res_msg);
  	    	 return;
           }
        } else {
    	    MsgUtil.ToastMessage(this, Constants.DATA_FAIL, Toast.LENGTH_LONG);
        }		
		
	}		
	
	// 결제취소 요청
	private void doAuthSignCancel() {
		if(Util.isEmpty(APPROVAL_NO)){
			MsgUtil.AlertDialogClose(this, "결제취소실패 - 승인번호가 없습니다.");
 	    	return;			
		}
		 apiAuthPaymentCancelCall();
	}
	
	private void apiAuthPaymentCancelCall(){
        Map<String, Object> params = new HashMap<String, Object>();
        params.put(Constants.API_FORMAT, Constants.JSON_FORMAT);		
        params.put("id", 	 		"ANDROID");
        params.put("act", 	 		"PAY_CANCEL_ACTION");
		params.put("EMP_CD", 			new Preference(this).getString(Constants.PREFERENCE_LOGIN_ID, ""));
		params.put("PWD", 				new Preference(this).getString(Constants.PREFERENCE_LOGIN_PWD, ""));
        params.put("DEVICE_ID", 	Util.getDeviceID(this));
        // 요청 구분 STSC - 취소, STPC - 부분취소
        params.put("mod_type", 		"STSC");
        // 요청종류 취소,매입(mod) 
        params.put("req_tx", 		"mod");
        // 승인번호
        params.put("APPROVAL_NO", 	APPROVAL_NO);
        // 주차번호
        params.put("PIO_NUM", 		vo.getPioNum());
        
        // 그룹코드
        params.put("GROUP_CD", 		    empGroupCD);         
        
        showProgressDialog(false);
		String url = Constants.PAY_SERVER;
		
		Log.d(TAG, " apiAuthPaymentCancelCall url >>> " + url);
		Log.d(TAG, " apiAuthPaymentCancelCall url >>> " + url + " params ::: " + params);
		
		AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>();        
		cb.url(url).params(params) .type(JSONObject.class).weakHandler(this, "authPaymentCancelCallback").redirect(true).retry(3).fileCache(false).expire(-1);
		AQuery aq = new AQuery(this);
		aq.ajax(cb);	        
	}
	
	public void authPaymentCancelCallback(String url, JSONObject json, AjaxStatus status){
		
		closeProgressDialog();
		
//		Log.d(TAG, " authPaymentCancelCallback  >>> " + url);	
		Log.d(TAG, " authPaymentCancelCallback  json ====== " +json);		
		
		// successful ajax call          
        if(json != null){   
           String res_msg = json.optString("res_msg");
           if("0000".equals(json.optString("res_cd"))){
   	     	  eB = new Bundle();
	    	  eB.putString("transactionNo", "");
   			  eB.putString("approvalNo", 	"");
   			  eB.putString("approvalDate",	"");
   			  eB.putString("cardNo",		"");
   			  eB.putString("cardCompany",	"");
   			  eB.putString("cardAmt",		PAYMENT_MONEY);
   			  eB.putString("pioNum",		vo.getPioNum());
   			  eB.putString("carNum",		vo.getCarNum());
   			
   			  payData.printPaymentCancel(DIALOG_TYPE, vo, mPrintService, eB, null);
   			  
   			  mHandler.sendEmptyMessage(PAYMENT_FINISH);
   			  
           } else {
	    	 // 승인취소 버튼 활성화
	    	 // btn_auth_cancel.setEnabled(true);	
       	     MsgUtil.AlertDialogClose(this, res_msg);
  	    	 return;
           }
        } else {
    	    MsgUtil.ToastMessage(this, Constants.DATA_FAIL, Toast.LENGTH_LONG);
        }		
		
	}			
		
	private void finishVan() {
		try {
			Intent i = new Intent();
			i.putExtras(eB);
			this.setResult(DIALOG_TYPE, i);
			finish();
		} catch (NullPointerException e) {
			if(Constants.DEBUG_PRINT_LOG){
				e.printStackTrace();
			}else{
				System.out.println("예외 발생");
			}
		}
	}	
	
	
	@Override
	protected void HandlerListener(Message msg) {
		// TODO Auto-generated method stub
		super.HandlerListener(msg);
		Log.e("HandlerListener", " PAY ==== " + String.valueOf(msg.what));
		switch (msg.what) {
		  case PAYMENT_FINISH:
			finishVan();
			break;
	 	  case MESSAGE_READ:
	 		  Log.e("HandlerListener"," PAY - MESSAGE_READ MAIN ACTIVITY");
	 		  mWoosim.processRcvData((byte[]) msg.obj, msg.arg1);
	 		  break;	   
		  case WoosimService.MESSAGE_PRINTER:
			Log.e("HandlerListener", "PAY - MESSAGE_PRINTER " + msg);
			switch (msg.arg1) {
			  case WoosimService.MSR:
				if (msg.arg2 == 0) {
					MsgUtil.ToastMessage(this, "카드를 읽지 못했습니다. 다시 시도해주세요.");
					setMSRTripleTrackMode();
					return;	
				} else {
					byte[][] track = (byte[][]) msg.obj;
					
					if (track[0] == null){
					  MsgUtil.ToastMessage(this, "카드를 읽지 못했습니다. 다시 시도해주세요.");
					  setMSRTripleTrackMode();
					  return;	
					}

					// 카드리딩 1번 정보  
					if (track[0] != null) {
					//	String trackStr = new String(track[0]);
					//	Log.d("mTrack1View1", " CARD_READ ::: " + trackStr);
					}

					
					// 카드리딩 2번 정보
					if (track[1] != null) {
						String trackStr = new String(track[1]);
					//	Log.d("mTrack1View2", " CARD_READ ::: " + trackStr);
						
						card_number = trackStr;
						txtAuthno.setText("카드번호 :"+trackStr.split("=")[0]);
						
						// 결제취소이면 승인취소버튼 활성화 
						if(DIALOG_TYPE == PayData.PAY_DIALOG_CANCEL_ENTRANCE_TIME
							|| DIALOG_TYPE == PayData.PAY_DIALOG_CANCEL_APPROVAL_EXIT
							|| DIALOG_TYPE == PayData.PAY_DIALOG_CANCEL_APPROVAL_CHANGE
							|| DIALOG_TYPE == PayData.PAY_DIALOG_CANCEL_APPROVAL_CANCEL){
							btn_sign.setEnabled(false); 	 
							btn_auth_cancel.setEnabled(true); 	
						} else { // 결제이면 승인버튼 활성화
							btn_sign.setEnabled(true); 	 
							btn_auth_cancel.setEnabled(false);
							txtAuthno.setVisibility(View.VISIBLE);
							// 2015.01.18 - 카드 리딩 성공 후 사인 승인하지 않고 바로 결제...  
							doSignAuthReqPay(PAYMENT_MONEY);						  
						}						
					    // AlertShow("카드정보가 확인되었습니다.\n서명 후 승인요청을 진행해주세요.");
					} else { 	// 카드리딩 정보 확인 실패
						MsgUtil.ToastMessage(this, "카드를 읽지 못했습니다. 다시 시도해주세요.");
						setMSRTripleTrackMode();
						return;
					}
					
					// 카드리딩 3번 정보
					if (track[2] != null) {
					//	String trackStr2 = new String(track[2]);
					//	Log.d("mTrack1View3", " CARD_READ ::: " + trackStr2);
					}
					
				}
				break;
			}
			break;
		}

	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (requestCode) {
		  default :
			 break;
		}
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event){
		// 백키 막음...
		switch(keyCode) {
			case KeyEvent.KEYCODE_HOME:
			case KeyEvent.KEYCODE_BACK:
				return false;
		}
		return true;
	}	
	
	
}
